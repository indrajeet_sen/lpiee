#-injars      libs
#-outjars     bin/classes-processed.jar
-dontpreverify
-repackageclasses ''
-allowaccessmodification
-optimizations !code/simplification/arithmetic
-keepattributes *Annotation*
-ignorewarnings

#jodaTime
-dontwarn org.joda.convert.**
-dontwarn org.joda.time.**


-dontwarn android.support.**
-dontoptimize
-dontpreverify

-assumenosideeffects class android.util.Log {
    public static *** d(...);
    public static *** v(...);
    public static *** e(...);
    public static *** i(...);
}

-keep class javax.** { *; }
-keep class org.** { *; }
-keep class net.sqlcipher.** { *; }
-keep class net.sqlcipher.database.** { *; }

-dontwarn org.**
-dontwarn nu.xom.**

-dontwarn org.xmlpull.v1.**
-dontnote org.xmlpull.v1.**

-keepattributes InnerClasses
-dontoptimize
#-optimizations optimization-filter


-keep class butterknife.*
-keepclasseswithmembernames class * { @butterknife.* <methods>; }
-keepclasseswithmembernames class * { @butterknife.* <fields>; }
#-keep public class * implements butterknife.Unbinder { public <init>(**, android.view.View); }
#-keep class com.github.** { *; }
#-keep class pl.droidsonroids.gif.** { *; }
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

-keep class com.google.android.gms.safetynet.** { *; }
-dontwarn com.google.android.gms.safetynet.**

-keep class com.isys.Security.** { *; }
-dontwarn com.isys.Security.**

-dontwarn android.net.http.*
-dontwarn okio.**
-dontwarn com.squareup.okhttp.**
-dontwarn org.xmlpull.v1.**

-keep class com.google.android.gms.safetynet.** { *; }
-dontwarn com.google.android.gms.safetynet.**