package com.lpi.kmi.lpi.DBHelper;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.SQLException;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.stringcare.library.SC;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

/**
 * Created by mustafakachwalla on 23/05/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static SQLiteDatabase db;
    private static DatabaseHelper instance;
    Context context;


    public DatabaseHelper(Context cntxt) {
        super(cntxt, SC.reveal(CommonClass.DBName), null, cntxt.getResources().getInteger(R.integer.dbVersion));
        this.context = cntxt;
        CommonClass.setLaguage(cntxt, "en");
        StaticVariables.sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

        try {
            SQLiteDatabase.loadLibs(context);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            db = this.getWritableDatabase(SC.reveal(CommonClass.DBPassword));
            int Old_version = db.getVersion();
            int version = cntxt.getResources().getInteger(R.integer.dbVersion);
            if (Old_version != version) {
                db.beginTransaction();
                try {
                    if (Old_version == 0) {
                        onCreate(db);
                    } else {
                        if (Old_version < version) {
                            onUpgrade(db, Old_version, version);
                        }
                    }
                    db.setVersion(version);
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
            } else {
                onCreate(db);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

 /*   public SQLiteDatabase getWritableDatabase() {
        String dbPath = context.getDatabasePath(context.getResources().getString(R.string.DBName)).getAbsolutePath();
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(dbPath,context.getResources().getString(R.string.DBPassword),
                null);
        if (db != null ){
            db = SQLiteDatabase.openDatabase(dbPath,context.getResources().getString(R.string.DBPassword).toCharArray()
                    ,null,SQLiteDatabase.OPEN_READWRITE);
        }
        return db;
    }*/

    static public synchronized DatabaseHelper getInstance(Context cntxt) {
        if (instance == null) {
            instance = new DatabaseHelper(cntxt);
        }
        db = instance.getWritableDatabase(SC.reveal(CommonClass.DBPassword));

        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_iCandidate));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_IsysLookupParam));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPICandidateExamAttempt));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPICandidateExamAttemptDtl));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPICandidateExamMapping));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPICandidateExamResult));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPICandidateExamResultFeedback));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPIChangeMaster));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPIExamAnswerMST));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPIExamMST));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPIExamQuestionMST));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPIQuestionCorrectAnswerMapping));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_ExamQueAns_SyncMaster));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_LPICandidateAttemptQueAns));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPICandidateAttemptedExam));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_LPIErrorLog));
            db.execSQL(context.getResources().getString(R.string.create_TblPostLicModuleData));
            db.execSQL(context.getResources().getString(R.string.create_TblActivityMaster));
            db.execSQL(context.getResources().getString(R.string.create_TblActivityTracker));
            db.execSQL(context.getResources().getString(R.string.CreateTblLPI_BasicDetails));
            db.execSQL(context.getResources().getString(R.string.CreateTblLPI_ViewSummary));
            db.execSQL(context.getResources().getString(R.string.CreateTblLPI_ZipContent));
            db.execSQL(context.getResources().getString(R.string.CreateTblLPI_ContentQueries));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_LPISysGeneratedXML));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPICandidateRegistration));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_FAQ_Conversation));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_FAQ_List));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_ReferApp));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_Profession));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_Country));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_FAQ_Group));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_SearchResult));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_CndFeedback));
            db.execSQL(context.getResources().getString(R.string.createtbl_settings));
            db.execSQL(context.getResources().getString(R.string.createTbl_TokenMst));
            db.execSQL(context.getResources().getString(R.string.createtbl_SectionExamMapping));

        } catch (Exception e) {
            Log.e("", "Error At table = " + e.toString());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {
            db.execSQL(context.getResources().getString(R.string.dropTbl_iCandidate));
            db.execSQL(context.getResources().getString(R.string.dropIsysLookupParam));
            db.execSQL(context.getResources().getString(R.string.dropLPICandidateExamAttempt));
            db.execSQL(context.getResources().getString(R.string.dropLPICandidateExamAttemptDtl));
            db.execSQL(context.getResources().getString(R.string.dropLPICandidateExamMapping));
            db.execSQL(context.getResources().getString(R.string.dropLPICandidateExamResult));
            db.execSQL(context.getResources().getString(R.string.dropLPICandidateExamResultFeedback));
            db.execSQL(context.getResources().getString(R.string.dropSyncMstSU));
            db.execSQL(context.getResources().getString(R.string.dropLPIExamAnswerMST));
            db.execSQL(context.getResources().getString(R.string.dropLPIExamMST));
            db.execSQL(context.getResources().getString(R.string.dropLPIExamQuestionMST));
            db.execSQL(context.getResources().getString(R.string.dropLPIQuestionCorrectAnswerMapping));
            db.execSQL(context.getResources().getString(R.string.dropLPIExamQueAns_SyncMaster));
            db.execSQL(context.getResources().getString(R.string.dropLPICandidateAttemptQueAns));
            db.execSQL(context.getResources().getString(R.string.dropLPICandidateAttemptedExam));
            db.execSQL(context.getResources().getString(R.string.dropLPIErrorLog));
            db.execSQL(context.getResources().getString(R.string.dropPostLicModuleData));
            db.execSQL(context.getResources().getString(R.string.dropActivityMaster));
            db.execSQL(context.getResources().getString(R.string.dropActivityTracker));
            db.execSQL(context.getResources().getString(R.string.dropCreateTblLPI_BasicDetails));
            db.execSQL(context.getResources().getString(R.string.dropLPI_ViewSummary));
            db.execSQL(context.getResources().getString(R.string.dropZipContent));
            db.execSQL(context.getResources().getString(R.string.dropContentQueries));
            db.execSQL(context.getResources().getString(R.string.dropLPISysGeneratedXML));
            db.execSQL(context.getResources().getString(R.string.dropLPICandidateRegistration));
            db.execSQL(context.getResources().getString(R.string.dropFAQ_List));
            db.execSQL(context.getResources().getString(R.string.dropReferApp));
            db.execSQL(context.getResources().getString(R.string.dropProfession));
            db.execSQL(context.getResources().getString(R.string.dropCountry));
            db.execSQL(context.getResources().getString(R.string.dropFAQ_Group));
            db.execSQL(context.getResources().getString(R.string.dropTBL_SearchResult));
            db.execSQL(context.getResources().getString(R.string.dropTBL_CndFeedback));
            db.execSQL(context.getResources().getString(R.string.dropTbl_TokenMst));
            db.execSQL(context.getResources().getString(R.string.dropSettings));
            db.execSQL(context.getResources().getString(R.string.dropFAQ_Conversation));
            db.execSQL(context.getResources().getString(R.string.dropTbl_SectionExamMapping));
//            DeleteAllRec(context, db);
            onCreate(db);
        } else {
            onCreate(db);
        }
    }

    public static Cursor getBasicDetails(Context cnxt, SQLiteDatabase db, String appID) {
        Cursor mCursor;
        mCursor = db.query(cnxt.getResources().getString(R.string.TBL_CreateTblLPI_BasicDetails), null, "AppNo=?",
                new String[]{"" + appID}, null, null, null, null);
        return mCursor;

    }

    public static void DeleteAllRec(Context cnxt, SQLiteDatabase db) {

        try {
            db.delete(cnxt.getResources().getString(R.string.TBL_iCandidate), null, null);
            db.delete(cnxt.getResources().getString(R.string.TBL_IsysLookupParam), null, null);
            db.delete(cnxt.getResources().getString(R.string.TBL_LPICandidateExamAttempt), null, null);
            db.delete(cnxt.getResources().getString(R.string.TBL_LPICandidateExamAttemptDtl), null, null);
            db.delete(cnxt.getResources().getString(R.string.TBL_LPICandidateExamMapping), null, null);
            db.delete(cnxt.getResources().getString(R.string.TBL_LPICandidateExamResult), null, null);
            db.delete(cnxt.getResources().getString(R.string.TBL_LPICandidateExamResultFeedback), null, null);
//            db.delete(cnxt.getResources().getString(R.string.TBL_LPIChangeMaster),null,null);
            db.delete(cnxt.getResources().getString(R.string.TBL_LPIExamAnswerMST), null, null);
            db.delete(cnxt.getResources().getString(R.string.TBL_LPIExamMST), null, null);
            db.delete(cnxt.getResources().getString(R.string.TBL_LPIExamQuestionMST), null, null);
            db.delete(cnxt.getResources().getString(R.string.TBL_LPIQuestionCorrectAnswerMapping), null, null);
            db.delete(cnxt.getResources().getString(R.string.TBL_LPIExamQueAns_SyncMaster), null, null);

            db.delete(cnxt.getResources().getString(R.string.TblPostLicModuleData), null, null);
            db.delete(cnxt.getResources().getString(R.string.TblActivityMaster), null, null);
            db.delete(cnxt.getResources().getString(R.string.TblActivityTracker), null, null);
            db.delete(cnxt.getResources().getString(R.string.TBL_ZipContent), null, null);
            db.delete(cnxt.getResources().getString(R.string.TBL_LPICandidateRegistration), null, null);
            db.delete(cnxt.getResources().getString(R.string.TBL_FAQ_Conversation), null, null);
            db.delete(cnxt.getResources().getString(R.string.TBL_LPISysGeneratedXML), null, null);
            db.delete(cnxt.getResources().getString(R.string.TBL_ContentQueries), null, null);
            db.delete(cnxt.getResources().getString(R.string.dropTbl_TokenMst), null, null);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("", "Exception=" + e.toString());
        }
    }

    public static void SaveUserInfo(Context cnxt, SQLiteDatabase db, String str_LoginId,
                                    String str_LoginPin, String str_Name, String str_InscType,
                                    String AllowOfflineMode, String PermisableDays,
                                    String LastAccessDate, String UserType,
                                    String emailid, String mobile, String ParentId, String ParentName,
                                    byte[] Image,
                                    String AppLink) {

        Cursor cquery = null;
        try {
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("CandidateLoginId", str_LoginId);
            cv.put("CandidatePin", str_LoginPin);
            cv.put("FirstName", str_Name);
            cv.put("InscType", str_InscType);
            cv.put("AllowOfflineMode", AllowOfflineMode);
            cv.put("PermisableDays", PermisableDays);
            cv.put("LastAccessDate", LastAccessDate);
            cv.put("UserType", UserType);
            cv.put("CandidateEmailId", emailid);
            cv.put("CandidateMobileNo1", mobile);
            cv.put("ParentId", ParentId);
            cv.put("ParentName", ParentName);
            cv.put("Image", Image);
            cv.put("is_mPIN_Data", "");
            cv.put("mPIN", "");
            cv.put("AppLink", AppLink);

            cquery = db.query(cnxt.getString(R.string.TBL_iCandidate), null, null, null, null, null, null);
            if (cquery.getCount() > 0) {
                db.delete(cnxt.getString(R.string.TBL_iCandidate), null, null);
//                db.update(cnxt.getString(R.string.TBL_iCandidate), cv, "CandidateLoginId=?", new String[]{str_LoginId});
                db.insertOrThrow(cnxt.getString(R.string.TBL_iCandidate), null, cv);
            } else
                db.insertOrThrow(cnxt.getString(R.string.TBL_iCandidate), null, cv);

            cquery.close();

        } catch (Exception e) {
            Log.e("SaveUserInfo", "err=" + e.toString());
            DatabaseHelper.SaveErroLog(cnxt, DatabaseHelper.db,
                    "DatabaseHelper", "SaveUserInfo", e.toString());
        }

    }

    public static void SaveErroLog(Context cntxt, SQLiteDatabase db,
                                   String PageName, String FuncName, String ErrDesc) {

        String BrandName = Build.MODEL;
        int DeviceAPILevel = Build.VERSION.SDK_INT; // 19
        String OSVersion = Build.VERSION.RELEASE; // 4.4.2
        String DeviceID = Settings.Secure.getString(cntxt.getContentResolver(), Settings.Secure.ANDROID_ID);
        String IMEINo = CommonClass.getIMEINumber(cntxt);
        String OSName = Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();
        String AppName = "LPIEE";

        try {
            ContentValues cv = new ContentValues();
            cv.clear();
//            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
            String TodaysDate = StaticVariables.sdf.format(Calendar.getInstance().getTime());

            cv.put("LogDtime", "" + TodaysDate);
            cv.put("AppName", AppName);
            cv.put("PageName", PageName);
            cv.put("FuncName", FuncName);
            cv.put("ErrDesc", ErrDesc);
            cv.put("UserId", "" + StaticVariables.UserLoginId);
            cv.put("Severity", "Medium");
            cv.put("DeviceID", DeviceID);
            cv.put("IMEI", IMEINo);
            cv.put("BrandName", BrandName);
            cv.put("ModelName", BrandName);
            cv.put("DeviceAPILevel", DeviceAPILevel);
            cv.put("OSName", OSName);
            cv.put("OSVersion", OSVersion);
            db.insertOrThrow(cntxt.getResources().getString(R.string.TBL_LPIErrorLog), null, cv);
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }

    }

    public static void SaveSearchResult(Context context, SQLiteDatabase db, String CandidateId,
                                        String Name, String Mobile, byte[] Images, String ExamId,
                                        String PPAttempted, String CreateDte, String ProfileName) {

        Cursor cursor;
        ContentValues values = new ContentValues();
        values.clear();

        values.put("CandidateId", CandidateId);
        values.put("Name", Name);
        values.put("Mobile", Mobile);
        values.put("Images", Images);
        values.put("ExamId", ExamId);
        values.put("PPAttempted", PPAttempted);
        values.put("CreateDte", CreateDte);
        values.put("ProfileName", ProfileName);

        try {
            cursor = db.query(context.getResources().getString(R.string.TBL_SearchResult), null,
                    "CandidateId =? and ExamId=?",
                    new String[]{CandidateId, ExamId}, null, null, null, null);
            if (cursor.getCount() > 0) {
                db.update(context.getResources().getString(R.string.TBL_SearchResult), values,
                        "CandidateId =? and ExamId=?", new String[]{CandidateId, ExamId});
            } else {
                db.insertOrThrow(context.getResources().getString(R.string.TBL_SearchResult), null, values);
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void SaveCndFeedback(Context context, SQLiteDatabase db, String CandidateId, String ExamId,
                                       String ExamDesc, String ExamDetails, String QuestionId, String Question,
                                       String AttempId, String FeedbackFor, String Feedback, String RateResult) {

        Cursor cursor;
        ContentValues values = new ContentValues();
        values.clear();

        values.put("CandidateId", CandidateId);
        values.put("ExamId", ExamId);
        values.put("ExamDesc", ExamDesc);
        values.put("ExamDetails", ExamDetails);
        values.put("QuestionId", QuestionId);
        values.put("Question", Question);
        values.put("AttempId", AttempId);
        values.put("FeedbackFor", FeedbackFor);
        values.put("Feedback", Feedback);
        values.put("RateResult", RateResult);

        try {
            cursor = db.query(context.getResources().getString(R.string.TBL_CndFeedback), null,
                    "CandidateId =? and ExamId=? and QuestionId=?",
                    new String[]{CandidateId, ExamId, QuestionId}, null, null, null, null);
            if (cursor.getCount() > 0) {
                db.update(context.getResources().getString(R.string.TBL_CndFeedback), values,
                        "CandidateId =? and ExamId=? and QuestionId=?",
                        new String[]{CandidateId, ExamId, QuestionId});
            } else {
                db.insertOrThrow(context.getResources().getString(R.string.TBL_CndFeedback), null, values);
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void UpdateCndFeedback(Context context, SQLiteDatabase db, String CandidateId, String ExamId,
                                         String QuestionId, String Feedback) {

        ContentValues values = new ContentValues();
        values.clear();

        values.put("CandidateId", CandidateId);
        values.put("Feedback", Feedback);

        try {

            db.update(context.getResources().getString(R.string.TBL_CndFeedback), values,
                    "CandidateId =? and ExamId=? and QuestionId=?",
                    new String[]{CandidateId, ExamId, QuestionId});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String Validate_OfflineIDPwd(Context cnxt, SQLiteDatabase db, String str_LoginId, String str_LoginPin) {

        String return_str = "";
        Cursor cquery;
        cquery = db.query(cnxt.getString(R.string.TBL_iCandidate), null, "CandidateLoginId=? and CandidatePin=?", new String[]{str_LoginId, str_LoginPin}, null, null, null);
        if (cquery.getCount() > 0)
            return_str = "SUCCESS";
        else {
            return_str = "FAIL";
        }
        cquery.close();

        return return_str;
    }

    // TODO: Get Questions selected answer sequence
    public static String GetAtmptdAnsSeq(Context cnxt, SQLiteDatabase db, String LoginId,
                                         String ExamId, String QuestionId, String AttemptId) {
        Cursor cursor;
        String AnsSeq = "";
        cursor = db.query(cnxt.getResources().getString(R.string.TBL_LPICandidateAttemptQueAns), new String[]{"AnswerSequens"},
                "LoginId=? and ExamId=? and QuestionId=? and AttemptId=?",
                new String[]{LoginId, ExamId, QuestionId, AttemptId}, null, null, null);
        if (cursor.getCount() > 0) {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                AnsSeq = cursor.getString(0).toString();
            }
        }
        cursor.close();
        return AnsSeq;
    }

    // TODO: To save attempted questios's answer
    public static void SaveAttemptedQueAns(Context cnxt, SQLiteDatabase db, String LoginId,
                                           String ExamId, String ExamName,
                                           String QuestionId, String QuestionAttempted, String AnswerId,
                                           String AnswerSequens, String Feedback, String AttemptId) {

        try {
            ContentValues cv = new ContentValues();
            cv.clear();
//            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
            String TodaysDate = StaticVariables.sdf.format(Calendar.getInstance().getTime());

            cv.put("LoginId", LoginId);
            cv.put("ExamId", ExamId);
            cv.put("ExamName", ExamName);
            cv.put("QuestionId", QuestionId);
            cv.put("QuestionAttempted", QuestionAttempted);
            cv.put("AnswerId", AnswerId);
            cv.put("AnswerSequens", AnswerSequens);
            cv.put("AttemptId", AttemptId);
            cv.put("QueAttemtedBy", LoginId);
            cv.put("QueAttemtedDate", TodaysDate);
            cv.put("ExamStartDtime", TodaysDate);
            cv.put("ExamEndDtime", TodaysDate);
            cv.put("Feedback", Feedback);

            Cursor cquery = db.query(cnxt.getString(R.string.TBL_LPICandidateAttemptQueAns), null,
                    "ExamId=? and QuestionId=? and LoginId=? and AttemptId=?",
                    new String[]{ExamId, QuestionId, LoginId, AttemptId}, null, null, null);
            if (cquery.getCount() > 0)
                db.update(cnxt.getString(R.string.TBL_LPICandidateAttemptQueAns), cv,
                        "ExamId=? and QuestionId=? and LoginId=? and AttemptId=?",
                        new String[]{ExamId, QuestionId, LoginId, AttemptId});
            else
                db.insertOrThrow(cnxt.getString(R.string.TBL_LPICandidateAttemptQueAns), null, cv);
            cquery.close();
//            db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_LPICandidateAttemptQueAns), null, cv);
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
    }

    // TODO: Offline Login]

    // TODO: To save Offline Generated XML
    public static void SaveXMLData(Context cnxt, SQLiteDatabase db, String XMLData, String WebMethodName) {

        try {
            ContentValues cv = new ContentValues();
            cv.clear();

            cv.put("XMLData", XMLData);
            cv.put("WebMethodName", WebMethodName);
            cv.put("SyncStatus", "Pending");
//            Cursor cquery = db.query(cnxt.getString(R.string.TBL_LPISysGeneratedXML), null, "WebMethodName=?", new String[]{WebMethodName}, null, null, null);
//            if (cquery.getCount() > 0)
//                db.update(cnxt.getString(R.string.TBL_LPISysGeneratedXML), cv, "WebMethodName=?", new String[]{WebMethodName});
//            else
            db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_LPISysGeneratedXML), null, cv);
//            cquery.close();
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
    }

    public static void SaveXMLData(Context cnxt, SQLiteDatabase db, String XMLData, String WebMethodName, String status) {

        try {
            ContentValues cv = new ContentValues();
            cv.clear();

            cv.put("XMLData", XMLData);
            cv.put("WebMethodName", WebMethodName);
            cv.put("SyncStatus", status);
//            Cursor cquery = db.query(cnxt.getString(R.string.TBL_LPISysGeneratedXML), null, "WebMethodName=?", new String[]{WebMethodName}, null, null, null);
//            if (cquery.getCount() > 0)
//                db.update(cnxt.getString(R.string.TBL_LPISysGeneratedXML), cv, "WebMethodName=?", new String[]{WebMethodName});
//            else
            db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_LPISysGeneratedXML), null, cv);
//            cquery.close();
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
    }

    public static void UpdateXMLData(Context cnxt, SQLiteDatabase db, String RecId,
                                     String WebMethodName, String status) {
        try {
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("SyncStatus", status);
            Cursor cquery = db.query(cnxt.getString(R.string.TBL_LPISysGeneratedXML), null, "WebMethodName=? and RecId=?",
                    new String[]{WebMethodName, RecId}, null, null, null);
            if (cquery.getCount() > 0)
                db.update(cnxt.getString(R.string.TBL_LPISysGeneratedXML), cv, "WebMethodName=? and RecId=?",
                        new String[]{WebMethodName, RecId});
            cquery.close();
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
    }

    public static Cursor GetXMLData(Context cnxt, SQLiteDatabase db) {
        Cursor cquery = null;
        try {
            cquery = db.query(cnxt.getString(R.string.TBL_LPISysGeneratedXML), null, "SyncStatus=?", new String[]{"Pending"}, null, null, null);
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
        return cquery;
    }

    public static void SaveQueAnsForFeedback(Context cnxt, SQLiteDatabase db, String ExamId,
                                             String QueId, String Question) {

        SaveQuestionMaster(cnxt, db,
                ExamId, QueId, "Q" + QueId, Question, "0", "", "", "5");

    }

    public static int SaveExamMaster(Context cnxt, SQLiteDatabase db, String ExamId, String ExamCode,
                                     String ExamType, String ExamDesc, String ExamDetails,
                                     String AllowNavigation_flag, String OptionCount, String ShowRandomQuestions,
                                     String TotalQuestions, String TotalTimeAllowedInMinutes,
                                     String IsAllowResultFeedback, String IsPracticeExam,
                                     String AllowedAttempt, String EffectiveDate, String IsAllQuesMendatory,
                                     String NoOfAttempts) {
        int operation = 0; //0- No notification , 1-Show Notification
        try {

            ContentValues cv = new ContentValues();
            cv.clear();


            cv.put("ExamId", ExamId);
            cv.put("ExamCode", ExamCode);
            cv.put("ExamType", ExamType);
            cv.put("ExamDesc", ExamDesc);
            cv.put("ExamDetails", ExamDetails);
            cv.put("AllowNavigationFlag", AllowNavigation_flag);
            cv.put("OptionCount", OptionCount);
            cv.put("ShowRandomQuestions", ShowRandomQuestions);
            cv.put("TotalQuestions", TotalQuestions);
            cv.put("TotalTimeAllowedInMinutes", TotalTimeAllowedInMinutes);
            cv.put("IsAllowResultFeedback", IsAllowResultFeedback);
            cv.put("IsPracticeExam", IsPracticeExam);
            cv.put("AllowedAttempt", AllowedAttempt);
            cv.put("NoOfAttempts", NoOfAttempts);
            cv.put("EffectiveDate", EffectiveDate);
            cv.put("SyncedToLocalFlag", "Complete");
            cv.put("LoginId", StaticVariables.UserLoginId);
            cv.put("IsAllQuesMendatory", IsAllQuesMendatory);
// and ExamDesc=?
            Cursor cursor = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamMST),
                    null, "ExamId=?", new String[]{ExamId}, null, null, null);
            if (cursor.getCount() > 0) {
                operation = 0;
                db.update(cnxt.getResources().getString(R.string.TBL_LPIExamMST), cv, "ExamId=?", new String[]{ExamId});
            } else {
                db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_LPIExamMST), null, cv);
                operation = 1;
            }
            cursor.close();
        } catch (Exception e) {
            Log.e("", "SaveExamMaster Error=" + e.toString());
        }
        return operation;
    }

    // TODO : Temperory saving of attempted exam
    public static int SaveAttemptedExam(Context cnxt, SQLiteDatabase db, String ExamId, String ExamName,
                                        String syncFlag, String Attempt, String CandidateId) {
        int count = 0;
        try {
            Cursor cquery;

            if (Attempt.equals(null) || Attempt.equals("") /*|| Attempt.equals("0")*/) {
                count = 1;
            } else {
                count = Integer.parseInt(Attempt);
            }
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPICandidateAttemptedExam), null, "ExamId=? and CandidateId=?",
                    new String[]{ExamId, CandidateId}, null, null, null);
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("ExamId", ExamId);
            cv.put("ExamName", ExamName);
            cv.put("CandidateId", CandidateId);
            cv.put("SyncToServer_Flag", syncFlag);
            cv.put("Attempt", "" + count);
            if (cquery.getCount() <= 0) {
                db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_LPICandidateAttemptedExam), null, cv);
            } else if (cquery.getCount() > 0) {
//                if (Attempt.equals("") || Attempt.equals(null) || Attempt.equals("0")) {
//                    count = "" + cquery.getCount();
//                    cv.put("Attempt", count);
//                }else {
//                    count=Attempt;
//                    cv.put("Attempt", Attempt);
//                }
//                db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_LPICandidateAttemptedExam),
//                        null, cv);
                db.update(cnxt.getResources().getString(R.string.TBL_LPICandidateAttemptedExam), cv,
                        "ExamId=? and CandidateId=?", new String[]{ExamId, CandidateId});
            }
            cquery.close();
        } catch (Exception e) {
            Log.e("", "SaveAttemptedExam Error=" + e.toString());
        }
        return count;
    }

    // TODO : Temperory saving of attempted exam
    public static void UpdateAttemptedExam(Context cnxt, SQLiteDatabase db, String ExamId, String ExamName,
                                           String syncFlag, String Attempt, String CandidateId) {
        try {
            Cursor cquery;
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPICandidateAttemptedExam), null,
                    "ExamId=? and Attempt=? and CandidateId=?",
                    new String[]{ExamId, Attempt, CandidateId}, null, null, null);
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("SyncToServer_Flag", syncFlag);
            if (cquery.getCount() > 0) {
                db.update(cnxt.getResources().getString(R.string.TBL_LPICandidateAttemptedExam), cv,
                        "ExamId=? and Attempt=?", new String[]{ExamId, Attempt});
            }
            cquery.close();

        } catch (Exception e) {
            Log.e("", "SaveAttemptedExam Error=" + e.toString());
        }
    }

    public static int DeleteAttemptedExam(Context cnxt, SQLiteDatabase db, String ExamId, String CandidateId) {
        int cnt = 0;
        try {
            if (GetAttemptedExam(cnxt, db, ExamId, CandidateId) != 0)
                db.delete(cnxt.getResources().getString(R.string.TBL_LPICandidateAttemptedExam),
                        "ExamId=? and CandidateId=?", new String[]{ExamId, CandidateId});
        } catch (Exception e) {
            Log.e("", "GetAttemptedExam err=" + e.toString());
        }

        return cnt;
    }

    // TODO : Temperory saving of attempted exam
    public static int GetAttemptedExam(Context cnxt, SQLiteDatabase db, String ExamId, String CandidateId) {
        int cnt = 0;
        try {
            Cursor cquery;
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPICandidateAttemptedExam), null, "ExamId=? and CandidateId=?",
                    new String[]{ExamId, CandidateId}, null, null, null);
            cnt = cquery.getCount();
            cquery.close();
        } catch (Exception e) {
            Log.e("", "GetAttemptedExam err=" + e.toString());
        }

        return cnt;
    }

    public static Cursor GetAttemptedExamDetails(Context cnxt, SQLiteDatabase db, String ExamId, String CandidateId) {
        Cursor mCursor = null;
        try {
            mCursor = db.query(cnxt.getResources().getString(R.string.TBL_LPICandidateAttemptedExam), null, "ExamId=? and CandidateId=?",
                    new String[]{ExamId, CandidateId}, null, null, null);
        } catch (Exception e) {
            Log.e("", "GetAttemptedExam err=" + e.toString());
        }
        return mCursor;
    }

    public static int SaveQuestionMaster(Context cnxt, SQLiteDatabase db, String ExamId, String QuestionId,
                                         String QuestionCode, String Question, String NoOfAttemptAllow,
                                         String CorrectAnsRemark, String WrongAnsRemark, String OptionCount) {
        int opr = 0;
        try {

            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("ExamId", ExamId);
            cv.put("QuestionId", QuestionId);
            cv.put("QuestionCode", QuestionCode);
            cv.put("Question", Question);
            cv.put("NoOfAttemptAllow", NoOfAttemptAllow);
            cv.put("CorrectAnsRemark", CorrectAnsRemark);
            cv.put("WrongAnsRemark", WrongAnsRemark);
            cv.put("OptionCount", OptionCount);
            cv.put("SyncedToLocalFlag", "Complete");
            cv.put("LoginId", StaticVariables.UserLoginId);

            Cursor cursor = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamQuestionMST),
                    null, "ExamId=? and QuestionId=?", new String[]{ExamId, QuestionId}, null, null, null);
            if (cursor.getCount() > 0) {
                db.update(cnxt.getResources().getString(R.string.TBL_LPIExamQuestionMST), cv, "ExamId=? and QuestionId=?", new String[]{ExamId, QuestionId});
                opr = 0;
            } else {
                db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_LPIExamQuestionMST), null, cv);
                opr = 1;
            }
            cursor.close();
//            db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_LPIExamQuestionMST), null, cv);
        } catch (Exception e) {
            Log.e("", "SaveQuestionMaster Error=" + e.toString());
        }
        return opr;
    }

    // TODO : To insert records in answer master
    public static void SaveAnswerMaster(Context cnxt, SQLiteDatabase db, String ExamId, String QuestionId,
                                        String AnswerId, String AnswerCode, String Answer) {

        try {

            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("ExamId", ExamId);
            cv.put("QuestionId", QuestionId);
            cv.put("AnswerId", AnswerId);
            cv.put("AnswerCode", AnswerCode);
            cv.put("Answer", Answer);

            Cursor cursor = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamAnswerMST),
                    null, "ExamId=? and QuestionId=? and AnswerId=?", new String[]{ExamId, QuestionId, AnswerId}, null, null, null);
            if (cursor.getCount() > 0) {
                db.update(cnxt.getResources().getString(R.string.TBL_LPIExamAnswerMST), cv, "ExamId=? and QuestionId=? and AnswerId=?", new String[]{ExamId, QuestionId, AnswerId});
            } else {
                db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_LPIExamAnswerMST), null, cv);
            }
            cursor.close();
//            db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_LPIExamAnswerMST), null, cv);
        } catch (Exception e) {
            Log.e("", "SaveAnswerMaster Error=" + e.toString());
        }

    }

    public static void SaveCorctQueAnsMapping(Context cnxt, SQLiteDatabase db, String ExamId, String QuestionId,
                                              String QuestionCode, String AnswerId, String AnswerCode) {

        try {

            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("ExamId", ExamId);
            cv.put("QuestionId", QuestionId);
            cv.put("QuestionCode", QuestionCode);
            cv.put("AnswerId", AnswerId);
            cv.put("AnswerCode", AnswerCode);

            Cursor cursor = db.query(cnxt.getResources().getString(R.string.TBL_LPIQuestionCorrectAnswerMapping),
                    null, "ExamId=? and QuestionId=? and AnswerId=?",
                    new String[]{ExamId, QuestionId, AnswerId}, null, null, null);
            if (cursor.getCount() > 0) {
                db.update(cnxt.getResources().getString(R.string.TBL_LPIQuestionCorrectAnswerMapping), cv, "ExamId=? and QuestionId=? and  AnswerId=?",
                        new String[]{ExamId, QuestionId, AnswerId});
            } else {
                db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_LPIQuestionCorrectAnswerMapping), null, cv);
            }
            cursor.close();
//            db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_LPIQuestionCorrectAnswerMapping), null, cv);
        } catch (Exception e) {
            Log.e("", "SaveCorctQueAnsMapping Error=" + e.toString());
        }

    }

    // TODO : To insert records in ExamQueAns_SyncMaster
    public static void SaveExamQueAns_SyncMaster(Context cnxt, SQLiteDatabase db, String ExamId, String QuestionId) {

        try {

            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("ExamId", ExamId);
            cv.put("QuestionId", QuestionId);
            cv.put("isSynced", "Complete");

            Cursor cquery;
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamQueAns_SyncMaster), null, "ExamId=? and QuestionId=?",
                    new String[]{ExamId, QuestionId}, null, null, null);
            if (cquery.getCount() > 0) {
                db.update(cnxt.getResources().getString(R.string.TBL_LPIExamQueAns_SyncMaster), cv, "ExamId=? and QuestionId=?",
                        new String[]{ExamId, QuestionId});
            } else {
                db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_LPIExamQueAns_SyncMaster), null, cv);
            }

            cquery.close();
        } catch (Exception e) {
            Log.e("", "SaveExamQueAns_SyncMaster Error=" + e.toString());
        }

    }

    // TODO: To insert ChangeMaster records
    public static void SaveChangeMaster(Context cnxt, SQLiteDatabase db, String ChkMstSUChangeFlag, String ChkMstSUChangeVer,
                                        String ChkMstSUTableChangeName) {
        Cursor cquery;
        cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIChangeMaster), null, null, null, null, null, null);
        ContentValues cv = new ContentValues();
        cv.clear();
        cv.put("ChkMstSUChangeFlag", ChkMstSUChangeFlag);
        cv.put("ChkMstSUChangeVer", ChkMstSUChangeVer);
        cv.put("ChkMstSUTableChangeName", ChkMstSUTableChangeName);

        try {
            if (cquery.getCount() == 0)
                db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_LPIChangeMaster), null, cv);
            else
                db.update(cnxt.getResources().getString(R.string.TBL_LPIChangeMaster), cv, null, null);
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
        cquery.close();
    }

    // TODO: To insert IsysLookupParam records
    public static void SaveIsysLookupParam(Context cnxt, SQLiteDatabase db, String LookupCode, String ParamValue,
                                           String ParamDesc01) {
        try {
            Cursor cquery;
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_IsysLookupParam), null, "LookupCode=?", new String[]{LookupCode}, null, null, null);
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("LookupCode", LookupCode);
            cv.put("ParamValue", ParamValue);
            cv.put("ParamDesc01", ParamDesc01);

            if (cquery.getCount() > 0) {
                db.update(cnxt.getResources().getString(R.string.TBL_IsysLookupParam), cv, "LookupCode=? and ParamValue=?", new String[]{LookupCode, ParamValue});
            } else {
                db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_IsysLookupParam), null, cv);
            }
            cquery.close();
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }

    }

    public static boolean IsExmQueMendatory(Context cnxt, SQLiteDatabase db, String ExamId) {
        Cursor cursor;
        boolean IsExmQueMendatory = false;
        cursor = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamMST), new String[]{"IsAllQuesMendatory"}, "ExamId=?", new String[]{ExamId}, null, null, null);
        if (cursor.getCount() > 0) {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                if (cursor.getString(0).toString().equals("Y"))
                    IsExmQueMendatory = true;
                else
                    IsExmQueMendatory = false;
            }
        }
        cursor.close();
        return IsExmQueMendatory;
    }

    public static void CurrentStatusInsertData(Context cnxt, SQLiteDatabase db, String appNo) {
        ContentValues cv = new ContentValues();
        Cursor cquery = null;
        try {
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_CreateTblLPI_BasicDetails), null, null, null, null, null, null);

            cv.clear();
            cv.put("AppNo", appNo);


        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }

        try {


            if (cquery.getCount() == 0)
                db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_CreateTblLPI_BasicDetails), null, cv);
            else

                db.update(cnxt.getResources().getString(R.string.TBL_CreateTblLPI_BasicDetails), cv, null, null);

            cquery.close();
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
    }

    public static String UserName(Context cnxt, SQLiteDatabase db, String UserId) {
        Cursor user_cquery;
        String str_cndFrstName = "", str_InscType = "";
        user_cquery = db.query(cnxt.getResources().getString(R.string.TBL_iCandidate), new String[]{"FirstName", "InscType"}, "" +
                "CandidateLoginId=?", new String[]{UserId}, null, null, null);
        for (user_cquery.moveToFirst(); !user_cquery.isAfterLast(); user_cquery.moveToNext()) {
            str_cndFrstName = user_cquery.getString(0);
            str_InscType = user_cquery.getString(1);
        }
        StaticVariables.UserName = str_cndFrstName;
        StaticVariables.crnt_InscType = str_InscType;
        if (!user_cquery.isClosed())
            user_cquery.close();

        return StaticVariables.UserName;
    }

    public static String getUserMobNo(Context cnxt, SQLiteDatabase db, String UserId) {
        Cursor user_cquery;
        String strMobNo = "";
        user_cquery = db.query(cnxt.getResources().getString(R.string.TBL_iCandidate), new String[]{"CandidateMobileNo1"}, "" +
                "CandidateLoginId=?", new String[]{UserId}, null, null, null);
        for (user_cquery.moveToFirst(); !user_cquery.isAfterLast(); user_cquery.moveToNext()) {
            strMobNo = user_cquery.getString(0);
        }
        if (!user_cquery.isClosed())
            user_cquery.close();

        return strMobNo;
    }

    public static Cursor GetQue(Context cnxt, SQLiteDatabase db, BigInteger ExamId, int QueId) {
        Cursor exam_cquery;

        exam_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamQuestionMST),
                new String[]{"ExamId,QuestionId,QuestionCode,Question"},
                "QuestionId=? and ExamId=?",
                new String[]{"" + QueId, "" + ExamId}, null, null, "QuestionId ASC");

        return exam_cquery;
    }

    public static Cursor GetAnsOptions(Context cnxt, SQLiteDatabase db, String ExamId, String QueId) {
        Cursor ansOpt_cquery;
        ansOpt_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamAnswerMST),
                new String[]{"ExamId,QuestionId,AnswerId,AnswerCode,Answer"},
                "QuestionId=? and ExamId=?",
                new String[]{"" + QueId, "" + ExamId}, null, null, "AnswerId");

        return ansOpt_cquery;
    }


//    public static void CheckMasterChange(Context cnxt, SQLiteDatabase db, String MasterChangeVer) {
//        Cursor cquery;
//        cquery = db.rawQuery("select * from SyncMstSU where ChkMstSUChangeVer='" + MasterChangeVer + "'", null);
//        if (cquery.getCount() == 0) {
//            db.execSQL("insert into SyncMstSU (ChkMstSUChangeFlag,ChkMstSUChangeVer,ChkMstSUTableChangeName)  values('Y','" + MasterChangeVer + "' ,'All Master')");
//            insertAllDummyData(cnxt, db);
//        }
//    }

    public static Cursor GetFeedBackAnsOptions(Context cnxt, SQLiteDatabase db) {
        Cursor ansOpt_cquery;
        ansOpt_cquery = db.query(cnxt.getResources().getString(R.string.TBL_IsysLookupParam),
                new String[]{"ParamDesc01"},
                "LookupCode=?",
                new String[]{"TraitAns"}, null, null, "ParamValue");

        return ansOpt_cquery;
    }

    public static String GetRightAns_Exm1(Context cnxt, SQLiteDatabase db, String ExamId, String QuestionId) {
        Cursor RigtAnsId_cquery, RigtAns_cquery;
        String RightAns = "";
        int RightAnsId = 0;
        RigtAnsId_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIQuestionCorrectAnswerMapping),
                new String[]{"AnswerId"},
                "QuestionId=? and ExamId=?",
                new String[]{"" + QuestionId, "" + ExamId}, null, null, null);
        if (RigtAnsId_cquery.getCount() > 0) {
            for (RigtAnsId_cquery.moveToFirst(); !RigtAnsId_cquery.isAfterLast(); RigtAnsId_cquery.moveToNext()) {
                RightAnsId = RigtAnsId_cquery.getInt(0);
            }
        }

        RigtAns_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamAnswerMST), new String[]{"Answer"},
                "AnswerId=? and ExamId=? and QuestionId=?",
                new String[]{"" + RightAnsId, "" + ExamId, "" + QuestionId}, null, null, null);
        if (RigtAns_cquery.getCount() > 0) {
            for (RigtAns_cquery.moveToFirst(); !RigtAns_cquery.isAfterLast(); RigtAns_cquery.moveToNext()) {
                RightAns = RigtAns_cquery.getString(0);
            }
        }
        RigtAnsId_cquery.close();
        RigtAns_cquery.close();
        return RightAns;
    }

    public static String GetAttemptedAns(Context cnxt, SQLiteDatabase db, String ExamId, String QuestionId,
                                         String strLoginId, int strAttemptId) {
        Cursor GetAttemptedAns_cquery, AttemptedAns_cquery;
        String RightAns = "";
        int RightAnsId = 0;
        GetAttemptedAns_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPICandidateAttemptQueAns),
                new String[]{"AnswerId"}, "ExamId=? and QuestionId=? and LoginId=? and AttemptId=?",
                new String[]{"" + ExamId, "" + QuestionId, "" + strLoginId, "" + strAttemptId}, null, null, null);
        if (GetAttemptedAns_cquery.getCount() > 0) {
            for (GetAttemptedAns_cquery.moveToFirst(); !GetAttemptedAns_cquery.isAfterLast(); GetAttemptedAns_cquery.moveToNext()) {
                RightAnsId = GetAttemptedAns_cquery.getInt(0);
            }
        }

        AttemptedAns_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamAnswerMST), new String[]{"Answer"},
                "AnswerId=? and ExamId=? and QuestionId=?", new String[]{"" + RightAnsId, "" + ExamId, "" + QuestionId}, null, null, null);
        if (AttemptedAns_cquery.getCount() > 0) {
            for (AttemptedAns_cquery.moveToFirst(); !AttemptedAns_cquery.isAfterLast(); AttemptedAns_cquery.moveToNext()) {
                RightAns = AttemptedAns_cquery.getString(0);
            }
        }
        GetAttemptedAns_cquery.close();
        AttemptedAns_cquery.close();
        return RightAns;
    }

    public static String GetCndFeedbackAns(Context cnxt, SQLiteDatabase db, String ExamId, String QuestionId,
                                           String strLoginId, int strAttemptId) {
        Cursor GetAttemptedAns_cquery;
        String RightAns = "";
        GetAttemptedAns_cquery = db.query(cnxt.getResources().getString(R.string.TBL_CndFeedback),
                new String[]{"Feedback"}, "ExamId=? and QuestionId=? and CandidateId=?",
                new String[]{"" + ExamId, "" + QuestionId, "" + strLoginId}, null, null, null);
        if (GetAttemptedAns_cquery.getCount() > 0) {
            for (GetAttemptedAns_cquery.moveToFirst(); !GetAttemptedAns_cquery.isAfterLast(); GetAttemptedAns_cquery.moveToNext()) {
                RightAns = GetAttemptedAns_cquery.getString(0);
            }
        }
        GetAttemptedAns_cquery.close();
        return RightAns;
    }

    public static int GetExamAttemptedCount(Context cnxt, SQLiteDatabase db, String CandidateId,
                                            String ExamId) {
        try {
            Cursor cquery;
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_CndFeedback),
                    null, "CandidateId=? and  ExamId=? ",
                    new String[]{"" + CandidateId, "" + ExamId}, null, null, null);
            if (cquery == null) {
                return 0;
            } else {
                return cquery.getCount();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static Cursor GetExamList(Context cnxt, SQLiteDatabase db) {
        Cursor ExmList_cquery;
        ExmList_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamMST),
                new String[]{"ExamId,ExamCode,ExamDetails,ExamType,AllowNavigationFlag,OptionCount,ShowRandomQuestions," +
                        "TotalQuestions,TotalTimeAllowedInMinutes,IsAllowResultFeedback,IsPracticeExam,AllowedAttempt,EffectiveDate,ExamDesc" +
                        ",IsAllQuesMendatory"}, "ExamType<>5 and ExamType<>4", null, null, null, null);
        return ExmList_cquery;
    }

    public static int GetAnsId(Context cnxt, SQLiteDatabase db, String Answer, BigInteger ExamId, int QuestionId) {
        Cursor AnsId_cquery;
        int AnsId = 0;
        AnsId_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamAnswerMST), new String[]{"AnswerId"},
                "ExamId=? and QuestionId=? and Answer=?",
                new String[]{"" + ExamId, "" + QuestionId, Answer}, null, null, null);
        if (AnsId_cquery.getCount() > 0) {
            for (AnsId_cquery.moveToFirst(); !AnsId_cquery.isAfterLast(); AnsId_cquery.moveToNext()) {
                AnsId = AnsId_cquery.getInt(0);
            }
        }
        AnsId_cquery.close();
        return AnsId;
    }

    public static String GetRightAns_Exm2(Context cnxt, SQLiteDatabase db, BigInteger ExamId, int QuestionId, String Answer) {
        Cursor RigtAns_cquery;
        String RightAns = "";
        RigtAns_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIQuestionCorrectAnswerMapping),
                new String[]{"Answer"},
                "QuestionId=? and ExamId=? and AnswerId=?",
                new String[]{"" + QuestionId, "" + ExamId, "" + GetAnsId(cnxt, db, Answer, ExamId, QuestionId)}, null, null, null);
        if (RigtAns_cquery.getCount() > 0) {
            for (RigtAns_cquery.moveToFirst(); !RigtAns_cquery.isAfterLast(); RigtAns_cquery.moveToNext()) {
                RightAns = RigtAns_cquery.getString(0);
            }
        }
        RigtAns_cquery.close();
        return RightAns;
    }

    public static Cursor GetAllAttemptedAnsDetails(Context cnxt, SQLiteDatabase db, int ExamId,
                                                   String strLoginId) {
        Cursor GetAttemptedAns_cquery;
        GetAttemptedAns_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPICandidateAttemptQueAns),
                null, "ExamId=? and LoginId=?",
                new String[]{"" + ExamId, "" + strLoginId}, null, null, null);
        return GetAttemptedAns_cquery;
    }

    public static List<Integer> GetQuestionId(Context cnxt, SQLiteDatabase db, String ExamId) {
        List<Integer> fetchQueId = new ArrayList<Integer>();
        try {
            Cursor cquery;
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamQuestionMST), new String[]{"QuestionId"},
                    "ExamId=? and LoginId=?", new String[]{ExamId, StaticVariables.UserLoginId}, null, null, "QuestionId", "1");
            if (cquery.getCount() > 0) {
                fetchQueId.clear();
                for (cquery.moveToFirst(); !cquery.isAfterLast(); cquery.moveToNext()) {
                    fetchQueId.add(cquery.getInt(0));
                }
            }
            cquery.close();
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }

        return fetchQueId;
    }

    public static int GetTopQuestionId(Context cnxt, SQLiteDatabase db, String ExamId) {
        int fetchQueId = 1;
        try {
            Cursor cquery;
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamQuestionMST), new String[]{"QuestionId"},
                    "ExamId=? and LoginId=?", new String[]{ExamId, StaticVariables.UserLoginId}, null, null, "QuestionId", "1");
            if (cquery.getCount() > 0) {
                for (cquery.moveToFirst(); !cquery.isAfterLast(); cquery.moveToNext()) {
                    fetchQueId = cquery.getInt(0);
                }
            }
            cquery.close();
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
            fetchQueId = 1;
        }

        return fetchQueId;
    }

    public static int GetLastQuestionId(Context cnxt, SQLiteDatabase db, String ExamId) {
        int fetchQueId = 0;
        try {
            Cursor cquery;
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamQuestionMST), new String[]{"max(QuestionId)"},
                    "ExamId=? and LoginId=?", new String[]{ExamId, StaticVariables.UserLoginId}, null, null, null);
            if (cquery.getCount() > 0) {
                for (cquery.moveToFirst(); !cquery.isAfterLast(); cquery.moveToNext()) {
                    fetchQueId = cquery.getInt(0);
                }
            }
            cquery.close();
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }

        return fetchQueId;
    }

    public static int GetNextQuestionId(Context cnxt, SQLiteDatabase db, String ExamId, String QuesId) {
        int fetchQueId = 0;
        try {
            Cursor cquery;
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamQuestionMST), new String[]{"QuestionId"},
                    "ExamId=? and LoginId=? and Cast(QuestionId as Integer)>?",
                    new String[]{ExamId, StaticVariables.UserLoginId, QuesId}, null, null, "QuestionId", "1");
            if (cquery.getCount() > 0) {
                for (cquery.moveToFirst(); !cquery.isAfterLast(); cquery.moveToNext()) {
                    fetchQueId = cquery.getInt(0);
                }
            }
            cquery.close();
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }

        return fetchQueId;
    }

    public static int GetPrevQuestionId(Context cnxt, SQLiteDatabase db, String ExamId, String QuesId) {
        int fetchQueId = 0;
        try {
            Cursor cquery;
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamQuestionMST), new String[]{"MAX(QuestionId)"},
                    "ExamId=? and LoginId=? and Cast(QuestionId as Integer)<?", new String[]{ExamId, StaticVariables.UserLoginId, QuesId}, null, null, "QuestionId desc");
            if (cquery.getCount() > 0) {
                for (cquery.moveToFirst(); !cquery.isAfterLast(); cquery.moveToNext()) {
                    fetchQueId = cquery.getInt(0);
                }
            }
            cquery.close();
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }

        return fetchQueId;
    }

    public static boolean isAllowNav(Context cnxt, SQLiteDatabase db, BigInteger ExamId) {
        boolean AllowNav = false;
        Cursor AllowNav_cquery;
        AllowNav_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamMST),
                new String[]{"AllowNavigationFlag"},
                "ExamId=?",
                new String[]{"" + ExamId}, null, null, null);
        for (AllowNav_cquery.moveToFirst(); !AllowNav_cquery.isAfterLast(); AllowNav_cquery.moveToNext()) {
            if (AllowNav_cquery.getString(0).equals("Y")) {
                AllowNav = true;
            } else if (AllowNav_cquery.getString(0).equals("N")) {
                AllowNav = false;
            }
        }
        AllowNav_cquery.close();
        return AllowNav;
    }

    public static int TotalOptionCount(Context cnxt, SQLiteDatabase db, BigInteger ExamId) {
        int OptionCount = 4;
        Cursor OptCnt_cquery;
        OptCnt_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamMST),
                new String[]{"OptionCount"},
                "ExamId=?",
                new String[]{"" + ExamId}, null, null, null);
        if (OptCnt_cquery.getCount() > 0) {
            for (OptCnt_cquery.moveToFirst(); !OptCnt_cquery.isAfterLast(); OptCnt_cquery.moveToNext()) {
                OptionCount = OptCnt_cquery.getInt(0);
            }
        }
        OptCnt_cquery.close();
        return OptionCount;
    }

    public static int TotalQueCount(Context cnxt, SQLiteDatabase db, BigInteger ExamId) {
        Cursor QueCount_cquery;
        QueCount_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamQuestionMST), null, "ExamId=?", new String[]{"" + ExamId}, null, null, null);
        int QueCount = QueCount_cquery.getCount();
        QueCount_cquery.close();
        return QueCount;
    }

    public static int AllExamQueCount(Context cnxt, SQLiteDatabase db) {
        Cursor QueCount_cquery;
        QueCount_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamQuestionMST), new String[]{"count(*) as qCount,ExamId"}, null, null, "ExamId", null, null);
        int QueCount = 0;
        if (QueCount_cquery.getCount() > 0) {
            for (QueCount_cquery.moveToFirst(); !QueCount_cquery.isAfterLast(); QueCount_cquery.moveToNext()) {
                QueCount = QueCount + QueCount_cquery.getInt(0);
            }
        }
        QueCount_cquery.close();
        return QueCount;
    }

    public static boolean CheckExamQueCount(Context cnxt, SQLiteDatabase db, BigInteger ExmId) {
        Cursor QueCount_cquery, MstCountCursor;
        boolean isSuccess = false;
        MstCountCursor = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamMST),
                new String[]{"ExamId", "TotalQuestions"}, "ExamId=?", new String[]{"" + ExmId}, null, null, null);

        QueCount_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamQuestionMST),
                new String[]{"count(*) as qCount,ExamId"}, "ExamId=?", new String[]{"" + ExmId}, "ExamId", null, null);
        int MSTQueCount = 0;
        int QueCount = 0;
        if (MstCountCursor.getCount() > 0) {
            for (MstCountCursor.moveToFirst(); !MstCountCursor.isAfterLast(); MstCountCursor.moveToNext()) {
                MSTQueCount = MstCountCursor.getInt(1);
            }
        }
        if (QueCount_cquery.getCount() > 0) {
            for (QueCount_cquery.moveToFirst(); !QueCount_cquery.isAfterLast(); QueCount_cquery.moveToNext()) {
                QueCount = QueCount_cquery.getInt(0);
            }
        }
        QueCount_cquery.close();
        MstCountCursor.close();
        if (MSTQueCount == QueCount) {
            return true;
        } else {
            return false;
        }
    }

    public static int PPExamQueCount(Context cnxt, SQLiteDatabase db, String ExamId) {
        Cursor QueCount_cquery;
        QueCount_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamQuestionMST), new String[]{"count(*) as qCount,ExamId"}, "ExamId=?", new String[]{ExamId}, "ExamId", null, null);
        int QueCount = 0;
        if (QueCount_cquery != null && QueCount_cquery.getCount() > 0) {
            for (QueCount_cquery.moveToFirst(); !QueCount_cquery.isAfterLast(); QueCount_cquery.moveToNext()) {
                QueCount = QueCount + QueCount_cquery.getInt(0);
            }
        }
        QueCount_cquery.close();
        return QueCount;
    }

    public static int AllowedAttempt(Context cnxt, SQLiteDatabase db, String ExamId, String CandidateId) {
        Cursor cquery;
        int AllowedAttempt = 0, QueCount = 0;
        try {
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamMST),
                    new String[]{"ExamId", "AllowedAttempt"}, "ExamId=?", new String[]{ExamId},
                    "", null, null);

            if (cquery.getCount() > 0) {
                for (cquery.moveToFirst(); !cquery.isAfterLast(); cquery.moveToNext()) {
                    AllowedAttempt = AllowedAttempt + cquery.getInt(1);
                }
            }
            cquery.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return AllowedAttempt;
    }

    public static int CheckAttempts(Context cnxt, SQLiteDatabase db, String ExamId, String CandidateId) {
        Cursor cquery;
        int QueCount = 0;
        try {
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPICandidateAttemptedExam),
                    new String[]{"Attempt"}, "ExamId=? and CandidateId=?",
                    new String[]{ExamId, CandidateId}, null, null, null);
            if (cquery != null && cquery.getCount() > 0) {

                for (cquery.moveToFirst(); !cquery.isAfterLast(); cquery.moveToNext()) {
                    QueCount = Integer.parseInt(cquery.getString(0));
                }
            }
            cquery.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return QueCount;
    }

    public static String GetExamName(Context cnxt, SQLiteDatabase db, int ExamId) {
        Cursor ExamName_cquery;
        String str_ExamName = "";
        ExamName_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamMST), new String[]{"ExamTypeName"},
                "ExamId=?", new String[]{"" + ExamId}, null, null, null);
        if (ExamName_cquery.getCount() > 0) {
            for (ExamName_cquery.moveToFirst(); !ExamName_cquery.isAfterLast(); ExamName_cquery.moveToNext()) {
                str_ExamName = ExamName_cquery.getString(0);
            }
        }
        ExamName_cquery.close();
        return str_ExamName;
    }

    public static String GetExamType(Context cnxt, SQLiteDatabase db, int ExamId) {
        Cursor ExamName_cquery;
        String str_ExamName = "";
        ExamName_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamMST), new String[]{"ExamType"},
                "ExamId=?", new String[]{"" + ExamId}, null, null, null);
        if (ExamName_cquery.getCount() > 0) {
            for (ExamName_cquery.moveToFirst(); !ExamName_cquery.isAfterLast(); ExamName_cquery.moveToNext()) {
                str_ExamName = ExamName_cquery.getString(0);
            }
        }
        ExamName_cquery.close();
        return str_ExamName;
    }

    public static Cursor GetExamCursor(Context cnxt, SQLiteDatabase db, int ExamType) {
        Cursor ExamCursor = null;
        try {
            ExamCursor = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamMST), null,
                    "ExamType=?", new String[]{"" + ExamType}, null, null, null);
            if (ExamCursor.getCount() > 0) {
                return ExamCursor;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ExamCursor;
        }
        return ExamCursor;
    }

//    public static int GetExamType(Context cnxt,SQLiteDatabase db,int ExamId){
//        Cursor ExamType_cquery;
//        int ExamType=0;
//        ExamType_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamMST),new String[]{"ExamType"},
//                "ExamId=?",new String[]{""+ExamId},null,null,null);
//        for (ExamType_cquery.moveToFirst();!ExamType_cquery.isAfterLast();ExamType_cquery.moveToNext()){
//            ExamType=ExamType_cquery.getInt(0);
//        }
//        return ExamType;
//    }

    // TODO: to check whether all Question and answer masters are synced or not
    public static int DidExamMastersSynced(Context cnxt, SQLiteDatabase db, String ExamId) {
        int counter = 0;
        try {
            Cursor cquery;
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamQueAns_SyncMaster), null, "ExamId=?",
                    new String[]{ExamId}, null, null, null, null);
            counter = cquery.getColumnCount();
            cquery.close();
        } catch (Exception e) {
            Log.e("", "TBL_LPIExamQueAns_SyncMaster counter Error=" + e.toString());
        }
        return counter;
    }

    // TODO: To Check Exam master record is available or not to avoid syncing again and again
    public static boolean IsExamMasterAvailable(Context cnxt, SQLiteDatabase db) {
        boolean isAvailable = false;
        try {
            Cursor cquery;
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamMST), null, null, null, null, null, null, null);
            if (cquery.getCount() > 0) {
                isAvailable = true;
            } else
                isAvailable = false;

            cquery.close();
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }

        return isAvailable;
    }

    // TODO: To Check Exam master record is available or not to avoid syncing again and again
    public static boolean IsExamTypeAvailable(Context cnxt, SQLiteDatabase db, String ExamType) {
        boolean isAvailable = false;
        try {
            Cursor cquery;
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamMST), null, "ExamType=?", new String[]{ExamType}, null, null, null, null);
            if (cquery.getCount() > 0) {
                isAvailable = true;
            } else
                isAvailable = false;

            cquery.close();
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }

        return isAvailable;
    }

    // TODO: To check any change in App level masters
    public static boolean NeedToFetchMaster(Context cnxt, SQLiteDatabase db, String ChkMstSUChangeVer) {
        boolean fetchMaster = false;
        try {
            Cursor cquery, cqueryMst;
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIChangeMaster), null, null, null, null, null, null);
            if (cquery.getCount() > 0) {

                cqueryMst = db.query(cnxt.getResources().getString(R.string.TBL_LPIChangeMaster), null,
                        "ChkMstSUChangeFlag=? and ChkMstSUChangeVer=?", new String[]{"Y", ChkMstSUChangeVer}, null, null, null);
                if (cqueryMst.getCount() > 0)
                    fetchMaster = false;
                else
                    fetchMaster = true;

                cqueryMst.close();
            } else
                fetchMaster = true;

            cquery.close();

        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }

        return fetchMaster;
    }

//    public static int GetExamType(Context cnxt,SQLiteDatabase db,int ExamId){
//        Cursor ExamType_cquery;
//        int ExamType=0;
//        ExamType_cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIExamMST),new String[]{"ExamType"},
//                "ExamId=?",new String[]{""+ExamId},null,null,null);
//        for (ExamType_cquery.moveToFirst();!ExamType_cquery.isAfterLast();ExamType_cquery.moveToNext()){
//            ExamType=ExamType_cquery.getInt(0);
//        }
//        return ExamType;
//    }

    public static int ChkMstSUChangeVer(Context cnxt, SQLiteDatabase db) {
        int fetchMaster = 0;
        try {
            Cursor cquery;
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_LPIChangeMaster),
                    new String[]{"ChkMstSUChangeVer"}, null, null, null, null, null);
            if (cquery.getCount() > 0) {
                for (cquery.moveToFirst(); !cquery.isAfterLast(); cquery.moveToNext()) {
                    fetchMaster = cquery.getInt(0);
                }
            } else
                fetchMaster = 0;

            cquery.close();

        } catch (Exception e) {
            Log.e("", "ChkMstSUChangeVer Error=" + e.toString());
        }

        return fetchMaster;
    }

    public static void LpiinsertData(Context cnxt, SQLiteDatabase db, String appNo, String givenName,
                                     String cndURN, String cndStatus, String SAPCode, String managerName,
                                     String branch, String hierarchy, String subClass, String lcnExpDate,
                                     String lcnNo, String lcnIssDate, String trnMode, String trnLocDesc,
                                     String trnInstitute, String accrdNo, String hrsTrn, String trnStartDate,
                                     String trnEndDate, String exmMode, String examLanguage, String exmBody,
                                     String exmCentre, String feesTokenNo, String feesExpected, String feesCollected,
                                     String paymentMode, String feesCollectedDate, String feesTransactionNumber) {

        try {
            Cursor cquery = null;
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("AppNo", appNo);
            cv.put("GivenName", givenName);
            cv.put("CndURN", cndURN);
            cv.put("cndStatus", cndStatus);
            cv.put("SAPCode", SAPCode);
            cv.put("ManagerName", managerName);
            cv.put("Branch", branch);
            cv.put("Hierarchy", hierarchy);
            cv.put("SubClass", subClass);
            cv.put("LcnExpDate", lcnExpDate);
            cv.put("LcnNo", lcnNo);
            cv.put("LcnIssDate", lcnIssDate);
            cv.put("TrnMode", trnMode);
            cv.put("TrnLocDesc", trnLocDesc);
            cv.put("TrnInstitute", trnInstitute);
            cv.put("AccrdNo", accrdNo);
            cv.put("HrsTrn", hrsTrn);
            cv.put("TrnStartDate", trnStartDate);
            cv.put("TrnEndDate", trnEndDate);
            cv.put("ExmMode", exmMode);
            cv.put("ExamLanguage", examLanguage);
            cv.put("ExmBody", exmBody);
            cv.put("ExmCentre", exmCentre);
            cv.put("FeesTokenNo", feesTokenNo);
            cv.put("FeesExpected", feesExpected);
            cv.put("FeesCollected", feesCollected);
            cv.put("PaymentMode", paymentMode);
            cv.put("FeesCollectedDate", feesCollectedDate);
            cv.put("FeesTransactionNumber", feesTransactionNumber);


            cquery = db.query(cnxt.getResources().getString(R.string.TBL_CreateTblLPI_BasicDetails), null, "AppNo=?", new String[]{appNo}, null, null, null);
            if (cquery.getCount() > 0) {
                db.update(cnxt.getResources().getString(R.string.TBL_CreateTblLPI_BasicDetails), cv,
                        "AppNo=?", new String[]{appNo});
            } else {
                db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_CreateTblLPI_BasicDetails), null, cv);
            }
            cquery.close();
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
    }

    public static void LpiinsertViewSummaryData(Context cnxt, SQLiteDatabase db, String appNo, String description, String createddt) {
        Cursor cquery = null;
        ContentValues cv = new ContentValues();
        try {
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_TblViewSummary), null, "AppNo=?",
                    new String[]{appNo}, null, null, null);

            cv.clear();
            cv.put("AppNo", appNo);
            cv.put("Description", description);
            cv.put("CreatedDt", createddt);

            if (cquery.getCount() == 0)
                db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_TblViewSummary), null, cv);
            else
                db.update(cnxt.getResources().getString(R.string.TBL_TblViewSummary), cv, "AppNo=?", new String[]{appNo});
            cquery.close();
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
    }

    public static boolean IsPostLicDataAvailable(Context cnxt, SQLiteDatabase db, String section) {
        boolean isAvailable = false;
        try {
            Cursor cquery;
            cquery = db.query(cnxt.getResources().getString(R.string.TblPostLicModuleData), null, "SectionId=?", new String[]{section}, null, null, null);
            if (cquery.getCount() > 0) {
                isAvailable = true;
            } else
                isAvailable = false;

            cquery.close();
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
        return isAvailable;
    }

    public static boolean IsActivityMasterAvailable(Context cnxt, SQLiteDatabase db, String code) {
        boolean isAvailable = false;
        try {
            Cursor cquery;
            cquery = db.query(cnxt.getResources().getString(R.string.TblActivityMaster), null,
                    "ActivityCode=?", new String[]{code}, null, null, null);
            if (cquery.getCount() > 0) {
                isAvailable = true;
            } else
                isAvailable = false;

            cquery.close();
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
        return isAvailable;
    }

    public static void PostLicModuleData(Context cnxt, SQLiteDatabase db, String SectionId, String ParentSectionId,
                                         String RootSectionId, String Desc01, String Desc02, String Desc03, String Desc04, String Desc05,
                                         String GenDesc01, String GenDesc02, String GenDesc03, String GenDesc04, String GenDesc05,
                                         String AdvDesc01, String AdvDesc02, String AdvDesc03, String AdvDesc04, String AdvDesc05,
                                         String ImageDesc01, String ImageDesc02, String ImageDesc03, String ImageDesc04, String ImageDesc05,
                                         String hasChild, String hasData, String IsActive, String IsLink, String Link,
                                         String CreatedBy, String CreatedDTime, String UpdatedBy, String UpdatedDTime) {
        try {
            ContentValues cv = new ContentValues();
            cv.clear();

            cv.put("SectionId", SectionId);
            cv.put("ParentSectionId", ParentSectionId);
            cv.put("RootSectionId", RootSectionId);
            cv.put("Desc01", Desc01);
            cv.put("Desc02", Desc02);
            cv.put("Desc03", Desc03);
            cv.put("Desc04", Desc04);
            cv.put("Desc05", Desc05);
            cv.put("GenDesc01", GenDesc01);
            cv.put("GenDesc02", GenDesc02);
            cv.put("GenDesc03", GenDesc03);
            cv.put("GenDesc04", GenDesc04);
            cv.put("GenDesc05", GenDesc05);
            cv.put("AdvDesc01", AdvDesc01);
            cv.put("AdvDesc02", AdvDesc02);
            cv.put("AdvDesc03", AdvDesc03);
            cv.put("AdvDesc04", AdvDesc04);
            cv.put("AdvDesc05", AdvDesc05);
            cv.put("ImageDesc01", ImageDesc01);
            cv.put("ImageDesc02", ImageDesc02);
            cv.put("ImageDesc03", ImageDesc03);
            cv.put("ImageDesc04", ImageDesc04);
            cv.put("ImageDesc05", ImageDesc05);
            cv.put("hasChild", hasChild);
            cv.put("hasData", hasData);
            cv.put("IsActive", IsActive);
            cv.put("IsVideoAvailable", IsLink);
            cv.put("videoLink", Link);
            cv.put("CreatedBy", CreatedBy);
            cv.put("CreatedDTime", CreatedDTime);
            cv.put("UpdatedBy", UpdatedBy);
            cv.put("UpdatedDTime", UpdatedDTime);

            if (IsPostLicDataAvailable(cnxt, db, SectionId)) {
                db.update(cnxt.getResources().getString(R.string.TblPostLicModuleData), cv,
                        "SectionId=?", new String[]{SectionId});
            } else {
                db.insertOrThrow(cnxt.getResources().getString(R.string.TblPostLicModuleData), null, cv);
            }


        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }

    }

    public static void InsertActivityMaster(Context cnxt, SQLiteDatabase db, String code, String value) {
        try {
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("ActivityCode", code);
            cv.put("ActivityDesc", value);
            if (IsActivityMasterAvailable(cnxt, db, code)) {
                db.update(cnxt.getResources().getString(R.string.TblActivityMaster), cv, "ActivityCode=?", new String[]{code});
            } else {
                db.insertOrThrow(cnxt.getResources().getString(R.string.TblActivityMaster), null, cv);
            }

        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
    }

    public static String getActivityMaster(Context context, SQLiteDatabase db, String value) {
        String code = "";
        Cursor cursor = db.query(context.getResources().getString(R.string.TblActivityMaster),
                new String[]{"ActivityCode", "ActivityDesc"}, "ActivityDesc = ?",
                new String[]{value}, null, null, null);
        if (cursor.getCount() > 0) {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                code = cursor.getString(0);
            }
        }
        cursor.close();
        return code;
    }

    public static void InsertActivityTracker(Context cnxt, SQLiteDatabase db, String UserId, String ActivityCode,
                                             String ParentActivity, String ActivityDesc, String LobId,
                                             String ProdId, String SectionId, String fromTime, String toTime,
                                             String Remark, String CreatedBy, String CreatedDate,
                                             String UpdatedBy, String UpdatedDate, String SyncStatus) {
        try {

            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("UserId", UserId);
            cv.put("ActivityCode", ActivityCode);
            cv.put("ParentActivity", ParentActivity);
            cv.put("ActivityDesc", ActivityDesc);
            cv.put("LobId", LobId);
            cv.put("ProdId", ProdId);
            cv.put("SectionId", SectionId);
            cv.put("fromTime", fromTime);
            if (toTime.equalsIgnoreCase("")) {
                cv.put("toTime", StaticVariables.sdf.format(Calendar.getInstance().getTime()));
            } else {
                cv.put("toTime", toTime);
            }

            cv.put("Remark", Remark);
            cv.put("DeviceId", Settings.Secure.getString(cnxt.getContentResolver(), Settings.Secure.ANDROID_ID));
            cv.put("DeviceName", StaticVariables.deviceInfo);
            cv.put("CreatedBy", CreatedBy);
            cv.put("CreatedDate", StaticVariables.sdf.format(Calendar.getInstance().getTime()));
            cv.put("UpdatedBy", UpdatedBy);
            cv.put("UpdatedDate", UpdatedDate);
            cv.put("SyncStatus", SyncStatus);

            db.insertOrThrow(cnxt.getResources().getString(R.string.TblActivityTracker), null, cv);

        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
    }

    public static void InsertZipContent(Context cnxt, SQLiteDatabase db, String BatchId,
                                        String FileName, String Path, String Synced, String UserID,
                                        String createdDt, String updatedDt) {
        try {
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("BatchId", BatchId);
            cv.put("FileName", FileName);
            cv.put("Path", Path);
            cv.put("Synced", Synced);
            cv.put("CreatedBy", UserID);
            cv.put("CreatedDate", createdDt);
            cv.put("UpdatedBy", UserID);
            cv.put("UpdatedDate", updatedDt);

            if (IsZipContentAvailable(cnxt, db, BatchId)) {
                db.update(cnxt.getResources().getString(R.string.TBL_ZipContent), cv, "BatchId=?",
                        new String[]{BatchId});
            } else {
                db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_ZipContent), null, cv);
            }
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
    }

    public static void UpdateZipContent(Context cnxt, SQLiteDatabase db, String BatchId, String Synced, String updtDt) {
        try {
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("Synced", Synced);
            cv.put("UpdatedDate", StaticVariables.sdf.format(Calendar.getInstance().getTime()));
            db.update(cnxt.getResources().getString(R.string.TBL_ZipContent), cv, "BatchId=?", new String[]{BatchId});
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
    }

    public static void UpdateActivityDetails(Context cnxt, SQLiteDatabase db, String RecId, String Status) {
        try {
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("SyncStatus", Status);
            db.update(cnxt.getResources().getString(R.string.TblActivityTracker), cv, "RecId=?", new String[]{RecId});
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
    }

    public static boolean IsZipContentAvailable(Context cnxt, SQLiteDatabase db, String code) {
        boolean isAvailable = false;
        try {
            Cursor cquery;
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_ZipContent), null,
                    "BatchId=?", new String[]{code}, null, null, null);
            if (cquery.getCount() > 0) {
                isAvailable = true;
            } else
                isAvailable = false;

            cquery.close();
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
        return isAvailable;
    }

    public static Cursor getViewSummary(Context cnxt, SQLiteDatabase db, String appID) {
        Cursor mCursor;
        mCursor = db.query(cnxt.getResources().getString(R.string.TBL_TblViewSummary), null, "AppNo=?",
                new String[]{"" + appID}, null, null, null, null);
        return mCursor;

    }

    public static void SaveContentQueries(Context context, SQLiteDatabase db, String UserId, String SectionId,
                                          String QueryType, String QueryCat, String QueryDesc,
                                          String isSkiped, String isLiked, String isDisliked, String MobRefId) {

        try {
            ContentValues values = new ContentValues();
            values.put("UserId", UserId);
            values.put("SectionId", SectionId);
            values.put("QueryType", QueryType);
            values.put("QueryCat", QueryCat);
            values.put("QueryDesc", QueryDesc);
            values.put("isSkiped", isSkiped);
            values.put("isLiked", isLiked);
            values.put("isDisliked", isDisliked);
            values.put("QueryDtime", "" + StaticVariables.sdf.format(Calendar.getInstance().getTime()));
            values.put("SyncStatus", "Pending");
            if (MobRefId.equals("")) {
                values.put("MobRefId", UUID.randomUUID().toString());
            } else {
                values.put("MobRefId", MobRefId);
            }


            db.insertOrThrow(context.getResources().getString(R.string.TBL_ContentQueries), null, values);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Cursor getContentQueries(Context context, SQLiteDatabase db, String UserId) {
        Cursor mCursor;
        mCursor = db.query(context.getResources().getString(R.string.TBL_ContentQueries),
                new String[]{"RecId", "UserId", "SectionId", "QueryType", "QueryCat", "QueryDesc",
                        "isSkiped", "isLiked", "isDisliked", "QueryDtime", "MobRefId"},
                "UserId=? and SyncStatus=?",
                new String[]{"" + UserId, "Pending"}, null, null, null, "1");
        return mCursor;
    }

    public static void UpdtContentQueries(Context context, SQLiteDatabase db, String RecId, String UserId, String Status) {

        try {
            ContentValues values = new ContentValues();
            values.put("SyncStatus", Status);
            Cursor cursor = db.query(context.getResources().getString(R.string.TBL_ContentQueries),
                    null, "MobRefId=? and UserId=?", new String[]{RecId, UserId},
                    null, null, null);
            if (cursor.getCount() > 0) {
                db.update(context.getResources().getString(R.string.TBL_ContentQueries), values,
                        "MobRefId=? and UserId=?", new String[]{RecId, UserId});
            }
            cursor.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    public static int RegisterCandidate(Context cnxt, SQLiteDatabase db, String CandidateId, String Name, String LName,
                                        String Gender, String DOB, String Email, String Mobile, String Password, String Profession,
                                        String Nationality, byte[] img_byteArray, String TrainerID,
                                        String createdDt, String updatedDt, String MobRefId,
                                        String strMobileCountryCode, String strAppLink, boolean modificationFlag, String status,
                                        String RefCode) {
        int isSuccess = 0;
        try {

            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("CandidateId", CandidateId);
            cv.put("FirstName", Name);
            cv.put("LastName", LName);
            cv.put("Gender", Gender);
            cv.put("DOB", DOB);
            cv.put("EmailID", Email);
            cv.put("MobileNo", Mobile);
            cv.put("Profession", Profession);
            cv.put("Nationality", Nationality);
            cv.put("ImageByte", img_byteArray);
            if (!modificationFlag) {
                cv.put("Password", Password);
                cv.put("MobRefId", MobRefId);
                cv.put("CreatedBy", TrainerID);
                cv.put("CreatedDtime", createdDt);
                cv.put("RefCode", RefCode);
            }
            cv.put("UpdatedBy", TrainerID);
            cv.put("UpdatedDtime", updatedDt);
            cv.put("MobileCountryCode", strMobileCountryCode);
            cv.put("AppLink", strAppLink);
            cv.put("SyncStatus", status);

            Cursor mCursor;
            if (!modificationFlag) {
                mCursor = db.query(cnxt.getResources().getString(R.string.TBL_LPICandidateRegistration), null,
                        "MobRefId=?", new String[]{MobRefId}, null, null, null);
            } else {
                mCursor = db.query(cnxt.getResources().getString(R.string.TBL_LPICandidateRegistration), null,
                        "CandidateId=?", new String[]{CandidateId}, null, null, null);
            }
            if (mCursor.getCount() <= 0) {
                isSuccess = (int) db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_LPICandidateRegistration), null, cv);
            } else if (mCursor.getCount() > 0 && modificationFlag) {
                isSuccess = db.update(cnxt.getResources().getString(R.string.TBL_LPICandidateRegistration), cv,
                        "CandidateId=?", new String[]{CandidateId});
            } else if (mCursor.getCount() > 0 && !modificationFlag) {
                isSuccess = db.update(cnxt.getResources().getString(R.string.TBL_LPICandidateRegistration), cv,
                        "MobRefId=?", new String[]{MobRefId});
            } else {
                isSuccess = (int) db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_LPICandidateRegistration), null, cv);
            }
            mCursor.close();
        } catch (Exception e) {
            Log.e("", "RegisterCandidate Error=" + e.toString());
        }
        return isSuccess;
    }

    public static Cursor getCandidateInfo(Context context, SQLiteDatabase db, String CandidateId) {
        Cursor mCursor;
        mCursor = db.query(context.getResources().getString(R.string.TBL_LPICandidateRegistration),
                null, "CandidateId=? ",
                new String[]{"" + CandidateId}, null, null, null, "1");
        return mCursor;
    }

    public static Cursor getOfflineCandidateInfo(Context context, SQLiteDatabase db) {
        Cursor mCursor;
        mCursor = db.query(context.getResources().getString(R.string.TBL_LPICandidateRegistration),
                null, "SyncStatus=? ",
                new String[]{"pending"}, null, null, null, null);
        return mCursor;
    }

    public static Cursor getOfflineCandidateInfo(Context context, SQLiteDatabase db, String CandidateId) {
        Cursor mCursor;
        mCursor = db.query(context.getResources().getString(R.string.TBL_LPICandidateRegistration),
                null, "CandidateId=? ",
                new String[]{"" + CandidateId}, null, null, null, "1");
        return mCursor;
    }

    public static Cursor getAllPendingCandidates(Context context, SQLiteDatabase db) {
        Cursor mCursor;
        mCursor = db.query(context.getResources().getString(R.string.TBL_LPICandidateRegistration),
                null, "SyncStatus=?",
                new String[]{"pending"}, null, null, null, null);
        return mCursor;
    }

    public static int SaveConversation(Context cnxt, SQLiteDatabase db, String strIsClosed, String message, String userLoginId,
                                       String strCandidateId, String question, String strFrom, String curDate, String strQuesID,
                                       String MobRefId, String MsgType, String docLocation, String SyncStatus, String RespStatus,
                                       String isShow, String isAttachment, String mFile, String mFileName,
                                       String mFilePath, String mFileType) {
        int isSuccess = 0;
        try {
            Cursor mCursor;
            if (MobRefId == null || MobRefId.trim().toString().equals("null")) {
                MobRefId = "";
            }
            mCursor = DatabaseHelper.db.query(cnxt.getResources().getString(R.string.TBL_FAQ_Conversation), null,
                    "QuesId=? and TrainerId=? and CandidateId=? and MobRefId=?", new String[]{strQuesID,
                            userLoginId, strCandidateId, MobRefId}, null, null, null);
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("QuesId", strQuesID);
            cv.put("QuesDesc", question);
            cv.put("TrainerId", userLoginId);
            cv.put("CandidateId", strCandidateId);
            cv.put("RespDesc", message);
            cv.put("RespDtime", curDate);
            cv.put("MsgFrom", strFrom);
            cv.put("MsgType", MsgType);
            cv.put("DocLocation", docLocation);
            cv.put("isClosed", strIsClosed);
            cv.put("CreatedBy", userLoginId);
            cv.put("CreatedDtime", curDate);
            cv.put("MobRefId", MobRefId);
            cv.put("SyncStatus", SyncStatus);
            cv.put("RespStatus", RespStatus);
            cv.put("isAttachment", isAttachment);
            cv.put("mFile", mFile);
            cv.put("mFileName", mFileName);
            cv.put("mFilePath", mFilePath);
            cv.put("mFileType", mFileType);
            if (isShow.equals(""))
                cv.put("isShow", "Y");
            else
                cv.put("isShow", isShow);
            if (mCursor.getCount() <= 0) {
                isSuccess = (int) DatabaseHelper.db.insert(cnxt.getResources().getString(R.string.TBL_FAQ_Conversation), null, cv);
            } else if (mCursor.getCount() > 0) {
                cv.remove("isShow");
                isSuccess = db.update(cnxt.getResources().getString(R.string.TBL_FAQ_Conversation), cv,
                        "MobRefId=?", new String[]{MobRefId.trim()});
            }
            mCursor.close();
        } catch (Exception e) {
            Log.e("", "SaveConversation Error=" + e.toString());
        }

        return isSuccess;
    }

    public static boolean isMessageAvailable(Context cnxt, SQLiteDatabase db, String MobRefId) {
        boolean status = false;
        Cursor cursor = db.query(cnxt.getResources().getString(R.string.TBL_FAQ_Conversation), null,
                "MobRefId=?", new String[]{MobRefId}, null, null, null);
        if (cursor.getCount() > 0) {
            status = true;
        } else {
            status = false;
        }
        cursor.close();
        return status;
    }

    public static boolean isMessagesAvailable(Context cnxt) {
        boolean status = true;
        Cursor cursor = db.query(cnxt.getResources().getString(R.string.TBL_FAQ_Conversation), null,
                null, null, null, null, null);
        if (cursor.getCount() > 0) {
            status = false;
        } else {
            status = true;
        }
        cursor.close();
        return status;
    }

    public static int SaveConversation(Context cnxt, SQLiteDatabase db, String strIsClosed, String message, String userLoginId,
                                       String strCandidateId, String question, String strFrom, String curDate, String strQuesID,
                                       String MobRefId, String MsgType, String docLocation, String SyncStatus, String RespStatus,
                                       String isShow) {
        int isSuccess = 0;
        try {
            Cursor mCursor;
            if (MobRefId == null || MobRefId.trim().toString().equals("null")) {
                MobRefId = "";
            }
            mCursor = DatabaseHelper.db.query(cnxt.getResources().getString(R.string.TBL_FAQ_Conversation), null,
                    "QuesId=? and TrainerId=? and CandidateId=? and MobRefId=?", new String[]{strQuesID,
                            userLoginId, strCandidateId, MobRefId}, null, null, null);
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("QuesId", strQuesID);
            cv.put("QuesDesc", question);
            cv.put("TrainerId", userLoginId);
            cv.put("CandidateId", strCandidateId);
            cv.put("RespDesc", message);
            cv.put("RespDtime", curDate);
            cv.put("MsgFrom", strFrom);
            cv.put("MsgType", MsgType);
            cv.put("DocLocation", docLocation);
            cv.put("isClosed", strIsClosed);
            cv.put("CreatedBy", userLoginId);
            cv.put("CreatedDtime", curDate);
            cv.put("MobRefId", MobRefId);
            cv.put("SyncStatus", SyncStatus);
            cv.put("RespStatus", RespStatus);

            if (mCursor.getCount() <= 0) {
                isSuccess = (int) DatabaseHelper.db.insert(cnxt.getResources().getString(R.string.TBL_FAQ_Conversation), null, cv);
            } else if (mCursor.getCount() > 0) {
                isSuccess = db.update(cnxt.getResources().getString(R.string.TBL_FAQ_Conversation), cv,
                        "MobRefId=?", new String[]{MobRefId.trim()});
            }
            mCursor.close();
        } catch (Exception e) {
            Log.e("", "SaveConversation Error=" + e.toString());
        }

        return isSuccess;
    }

    public static void updateMsgDelevered(Context context, SQLiteDatabase db, String MobRefId,
                                          String RespStatus, String SyncStatus) {
        Cursor mCursor;
        ContentValues contentValues = new ContentValues();
        contentValues.put("RespStatus", RespStatus);
        contentValues.put("SyncStatus", SyncStatus);

        mCursor = db.query(context.getResources().getString(R.string.TBL_FAQ_Conversation),
                null, "MobRefId=?",
                new String[]{"" + MobRefId}, null, null, null, null);

        if (mCursor.getCount() > 0) {
            db.update(context.getResources().getString(R.string.TBL_FAQ_Conversation), contentValues,
                    "MobRefId=?", new String[]{MobRefId});
        } else {

        }
    }

    public static Cursor getFAQConversation(Context context, SQLiteDatabase db, String candidateId, String trainer, String QuesID) {
        Cursor mCursor;
        mCursor = db.query(context.getResources().getString(R.string.TBL_FAQ_Conversation),
                null, "CandidateId=? and TrainerId=? and QuesId=?",
                new String[]{"" + candidateId, "" + trainer, QuesID}, null, null, null, null);
        return mCursor;
    }

    @SuppressLint("Range")
    public static boolean isShowFAQConversation(Context context, SQLiteDatabase db, String candidateId,
                                                String trainer, String QuesID, String MobRefId) {
        Cursor mCursor;
        boolean stats = false;
        try {
            mCursor = db.query(context.getResources().getString(R.string.TBL_FAQ_Conversation),
                    null, "CandidateId=? and TrainerId=? and QuesId=? and MobRefId=?",
                    new String[]{"" + candidateId, "" + trainer, QuesID, MobRefId}, null, null, null, null);
            if (mCursor.getColumnCount() > 0) {
                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    if (mCursor.isNull(mCursor.getColumnIndex("isShow"))
                            || mCursor.getString(mCursor.getColumnIndex("isShow")).equals("")
                            || mCursor.getString(mCursor.getColumnIndex("isShow")).equals("Y")) {
                        stats = true;
                    } else {
                        stats = false;
                    }
                }
            }
            mCursor.close();
        } catch (Exception e) {
            stats = false;
        }
        return stats;
    }

    public static boolean updateShowFAQConversation(Context context, SQLiteDatabase db, String candidateId,
                                                    String trainer, String QuesID, String MobRefId) {
        Cursor mCursor;
        mCursor = db.query(context.getResources().getString(R.string.TBL_FAQ_Conversation),
                null, "CandidateId=? and TrainerId=? and QuesId=? and MobRefId=?",
                new String[]{"" + candidateId, "" + trainer, QuesID, MobRefId}, null, null, null, null);
        if (mCursor.getColumnCount() > 0) {
            ContentValues cv = new ContentValues();
            cv.put("isShow", "N");
            db.update(context.getResources().getString(R.string.TBL_FAQ_Conversation), cv,
                    "CandidateId=? and TrainerId=? and QuesId=? and MobRefId=?",
                    new String[]{"" + candidateId, "" + trainer, QuesID, MobRefId});
            return true;
        } else {
            return false;
        }
    }

    public static Cursor sendFAQToServer(Context context, SQLiteDatabase db, String candidateId) {
        Cursor mCursor;
        mCursor = db.query(context.getResources().getString(R.string.TBL_FAQ_Conversation),
                null, "CandidateId=?",
                new String[]{candidateId, "Pending"}, null, null, null, null);
        return mCursor;
    }

    public static Cursor sendAllFAQConversationToServer(Context context, SQLiteDatabase db) {
        Cursor mCursor;
        mCursor = db.query(context.getResources().getString(R.string.TBL_FAQ_Conversation),
                null, "SyncStatus=?",
                new String[]{"pending"}, null, null, null, null);
        return mCursor;
    }

    public static void CloseConversationDetails(Context cnxt, SQLiteDatabase db, String strLoginId, String strTrainerID,
                                                String strQuesID, String Status) {
        try {
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("isClosed", Status);
            int iRec = db.update(cnxt.getResources().getString(R.string.TBL_FAQ_List), cv, "QuesId=? and CandidateId=? and TrainerId=?"
                    , new String[]{strQuesID, "" + strLoginId, "" + strTrainerID});
            Log.d("", iRec + "");
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
    }


    public static int SaveFAQList(Context cnxt, SQLiteDatabase db, String CandidateId, String QuesId, String TrainerId,
                                  String QuesDesc, String RespDesc, String RespDtime, String RespFrom, String isClosed,
                                  String Name, String Mobile, byte[] Images, String RN, String MobRefId, String LastRespDtime,
                                  String Sync) {
        int isSuccess = 0;
        try {
            Cursor mCursor;
//            mCursor = DatabaseHelper.db.query(cnxt.getResources().getString(R.string.TBL_FAQ_List), null,
//                    "MobRefId=?", new String[]{MobRefId}, null, null, null);
            mCursor = DatabaseHelper.db.query(cnxt.getResources().getString(R.string.TBL_FAQ_List), null,
                    "CandidateId=? and TrainerId=? and QuesId=?", new String[]{CandidateId, TrainerId, QuesId}, null, null, null);

            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("CandidateId", CandidateId);
            cv.put("QuesId", QuesId);
            cv.put("TrainerId", TrainerId);
            cv.put("QuesDesc", QuesDesc);
            cv.put("RespDesc", RespDesc);
            cv.put("RespDtime", RespDtime);
            cv.put("RespFrom", RespFrom);
            cv.put("isClosed", isClosed);
            cv.put("Name", Name);
            cv.put("Mobile", Mobile);
            cv.put("Images", Images);
            cv.put("RN", RN);
            cv.put("LastRespDtime", CommonClass.getMilliFromDate(LastRespDtime));
            cv.put("MobRefId", MobRefId);
            cv.put("SyncStatus", Sync);

            if (mCursor.getCount() > 0) {
                //isSuccess = db.update(cnxt.getResources().getString(R.string.TBL_FAQ_List), cv, "MobRefId=?", new String[]{MobRefId});
                isSuccess = db.delete(cnxt.getResources().getString(R.string.TBL_FAQ_List), "CandidateId=? and TrainerId=? and QuesId=?", new String[]{CandidateId, TrainerId, QuesId});
                isSuccess = (int) DatabaseHelper.db.insert(cnxt.getResources().getString(R.string.TBL_FAQ_List), null, cv);
            } else {
                isSuccess = (int) DatabaseHelper.db.insert(cnxt.getResources().getString(R.string.TBL_FAQ_List), null, cv);
            }
            mCursor.close();
        } catch (Exception e) {
            Log.e("DatabaseHelper", "error" + e.toString());
            DatabaseHelper.SaveErroLog(cnxt, DatabaseHelper.db,
                    "DatabaseHelper", "SaveFAQList", e.toString());
        }

        return isSuccess;
    }

    public static int UpdateSubjectTimeFAQList(Context cnxt, SQLiteDatabase db, String strCandidateId,
                                               String strTrainerId, String strQuesID, String LastRespDtime) {
        int isSuccess = 0;
        try {
            Cursor mCursor;
            mCursor = DatabaseHelper.db.query(cnxt.getResources().getString(R.string.TBL_FAQ_List), null,
                    "CandidateId=? and TrainerId=? and QuesId=?", new String[]{strCandidateId,
                            strTrainerId, strQuesID}, null, null, null);
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("LastRespDtime", CommonClass.getMilliFromDate(LastRespDtime));

            if (mCursor.getCount() > 0) {
                isSuccess = db.update(cnxt.getResources().getString(R.string.TBL_FAQ_List), cv,
                        "CandidateId=? and TrainerId=? and QuesId=?", new String[]{strCandidateId,
                                strTrainerId, strQuesID});
            }
            if (StaticVariables.UserType.trim().equalsIgnoreCase("T")) {
                isSuccess = db.update(cnxt.getResources().getString(R.string.TBL_FAQ_Group), cv,
                        "CandidateId=? and TrainerId=?", new String[]{strCandidateId,
                                strTrainerId});
            }
            mCursor.close();
        } catch (Exception e) {
            Log.e("DatabaseHelper", "error" + e.toString());
            DatabaseHelper.SaveErroLog(cnxt, DatabaseHelper.db,
                    "DatabaseHelper", "SaveFAQList", e.toString());
        }

        return isSuccess;
    }

    public static int SaveFAQGroup(Context cnxt, SQLiteDatabase db, String CandidateId, String QuesCount, String TrainerId,
                                   String CandidateName, String TrainerName, String CndMobile, String TrnMobile, byte[] Images,String LastRespDtime) {
        int isSuccess = 0;
        try {
            Cursor mCursor;
            mCursor = DatabaseHelper.db.query(cnxt.getResources().getString(R.string.TBL_FAQ_Group), null,
                    "CandidateId=? and TrainerId=?", new String[]{CandidateId, TrainerId}, null, null, null);
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("CandidateId", CandidateId);
            cv.put("QuesCount", QuesCount);
            cv.put("TrainerId", TrainerId);
            cv.put("CandidateName", CandidateName);
            cv.put("TrainerName", TrainerName);
            cv.put("CndMobile", CndMobile);
            cv.put("TrnMobile", TrnMobile);
            cv.put("Images", Images);
            cv.put("LastRespDtime", CommonClass.getMilliFromDate(LastRespDtime));
            if (mCursor.getCount() > 0) {
                isSuccess = db.update(cnxt.getResources().getString(R.string.TBL_FAQ_Group), cv,
                        "CandidateId=? and TrainerId=?", new String[]{CandidateId, TrainerId});
            } else {
                isSuccess = (int) DatabaseHelper.db.insert(cnxt.getResources().getString(R.string.TBL_FAQ_Group), null, cv);
            }
            mCursor.close();
        } catch (Exception e) {
            Log.e("DatabaseHelper", "error" + e.toString());
            DatabaseHelper.SaveErroLog(cnxt, DatabaseHelper.db,
                    "DatabaseHelper", "SaveFAQGroup", e.toString());
        }

        return isSuccess;
    }

    public static int UpdateFAQList(Context cnxt, SQLiteDatabase db, String QuesId, String MobRefId) {
        int isSuccess = 0;
        try {
            Cursor mCursor;

            mCursor = DatabaseHelper.db.query(cnxt.getResources().getString(R.string.TBL_FAQ_List), null,
                    "MobRefId=?", new String[]{MobRefId}, null, null, null);
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("QuesId", QuesId);
            cv.put("SyncStatus", "Complete");

            if (mCursor.getCount() > 0) {
                isSuccess = db.update(cnxt.getResources().getString(R.string.TBL_FAQ_List), cv,
                        "MobRefId=?", new String[]{MobRefId});
            }
            mCursor.close();
        } catch (Exception e) {
            Log.e("DatabaseHelper", "error" + e.toString());
            DatabaseHelper.SaveErroLog(cnxt, DatabaseHelper.db,
                    "DatabaseHelper", "UpdateFAQList", e.toString());
        }

        return isSuccess;
    }


    public static Cursor getFAQListCursor(Context cnxt, SQLiteDatabase db, String CandidateId, String TrainerId, String UserType) {
        try {
            Cursor mCursor;
            if (UserType.equals("C")) {
                mCursor = DatabaseHelper.db.query(cnxt.getResources().getString(R.string.TBL_FAQ_List), null,
                        "CandidateId=?", new String[]{CandidateId}, null, null, "LastRespDtime DESC");
            } else {
                mCursor = DatabaseHelper.db.query(cnxt.getResources().getString(R.string.TBL_FAQ_List), null,
                        "TrainerId=? and CandidateId=?", new String[]{TrainerId, CandidateId}, null, null, "LastRespDtime DESC");
            }

            return mCursor;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Cursor Get_mPINCursor(Context cnxt, SQLiteDatabase db, String UserId) {
        Cursor mCursor = null;
        try {
            mCursor = db.query(cnxt.getString(R.string.TBL_iCandidate), null, "CandidateLoginId=?",
                    new String[]{UserId + ""}, null, null, null);
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
        return mCursor;
    }

    public static Cursor Get_PINwithoutID(Context cnxt, SQLiteDatabase db) {
        Cursor mCursor = null;
        try {
            mCursor = db.query(cnxt.getString(R.string.TBL_iCandidate), null, null,
                    null, null, null, null, "1");
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
        return mCursor;
    }

    public static void SavePINInfo(Context cnxt, SQLiteDatabase db, String str_LoginId,
                                   String mPIN, String is_mPIN_Data) {

        Cursor cquery = null;
        try {
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("mPIN", mPIN);
            cv.put("is_mPIN_Data", is_mPIN_Data);

            cquery = db.query(cnxt.getString(R.string.TBL_iCandidate), null, "CandidateLoginId=?", new String[]{str_LoginId}, null, null, null);
            if (cquery.getCount() > 0)
                db.update(cnxt.getString(R.string.TBL_iCandidate), cv, "CandidateLoginId=?", new String[]{str_LoginId});
            else
                db.insertOrThrow(cnxt.getString(R.string.TBL_iCandidate), null, cv);

            cquery.close();

        } catch (Exception e) {
            Log.e("SavePINInfo", "err=" + e.toString());
        }
    }

    public static int GetReferCount(Context cnxt, SQLiteDatabase db, String LoginId) {
        int cnt = 0;
        try {
            Cursor cquery;
            cquery = db.query(cnxt.getResources().getString(R.string.TBL_ReferApp), null, "TrainerId=? and RefBy=?",
                    new String[]{LoginId, LoginId}, null, null, null);
            cnt = cquery.getCount();
            cquery.close();
        } catch (Exception e) {
            Log.e("", "GetAttemptedExam err=" + e.toString());
        }
        return cnt;
    }

    public static void InsertReferenceDetails(Context cnxt, SQLiteDatabase db, String UserId, String TrainerId,
                                              String CandidateId, String RefId, String RefBy, String IsPaid, String PaymentMode,
                                              String Amount, String PaymentStatus) {
        try {

            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("RefId", RefId);
            cv.put("RefCode", UserId);
            cv.put("RefBy", RefBy);
            cv.put("TrainerId", TrainerId);
            cv.put("CandidateId", CandidateId);
            cv.put("IsPaid", IsPaid);
            cv.put("PaymentMode", PaymentMode);
            cv.put("Amount", Amount);
            cv.put("PaymentStatus", PaymentStatus);

            db.insertOrThrow(cnxt.getResources().getString(R.string.TBL_ReferApp), null, cv);

        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
    }

    public static Cursor getContentRefQueries(Context context, SQLiteDatabase db, String UserId, String strMsg) {
        Cursor mCursor;
        mCursor = db.query(context.getResources().getString(R.string.TBL_ContentQueries),
                new String[]{"RecId", "UserId", "SectionId", "QueryType", "QueryCat", "QueryDesc",
                        "isSkiped", "isLiked", "isDisliked", "QueryDtime"},
                "UserId=? and QueryDesc=?",
                new String[]{"" + UserId, strMsg}, null, null, null, "1");
        return mCursor;
    }

    public static Cursor getFAQListRefDetails(Context cnxt, SQLiteDatabase db, String CandidateId, String UserType, String strMsg) {
        try {
            Cursor mCursor;
            if (UserType.equals("C")) {
                mCursor = DatabaseHelper.db.query(cnxt.getResources().getString(R.string.TBL_FAQ_List), null,
                        "CandidateId=? and QuesDesc=?", new String[]{CandidateId, strMsg}, null, null, null);
            } else {
                mCursor = DatabaseHelper.db.query(cnxt.getResources().getString(R.string.TBL_FAQ_List), null,
                        "TrainerId=? and QuesDesc=?", new String[]{CandidateId, strMsg}, null, null, null);
            }

            return mCursor;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void SaveProfession(Context cnxt, SQLiteDatabase db, String ProfCode,
                                      String ProfName) {
        Cursor cquery = null;
        try {
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("ProfCode", ProfCode);
            cv.put("ProfName", ProfName);

            cquery = db.query(cnxt.getString(R.string.TBL_Profession), null, "ProfName=?", new String[]{ProfName.trim()}, null, null, null);
            if (cquery.getCount() > 0)
                db.update(cnxt.getString(R.string.TBL_Profession), cv, "ProfName=?", new String[]{ProfName});
            else
                db.insertOrThrow(cnxt.getString(R.string.TBL_Profession), null, cv);

            cquery.close();

        } catch (Exception e) {
            Log.e("SaveProfession", "err=" + e.toString());
        }
    }

    public static void SaveCountry(Context cnxt, SQLiteDatabase db,
                                   String CountryName, String CountryCode) {
        Cursor cquery = null;
        try {
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("CountryCode", CountryCode);
            cv.put("CountryName", CountryName);

            cquery = db.query(cnxt.getString(R.string.TBL_Country), null, "CountryName=?",
                    new String[]{CountryName.trim()}, null, null, null);
            if (cquery.getCount() > 0)
                db.update(cnxt.getString(R.string.TBL_Country), cv, "CountryName=?", new String[]{CountryName});
            else
                db.insertOrThrow(cnxt.getString(R.string.TBL_Country), null, cv);

            cquery.close();

        } catch (Exception e) {
            Log.e("Country", "err=" + e.toString());
        }
    }

    @SuppressLint("Range")
    public static String GetCountryCode(Context cnxt, SQLiteDatabase db, String Country) {
        Cursor mCursor;
        String CountryCode = "";
        try {
            mCursor = db.query(cnxt.getResources().getString(R.string.TBL_Country), null, "CountryName=?",
                    new String[]{"" + Country}, null, null, null);
            if (mCursor.getCount() > 0) {
                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    CountryCode = mCursor.getString(mCursor.getColumnIndex("CountryCode"));
                }
            }
            mCursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return CountryCode;
    }

    public static void SaveTokens(Context cnxt, SQLiteDatabase db, String UserId, String Token,
                                  String TokenType, String ValidFrom, String ValidTo, String TotalCount,
                                  String TokenUsed, String isActive,String isExpired,
                                  String CreatedBy, String CreatedDtime) {
        Cursor cquery = null;
        try {
            ContentValues cv = new ContentValues();
            cv.clear();
            cv.put("UserId", UserId);
            cv.put("Token", Token);
            cv.put("TokenType", TokenType);
            cv.put("ValidFrom", ValidFrom);
            cv.put("ValidTo", ValidTo);
            cv.put("TotalCount", TotalCount);
            cv.put("TokenUsed", TokenUsed);
            cv.put("isActive", isActive);
            cv.put("isExpired", isExpired);
            cv.put("CreatedBy", CreatedBy);
            cv.put("CreatedDtime", CreatedDtime);

            cquery = db.query(cnxt.getString(R.string.Tbl_TokenMst), null,
                    "UserId=? and Token=?", new String[]{UserId, Token}, null, null, null);
            int i;
            if (cquery.getCount() > 0)
                i = db.update(cnxt.getString(R.string.Tbl_TokenMst), cv, "UserId=? and Token=?",
                        new String[]{UserId, Token});
            else
                i = (int) db.insertOrThrow(cnxt.getString(R.string.Tbl_TokenMst), null, cv);

            cquery.close();

        } catch (Exception e) {
            Log.e("Country", "err=" + e.toString());
        }
    }

    @Override
    public synchronized void close() {
        if (db != null) {
            db.close();
            super.close();
        }
    }


    public static int SaveSectionExamMapping(Context cnxt, SQLiteDatabase db, String SectionId,
                                             String ExamId,
                                             String SectionDesc,
                                             String ExamDesc,
                                             String IsActive,
                                             String CreatedBy,
                                             String CreateDtime,
                                             String UpdatedBy,
                                             String UpdateDtime) {
        int operation = 0; //0- No notification , 1-Show Notification
        try {

            ContentValues cv = new ContentValues();
            cv.clear();

            cv.put("SectionId", SectionId);
            cv.put("ExamId", ExamId);
            cv.put("SectionDesc", SectionDesc);
            cv.put("ExamDesc", ExamDesc);
            cv.put("IsActive", IsActive);
            cv.put("CreatedBy", CreatedBy);
            cv.put("CreateDtime", CreateDtime);
            cv.put("UpdatedBy", UpdatedBy);
            cv.put("UpdateDtime", UpdateDtime);

            Cursor cursor = db.query(cnxt.getResources().getString(R.string.Tbl_SectionExamMapping),
                    null, "SectionId=? and ExamId=?", new String[]{SectionId, ExamId}, null, null, null);
            if (cursor.getCount() > 0) {
                operation = 0;
                db.update(cnxt.getResources().getString(R.string.Tbl_SectionExamMapping), cv,
                        "SectionId=? and ExamId=?", new String[]{SectionId, ExamId});
            } else {
                db.insertOrThrow(cnxt.getResources().getString(R.string.Tbl_SectionExamMapping), null, cv);
                operation = 1;
            }
            cursor.close();
        } catch (Exception e) {
            Log.e("", "SaveExamMaster Error=" + e.toString());
        }
        return operation;
    }

}
