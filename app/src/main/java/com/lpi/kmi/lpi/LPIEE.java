package com.lpi.kmi.lpi;

import android.app.Application;
import android.content.Context;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.google.android.play.core.appupdate.AppUpdateManager;

public class LPIEE extends Application {



    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}


