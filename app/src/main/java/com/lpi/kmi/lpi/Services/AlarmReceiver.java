package com.lpi.kmi.lpi.Services;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.GetConversation;
import com.lpi.kmi.lpi.activities.Login;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.stringcare.library.SC;

import net.sqlcipher.database.SQLiteDatabase;

public class AlarmReceiver extends BroadcastReceiver {

    @SuppressLint("Range")
    @Override
    public void onReceive(final Context context, Intent intent) {
        try {

            try {
                if (DatabaseHelper.db == null) {
                    SQLiteDatabase.loadLibs(context);
                    DatabaseHelper.db = DatabaseHelper.getInstance(context).
                            getWritableDatabase(SC.reveal(CommonClass.DBPassword));
                }
            } catch (Exception e) {
                Log.e("", "db error=" + e.toString());
            }

            //Toast.makeText(context,"test............",Toast.LENGTH_SHORT).show();
            if (intent != null && intent.getAction() != null && intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
                Intent alarmIntent = new Intent(context.getApplicationContext(), AlarmReceiver.class);
                PendingIntent pendingIntent;
//                = PendingIntent.getBroadcast(context.getApplicationContext(), 0, alarmIntent, 0);
//                pendingIntent= PendingIntent.getBroadcast(context.getApplicationContext(), 0, alarmIntent, 0);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                    pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), 0, alarmIntent, PendingIntent.FLAG_IMMUTABLE);
                } else {
                    pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), 0, alarmIntent, 0);
                }
                AlarmManager manager = (AlarmManager) context.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                int interval = 5000;
                manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
            }
            CommonClass.checkSettings(context,"C");
            if (StaticVariables.SyncWIFI.equals("Yes")) {

                Cursor mCursor = DatabaseHelper.db.query(context.getResources().getString(R.string.TBL_iCandidate), null,
                        null, null,null, null, null,"1");
                if (mCursor.getCount() > 0) {

                    for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                        StaticVariables.crnt_InscType = "";
                        StaticVariables.isLogin = true;
                        StaticVariables.UserName = mCursor.getString(mCursor.getColumnIndex("FirstName"));
                        StaticVariables.UserLoginId = mCursor.getString(mCursor.getColumnIndex("CandidateLoginId"));
                        StaticVariables.ParentId = mCursor.getString(mCursor.getColumnIndex("ParentId"));
                        StaticVariables.ParentName = mCursor.getString(mCursor.getColumnIndex("ParentName"));
                        StaticVariables.UserMobileNo = mCursor.getString(mCursor.getColumnIndex("CandidateMobileNo1"));
                        StaticVariables.offlineMode = "N";
                        StaticVariables.UserType = mCursor.getString(mCursor.getColumnIndex("UserType"));
                    }
                }

                if (CommonClass.networkType(context).equals("WiFi")) {
                    if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncExamMaster")) {
                        Intent newIntent = new Intent(context.getApplicationContext(), SyncExamMaster.class);
//                        context.getApplicationContext().startService(newIntent);
                        CommonClass.StartService(context.getApplicationContext(), newIntent);
                    }
                    GetConversation conversation = new GetConversation();
                    if (conversation.getStatus() != AsyncTask.Status.RUNNING)
                        new GetConversation(context, new Callback() {
                            @Override
                            public void execute(Object result, String status) {
                                startServices(context);
                            }
                        }).execute();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AlarmReceiver", "onReceive", e.toString());
        }
    }

    private void startServices(Context context) {
        if (CommonClass.isConnected(context.getApplicationContext())) {


//            if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncFAQModuleData")) {
//                Intent newIntent = new Intent(context.getApplicationContext(), SyncFAQModuleData.class);
//                context.getApplicationContext().startService(newIntent);
//            }

            if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncActivityDetails")) {
                Intent trackIntent = new Intent(context.getApplicationContext(), SyncActivityDetails.class);
//                context.getApplicationContext().startService(trackIntent);
                CommonClass.StartService(context.getApplicationContext(), trackIntent);
            }

            if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncErrorLog")) {
                Intent newIntent = new Intent(context.getApplicationContext(), SyncErrorLog.class);
//                context.getApplicationContext().startService(newIntent);
                CommonClass.StartService(context.getApplicationContext(), newIntent);
            }

            if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncOfflineData")) {
                Intent newIntent = new Intent(context.getApplicationContext(), SyncOfflineData.class);
//                context.getApplicationContext().startService(newIntent);
                CommonClass.StartService(context.getApplicationContext(), newIntent);
            }

        }
    }
}




