package com.lpi.kmi.lpi.Services;

import android.app.IntentService;
import android.content.Intent;
import androidx.annotation.Nullable;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.stringcare.library.SC;

import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by darshanad on 9/6/2017.
 */

public class CurrentStatusIntentService  extends IntentService {

    public CurrentStatusIntentService() {
        super("");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        try {
            startForeground(CommonClass.getNotificationId(), CommonClass.createServiceNotification(this));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (DatabaseHelper.db == null) {
            SQLiteDatabase.loadLibs(getApplicationContext());
            DatabaseHelper.db = DatabaseHelper.getInstance(getApplicationContext()).
                    getWritableDatabase(SC.reveal(CommonClass.DBPassword));
        }

        LPI_BasicDetails();

    }

    public void LPI_BasicDetails() {



        org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                getApplicationContext().getString(R.string.Exam_NAMESPACE),
                getApplicationContext().getString(R.string.CurrentStatusService));

        // property which holds input parameters
        PropertyInfo inputPI1 = new PropertyInfo();
        String strResponse = "";

        inputPI1.setName("AppNo");
        try {

            inputPI1.setValue(/*StaticVariables.UserLoginId*/ SC.reveal(CommonClass.DummyAppId));
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        inputPI1.setType(String.class);
        request.addProperty(inputPI1);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.dotNet = true;
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(getApplicationContext().getString(R.string.Exam_URL));


        try {
            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
            androidHttpTransport.call(getApplicationContext().getString(R.string.Exam_SOAP_ACTION) +
                    getApplicationContext().getString(R.string.CurrentStatusService), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            // Assign it to static variable
            strResponse = response.toString();
            Log.e("", "response=" + strResponse);



        } catch (Exception e) {
            Log.e("", "Login Error=" + e.toString());
        }
        GetJSONValue(strResponse);

    }

    private void GetJSONValue(String strResponse) {

        try {
            JSONObject jsonobj = new JSONObject(strResponse);
            String json="";
            String response="";

            json = jsonobj.getJSONArray("Table").toString();


            try {
                JSONArray basicDetailsArray = new JSONArray(json);
                for (int i = 0; i < basicDetailsArray.length(); i++) {
                    JSONObject obj = new JSONObject(basicDetailsArray.getString(i));
                    response = obj.getString("");
                }
            }catch (Exception e){
                Log.e("Error",e.getMessage());
            }



            DatabaseHelper.CurrentStatusInsertData(getApplicationContext(), DatabaseHelper.db,response);


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
