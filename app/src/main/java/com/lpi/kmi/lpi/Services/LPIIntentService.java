package com.lpi.kmi.lpi.Services;

import android.app.IntentService;
import android.content.Intent;
import androidx.annotation.Nullable;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.stringcare.library.SC;

import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by darshanad on 9/6/2017.
 */

public class LPIIntentService extends IntentService {

    public LPIIntentService() {
        super("");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        try {
            startForeground(CommonClass.getNotificationId(), CommonClass.createServiceNotification(this));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (DatabaseHelper.db == null) {
            SQLiteDatabase.loadLibs(getApplicationContext());
            DatabaseHelper.db = DatabaseHelper.getInstance(getApplicationContext()).
                    getWritableDatabase(SC.reveal(CommonClass.DBPassword));
        }

        LPI_BasicDetails();

    }

    public void LPI_BasicDetails() {


        org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                getApplicationContext().getString(R.string.Exam_NAMESPACE),
                getApplicationContext().getString(R.string.LPI_BasicDetails));

        // property which holds input parameters
        PropertyInfo inputPI1 = new PropertyInfo();
        String strResponse = "";

        inputPI1.setName("AppNo");
        try {

            inputPI1.setValue(/*StaticVariables.UserLoginId*/ SC.reveal(CommonClass.DummyAppId));
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        inputPI1.setType(String.class);
        request.addProperty(inputPI1);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.dotNet = true;
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(getApplicationContext().getString(R.string.Exam_URL));


        try {
            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
            androidHttpTransport.call(getApplicationContext().getString(R.string.Exam_SOAP_ACTION) +
                    getApplicationContext().getString(R.string.LPI_BasicDetails), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            // Assign it to static variable
            strResponse = response.toString();
            Log.e("", "response=" + strResponse);


        } catch (Exception e) {
            Log.e("", "Login Error=" + e.toString());
        }
        GetJSONValue(strResponse);

    }

    private void GetJSONValue(String strResponse) {

        try {
            JSONObject jsonobj = new JSONObject(strResponse);

            String basicDetails, companyDetails, LicenDetails, TrainingDetails, ExamDetails, FeesDetails;
            String AppNo = "",
                    GivenName = "",
                    CndURN = "",
                    cndStatus = "",
                    SAPCode = "",
                    ManagerName = "",
                    Branch = "",
                    Hierarchy = "",
                    SubClass = "",
                    LcnExpDate = "",
                    LcnNo = "",
                    LcnIssDate = "",
                    TrnMode = "",
                    TrnLocDesc = "",
                    TrnInstitute = "",
                    AccrdNo = "",
                    HrsTrn = "",
                    TrnStartDate = "",
                    TrnEndDate = "",
                    ExmMode = "",
                    ExamLanguage = "",
                    ExmBody = "",
                    ExmCentre = "",
                    FeesTokenNo = "",
                    FeesExpected = "",
                    FeesCollected = "",
                    PaymentMode = "",
                    FeesCollectedDate = "",
                    FeesTransactionNumber = "";


            basicDetails = jsonobj.getJSONArray("Table").toString();
            companyDetails = jsonobj.getJSONArray("Table1").toString();
            LicenDetails = jsonobj.getJSONArray("Table2").toString();
            TrainingDetails = jsonobj.getJSONArray("Table3").toString();
            ExamDetails = jsonobj.getJSONArray("Table4").toString();
            FeesDetails = jsonobj.getJSONArray("Table5").toString();

            try {
                JSONArray basicDetailsArray = new JSONArray(basicDetails);
                for (int i = 0; i < basicDetailsArray.length(); i++) {
                    JSONObject obj = new JSONObject(basicDetailsArray.getString(i));
                    AppNo = obj.getString("AppNo");
                    GivenName = obj.getString("GivenName");
                    CndURN = obj.getString("CndURN");
                    cndStatus = obj.getString("cndStatus");
                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
            }

            try {
                JSONArray companyDetailsArray = new JSONArray(companyDetails);
                for (int i = 0; i < companyDetails.length(); i++) {
                    JSONObject obj = new JSONObject(companyDetailsArray.getString(i));
                    SAPCode = obj.getString("SAPCode");
                    ManagerName = obj.getString("ManagerName");
                    Branch = obj.getString("Branch");
                    Hierarchy = obj.getString("Hierarchy");
                    SubClass = obj.getString("SubClass");
                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
            }


            try {

                JSONArray LicenDetailsArray = new JSONArray(LicenDetails);
                for (int i = 0; i < LicenDetails.length(); i++) {
                    JSONObject obj = new JSONObject(LicenDetailsArray.getString(i));
                    LcnExpDate = obj.getString("LcnExpDate");
                    LcnNo = obj.getString("LcnNo");
                    LcnIssDate = obj.getString("LcnIssDate");
                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
            }


            try {
                JSONArray TrainingDetailsArray = new JSONArray(TrainingDetails);
                for (int i = 0; i < TrainingDetails.length(); i++) {
                    JSONObject obj = new JSONObject(TrainingDetailsArray.getString(i));
                    TrnMode = obj.getString("TrnMode");
                    TrnLocDesc = obj.getString("TrnLocDesc");
                    TrnInstitute = obj.getString("TrnInstitute");
                    AccrdNo = obj.getString("AccrdNo");
                    HrsTrn = obj.getString("HrsTrn");
                    TrnStartDate = obj.getString("TrnStartDate");
                    TrnEndDate = obj.getString("TrnEndDate");
                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
            }

            try {

                JSONArray ExamDetailsArray = new JSONArray(ExamDetails);
                for (int i = 0; i < ExamDetails.length(); i++) {
                    JSONObject obj = new JSONObject(ExamDetailsArray.getString(i));
                    ExmMode = obj.getString("ExmMode");
                    ExamLanguage = obj.getString("ExamLanguage");
                    ExmBody = obj.getString("ExmBody");
                    ExmCentre = obj.getString("AccrdNo");
                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
            }


            try {
                JSONArray FeesDetailsArray = new JSONArray(FeesDetails);
                for (int i = 0; i < FeesDetails.length(); i++) {
                    JSONObject obj = new JSONObject(FeesDetailsArray.getString(i));
                    FeesTokenNo = obj.getString("FeesTokenNo");
                    FeesExpected = obj.getString("FeesExpected");
                    FeesCollected = obj.getString("FeesCollected");
                    PaymentMode = obj.getString("PaymentMode");
                    FeesCollectedDate = obj.getString("FeesCollectedDate");
                    FeesTransactionNumber = obj.getString("FeesTransactionNumber");
                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
            }


            DatabaseHelper.LpiinsertData(getApplicationContext(), DatabaseHelper.db, AppNo,
                    GivenName,
                    CndURN,
                    cndStatus,
                    SAPCode,
                    ManagerName,
                    Branch,
                    Hierarchy,
                    SubClass,
                    LcnExpDate,
                    LcnNo,
                    LcnIssDate,
                    TrnMode,
                    TrnLocDesc,
                    TrnInstitute,
                    AccrdNo,
                    HrsTrn,
                    TrnStartDate,
                    TrnEndDate,
                    ExmMode,
                    ExamLanguage,
                    ExmBody,
                    ExmCentre,
                    FeesTokenNo,
                    FeesExpected,
                    FeesCollected,
                    PaymentMode,
                    FeesCollectedDate,
                    FeesTransactionNumber);


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}
