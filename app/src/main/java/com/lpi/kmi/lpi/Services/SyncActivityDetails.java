package com.lpi.kmi.lpi.Services;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import androidx.annotation.Nullable;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.stringcare.library.SC;

import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mustafakachwalla on 21/08/17.
 */

public class SyncActivityDetails extends IntentService {
    String strResponse;
    List<String> XMLElementIDs = new ArrayList<String>(), XMLElementValues = new ArrayList<String>();

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public SyncActivityDetails() {
        super("SyncActivityDetails");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @SuppressLint("Range")
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
            startForeground(CommonClass.getNotificationId(), CommonClass.createServiceNotification(this));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            if (DatabaseHelper.db == null) {
                SQLiteDatabase.loadLibs(getApplicationContext());
                DatabaseHelper.db = DatabaseHelper.getInstance(getApplicationContext()).
                        getWritableDatabase(SC.reveal(CommonClass.DBPassword));
            }

            Cursor cursor = DatabaseHelper.db.query(getResources().getString(R.string.TblActivityTracker),
                    new String[]{"RecId", "UserId", "ParentActivity", "ActivityCode", "ActivityDesc", "LobId", "ProdId", "SectionId",
                            "fromTime", "toTime", "Remark", "CreatedBy", "CreatedDate", "UpdatedBy", "UpdatedDate",
                            "DeviceId", "DeviceName"},
                    "SyncStatus=?", new String[]{"Pending"}, null, null, "RecId");
            XMLElementIDs.clear();
            XMLElementValues.clear();
            if (cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    for (int i = 0; i < cursor.getColumnCount(); i++) {
                        XMLElementIDs.add(cursor.getColumnName(i));
                        XMLElementValues.add(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(i))));
                    }
                    syncActivityData();
                    XMLElementIDs.clear();
                    XMLElementValues.clear();
                }
            } else {
                Log.e("SyncActivityDetails", "No Activity Details");
            }
            cursor.close();
            cursor = DatabaseHelper.db.query(getResources().getString(R.string.TBL_ContentQueries),
                    new String[]{"RecId", "UserId", "SectionId", "QueryType", "QueryCat", "QueryDesc",
                            "isSkiped", "isLiked", "isDisliked", "QueryDtime"},
                    "SyncStatus=?",
                    new String[]{"Pending"}, null, null, null);

            XMLElementIDs.clear();
            XMLElementValues.clear();
            if (cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    for (int i = 0; i < cursor.getColumnCount(); i++) {
                        XMLElementIDs.add(cursor.getColumnName(i));
                        XMLElementValues.add(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(i))));
                    }
                    syncQueryData();
                    XMLElementIDs.clear();
                    XMLElementValues.clear();
                }
            } else {
                Log.e("SyncActivityDetails", "No Activity Details");
            }

            cursor.close();
        } catch (Exception e) {
            Log.e("SyncActivityDetails", "error=" + e.toString());
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncActivityDetails", "onHandleIntent", e.toString());
            onDestroy();
        }
    }

    protected void syncActivityData() {
        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    getResources().getString(R.string.Exam_NAMESPACE), getResources().getString(R.string.setActivityTracker));
            //property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            inputPI1.setName("objXMLDoc");

            String objXMLDoc = CommonClass.GenerateXML(getApplicationContext(), XMLElementIDs.size(), "setActivityTracker",
                    XMLElementIDs.toArray(new String[XMLElementIDs.size()]),
                    XMLElementValues.toArray(new String[XMLElementValues.size()]));
            inputPI1.setValue(objXMLDoc);

            inputPI1.setType(String.class);
            request.addProperty(inputPI1);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);
            envelope.dotNet = true;
            //Set output SOAP object
            envelope.setOutputSoapObject(request);

            try {
                //Create HTTP call object
                HttpTransportSE androidHttpTransport = new HttpTransportSE(getResources().getString(R.string.Exam_URL));
//        InputStream isResponse = null;

                //Involve Web service
                LPIEEX509TrustManager.allowAllSSL();
                androidHttpTransport.call(getResources().getString(R.string.Exam_SOAP_ACTION) +
                        getResources().getString(R.string.setActivityTracker), envelope);
                //Get the response
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strResponse = response.toString();
//                Log.e("SyncActivityDetails", "sstrResponse=" + strResponse);
            } catch (Exception e) {
                Log.e("SyncActivityDetails", "syncData Error=" + e.toString());
                DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                        "SyncActivityDetails", "syncData", e.toString());
            }

            if (strResponse != null && !strResponse.trim().equalsIgnoreCase("null") && !strResponse.trim().equalsIgnoreCase("")) {
                JSONObject objJsonObject = new JSONObject(strResponse);
                JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");
                for (int index = 0; index < objJsonArray.length(); index++) {
                    JSONObject jsonobject = objJsonArray.getJSONObject(index);
                    if (jsonobject.has("status")) {
                        DatabaseHelper.UpdateActivityDetails(getApplicationContext(), DatabaseHelper.db,
                                jsonobject.getString("RecId"), jsonobject.getString("status"));
                    } else {
                        DatabaseHelper.UpdateActivityDetails(getApplicationContext(), DatabaseHelper.db,
                                jsonobject.getString("RecId"), "Pending");
                    }
                }
            }
        } catch (Exception e) {
            Log.e("SyncActivityDetails", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncActivityDetails", "Async>doInBackgraund", e.toString());
            onDestroy();
        }
    }

    protected void syncQueryData() {
        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    getResources().getString(R.string.Exam_NAMESPACE), getResources().getString(R.string.syncContentQueries));

            //property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            inputPI1.setName("objXMLDoc");
            String objXMLDoc = CommonClass.GenerateXML(getApplicationContext(), XMLElementIDs.size(), "SubmitContentQueries",
                    XMLElementIDs.toArray(new String[XMLElementIDs.size()]),
                    XMLElementValues.toArray(new String[XMLElementValues.size()]));
            inputPI1.setValue(objXMLDoc);
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(getResources().getString(R.string.Exam_URL));
            LPIEEX509TrustManager.allowAllSSL();
            androidHttpTransport.call(getResources().getString(R.string.Exam_SOAP_ACTION) +
                    getResources().getString(R.string.syncContentQueries), envelope);
            //Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            strResponse = response.toString();
            if (strResponse != null && !strResponse.trim().equalsIgnoreCase("null") && !strResponse.trim().equalsIgnoreCase("")) {

                JSONObject objJsonObject = new JSONObject(strResponse);
                JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");

                for (int index = 0; index < objJsonArray.length(); index++) {
                    JSONObject jsonobject = objJsonArray.getJSONObject(index);
                    if (jsonobject.has("Status")) {
                        DatabaseHelper.UpdtContentQueries(getApplicationContext(), DatabaseHelper.db,
                                "" + jsonobject.getInt("RecId"), jsonobject.getString("UserId"),
                                jsonobject.getString("Status"));
                    }
                }
            }
        } catch (Exception e) {
            Log.e("SubmitContentQueries", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SubmitContentQueries", "Async>doInBackgraund", e.toString());
            onDestroy();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("SyncActivityDetails", "SyncActivityDetails onDestroy");
    }
}
