package com.lpi.kmi.lpi.Services;

import android.annotation.TargetApi;
import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import androidx.annotation.Nullable;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.stringcare.library.SC;

import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by mustafakachwalla on 10/08/17.
 */

public class SyncActivityMaster extends IntentService {
    String strResponse;

    public SyncActivityMaster() {
        super("SyncActivityMaster");
    }

    //    SQLiteDatabase db;
    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

//        SQLiteDatabase.loadLibs(getApplicationContext());
//        db = DatabaseHelper.getInstance(getApplicationContext()).
//                getWritableDatabase(getResources().getString(R.string.DBPassword));

        try {
            startForeground(CommonClass.getNotificationId(), CommonClass.createServiceNotification(this));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (DatabaseHelper.db == null) {
                SQLiteDatabase.loadLibs(getApplicationContext());
                DatabaseHelper.db = DatabaseHelper.getInstance(getApplicationContext()).
                        getWritableDatabase(SC.reveal(CommonClass.DBPassword));
            }

            Cursor cquery;
            cquery = DatabaseHelper.db.query(getResources().getString(R.string.TblActivityMaster), null,
                    null, null, null, null, null);
            if (cquery.getCount() > 0) {
                onDestroy();
            } else {
                syncData();
            }
            cquery.close();
        } catch (Exception e) {
            e.printStackTrace();
            onDestroy();
        }
    }

    private void syncData() {
        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    getResources().getString(R.string.Exam_NAMESPACE), getResources().getString(R.string.GetActivityMaster));
            //property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            inputPI1.setName("objXMLDoc");
            String objXMLDoc = CommonClass.GenerateXML(getApplicationContext(), 0, "GetActivityMaster", new String[]{""}, new String[]{""});
            inputPI1.setValue(objXMLDoc);
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);
            envelope.dotNet = true;
            //Set output SOAP object
            envelope.setOutputSoapObject(request);
            //Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(getResources().getString(R.string.Exam_URL));
//        InputStream isResponse = null;

            //Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
            androidHttpTransport.call(getResources().getString(R.string.Exam_SOAP_ACTION) + getResources().getString(R.string.GetActivityMaster), envelope);
            //Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            strResponse = response.toString();

            JSONObject objJsonObject = new JSONObject(strResponse);
            JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");
            for (int index = 0; index < objJsonArray.length(); index++) {
                JSONObject jsonobject = objJsonArray.getJSONObject(index);
                DatabaseHelper.InsertActivityMaster(getApplicationContext(), DatabaseHelper.db,
                        jsonobject.getString("ActivityCode"),
                        jsonobject.getString("ActivityDesc"));
            }


        } catch (Exception e) {
            Log.e("SyncActivityMaster", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncActivityMaster", "onHandleIntent", e.toString());
            onDestroy();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("SyncActivityMaster", "SyncActivityMaster call onDestroy");
    }
}
