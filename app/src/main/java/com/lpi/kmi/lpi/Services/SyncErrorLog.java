package com.lpi.kmi.lpi.Services;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import androidx.annotation.Nullable;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.stringcare.library.SC;

import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by Bharat
 */

public class SyncErrorLog extends IntentService {
    String strResponse = "";
    DatabaseHelper DBHelper = null;
    String[] reqName = null;
    String[] reqValue = null;

    //    SQLiteDatabase db;
    public SyncErrorLog() {
        super("Errorlog");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
            startForeground(CommonClass.getNotificationId(), CommonClass.createServiceNotification(this));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (DatabaseHelper.db == null) {
                SQLiteDatabase.loadLibs(getApplicationContext());
                DatabaseHelper.db = DatabaseHelper.getInstance(getApplicationContext()).
                        getWritableDatabase(SC.reveal(CommonClass.DBPassword));
            }
            Cursor mCursor = DatabaseHelper.db.query("" + getApplicationContext().getString(R.string.TBL_LPIErrorLog), null, null, null, null, null, null);
            if (mCursor.getCount() > 0) {
                syncData();
            }
            mCursor.close();
        } catch (Exception e) {
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncErrorLog", "onHandleIntent()", e.toString());
            onDestroy();
        }
    }


    private void syncData() {
        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    getResources().getString(R.string.Exam_NAMESPACE), getResources().getString(R.string.setErrorLogMob));
            //property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            reqName = null;
            reqValue = null;

            Cursor mCursor = DatabaseHelper.db.query(getApplicationContext().getString(R.string.TBL_LPIErrorLog), null, null, null, null, null, null);
            if (mCursor != null) {
                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    //get req array
                    boolean isData = getRequestData(mCursor);
                    if (isData) {
                        inputPI1.setName("objXMLDoc");
                        try {
                            String objXMLDoc = CommonClass.GenerateXML(getApplicationContext(), reqName.length, "setErrorLogMob", reqName, reqValue);
                            inputPI1.setValue(objXMLDoc);
                        } catch (Exception e1) {
                            //TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                        inputPI1.setType(String.class);
                        request.addProperty(inputPI1);

                        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                        envelope.dotNet = true;
                        //Set output SOAP object
                        envelope.setOutputSoapObject(request);
                        HttpTransportSE androidHttpTransport = new HttpTransportSE(getResources().getString(R.string.Exam_URL));
                        try {
                            //Involve Web service
                            LPIEEX509TrustManager.allowAllSSL();
                            androidHttpTransport.call(getResources().getString(R.string.Exam_SOAP_ACTION) + getResources().getString(R.string.setErrorLogMob), envelope);
                            //Get the response
                            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                            strResponse = response.toString();
//                            Log.e("SyncErrorLog", "sstrResponse=" + strResponse);
                        } catch (Exception e) {
                            Log.e("SyncErrorLog", "Error=" + e.toString());
                            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                                    "SyncErrorLog", "syncData()", e.toString());
                            onDestroy();
                        }
                        try {
                            //{"Table":[{"Result":"Success","RecId":"2"}]}
                            if (strResponse != null && strResponse.length() > 0) {
                                JSONObject jsonResult = new JSONObject(strResponse);
                                JSONArray jsonArray = jsonResult.getJSONArray("Table");
                                JSONObject jsonObjectct = jsonArray.getJSONObject(0);
                                String result = (jsonObjectct.getString("Result")).trim();
                                if (result != null && result.equalsIgnoreCase("Success")) {
                                    DatabaseHelper.db.delete(getApplicationContext().getString(R.string.TBL_LPIErrorLog), "RecId=?", new String[]{"" + mCursor.getString(0)});
                                    Log.e("Deleted Error ID: ", "" + mCursor.getString(0));
                                }
                            }
                        } catch (Exception e) {
                            Log.e("syncData", "roor=" + e.toString());
                            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                                    "SyncErrorLog", "syncData()", e.toString());
                        }
                    }
                }
            }
            mCursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("SyncErrorLog", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncErrorLog", "syncData()", e.toString());
            onDestroy();
        }
    }

    private boolean getRequestData(Cursor mCursor) {
        try {
            reqName = new String[15];
            reqValue = new String[15];

            reqName[0] = ("LogDtime");
            reqValue[0] = (mCursor.getString(1));

            reqName[1] = ("AppName");
            reqValue[1] = (mCursor.getString(2));

            reqName[2] = ("PageName");
            reqValue[2] = (mCursor.getString(3));

            reqName[3] = ("FuncName");
            reqValue[3] = (mCursor.getString(4));

            reqName[4] = ("ErrDesc");
            reqValue[4] = (mCursor.getString(5));

            reqName[5] = ("UserId");
            reqValue[5] = (/*mCursor.getString(6)*/"wefeg");

            reqName[6] = ("Severity");
            reqValue[6] = (mCursor.getString(7));


            reqName[7] = ("DeviceID");
            reqValue[7] = (mCursor.getString(8));

            reqName[8] = ("IMEINo");
            reqValue[8] = (mCursor.getString(9));

            reqName[9] = ("BrandName");
            reqValue[9] = (mCursor.getString(10));

            reqName[10] = ("ModelName");
            reqValue[10] = (mCursor.getString(11));

            reqName[11] = ("DeviceAPILevel");
            reqValue[11] = (mCursor.getString(12));

            reqName[12] = ("OSName");
            reqValue[12] = (mCursor.getString(13));

            reqName[13] = ("OSVersion");
            reqValue[13] = (mCursor.getString(14));

            reqName[14] = ("UniqRefID");
            reqValue[14] = (mCursor.getString(0));

            return true;
        } catch (Exception e) {
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncErrorLog", "getRequestData()", e.toString());
            return false;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
