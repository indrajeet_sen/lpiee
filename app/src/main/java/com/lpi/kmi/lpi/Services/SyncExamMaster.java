package com.lpi.kmi.lpi.Services;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.GetCandidateAttemptedExamsDetails;
import com.lpi.kmi.lpi.activities.EmotionalExcellenceListActivity;
import com.lpi.kmi.lpi.activities.LPIPracticeLessonActivity;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.stringcare.library.SC;

import net.sqlcipher.database.SQLiteDatabase;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by mustafakachwalla on 08/06/17.
 */

public class SyncExamMaster extends IntentService {

    String strResponse, ExamID, ExamType;//strLoginId,strUserType="C",strUserName;

    public SyncExamMaster() {
        super("SyncExamMaster");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    //    SQLiteDatabase db;
    @SuppressLint("Range")
    @Override
    protected void onHandleIntent(Intent intent) {

        Log.e("", "SyncExamMaster Started");

        try {
            startForeground(CommonClass.getNotificationId(), CommonClass.createServiceNotification(this));
        } catch (Exception e) {
            e.printStackTrace();
        }

        strResponse = "";
        ExamID = "";

        if (DatabaseHelper.db == null) {
            SQLiteDatabase.loadLibs(getApplicationContext());
            DatabaseHelper.db = DatabaseHelper.getInstance(getApplicationContext()).
                    getWritableDatabase(SC.reveal(CommonClass.DBPassword));
        }

        Cursor mCursor = DatabaseHelper.Get_PINwithoutID(getApplicationContext(), DatabaseHelper.db);
        if (mCursor.getCount() > 0) {
            for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                StaticVariables.UserName = mCursor.getString(mCursor.getColumnIndex("FirstName"));
                StaticVariables.UserLoginId = mCursor.getString(mCursor.getColumnIndex("CandidateLoginId"));
                StaticVariables.ParentId = mCursor.getString(mCursor.getColumnIndex("ParentId"));
                StaticVariables.ParentName = mCursor.getString(mCursor.getColumnIndex("ParentName"));
                StaticVariables.UserMobileNo = mCursor.getString(mCursor.getColumnIndex("CandidateMobileNo1"));
                StaticVariables.UserType = mCursor.getString(mCursor.getColumnIndex("UserType"));
            }
        }
        mCursor.close();
        GetExamMaster();
    }

    @SuppressLint("NewApi")
    public void GetExamMaster() {
        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    getApplicationContext().getString(R.string.Exam_NAMESPACE),
                    getApplicationContext().getString(R.string.GetAssignedExamSetForUser));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();

            inputPI1.setName("objXMLDoc");
            String objXMLDoc = CommonClass.GenerateXML(getApplicationContext(), 1, "ExamSetRequest", new String[]{"LoginID"},
                    new String[]{StaticVariables.UserLoginId});
            inputPI1.setValue(objXMLDoc);
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            // Set output SOAP object
            envelope.setOutputSoapObject(request);
            // Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(getApplicationContext().getString(R.string.Exam_URL));
            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
            androidHttpTransport.call(getApplicationContext().getString(R.string.Exam_SOAP_ACTION) +
                    getApplicationContext().getString(R.string.GetAssignedExamSetForUser), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            // Assign it to static variable
            strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString();
//            Log.e("", "response=" + strResponse);

            InputStream isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            InputStream is = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            if (IsCorrectXMLResponse(isResponse)) {
                NodeList nList = getExamDatafrmXML(is, "Exam");
                for (int i = 0; i < nList.getLength(); i++) {

                    Node node = nList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;
                        int opr = DatabaseHelper.SaveExamMaster(getApplicationContext(), DatabaseHelper.db, getValue("ExamID", element),
                                getValue("ExamCode", element), getValue("ExamType", element), getValue("ExamDesc", element),
                                getValue("ExamDetails", element), getValue("AllowNavigationFlag", element), getValue("OptionCount", element),
                                getValue("ShowRandomQuestions", element), getValue("TotalQuestions", element), getValue("TotalTimeAllowedInMinutes", element),
                                getValue("IsAllowResultFeedback", element), getValue("IsPracticeExam", element),
                                getValue("AllowedAttempt", element), getValue("EffectiveDate", element),
                                getValue("IsAllQuesMendatory", element), getValue("NoOfAttempts", element));
                        if (opr == 1) {
                            Intent intent;
                            if (getValue("ExamID", element).equals("6001")) {
                                intent = new Intent(getApplicationContext(), LPIPracticeLessonActivity.class);
                                intent.putExtra("ExamID", getValue("ExamID", element));
                                intent.putExtra("LoginId", StaticVariables.UserLoginId);
                                intent.putExtra("UserType", StaticVariables.UserType);
                            } else {
                                intent = new Intent(getApplicationContext(), EmotionalExcellenceListActivity.class);
                                intent.putExtra("LoginId", StaticVariables.UserType);
                                intent.putExtra("UserType", StaticVariables.UserType);
                            }
//                            if (!getValue("ExamDesc", element).equals("Feedback")
//                                    && StaticVariables.Notification.equals("Yes"))
//                                CommonClass.createNotification(getApplicationContext(), "Training Manager",
//                                        getValue("ExamDesc", element) + "\n"
//                                                + getValue("ExamDetails", element), intent);
                        }

                        ExamID = getValue("ExamID", element);
                        ExamType = getValue("ExamType", element);

                    }

                    if (!DatabaseHelper.CheckExamQueCount(getApplicationContext(), DatabaseHelper.db, BigInteger.valueOf(Long.valueOf(ExamID)))) {
                        GetQueMaster(ExamID, ExamType);
                    }
                    new GetCandidateAttemptedExamsDetails(getApplicationContext(), ExamID, StaticVariables.UserLoginId).execute();
                }
            }
        } catch (Exception e) {
            Log.e("", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncExamMaster", "IntentService > GetExamMaster", e.toString());
        }
    }

    @SuppressLint("NewApi")
    public void GetQueMaster(String ExamId, String ExamType) {
        try {

            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    getApplicationContext().getString(R.string.Exam_NAMESPACE),
                    getApplicationContext().getString(R.string.GetQuestionSetForExam));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();

            inputPI1.setName("objXMLDoc");
            String objXMLDoc = CommonClass.GenerateXML(getApplicationContext(), 1, "QuestionSetRequest", new String[]{"ExamID"}, new String[]{ExamId});
            inputPI1.setValue(objXMLDoc);
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            // Set output SOAP object
            envelope.setOutputSoapObject(request);
            // Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(getApplicationContext().getString(R.string.Exam_URL));


            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
            androidHttpTransport.call(getApplicationContext().getString(R.string.Exam_SOAP_ACTION) +
                    getApplicationContext().getString(R.string.GetQuestionSetForExam), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            // Assign it to static variable
            strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString();
//            Log.e("", "response=" + strResponse);

            InputStream isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            InputStream is = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            if (IsCorrectXMLResponse(isResponse)) {

                NodeList nList = getExamDatafrmXML(is, "Questions");
                for (int i = 0; i < nList.getLength(); i++) {

                    Node node = nList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;
                        int opr = DatabaseHelper.SaveQuestionMaster(getApplicationContext(), DatabaseHelper.db,
                                ExamId, getValue("QuestionId", element),
                                getValue("QuestionCode", element), getValue("Question", element),
                                getValue("NoOfAttemptAllow", element), getValue("CorrectAnsRemark", element),
                                getValue("WrongAnsRemark", element), getValue("OptionCount", element));
//                        GetAnsMaster(ExamId);

                        if (opr == 1 && ExamType.equals("5") && !ExamId.equals("5001") && StaticVariables.Notification.equals("Yes")) {
                            Intent intent = new Intent(getApplicationContext(), EmotionalExcellenceListActivity.class);
                            intent.putExtra("LoginId", StaticVariables.UserType);
                            intent.putExtra("UserType", StaticVariables.UserType);
//                            CommonClass.createNotification(getApplicationContext(), "Training Manager",
//                                    getValue("Question", element), intent);

                        }
                    }

                }
                GetAnsMaster(ExamId);
            }

        } catch (Exception e) {
            Log.e("", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncExamMaster", "IntentService > GetQueMaster", e.toString());
        }
    }

    @SuppressLint("NewApi")
    public void GetAnsMaster(String ExamId) {
        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    getApplicationContext().getString(R.string.Exam_NAMESPACE),
                    getApplicationContext().getString(R.string.GetAnswerSetExam));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();

            inputPI1.setName("objXMLDoc");
            String objXMLDoc = CommonClass.GenerateXML(getApplicationContext(), 1, "AnswerSetRequest", new String[]{"ExamID"}, new String[]{ExamId});
            inputPI1.setValue(objXMLDoc);
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            // Set output SOAP object
            envelope.setOutputSoapObject(request);
            // Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(getApplicationContext().getString(R.string.Exam_URL));


            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
            androidHttpTransport.call(getApplicationContext().getString(R.string.Exam_SOAP_ACTION) +
                    getApplicationContext().getString(R.string.GetAnswerSetExam), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            // Assign it to static variable
            strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString();
//            Log.e("", "response=" + strResponse);


            InputStream isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            InputStream is = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
//            if (IsCorrectXMLResponse(isResponse)) {

            String QueId = "";
            NodeList nList = getExamDatafrmXML(is, "Answers");
//                NodeList nList = getExamDatafrmXML(is, "Questions");
            for (int i = 0; i < nList.getLength(); i++) {

                Node node = nList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    QueId = getValue("QuestionId", element);
                    DatabaseHelper.SaveAnswerMaster(getApplicationContext(), DatabaseHelper.db,
                            ExamId, QueId, getValue("AnswerId", element),
                            getValue("AnswerCode", element), getValue("AnswerDesc", element));
                }
            }
            GetCorrectQueAnsMapping(ExamId);

//            }
        } catch (Exception e) {
            Log.e("", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncExamMaster", "IntentService > GetAnsMaster", e.toString());
        }
    }

    @SuppressLint("NewApi")
    public void GetCorrectQueAnsMapping(String ExamId) {

        try {

            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    getApplicationContext().getString(R.string.Exam_NAMESPACE),
                    getApplicationContext().getString(R.string.GetCurrectQueAnsSet));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();

            inputPI1.setName("objXMLDoc");
            String objXMLDoc = CommonClass.GenerateXML(getApplicationContext(), 1, "CorrectQueAnsSet", new String[]{"ExamID"}, new String[]{ExamId});
            inputPI1.setValue(objXMLDoc);
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            // Set output SOAP object
            envelope.setOutputSoapObject(request);
            // Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(getApplicationContext().getString(R.string.Exam_URL));


            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
            androidHttpTransport.call(getApplicationContext().getString(R.string.Exam_SOAP_ACTION) +
                    getApplicationContext().getString(R.string.GetCurrectQueAnsSet), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            // Assign it to static variable
            strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString();
//            Log.e("", "response=" + strResponse);

            InputStream isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            InputStream is = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            if (IsCorrectXMLResponse(isResponse)) {
                String QueId = "";
                NodeList nList = getExamDatafrmXML(is, "QuesAnsSet");
                for (int i = 0; i < nList.getLength(); i++) {
                    Node node = nList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;
                        QueId = getValue("QuestionId", element);
                        DatabaseHelper.SaveCorctQueAnsMapping(getApplicationContext(), DatabaseHelper.db,
                                ExamId, QueId, getValue("QuestionCode", element),
                                getValue("AnswerId", element), getValue("AnswerCode", element));
                    }
                }
                DatabaseHelper.SaveExamQueAns_SyncMaster(getApplicationContext(), DatabaseHelper.db, ExamId, QueId);
            }

//            Cursor cquery;
//            cquery = DatabaseHelper.db.query(getApplicationContext().getResources().getString(R.string.TBL_LPIQuestionCorrectAnswerMapping), null,
//                    null, null, null, null, null);
//            int cntr = cquery.getCount();
//            Log.e("", "Counter=" + cntr);

//            }
        } catch (Exception e) {
            Log.e("", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncExamMaster", "IntentService > GetCorrectQueAnsMapping", e.toString());
        }
    }

    // TODO: Get the List of Node from XML
    public NodeList getExamDatafrmXML(InputStream strXML, String NodeListItem) {
        NodeList nList = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(strXML);
            Element element = doc.getDocumentElement();
            element.normalize();
            nList = doc.getElementsByTagName(NodeListItem);
        } catch (Exception e) {
            Log.e("", "error=" + e.toString());
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncExamMaster", "getExamDatafrmXML()", e.toString());
        }

        return nList;
    }

    // TODO: Check the Response Code
    public boolean IsCorrectXMLResponse(InputStream strXML) {
        boolean isZeroResponseCode = false;
        try {
            if (strXML != null || !strXML.equals(null)) {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(strXML);
                Element element = doc.getDocumentElement();
                element.normalize();
                String ResponseCode = getValue("ResponseCode", element);
                if (ResponseCode.equals("0")) {
                    isZeroResponseCode = true;
                } else {
                    isZeroResponseCode = false;
                }
            } else {
                isZeroResponseCode = false;
            }
        } catch (Exception e) {
            Log.e("", "error=" + e.toString());
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncExamMaster", "IsCorrectXMLResponse()", e.toString());
            isZeroResponseCode = false;
        }
        return isZeroResponseCode;
    }

    // TODO: Get the Value of Element
    public String getValue(String tag, Element element) {
        String value = "";
        try {
            if (element != null) {
                if (element.hasChildNodes()) {
                    NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
                    Node node = nodeList.item(0);
                    if (node != null)
                        value = node.getNodeValue();
                } else {
                    value = "";
                }
            } else {
                value = "";
            }
        } catch (Exception e) {
            Log.e("", "error=" + e.toString());
//            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
//                    "SyncExamMaster", "getValue() >> " + tag, e.toString());
            value = "";
        }
        return value;
    }
}
