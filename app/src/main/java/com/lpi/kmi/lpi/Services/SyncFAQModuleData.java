package com.lpi.kmi.lpi.Services;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.stringcare.library.SC;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import nu.xom.Serializer;


public class SyncFAQModuleData extends IntentService {
    private boolean isRunning;
    private static String CandidateId, TrainerId, InscType, UserType;
    Cursor cursor;

    public SyncFAQModuleData() {
        super("SyncFAQModuleData");

    }

    @Override
    public void onCreate() {
        super.onCreate();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            Service.startForeground(0, new Notification());
//        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            startForeground(CommonClass.getNotificationId(), CommonClass.createServiceNotification(this));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Log.d("SyncFAQModuleData", "Service Started");
            if (DatabaseHelper.db == null) {
                SQLiteDatabase.loadLibs(getApplicationContext());
                DatabaseHelper.db = DatabaseHelper.getInstance(getApplicationContext()).
                        getWritableDatabase(SC.reveal(CommonClass.DBPassword));
            }

            cursor = DatabaseHelper.db.query(getApplicationContext().getResources().getString(R.string.TBL_iCandidate),
                    new String[]{"CandidateLoginId", "ParentId", "InscType", "UserType"},
                    null, null, null, null, null);
            if (cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    if (cursor.getString(3).equals("T")) {
                        CandidateId = "";
                        TrainerId = cursor.getString(0);
                    } else {
                        CandidateId = cursor.getString(0);
                        TrainerId = cursor.getString(1);
                    }
                    InscType = cursor.getString(2);
                    UserType = cursor.getString(3);
                }
            }
            cursor.close();

            if (CandidateId != null || !CandidateId.equals("") || !CandidateId.equalsIgnoreCase("")) {
                if (CommonClass.isConnected(getApplicationContext())) {
                    SubmitFAQTopics();
                }

                if (CommonClass.isConnected(getApplicationContext())) {
                    getFAQFromServer();
                }

//                if (CommonClass.isConnected(getApplicationContext())){
//                    getFAQConversationFromServer();
//                }

                if (CommonClass.isConnected(getApplicationContext())) {
                    SubmitConversation();
                }


            }

        } catch (Exception e) {
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncFAQModuleData", "onHandleIntent", e.toString());
            onDestroy();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @SuppressLint("Range")
    private void SubmitFAQTopics() {
        String strResponse = "";
        try {
            Cursor cursor = DatabaseHelper.db.query(getApplicationContext().getResources().getString(R.string.TBL_FAQ_List),
                    null, "CandidateId=? and SyncStatus=?",
                    new String[]{CandidateId, "Pending"}, null, null, null, null);
            if (cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToFirst()) {
                    org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                            getApplicationContext().getResources().getString(R.string.Exam_NAMESPACE),
                            getApplicationContext().getResources().getString(R.string.SubmitFAQTopic));
                    //property which holds input parameters
                    PropertyInfo inputPI1 = new PropertyInfo();
                    inputPI1.setName("objXMLDoc");
                    inputPI1.setValue(CommonClass.FAQXML(getApplicationContext(), cursor.getString(cursor.getColumnIndex("QuesDesc")),
                            cursor.getString(cursor.getColumnIndex("MobRefId"))));
                    inputPI1.setType(String.class);
                    request.addProperty(inputPI1);

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    //Set output SOAP object
                    envelope.setOutputSoapObject(request);
                    //Create HTTP call object
                    HttpTransportSE androidHttpTransport = new HttpTransportSE(getApplicationContext().getResources().getString(R.string.Exam_URL));
                    //Involve Web service
                    LPIEEX509TrustManager.allowAllSSL();
                    androidHttpTransport.call(getApplicationContext().getResources().getString(R.string.Exam_SOAP_ACTION) +
                            getApplicationContext().getResources().getString(R.string.SubmitFAQTopic), envelope);
                    //Get the response
                    SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                    strResponse = response.toString();

                    JSONObject objJsonObject = new JSONObject(strResponse);
                    JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");
                    for (int index = 0; index < objJsonArray.length(); index++) {
                        JSONObject jsonobject = objJsonArray.getJSONObject(index);
                        if (jsonobject.has("ResponseCode") && jsonobject.getString("ResponseCode").equals("0")) {
                            DatabaseHelper.UpdateFAQList(getApplicationContext(), DatabaseHelper.db,
                                    StaticVariables.UserLoginId, jsonobject.getString("MobRefId"));
//                            CommonClass.createNotification(getApplicationContext(), "Request Submitted", "Your FAQ topic is submitted successfully", "");
                        } else {
                            DatabaseHelper.UpdtContentQueries(getApplicationContext(), DatabaseHelper.db,
                                    "" + jsonobject.getInt("MobRefId"), StaticVariables.UserLoginId,
                                    jsonobject.getString("Pending"));
                        }
                    }
                }
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncFAQModuleData", "SubmitFAQTopics", e.toString());
        }
    }

    private void getFAQFromServer() {
        String strResp = "";
        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    getApplicationContext().getString(R.string.Exam_NAMESPACE),
                    getApplicationContext().getString(R.string.FAQ));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            String strReqXML = FAQListReqBuild();
            inputPI1.setName("objXMLDoc");
            inputPI1.setValue(strReqXML);
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(getApplicationContext().getString(R.string.Exam_URL));


            LPIEEX509TrustManager.allowAllSSL();
            androidHttpTransport.call(getApplicationContext().getString(R.string.Exam_SOAP_ACTION) +
                    getApplicationContext().getString(R.string.FAQ), envelope);
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            strResp = response.toString();

            byte[] imageBytes = null;
            JSONObject result = new JSONObject(strResp);
            String strJsonArray = result.getJSONArray("Table").toString();
            JSONArray jsonArray = new JSONArray(strJsonArray);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                if (jsonObject.getString("ResponseCode").equals("0")) {
                    if (jsonObject.getString("Images") == null || jsonObject.getString("Images").equals("")) {
                        try {
                            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.profile_blank_m);
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                            imageBytes = stream.toByteArray();
                            stream.flush();
                            stream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        imageBytes = Base64.decode(jsonObject.getString("Images"), Base64.NO_WRAP);
                    }
                    DatabaseHelper.SaveFAQList(getApplicationContext(), DatabaseHelper.db, (jsonObject.getString("CandidateId")).trim(),
                            (jsonObject.getString("QuesId")).trim(), (jsonObject.getString("TrainerId")).trim(),
                            (jsonObject.getString("QuesDesc")).trim(), (jsonObject.getString("RespDesc")).trim(),
                            (jsonObject.getString("RespDtime")).trim(), (jsonObject.getString("RespFrom")).trim(),
                            (jsonObject.getString("isClosed")).trim(), (jsonObject.getString("Name")).trim(),
                            (jsonObject.getString("Mobile")).trim(), imageBytes, (jsonObject.getString("RN")).trim(),
                            (jsonObject.getString("MobRefId")).trim(), "" + (jsonObject.get("RespDtime")), "Complete");
                }
            }
//            CommonClass.createNotification(getApplicationContext(), "Checking for queries", "Checking for new queries", "");
        } catch (Exception e) {
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncFAQModuleData", "getFAQFromServer", e.toString());
        }
    }

    private String FAQListReqBuild() {

        String str_XML = "";
        try {
            nu.xom.Element root = new nu.xom.Element("GetFAQList");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(getApplicationContext()));

            nu.xom.Element InscType = new nu.xom.Element("InscType");
            InscType.appendChild("");

            nu.xom.Element ParentId = new nu.xom.Element("ParentId");
            ParentId.appendChild("" + SyncFAQModuleData.TrainerId);

            nu.xom.Element ChildId = new nu.xom.Element("ChildId");
            ChildId.appendChild("" + SyncFAQModuleData.CandidateId);

            nu.xom.Element UserType = new nu.xom.Element("UserType");
            UserType.appendChild("" + SyncFAQModuleData.UserType);

            nu.xom.Element ListType = new nu.xom.Element("ListType");
            ListType.appendChild("");

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
            root.appendChild(InscType);
            root.appendChild(ParentId);
            root.appendChild(ChildId);
            root.appendChild(UserType);
            root.appendChild(ListType);

//            nu.xom.Element UserId = new nu.xom.Element("UserId");
//            UserId.appendChild(StaticVariables.UserLoginId);
////            UserId.appendChild("70011354");
//            root.appendChild(UserId);

            nu.xom.Document doc = new nu.xom.Document(root);

            OutputStream out = new ByteArrayOutputStream();
            Serializer serializer = new Serializer(System.out, "UTF-8");
            serializer.setOutputStream(out);
            serializer.setIndent(4);
            serializer.setMaxLength(64);
            serializer.write(doc);
            str_XML = out.toString();
            str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
            str_XML = str_XML.replaceAll("\n", "");
            str_XML = str_XML.replaceAll("\r", "");
            str_XML = str_XML.replaceAll("     ", "");
            str_XML = str_XML.replaceAll("         ", "");
            str_XML = str_XML.trim();
//            Log.e("", "XMLStr==" + str_XML);
        } catch (IOException e) {
            Log.e("", "SyncFAQModuleData Error=" + e.toString());
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncFAQModuleData", "FAQListReqBuild", e.toString());
        }
        return str_XML;
    }

    private void getFAQConversationFromServer() {
        String strResp = "";
        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    getApplicationContext().getString(R.string.Exam_NAMESPACE),
                    getApplicationContext().getString(R.string.GetConversation));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            String strReqXML = reqBuild();
            inputPI1.setName("objXMLDoc");
            inputPI1.setValue(strReqXML);
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(getApplicationContext().getString(R.string.Exam_URL));

            LPIEEX509TrustManager.allowAllSSL();
            androidHttpTransport.call(getApplicationContext().getString(R.string.Exam_SOAP_ACTION) +
                    getApplicationContext().getString(R.string.GetConversation), envelope);
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            strResp = response.toString();


            JSONObject result = new JSONObject(strResp);
            String strJsonArray = result.getJSONArray("Table").toString();
            JSONArray jsonArray = new JSONArray(strJsonArray);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                if (jsonObject.has("ResponseCode") && jsonObject.getString("ResponseCode").equals("1")) {

                } else if (jsonObject.has("ResponseCode") && jsonObject.getString("ResponseCode").equals("0")) {
                    if (!DatabaseHelper.isMessageAvailable(getApplicationContext(), DatabaseHelper.db, jsonObject.getString("MobRefId"))) {

                        if (CandidateId != jsonObject.getString("RespFrom")
                                && (jsonObject.getString("RespDesc") != null
                                || !jsonObject.getString("RespDesc").equals(""))) {
                            if (UserType.equals("C")) {
                                if (jsonObject.getString("isAttachment").equalsIgnoreCase("Y")) {
                                    CommonClass.createNotification(getApplicationContext(),
                                            jsonObject.getString("TrainerName"), jsonObject.getString("mFileName"), "");
                                } else {
                                    CommonClass.createNotification(getApplicationContext(),
                                            jsonObject.getString("TrainerName"), jsonObject.getString("RespDesc"), "");
                                }

                            } else {
                                if (jsonObject.getString("isAttachment").equalsIgnoreCase("Y")) {
                                    CommonClass.createNotification(getApplicationContext(),
                                            jsonObject.getString("CandidateName"), jsonObject.getString("mFileName"), "");
                                } else {
                                    CommonClass.createNotification(getApplicationContext(),
                                            jsonObject.getString("CandidateName"), jsonObject.getString("RespDesc"), "");
                                }
                            }

                        }
                    }
                    DatabaseHelper.SaveConversation(getApplicationContext(), DatabaseHelper.db,
                            jsonObject.getString("isClosed"), jsonObject.getString("RespDesc"),
                            jsonObject.getString("TrainerId"), jsonObject.getString("CandidateId"),
                            jsonObject.getString("QuesDesc"), jsonObject.getString("RespFrom"),
                            jsonObject.getString("RespDtime").toString(), jsonObject.getString("QuesId"),
                            jsonObject.getString("MobRefId"),
                            /*jsonObject.getString("MsgType"), jsonObject.getString("DocLocation")*/"", "",
                            "success", jsonObject.getString("RespStatus"), "Y",
                            jsonObject.getString("isAttachment"), "", jsonObject.getString("mFileName"),
                            jsonObject.getString("mFilePath"), jsonObject.getString("mFileType"));

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncFAQModuleData", "getFAQConversationFromServer", e.toString());
        }
    }

    @SuppressLint("Range")
    private void SubmitConversation() {
        String strResp = "";
        String strData = "";
        int timeout = 30000;
        Cursor mCursor = null;
        try {
            mCursor = DatabaseHelper.db.query(getApplicationContext().getResources().getString(R.string.TBL_FAQ_Conversation),
                    null, "SyncStatus=?",
                    new String[]{"pending"}, null, null, null, null);
            if (mCursor.getCount() > 0) {
                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                            getApplicationContext().getString(R.string.Exam_NAMESPACE),
                            getApplicationContext().getString(R.string.SubmitConversation));

                    if (!mCursor.getString(mCursor.getColumnIndex("mFilePath")).equals("")) {
                        byte[] inputData = getBytes(mCursor.getString(mCursor.getColumnIndex("mFilePath")));
                        strData = Base64.encodeToString(inputData, Base64.NO_WRAP);
                        timeout = inputData.length;
                    }
                    // property which holds input parameters
                    PropertyInfo inputPI1 = new PropertyInfo();
                    String strReqXML = reqSubmitBuild(mCursor);
                    inputPI1.setName("objXMLDoc");
                    inputPI1.setValue(strReqXML);
                    inputPI1.setType(String.class);
                    request.addProperty(inputPI1);

                    PropertyInfo inputPI2 = new PropertyInfo();
                    inputPI2.setName("inputData");
                    inputPI2.setValue(strData);
                    inputPI2.setType(byte.class);
                    request.addProperty(inputPI2);

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);
                    HttpTransportSE androidHttpTransport =
                            new HttpTransportSE(getApplicationContext().getString(R.string.Exam_URL), timeout);

                    LPIEEX509TrustManager.allowAllSSL();
                    androidHttpTransport.call(getApplicationContext().getString(R.string.Exam_SOAP_ACTION) +
                            getApplicationContext().getString(R.string.SubmitConversation), envelope);
                    SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                    strResp = response.toString();

                    JSONObject result = new JSONObject(strResp.toString());
                    if (result != null && !result.toString().equalsIgnoreCase("{}")
                            && !result.toString().equalsIgnoreCase("null")) {
                        String strJsonArray = result.getJSONArray("Table").toString();
                        JSONArray jsonArray = new JSONArray(strJsonArray);
                        byte[] imageBytes = null;//[{"Status":"Success","RecId":3,"RespFrom":"hello"}]
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                            String MobRefId = jsonObject.getString("MobRefId").toString();
                            String QuesId = jsonObject.getString("QuesId").toString();
                            String strTrainerID = jsonObject.getString("TrainerId").toString();
                            String strCandidateID = jsonObject.getString("CandidateId").toString();
                            ContentValues cv = new ContentValues();
                            cv.clear();
                            cv.put("SyncStatus", "Success");
                            int iRec = DatabaseHelper.db.update(getApplicationContext().getResources().getString(R.string.TBL_FAQ_Conversation), cv,
                                    "TrainerId=? and CandidateId=? and QuesId=? and MobRefId=? ", new String[]{strTrainerID,
                                            strCandidateID, QuesId, MobRefId});
                            Log.e("OfflineUpdated record: ", iRec + "");
                        }

                    }
                }
            }
            mCursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncFAQModuleData", "SubmitConversation", e.toString());
        }
    }

    public byte[] getBytes(String filePath) {
        byte[] bytes = null;
        try {
            File file = new File(filePath);
            int size = (int) file.length();
            bytes = new byte[size];
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (Exception e) {
            e.printStackTrace();
            bytes = null;
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncFAQModuleData", "getBytes", e.toString());
        }
        return bytes;
    }

    private String reqBuild() {

        String str_XML = "";
        try {
            nu.xom.Element root = new nu.xom.Element("GetConversation");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(getApplicationContext()));

            nu.xom.Element InscType = new nu.xom.Element("InscType");
            InscType.appendChild("" + SyncFAQModuleData.InscType);

            nu.xom.Element RespDtime = new nu.xom.Element("RespDtime");
            String strRespDtime = "";
            cursor = DatabaseHelper.db.query(getResources().getString(R.string.TBL_FAQ_Conversation),
                    new String[]{"max(cast(RespDtime as date))"},
                    "TrainerId<>?", new String[]{CandidateId}, null, null, null);
            if (cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    strRespDtime = cursor.getString(0);
                }
            }
            RespDtime.appendChild(strRespDtime);

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
            root.appendChild(InscType);
            root.appendChild(RespDtime);

            nu.xom.Element TrainerId = new nu.xom.Element("TrainerId");
            TrainerId.appendChild(SyncFAQModuleData.TrainerId);
            root.appendChild(TrainerId);
            nu.xom.Element CandidateId = new nu.xom.Element("CandidateId");
            CandidateId.appendChild(SyncFAQModuleData.CandidateId);
            root.appendChild(CandidateId);

            nu.xom.Element UserType = new nu.xom.Element("UserType");
            UserType.appendChild(SyncFAQModuleData.UserType);
            root.appendChild(UserType);

            nu.xom.Element QuesId = new nu.xom.Element("QuesId");
            QuesId.appendChild("");
            root.appendChild(QuesId);

            nu.xom.Document doc = new nu.xom.Document(root);

            OutputStream out = new ByteArrayOutputStream();
            Serializer serializer = new Serializer(System.out, "UTF-8");
            serializer.setOutputStream(out);
            serializer.setIndent(4);
            serializer.setMaxLength(64);
            serializer.write(doc);
            str_XML = out.toString();
            str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
            str_XML = str_XML.replaceAll("\n", "");
            str_XML = str_XML.replaceAll("\r", "");
            str_XML = str_XML.replaceAll("     ", "");
            str_XML = str_XML.replaceAll("         ", "");
            str_XML = str_XML.trim();
//            Log.e("", "XMLStr==" + str_XML);
        } catch (IOException e) {
            Log.e("", "getFAQFromServer Error=" + e.toString());
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "AsyncSearchCandidates", "getFAQFromServer", e.toString());
        }
        return str_XML;
    }

    @SuppressLint("Range")
    private String reqSubmitBuild(Cursor mCursor) {

        String str_XML = "";
        try {
            nu.xom.Element root = new nu.xom.Element("SubmitConversation");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(getApplicationContext()));

            nu.xom.Element InscType = new nu.xom.Element("InscType");
            InscType.appendChild("" + SyncFAQModuleData.InscType);

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
            root.appendChild(InscType);
//        if (mCursor.getCount() > 0) {
//            for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {

            nu.xom.Element TrainerId = new nu.xom.Element("TrainerId");
            TrainerId.appendChild(mCursor.getString(mCursor.getColumnIndex("TrainerId")));
            root.appendChild(TrainerId);
            nu.xom.Element CandidateId = new nu.xom.Element("CandidateId");
            CandidateId.appendChild(mCursor.getString(mCursor.getColumnIndex("CandidateId")));
            root.appendChild(CandidateId);
            nu.xom.Element QuesId = new nu.xom.Element("QuesId");
            QuesId.appendChild(mCursor.getString(mCursor.getColumnIndex("QuesId")));
            root.appendChild(QuesId);
            nu.xom.Element QuesDesc = new nu.xom.Element("QuesDesc");
            QuesDesc.appendChild(mCursor.getString(mCursor.getColumnIndex("QuesDesc")));
            root.appendChild(QuesDesc);
            nu.xom.Element RespDesc = new nu.xom.Element("RespDesc");
            RespDesc.appendChild(mCursor.getString(mCursor.getColumnIndex("RespDesc")));
            root.appendChild(RespDesc);
            nu.xom.Element RespDtime = new nu.xom.Element("RespDtime");
            RespDtime.appendChild(mCursor.getString(mCursor.getColumnIndex("RespDtime")));
            root.appendChild(RespDtime);
            nu.xom.Element RespFrom = new nu.xom.Element("RespFrom");
            RespFrom.appendChild(mCursor.getString(mCursor.getColumnIndex("MsgFrom")));
            root.appendChild(RespFrom);
            nu.xom.Element isClosed = new nu.xom.Element("isClosed");
            isClosed.appendChild(mCursor.getString(mCursor.getColumnIndex("isClosed")));
            root.appendChild(isClosed);
            nu.xom.Element CreatedBy = new nu.xom.Element("CreatedBy");
            CreatedBy.appendChild(mCursor.getString(mCursor.getColumnIndex("CreatedBy")));
            root.appendChild(CreatedBy);

            nu.xom.Element MobRefId = new nu.xom.Element("MobRefId");
            MobRefId.appendChild(mCursor.getString(mCursor.getColumnIndex("MobRefId")));
            root.appendChild(MobRefId);

            nu.xom.Element RespStatus = new nu.xom.Element("RespStatus");
            RespStatus.appendChild(mCursor.getString(mCursor.getColumnIndex("RespStatus")));
            root.appendChild(RespStatus);

            nu.xom.Element isAttachment = new nu.xom.Element("isAttachment");
            RespStatus.appendChild(mCursor.getString(mCursor.getColumnIndex("isAttachment")));
            root.appendChild(isAttachment);

            nu.xom.Element mFile = new nu.xom.Element("mFile");
            RespStatus.appendChild(mCursor.getString(mCursor.getColumnIndex("mFile")));
            root.appendChild(mFile);

            nu.xom.Element mFileName = new nu.xom.Element("mFileName");
            RespStatus.appendChild(mCursor.getString(mCursor.getColumnIndex("mFileName")));
            root.appendChild(mFileName);

            nu.xom.Element mFilePath = new nu.xom.Element("mFilePath");
            RespStatus.appendChild(mCursor.getString(mCursor.getColumnIndex("mFilePath")));
            root.appendChild(mFilePath);

            nu.xom.Element mFileType = new nu.xom.Element("mFileType");
            RespStatus.appendChild(mCursor.getString(mCursor.getColumnIndex("mFileType")));
            root.appendChild(mFileType);

//            }
//        }
            nu.xom.Document doc = new nu.xom.Document(root);

            OutputStream out = new ByteArrayOutputStream();
            Serializer serializer = new Serializer(System.out, "UTF-8");
            serializer.setOutputStream(out);
            serializer.setIndent(4);
            serializer.setMaxLength(64);
            serializer.write(doc);
            str_XML = out.toString();
            str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
            str_XML = str_XML.replaceAll("\n", "");
            str_XML = str_XML.replaceAll("\r", "");
            str_XML = str_XML.replaceAll("     ", "");
            str_XML = str_XML.replaceAll("         ", "");
            str_XML = str_XML.trim();
//            Log.e("", "XMLStr==" + str_XML);
        } catch (IOException e) {
            Log.e("", "getFAQFromServer Error=" + e.toString());
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncOffline", "getFAQFromServer", e.toString());
        }
        return str_XML;
    }
}
