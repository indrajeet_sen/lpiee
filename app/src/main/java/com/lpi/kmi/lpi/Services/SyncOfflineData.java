package com.lpi.kmi.lpi.Services;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import androidx.core.app.NotificationCompat;

import android.os.Build;
import android.util.Base64;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.LPI_ShareAppActivity;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.stringcare.library.SC;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import nu.xom.Serializer;

import static com.lpi.kmi.lpi.DBHelper.DatabaseHelper.db;

/**
 * Created by mustafakachwalla on 10/07/17.
 */

public class SyncOfflineData extends IntentService {
    static int mNotificationId = 000;
    String strResponse, ExamID, AttemptId, CandidateId;
    InputStream responseStream;
    NodeList nList = null;
    String notification = "";
    Context context;
    byte[] img_byteArray;

    public SyncOfflineData() {
        super("SyncOfflineData");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.e("", "Service Started");

        try {
            startForeground(CommonClass.getNotificationId(), CommonClass.createServiceNotification(this));
        } catch (Exception e) {
            e.printStackTrace();
        }

        strResponse = "";
        ExamID = "";
        AttemptId = "";
        CandidateId = "";

        try {
            if (DatabaseHelper.db == null) {
                SQLiteDatabase.loadLibs(getApplicationContext());
                DatabaseHelper.db = DatabaseHelper.getInstance(getApplicationContext()).
                        getWritableDatabase(SC.reveal(CommonClass.DBPassword));
            }

            CommonClass.checkSettings(context,"C");

            context = getApplicationContext();
//            syncContentQueries();

            sendCandidateDetailsToServer();

            Cursor cursor = DatabaseHelper.GetXMLData(getApplicationContext(), DatabaseHelper.db);

            if (cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
//                mNotificationId = mNotificationId +1;
                    SyncOfflineData(cursor.getString(2), cursor.getString(0), cursor.getString(1));
                }
            }
            cursor.close();

        } catch (Exception e) {
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncExamMaster", "onHandleIntent", e.toString());
            onDestroy();
        }
    }

    @SuppressLint("NewApi")
    public void SyncOfflineData(String method, String recId, String xmlData) {
        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    getApplicationContext().getString(R.string.Exam_NAMESPACE), method);

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();

            inputPI1.setName("objXMLDoc");
            inputPI1.setValue(xmlData);
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            // Set output SOAP object
            envelope.setOutputSoapObject(request);
            // Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(getApplicationContext().getString(R.string.Exam_URL));
            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
            androidHttpTransport.call(getApplicationContext().getString(R.string.Exam_SOAP_ACTION) +
                    method, envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString().trim();
//            Log.e("", "response=" + strResponse);
            InputStream isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            responseStream = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            if (IsCorrectXMLResponse(isResponse)) {
                if (method.equals(getApplicationContext().getString(R.string.SubmitExam))) {
                    nList = CommonClass.GetXMLElementNodeList(responseStream, "SubmitExam");
                    if (nList == null) {

                    } else if (nList.getLength() > 0) {
                        for (int i = 0; i < nList.getLength(); i++) {
                            Node node = nList.item(i);
                            notification = "";
                            if (node.getNodeType() == Node.ELEMENT_NODE) {
                                Element element = (Element) node;
                                if (getValue("ResponseCode", element).equals("0")) {
                                    ExamID = getValue("ExamId", element);
                                    AttemptId = getValue("AttemptId", element);
                                    CandidateId = getValue("CandidateId", element);
                                    if (getValue("ExamId", element).equals("4001")) {
                                        notification = "Psychometric profiling";
                                    } else if (getValue("ExamId", element).equals("3001")) {
                                        notification = "IRDAI Section-3 exam ";
                                    }
//                                    fireNotification(notification, "submitted successfully");
                                    if (StaticVariables.Notification.equals("Yes")) {
                                        CommonClass.createNotification(context, "Exam Submitted", "Exam submitted sucessfully", "");
                                    }
                                    DatabaseHelper.UpdateXMLData(getApplicationContext(), DatabaseHelper.db, recId, method, "Success");
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e("", "SyncOfflineData Error=" + e.toString());
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncOfflineData", "SyncOfflineData.doInBackground()", e.toString());
        }
    }
    @SuppressLint("Range")
    private void sendCandidateDetailsToServer() {
        try {
            Cursor mCursor = DatabaseHelper.getOfflineCandidateInfo(context, DatabaseHelper.db);
            if (mCursor.getCount() > 0) {
                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    String strResp = "",
                            CandidateId = mCursor.getString(mCursor.getColumnIndex("CandidateId")),
                            MobRefId = mCursor.getString(mCursor.getColumnIndex("MobRefId"));
                    org.ksoap2.serialization.SoapObject request = null;
                    String reqXML = "";
                    if (CandidateId.equalsIgnoreCase("")
                            || CandidateId.equals(null)) {
                        request = new org.ksoap2.serialization.SoapObject(
                                context.getString(R.string.Exam_NAMESPACE),
                                context.getString(R.string.RegisterCandidate));
                        reqXML = reqBuild(mCursor);
                    } else {
                        request = new org.ksoap2.serialization.SoapObject(
                                context.getString(R.string.Exam_NAMESPACE),
                                context.getString(R.string.ModifyCandidate));
                        reqXML = updateBuild(mCursor);
                    }
                    // property which holds input parameters
                    PropertyInfo inputPI1 = new PropertyInfo();


                    inputPI1.setName("objXMLDoc");
                    inputPI1.setValue(reqXML);
                    inputPI1.setType(String.class);
                    request.addProperty(inputPI1);
//                    Log.e("OfflineRegXMLDoc", reqXML);
                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);
                    HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));


                    LPIEEX509TrustManager.allowAllSSL();
                    if (CandidateId.equalsIgnoreCase("")
                            || CandidateId.equals(null)) {
                        androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                                context.getString(R.string.RegisterCandidate), envelope);
                    } else {
                        androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                                context.getString(R.string.ModifyCandidate), envelope);
                    }

                    SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                    strResp = response.toString();

                    JSONObject objJsonObject = new JSONObject(strResp);
                    JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");
                    ContentValues cv = new ContentValues();
                    for (int index = 0; index < objJsonArray.length(); index++) {
                        JSONObject jsonObject = objJsonArray.getJSONObject(index);
                        if (jsonObject != null && jsonObject.getString("RespCode").equalsIgnoreCase("Success")) {
                            cv.clear();

                            cv.put("SyncStatus", "Success");
                            if (jsonObject.has("AppLink")) {
                                cv.put("AppLink", jsonObject.getString("AppLink"));
                            }
                            if (jsonObject.has("message")) {
                                String AppLink = jsonObject.getString("message");
                            }
                            if (jsonObject.has("CandidateId")) {
                                cv.put("CandidateId", jsonObject.getString("CandidateId"));
                            }
                            if (jsonObject.has("MobRefId")) {
                                cv.put("MobRefId", jsonObject.getString("MobRefId"));
                            }
                            String strMessage = "";
                            if (CandidateId.equalsIgnoreCase("")
                                    || CandidateId.equals(null)) {
                                DatabaseHelper.db.update(context.getResources().getString(R.string.TBL_LPICandidateRegistration), cv,
                                        "MobRefId=?", new String[]{MobRefId});
//                                fireNotification(jsonObject.getString("CandidateId"), "Registration Successful");
                                strMessage = "Registration Successful";
                            } else {
                                DatabaseHelper.db.update(context.getResources().getString(R.string.TBL_LPICandidateRegistration), cv,
                                        "CandidateId=?", new String[]{jsonObject.getString("CandidateId")});
//                                fireNotification(jsonObject.getString("CandidateId"), "Modification Successful");
                                strMessage = "Modification Successful";
                            }
                            if (StaticVariables.Notification.equals("Yes")) {
                                notification(jsonObject.getString("CandidateId"), strMessage);
                            }
                        }
                    }

                }
            } else {
                mCursor.close();
            }

        } catch (Exception e) {
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncExamMaster", "sendCandidateDetailsToServer", e.toString());
        }
    }

    @SuppressLint("Range")
    public void syncContentQueries() {
        try {
            Cursor cursor = DatabaseHelper.getContentQueries(getApplicationContext(), DatabaseHelper.db, StaticVariables.UserLoginId);
            if (cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                            getApplicationContext().getResources().getString(R.string.Exam_NAMESPACE), getApplicationContext().getResources().getString(R.string.syncContentQueries));

                    //property which holds input parameters
                    PropertyInfo inputPI1 = new PropertyInfo();
                    inputPI1.setName("objXMLDoc");
                    inputPI1.setValue(CommonClass.ContentQueryXML(getApplicationContext(),
                            cursor.getString(cursor.getColumnIndex("MobRefId"))));
                    inputPI1.setType(String.class);
                    request.addProperty(inputPI1);
                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    //Set output SOAP object
                    envelope.setOutputSoapObject(request);
                    //Create HTTP call object
                    HttpTransportSE androidHttpTransport = new HttpTransportSE(getApplicationContext().getResources().getString(R.string.Exam_URL));
//        InputStream isResponse = null;

                    //Involve Web service
                    LPIEEX509TrustManager.allowAllSSL();
                    androidHttpTransport.call(getApplicationContext().getResources().getString(R.string.Exam_SOAP_ACTION) +
                            getApplicationContext().getResources().getString(R.string.syncContentQueries), envelope);
                    //Get the response
                    SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                    strResponse = response.toString();

                    JSONObject objJsonObject = new JSONObject(strResponse);
                    JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");

                    for (int index = 0; index < objJsonArray.length(); index++) {
                        JSONObject jsonobject = objJsonArray.getJSONObject(index);
                        if (jsonobject.has("Status")) {
                            DatabaseHelper.UpdtContentQueries(getApplicationContext(), DatabaseHelper.db,
                                    "" + jsonobject.getInt("MobRefId"), jsonobject.getString("UserId"),
                                    jsonobject.getString("Status"));
                        } else {
                            DatabaseHelper.UpdtContentQueries(getApplicationContext(), DatabaseHelper.db,
                                    "" + jsonobject.getInt("MobRefId"), StaticVariables.UserLoginId,
                                    jsonobject.getString("Pending"));
                        }
                    }
                }
            }
            cursor.close();
        } catch (Exception ioe) {
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncExamMaster", "syncContentQueries", ioe.toString());
        }
    }

    @SuppressLint("Range")
    private String reqBuild(Cursor mCursor) {
        String str_XML = "";
        try {
            nu.xom.Element root = new nu.xom.Element("RegisterCandidate");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(context));

//        nu.xom.Element InscType = new nu.xom.Element("InscType");
//        InscType.appendChild("" + StaticVariables.crnt_InscType);

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
//        root.appendChild(InscType);

//        for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {

            nu.xom.Element FirstName = new nu.xom.Element("FName");
            FirstName.appendChild(mCursor.getString(mCursor.getColumnIndex("FirstName")));
            root.appendChild(FirstName);

            nu.xom.Element LastName = new nu.xom.Element("LName");
            LastName.appendChild(mCursor.getString(mCursor.getColumnIndex("LastName")));
            root.appendChild(LastName);

            nu.xom.Element Gender = new nu.xom.Element("Gender");
            Gender.appendChild(mCursor.getString(mCursor.getColumnIndex("Gender")));
            root.appendChild(Gender);

            nu.xom.Element DOB = new nu.xom.Element("DOB");
            DOB.appendChild(mCursor.getString(mCursor.getColumnIndex("DOB")));
            root.appendChild(DOB);

            nu.xom.Element MobileNo = new nu.xom.Element("Mobile");
            MobileNo.appendChild(mCursor.getString(mCursor.getColumnIndex("MobileNo")));
            root.appendChild(MobileNo);

            nu.xom.Element EmailID = new nu.xom.Element("EmailId");
            EmailID.appendChild(mCursor.getString(mCursor.getColumnIndex("EmailID")));
            root.appendChild(EmailID);

            nu.xom.Element Nationality = new nu.xom.Element("Nationality");
            Nationality.appendChild(mCursor.getString(mCursor.getColumnIndex("Nationality")));
            root.appendChild(Nationality);

            nu.xom.Element Profession = new nu.xom.Element("Profession");
            Profession.appendChild(mCursor.getString(mCursor.getColumnIndex("Profession")));
            root.appendChild(Profession);

            nu.xom.Element UserLogin = new nu.xom.Element("UserLogin");
            UserLogin.appendChild(StaticVariables.UserLoginId);
            root.appendChild(UserLogin);

            nu.xom.Element Password = new nu.xom.Element("UserPin");
            Password.appendChild(mCursor.getString(mCursor.getColumnIndex("Password")));
            root.appendChild(Password);

            nu.xom.Element Device = new nu.xom.Element("Device");
            Device.appendChild(CommonClass.getIMEINumber(context));
            root.appendChild(Device);

            nu.xom.Element CreatedBy = new nu.xom.Element("ParentId");
            CreatedBy.appendChild(mCursor.getString(mCursor.getColumnIndex("CreatedBy")));
            root.appendChild(CreatedBy);

            nu.xom.Element MobRefId = new nu.xom.Element("MobRefId");
            MobRefId.appendChild(mCursor.getString(mCursor.getColumnIndex("MobRefId")));
            root.appendChild(MobRefId);

            img_byteArray = mCursor.getBlob(mCursor.getColumnIndex("ImageByte"));


            if (img_byteArray != null) {
            } else {
                try {
                    Drawable drawable = null;
                    drawable = context.getResources().getDrawable(R.drawable.profile_blank_m);
                    Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                    img_byteArray = stream.toByteArray();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            nu.xom.Element Image = new nu.xom.Element("Image");
            Image.appendChild(Base64.encodeToString(img_byteArray, Base64.NO_WRAP));
            root.appendChild(Image);
//        }
            nu.xom.Document doc = new nu.xom.Document(root);
            OutputStream out = new ByteArrayOutputStream();
            Serializer serializer = new Serializer(System.out, "UTF-8");
            serializer.setOutputStream(out);
            serializer.setIndent(4);
            serializer.setMaxLength(64);
            serializer.write(doc);
            str_XML = out.toString();
            str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
            str_XML = str_XML.replaceAll("\n", "");
            str_XML = str_XML.replaceAll("\r", "");
            str_XML = str_XML.replaceAll("     ", "");
            str_XML = str_XML.replaceAll("         ", "");
            str_XML = str_XML.trim();
//            Log.e("", "XMLStr==" + str_XML);
        } catch (IOException e) {
            Log.e("", "RegisterCandidate Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, db,
                    "SyncOfflineData", "reqBuild", e.toString());
        }
        return str_XML;
    }

    @SuppressLint("Range")
    private String updateBuild(Cursor mCursor) {

        String str_XML = "";
        try {
            nu.xom.Element root = new nu.xom.Element("ModifyCandidate");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(context));

            nu.xom.Element InscType = new nu.xom.Element("InscType");
            InscType.appendChild("" + StaticVariables.crnt_InscType);

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
            root.appendChild(InscType);

//        for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {

            nu.xom.Element CandidateId = new nu.xom.Element("CandidateId");
            CandidateId.appendChild(mCursor.getString(mCursor.getColumnIndex("CandidateId")));
            root.appendChild(CandidateId);

            nu.xom.Element FirstName = new nu.xom.Element("FName");
            FirstName.appendChild(mCursor.getString(mCursor.getColumnIndex("FirstName")));
            root.appendChild(FirstName);

            nu.xom.Element LastName = new nu.xom.Element("LName");
            LastName.appendChild(mCursor.getString(mCursor.getColumnIndex("LastName")));
            root.appendChild(LastName);

            nu.xom.Element Gender = new nu.xom.Element("Gender");
            Gender.appendChild(mCursor.getString(mCursor.getColumnIndex("Gender")));
            root.appendChild(Gender);

            nu.xom.Element DOB = new nu.xom.Element("DOB");
            DOB.appendChild(mCursor.getString(mCursor.getColumnIndex("DOB")));
            root.appendChild(DOB);

            nu.xom.Element MobileNo = new nu.xom.Element("Mobile");
            MobileNo.appendChild(mCursor.getString(mCursor.getColumnIndex("MobileNo")));
            root.appendChild(MobileNo);

            nu.xom.Element EmailID = new nu.xom.Element("EmailId");
            EmailID.appendChild(mCursor.getString(mCursor.getColumnIndex("EmailID")));
            root.appendChild(EmailID);

            nu.xom.Element Nationality = new nu.xom.Element("Nationality");
            Nationality.appendChild(mCursor.getString(mCursor.getColumnIndex("Nationality")));
            root.appendChild(Nationality);

            nu.xom.Element Profession = new nu.xom.Element("Profession");
            Profession.appendChild(mCursor.getString(mCursor.getColumnIndex("Profession")));
            root.appendChild(Profession);
            img_byteArray = mCursor.getBlob(mCursor.getColumnIndex("ImageByte"));


            if (img_byteArray != null) {
            } else {
                try {
                    Drawable drawable = null;
                    drawable = context.getResources().getDrawable(R.drawable.profile_blank_m);
                    Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                    img_byteArray = stream.toByteArray();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            nu.xom.Element Image = new nu.xom.Element("Image");
            Image.appendChild(Base64.encodeToString(img_byteArray, Base64.NO_WRAP));
            root.appendChild(Image);
//        }


            nu.xom.Document doc = new nu.xom.Document(root);
            OutputStream out = new ByteArrayOutputStream();
            Serializer serializer = new Serializer(System.out, "UTF-8");
            serializer.setOutputStream(out);
            serializer.setIndent(4);
            serializer.setMaxLength(64);
            serializer.write(doc);
            str_XML = out.toString();
            str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
            str_XML = str_XML.replaceAll("\n", "");
            str_XML = str_XML.replaceAll("\r", "");
            str_XML = str_XML.replaceAll("     ", "");
            str_XML = str_XML.replaceAll("         ", "");
            str_XML = str_XML.trim();
//            Log.e("", "XMLStr==" + str_XML);
        } catch (IOException e) {
            Log.e("", "ModifyCandidate Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, db,
                    "SyncOfflineData", "updateBuild", e.toString());
        }
        return str_XML;
    }

    // TODO: Check the Response Code
    public boolean IsCorrectXMLResponse(InputStream strXML) {
        boolean isZeroResponseCode = false;
        try {
            if (!(strXML.equals(null) || strXML == null)) {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(strXML);
                Element element = doc.getDocumentElement();
                element.normalize();
                String ResponseCode = getValue("ResponseCode", element);
                if (ResponseCode.equals("0")) {
                    isZeroResponseCode = true;
                } else {
                    isZeroResponseCode = false;
                }
            } else {
                isZeroResponseCode = false;
            }
        } catch (Exception e) {
            Log.e("", "error=" + e.toString());
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncExamMaster", "IsCorrectXMLResponse()", e.toString());
            isZeroResponseCode = false;
        }
        return isZeroResponseCode;
    }

    // TODO: Get the Value of Element
    public String getValue(String tag, Element element) {
        String value = "";
        try {
            if (element != null) {
                if (element.hasChildNodes()) {
                    NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
                    Node node = nodeList.item(0);
                    if (node != null)
                        value = node.getNodeValue();
                } else {
                    value = "";
                }
            } else {
                value = "";
            }
        } catch (Exception e) {
            Log.e("", "error=" + e.toString());
//            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
//                    "SyncOfflineData", "getValue() >> " + tag, e.toString());
            value = "";
        }
        return value;
    }

    private void fireNotification(String examNamae, String msg) {
        try {
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.training_icon)
                            .setContentTitle("The Agent Manager")
                            .setContentText(examNamae + " " + msg)
                            .setAutoCancel(true);

//            Intent resultIntent = new Intent(this, ViewResultActivity.class);
//            resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            resultIntent.putExtra("ExamId",ExamID);
//            resultIntent.putExtra("AttemptedId",AttemptId);
//            resultIntent.putExtra("CandidateId", CandidateId);
//            resultIntent.putExtra("initFrom", "SyncOfflineData");
//
//            PendingIntent resultPendingIntent =
//                    PendingIntent.getActivity(
//                            this,
//                            0,
//                            resultIntent,
//                            PendingIntent.FLAG_UPDATE_CURRENT
//                    );
//
//            mBuilder.setContentIntent(resultPendingIntent);

            NotificationManager mNotifyMgr =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            mNotifyMgr.notify(mNotificationId, mBuilder.build());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void notification(String examNamae, String msg) {
        try {
            int uniqueId = (int) System.currentTimeMillis();
            NotificationManager nm = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            Resources res = context.getResources();
            // Creates an explicit intent for an Activity in your app
            Intent resultIntent = new Intent(this, LPI_ShareAppActivity.class);
            resultIntent.setData(new Uri.Builder().scheme("data")
                    .appendQueryParameter(examNamae, msg).build());
            resultIntent.putExtra("candidate_id", examNamae);
            resultIntent.putExtra("initFrom", "notification");
            resultIntent.putExtra("notification_id", uniqueId);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            // Adds the back stack for the Intent (but not the Intent itself)
            stackBuilder.addParentStack(LPI_ShareAppActivity.class);
            // Adds the Intent that starts the Activity to the top of the stack
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent;
            //= stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                resultPendingIntent = stackBuilder.getPendingIntent(0,  PendingIntent.FLAG_IMMUTABLE);
            } else {
                resultPendingIntent = stackBuilder.getPendingIntent(0,  PendingIntent.FLAG_UPDATE_CURRENT);
            }

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.setSmallIcon(R.drawable.training_icon)
                    .setLargeIcon(
                            BitmapFactory.decodeResource(res,
                                    R.drawable.training_icon))
                    .setAutoCancel(true)
                    .setContentTitle("The Training Manager")
                    .setContentText(examNamae + " " + msg)
                    .setContentIntent(resultPendingIntent);
            builder.getNotification().flags |= Notification.FLAG_AUTO_CANCEL;
            builder.setOngoing(false);
            builder.setAutoCancel(true);
            Notification n = builder.build();
            n.flags = Notification.FLAG_NO_CLEAR;
            nm.notify(uniqueId, n);
        }catch (Exception e){
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "SyncOfflineData", "notification", e.toString());
        }
    }
}
