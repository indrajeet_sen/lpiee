package com.lpi.kmi.lpi.Services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.stringcare.library.SC;

import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;


/**
 * Created by mustafakachwalla on 28/07/17.
 */

public class SyncPostLicModuleData extends IntentService {
    String strResponse;

    public SyncPostLicModuleData() {
        super("SyncPostLicModuleData");
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
            startForeground(CommonClass.getNotificationId(), CommonClass.createServiceNotification(this));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (DatabaseHelper.db == null) {
            SQLiteDatabase.loadLibs(getApplicationContext());
            DatabaseHelper.db = DatabaseHelper.getInstance(getApplicationContext()).
                    getWritableDatabase(SC.reveal(CommonClass.DBPassword));
        }
        getModuleData(getApplicationContext());
    }

    public void getModuleData(Context context) {
        org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                context.getResources().getString(R.string.Exam_NAMESPACE), context.getResources().getString(R.string.GetLPI_LessonData));
        //property which holds input parameters
        PropertyInfo inputPI1 = new PropertyInfo();
        inputPI1.setName("objXMLDoc");
        try {
            String objXMLDoc = CommonClass.GenerateXML(context, 2, "GetLPI_LessonData",
                    new String[]{"RootSectionId", "Mode", "LoginId"}, new String[]{"6", "", StaticVariables.UserLoginId});
            inputPI1.setValue(objXMLDoc);
        } catch (Exception e1) {
            //TODO Auto-generated catch block
            e1.printStackTrace();
        }
        inputPI1.setType(String.class);
        request.addProperty(inputPI1);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        //Set output SOAP object
        envelope.setOutputSoapObject(request);
        //Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getResources().getString(R.string.Exam_URL), 120000);
//        InputStream isResponse = null;
        try {
            //Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
            androidHttpTransport.call(context.getResources().getString(R.string.Exam_SOAP_ACTION) + context.getResources().getString(R.string.GetLPI_LessonData), envelope);
            //Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            strResponse = response.toString();

            JSONObject objJsonObject = new JSONObject(strResponse);
            JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");

            for (int index = 0; index < objJsonArray.length(); index++) {
                JSONObject jsonobject = objJsonArray.getJSONObject(index);

                DatabaseHelper.PostLicModuleData(context, DatabaseHelper.db,
                        jsonobject.getString("SectionId"),
                        jsonobject.getString("ParentSectionId"),
                        jsonobject.getString("RootSectionId"),
                        jsonobject.getString("Desc01"),
                        jsonobject.getString("Desc02"),
                        jsonobject.getString("Desc03"),
                        jsonobject.getString("Desc04"),
                        jsonobject.getString("Desc05"),
                        jsonobject.getString("GenDesc01"),
                        jsonobject.getString("GenDesc02"),
                        jsonobject.getString("GenDesc03"),
                        jsonobject.getString("GenDesc04"),
                        jsonobject.getString("GenDesc05"),
                        jsonobject.getString("AdvDesc01"),
                        jsonobject.getString("AdvDesc02"),
                        jsonobject.getString("AdvDesc03"),
                        jsonobject.getString("AdvDesc04"),
                        jsonobject.getString("AdvDesc05"),
                        jsonobject.getString("ImageDesc01"),
                        jsonobject.getString("ImageDesc02"),
                        jsonobject.getString("ImageDesc03"),
                        jsonobject.getString("ImageDesc04"),
                        jsonobject.getString("ImageDesc05"),
                        jsonobject.getString("hasChild"),
                        jsonobject.getString("hasData"),
                        jsonobject.getString("IsActive"),
                        jsonobject.getString("IsVideoAvailable"),
                        jsonobject.getString("videoLink"),
                        StaticVariables.UserLoginId + "", "08-08-2017", "", "");

                if (jsonobject.getString("AdvDesc01").contains("<body>")) {
                    String html = "<!DOCTYPE html><html><head><meta name=\"viewport\" " +
                            "content=\"width=device-width, initial-scale=1.0\">" +
                            jsonobject.getString("AdvDesc01") + "</html>";
                    String fName = "A01" + jsonobject.getString("SectionId") + ".html";
                    CommonClass.saveHtmlInStorage(context, fName, html);
                }

                if (jsonobject.getString("GenDesc01").contains("<body>")) {
                    String html = "<!DOCTYPE html><html><head><meta name=\"viewport\" " +
                            "content=\"width=device-width, initial-scale=1.0\">" +
                            jsonobject.getString("GenDesc01") + "</html>";
                    String fName = "B01" + jsonobject.getString("SectionId") + ".html";
                    CommonClass.saveHtmlInStorage(context, fName, html);
                }
            }

        } catch (Exception e) {
            Log.e("GetLPI_LessonData", "GetLPI_LessonData Error=" + e.toString());
        }
    }
}
