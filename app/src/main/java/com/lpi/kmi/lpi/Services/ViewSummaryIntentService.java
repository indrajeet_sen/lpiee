package com.lpi.kmi.lpi.Services;

import android.app.IntentService;
import android.content.Intent;
import androidx.annotation.Nullable;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.stringcare.library.SC;

import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by darshanad on 9/6/2017.
 */

public class ViewSummaryIntentService extends IntentService {

    public ViewSummaryIntentService() {
        super("");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        try {
            startForeground(CommonClass.getNotificationId(), CommonClass.createServiceNotification(this));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (DatabaseHelper.db == null) {
            SQLiteDatabase.loadLibs(getApplicationContext());
            DatabaseHelper.db = DatabaseHelper.getInstance(getApplicationContext()).
                    getWritableDatabase(SC.reveal(CommonClass.DBPassword));
        }
        
        LPI_ViewSummary();
//        setSummary();

    }

    public void LPI_ViewSummary() {


        org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                getApplicationContext().getString(R.string.Exam_NAMESPACE),
                getApplicationContext().getString(R.string.ViewSummaryService));

        // property which holds input parameters
        PropertyInfo inputPI1 = new PropertyInfo();
        String strResponse = "";

        inputPI1.setName("AppNo");
        try {

            inputPI1.setValue(/*StaticVariables.UserLoginId*/ SC.reveal(CommonClass.DummyAppId));
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        inputPI1.setType(String.class);
        request.addProperty(inputPI1);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.dotNet = true;
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(getApplicationContext().getString(R.string.Exam_URL));


        try {
            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
            androidHttpTransport.call(getApplicationContext().getString(R.string.Exam_SOAP_ACTION) +
                    getApplicationContext().getString(R.string.ViewSummaryService), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            // Assign it to static variable
            strResponse = response.toString();
            Log.e("", "response=" + strResponse);


        } catch (Exception e) {
            Log.e("", "Login Error=" + e.toString());
        }
        GetJSONValue(strResponse);

    }

    private void GetJSONValue(String strResponse) {

        String Description = "", CreatedDt = "";

        try {
            JSONObject jsonobj = new JSONObject(strResponse);
            String json = "";
            String AppNo = "";

            json = jsonobj.getJSONArray("Table").toString();


            try {
                JSONArray viewSummaryArray = new JSONArray(json);
                for (int i = 0; i < viewSummaryArray.length(); i++) {
                    JSONObject obj = new JSONObject(viewSummaryArray.getString(i));

                    Description = obj.getString("Description01");
                    CreatedDt = obj.getString("CreatedDt");
                    AppNo = obj.getString("AppNo");

                    DatabaseHelper.LpiinsertViewSummaryData(getApplicationContext(), DatabaseHelper.db, AppNo,Description,CreatedDt);

                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
            }




        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}