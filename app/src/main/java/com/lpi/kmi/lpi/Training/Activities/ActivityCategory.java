package com.lpi.kmi.lpi.Training.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Services.SyncActivityDetails;
import com.lpi.kmi.lpi.Services.SyncErrorLog;
import com.lpi.kmi.lpi.Training.Adapters.CardAdapter;
import com.lpi.kmi.lpi.Training.Adapters.CardModel;
import com.lpi.kmi.lpi.activities.AsyncTasks.GetHtmlContent;
import com.lpi.kmi.lpi.activities.ParticipantMenuActivity;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;

import java.util.ArrayList;
import java.util.List;


public class ActivityCategory extends AppCompatActivity {

//    ImageButton btn_genDetail, btn_lifDetail, btn_hltDetail;

    List<String> catId = new ArrayList<String>();
    List<String> catList = new ArrayList<String>();
    List<String> catDesc = new ArrayList<String>();
    List<CardModel> cardModels = new ArrayList<CardModel>();
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
    Toolbar toolbar;
    Button plmSync;
    LinearLayout linPlmSync;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_category);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        toolbar = (Toolbar) findViewById(R.id.trainingtoolbar);
        toolbar.setTitle("Insurance Type");
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.CategoryRecycler);
        plmSync = (Button) findViewById(R.id.btn_plmSync);
        linPlmSync = (LinearLayout) findViewById(R.id.lin_plmcontent);

        recyclerView.setVisibility(View.GONE);
        linPlmSync.setVisibility(View.GONE);


        plmSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonClass.isConnected(ActivityCategory.this)) {
//                    Intent newIntent = new Intent(ActivityCategory.this, SyncPostLicModuleData.class);
//                    startService(newIntent);

                    Intent trackIntent = new Intent(getApplicationContext(), SyncActivityDetails.class);
//                    startService(trackIntent);
                    CommonClass.StartService(getApplicationContext(), trackIntent);

                    Intent errIntent = new Intent(getApplicationContext(), SyncErrorLog.class);
//                    startService(errIntent);
                    CommonClass.StartService(getApplicationContext(), errIntent);

                    new GetHtmlContent(ActivityCategory.this, "ActivityCategory").execute();
                }
                linPlmSync.setVisibility(View.GONE);
                fillList();
                onBackPressed();

            }
        });
        fillList();
        layoutManager = new LinearLayoutManager(ActivityCategory.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new CardAdapter(ActivityCategory.this, "Category", cardModels);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(itemAnimator);


//        btn_genDetail = (ImageButton) findViewById(R.id.btn_genDetail);
//        btn_lifDetail = (ImageButton) findViewById(R.id.btn_lifDetail);
//        btn_hltDetail = (ImageButton) findViewById(R.id.btn_hltDetail);
//
//        btn_genDetail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(ActivityCategory.this,LOBActivity.class);
//                intent.putExtra("LOB","General Insurance");
//                startActivity(intent);
//                ActivityCategory.this.finish();
//            }
//        });
//        btn_lifDetail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(ActivityCategory.this,LOBActivity.class);
//                intent.putExtra("LOB","Life Insurance");
//                startActivity(intent);
//                ActivityCategory.this.finish();
//            }
//        });
//        btn_hltDetail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(ActivityCategory.this,LOBActivity.class);
//                intent.putExtra("LOB","Health Insurance");
//                startActivity(intent);
//                ActivityCategory.this.finish();
//            }
//        });

    }

    public void fillList() {
        cardModels.clear();
        catList.clear();
        catDesc.clear();
        catId.clear();
        try {
            String sql = "select SectionId,Desc01 from PostLicModuleData where ParentSectionId=" + 1 +
                    " and RootSectionId=" + 1;

            Cursor cursor = DatabaseHelper.db.rawQuery(sql, null);
            if (cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    catList.add(cursor.getString(1));
                    catId.add(cursor.getString(0));
                }
                for (int i = 0; i < catList.size(); i++) {
                    CardModel cardModel = new CardModel();
                    cardModel.setSectionId(catId.get(i));
                    cardModel.setId("" + (i + 1));
                    cardModel.setName(catList.get(i));
                    cardModel.setDesc("");
                    cardModel.setStart("START");
                    cardModels.add(cardModel);
                }
                linPlmSync.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            } else {
                recyclerView.setVisibility(View.GONE);
                linPlmSync.setVisibility(View.VISIBLE);
            }
            cursor.close();
        } catch (Exception e) {
            Log.e("fillList", "error=" + e.toString());
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ActivityCategory.this, ParticipantMenuActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        ActivityCategory.this.finish();
    }
}
