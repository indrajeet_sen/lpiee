package com.lpi.kmi.lpi.Training.Activities;

import android.content.Context;

import com.lpi.kmi.lpi.Training.Adapters.TViewChild;
import com.lpi.kmi.lpi.Training.Adapters.TViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chandrashekhar on 16-06-2017.
 */

public class CommonMethods {

    public static TViewGroup getProduct(Context context,List<String> child) {
        TViewGroup group = new TViewGroup();
        List<TViewChild> children = new ArrayList<TViewChild>();
        for (int i=0;i<child.size();i++){
            TViewChild viewChild = new TViewChild();
            viewChild.setText1(""+(i+1));
            viewChild.setText2(child.get(i));
            viewChild.setText3("");
            children.add(viewChild);
        }
        group.setItems(children);
        return group;
    }

    public static TViewGroup getViewDetails(Context context,List<String> childName,List<String> childValues) {
        TViewGroup group = new TViewGroup();
        List<TViewChild> children = new ArrayList<TViewChild>();
        for (int i=0;i<childName.size();i++){
            TViewChild viewChild = new TViewChild();
            viewChild.setText1(childName.get(i));
            viewChild.setText2(childValues.get(i));
            viewChild.setText3("");
            children.add(viewChild);
        }
        group.setItems(children);
        return group;
    }


    public static TViewGroup getLobProduct(Context context,List<String> child,List<String> cIdentity) {
        TViewGroup group = new TViewGroup();
        List<TViewChild> children = new ArrayList<TViewChild>();
        for (int i=0;i<child.size();i++){
            TViewChild viewChild = new TViewChild();
            viewChild.setText1(""+(i+1));
            viewChild.setText2(child.get(i));
            viewChild.setText3(cIdentity.get(i));
            children.add(viewChild);
        }
        group.setItems(children);
        return group;
    }
}
