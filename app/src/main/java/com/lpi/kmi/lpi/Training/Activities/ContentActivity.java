package com.lpi.kmi.lpi.Training.Activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Training.fragment.ProductFragment;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.StaticVariables;

public class ContentActivity extends AppCompatActivity {


    public static RelativeLayout container;
    public static RelativeLayout scrollButtons;
    public static Toolbar toolbar;
    public static String title = "";
    FragmentManager fragmentManager = getFragmentManager();
    Fragment fragment;

//    public static ImageView fabScrollUp, fabScrollDown, fabBack;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_content);
        try {
//            StaticVariables.mTitle = getIntent().getStringExtra("ActivityTitle");
            title = getIntent().getStringExtra("ActivityTitle");
            StaticVariables.mTitle = getIntent().getStringExtra("ActivityTitle");
            if (title.equals("Health")) {
                StaticVariables.mLob = "Health Insurance";
            }
            if (getIntent().hasExtra("SectionId")) {
                StaticVariables.mIdentity = getIntent().getStringExtra("SectionId");
            }
            toolbar = (Toolbar) findViewById(R.id.contenttollbar);
            toolbar.setTitleTextColor(getResources().getColor(R.color.white));
            setSupportActionBar(toolbar);
            setTitle(ContentActivity.this, title);
            container = (RelativeLayout) findViewById(R.id.Content_Container);
            container.setVisibility(View.GONE);
//            fabScrollUp = (ImageView) findViewById(R.id.fab_scrollup);
//            fabScrollDown = (ImageView) findViewById(R.id.fab_scrolldown);
//            fabBack = (ImageView) findViewById(R.id.fab_back);
//            scrollButtons = (RelativeLayout) findViewById(R.id.scrollButtons);
//            scrollButtons.setVisibility(View.GONE);
//            container.setOnTouchListener(new ListView.OnTouchListener() {
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    int action = event.getAction();
//                    switch (action) {
//                        case MotionEvent.ACTION_DOWN:
//                            // Disallow ScrollView to intercept touch events.
//                            v.getParent().requestDisallowInterceptTouchEvent(true);
//                            break;
//
//                        case MotionEvent.ACTION_UP:
//                            // Allow ScrollView to intercept touch events.
//                            v.getParent().requestDisallowInterceptTouchEvent(false);
//                            break;
//                    }
//
//                    // Handle ListView touch events.
//                    v.onTouchEvent(event);
//                    return true;
//                }
//            });

            if (fragment==null){
                fragment = new ProductFragment();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
//                    transaction.replace(R.id.content_frame, fragment);
                transaction.replace(R.id.Content_Container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
                ContentActivity.container.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            Log.e("ContentActivity", "error=" + e.toString());
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    //    public static void showChiled(Context context,String section) {
//        try {
//            List<ChildNodeModel> childNodeModelList = new ArrayList<ChildNodeModel>();
//            List<String> childId = new ArrayList<>();
//            List<String> childNode = new ArrayList<>();
//            Cursor cursor;
//            cursor = DatabaseHelper.db.query(context.getResources().getString(R.string.TblPostLicModuleData),
//                    new String[]{"SectionId", "Desc01"},
//                    "ParentSectionId=?", new String[]{section}, null, null, null);
//            childNodeModelList.clear();
//            childId.clear();
//            childNode.clear();
//            if (cursor.getCount() > 0) {
//                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
//                    childId.add(cursor.getString(0));
//                    childNode.add(cursor.getString(1));
//                }
//
//            }
//
//            if (childId.size() > 0) {
//                for (int i = 0; i < childId.size(); i++) {
//                    ChildNodeModel childNodeModel = new ChildNodeModel(childId.get(i), childNode.get(i));
//                    childNodeModelList.add(childNodeModel);
//                }
//                childList.setAdapter(new ChildNodeAdapter(context, childNodeModelList));
//            }
//            childList.setVisibility(View.VISIBLE);
//        } catch (Exception e) {
//            Log.e("", "error setting child =" + e.toString());
//        }
//    }

    public static void setTitle(Context context, String title) {
        try {
            ((AppCompatActivity) context).getSupportActionBar().setTitle(title);
//            toolbar.setTitle(title);

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("setTitle", "error-" + e.toString());
        }
    }

    @Override
    public void onBackPressed() {

        String sql = "select distinct ParentSectionId from PostLicModuleData where SectionId not in (select distinct RootSectionId from PostLicModuleData)" +
                " and SectionId = '" + StaticVariables.mIdentity + "';";
        Cursor cursor = DatabaseHelper.db.rawQuery(sql, null);
        if (cursor.getCount() > 0) {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                StaticVariables.mIdentity = cursor.getString(0);
                Fragment fragment = new ProductFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.Content_Container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        } else {
            Intent intent = new Intent(ContentActivity.this, LOBActivity.class);
            intent.putExtra("LOB", StaticVariables.InsType);
            intent.putExtra("SectionId", StaticVariables.SectionId);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            ContentActivity.this.finish();
        }
        cursor.close();
    }


}
