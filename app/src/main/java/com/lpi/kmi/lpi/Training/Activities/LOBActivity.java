package com.lpi.kmi.lpi.Training.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Training.Adapters.PostLicenseViewAdapter;
import com.lpi.kmi.lpi.Training.Adapters.TViewGroup;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.util.ArrayList;
import java.util.List;

public class LOBActivity extends AppCompatActivity {

    private PostLicenseViewAdapter ExpAdapter;
    private List<TViewGroup> ExpListItems;
    private ExpandableListView genExpandList;
    //            ,lifExpandList,hltExpandList;
    private int lastExpandedPosition = -1;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_lob);
        toolbar = (Toolbar) findViewById(R.id.trainingtoolbar);
        try {
            StaticVariables.productFlag = getIntent().getStringExtra("LOB");
            StaticVariables.SectionId = getIntent().getStringExtra("SectionId");
        } catch (Exception e) {
//            StaticVariables.productFlag = "Products";
            Log.e("", "error=" + e.toString());
        }
        toolbar.setTitle(StaticVariables.productFlag);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);

        genExpandList = (ExpandableListView) findViewById(R.id.exp_list);
//        lifExpandList = (ExpandableListView) findViewById(R.id.exp_list_life);
//        hltExpandList = (ExpandableListView) findViewById(R.id.exp_list_helth);

        try {
            if (StaticVariables.productFlag != null && !StaticVariables.productFlag.trim().equalsIgnoreCase("")) {
                ExpListItems = SetStandardGroups(StaticVariables.productFlag);

                ExpAdapter = new PostLicenseViewAdapter(LOBActivity.this, ExpListItems);
                genExpandList.setAdapter(ExpAdapter);

                genExpandList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

                    @Override
                    public void onGroupExpand(int groupPosition) {
                        if (lastExpandedPosition != -1
                                && groupPosition != lastExpandedPosition) {
                            genExpandList.collapseGroup(lastExpandedPosition);
                        }
                        lastExpandedPosition = groupPosition;
                    }
                });
                genExpandList.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ArrayList<TViewGroup> SetStandardGroups(String insid) {
        List<String> headers = new ArrayList<String>();
        List<String> headersId = new ArrayList<String>();
        List<String> headersVideo = new ArrayList<String>();
        List<String> headersLink = new ArrayList<String>();

        List<String> child = new ArrayList<String>();
        ArrayList<TViewGroup> groups = new ArrayList<TViewGroup>();
        headers.clear();
        headersId.clear();
        headersVideo.clear();
        headersLink.clear();
        child.clear();
        String sql = "select SectionId,Desc01,IsVideoAvailable,videoLink " +
                "from PostLicModuleData where ParentSectionId = " + StaticVariables.SectionId;

        Cursor cursor = DatabaseHelper.db.rawQuery(sql, null);

        if (cursor.getCount() > 0) {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                headers.add(cursor.getString(1));
                headersId.add(cursor.getString(0));
                headersVideo.add(cursor.getString(2));
                headersLink.add(cursor.getString(3));
            }
        }
        cursor.close();
        for (int i = 0; i < headers.size(); i++) {
            child.clear();
            TViewGroup group = new TViewGroup();
            child.add("Currently not available");
            group = CommonMethods.getProduct(LOBActivity.this, child);
            group.setName(headers.get(i));
            group.setIdentiy("");
            groups.add(group);
        }


//        if (insid.equals("General Insurance")){
//            headers.clear();
//            headers.add("Motor");
//            headers.add("Health");
//            headers.add("Home");
//            headers.add("Travel");
//            headers.add("Other Products");
//        }else if (insid.equals("Life Insurance")){
//            headers.clear();
//            headers.add("Term Life Insurance");
//            headers.add("Whole Life Policy");
//            headers.add("Endowment Plans");
//            headers.add("Unit Linked Insurance Plans");
//            headers.add("Money Back Policy");
//        }
//
//
//
//        ArrayList<TViewGroup> groups = new ArrayList<TViewGroup>();
//        for (int i = 0; i < headers.size(); i++) {
//            if (headers.get(i).equals("Motor")) {
//                child.clear();
//                child.add("Private Car Package (Comprehensive)");
//                child.add("Private Car Liability (Third Party)");
//                child.add("Two Wheeler Package (Comprehensive)");
//                child.add("Two Wheeler Liability (Third Party)");
//                child.add("Passenger Carrying Package (Comprehensive)");
//                child.add("Passenger Carrying Liability (Third Party)");
//                child.add("Goods Carrying Package (Comprehensive)");
//                child.add("Goods Carrying Liability (Third Party)");
//                child.add("Miscellaneous Package (Comprehensive)");
//                child.add("Miscellaneous Liability (Third Party)");
//                TViewGroup group = new TViewGroup();
//                group = CommonMethods.getProduct(LOBActivity.this, child);
//                group.setName(headers.get(i));
//                group.setIdentiy("");
//                groups.add(group);
//            } else if (headers.get(i).equals("Health")) {
//                child.clear();
//                child.add("Health Insurance");
//                TViewGroup group = new TViewGroup();
//                group = CommonMethods.getProduct(LOBActivity.this, child);
//                group.setName(headers.get(i));
//                group.setIdentiy("");
//                groups.add(group);
//            } else if (headers.get(i).equals("Home")) {
//                child.clear();
//                child.add("Coming Soon");
//                TViewGroup group = new TViewGroup();
//                group = CommonMethods.getProduct(LOBActivity.this, child);
//                group.setName(headers.get(i));
//                group.setIdentiy("");
//                groups.add(group);
//            } else if (headers.get(i).equals("Life")) {
//                child.clear();
//                child.add("Coming Soon");
//                TViewGroup group = new TViewGroup();
//                group = CommonMethods.getProduct(LOBActivity.this, child);
//                group.setName(headers.get(i));
//                group.setIdentiy("");
//                groups.add(group);
//            } else if (headers.get(i).equals("Travel")) {
//                child.clear();
//                child.add("Coming Soon");
//                TViewGroup group = new TViewGroup();
//                group = CommonMethods.getProduct(LOBActivity.this, child);
//                group.setName(headers.get(i));
//                groups.add(group);
//            } else if (headers.get(i).equals("Other Products")) {
//                child.clear();
//                child.add("Coming Soon");
//                TViewGroup group = new TViewGroup();
//                group = CommonMethods.getProduct(LOBActivity.this, child);
//                group.setName(headers.get(i));
//                group.setIdentiy("");
//                groups.add(group);
//            }else if (headers.get(i).equals("Term Life Insurance")) {
//                child.clear();
//                child.add("Term Insurance");
//                TViewGroup group = new TViewGroup();
//                group = CommonMethods.getProduct(LOBActivity.this, child);
//                group.setName(headers.get(i));
//                group.setIdentiy("");
//                groups.add(group);
//            }else if (headers.get(i).equals("Whole Life Policy")) {
//                child.clear();
//                child.add("Coming Soon");
//                TViewGroup group = new TViewGroup();
//                group = CommonMethods.getProduct(LOBActivity.this, child);
//                group.setName(headers.get(i));
//                group.setIdentiy("");
//                groups.add(group);
//            }else if (headers.get(i).equals("Endowment Plans")) {
//                child.clear();
//                child.add("Coming Soon");
//                TViewGroup group = new TViewGroup();
//                group = CommonMethods.getProduct(LOBActivity.this, child);
//                group.setName(headers.get(i));
//                group.setIdentiy("");
//                groups.add(group);
//            }else if (headers.get(i).equals("Unit Linked Insurance Plans")) {
//                child.clear();
//                child.add("Coming Soon");
//                TViewGroup group = new TViewGroup();
//                group = CommonMethods.getProduct(LOBActivity.this, child);
//                group.setName(headers.get(i));
//                group.setIdentiy("");
//                groups.add(group);
//            }else if (headers.get(i).equals("Money Back Policy")) {
//                child.clear();
//                child.add("Coming Soon");
//                TViewGroup group = new TViewGroup();
//                group = CommonMethods.getProduct(LOBActivity.this, child);
//                group.setName(headers.get(i));
//                group.setIdentiy("");
//                groups.add(group);
//            }
//        }
        return groups;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LOBActivity.this, ActivityCategory.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        LOBActivity.this.finish();
    }
}
