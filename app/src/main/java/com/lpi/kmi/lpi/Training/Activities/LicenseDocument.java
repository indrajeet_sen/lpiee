package com.lpi.kmi.lpi.Training.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class LicenseDocument extends AppCompatActivity {

    TextView textView1, textView2;
    Toolbar toolbar;
    ImageView imageView1, imageView2;
    static String strPathLoction = "";
    static String strText = "This is Sample PDF example.";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_view_summary);
        initUI();
        createPDF();
        UIListener();
    }

    private void UIListener() {
        try {
            imageView1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (strPathLoction.trim().length() > 0) {
                        File file = new File(strPathLoction);
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    } else {
                        Toast.makeText(LicenseDocument.this, "PDF not generated.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initUI() {
        try {
            toolbar= (Toolbar) findViewById(R.id.trainingtoolbar);
            toolbar.setTitle("License Document");
            toolbar.setTitleTextColor(getResources().getColor(R.color.white));
            textView1 = (TextView) findViewById(R.id.textView1);
            textView2 = (TextView) findViewById(R.id.textView2);
            imageView1 = (ImageView) findViewById(R.id.imageView1);
            imageView2 = (ImageView) findViewById(R.id.imageView2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("NewApi")
    private void createPDF() {
        try {
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/PDFfile/";
            PdfDocument document = new PdfDocument();
            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);
            int width = (int) (dm.widthPixels);
            int height = (int) (dm.heightPixels);
            int pageNumber = 1;
            Rect rect = new Rect(0, 0, width, height);

            PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(width, height, pageNumber).create();

            // create a new page from the PageInfo
            PdfDocument.Page page = document.startPage(pageInfo);

            Canvas canvas = page.getCanvas();
            canvas.clipRect(rect);

            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawPaint(paint);
            paint.setColor(Color.BLACK);
            paint.setTextSize(50);
            canvas.drawText(strText, 50, 50, paint);

            // do final processing of the page
            document.finishPage(page);
            try {
                String strPDFName = "newFile.pdf";
                File dir = new File(path);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                File file = new File(dir, strPDFName);
                FileOutputStream OutputStream = new FileOutputStream(file);
                document.writeTo(OutputStream);
                document.close();
                OutputStream.close();

                strPathLoction = path.toString() + strPDFName;
            } catch (IOException e) {
                strPathLoction = "";
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "LicenseDocument", "createPDF", e.toString());
        }
    }
}