package com.lpi.kmi.lpi.Training.Activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import androidx.legacy.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Training.Adapters.TViewAdapter;
import com.lpi.kmi.lpi.Training.Adapters.TViewGroup;
import com.lpi.kmi.lpi.Training.fragment.ProductFragment;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

//import static com.lpi.kmi.lpi.activities.DatabaseHelper.db;

/**
 * Created by mustafakachwalla on 26/07/17.
 */

public class LobNavigationDrawer extends AppCompatActivity {
    public static DrawerLayout mDrawerLayout;
    //    private ListView mDrawerList;
    public static ExpandableListView mDrawerList;
    private TViewAdapter ExpAdapter;
    private List<TViewGroup> ExpListItems;
    private ActionBarDrawerToggle mDrawerToggle;
    Toolbar toolbar;
    public static TextView textNote;
    //    private CharSequence mDrawerTitle;

    //    private String[] mPlanetTitles;
    private List<String> header = new ArrayList<>();
    private List<String> childName = new ArrayList<>();
    private List<String> hIdentity = new ArrayList<>();
    private List<String> cIdentity = new ArrayList<>();
    public static Cursor cursor = null,cursor1=null;
//    public static SQLiteDatabase db;
    public static String title="";


    public final static String FRAGMENT_PARAM = "fragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_layout);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        toolbar = (Toolbar) findViewById(R.id.trainingtoolbar);
        try {
            StaticVariables.mTitle = getIntent().getStringExtra("ActivityTitle");
            title = getIntent().getStringExtra("ActivityTitle");
        } catch (Exception e) {
            StaticVariables.mTitle = getTitle().toString();
        }
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
//        mTitle = getTitle().toString();
//        mPlanetTitles = getResources().getStringArray(R.array.planets_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ExpandableListView) findViewById(R.id.left_drawer);
        textNote = (TextView) findViewById(R.id.textNote);
        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
//        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
//                R.layout.drawer_list_item, mPlanetTitles));
        ExpListItems = SetStandardGroups();
        ExpAdapter = new TViewAdapter(LobNavigationDrawer.this, ExpListItems);
        mDrawerList.setAdapter(ExpAdapter);
//        mDrawerList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
//            @Override
//            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
//                selectItem(childPosition);
//                return false;
//            }
//        });
        mDrawerList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int c = mDrawerList.getCount();
                StaticVariables.mLob = header.get(groupPosition).toString();
                String code = DatabaseHelper.getActivityMaster(LobNavigationDrawer.this,DatabaseHelper.db,"Lob");
                DatabaseHelper.InsertActivityTracker(LobNavigationDrawer.this,DatabaseHelper.db,StaticVariables.UserLoginId,
                        code,"","Lob Open",StaticVariables.mLob,"","",
                        StaticVariables.sdf.format(Calendar.getInstance().getTime()),"","",StaticVariables.UserLoginId,"","","","Pending");
                for (int i = 0; i < c; i++) {
                    if (i != groupPosition)
                        mDrawerList.collapseGroup(i);
                }

            }
        });

        // enable ActionBar app icon to behave as action to toggle nav drawer
        setSupportActionBar(toolbar);
        getSupportActionBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer);
        getSupportActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(StaticVariables.mTitle);

                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(StaticVariables.mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            selectItem(0);
        }
        String code = DatabaseHelper.getActivityMaster(LobNavigationDrawer.this,DatabaseHelper.db,"Post Licence");
        DatabaseHelper.InsertActivityTracker(LobNavigationDrawer.this,DatabaseHelper.db,StaticVariables.UserLoginId,
                code,"","Post Licence",StaticVariables.mLob,"","",
                StaticVariables.sdf.format(Calendar.getInstance().getTime()),"","",StaticVariables.UserLoginId,"","","","Pending");
    }

    private List<TViewGroup> SetStandardGroups() {

        header.clear();
        hIdentity.clear();
//        Cursor cursor =null;
//        cursor = DatabaseHelper.GetLobName(getBaseContext(),DatabaseHelper.db,cursor);


        String sql = "select * from PostLicModuleData;";
//        cursor = db.rawQuery(sql, null);
//        Log.e("LPIPOST", "cursor.getCount()=" + cursor.getCount());
//        if (cursor.getCount() > 0) {
//            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
//                int c = 0;
//                while (c < cursor.getColumnCount()) {
//                    Log.e("LPIPOST", cursor.getColumnName(c) + " - " + cursor.getString(c));
//                    c++;
//                }
//            }
//        }

        if (title.equals("Health Insurance")){
            cursor = DatabaseHelper.db.rawQuery(getResources().getString(R.string.getHealthLob), null);
        }else if (title.equals("Term Insurance")){
            cursor = DatabaseHelper.db.rawQuery(getResources().getString(R.string.getTermLob), null);
        }


        if (cursor == null || cursor.getCount() == 0) {

        } else {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                hIdentity.add(cursor.getString(0));
                header.add(cursor.getString(1));
            }
        }
        List<TViewGroup> groups = new ArrayList<TViewGroup>();
        groups.clear();
        for (int i = 0; i < header.size(); i++) {
            String LoB = hIdentity.get(i).toString();
            Log.e("LPIPOST", "hIdentity.get(" + i + ")=" + hIdentity.get(i).toString());
            Log.e("LPIPOST", "header.get(" + i + ")=" + header.get(i).toString());
            try {
                childName.clear();
                cIdentity.clear();

//                cursor = db.query(getResources().getString(R.string.TblPostLicModuleData),
//                        new String[]{"SectionId", "Desc01"}, "ParentSectionId=?",
//                        new String[]{LoB}, null, null, "SectionId");

//                if (title.equals("Health Insurance")){
//                    sql = "select SectionId,Desc01 from PostLicModuleData where ParentSectionId=" +
//                            LoB +" and SectionId!="+ LoB + " order by SectionId;";
//                }else if (title.equals("Term Insurance")){
//                    sql = "select SectionId,Desc01 from PostLicModuleData where ParentSectionId="
//                            + LoB +" and SectionId!="+ LoB +" order by SectionId;";
//                }

                sql = "select SectionId,Desc01 from PostLicModuleData where ParentSectionId="
                        + LoB +" and SectionId!="+ LoB +" order by SectionId;";

                cursor = DatabaseHelper.db.rawQuery(sql, null);
                if (cursor == null || cursor.getCount() == 0) {

                } else {
                    for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                        cIdentity.add(cursor.getString(0));
                        childName.add(cursor.getString(1));
                    }
                }
                TViewGroup group = new TViewGroup();
                group = CommonMethods.getLobProduct(LobNavigationDrawer.this, childName, cIdentity);
                group.setName(header.get(i).toString());
                group.setIdentiy(hIdentity.get(i).toString());
                groups.add(group);
            } catch (Exception e) {
                Log.e("LPIPOST", "group error=" + e.toString());
            }
        }
        return groups;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.drawer_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        if (drawerOpen){
            textNote.setVisibility(View.GONE);
        }
        menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        return super.onOptionsItemSelected(item);
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

//    /* The click listner for ListView in the navigation drawer */
//    private class DrawerItemClickListener implements ExpandableListView.OnChildClickListener {
//        @Override
//        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
//            selectItem(childPosition);
//            return true;
//        }
//    }

    public void selectItem(int position) {
        // update the main content by replacing fragments
//        Fragment fragment = new ProductFragment();
//        Bundle args = new Bundle();
//        args.putInt("String", position);
//        fragment.setArguments(args);

        getSupportActionBar().setTitle(StaticVariables.mTitle);

//        FragmentManager fragmentManager = getFragmentManager();
//        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }

    @Override
    public void setTitle(CharSequence title) {
        StaticVariables.mTitle = title.toString();
        getSupportActionBar().setTitle(StaticVariables.mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        String sql = "select distinct ParentSectionId from PostLicModuleData where SectionId not in (select distinct RootSectionId from PostLicModuleData)" +
                " and SectionId = '"+StaticVariables.mIdentity +"';";
        Cursor cursor = DatabaseHelper.db.rawQuery(sql,null);
        if (cursor.getCount()>0){
            for (cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
                StaticVariables.mIdentity= cursor.getString(0);
                Fragment fragment = new ProductFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.content_frame, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        }else {
            Intent intent  = new Intent(LobNavigationDrawer.this,LOBActivity.class);
            if (title.equals("Health Insurance")){
                intent.putExtra("LOB","General Insurance");
            }else if (title.equals("Term Insurance")){
                intent.putExtra("LOB","Life Insurance");
            }
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            LobNavigationDrawer.this.finish();
        }

    }
}
