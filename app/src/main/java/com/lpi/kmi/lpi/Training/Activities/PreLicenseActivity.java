package com.lpi.kmi.lpi.Training.Activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;


public class PreLicenseActivity extends AppCompatActivity {

    ImageButton btn_csDetail,btn_viewDetail,btn_LicDocDetail;
    Toolbar toolbar;
    public static String actionFlag="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_license);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        toolbar= (Toolbar) findViewById(R.id.trainingtoolbar);
        toolbar.setTitle("PreLicense Training");
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        init();


    }

    private void init() {

        btn_csDetail= (ImageButton) findViewById(R.id.btn_csDetail);
        btn_viewDetail= (ImageButton) findViewById(R.id.btn_viewDetail);
//        btn_LicDocDetail= (ImageButton) findViewById(R.id.btn_LicDocDetail);

        btn_csDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(PreLicenseActivity.this,StatusActivity.class);
                intent.putExtra("action","csDetail");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();

            }
        });

        btn_viewDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(PreLicenseActivity.this,ViewDetailsActivity.class);
                intent.putExtra("action","viewDetail");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();


            }
        });

//        btn_LicDocDetail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent=new Intent(PreLicenseActivity.this,LicenseDocument.class);
//                startActivity(intent);
//            }
//        });
    }




}
