package com.lpi.kmi.lpi.Training.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.widget.ExpandableListView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Training.Adapters.TViewAdapter;
import com.lpi.kmi.lpi.Training.Adapters.TViewGroup;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.util.ArrayList;
import java.util.List;

public class ProductActivity extends AppCompatActivity {

//    private RecyclerView recyclerView;
//    private RecyclerView.LayoutManager layoutManager;
//    private RecyclerView.Adapter adapter;
//    RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();

    public static ExpandableListView mDrawerList;
    private TViewAdapter ExpAdapter;
    private List<TViewGroup> ExpListItems;

    private List<String> header = new ArrayList<>();
    private List<String> childName = new ArrayList<>();
    private List<String> hIdentity = new ArrayList<>();
    private List<String> cIdentity = new ArrayList<>();
    Toolbar toolbar;
    public static String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
//        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
//        itemAnimator.setAddDuration(1000);
//        itemAnimator.setRemoveDuration(1000);

        try {
            StaticVariables.mTitle = getIntent().getStringExtra("ActivityTitle");
//            StaticVariables.mIdentity = getIntent().getStringExtra("SectionId");
            title = getIntent().getStringExtra("ActivityTitle");
            toolbar = (Toolbar) findViewById(R.id.trainingtoolbar);
            toolbar.setTitle(StaticVariables.mTitle);
            toolbar.setTitleTextColor(getResources().getColor(R.color.white));
            setSupportActionBar(toolbar);

            mDrawerList = (ExpandableListView) findViewById(R.id.productList);
            ExpListItems = SetStandardGroups();
            ExpAdapter = new TViewAdapter(ProductActivity.this, ExpListItems);

            mDrawerList.setAdapter(ExpAdapter);

            mDrawerList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                @Override
                public void onGroupExpand(int groupPosition) {
                    int c = mDrawerList.getCount();
                    StaticVariables.mLob = header.get(groupPosition).toString();
//                    String code = DatabaseHelper.getActivityMaster(ProductActivity.this, DatabaseHelper.db, "Lob");
//                    DatabaseHelper.InsertActivityTracker(ProductActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
//                            code, "Lob Open", StaticVariables.mLob, "", "", "", "", "", StaticVariables.UserLoginId, "", "", "", "Pending");
                    for (int i = 0; i < c; i++) {
                        if (i != groupPosition)
                            mDrawerList.collapseGroup(i);
                    }

                }
            });
        } catch (Exception e) {
            Log.e("Product Activity", "error=" + e.toString());
        }

    }

    @Override
    public void onBackPressed() {
        try {
            Intent intent = new Intent(ProductActivity.this, LOBActivity.class);
            intent.putExtra("LOB", StaticVariables.productFlag);
            intent.putExtra("SectionId", StaticVariables.SectionId);
//            if (LOBActivity.LOB.equals("Health")){
//                intent.putExtra("LOB","General Insurance");
//                intent.putExtra("LOB","General Insurance");
//            }else if (LOBActivity.LOB.equals("Term Life")){
//                intent.putExtra("LOB","Life Insurance");
//                intent.putExtra("LOB","Life Insurance");
//            }
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            ProductActivity.this.finish();
        } catch (Exception e) {
            Log.e("Product Activity", "Back Press -" + e.toString());
            super.onBackPressed();
        }
    }

    private List<TViewGroup> SetStandardGroups() {

        header.clear();
        hIdentity.clear();
        Cursor cursor = null;
//        cursor = DatabaseHelper.GetLobName(getBaseContext(),DatabaseHelper.db,cursor);


        String sql = "select * from PostLicModuleData;";
//        cursor = db.rawQuery(sql, null);
//        Log.e("LPIPOST", "cursor.getCount()=" + cursor.getCount());
//        if (cursor.getCount() > 0) {
//            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
//                int c = 0;
//                while (c < cursor.getColumnCount()) {
//                    Log.e("LPIPOST", cursor.getColumnName(c) + " - " + cursor.getString(c));
//                    c++;
//                }
//            }
//        }

        if (title.equals("Health Insurance")) {
            cursor = DatabaseHelper.db.rawQuery(getResources().getString(R.string.getHealthLob), null);
        } else if (title.equals("Term Life")) {
            cursor = DatabaseHelper.db.rawQuery(getResources().getString(R.string.getTermLob), null);
        }


        if (cursor == null || cursor.getCount() == 0) {

        } else {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                hIdentity.add(cursor.getString(0));
                header.add(cursor.getString(1));
            }
        }
        List<TViewGroup> groups = new ArrayList<TViewGroup>();
        groups.clear();
        for (int i = 0; i < header.size(); i++) {
            String LoB = hIdentity.get(i).toString();
            Log.e("LPIPOST", "hIdentity.get(" + i + ")=" + hIdentity.get(i).toString());
            Log.e("LPIPOST", "header.get(" + i + ")=" + header.get(i).toString());
            try {
                childName.clear();
                cIdentity.clear();

//                cursor = db.query(getResources().getString(R.string.TblPostLicModuleData),
//                        new String[]{"SectionId", "Desc01"}, "ParentSectionId=?",
//                        new String[]{LoB}, null, null, "SectionId");

//                if (title.equals("Health Insurance")){
//                    sql = "select SectionId,Desc01 from PostLicModuleData where ParentSectionId=" +
//                            LoB +" and SectionId!="+ LoB + " order by SectionId;";
//                }else if (title.equals("Term Insurance")){
//                    sql = "select SectionId,Desc01 from PostLicModuleData where ParentSectionId="
//                            + LoB +" and SectionId!="+ LoB +" order by SectionId;";
//                }

                sql = "select SectionId,Desc01 from PostLicModuleData where ParentSectionId="
                        + LoB + " and SectionId!=" + LoB + " order by SectionId;";

                cursor = DatabaseHelper.db.rawQuery(sql, null);
                if (cursor == null || cursor.getCount() == 0) {

                } else {
                    for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                        cIdentity.add(cursor.getString(0));
                        childName.add(cursor.getString(1));
                    }
                }
                TViewGroup group = new TViewGroup();
                group = CommonMethods.getLobProduct(ProductActivity.this, childName, cIdentity);
                group.setName(header.get(i).toString());
                group.setIdentiy(hIdentity.get(i).toString());
                groups.add(group);
            } catch (Exception e) {
                Log.e("LPIPOST", "group error=" + e.toString());
            }
        }
        return groups;

    }
}
