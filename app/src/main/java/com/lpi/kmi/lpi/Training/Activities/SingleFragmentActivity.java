package com.lpi.kmi.lpi.Training.Activities;

import android.app.Fragment;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;


/**
 * Created by Bogdan Melnychuk on 2/12/15.
 */
public class SingleFragmentActivity extends AppCompatActivity {
    public final static String FRAGMENT_PARAM = "fragment";

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_single_fragment);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        toolbar = (Toolbar) findViewById(R.id.trainingtoolbar);
        try {
            toolbar.setTitle(getIntent().getStringExtra("ActivityTitle"));
            toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        } catch (Exception e) {
            toolbar.setTitle("Products Details");
        }
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        Bundle b = getIntent().getExtras();
        Class<?> fragmentClass = (Class<?>) b.get(FRAGMENT_PARAM);
        if (bundle == null) {
            Fragment f = Fragment.instantiate(this, fragmentClass.getName());
            f.setArguments(b);
            getFragmentManager().beginTransaction().replace(R.id.fragment, f, fragmentClass.getName()).commit();
        }
    }
}
