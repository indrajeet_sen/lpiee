package com.lpi.kmi.lpi.Training.Activities;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.stringcare.library.SC;


/**
 * Created by Chandrashekhar on 14-07-2017.
 */

public class StatusActivity extends AppCompatActivity implements View.OnClickListener{
    //,TextViewExamValue ,textViewName1
    TextView //TextViewEvent,
            statusHeading,textViewName,textViewValue,textViewExam,textViewEValue,
            currentStatusText, trainingDetails,  textViewExam11,textViewEValue11,

            //TextViewNotification,
            textViewName1,trainingDetails1,currentStatusText1,
            statusHeading1,textViewValue1, textViewExam1, textViewEValue1;
    Button TextViewEvent,TextViewNotification;

    String AppNo = "", CndURN = "", cndStatus = "", LcnExpDate = "", LcnNo = "", LcnIssDate = "";
//    View viewLeft, viewRight;
    CardView statusCard, statusCard1;
    ImageButton btn_more, btn_more1;
    LinearLayout linearLayout, linearLayout1;
    public static String flag="Event";
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_status);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        toolbar= (Toolbar) findViewById(R.id.trainingtoolbar);
        toolbar.setTitle("Current Status");
        toolbar.setTitleTextAppearance(StatusActivity.this,R.style.toolbarTextStyle);
        init();
    }

    private void setNotificationContent() {
        setStatusContent();
        statusCard.setVisibility(View.VISIBLE);
        statusCard1.setVisibility(View.VISIBLE);

        if (flag.equals("Notification")) {
            statusHeading.setText("SMS");
            currentStatusText.setText("SMS sent for candidate");
            trainingDetails.setText("SMS Details");
            textViewName.setText("SMS Date");
            textViewValue.setText("Dear Anil,\nThe Licence particulars are as follows" +
                    "\nLicence Date: 18/06/2015\nLicence Number: RGI15A25812\nExpiry Date:NA");

            textViewExam.setText("SMS Content");
            textViewEValue.setText("18/06/2015");

            textViewExam11.setText("");
            textViewEValue11.setText("");
//            viewLeft.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//            viewRight.setBackgroundColor(getResources().getColor(R.color.white));

        }

        statusHeading1.setText("Email");
        currentStatusText1.setText("Email sent for candidate");
        trainingDetails1.setText("Email Details");
        textViewName1.setText("Email Date");
        textViewValue1.setText("18/06/2015");
        textViewExam1.setText("Email Content");
        textViewEValue1.setText("Dear Anil,\nWarm welcome to Reliance General Insurance Family!!!!!!!!" +
                "\nReliance General Insurance is one of the leading general insurance companies of India." +
                "\nThe details of your registration are mentioned below:" +
                "\nLicence Date: 18/06/2015\nLicence Number: RGI15A25812" +
                "\nExpiry Date:NA");


    }

    private void setStatusContent() {
        statusCard1.setVisibility(View.GONE);
        statusCard.setVisibility(View.VISIBLE);
        if (flag.equals("Notification")){
            statusHeading.setText("SMS");
            currentStatusText.setText("SMS sent for candidate");
            trainingDetails.setText("SMS Details");
            textViewName.setText("SMS Date");
            textViewValue.setText("Dear Anil,\nThe Licence particulars are as follows" +
                    "\nLicence Date: 18/06/2015\nLicence Number: RGI15A25812\nExpiry Date:NA");

            textViewExam.setText("SMS Content");
            textViewEValue.setText("18/06/2015");

            textViewExam11.setText("");
            textViewEValue11.setText("");

        }else if (flag.equals("Event")){
            statusHeading.setText("Current Status");
            currentStatusText.setText("Licensed");
            trainingDetails.setText("License Details");
            textViewName.setText("URN Number");
            textViewValue.setText(" HDFI2909094948");
            textViewExam.setText("Licence Date");
            textViewEValue.setText("18/06/2015");
            textViewExam11.setText("Licence Exp Date");
            textViewEValue11.setText("NA");

        }else {
            statusHeading.setText("NA");
            currentStatusText.setText("NA");
            trainingDetails.setText("NA");
            textViewName.setText("NA");
            textViewValue.setText("NA");
            textViewExam.setText("NA");
            textViewEValue.setText("NA");
            textViewExam11.setText("NA");
            textViewEValue11.setText("NA");
        }

    }

    void init() {
        statusCard = (CardView) findViewById(R.id.statusCard);
        TextViewEvent = (Button) findViewById(R.id.TextViewEvent);
        statusHeading = (TextView) findViewById(R.id.statusHeading);
        trainingDetails = (TextView) findViewById(R.id.trainingDetails);
        textViewName = (TextView) findViewById(R.id.textViewName);
        textViewExam = (TextView) findViewById(R.id.textViewExam);
        textViewEValue = (TextView) findViewById(R.id.textViewValue);
        textViewValue = (TextView) findViewById(R.id.textViewEValue);
        textViewExam11= (TextView) findViewById(R.id.textViewExam11);
        textViewEValue11= (TextView) findViewById(R.id.textViewEValue11);
        btn_more = (ImageButton) findViewById(R.id.btn_more);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);

        statusCard1 = (CardView) findViewById(R.id.statusCard1);
        TextViewNotification = (Button) findViewById(R.id.TextViewNotification);
        statusHeading1 = (TextView) findViewById(R.id.statusHeading1);
        trainingDetails1 = (TextView) findViewById(R.id.trainingDetails1);
        currentStatusText1= (TextView) findViewById(R.id.currentStatusText1);
        textViewName1 = (TextView) findViewById(R.id.textViewName1);
        textViewValue1 = (TextView) findViewById(R.id.textViewValue1);
        textViewEValue1 = (TextView) findViewById(R.id.textViewEValue1);
        textViewExam1 = (TextView) findViewById(R.id.textViewExam1);
        currentStatusText = (TextView) findViewById(R.id.currentStatusText);
        btn_more1 = (ImageButton) findViewById(R.id.btn_more1);
        linearLayout1 = (LinearLayout) findViewById(R.id.linearLayout1);
//        viewLeft = findViewById(R.id.viewLeft);
//        viewRight = findViewById(R.id.viewRight);

        TextViewEvent.setOnClickListener(this);
        TextViewNotification.setOnClickListener(this);
        btn_more.setOnClickListener(this);
        btn_more1.setOnClickListener(this);


        getLicenceDatafromDB();
        setStatusContent();
    }

    @SuppressLint("Range")
    private void getLicenceDatafromDB() {

        Cursor mCursor = null;
        try {
            try {
                mCursor = DatabaseHelper.getBasicDetails(this, DatabaseHelper.db, SC.reveal(CommonClass.DummyAppId));
            } catch (Exception e) {
                Log.d("StatusActivity: ", e.getMessage());
            }
            if (mCursor.getCount() > 0) {
                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    AppNo = mCursor.getString(mCursor.getColumnIndex("AppNo"));
                    CndURN = mCursor.getString(mCursor.getColumnIndex("CndURN"));
                    cndStatus = mCursor.getString(mCursor.getColumnIndex("cndStatus"));
                    LcnExpDate = mCursor.getString(mCursor.getColumnIndex("LcnExpDate"));
                    LcnNo = mCursor.getString(mCursor.getColumnIndex("LcnNo"));
                    LcnIssDate = mCursor.getString(mCursor.getColumnIndex("LcnIssDate"));
                }
            }
            mCursor.close();
        } catch (Exception e) {
            Log.d("StatusActivity: ", e.getMessage());
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.TextViewEvent:
                TextViewEvent.setTextColor(getResources().getColor(R.color.colorPrimary));
                TextViewNotification.setTextColor(getResources().getColor(R.color.Gray));
                flag="Event";
                setStatusContent();
                if (linearLayout.getVisibility() == View.VISIBLE) {
                    linearLayout.setVisibility(View.GONE);
                }
                if (linearLayout1.getVisibility() == View.VISIBLE) {
                    linearLayout1.setVisibility(View.GONE);
                }
                break;
            case R.id.TextViewNotification:
                TextViewEvent.setTextColor(getResources().getColor(R.color.Gray));
                TextViewNotification.setTextColor(getResources().getColor(R.color.colorPrimary));
                flag="Notification";
                setNotificationContent();
                if (linearLayout.getVisibility() == View.VISIBLE) {
                    linearLayout.setVisibility(View.GONE);
                }
                if (linearLayout1.getVisibility() == View.VISIBLE) {
                    linearLayout1.setVisibility(View.GONE);
                }
                break;
            case R.id.btn_more:
                if (linearLayout.getVisibility() == View.VISIBLE) {
                    linearLayout.setVisibility(View.GONE);
                } else {
                    linearLayout.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.btn_more1:
                if (linearLayout1.getVisibility() == View.VISIBLE) {
                    linearLayout1.setVisibility(View.GONE);
                } else {
                    linearLayout1.setVisibility(View.VISIBLE);
                }
                break;

        }
    }
}
