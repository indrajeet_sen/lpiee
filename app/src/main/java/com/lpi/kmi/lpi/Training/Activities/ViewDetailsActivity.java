package com.lpi.kmi.lpi.Training.Activities;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Training.Adapters.SummaryAdapter;
import com.lpi.kmi.lpi.Training.Adapters.TViewAdapter;
import com.lpi.kmi.lpi.Training.Adapters.TViewGroup;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.stringcare.library.SC;

import java.util.ArrayList;

public class ViewDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private TViewAdapter ExpAdapter;
    private ArrayList<TViewGroup> ExpListItems;
    private ExpandableListView genExpandList;
    public static String action = "";
    Toolbar toolbar;
    private boolean add;
    ListView listView;
    ArrayList<String> arrayList1 = new ArrayList<>();
    ArrayList<String> arrayList2 = new ArrayList<>();
    SummaryAdapter summaryAdapter;

    Button textViewBasicDetails, textViewSummary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_details);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));

        toolbar = (Toolbar) findViewById(R.id.trainingtoolbar);
        toolbar.setTitle("View Details");
        toolbar.setTitleTextAppearance(ViewDetailsActivity.this, R.style.toolbarTextStyle);
        listView = (ListView) findViewById(R.id.listView);

        textViewBasicDetails = (Button) findViewById(R.id.TextViewBasicDetails);
        textViewSummary = (Button) findViewById(R.id.TextViewSummary);
        genExpandList = (ExpandableListView) findViewById(R.id.expandaleList);

        genExpandList.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);
        textViewBasicDetails.setOnClickListener(this);
        textViewSummary.setOnClickListener(this);

        ExpListItems = SetStandardGroups();
        ExpAdapter = new TViewAdapter(ViewDetailsActivity.this, ExpListItems);
        genExpandList.setAdapter(ExpAdapter);
        genExpandList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int c = genExpandList.getCount();
                for (int i = 0; i < c; i++) {
                    if (i != groupPosition) {
                        genExpandList.collapseGroup(i);
                    }
                }
            }
        });
        genExpandList.setVisibility(View.VISIBLE);

        setSummary();
        summaryAdapter = new SummaryAdapter(arrayList1, arrayList2, this);
        listView.setAdapter(summaryAdapter);

//        textViewBasicDetails.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                genExpandList.setVisibility(View.VISIBLE);
//                listView.setVisibility(View.GONE);
//                textViewBasicDetails.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//                textViewSummary.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
//            }
//        });
//
//        textViewSummary.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                genExpandList.setVisibility(View.GONE);
//                listView.setVisibility(View.VISIBLE);
//                textViewBasicDetails.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
//                textViewSummary.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//            }
//        });
//        try {
//
//            setSummary();
//
////            arrayList1.add("13/01/2015");
////            arrayList1.add("12/01/2015");
////            arrayList1.add("05/01/2015");
////            arrayList1.add("02/01/2015");
////            arrayList1.add("01/01/2015");
////            arrayList1.add("24/12/2014");
////            arrayList1.add("22/12/2014");
////            arrayList1.add("20/12/2014");
////            arrayList1.add("14/12/2014");
////            arrayList1.add("14/12/2014");
////            arrayList1.add("14/12/2014");
////
////            arrayList2.add("Licensed");
////            arrayList2.add("Trainimg complete");
////            arrayList2.add("Exam Taken");
////            arrayList2.add("Examination Slot Allocated");
////            arrayList2.add("Exam Schedule Requested");
////            arrayList2.add("Training Completion Certificate Uploaded");
////            arrayList2.add("Training Schedule Requested");
////            arrayList2.add("Quality Check Completed");
////            arrayList2.add("Pending Quality Check");
////            arrayList2.add("Sponsorship Requested");
////            arrayList2.add("Candidate Registration");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.TextViewBasicDetails:
                textViewBasicDetails.setTextColor(getResources().getColor(R.color.colorPrimary));
                textViewSummary.setTextColor(getResources().getColor(R.color.Gray));
                ExpAdapter.notifyDataSetChanged();
                genExpandList.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
                break;

            case R.id.TextViewSummary:
                textViewBasicDetails.setTextColor(getResources().getColor(R.color.Gray));
                textViewSummary.setTextColor(getResources().getColor(R.color.colorPrimary));
                //TViewChild tVchild=new TViewChild();
                summaryAdapter.notifyDataSetChanged();
                genExpandList.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);

                break;
        }
    }

    @SuppressLint("Range")
    public void setSummary() {
        String Description = "",
                CreatedDt = "";

        arrayList1.clear();
        arrayList2.clear();

        Cursor mCursor = null;
        try {
            mCursor = DatabaseHelper.db.query(getResources().getString(R.string.TBL_TblViewSummary), null, "AppNo=?",
                    new String[]{"" + SC.reveal(CommonClass.DummyAppId)}, null, null, null, null);
//            mCursor = DatabaseHelper.getViewSummary(ViewDetailsActivity.this, DatabaseHelper.db, getApplicationContext().getString(R.string.DummyAppId));
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }
        if (mCursor.getCount() > 0) {
            for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {

                //AppNo = mCursor.getString(mCursor.getColumnIndex("AppNo"));
                Description = mCursor.getString(mCursor.getColumnIndex("Description"));
                CreatedDt = mCursor.getString(mCursor.getColumnIndex("CreatedDt"));

                arrayList1.add(Description);
                arrayList2.add(CreatedDt);
            }
        }

        mCursor.close();

    }

    @SuppressLint("Range")
    private ArrayList<TViewGroup> SetStandardGroups() {
        String AppNo = "",
                GivenName = "",
                CndURN = "",
                cndStatus = "",
                SAPCode = "",
                ManagerName = "",
                Branch = "",
                Hierarchy = "",
                SubClass = "",
                LcnExpDate = "",
                LcnNo = "",
                LcnIssDate = "",
                TrnMode = "",
                TrnLocDesc = "",
                TrnInstitute = "",
                AccrdNo = "",
                HrsTrn = "",
                TrnStartDate = "",
                TrnEndDate = "",
                ExmMode = "",
                ExamLanguage = "",
                ExmBody = "",
                ExmCentre = "",
                FeesTokenNo = "",
                FeesExpected = "",
                FeesCollected = "",
                PaymentMode = "",
                FeesCollectedDate = "",
                FeesTransactionNumber = "";
        Cursor mCursor = null;
        try {
            mCursor = DatabaseHelper.getBasicDetails(ViewDetailsActivity.this, DatabaseHelper.db, SC.reveal(CommonClass.DummyAppId));
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }
        if (mCursor.getCount() > 0) {
            for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {

                AppNo = mCursor.getString(mCursor.getColumnIndex("AppNo"));
                GivenName = mCursor.getString(mCursor.getColumnIndex("GivenName"));
                CndURN = mCursor.getString(mCursor.getColumnIndex("CndURN"));
                cndStatus = mCursor.getString(mCursor.getColumnIndex("cndStatus"));
                SAPCode = mCursor.getString(mCursor.getColumnIndex("SAPCode"));
                ManagerName = mCursor.getString(mCursor.getColumnIndex("ManagerName"));
                Branch = mCursor.getString(mCursor.getColumnIndex("Branch"));
                Hierarchy = mCursor.getString(mCursor.getColumnIndex("Hierarchy"));
                SubClass = mCursor.getString(mCursor.getColumnIndex("SubClass"));
                LcnExpDate = mCursor.getString(mCursor.getColumnIndex("LcnExpDate"));
                LcnNo = mCursor.getString(mCursor.getColumnIndex("LcnNo"));
                LcnIssDate = mCursor.getString(mCursor.getColumnIndex("LcnIssDate"));
                TrnMode = mCursor.getString(mCursor.getColumnIndex("TrnMode"));
                TrnLocDesc = mCursor.getString(mCursor.getColumnIndex("TrnLocDesc"));
                TrnInstitute = mCursor.getString(mCursor.getColumnIndex("TrnInstitute"));
                AccrdNo = mCursor.getString(mCursor.getColumnIndex("AccrdNo"));
                HrsTrn = mCursor.getString(mCursor.getColumnIndex("HrsTrn"));
                TrnStartDate = mCursor.getString(mCursor.getColumnIndex("TrnStartDate"));
                TrnEndDate = mCursor.getString(mCursor.getColumnIndex("TrnEndDate"));
                ExmMode = mCursor.getString(mCursor.getColumnIndex("ExmMode"));
                ExamLanguage = mCursor.getString(mCursor.getColumnIndex("ExamLanguage"));
                ExmBody = mCursor.getString(mCursor.getColumnIndex("ExmBody"));
                ExmCentre = mCursor.getString(mCursor.getColumnIndex("ExmCentre"));
                FeesTokenNo = mCursor.getString(mCursor.getColumnIndex("FeesTokenNo"));
                FeesExpected = mCursor.getString(mCursor.getColumnIndex("FeesExpected"));
                FeesCollected = mCursor.getString(mCursor.getColumnIndex("FeesCollected"));
                PaymentMode = mCursor.getString(mCursor.getColumnIndex("PaymentMode"));
                FeesCollectedDate = mCursor.getString(mCursor.getColumnIndex("FeesCollectedDate"));
                FeesTransactionNumber = mCursor.getString(mCursor.getColumnIndex("FeesTransactionNumber"));

            }
        }
        ArrayList<String> header = new ArrayList<String>();
        ArrayList<String> childName = new ArrayList<String>();
        ArrayList<String> childValue = new ArrayList<String>();

        header.clear();
        header.add("Basic Details");
        header.add("View Candidate Status");
        header.add("Company Details");
        header.add("Training Details");
        header.add("Examination Details");
        header.add("License Details");
        header.add("Fees Details");


        ArrayList<TViewGroup> groups = new ArrayList<TViewGroup>();
        groups.clear();
        for (int i = 0; i < header.size(); i++) {
            if (header.get(i).equals("Basic Details")) {

                try {
                    childName.clear();
                    childName.add("Application Number");
                    childName.add("Candidate Name");
                    childName.add("status");
                    childName.add("URN Number");

                    childValue.clear();
                    childValue.add(AppNo);
                    childValue.add(GivenName);
                    childValue.add(cndStatus);
                    childValue.add(CndURN);


                    TViewGroup group = new TViewGroup();
                    group = CommonMethods.getViewDetails(ViewDetailsActivity.this, childName, childValue);
                    group.setName(header.get(i));
                    groups.add(group);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("", "error=" + e.toString());
                }
            } else if (header.get(i).equals("View Candidate Status")) {


                try {
                    childName.clear();
                    childName.add("Created By");
                    childName.add("Current Status");
                    childName.add("Date");
                    childName.add("Actionable");

                    childValue.clear();
                    childValue.add(SAPCode);
                    childValue.add("Licensed");
                    childValue.add("13-01-2015");
                    childValue.add("Not Defined");

                    TViewGroup group = new TViewGroup();
                    group = CommonMethods.getViewDetails(ViewDetailsActivity.this, childName, childValue);
                    group.setName(header.get(i));
                    groups.add(group);

                } catch (Exception e) {

                }
            } else if (header.get(i).equals("Company Details")) {

                try {
                    childName.clear();
                    childName.add("Manager SAP Code");
                    childName.add("Manager Name");
                    childName.add("Branch Name");
                    childName.add("Hierarchy Name");
                    childName.add("SubClass Name");
                    childName.add("Manager SAP Code");

                    childValue.clear();
                    childValue.add("70258842");
                    childValue.add(ManagerName);
                    childValue.add(Branch);
                    childValue.add(Hierarchy);
                    childValue.add(SubClass);
                    childValue.add(SubClass);

                    TViewGroup group = new TViewGroup();
                    group = CommonMethods.getViewDetails(ViewDetailsActivity.this, childName, childValue);
                    group.setName(header.get(i));
                    groups.add(group);
                } catch (Exception e) {
                }

            } else if (header.get(i).equals("License Details")) {

                try {
                    childName.clear();
                    childName.add("Liecense Number");
                    childName.add("Issue Date");
                    childName.add("Expiry Date");

                    childValue.clear();
                    childValue.add(LcnNo);
                    childValue.add(LcnIssDate);
                    childValue.add(LcnExpDate);


                    TViewGroup group = new TViewGroup();
                    group = CommonMethods.getViewDetails(ViewDetailsActivity.this, childName, childValue);
                    group.setName(header.get(i));
                    groups.add(group);
                } catch (Exception e) {
                }
            } else if (header.get(i).equals("Training Details")) {

                try {

                    childName.clear();
                    childName.add("Location");
                    childName.add("Accrediation Number");
                    childName.add("Institute");
                    childName.add("Training Duration");

                    childValue.clear();
                    childValue.add(TrnLocDesc);
                    childValue.add(AccrdNo);
                    childValue.add(TrnInstitute);
                    childValue.add(HrsTrn);


                    TViewGroup group = new TViewGroup();
                    group = CommonMethods.getViewDetails(ViewDetailsActivity.this, childName, childValue);
                    group.setName(header.get(i));
                    groups.add(group);
                } catch (Exception e) {
                }
            } else if (header.get(i).equals("Examination Details")) {

                try {
                    childName.clear();
                    childName.add("Examination Mode");
                    childName.add("Language");
                    childName.add("Examination Body");
                    childName.add("Exam Centre");
                    childName.add("Exam Date");
                    childName.add("Exam Time");
                    childName.add("Result");
                    childName.add("Marks");

                    childValue.clear();
                    childValue.add(ExmMode);
                    childValue.add(ExamLanguage);
                    childValue.add(ExmBody);
                    childValue.add(ExmCentre);
                    childValue.add("NA");
                    childValue.add("NA");
                    childValue.add("NA");
                    childValue.add("NA");

                    TViewGroup group = new TViewGroup();
                    group = CommonMethods.getViewDetails(ViewDetailsActivity.this, childName, childValue);
                    group.setName(header.get(i));
                    groups.add(group);
                } catch (Exception e) {
                }

            } else if (header.get(i).equals("Fees Details")) {

                try {
                    childName.clear();
                    childName.add("Token No.");
                    childName.add("Transaction No.");
                    childName.add("Fees collected");
                    childName.add("Fees expected");
                    childName.add("Payment Mode");
                    childName.add("Date off collection");

                    childValue.clear();
                    childValue.add(FeesTokenNo);
                    childValue.add(FeesTransactionNumber);
                    childValue.add(FeesCollected);
                    childValue.add(FeesExpected);
                    childValue.add(PaymentMode);
                    childValue.add(FeesCollectedDate);

                    TViewGroup group = new TViewGroup();
                    group = CommonMethods.getViewDetails(ViewDetailsActivity.this, childName, childValue);
                    group.setName(header.get(i));
                    groups.add(group);
                } catch (Exception e) {
                }
            }

        }
        mCursor.close();
        return groups;
    }

//    private ArrayList<String> SetViewSummary() {
//        String AppNo = "",
//                Description = "",
//                CreatedDt = "";
//
//        Cursor mCursor = null;
//        try {
//            mCursor = DatabaseHelper.getBasicDetails(ViewDetailsActivity.this, DatabaseHelper.db, getApplicationContext().getString(R.string.DummyAppId));
//        } catch (Exception e) {
//            Log.e("", e.getMessage());
//        }
//        if (mCursor.getCount() > 0) {
//            for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
//
//                AppNo = mCursor.getString(mCursor.getColumnIndex("AppNo"));
//                Description = mCursor.getString(mCursor.getColumnIndex("Description"));
//                CreatedDt = mCursor.getString(mCursor.getColumnIndex("CreatedDt"));
//
//            }
//
//            //descriptioArray.add()
//        }
//
//    }

}
