package com.lpi.kmi.lpi.Training.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Training.Activities.LOBActivity;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by mustafakachwalla on 02/09/17.
 */

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {
    Context context;
    String ActivityFlag = "";
//    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    List<CardModel> cardModels = new ArrayList<CardModel>();


    public CardAdapter(Context context, String ActivityFlag, List<CardModel> cardModels) {
        this.context = context;
        this.ActivityFlag = ActivityFlag;
        this.cardModels = cardModels;
    }

    @Override
    public CardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if (ActivityFlag.equals("Category")) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.category_layout, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.category_layout, parent, false);
        }

        ViewHolder viewHolder = new ViewHolder(view, ActivityFlag);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CardAdapter.ViewHolder holder, int position) {
        CardModel cardModel = cardModels.get(position);
        if (ActivityFlag.equals("Category")) {
            holder.sectionId.setText(cardModel.getSectionId());
            holder.id.setText(cardModel.getId());
            holder.name.setText(cardModel.getName());
            holder.desc.setText(cardModel.getDesc());
            holder.start.setText(cardModel.getStart());

            holder.start.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, LOBActivity.class);
                    StaticVariables.InsType=holder.name.getText().toString();
                    intent.putExtra("LOB", holder.name.getText().toString());
                    intent.putExtra("SectionId", holder.sectionId.getText().toString());
                    context.startActivity(intent);
                    ((Activity) context).finish();
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return cardModels.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView sectionId,id, name, desc, start;

        public ViewHolder(View itemView, String ActivityFlag) {
            super(itemView);

            View view = itemView;

            if (ActivityFlag.equals("Category")) {
                sectionId= (TextView) view.findViewById(R.id.textSectionId);
                id = (TextView) view.findViewById(R.id.text_catId);
                name = (TextView) view.findViewById(R.id.text_Category);
                desc = (TextView) view.findViewById(R.id.text_CategoryDesc);
                start = (TextView) view.findViewById(R.id.text_startCat);
            } else {

            }

        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
    }
}
