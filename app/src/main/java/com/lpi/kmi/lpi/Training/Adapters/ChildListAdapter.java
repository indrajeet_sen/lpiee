package com.lpi.kmi.lpi.Training.Adapters;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Training.Activities.ContentActivity;
import com.lpi.kmi.lpi.Training.fragment.ProductFragment;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mustafakachwalla on 12/09/17.
 */

public class ChildListAdapter implements ListAdapter {

    Context context;

    List<ChildNodeModel> childModel = new ArrayList<ChildNodeModel>();
    public static LayoutInflater inflater = null;

    public ChildListAdapter(Context context, List<ChildNodeModel> childModel) {
        this.context = context;
        this.childModel = childModel;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return childModel.size();
    }

    @Override
    public Object getItem(int position) {
        return childModel.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ChildNodeModel childNodeModel = (ChildNodeModel) getItem(position);
        final TextView childId, childNode;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.child_node, null);
            childId = (TextView) convertView.findViewById(R.id.childId);
            childNode = (TextView) convertView.findViewById(R.id.childNode);
            childId.setText(childNodeModel.getChildId());
            childNode.setText(childNodeModel.getChildNode());
            convertView.setTag(new ChildListAdapter.ViewHolder(childId, childNode));

        } else {
            ChildNodeAdapter.ViewHolder holder = (ChildNodeAdapter.ViewHolder) convertView.getTag();
            childId = holder.getChildId();
            childNode = holder.getChildNode();
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentActivity.title=childNode.getText().toString();
                StaticVariables.mIdentity=childId.getText().toString();
                StaticVariables.openContent="Yes";

                Fragment fragment = new ProductFragment();
                FragmentManager fragmentManager = ((Activity) context).getFragmentManager();
                fragmentManager.popBackStackImmediate(0, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.replace(R.id.Content_Container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        return convertView;
    }

    public class ViewHolder{
        public TextView childId;
        public TextView childNode;

        public ViewHolder()
        {
        }

        public ViewHolder(TextView childId, TextView childNode) {
            super();
            this.childId = childId;
            this.childNode = childNode;
        }

        public TextView getChildId() {
            return childId;
        }

        public void setChildId(TextView childId) {
            this.childId = childId;
        }

        public TextView getChildNode() {
            return childNode;
        }

        public void setChildNode(TextView childNode) {
            this.childNode = childNode;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}
