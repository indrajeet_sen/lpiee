package com.lpi.kmi.lpi.Training.Adapters;

/**
 * Created by mustafakachwalla on 01/09/17.
 */


public class ChildNodeModel {
    String childId, childNode;

    public ChildNodeModel(String childId, String childNode) {
        this.childId = childId;
        this.childNode = childNode;
    }

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public String getChildNode() {
        return childNode;
    }

    public void setChildNode(String childNode) {
        this.childNode = childNode;
    }
}