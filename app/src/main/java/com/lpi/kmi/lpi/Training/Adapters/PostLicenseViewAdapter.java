package com.lpi.kmi.lpi.Training.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseExpandableListAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Training.Activities.ContentActivity;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.io.File;
import java.util.Calendar;
import java.util.List;

/**
 * Created by darshanad on 8/10/2017.
 */

public class PostLicenseViewAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<TViewGroup> groups;
    private List<TViewChild> children;
    public LinearLayout linearLayoutName;
    public RelativeLayout linearLayoutVideo;

    public PostLicenseViewAdapter(Context context, List<TViewGroup> groups) {
        this.context = context;
        this.groups = groups;
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        try {
            children = groups.get(groupPosition).getItems();
            return children.size();
        } catch (Exception e) {
            //Log.e("", "Exc=" + e.toString());
            return 0;
        }

    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        children = groups.get(groupPosition).getItems();
        return children.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        TViewGroup group = (TViewGroup) getGroup(groupPosition);
        TextView textViewName, textViewIdentity, hasVideo, videoLink;
        ProgressBar spin;
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.group_item, null);
        }
        spin = (ProgressBar) convertView.findViewById(R.id.spin1);
        textViewName = (TextView) convertView.findViewById(R.id.header);
        textViewIdentity = (TextView) convertView.findViewById(R.id.textHeaderIdentity);
        hasVideo = (TextView) convertView.findViewById(R.id.textHeaderVideo);
        videoLink = (TextView) convertView.findViewById(R.id.textHeaderLink);

        textViewName.setText(group.getName());
        textViewIdentity.setText(group.getIdentiy());
        hasVideo.setText(group.getHasVideo());
        videoLink.setText(group.getLink());
//        spin.setVisibility(View.VISIBLE);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        TViewChild child = (TViewChild) getChild(groupPosition, childPosition);
        final TextView textSrno, textValue, textIdentity, textViewMore;
        final VideoView videoview;
        final WebView webView;
        final ProgressBar progressBar;

//        final ImageView info;

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.child_item, null);
        }

        textSrno = (TextView) convertView.findViewById(R.id.srno);
        textValue = (TextView) convertView.findViewById(R.id.childName);
        textIdentity = (TextView) convertView.findViewById(R.id.textChildIdentity);

        linearLayoutName = (LinearLayout) convertView.findViewById(R.id.nameContainer);
        linearLayoutVideo = (RelativeLayout) convertView.findViewById(R.id.videoContainer);


        progressBar = (ProgressBar) convertView.findViewById(R.id.childProgress);
        progressBar.setMax(100);
        videoview = (VideoView) convertView.findViewById(R.id.VideoView);
        textViewMore = (TextView) convertView.findViewById(R.id.textViewMore);
        webView = (WebView) convertView.findViewById(R.id.webViewVideo);
//        info = (ImageView) convertView.findViewById(R.id.getMoreInfo);

        if (TextUtils.isEmpty(child.getText2()) || child.getText2().equals(null) || child.getText2() == "") {
            textValue.setText("NA");
        } else {
            textSrno.setText(child.getText1());
            textValue.setText(child.getText2());
            textIdentity.setText(child.getText3());
        }

        if (groups.get(groupPosition).getName().equals("Health")) {
            linearLayoutName.setVisibility(View.GONE);
            linearLayoutVideo.setVisibility(View.VISIBLE);
            videoview.stopPlayback();
            videoStream(videoview, textViewMore, webView, groups.get(groupPosition).getIdentiy(), progressBar);
        } else {
            videoview.stopPlayback();
            linearLayoutName.setVisibility(View.VISIBLE);
            linearLayoutVideo.setVisibility(View.GONE);
        }

        textViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.stopLoading();
                webView.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                videoview.stopPlayback();
                linearLayoutVideo.setVisibility(View.GONE);

//                StaticVariables.mLob = textValue.getText().toString();
                StaticVariables.mLob = "Health Insurance";
                StaticVariables.mIdentity="100";
                Intent i = new Intent(context, ContentActivity.class);
                i.putExtra("ActivityTitle", "Health Insurance");
                context.startActivity(i);
                ((Activity) context).finish();
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("", "child item is clicked");

                String code = DatabaseHelper.getActivityMaster(context, DatabaseHelper.db, "Lob Open");
                DatabaseHelper.InsertActivityTracker(context, DatabaseHelper.db, StaticVariables.UserLoginId,
                        code, "", "Lob Open", StaticVariables.mLob, "", "",
                        StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "", StaticVariables.UserLoginId, "", "", "", "Pending");
                if (textValue.getText().equals("Health Insurance")) {
////                    Intent i = new Intent(context, LobNavigationDrawer.class);
                    StaticVariables.mLob = textValue.getText().toString();

                } else if (textValue.getText().equals("Term Insurance")) {
                    StaticVariables.mLob = textValue.getText().toString();
                    Intent i = new Intent(context, ContentActivity.class);
                    i.putExtra("ActivityTitle", "Term Life");
                    i.putExtra("SectionId", textIdentity.getText().toString());
                    context.startActivity(i);
                    ((Activity) context).finish();
                } else if (textValue.getText().equals("Currently not available")) {
                    StaticVariables.mLob = "Health Insurance";
                }

            }
        });

        return convertView;

    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }


    public void videoStream(final VideoView videoview, final TextView textView, final WebView webView, final String textIdentity, final ProgressBar progressBar) {
        try {
            // Start the MediaController
            FrameLayout.LayoutParams layoutParams =
                    new FrameLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.gravity = Gravity.BOTTOM;

            MediaController mediacontroller = new MediaController(context);
            mediacontroller.setAnchorView(videoview);
            mediacontroller.setLayoutParams(layoutParams);
            // Get the URL from String VideoURL
            if (getVideoFile().contains("http")) {
                videoview.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setProgress(10);
                WebSettings settings = webView.getSettings();
                settings.setJavaScriptEnabled(true);
                settings.setJavaScriptCanOpenWindowsAutomatically(true);
                settings.setPluginState(WebSettings.PluginState.ON);
                settings.setLoadWithOverviewMode(true);
                settings.setUseWideViewPort(true);
                settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
                webView.clearCache(true);
                webView.clearHistory();
                webView.loadUrl(getVideoFile());

                webView.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        return false;
                    }
                });
                webView.setWebChromeClient(new WebChromeClient() {
                    //                    ProgressDialog mProgress;;
                    @Override
                    public void onProgressChanged(WebView view, int newProgress) {
//                        if (mProgress == null) {
//                            mProgress = new ProgressDialog(context);
//                            mProgress.show();
//                    }
//                        mProgress.setMessage("Loading " + String.valueOf(newProgress) + "%");
                        progressBar.setProgress(newProgress);
                        if (newProgress == 100) {
//                        mProgress.dismiss();
//                        mProgress = null;
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                });

            } else {
                webView.stopLoading();
                webView.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                Uri video = Uri.parse(getVideoFile());
                videoview.setMediaController(mediacontroller);
                videoview.getHolder().setFixedSize(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                videoview.setVideoURI(video);
            }

        } catch (Exception e) {
            Log.e("videoStream", "Error=" + e.getMessage());
        }

        videoview.requestFocus();
        videoview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                videoview.start();
            }
        });

//        videoview.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                videoview.stopPlayback();
//            }
//        });
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoview.stopPlayback();
                progressBar.setVisibility(View.GONE);
                webView.stopLoading();
                webView.destroy();
                StaticVariables.mLob = StaticVariables.productFlag;
                Intent i = new Intent(context, ContentActivity.class);
                i.putExtra("ActivityTitle", "Health");
                i.putExtra("SectionId", "" + 100);
                context.startActivity(i);
                ((Activity) context).finish();
            }
        });
    }

    public String getVideoFile() {
        try {
            String root = context.getExternalFilesDir(Environment.MEDIA_SHARED).toString()  + "/.www/";
//            String root = Environment.getExternalStoragePublicDirectory(
//                    Environment.MEDIA_SHARED).toString() + "/.www/";
            File mFile, mDir;
            mDir = new File(root);
            if (mDir.exists()) {
                mFile = new File(mDir, "Reliance HealthGain Policy - Health.mp4");
                if (mFile.exists()) {
                    return mFile.getAbsolutePath();
                } else {
//                    Toast.makeText(context, "Video not found", Toast.LENGTH_SHORT).show();
                    return "https://www.youtube.com/watch?v=-T4G2E5GoTc&feature=youtu.be";
                }
            } else {
//                Toast.makeText(context, "Video not found", Toast.LENGTH_SHORT).show();
                return "https://www.youtube.com/watch?v=-T4G2E5GoTc&feature=youtu.be";
            }
        } catch (Exception e) {
            Log.e("getVideoFile", "Error=" + e.toString());
            return "https://www.youtube.com/watch?v=-T4G2E5GoTc&feature=youtu.be";
        }
    }
}
