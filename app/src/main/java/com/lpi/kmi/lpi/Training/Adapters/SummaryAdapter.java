package com.lpi.kmi.lpi.Training.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lpi.kmi.lpi.R;

import java.util.ArrayList;

/**
 * Created by BhauraoP on 19/07/2017.
 */

public class SummaryAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> arrayList1=new ArrayList<>();
    ArrayList<String> arrayList2=new ArrayList<>();

    public SummaryAdapter(ArrayList<String> arrayList1, ArrayList<String> arrayList2,Context context) {
        this.arrayList1 = arrayList1;
        this.arrayList2 = arrayList2;
        this.context=context;
    }

    @Override
    public int getCount() {
        return arrayList1.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.summary_item,null);
        TextView textDate= (TextView) view.findViewById(R.id.textDate);
        TextView textContent= (TextView) view.findViewById(R.id.textContent);
        try {
            textDate.setText(arrayList1.get(position));
            textContent.setText(arrayList2.get(position));
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        return view;
    }
}
