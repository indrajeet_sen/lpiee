package com.lpi.kmi.lpi.Training.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Training.Activities.ContentActivity;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Chandrashekhart on 05-09-2016.
 */
public class TViewAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<TViewGroup> groups;
    private List<TViewChild> children;

    public TViewAdapter(Context context, List<TViewGroup> groups) {
        this.context = context;
        this.groups = groups;
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        try {
            children = groups.get(groupPosition).getItems();
            return children.size();
        } catch (Exception e) {
            //Log.e("", "Exc=" + e.toString());
            return 0;
        }

    }


    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        children = groups.get(groupPosition).getItems();
        return children.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        TViewGroup group = (TViewGroup) getGroup(groupPosition);
        TextView textView, textViewId;
        ProgressBar spin;
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.group_item, null);
        }
        spin = (ProgressBar) convertView.findViewById(R.id.spin1);
        textView = (TextView) convertView.findViewById(R.id.header);
        textView.setText(group.getName());
        textViewId = (TextView) convertView.findViewById(R.id.textHeaderIdentity);
        textViewId.setText(group.getIdentiy());
//        spin.setVisibility(View.VISIBLE);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        TViewChild child = (TViewChild) getChild(groupPosition, childPosition);
        final TextView textSrno, textValue, textIdentity;
//        final ImageView info;

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            if (context.getClass().getSimpleName().toString().trim().equalsIgnoreCase("ViewDetailsActivity")) {
                convertView = infalInflater.inflate(R.layout.child_row, null);
            } else {
                convertView = infalInflater.inflate(R.layout.child_item, null);
            }
        }
        textSrno = (TextView) convertView.findViewById(R.id.srno);
        textValue = (TextView) convertView.findViewById(R.id.childName);
        textIdentity = (TextView) convertView.findViewById(R.id.textChildIdentity);
//        info = (ImageView) convertView.findViewById(R.id.getMoreInfo);
        if (TextUtils.isEmpty(child.getText2()) || child.getText2().equals(null) || child.getText2().equalsIgnoreCase("") ||
                child.getText2().toLowerCase().equalsIgnoreCase("null")) {
            textValue.setText("NA");
        } else {
            textSrno.setText(child.getText1());
            textValue.setText(child.getText2());
            textIdentity.setText(child.getText3());
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("", "child item is clicked");
                if (!(context.getClass().getSimpleName().toString().trim().equalsIgnoreCase("ViewDetailsActivity"))) {
                    try {
//                    StaticVariables.mTitle=textValue.getText().toString();
                        StaticVariables.mIdentity = textIdentity.getText().toString();
//                    LobNavigationDrawer.mDrawerLayout.closeDrawer(LobNavigationDrawer.mDrawerList);
                        StaticVariables.mProduct = textIdentity.getText().toString();
                        ContentActivity.title = textValue.getText().toString();
//                    ContentActivity.setTitle(context,ContentActivity.title);

                        String code = DatabaseHelper.getActivityMaster(context, DatabaseHelper.db, "Product List");
                        DatabaseHelper.InsertActivityTracker(context, DatabaseHelper.db, StaticVariables.UserLoginId,
                                code, "", "Product List", StaticVariables.mLob, StaticVariables.mProduct, StaticVariables.mIdentity,
                                StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "", StaticVariables.UserLoginId, "", "", "", "Pending");

                        Intent intent = new Intent(context, ContentActivity.class);
                        intent.putExtra("ActivityTitle", "" + ContentActivity.title);
                        context.startActivity(intent);

                        ((Activity) context).finish();

                    } catch (Exception e) {
                        Log.e("TViewAdapter", "convertView err=" + e.toString());
                    }
                }
            }
        });

        return convertView;

    }


    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }
}
