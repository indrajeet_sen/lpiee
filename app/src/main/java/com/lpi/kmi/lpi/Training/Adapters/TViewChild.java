package com.lpi.kmi.lpi.Training.Adapters;

/**
 * Created by Chandrashekhart on 05-09-2016.
 */
public class TViewChild {
    private String text1;
    private String text2;
    private String text3;

    //    private Bitmap doc;
//    private String doc_name;
    public TViewChild() {
    }

    public String getText3() {
        return text3;
    }

    public void setText3(String text3) {
        this.text3 = text3;
    }

    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public String getText2() {
        return text2;
    }

    public void setText2(String text2) {
        this.text2 = text2;
    }

//    public Bitmap getDoc() {
//        return doc;
//    }
//
//    public void setDoc(Bitmap doc) {
//        this.doc = doc;
//    }
//
//    public String getDoc_name() {
//        return doc_name;
//    }
//
//    public void setDoc_name(String doc_name) {
//        this.doc_name = doc_name;
//    }

}
