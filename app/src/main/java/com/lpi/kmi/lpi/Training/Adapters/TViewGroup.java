package com.lpi.kmi.lpi.Training.Adapters;

import android.widget.ProgressBar;

import java.util.List;

/**
 * Created by Chandrashekhart on 05-09-2016.
 */
public class TViewGroup {
    private String Name,identiy,hasVideo,link;
    private ProgressBar spin;
    private String ee_Status;
    private List<TViewChild> Items;

    private String ee_Name;
    private String ee_Details;

    public String getEe_examID() {
        return ee_examID;
    }

    public void setEe_examID(String ee_examID) {
        this.ee_examID = ee_examID;
    }

    private String ee_examID;

    public String getEe_Details() {
        return ee_Details;
    }

    public void setEe_Details(String ee_Details) {
        this.ee_Details = ee_Details;
    }

    private String ee_Result;
    private String ee_Summery;

    public int getEe_crnt_ExmId() {
        return ee_crnt_ExmId;
    }

    public void setEe_crnt_ExmId(int ee_crnt_ExmId) {
        this.ee_crnt_ExmId = ee_crnt_ExmId;
    }

    private int ee_crnt_ExmId;

    public String getEe_Status() {
        return ee_Status;
    }

    public void setEe_Status(String ee_Status) {
        this.ee_Status = ee_Status;
    }

    public String getEe_Name() {
        return ee_Name;
    }

    public void setEe_Name(String ee_Name) {
        this.ee_Name = ee_Name;
    }

    public String getEe_Result() {
        return ee_Result;
    }

    public void setEe_Result(String ee_Result) {
        this.ee_Result = ee_Result;
    }

    public String getEe_Summery() {
        return ee_Summery;
    }

    public void setEe_Summery(String ee_Summery) {
        this.ee_Summery = ee_Summery;
    }
//    private List<ViewDoc> docItems;

    public TViewGroup() {
    }
    public String getIdentiy() {
        return identiy;
    }

    public void setIdentiy(String identiy) {
        this.identiy = identiy;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getHasVideo() {
        return hasVideo;
    }

    public void setHasVideo(String hasVideo) {
        this.hasVideo = hasVideo;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public ProgressBar getSpin() {
        return spin;
    }

    public void setSpin(ProgressBar spin) {
        this.spin = spin;
    }

    public List<TViewChild> getItems() {
        return Items;
    }

    public void setItems(List<TViewChild> items) {
        Items = items;
    }

    public List<TViewChild> getDocItems() {
        return Items;
    }

    public void setDocItems(List<TViewChild> docItems) {
        this.Items = Items;
    }
}
