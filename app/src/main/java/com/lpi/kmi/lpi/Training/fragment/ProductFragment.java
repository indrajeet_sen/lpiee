package com.lpi.kmi.lpi.Training.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Training.Activities.ContentActivity;
import com.lpi.kmi.lpi.Training.Adapters.ChildListAdapter;
import com.lpi.kmi.lpi.Training.Adapters.ChildNodeModel;
import com.lpi.kmi.lpi.activities.AsyncTasks.GetHtmlContent;
import com.lpi.kmi.lpi.activities.AsyncTasks.syncContentQueries;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;


/**
 * Created by mustafakachwalla on 09/08/17.
 */

public class ProductFragment extends Fragment {
    //        public static final String ARG_PLANET_NUMBER = "planet_number";
//    public AndroidTreeView tView;
//    public View rootView;
//    public RelativeLayout containerView;

    Context context;
    RelativeLayout first_CardView;
    ImageButton btn_regReload, btn_downloadData, btn_showChild, btnBack;
    ImageButton knowMore, getHelp, skipContent, like, dislike, comment,btnSubmit;
    RelativeLayout fragment_container, contentActions,relHasData;
    TextView basicText, moreText, unlockContent, actionText,textCount;
    EditText editAction;
    String section, hasChild,hasData, strText = "", strMoreText = "", isBasHTML = "", isAdvHTML = "";
    RadioGroup grpActions;
    RadioButton rdoMkt, rdoUW, rdoClaims;
    WebView webViewBasic;
    ProgressBar progressBar;
    String queryCat="";
//    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//    public static String htmlHeader = "<!DOCTYPE>\n<html>\n<head>\n<meta name=\"viewport\" " +
//            "content=\"width=device-width, initial-scale=1.0\">\n" +
//            "<link rel=\"stylesheet\" href=\"..\\CSS\\styles.css\">\n " +
//            "<script type=\"text/javascript\">\n" +
//            "    function toggle_visibility(id) {\n" +
//            "       var e = document.getElementById(id);\n" +
//            "       if(e.style.display == 'block')\n" +
//            "          e.style.display = 'none';\n" +
//            "       else\n" +
//            "          e.style.display = 'block';\n" +
//            "    }\n" +
//            "    </script>\n</head>\n<body>\n";
    public static String html = "";
//    public static String htmlFooter = "</body>\n</html>";
    LinearLayout ChildNodes;
    ScrollView contentScrollView;
    int scrollBy = 10;
    public static int count=0,cnt =1;;

    public ProductFragment() {
        // Empty constructor required for fragment subclasses
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        final View view = inflater.inflate(R.layout.layout_social_node, null, false);
        context = container.getContext();
//        if (savedInstanceState != null){
//            return view;
//        }else {
            init(view);

            return view;
//        }
    }



    @SuppressLint("NewApi")
    public void init(View view){
        try {
            final String uniqueId = UUID.randomUUID().toString();
            count=0;
            cnt =1;
            Cursor cursor = DatabaseHelper.db.query(getResources().getString(R.string.TblPostLicModuleData),
                    new String[]{"SectionId", "Desc03", "hasChild", "hasData","Desc01"},
                    "SectionId = ?", new String[]{StaticVariables.mIdentity}, null, null, "SectionId");
            if (cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    section = cursor.getString(0);
                    hasChild = cursor.getString(2);
                    hasData = cursor.getString(3);
                    if(cursor.getString(1).equals("") || cursor.getString(1).equals(null)
                            || cursor.getString(1).equalsIgnoreCase("NULL")){
                        ContentActivity.title = cursor.getString(1);
                    }else {
                        ContentActivity.title = cursor.getString(4);
                    }
                }
            }
            cursor.close();
            ContentActivity.setTitle(context, ContentActivity.title);

            initView(view);

            webViewBasic.setWebViewClient(new MyWebViewClient());

            webViewBasic.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        WebView webView = (WebView) v;
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_BACK:
                                if (webView.canGoBack()) {
                                    webView.goBack();
                                    return true;
                                }
                                break;
                        }
                    }
                    return false;
                }
            });
//
            webViewBasic.setWebChromeClient(new WebChromeClient(){
//                ProgressDialog mProgress;;

                @Override
                public void onProgressChanged(WebView view, int newProgress) {
//                    if (progressBar == null) {
////                        mProgress = new ProgressDialog(context);
////                        mProgress.show();
//                        pr
//                    }
                    progressBar.setProgress(newProgress);
//                    mProgress.setMessage("Loading " + String.valueOf(newProgress) + "%");
                    if (newProgress == 100) {
                        progressBar.setVisibility(View.GONE);
//                        mProgress.dismiss();
//                        mProgress = null;
                    }
                }
            });

//        linearLayout.setVisibility(View.GONE);
            btn_showChild.setVisibility(View.GONE);
//        moreText.setVisibility(View.GONE);
            webViewBasic.setVisibility(View.GONE);
            fragment_container.setVisibility(View.GONE);
            contentActions.setVisibility(View.GONE);

            if (hasChild.equals("Y") && hasData.equals("N")) {
                btn_showChild.setVisibility(View.VISIBLE);
                relHasData.setVisibility(View.GONE);
                showChiled();
            } else if (hasChild.equals("Y") && hasData.equals("Y")) {
                btn_showChild.setVisibility(View.VISIBLE);
                relHasData.setVisibility(View.VISIBLE);
                showChiled();
            }else {
                ChildNodes.setVisibility(View.GONE);
                btn_showChild.setVisibility(View.GONE);
            }

            btn_showChild.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //                LobNavigationDrawer.mTitle = title;
                    //                LobNavigationDrawer.mIdentity = section;

                    //                Fragment fragment = new ProductFragment();
                    //                FragmentManager fragmentManager = ((Activity) context).getFragmentManager();
                    //                FragmentTransaction transaction = fragmentManager.beginTransaction();
                    //                transaction.replace(R.id.content_frame, fragment);
                    //                transaction.addToBackStack(null);
                    //                transaction.commit();
                }
            });

            btn_regReload.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_action_reload_black));

//            webViewBasic.setVisibility(View.VISIBLE);
////                        webViewAdv.loadUrl("file:///" + htmlFile.getAbsolutePath());
//            webViewBasic.loadUrl("file:///android_asset/www/" + "B01"+section + ".html");

            btn_regReload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new GetHtmlContent(context,"ProductFragment").execute();
                }
            });

            btn_downloadData.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CheckCount(section);
                    LoadData();
                }
            });
            knowMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String code = DatabaseHelper.getActivityMaster(context, DatabaseHelper.db, "Advance Reading");
                    DatabaseHelper.InsertActivityTracker(context, DatabaseHelper.db, StaticVariables.UserLoginId,
                            code, "","Advance Reading", StaticVariables.mLob, StaticVariables.mProduct, section,
                            StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "", StaticVariables.UserLoginId, "", "", "", "Pending");
//                    html = CheckLocalHtml(context, "HTML/B"+"01" + section + ".html") ;
                    if (StaticVariables.mLob.equals("Health Insurance")) {

                        if (count>0){
                            if (cnt>9){
//                                html = html + "<br/>" + CheckLocalHtml(context, "HTML/A"+cnt + section + ".html");
                                html = html +  CheckLocalHtml(context, "A"+cnt + section + ".html");
                            }else {
//                                html = html +"<br/>" + CheckLocalHtml(context, "HTML/A"+"0"+cnt + section + ".html");
                                html = html + CheckLocalHtml(context, "A"+"0"+cnt + section + ".html");
                            }
                            count= count-1;
                            cnt=cnt+1;
                            if (count==0){
                                cnt=1;
                                textCount.setVisibility(View.GONE);
                            }
                            LoadWebView();
                            textCount.setText("0"+count);
                        }else {
                            textCount.setVisibility(View.GONE);
                            count=0;
                            cnt=1;
                        }
                    } else if (isAdvHTML.equals("Yes")) {
//                        ReadHTML("A01" + section + ".html");
                        html =  ReadHTML("B01" + section + ".html") + "<br/>" +
                                ReadHTML("A01" + section + ".html");
                        LoadWebView();
//                        webViewBasic.loadDataWithBaseURL(null,html,"text/html", "utf-8", null);
//                        CommonClass.saveHtmlInStorage(context, "content.html", htmlHeader + html + htmlFooter);
//                        webViewBasic.loadUrl("file:///" + CommonClass.checkFile(context, "content.html"));
                    } else {
                        moreText.setText(strMoreText);
                        moreText.setVisibility(View.VISIBLE);
                    }
                    //strMoreText +"</html>";

                }
            });
            getHelp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String code = DatabaseHelper.getActivityMaster(context, DatabaseHelper.db, "Ask Question");
                    DatabaseHelper.InsertActivityTracker(context, DatabaseHelper.db, StaticVariables.UserLoginId,
                            code, "","Ask Question", StaticVariables.mLob, StaticVariables.mProduct, section,
                            StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "", StaticVariables.UserLoginId, "", "", "", "Pending");
                    grpActions.setVisibility(View.GONE);
                    if (contentActions.getVisibility() == View.VISIBLE) {
                        contentActions.setVisibility(View.GONE);
                    } else {
                        actionText.setText("Ask : ");
                        editAction.setText("");
                        editAction.setHint("Query");
                        contentActions.setVisibility(View.VISIBLE);
                    }
                    //Toast.makeText(context, "Button ask question is clicked...!", Toast.LENGTH_SHORT).show();
                }
            });
            skipContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String code = DatabaseHelper.getActivityMaster(context, DatabaseHelper.db, "Skip Content");
                    DatabaseHelper.InsertActivityTracker(context, DatabaseHelper.db, StaticVariables.UserLoginId,
                            code, "","Skip Content", StaticVariables.mLob, StaticVariables.mProduct, section,
                            StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "", StaticVariables.UserLoginId, "", "", "", "Pending");
                    Toast.makeText(context, "Button skip is clicked...!", Toast.LENGTH_SHORT).show();

                    DatabaseHelper.SaveContentQueries(context,DatabaseHelper.db,StaticVariables.UserLoginId,section,
                            "","","","Y","","",uniqueId);

                    fragment_container.setVisibility(View.GONE);
                }
            });
            like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String code = DatabaseHelper.getActivityMaster(context, DatabaseHelper.db, "Like/Unlike");
                    DatabaseHelper.InsertActivityTracker(context, DatabaseHelper.db, StaticVariables.UserLoginId,
                            code, "","Like/Unlike", StaticVariables.mLob, StaticVariables.mProduct, section,
                            StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "Like/Unlike", StaticVariables.UserLoginId, "", "", "", "Pending");
                    DatabaseHelper.SaveContentQueries(context,DatabaseHelper.db,StaticVariables.UserLoginId,section,
                            "","","","","Y","",uniqueId);
//                    Toast.makeText(context, "Button like/unlike is clicked...!", Toast.LENGTH_SHORT).show();
                }
            });
            dislike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String code = DatabaseHelper.getActivityMaster(context, DatabaseHelper.db, "Dislike");
                    DatabaseHelper.InsertActivityTracker(context, DatabaseHelper.db, StaticVariables.UserLoginId,
                            code,"", "Dislike", StaticVariables.mLob, StaticVariables.mProduct, section,
                            StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "Dislike", StaticVariables.UserLoginId, "", "", "", "Pending");
                    DatabaseHelper.SaveContentQueries(context,DatabaseHelper.db,StaticVariables.UserLoginId,section,
                            "","","","","","Y",uniqueId);
//                    Toast.makeText(context, "Button dislike is clicked...!", Toast.LENGTH_SHORT).show();
                }
            });
            comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String code = DatabaseHelper.getActivityMaster(context, DatabaseHelper.db, "Comment");
                    DatabaseHelper.InsertActivityTracker(context, DatabaseHelper.db, StaticVariables.UserLoginId,
                            code,"", "Comment", StaticVariables.mLob, StaticVariables.mProduct, section,
                            StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "Comment", StaticVariables.UserLoginId, "", "", "", "Pending");
                    grpActions.setVisibility(View.GONE);
                    if (contentActions.getVisibility() == View.VISIBLE) {
                        contentActions.setVisibility(View.GONE);
                    } else {
                        actionText.setText("Comment : ");
                        editAction.setText("");
                        editAction.setHint("Comment");
                        contentActions.setVisibility(View.VISIBLE);
                    }

                    ////Toast.makeText(context, "Button comment is clicked...!", Toast.LENGTH_SHORT).show();
                }
            });

            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (actionText.getText().toString().contains("Comment")){
                        DatabaseHelper.SaveContentQueries(context,DatabaseHelper.db,StaticVariables.UserLoginId,section,
                                "Comment","",editAction.getText().toString(),
                                "","","","");
                        if (CommonClass.isConnected(context)){
                            new syncContentQueries(context,"Comment",uniqueId).execute();
                            actionText.setText("");
                            actionText.setHint("Comment");
                        }
                        //Toast.makeText(context, "Comment submitted successfully", Toast.LENGTH_SHORT).show();
                    }else if (actionText.getText().toString().contains("Ask")){

                        DatabaseHelper.SaveContentQueries(context,DatabaseHelper.db,StaticVariables.UserLoginId,section,
                                "Query",queryCat,editAction.getText().toString(),
                                "","","",uniqueId);
                        if (CommonClass.isConnected(context)){
                            new syncContentQueries(context,"Query",uniqueId).execute();
                            actionText.setText("");
                            actionText.setHint("Query");
                        }
                        //Toast.makeText(context, "Query submitted successfully", Toast.LENGTH_SHORT).show();
                    }

                }
            });

            grpActions.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId==R.id.rdoMkt){
                        queryCat=rdoMkt.getText().toString();
                    }else if (checkedId==R.id.rdoUW){
                        queryCat=rdoUW.getText().toString();
                    }else if (checkedId==R.id.rdoClaims){
                        queryCat=rdoClaims.getText().toString();
                    }else {
                        queryCat="";
                    }
                }
            });



//        if (savedInstanceState != null) {
//            String state = savedInstanceState.getString("tState");
//            if (!TextUtils.isEmpty(state)) {
//                view.restoreState(state);
//            }
//        }
        } catch (Exception e) {
            Log.e("ProductFragment", "action err=" + e.toString());
        }
    }

    @SuppressLint("NewApi")
    private void initView(final View view) {
        try {
            first_CardView = (RelativeLayout) view.findViewById(R.id.hasData);
            btn_regReload = (ImageButton) view.findViewById(R.id.btn_regReload);
            btn_downloadData = (ImageButton) view.findViewById(R.id.btn_downloadData);
            btn_showChild = (ImageButton) view.findViewById(R.id.btn_regDetail);
            btnBack = (ImageButton) view.findViewById(R.id.btnBack);
            knowMore = (ImageButton) view.findViewById(R.id.knowMore);
            getHelp = (ImageButton) view.findViewById(R.id.getHelp);
            skipContent = (ImageButton) view.findViewById(R.id.skipContent);
            like = (ImageButton) view.findViewById(R.id.like);
            dislike = (ImageButton) view.findViewById(R.id.dislike);
            comment = (ImageButton) view.findViewById(R.id.comment);
            btnSubmit = (ImageButton) view.findViewById(R.id.btnSubmit);

            grpActions = (RadioGroup) view.findViewById(R.id.grpActions);
            rdoMkt = (RadioButton) view.findViewById(R.id.rdoMkt);
            rdoUW = (RadioButton) view.findViewById(R.id.rdoUW);
            rdoClaims = (RadioButton) view.findViewById(R.id.rdoClaims);
            unlockContent = (TextView) view.findViewById(R.id.unlockContent);

            rdoMkt.setSelected(true);
            queryCat=rdoMkt.getText().toString();

//        linearLayout = (LinearLayout) view.findViewById(R.id.linTable);
            ChildNodes = (LinearLayout) view.findViewById(R.id.ChildNodes);
            contentScrollView = (ScrollView) view.findViewById(R.id.contentScrollView);
            contentScrollView.setFillViewport(true);
//            contentScrollView.setNestedScrollingEnabled(true);
            fragment_container = (RelativeLayout) view.findViewById(R.id.fragment_container);
            contentActions = (RelativeLayout) view.findViewById(R.id.contentActions);
            relHasData = (RelativeLayout) view.findViewById(R.id.hasData);

            basicText = (TextView) view.findViewById(R.id.basicInfo);
            moreText = (TextView) view.findViewById(R.id.moreText);
            textCount = (TextView) view.findViewById(R.id.text_Count);
            textCount.setVisibility(View.GONE);
            actionText = (TextView) view.findViewById(R.id.actionText);
            editAction = (EditText) view.findViewById(R.id.editAction);

//            childList = (ListView) view.findViewById(R.id.childList);
//            childList.setNestedScrollingEnabled(true);
//            childList.setVisibility(View.GONE);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            progressBar.setVisibility(View.GONE);
            progressBar.setMax(100);
            webViewBasic = (WebView) view.findViewById(R.id.webViewBasic);
//            webViewBasic.setInitialScale(getScale());
            webViewBasic.getSettings().setLoadWithOverviewMode(true);
            webViewBasic.getSettings().setJavaScriptEnabled(true);
            webViewBasic.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            webViewBasic.getSettings().setDomStorageEnabled(true);
            webViewBasic.getSettings().setSupportMultipleWindows(true);
            webViewBasic.getSettings().setUseWideViewPort(true);
            webViewBasic.getSettings().setLoadWithOverviewMode(true);
            webViewBasic.getSettings().setSupportZoom(false);
            webViewBasic.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            webViewBasic.getSettings().setBuiltInZoomControls(false);
            webViewBasic.getSettings().setDisplayZoomControls(false);
            webViewBasic.setVerticalScrollBarEnabled(false);
            webViewBasic.setHorizontalScrollBarEnabled(false);
            webViewBasic.clearCache(true);
            webViewBasic.clearHistory();
            webViewBasic.clearView();
            webViewBasic.reload();
        } catch (Exception e) {
            Log.e("ProductFragment", "init err=" + e.toString());
        }
    }

    private void CheckCount(String mIdentity) {
        try {
            String root = null;
            /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                root = Environment.getExternalStoragePublicDirectory(Environment.MEDIA_SHARED).toString()
                        + StaticVariables.PostLicStoragePath;
            } else {
                root = context.getExternalFilesDir(Environment.MEDIA_SHARED) + StaticVariables.PostLicStoragePath;
            }*/
            root = context.getExternalFilesDir(Environment.MEDIA_SHARED) + StaticVariables.PostLicStoragePath;
            File mFile, mDir;
            mDir = new File(root);
            if (mDir.exists()) {
                mFile = new File(mDir, "A01"+mIdentity+".html");
                if (mFile.exists()) {
                    count=count+1;
                }
                mFile = new File(mDir, "A02"+mIdentity+".html");
                if (mFile.exists()) {
                    count=count+1;
                }
                mFile = new File(mDir, "A03"+mIdentity+".html");
                if (mFile.exists()) {
                    count=count+1;
                }
            }

            if (count <= 9 && count!=0) {
                textCount.setText("0" + count);
                textCount.setVisibility(View.VISIBLE);
            } else if (count==0) {
                textCount.setVisibility(View.GONE);
            } else {
                textCount.setText(""+count);
                textCount.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            Log.e("CheckCount", "Error=" + e.toString());
        }
    }

    private void showChiled() {
        List<ChildNodeModel> childNodeModelList = new ArrayList<ChildNodeModel>();
        List<String> childId = new ArrayList<>();
        List<String> childNode = new ArrayList<>();
        Cursor cursor;
        cursor = DatabaseHelper.db.query(context.getResources().getString(R.string.TblPostLicModuleData),
                new String[]{"SectionId", "Desc01"},
                "ParentSectionId =? and SectionId <> ParentSectionId ", new String[]{section}, null, null, null);
        childNodeModelList.clear();
        childId.clear();
        childNode.clear();
        if (cursor.getCount() > 0) {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                childId.add(cursor.getString(0));
                childNode.add(cursor.getString(1));
            }

        }
        cursor.close();
        if (childId.size() > 0) {
            for (int i = 0; i < childId.size(); i++) {
                ChildNodeModel childNodeModel = new ChildNodeModel(childId.get(i), childNode.get(i));
                childNodeModelList.add(childNodeModel);
            }
            ListAdapter listAdapter = new ChildListAdapter(context, childNodeModelList);
            final int adapterCount = listAdapter.getCount();

            for (int i = 0; i < adapterCount; i++) {
                View item = listAdapter.getView(i, null, null);
                ChildNodes.addView(item);
            }
        }
        ChildNodes.setVisibility(View.VISIBLE);
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            int index = url.lastIndexOf('/');
            String file = url.substring(index + 1);
            String root = null;
            /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                root = Environment.getExternalStoragePublicDirectory(Environment.MEDIA_SHARED).toString()
                        + StaticVariables.PostLicStoragePath;
            } else {
                root = context.getExternalFilesDir(Environment.MEDIA_SHARED) + StaticVariables.PostLicStoragePath;
            }*/
            root = context.getExternalFilesDir(Environment.MEDIA_SHARED) + StaticVariables.PostLicStoragePath;
            File htmlFile = CommonClass.CheckHtml(context, root, file);
            if (htmlFile != null) {
                showPopUp(htmlFile);
                return true;
            }
//            else if (CommonClass.checkFile(context, file) != null) {
//                showPopUp(url);
//                return true;
//            }
            else {
                showPopUp(url);
                return true;
            }
        }
    }

    private void LoadData() {
        Cursor cursor = null;
//        cursor = DatabaseHelper.GetData(context, DatabaseHelper.db,cursor,title,section);
        Log.e("SocialViewHolder", "section=" + section);
        cursor = DatabaseHelper.db.query(context.getResources().getString(R.string.TblPostLicModuleData),
                new String[]{"SectionId", "Desc03", "GenDesc01", "AdvDesc01","Desc01"},
                "SectionId=?", new String[]{section}, null, null, null);
        if (cursor.getCount() > 0) {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                if (cursor.isNull(2)) {
                    basicText.setText("");
                    strText = "";
                } else {
                    basicText.setText(cursor.getString(2));
                    strText = cursor.getString(2);
                }

                if (cursor.isNull(3)) {
                    moreText.setText("");
                    strMoreText = "";
                } else {
                    moreText.setText(cursor.getString(3));
                    strMoreText = cursor.getString(3);
                }

                if (strText.contains("<body>")) {
                    isBasHTML = "Yes";
//                    webViewBasic.loadDataWithBaseURL("", cursor.getString(3), "text/html", "utf-8", null);
                } else {
                    isBasHTML = "No";
                }
                if (strMoreText.contains("<body>")) {
                    isAdvHTML = "Yes";
//                    webViewBasic.loadDataWithBaseURL("", cursor.getString(3), "text/html", "utf-8", null);
                } else {
                    isAdvHTML = "No";
                }
            }
        }
        cursor.close();
        html = "";
        if (StaticVariables.mLob.equals("Health Insurance")) {

            html = html + CheckLocalHtml(context, "B01" + section + ".html");
//            webViewBasic.loadDataWithBaseURL(null,htmlHeader+html+htmlFooter,"text/html", "utf-8", null);
            LoadWebView();
            basicText.setVisibility(View.GONE);
        } else if (isBasHTML.equals("Yes")) {

            html = html + ReadHTML("B01" + section + ".html");
            LoadWebView();
        } else {
            basicText.setText(strText);
            webViewBasic.setVisibility(View.GONE);
            basicText.setVisibility(View.VISIBLE);
        }

        if (fragment_container.getVisibility() == View.GONE) {
            btn_downloadData.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_action_lock_open_green));
            btn_regReload.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_action_reload_green));
            unlockContent.setText("Content unlocked");
            String code = DatabaseHelper.getActivityMaster(context, DatabaseHelper.db, "Section Open");
            DatabaseHelper.InsertActivityTracker(context, DatabaseHelper.db, StaticVariables.UserLoginId,
                    code,"", "Section Open", StaticVariables.mLob, StaticVariables.mProduct, section,
                    StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "", StaticVariables.UserLoginId, "", "", "", "Pending");

            code = DatabaseHelper.getActivityMaster(context, DatabaseHelper.db, "Basic Reading");
            DatabaseHelper.InsertActivityTracker(context, DatabaseHelper.db, StaticVariables.UserLoginId,
                    code,"", "Basic Reading", StaticVariables.mLob, StaticVariables.mProduct, section,
                    StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "", StaticVariables.UserLoginId, "", "", "", "Pending");

            fragment_container.setVisibility(View.VISIBLE);
        } else {
            btn_downloadData.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_action_lock_closed_green));
            unlockContent.setText("Content locked");
            fragment_container.setVisibility(View.GONE);
            btn_regReload.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_action_reload_black));
        }
    }

    private void LoadWebView() {
//        CommonClass.saveHtmlInStorage(context, "content.html", htmlHeader + html + htmlFooter);
        CommonClass.saveHtmlInStorage(context, "content.html", html);
//        new SaveHTMLFile(context, "content.html", htmlHeader + html + htmlFooter).execute();
        String root = null;
        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            root = Environment.getExternalStoragePublicDirectory(Environment.MEDIA_SHARED).toString()
                    + StaticVariables.PostLicStoragePath;
        } else {
            root = context.getExternalFilesDir(Environment.MEDIA_SHARED) + StaticVariables.PostLicStoragePath;
        }*/
        root = context.getExternalFilesDir(Environment.MEDIA_SHARED) + StaticVariables.PostLicStoragePath;

        File file = CommonClass.CheckHtml(context, root,"content.html");
        progressBar.setVisibility(View.VISIBLE);
        if (file != null) {
            webViewBasic.loadUrl("file:///" + file.getAbsolutePath());
        } else {
//            webViewBasic.loadDataWithBaseURL(null, htmlHeader + "<p>Page Not Found</p>"+ htmlFooter, "text/html", "utf-8", null);
            webViewBasic.loadDataWithBaseURL(null, "<html><body><p>Page Not Found</p></html></body>", "text/html", "utf-8", null);
        }
        webViewBasic.setVisibility(View.VISIBLE);
//        ContentActivity.scrollButtons.setVisibility(View.VISIBLE);
//        ContentActivity.scrollButtons.bringToFront();
    }

    @SuppressLint("NewApi")
    public String CheckLocalHtml(Context context, String fName) {
        try {
//            String root = Environment.getExternalStoragePublicDirectory(
//                    Environment.MEDIA_SHARED).toString() + "/.www/HTML/";
            String  root = context.getExternalFilesDir(Environment.MEDIA_SHARED) + StaticVariables.PostLicStoragePath+"HTML/";
//        String root = context.getFilesDir() + "/www/";
            File mFile, mDir, content;
//        mDir = new File(context.getFilesDir().getAbsolutePath());
            mDir = new File(root);
            if (mDir.exists()) {
                mFile = new File(mDir, fName);
                if (mFile.exists()) {
                    StringBuilder contentBuilder = new StringBuilder();
                    BufferedReader in = new BufferedReader(new FileReader(mFile));
                    String str = "";
                    while ((str = in.readLine()) != null) {
                        contentBuilder.append(str);
                        contentBuilder.append("\n");
                    }
                    in.close();
                    str = contentBuilder.toString();
//                    s = str.substring(str.indexOf("<body>") + 6,
//                            str.indexOf("</body>") - 6);
//                    s = s.replace("../Images/","Images/");
                    str = str.replace("\\","/");
//                    str = str.replace("//<![CDATA[","//<![CDATA[\n");
//                    str = str.replace("//]]>","//]]>\n");

                    Log.e("","html string ="+str);
                    return str;
                } else {
                    Toast.makeText(context, "Page not found", Toast.LENGTH_SHORT).show();
                    return "Error 404 Page Not Found";
                }
            } else {
                Toast.makeText(context, "Directory not found", Toast.LENGTH_SHORT).show();
                return "Error 404 Page Not Found";
            }
        } catch (Exception e) {
            Log.e("ReadHTML", "Error=" + e.toString());
            return "Error 404 Page Not Found";
        }
    }

    public String ReadHTML(String str) {
        String root = null;
        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            root = Environment.getExternalStoragePublicDirectory(Environment.MEDIA_SHARED).toString()
                    + StaticVariables.PostLicStoragePath;
        } else {
            root = context.getExternalFilesDir(Environment.MEDIA_SHARED) + StaticVariables.PostLicStoragePath;
        }*/
        root = context.getExternalFilesDir(Environment.MEDIA_SHARED) + StaticVariables.PostLicStoragePath;
        File htmlFile = CommonClass.CheckHtml(context,root, str);
        StringBuilder contentBuilder = new StringBuilder();
        try {
            BufferedReader in = new BufferedReader(new FileReader(htmlFile));
            String s = "";
            while ((s = in.readLine()) != null) {
                contentBuilder.append(s);
            }
            in.close();
            return contentBuilder.toString();
        } catch (Exception e) {
            Log.e("ReadHTML", "Error=" + e.toString());
        }
        return "";
    }

    private void showPopUp(String url) {

        // Include dialog.xml file
//        initDialog();\
        LinearLayout  lin= new LinearLayout(context);
        LinearLayout.LayoutParams param;
        WebView webView = new WebView(context);
        final ProgressBar mProgressBar = new ProgressBar(context);

        param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        progressBar.setLayoutParams(param);

        param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        lin.setOrientation(LinearLayout.VERTICAL);
        lin.setLayoutParams(param);
        lin.addView(mProgressBar);
        lin.addView(webView);
        mProgressBar.setProgress(0);
        mProgressBar.setMax(100);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setDisplayZoomControls(false);
        webView.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                mProgressBar.setProgress(newProgress);
                if (newProgress == 100) {
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });
        webView.clearCache(true);
        webView.clearHistory();
        webView.clearView();
        webView.reload();
        webView.loadUrl(url);
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle("Read Content");
        dialog.setView(lin);
        dialog.show();

    }

    public void showPopUp(File htmlFile) {
        // Include dialog.xml file
//        initDialog();
        LinearLayout  lin= new LinearLayout(context);
        LinearLayout.LayoutParams param;
        WebView webView = new WebView(context);
        final ProgressBar mProgressBar = new ProgressBar(context);

        param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        mProgressBar.setLayoutParams(param);

        param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        lin.setOrientation(LinearLayout.VERTICAL);
        lin.setLayoutParams(param);
        lin.addView(mProgressBar);
        lin.addView(webView);
        mProgressBar.setProgress(0);
        mProgressBar.setMax(100);

        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setDisplayZoomControls(false);
        webView.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                mProgressBar.setProgress(newProgress);
                if (newProgress == 100) {
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });
        webView.clearCache(true);
        webView.clearHistory();
        webView.clearView();
        webView.loadUrl("file:///" + htmlFile.getAbsolutePath());
//        dialog.show();
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle("Read Content");
        dialog.setView(lin);
        dialog.show();

    }
}