package com.lpi.kmi.lpi.activities.AsyncTasks;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import androidx.appcompat.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.LPI_ShareAppActivity;
import com.lpi.kmi.lpi.activities.LPI_TrainerMenuActivity;
import com.lpi.kmi.lpi.activities.ParticipantHomeActivity;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;

import net.sqlcipher.Cursor;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import nu.xom.Serializer;

import static com.lpi.kmi.lpi.DBHelper.DatabaseHelper.db;

/**
 * Created by bharatg on 07-Mar-18.
 */
@SuppressLint("Range")
public class AsyncCandidateRegistration extends AsyncTask<String, Void, JSONObject> {
    Context context;
    ProgressDialog mProgressDialog;
    String MobRefId = "", strEmailId = "", password = "";
    Activity mActivity;
    byte[] img_byteArray;
    String flag = "";
    String CandidateID = "", Method = "";
    Callback<String> callback;
    String callFrom = "";

    public AsyncCandidateRegistration(Callback<String> callback, Activity mActivity, Context context, String MobRefId, String strEmailId, String Method,
                                      String callFrom) {
        this.callback = callback;
        this.context = context;
        this.mActivity = mActivity;
        this.MobRefId = MobRefId;
        this.strEmailId = strEmailId;
        this.Method = Method;
        this.callFrom = callFrom;
    }

    public AsyncCandidateRegistration(Activity mActivity, Context context, String MobRefId, String strEmailId, String Method) {
        this.callback = callback;
        this.context = context;
        this.mActivity = mActivity;
        this.MobRefId = MobRefId;
        this.strEmailId = strEmailId;
        this.Method = Method;
    }

    public AsyncCandidateRegistration(Activity mActivity, Context context, String CandidateID, String strEmailId, String Method, String flag) {
        this.context = context;
        this.mActivity = mActivity;
        this.CandidateID = CandidateID;
        this.flag = flag;
        this.strEmailId = strEmailId;
        this.Method = Method;
    }


    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        mProgressDialog = new ProgressDialog(context);
        if (flag.trim().equalsIgnoreCase("ForModification") && flag.length() > 1) {
            mProgressDialog.setMessage("Updating..\nPlease wait..");
        } else {
            mProgressDialog.setMessage("Registering..\nPlease wait..");
        }
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    @Override
    protected JSONObject doInBackground(String... params) {
        // TODO Auto-generated method stub
        String strResponse = "";
        try {

            syncData();

            if (flag.trim().equalsIgnoreCase("ForModification") && flag.length() > 1) {
                strResponse = updateCandidateDetailsToServer(CandidateID);
            } else {
                strResponse = sendCandidateDetailsToServer(MobRefId, strEmailId);
            }
            if (strResponse != null && !strResponse.equalsIgnoreCase("{}")
                    && !strResponse.equalsIgnoreCase("null") &&
                    !strResponse.equalsIgnoreCase("")) {
                return new JSONObject(strResponse);
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AsyncCandidateRegistration", "doInBackground", e.toString());
            return null;
        }
    }

    @SuppressLint("WrongThread")
    @Override
    protected void onPostExecute(JSONObject result) {
        // TODO Auto-generated method stub
        try {
            mProgressDialog.dismiss();
            if (result != null && !result.toString().equalsIgnoreCase("{}")
                    && !result.toString().equalsIgnoreCase("null")
                    && !result.toString().equalsIgnoreCase("")) {
                if (callFrom.equalsIgnoreCase("Login")) {
                    if (result != null) {
                        callback.execute(result.toString(), "success");
                    } else {
                        callback.execute("", "fail");
                    }
                } else {
                    String strJsonArray = result.getJSONArray("Table").toString();
                    JSONArray jsonArray = new JSONArray(strJsonArray);
                    JSONObject jsonObjectError = new JSONObject();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                        jsonObjectError = jsonObject;
                        if (flag.trim().equalsIgnoreCase("ForModification") && flag.length() > 1) {
                            if (jsonObject != null && jsonObject.getString("RespCode").equalsIgnoreCase("Success")) {
                                String CndID = jsonObject.getString("CandidateId");
                                String message = jsonObject.getString("message");
                                ContentValues cv = new ContentValues();
                                cv.clear();
                                cv.put("SyncStatus", "Success");
                                DatabaseHelper.db.update(context.getResources().getString(R.string.TBL_LPICandidateRegistration), cv,
                                        "CandidateId=?", new String[]{CndID});
                                try {
                                    if (CndID != null && CndID.equals(StaticVariables.UserLoginId)) {
                                        Cursor mCursor = db.query(context.getResources().getString(R.string.TBL_LPICandidateRegistration),
                                                null, "CandidateId=?",
                                                new String[]{"" + CndID}, null, null, null, "1");
                                        if (mCursor != null && mCursor.getCount() > 0) {
                                            for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                                                StaticVariables.UserName = mCursor.getString(mCursor.getColumnIndex("FirstName"));
                                                StaticVariables.profileBitmap = mCursor.getBlob(mCursor.getColumnIndex("ImageByte"));
                                                ContentValues values = new ContentValues();
                                                values.put("FirstName", "" + StaticVariables.UserName);
                                                values.put("Image", StaticVariables.profileBitmap);
                                                int ii = DatabaseHelper.db.update(context.getResources().getString(R.string.TBL_iCandidate), values,
                                                        "CandidateLoginId=?", new String[]{StaticVariables.UserLoginId});
                                                Log.e("", "");
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                }
                                alertCustomDialog("Modification Success", "Modification successfully done.\nCandidate ID: " + CndID + "\n");
                            }
                        } else if (jsonObject != null && jsonObject.has("RespCode")) {

                            if (jsonObject.getString("RespCode").equalsIgnoreCase("0")) {

                                String CndID = jsonObject.getString("CandidateId");
                                String MobRef = jsonObject.getString("MobRefId");
                                String AppLink = jsonObject.getString("AppLink");
                                ContentValues cv = new ContentValues();
                                cv.clear();
                                cv.put("CandidateId", CndID);
                                cv.put("AppLink", AppLink);
                                cv.put("SyncStatus", "Success");
                                DatabaseHelper.db.update(context.getResources().getString(R.string.TBL_LPICandidateRegistration), cv,
                                        "MobRefId=?", new String[]{MobRef});

                                Intent intent = new Intent(context, LPI_ShareAppActivity.class);
                                intent.putExtra("initFrom", "LPI_RegistrationActivity");
                                intent.putExtra("candidate_id", CndID);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                context.startActivity(intent);
                                mActivity.finish();

                            } else if (jsonObject != null && jsonObject.getString("RespCode").equalsIgnoreCase("1")) {
//message                   // email id exists
                                alertCustomDialog("LPI EE...!", jsonObject.getString("message") + "\nUser ID: " +
                                        jsonObject.getString("CandidateId"));

                            } else if (jsonObject != null && jsonObject.getString("RespCode").equalsIgnoreCase("2")) {
                                //invalid ref code
                                alertCustomDialog("LPI EE...!", jsonObject.getString("message") + "\nUser ID: " +
                                        jsonObject.getString("CandidateId"));
                            } else if (jsonObject != null && jsonObject.getString("RespCode").equalsIgnoreCase("3")) {
                                //self registration
                                String CndID = jsonObject.getString("UserId");
                                String MobRef = jsonObject.getString("MobRefId");
                                password = jsonObject.getString("Password");

                                ContentValues cv = new ContentValues();
                                cv.clear();
                                cv.put("CandidateId", CndID);
                                cv.put("SyncStatus", "Success");
                                DatabaseHelper.db.update(context.getResources().getString(R.string.TBL_LPICandidateRegistration), cv,
                                        "MobRefId=? and EmailID=?", new String[]{MobRef, strEmailId});

                                StaticVariables.crnt_InscType = jsonObject.getString("Ins_Type");
                                StaticVariables.isLogin = true;
                                StaticVariables.UserName = jsonObject.getString("LegalName");
                                StaticVariables.UserLoginId = CndID;
                                StaticVariables.ParentId = jsonObject.getString("ParentId");
                                StaticVariables.ParentName = jsonObject.getString("ParentName");
                                StaticVariables.UserMobileNo = jsonObject.getString("UserMobileNo1");
                                StaticVariables.offlineMode = "N";
                                StaticVariables.AppLink = jsonObject.getString("AppLink");
                                StaticVariables.UserType = jsonObject.getString("UserType");
                                byte[] imageBytes = null;
                                try {
                                    imageBytes = Base64.decode(jsonObject.getString("Image"), Base64.DEFAULT);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                if (imageBytes != null) {
                                } else {
                                    try {
                                        Drawable drawable = null;
                                        drawable = context.getResources().getDrawable(R.drawable.profile_blank_m);
                                        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                                        imageBytes = stream.toByteArray();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                StaticVariables.profileBitmap = imageBytes;
                                DatabaseHelper.SaveUserInfo(context, DatabaseHelper.db,
                                        jsonObject.getString("UserId"),
                                        password,
                                        jsonObject.getString("LegalName"),
                                        StaticVariables.crnt_InscType,
                                        jsonObject.getString("AllowOfflineMode"),
                                        "" + jsonObject.get("PermisableDays"),
                                        jsonObject.getString("LastAccessDate"),
                                        jsonObject.getString("UserType"),
                                        jsonObject.getString("UserEMailAddress"),
                                        jsonObject.getString("UserMobileNo1"),
                                        jsonObject.getString("ParentId"),
                                        jsonObject.getString("ParentName"),
                                        imageBytes,
                                        jsonObject.getString("AppLink"));

                                String code = DatabaseHelper.getActivityMaster(context, DatabaseHelper.db, "Login");
                                DatabaseHelper.InsertActivityTracker(context, DatabaseHelper.db, StaticVariables.UserLoginId,
                                        code, "", "Login", "", "", "",
                                        StaticVariables.sdf.format(new Date()), StaticVariables.sdf.format(new Date()), "", "", "", "", "", "Pending");

                                try {
                                    if (CommonClass.isConnected(context)) {
                                        new syncExams(new Callback<String>() {
                                            @Override
                                            public void execute(String result, String status) {
                                                new GetAtemptedExams(context, StaticVariables.UserLoginId
                                                        , new Callback() {
                                                    @Override
                                                    public void execute(Object result, String status) {
                                                        Intent intent;
                                                        if (StaticVariables.UserType.equals("T")) {
                                                            intent = new Intent(context, LPI_TrainerMenuActivity.class);
                                                        } else {
                                                            intent = new Intent(context, ParticipantHomeActivity.class);
                                                        }
                                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                        mActivity.startActivity(intent);
                                                        onCancelled();
                                                        ((Activity) mActivity).finish();
                                                    }
                                                }).execute();
                                            }
                                        }, context, "CandidateRegistration", StaticVariables.UserLoginId,"").execute();
                                    } else {
                                        Intent intent;
                                        if (StaticVariables.UserType.equals("T")) {
                                            intent = new Intent(context, LPI_TrainerMenuActivity.class);
                                        } else {
                                            intent = new Intent(context, ParticipantHomeActivity.class);
                                        }
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        mActivity.startActivity(intent);
                                        onCancelled();
                                        ((Activity) mActivity).finish();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else if (jsonObject != null && jsonObject.getString("RespCode").equalsIgnoreCase("4")) {
                                alertCustomDialog("LPI EE...!", jsonObject.getString("message"));
                            }
                        } else {
                            alertCustomDialog("LPI EE...!", "Server not responding.... \nRegistration details will be saved and we will try to sync with the server in background");
                        }
                    }
                }
            } else {
                alertCustomDialog("LPI EE...!", "Server not responding, please try again later.");
            }
        } catch (
                Exception e) {
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AsyncCandidateRegistration", "onPostExecute", e.toString());
            alertCustomDialog("LPI EE...!", "Something went wrong, please try again later.");

        }

    }

//    private void showError(String msg) {
//
//        /*AlertDialog.Builder builder;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            builder = new AlertDialog.Builder(context, R.style.myCoolDialog);
//        } else {
//            builder = new AlertDialog.Builder(context);
//        }
//        builder.setTitle("Alert!");
//        builder.setMessage("Something went wrong please try again.");*/
//        AlertDialog.Builder builder = CommonClass.DialogOpen(context, "Alert!", msg);
//        builder.setCancelable(true);
//        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//                mActivity.onBackPressed();
//            }
//        });
//        AlertDialog alert = builder.create();
//        alert.show();
//    }

    private void alertCustomDialog(String title, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        try {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_exit_dialog, null);
            TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            Button btn_ok = (Button) dialogView.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialogView.findViewById(R.id.btn_no);

            btn_no.setVisibility(View.GONE);
            btn_ok.setText("OK");
            promt_title.setText(title);
            txt_message.setText(message);//"User account created

            final AlertDialog alertDialog = dialog.create();
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);

            if (title.equalsIgnoreCase("Modification Success")) {
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        mActivity.onBackPressed();
                    }
                });
            } else if (title.equalsIgnoreCase("Registration Success")) {
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        mActivity.onBackPressed();
                    }
                });
            } else if (title.equalsIgnoreCase("User account created")) {
                btn_ok.setText("LOGIN");
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();

//                        if (CommonClass.isConnected(context)){
//                            new UserLogin(context,strEmailId,password).execute();
//                        }else {
//                            alertCustomDialog("No Internet", "No internet connectivity, please try again in connected mode");
//                        }

                    }
                });
            } else if (title.equalsIgnoreCase("LPI EE...!")) {
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        onCancelled();
                        mActivity.onBackPressed();
                    }
                });
            } else if (title.equalsIgnoreCase("No Internet")) {
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        onCancelled();
                        mActivity.onBackPressed();
                    }
                });
            }
            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("", "Error" + e.toString());
        }
    }

    private String sendCandidateDetailsToServer(String mobRefId, String strMobile) {
        String strResp = "";
        try {
            Cursor mCursor = null;
            mCursor = db.query(context.getResources().getString(R.string.TBL_LPICandidateRegistration),
                    null, "MobRefId=? and EmailID=? and SyncStatus=?",
                    new String[]{"" + mobRefId, strMobile, "pending"}, null, null, null, "1");


            org.ksoap2.serialization.SoapObject request;
            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            String reqXML = reqBuild(mCursor);

            if (!Method.equals("")) {
                request = new org.ksoap2.serialization.SoapObject(
                        context.getString(R.string.Exam_NAMESPACE), Method);
            } else {
                request = new org.ksoap2.serialization.SoapObject(
                        context.getString(R.string.Exam_NAMESPACE),
                        context.getString(R.string.RegisterCandidateEmail));
            }

            inputPI1.setName("objXMLDoc");
            inputPI1.setValue(reqXML);
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL), 90000);

            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            if (!Method.equals("")) {
                androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                        Method, envelope);
            } else {
                androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                        context.getString(R.string.RegisterCandidateEmail), envelope);
            }
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            strResp = response.toString();

            mCursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AsyncCandidateRegistration", "sendCandidateDetailsToServer", e.toString());
        }
        return strResp;
    }

    private String reqBuild(Cursor mCursor) {

        String str_XML = "";
        try {
            nu.xom.Element root = new nu.xom.Element("RegisterCandidate");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(context));

//        nu.xom.Element InscType = new nu.xom.Element("InscType");
//        InscType.appendChild("" + StaticVariables.crnt_InscType);

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
//        root.appendChild(InscType);

            if (mCursor.getCount() > 0) {
                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {

                    nu.xom.Element FirstName = new nu.xom.Element("FName");
                    FirstName.appendChild(mCursor.getString(mCursor.getColumnIndex("FirstName")));
                    root.appendChild(FirstName);

                    nu.xom.Element LastName = new nu.xom.Element("LName");
                    LastName.appendChild(mCursor.getString(mCursor.getColumnIndex("LastName")));
                    root.appendChild(LastName);

                    nu.xom.Element Gender = new nu.xom.Element("Gender");
                    Gender.appendChild(mCursor.getString(mCursor.getColumnIndex("Gender")));
                    root.appendChild(Gender);

                    nu.xom.Element DOB = new nu.xom.Element("DOB");
                    DOB.appendChild(mCursor.getString(mCursor.getColumnIndex("DOB")));
                    root.appendChild(DOB);

                    nu.xom.Element MobileNo = new nu.xom.Element("Mobile");
                    MobileNo.appendChild(mCursor.getString(mCursor.getColumnIndex("MobileNo")));
                    root.appendChild(MobileNo);

                    nu.xom.Element EmailID = new nu.xom.Element("EmailId");
                    EmailID.appendChild(mCursor.getString(mCursor.getColumnIndex("EmailID")));
                    root.appendChild(EmailID);

                    nu.xom.Element Nationality = new nu.xom.Element("Nationality");
                    Nationality.appendChild(mCursor.getString(mCursor.getColumnIndex("Nationality")));
                    root.appendChild(Nationality);

                    nu.xom.Element Profession = new nu.xom.Element("Profession");
                    Profession.appendChild(mCursor.getString(mCursor.getColumnIndex("Profession")));
                    root.appendChild(Profession);

                    nu.xom.Element UserLogin = new nu.xom.Element("UserLogin");
                    UserLogin.appendChild(StaticVariables.UserLoginId);
                    root.appendChild(UserLogin);

                    nu.xom.Element Password = new nu.xom.Element("UserPin");
                    Password.appendChild(mCursor.getString(mCursor.getColumnIndex("Password")));
                    root.appendChild(Password);

                    nu.xom.Element Device = new nu.xom.Element("Device");
                    Device.appendChild(CommonClass.getIMEINumber(context));
                    root.appendChild(Device);

                    nu.xom.Element CreatedBy = new nu.xom.Element("ParentId");
                    CreatedBy.appendChild(mCursor.getString(mCursor.getColumnIndex("CreatedBy")));
                    root.appendChild(CreatedBy);

                    nu.xom.Element MobRefId = new nu.xom.Element("MobRefId");
                    MobRefId.appendChild(mCursor.getString(mCursor.getColumnIndex("MobRefId")));
                    root.appendChild(MobRefId);

                    nu.xom.Element RefCode = new nu.xom.Element("RefCode");
                    RefCode.appendChild(mCursor.getString(mCursor.getColumnIndex("RefCode")));
                    root.appendChild(RefCode);

                    img_byteArray = mCursor.getBlob(mCursor.getColumnIndex("ImageByte"));

                    if (img_byteArray != null) {
                    } else {
                        try {
                            Drawable drawable = null;
                            drawable = context.getResources().getDrawable(R.drawable.profile_blank_m);
                            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                            img_byteArray = stream.toByteArray();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    nu.xom.Element Image = new nu.xom.Element("Image");
                    Image.appendChild(Base64.encodeToString(img_byteArray, Base64.NO_WRAP));
                    root.appendChild(Image);
                }
            } else {
                nu.xom.Element FirstName = new nu.xom.Element("FName");
                FirstName.appendChild(mCursor.getString(mCursor.getColumnIndex("FirstName")));
                root.appendChild(FirstName);

                nu.xom.Element EmailID = new nu.xom.Element("EmailId");
                EmailID.appendChild(mCursor.getString(mCursor.getColumnIndex("EmailID")));
                root.appendChild(EmailID);

                nu.xom.Element MobRefId = new nu.xom.Element("MobRefId");
                MobRefId.appendChild(mCursor.getString(mCursor.getColumnIndex("MobRefId")));
                root.appendChild(MobRefId);

                nu.xom.Element RefCode = new nu.xom.Element("RefCode");
                RefCode.appendChild(mCursor.getString(mCursor.getColumnIndex("RefCode")));
                root.appendChild(RefCode);
            }


            nu.xom.Document doc = new nu.xom.Document(root);

            OutputStream out = new ByteArrayOutputStream();
            Serializer serializer = new Serializer(System.out, "UTF-8");
            serializer.setOutputStream(out);
            serializer.setIndent(4);
            serializer.setMaxLength(64);
            serializer.write(doc);
            str_XML = out.toString();
            str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
            str_XML = str_XML.replaceAll("\n", "");
            str_XML = str_XML.replaceAll("\r", "");
            str_XML = str_XML.replaceAll("     ", "");
            str_XML = str_XML.replaceAll("         ", "");
            str_XML = str_XML.trim();
            Log.e("", "XMLStr==" + str_XML);
        } catch (IOException e) {
            Log.e("", "RegisterCandidate Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, db,
                    "AsyncCandidateRegistration", "reqBuild", e.toString());
        }
        return str_XML;
    }


    private String updateCandidateDetailsToServer(String candidateID) {
        String strResp = "";
        try {
            Cursor mCursor = null;
            mCursor = db.query(context.getResources().getString(R.string.TBL_LPICandidateRegistration),
                    null, "CandidateId=?",
                    new String[]{"" + candidateID}, null, null, null, "1");

            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.ModifyCandidate));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();

            String reqXML = updateBuild(mCursor);
            inputPI1.setName("objXMLDoc");
            inputPI1.setValue(reqXML);
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));
            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                    context.getString(R.string.ModifyCandidate), envelope);
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            strResp = response.toString();

            mCursor.close();

        } catch (Exception e) {
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AsyncCandidateRegistration", "updateCandidateDetailsToServer", e.toString());
        }
        return strResp;
    }

    private String updateBuild(Cursor mCursor) {

        String str_XML = "";
        try {
            nu.xom.Element root = new nu.xom.Element("ModifyCandidate");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(context));

            nu.xom.Element InscType = new nu.xom.Element("InscType");
            InscType.appendChild("" + StaticVariables.crnt_InscType);

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
            root.appendChild(InscType);

            for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {

                nu.xom.Element CandidateId = new nu.xom.Element("CandidateId");
                CandidateId.appendChild(mCursor.getString(mCursor.getColumnIndex("CandidateId")));
                root.appendChild(CandidateId);

                nu.xom.Element FirstName = new nu.xom.Element("FName");
                FirstName.appendChild(mCursor.getString(mCursor.getColumnIndex("FirstName")));
                root.appendChild(FirstName);

                nu.xom.Element LastName = new nu.xom.Element("LName");
                LastName.appendChild(mCursor.getString(mCursor.getColumnIndex("LastName")));
                root.appendChild(LastName);

                nu.xom.Element Gender = new nu.xom.Element("Gender");
                Gender.appendChild(mCursor.getString(mCursor.getColumnIndex("Gender")));
                root.appendChild(Gender);

                nu.xom.Element DOB = new nu.xom.Element("DOB");
                DOB.appendChild(mCursor.getString(mCursor.getColumnIndex("DOB")));
                root.appendChild(DOB);

                nu.xom.Element MobileNo = new nu.xom.Element("Mobile");
                MobileNo.appendChild(mCursor.getString(mCursor.getColumnIndex("MobileNo")));
                root.appendChild(MobileNo);

                nu.xom.Element EmailID = new nu.xom.Element("EmailId");
                EmailID.appendChild(mCursor.getString(mCursor.getColumnIndex("EmailID")));
                root.appendChild(EmailID);

                nu.xom.Element Nationality = new nu.xom.Element("Nationality");
                Nationality.appendChild(mCursor.getString(mCursor.getColumnIndex("Nationality")));
                root.appendChild(Nationality);

                nu.xom.Element Profession = new nu.xom.Element("Profession");
                Profession.appendChild(mCursor.getString(mCursor.getColumnIndex("Profession")));
                root.appendChild(Profession);
                img_byteArray = mCursor.getBlob(mCursor.getColumnIndex("ImageByte"));

                if (img_byteArray != null) {
                } else {
                    try {
                        Drawable drawable = null;
                        drawable = context.getResources().getDrawable(R.drawable.profile_blank_m);
                        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                        img_byteArray = stream.toByteArray();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                nu.xom.Element Image = new nu.xom.Element("Image");
                Image.appendChild(Base64.encodeToString(img_byteArray, Base64.NO_WRAP));
                root.appendChild(Image);
            }


            nu.xom.Document doc = new nu.xom.Document(root);

            OutputStream out = new ByteArrayOutputStream();
            Serializer serializer = new Serializer(System.out, "UTF-8");
            serializer.setOutputStream(out);
            serializer.setIndent(4);
            serializer.setMaxLength(64);
            serializer.write(doc);
            str_XML = out.toString();
            str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
            str_XML = str_XML.replaceAll("\n", "");
            str_XML = str_XML.replaceAll("\r", "");
            str_XML = str_XML.replaceAll("     ", "");
            str_XML = str_XML.replaceAll("         ", "");
            str_XML = str_XML.trim();
            Log.e("", "XMLStr==" + str_XML);
        } catch (IOException e) {
            Log.e("", "ModifyCandidate Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, db,
                    "AsyncCandidateRegistration", "ModifyCandidate", e.toString());
        }
        return str_XML;
    }

    private void syncData() {
        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getResources().getString(R.string.Exam_NAMESPACE), context.getResources().getString(R.string.GetActivityMaster));
            //property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            inputPI1.setName("objXMLDoc");
            String objXMLDoc = CommonClass.GenerateXML(context, 0, "GetActivityMaster", new String[]{""}, new String[]{""});
            inputPI1.setValue(objXMLDoc);
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);
            envelope.dotNet = true;
            //Set output SOAP object
            envelope.setOutputSoapObject(request);
            //Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getResources().getString(R.string.Exam_URL));
//        InputStream isResponse = null;

            //Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getResources().getString(R.string.Exam_SOAP_ACTION) + context.getResources().getString(R.string.GetActivityMaster), envelope);
            //Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            JSONObject objJsonObject = new JSONObject(response.toString());
            JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");
            for (int index = 0; index < objJsonArray.length(); index++) {
                JSONObject jsonobject = objJsonArray.getJSONObject(index);
                DatabaseHelper.InsertActivityMaster(context.getApplicationContext(), DatabaseHelper.db,
                        jsonobject.getString("ActivityCode"),
                        jsonobject.getString("ActivityDesc"));
            }


        } catch (Exception e) {
            Log.e("Login", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context.getApplicationContext(), DatabaseHelper.db,
                    "AsyncCandidateRegistration", "sync data", e.toString());
//            onDestroy();
        }
    }
}