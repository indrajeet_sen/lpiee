//package com.lpi.kmi.lpi.activities.AsyncTasks;
//
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.os.AsyncTask;
//import android.util.Log;
//
//import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
//import com.lpi.kmi.lpi.R;
//import com.lpi.kmi.lpi.classes.Callback;
//import com.lpi.kmi.lpi.classes.CommonClass;
//import com.lpi.kmi.lpi.classes.FakeX509TrustManager;
//
//import org.ksoap2.SoapEnvelope;
//import org.ksoap2.serialization.PropertyInfo;
//import org.ksoap2.serialization.SoapPrimitive;
//import org.ksoap2.serialization.SoapSerializationEnvelope;
//import org.ksoap2.transport.HttpTransportSE;
//
//import java.io.ByteArrayOutputStream;
//import java.io.IOException;
//import java.io.OutputStream;
//
//import nu.xom.Serializer;
//
//public class AsyncCheckAppVersion extends AsyncTask<String, String, String> {
//    public static String UserID;
//    Context context;
//    ProgressDialog mProgressDialog;
//    Callback<String> callback;
//
//
//    public AsyncCheckAppVersion(Context context,
//                                String UserID, Callback<String> callback) {
//        this.context = context;
//        this.callback = callback;
//        this.UserID = UserID;
//    }
//
//    @Override
//    protected void onPreExecute() {
//        // TODO Auto-generated method stub
//        super.onPreExecute();
//        mProgressDialog = new ProgressDialog(context);
//        mProgressDialog.setMessage("Checking Version..\nPlease wait..");
//        mProgressDialog.setCancelable(true);
//        mProgressDialog.show();
//
//    }
//
//    @Override
//    protected String doInBackground(String... params) {
//        // TODO Auto-generated method stub
//        String strResponse = "";
//        try {
//            strResponse = CheckVersionWithServer();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
//                    "AsyncCheckAppVersion", "onPostExecute", e.toString());
//        }
//        return strResponse;
//    }
//
//    @Override
//    protected void onPostExecute(String result) {
//        mProgressDialog.dismiss();
//        if (context != null) {
//            callback.execute(result, result);
//            /*if (strResponse != null && !strResponse.toString().equalsIgnoreCase("{}")
//                    && !strResponse.toString().equalsIgnoreCase("null")
//                    && !strResponse.toString().equalsIgnoreCase("")) {
//
//                JSONObject jObject = new JSONObject(strResponse);
//                JSONArray jsonArray = jObject.getJSONArray("Table");
//
//                byte[] imageBytes = null;
//                for (int i = 0; i < jsonArray.length(); i++) {
//                    JSONObject jsonObject = new JSONObject(jsonArray.get(i).toString());
//                    if (jsonObject.getString("ResponseCode").equals("0")) {
//
//
//                    } else {
//                        status = "Failed";
//                    }
//                }
//            }*/
//        }
//    }
//
//    @Override
//    protected void onCancelled() {
//        super.onCancelled();
//    }
//
//    private String CheckVersionWithServer() {
//        String strResp = "";
//        try {
//            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
//                    context.getString(R.string.Exam_NAMESPACE),
//                    context.getString(R.string.CheckAppVersion));
//
//            // property which holds input parameters
//            PropertyInfo inputPI1 = new PropertyInfo();
//            String strReqXML = reqBuild();
//            inputPI1.setName("objXMLDoc");
//            inputPI1.setValue(strReqXML);
//            inputPI1.setType(String.class);
//            request.addProperty(inputPI1);
//
//            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
//                    SoapEnvelope.VER11);
//            envelope.dotNet = true;
//            envelope.setOutputSoapObject(request);
//            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));
//
//            FakeX509TrustManager.allowAllSSL();
////            FakeX509TrustManager.trustSSLCertificate((Activity) context);
//            androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
//                    context.getString(R.string.CheckAppVersion), envelope);
//            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
//            strResp = response.toString();
//        } catch (Exception e) {
//            e.printStackTrace();
//            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
//                    "AsyncGetFAQ", "getFAQFromServer()", e.toString());
//        }
//        return strResp;
//    }
//
//    private String reqBuild() {
//
//        String str_XML = "";
//        try {
//            nu.xom.Element root = new nu.xom.Element("CheckAppVersion");
//
//            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
//            AppUserId.appendChild("LPIUser");
//
//            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
//            AppPassword.appendChild("pass@123");
//
//            nu.xom.Element AppID = new nu.xom.Element("AppID");
//            AppID.appendChild("1");
//
//            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
//            CallingMode.appendChild("Mobile");
//
//            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
//            ServiceVersionID.appendChild("1");
//
//            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
//            UniqRefID.appendChild("121");
//
//            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
//            IMEINo.appendChild(CommonClass.getIMEINumber(context));
//
//            nu.xom.Element InscType = new nu.xom.Element("InscType");
//            InscType.appendChild("");
//
//            nu.xom.Element UserID = new nu.xom.Element("UserID");
//            UserID.appendChild("" + AsyncCheckAppVersion.UserID);
//
//            nu.xom.Element AppVersion = new nu.xom.Element("AppVersion");
//            try {
//                AppVersion.appendChild("" + (context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//
//            root.appendChild(AppUserId);
//            root.appendChild(AppPassword);
//            root.appendChild(AppID);
//            root.appendChild(CallingMode);
//            root.appendChild(ServiceVersionID);
//            root.appendChild(UniqRefID);
//            root.appendChild(IMEINo);
//            root.appendChild(InscType);
//            root.appendChild(UserID);
//            root.appendChild(AppVersion);
//
//            nu.xom.Document doc = new nu.xom.Document(root);
//
//            OutputStream out = new ByteArrayOutputStream();
//            Serializer serializer = new Serializer(System.out, "UTF-8");
//            serializer.setOutputStream(out);
//            serializer.setIndent(4);
//            serializer.setMaxLength(64);
//            serializer.write(doc);
//            str_XML = out.toString();
//            str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
//            str_XML = str_XML.replaceAll("\n", "");
//            str_XML = str_XML.replaceAll("\r", "");
//            str_XML = str_XML.replaceAll("     ", "");
//            str_XML = str_XML.replaceAll("         ", "");
//            str_XML = str_XML.trim();
//            Log.e("", "XMLStr==" + str_XML);
//        } catch (IOException e) {
//            Log.e("", "CheckVersionWithServer Error=" + e.toString());
//            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
//                    "AsyncCheckAppVersion", "CheckVersionWithServer", e.toString());
//        }
//        return str_XML;
//    }
//}