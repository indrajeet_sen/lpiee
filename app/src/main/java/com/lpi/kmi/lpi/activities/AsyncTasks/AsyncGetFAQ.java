package com.lpi.kmi.lpi.activities.AsyncTasks;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import nu.xom.Serializer;

@SuppressLint("Range")
public class AsyncGetFAQ extends AsyncTask<String, String, String> {
    public static String grpFlag, CandidateId, TrainerId, UserType;
    Context context;
    ProgressDialog mProgressDialog;
    Callback<String> callback;

    public AsyncGetFAQ(Context context) {
        this.context = context;
    }

    public AsyncGetFAQ(Callback<String> callback, Context context, String grpFlag,
                       String CandidateId, String TrainerId, String UserType) {
        this.context = context;
        this.callback = callback;
        this.grpFlag = grpFlag;
        this.CandidateId = CandidateId;
        this.TrainerId = TrainerId;
        this.UserType = UserType;
    }

    public AsyncGetFAQ() {
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage("Loading..\nPlease wait..");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

    }

    @Override
    protected String doInBackground(String... params) {
        // TODO Auto-generated method stub
        String strResponse = "";
        String status = "";
        try {
            strResponse = getFAQFromServer();
//                mArraylistEventslistComplete.clear();
//                mArraylistEventslistIncomplete.clear();
            if (strResponse != null && !strResponse.toString().equalsIgnoreCase("{}")
                    && !strResponse.toString().equalsIgnoreCase("null")
                    && !strResponse.toString().equalsIgnoreCase("")) {

                JSONObject jObject = new JSONObject(strResponse);
                JSONArray jsonArray = jObject.getJSONArray("Table");

                byte[] imageBytes = null;
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = new JSONObject(jsonArray.get(i).toString());
                    if (jsonObject.getString("ResponseCode").equals("0")) {

                        //CandidateId	QuesCount	TrainerId	CandidateName	TrainerName	CndMobile	TrnMobile	ResponseCode	ListType	Images

                        status = "Success";
                        try {
                            String strIMG = "" + jsonObject.get("Images");
                            if (strIMG == null || strIMG.equals(null) || strIMG.equals("null") || strIMG.equalsIgnoreCase("")) {

                                Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.profile_blank_m);
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                                imageBytes = stream.toByteArray();
                                stream.flush();
                                stream.close();

                            } else {
                                imageBytes = Base64.decode(jsonObject.getString("Images"), Base64.NO_WRAP);
                            }
                        } catch (Exception e) {
                            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                                    "AsyncGetFAQ", "onPostExecute>Image", e.toString());
                        }

                        if (jsonObject.has("ListType")) {
                            DatabaseHelper.SaveFAQGroup(context, DatabaseHelper.db, (jsonObject.getString("CandidateId")).trim(),
                                    "" + jsonObject.getInt("QuesCount"), jsonObject.getString("TrainerId"), jsonObject.getString("CandidateName"),
                                    jsonObject.getString("TrainerName"), jsonObject.getString("CndMobile"), jsonObject.getString("TrnMobile"),
                                    imageBytes,"" + (jsonObject.get("RespDtime")));
                        } else {
                            DatabaseHelper.SaveFAQList(context, DatabaseHelper.db, (jsonObject.getString("CandidateId")).trim(),
                                    "" + (jsonObject.getInt("QuesId")), (jsonObject.getString("TrainerId")).trim(),
                                    (jsonObject.getString("QuesDesc")).trim(), (jsonObject.getString("RespDesc")).trim(),
                                    (jsonObject.getString("RespDtime")).trim(), (jsonObject.getString("RespFrom")).trim(),
                                    (jsonObject.getString("isClosed")).trim(), (jsonObject.getString("Name")).trim(),
                                    (jsonObject.getString("Mobile")).trim(), imageBytes, "" + (jsonObject.getInt("RN")),
                                    (jsonObject.getString("MobRefId")).trim(), "" + (jsonObject.get("RespDtime")), "Complete");
                        }
                    } else {
                        status = "Failed";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AsyncGetFAQ", "onPostExecute", e.toString());
            status = "Failed";
        }
        return status;
    }

    @Override
    protected void onPostExecute(String result) {
        mProgressDialog.dismiss();
        if (context != null) {
            callback.execute(result, result);
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    private String getFAQFromServer() {
        String strResp = "";
        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.FAQ));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            String strReqXML = reqBuild();
            inputPI1.setName("objXMLDoc");
            inputPI1.setValue(strReqXML);
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));


            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                    context.getString(R.string.FAQ), envelope);
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            strResp = response.toString();
        } catch (Exception e) {
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AsyncGetFAQ", "getFAQFromServer()", e.toString());
        }
        return strResp;
    }

    private String reqBuild() {

        String str_XML = "";
        try {
            nu.xom.Element root = new nu.xom.Element("GetFAQList");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(context));

            nu.xom.Element InscType = new nu.xom.Element("InscType");
            InscType.appendChild("");

            nu.xom.Element ParentId = new nu.xom.Element("ParentId");
            ParentId.appendChild("" + AsyncGetFAQ.TrainerId);

            nu.xom.Element ChildId = new nu.xom.Element("ChildId");
            ChildId.appendChild("" + AsyncGetFAQ.CandidateId);

            nu.xom.Element UserType = new nu.xom.Element("UserType");
            UserType.appendChild("" + AsyncGetFAQ.UserType);

            nu.xom.Element ListType = new nu.xom.Element("ListType");
            ListType.appendChild("" + AsyncGetFAQ.grpFlag);

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
            root.appendChild(InscType);

            root.appendChild(ParentId);
            root.appendChild(ChildId);
            root.appendChild(UserType);
            root.appendChild(ListType);

//            nu.xom.Element UserId = new nu.xom.Element("UserId");
//            UserId.appendChild(StaticVariables.UserLoginId);
////            UserId.appendChild("70011354");
//            root.appendChild(UserId);

            nu.xom.Document doc = new nu.xom.Document(root);

            OutputStream out = new ByteArrayOutputStream();
            Serializer serializer = new Serializer(System.out, "UTF-8");
            serializer.setOutputStream(out);
            serializer.setIndent(4);
            serializer.setMaxLength(64);
            serializer.write(doc);
            str_XML = out.toString();
            str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
            str_XML = str_XML.replaceAll("\n", "");
            str_XML = str_XML.replaceAll("\r", "");
            str_XML = str_XML.replaceAll("     ", "");
            str_XML = str_XML.replaceAll("         ", "");
            str_XML = str_XML.trim();
            Log.e("", "XMLStr==" + str_XML);
        } catch (IOException e) {
            Log.e("", "getFAQFromServer Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AsyncSearchCandidates", "getFAQFromServer", e.toString());
        }
        return str_XML;
    }
}