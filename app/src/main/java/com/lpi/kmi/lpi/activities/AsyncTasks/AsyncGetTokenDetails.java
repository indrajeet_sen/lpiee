package com.lpi.kmi.lpi.activities.AsyncTasks;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

@SuppressLint("Range")
public class AsyncGetTokenDetails extends AsyncTask<String, String, String> {

    public final Callback<String> callback;
    Context context;
    ProgressDialog progressDialog;
    String strResponse = "", UserID = "";

    public AsyncGetTokenDetails() {
        callback = null;
    }

    public AsyncGetTokenDetails(Callback<String> callback, Context context, String UserID) {
        this.callback = callback;
        this.context = context;
        this.UserID = UserID;
//        this.OTP = OTP;
//        this.Password=Password;
//        progressDialog = new ProgressDialog(this.context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressDialog = new ProgressDialog(this.context, R.style.AppTheme_Dark_Dialog);
        } else {
            progressDialog = new ProgressDialog(this.context);
        }
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Looking for tokens");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.GetTokenDetails));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
//,"OTP","Password",OTP,Password
            inputPI1.setName("objXMLDoc");
            try {
                String objXMLDoc = CommonClass.GenerateXML(context, 1, "GetTokenDetails",
                        new String[]{"UserID"}, new String[]{UserID});
                inputPI1.setValue(objXMLDoc);
            } catch (Exception e1) {
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "AsyncGetTokenDetails", "doInBackground()", e1.toString());
            }
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            // Set output SOAP object
            envelope.setOutputSoapObject(request);
            // Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));

            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                    context.getString(R.string.GetTokenDetails), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            // Assign it to static variable
            String sResponse = response.toString();
//            Log.e("", "response=" + sResponse);

            try {
                JSONObject jObject = new JSONObject(sResponse);
                JSONArray jsonArray = jObject.getJSONArray("Table");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = new JSONObject(jsonArray.get(i).toString());
                    if (jsonObject.has("ResponseCode") && jsonObject.get("ResponseCode").equals("0")) {
                        DatabaseHelper.SaveTokens(context, DatabaseHelper.db, StaticVariables.UserLoginId,
                                "" + jsonObject.get("Token"), "" + jsonObject.get("TokenType"), "" + jsonObject.get("ValidFrom"),
                                "" + jsonObject.get("ValidTo"), "" + jsonObject.get("TotalCount"), "" + jsonObject.get("TokenUsed"),
                                "" + jsonObject.get("isActive"),"" + jsonObject.get("isExpired"),
                                "" + jsonObject.get("CreatedBy"), "" + jsonObject.get("CreatedDtime"));
                        strResponse = "success";
                    } else if (jsonObject.has("ResponseCode") && jsonObject.get("ResponseCode").equals("1") && jsonObject.has("RespDesc")) {
                        strResponse = "" + jsonObject.get("RespDesc");
                    } else {
                        strResponse = "fail";
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                strResponse = "error";
            }

            return strResponse;
        } catch (Exception e) {
            Log.e("", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AsyncGetTokenDetails", "doInBackground()", e.toString());
            return "error";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
        strResponse = (result.toString());
        callback.execute(strResponse, "");
    }
}
