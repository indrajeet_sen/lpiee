package com.lpi.kmi.lpi.activities.AsyncTasks;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import androidx.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;

import net.sqlcipher.Cursor;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by Bharatg on 15/02/18.
 */

@SuppressLint("Range")
public class AsyncSubmitExams extends AsyncTask<String, String, String> {
    static boolean isSuccess = false;
    static int mNotificationId = 000;
    Context context;
    ProgressDialog progressDialog;
    String strResponse = "";
    String activityName = "";
    String ExamID = "", AttemptId = "", CandidateId = "";
    String notification = "";
    NodeList nList = null;
    InputStream responseStream;

    public AsyncSubmitExams(Context context, String name) {
        this.context = context;
        this.activityName = name;
        isSuccess = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressDialog = new ProgressDialog(this.context, R.style.AppTheme_Dark_Dialog);
        } else {
            progressDialog = new ProgressDialog(this.context);
        }

    }

    @Override
    protected void onPreExecute() {
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Synchronising Data\nPlease wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }


    @SuppressLint("NewApi")
    @Override
    protected String doInBackground(String... strings) {
        strResponse = "";
        try {
            Cursor cursor = DatabaseHelper.GetXMLData(context, DatabaseHelper.db);

            if (cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    SyncOfflineData(cursor.getString(2), cursor.getString(0), cursor.getString(1));
                }
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AsyncSubmitExams", "doInBackground", e.toString());
        }
        return strResponse;
    }

    @Override
    protected void onPostExecute(String s) {
//        super.onPostExecute(s);
        progressDialog.dismiss();
        if (isSuccess) {
            Toast.makeText(context, "Offline Exam data are successfully synced.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void SyncOfflineData(String method, String recId, String xmlData) {
        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE), method);

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();

            inputPI1.setName("objXMLDoc");
            inputPI1.setValue(xmlData);
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            // Set output SOAP object
            envelope.setOutputSoapObject(request);
            // Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));
            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                    method, envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString().trim();
            Log.e("", "response=" + strResponse);
            InputStream isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            responseStream = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            if (IsCorrectXMLResponse(isResponse)) {
                if (method.equals(context.getString(R.string.SubmitExam))) {
                    nList = CommonClass.GetXMLElementNodeList(responseStream, "SubmitExam");
                    if (nList == null) {

                    } else if (nList.getLength() > 0) {
                        for (int i = 0; i < nList.getLength(); i++) {
                            Node node = nList.item(i);
                            notification = "";
                            if (node.getNodeType() == Node.ELEMENT_NODE) {
                                Element element = (Element) node;
                                if (getValue("ResponseCode", element).equals("0")) {
                                    ExamID = getValue("ExamId", element);
                                    AttemptId = getValue("AttemptId", element);
                                    CandidateId = getValue("CandidateId", element);
                                    if (getValue("ExamId", element).equals("4001")) {
                                        notification = "Psychometric profiling";
                                    } else if (getValue("ExamId", element).equals("3001")) {
                                        notification = "IRDAI Section-3 exam ";
                                    }
                                    if (StaticVariables.Notification.equals("Yes"))
                                        CommonClass.createNotification(context, "Exam Submitted", notification + " submitted successfully", "");
                                    DatabaseHelper.UpdateXMLData(context, DatabaseHelper.db, recId, method, "Success");
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e("", "AsyncSubmitExams Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AsyncSubmitExams", "AsyncSubmitExams.doInBackground()", e.toString());
        }
    }


    // TODO: Check the Response Code
    public boolean IsCorrectXMLResponse(InputStream strXML) {
        boolean isZeroResponseCode = false;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(strXML);
            Element element = doc.getDocumentElement();
            element.normalize();
            String ResponseCode = getValue("ResponseCode", element);
            if (ResponseCode.equals("0")) {
                isZeroResponseCode = true;
            } else {
                isZeroResponseCode = false;
            }
        } catch (Exception e) {
            Log.e("", "error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AsyncSubmitExams", "IsCorrectXMLResponse()", e.toString());
            isZeroResponseCode = false;
        }
        return isZeroResponseCode;
    }

    // TODO: Get the Value of Element
    public String getValue(String tag, Element element) {
        try {
            Node node = null;
            if (element != null) {
                NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
                node = nodeList.item(0);
            }
            return node.getNodeValue();

        } catch (Exception e) {
            Log.e("", "error=" + e.toString());
//            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
//                    "AsyncSubmitExams", "getValue() - " + tag, e.toString());
            return "";
        }
    }

}
