package com.lpi.kmi.lpi.activities.AsyncTasks;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

@SuppressLint("Range")
public class AsyncSubmitTokenDetails extends AsyncTask<String, String, String> {

    public final Callback<String> callback;
    Context context;
    ProgressDialog progressDialog;
    String strResponse = "", UserID = "";
    String strTokenType;
    String strFromDate;
    String strToDate;
    String strTokenCount;

    public AsyncSubmitTokenDetails(Context context, String UserID, String strTokenType, String strFromDate, String strToDate,
                                   String strTokenCount, Callback<String> callback) {
        this.callback = callback;
        this.context = context;
        this.UserID = UserID;
        this.strTokenType = strTokenType;
        this.strFromDate = strFromDate;
        this.strToDate = strToDate;
        this.strTokenCount = strTokenCount;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressDialog = new ProgressDialog(this.context, R.style.AppTheme_Dark_Dialog);
        } else {
            progressDialog = new ProgressDialog(this.context);
        }
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Submitting token request\nPlease wait..");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.GenerateToken));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            inputPI1.setName("objXMLDoc");
            try {
                String objXMLDoc = CommonClass.GenerateXML(context, 1, "GenerateToken",
                        new String[]{"UserID", "TokenType", "ValidFrom", "ValidTo", "TokenCount"},
                        new String[]{UserID, strTokenType, strFromDate, strToDate, strTokenCount});
                inputPI1.setValue(objXMLDoc);
            } catch (Exception e1) {
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "AsyncSubmitTokenDetails", "doInBackground()", e1.toString());
            }

            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            // Set output SOAP object
            envelope.setOutputSoapObject(request);
            // Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));

            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                    context.getString(R.string.GenerateToken), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            // Assign it to static variable
            strResponse = response.toString();
            Log.e("", "response=" + strResponse);

            return strResponse;
        } catch (Exception e) {
            Log.e("", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AsyncSubmitTokenDetails", "doInBackground()", e.toString());
            return "error";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
        callback.execute(result, "");
    }
}