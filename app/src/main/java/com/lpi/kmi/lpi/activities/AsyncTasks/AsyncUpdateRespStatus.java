package com.lpi.kmi.lpi.activities.AsyncTasks;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;

import net.sqlcipher.Cursor;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import nu.xom.Serializer;

@SuppressLint("Range")
public class AsyncUpdateRespStatus extends AsyncTask<String, Void, JSONObject> {
    Context context;
    String mobRefId;

    public AsyncUpdateRespStatus(Context context) {
        this.context = context;
    }

    public AsyncUpdateRespStatus() {

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected JSONObject doInBackground(String... params) {
        // TODO Auto-generated method stub

        try {
            Cursor mCursor = DatabaseHelper.db.query(context.getResources().getString(R.string.TBL_FAQ_Conversation),
                    new String[]{"MobRefId"}, "RespStatus=? and MsgFrom !=?",
                    new String[]{"S", StaticVariables.UserLoginId}, null, null, "");
            if (mCursor.getCount() > 0) {
                sendStatus(mCursor,"D");
            } else {
                mCursor = DatabaseHelper.db.query(context.getResources().getString(R.string.TBL_FAQ_Conversation),
                        new String[]{"MobRefId"}, "RespStatus=? and MsgFrom<>?",
                        new String[]{"R", StaticVariables.UserLoginId}, null, null, "");
                if (mCursor.getCount()>0){
                    sendStatus(mCursor,"R");
                }
            }
            mCursor.close();
            return new JSONObject();
        } catch (Exception e) {
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AsyncUpdateRespStatus", "doInBackground", e.toString());
            return null;
        }
    }

    protected void sendStatus(Cursor mCursor, String status) {
        String strResponse = "";
        try {
            for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                mobRefId = mCursor.getString(0);
                if (mobRefId != null && !mobRefId.equalsIgnoreCase("{}")
                        && !mobRefId.equalsIgnoreCase("null")) {
                    strResponse = SubmitRespStatus(mobRefId, status);
                    if (strResponse != null && !strResponse.equalsIgnoreCase("{}")
                            && !strResponse.equalsIgnoreCase("null")) {
                        JSONObject result = new JSONObject(strResponse);
                        String strJsonArray = result.getJSONArray("Table").toString();
                        JSONArray jsonArray = new JSONArray(strJsonArray);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                            if (jsonObject.has("ResponseCode") && jsonObject.getString("ResponseCode").equals("0")) {
                                DatabaseHelper.updateMsgDelevered(context, DatabaseHelper.db,
                                        jsonObject.getString("MobRefId"),
                                        jsonObject.getString("RespStatus"),
                                        jsonObject.getString("SyncStatus"));
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AsyncUpdateRespStatus", "doInBackground", e.toString());
        }
    }

    @Override
    protected void onPostExecute(JSONObject result) {
        onCancelled();
    }

    private String SubmitRespStatus(String mobRefId, String status) {
        try {

            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.SubmitRespStatus));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            String strReqXML = reqRespStatusBuild(mobRefId, status);
            inputPI1.setName("objXMLDoc");
            inputPI1.setValue(strReqXML);
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));

            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                    context.getString(R.string.SubmitRespStatus), envelope);
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            return response.toString();

        } catch (Exception e) {
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AsyncUpdateRespStatus", "SubmitRespStatus", e.toString());
            return "";
        }
    }

    private String reqRespStatusBuild(String mobRefId, String status) {

        String str_XML = "";
        try {
            nu.xom.Element root = new nu.xom.Element("SubmitRespStatus");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(context));

            nu.xom.Element InscType = new nu.xom.Element("InscType");
            InscType.appendChild("" + StaticVariables.crnt_InscType);

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
            root.appendChild(InscType);
//            if (mCursor.getCount() > 0) {
//                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
            nu.xom.Element MobRefId = new nu.xom.Element("MobRefId");
            MobRefId.appendChild(mobRefId);
            root.appendChild(MobRefId);

            nu.xom.Element RespStatus = new nu.xom.Element("RespStatus");
            RespStatus.appendChild(status);
            root.appendChild(RespStatus);
//                }
//            }
            nu.xom.Document doc = new nu.xom.Document(root);

            OutputStream out = new ByteArrayOutputStream();
            Serializer serializer = new Serializer(System.out, "UTF-8");
            serializer.setOutputStream(out);
            serializer.setIndent(4);
            serializer.setMaxLength(64);
            serializer.write(doc);
            str_XML = out.toString();
            str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
            str_XML = str_XML.replaceAll("\n", "");
            str_XML = str_XML.replaceAll("\r", "");
            str_XML = str_XML.replaceAll("     ", "");
            str_XML = str_XML.replaceAll("         ", "");
            str_XML = str_XML.trim();
            Log.e("", "XMLStr==" + str_XML);
        } catch (IOException e) {
            Log.e("", "getFAQFromServer Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AsyncRespStatus", "reqRespStatusBuild", e.toString());
        }
        return str_XML;
    }
}