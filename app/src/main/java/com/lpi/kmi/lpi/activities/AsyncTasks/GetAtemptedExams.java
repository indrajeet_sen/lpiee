package com.lpi.kmi.lpi.activities.AsyncTasks;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import androidx.annotation.RequiresApi;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * Created by mustafakachwalla on 23/01/18.
 */

@SuppressLint("Range")
public class GetAtemptedExams extends AsyncTask<String, String, String> {
    public Context context;
    ProgressDialog progressDialog;
    String strResponse = "", CandidateId = "";
    InputStream responseStream;
    Callback callback;

    public GetAtemptedExams(Context context, String CandidateId, Callback callback) {
        this.context = context;
        this.CandidateId = CandidateId;
        this.callback = callback;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressDialog = new ProgressDialog(this.context, R.style.AppTheme_Dark_Dialog);
        } else {
            progressDialog = new ProgressDialog(this.context);
        }

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Synchronising Data\nPlease wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected String doInBackground(String... strings) {
        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.GetCandidateAtemptedExams));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            inputPI1.setName("objXMLDoc");
            String objXMLDoc = CommonClass.GenerateXML(context, 1, "GetCandAttemtedExamRequest",
                    new String[]{"LoginId"},
                    new String[]{CandidateId});
            inputPI1.setValue(objXMLDoc);
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            // Set output SOAP object
            envelope.setOutputSoapObject(request);
            // Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));


            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                    context.getString(R.string.GetCandidateAtemptedExams), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString().trim();
            Log.e("", "response=" + strResponse);

            InputStream isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            responseStream = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));

            if (CommonClass.IsCorrectXMLResponse(isResponse)) {
                NodeList nList = CommonClass.GetXMLElementNodeList(responseStream, "Exams");
                if (nList == null) {

                } else if (nList.getLength() > 0) {
                    for (int i = 0; i < nList.getLength(); i++) {
                        Node node = nList.item(i);
                        if (node.getNodeType() == Node.ELEMENT_NODE) {
                            Element element = (Element) node;
                            DatabaseHelper.SaveAttemptedExam(context, DatabaseHelper.db,
                                    CommonClass.getXMLElementValue("ExamID", element),
                                    CommonClass.getXMLElementValue("ExamName", element),
                                    "Y", CommonClass.getXMLElementValue("NoOfAttempt", element),
                                    CommonClass.getXMLElementValue("CandidateID", element));
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e("", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "GetAtemptedExams", "GetAtemptedExams.doInBackground()", e.toString());
            return "Fail";
        }
        return "Success";
    }

    @Override
    protected void onPostExecute(String s) {
//        super.onPostExecute(s);
        progressDialog.dismiss();
        callback.execute(s, s);
    }
}
