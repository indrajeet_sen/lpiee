package com.lpi.kmi.lpi.activities.AsyncTasks;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@SuppressLint("Range")
public class GetCandidateAttemptedExamsDetails extends AsyncTask<String, String, String> {

    Context context;
    String ExamId, CandidateId, strResponse;
    Callback callback;
    ProgressDialog progressDialog;
    String callFrom = null;

    public GetCandidateAttemptedExamsDetails(Context context, String ExamId, String CandidateId) {
        this.context = context;
        this.ExamId = ExamId;
        this.CandidateId = CandidateId;
    }

    public GetCandidateAttemptedExamsDetails(Context context, String ExamId, String CandidateId, String callFrom, Callback callback) {
        this.context = context;
        this.ExamId = ExamId;
        this.CandidateId = CandidateId;
        this.callFrom = callFrom;
        this.callback = callback;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressDialog = new ProgressDialog(this.context, R.style.AppTheme_Dark_Dialog);
        } else {
            progressDialog = new ProgressDialog(this.context);
        }
    }

    @Override
    protected void onPreExecute() {
        if (callFrom != null && callFrom.equalsIgnoreCase("MCQ_QuestionActivity")) {
            progressDialog.setIndeterminate(false);
            progressDialog.setMessage("Synchronising Data\nPlease wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected String doInBackground(String... strings) {
        org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                context.getString(R.string.Exam_NAMESPACE),
                context.getString(R.string.GetCandidateAttemptedExamsDetails));

        // property which holds input parameters
        PropertyInfo inputPI1 = new PropertyInfo();

        inputPI1.setName("objXMLDoc");
        try {
            String objXMLDoc = CommonClass.GenerateXML(context, 1, "AttemptedExamsDetails", new String[]{"ExamID", "CandidateId"}, new String[]{ExamId, CandidateId});
            inputPI1.setValue(objXMLDoc);
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        inputPI1.setType(String.class);
        request.addProperty(inputPI1);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.dotNet = true;
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));


        try {
            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                    context.getString(R.string.GetCandidateAttemptedExamsDetails), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            // Assign it to static variable
            strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString();
//            Log.e("", "response=" + strResponse);

            InputStream isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            InputStream is = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            if (CommonClass.IsCorrectXMLResponse(isResponse)) {
                NodeList nList = CommonClass.getExamDatafrmXML(context, is, "ExamsDetails");
                for (int i = 0; i < nList.getLength(); i++) {
                    Node node = nList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;
                        DatabaseHelper.SaveAttemptedQueAns(context, DatabaseHelper.db,
                                CommonClass.getValue(context, "CandidateId", element),
                                CommonClass.getValue(context, "ExamID", element),
                                CommonClass.getValue(context, "ExamName", element),
                                CommonClass.getValue(context, "QuestionId", element),
                                CommonClass.getValue(context, "QuestionAttempted", element),
                                CommonClass.getValue(context, "AnswerId", element),
                                CommonClass.getValue(context, "AnswerSequens", element),
                                CommonClass.getValue(context, "Feedback", element),
                                CommonClass.getValue(context, "AttemptId", element));
                    }
                }
            }
        } catch (Exception e) {
            Log.e("", "GetCandidateAttemptedExamsDetails Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "SyncExamMaster", "GetCandidateAttemptedExamsDetails", e.toString());
            return "Fail";
        }
        return "Success";
    }

    @Override
    protected void onPostExecute(String s) {
//        super.onPostExecute(s);
        if (callFrom != null && callFrom.equalsIgnoreCase("MCQ_QuestionActivity")) {
            progressDialog.dismiss();
            callback.execute(s, s);
        }
    }
}