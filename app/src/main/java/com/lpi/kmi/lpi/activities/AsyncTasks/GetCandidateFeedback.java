package com.lpi.kmi.lpi.activities.AsyncTasks;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import androidx.annotation.RequiresApi;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.adapter.GetterSetter.ModelGroupClass;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

@SuppressLint("Range")
public class GetCandidateFeedback extends AsyncTask<String, String, String> {
    Context context;
    Callback callback;
    String CandidateId;
    String ExamType;
    ProgressDialog progressDialog;
    ArrayList<ModelGroupClass> ExpListItems;

    public GetCandidateFeedback(Context context, Callback callback, String CandidateId, String ExamType) {
        this.context = context;
        this.callback = callback;
        this.CandidateId = CandidateId;
        this.ExamType = ExamType;
        ExpListItems = new ArrayList<>();
    }

    @Override
    protected void onPreExecute() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressDialog = new ProgressDialog(this.context, R.style.AppTheme_Dark_Dialog);
        } else {
            progressDialog = new ProgressDialog(this.context);
        }
        progressDialog.setMessage("Loading Details..\nPlease wait..");
        progressDialog.show();
    }

    @Override
    protected void onPostExecute(String s) {
        progressDialog.dismiss();
        callback.execute(ExpListItems, s);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected String doInBackground(String... strings) {
        String strResponse = "";
        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.GetCandidateFeedback));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();

            inputPI1.setName("objXMLDoc");
            try {
                String objXMLDoc = CommonClass.GenerateXML(context, 1, "CandidateFeedback",
                        new String[]{"CandidateId", "ExamType"}, new String[]{CandidateId, ExamType});
                inputPI1.setValue(objXMLDoc);
            } catch (Exception e1) {
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "CandidateFeedback", "doInbackground()", e1.toString());
            }
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            // Set output SOAP object
            envelope.setOutputSoapObject(request);
            // Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));


            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                    context.getString(R.string.GetCandidateFeedback), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            // Assign it to static variable
            strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString();
            Log.e("", "response=" + strResponse);


            InputStream isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            InputStream is = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            if (IsCorrectXMLResponse(isResponse)) {
                NodeList nList = getExamDatafrmXML(is, "Feedback");
                for (int i = 0; i < nList.getLength(); i++) {

                    Node node = nList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;
                        if (getValue("ExamId", element).equals("")
                                || getValue("Question", element).equals("")) {

                        } else {
                            DatabaseHelper.SaveCndFeedback(context, DatabaseHelper.db,
                                    getValue("CandidateId", element),
                                    getValue("ExamId", element), getValue("ExamDesc", element),
                                    getValue("ExamDetails", element), getValue("QuestionId", element),
                                    getValue("Question", element), "",
                                    "", getValue("Feedback", element), "");
                        }
                    }
                }
            }

            return "Success";

        } catch (Exception e) {
            Log.e("", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "GetCandidateFeedback", "doInBackground", e.toString());
            return "Failed";
        }
    }

    // TODO: Get the Value of Element
    public String getValue(String tag, Element element) {
        try {
            NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
            Node node = nodeList.item(0);
            return node.getNodeValue();
        } catch (Exception e) {
            Log.e("", "error=" + e.toString());
//            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
//                    "SyncExamMaster", "getValue()", e.toString());
            return "";
        }
    }

    // TODO: Check the Response Code
    public boolean IsCorrectXMLResponse(InputStream strXML) {
        boolean isZeroResponseCode = false;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(strXML);
            Element element = doc.getDocumentElement();
            element.normalize();
            String ResponseCode = getValue("ResponseCode", element);
            if (ResponseCode.equals("0")) {
                isZeroResponseCode = true;
            } else {
                isZeroResponseCode = false;
            }
        } catch (Exception e) {
            Log.e("", "error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "SyncExamMaster", "IsCorrectXMLResponse()", e.toString());
            isZeroResponseCode = false;
        }
        return isZeroResponseCode;
    }

    public NodeList getExamDatafrmXML(InputStream strXML, String NodeListItem) {
        NodeList nList = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(strXML);
            Element element = doc.getDocumentElement();
            element.normalize();
            nList = doc.getElementsByTagName(NodeListItem);
        } catch (Exception e) {
            Log.e("", "error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "SyncExamMaster", "getExamDatafrmXML()", e.toString());
        }

        return nList;
    }
}
