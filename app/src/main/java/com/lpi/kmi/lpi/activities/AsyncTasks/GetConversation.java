package com.lpi.kmi.lpi.activities.AsyncTasks;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.LPI_FAQ_ConversationActivity;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.stringcare.library.SC;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import nu.xom.Serializer;

@SuppressLint("Range")
public class GetConversation extends AsyncTask<String, String, String> {
    private static String CandidateId, TrainerId, InscType, UserType;
    boolean isFirstTime = false;
    Context context;
    Cursor cursor;
    Callback callback;

    public GetConversation() {

    }

    public GetConversation(Context context, Callback callback) {
        this.context = context;
        this.callback = callback;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (s.equals("Success")) {
            callback.execute("", s);
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected String doInBackground(String... strings) {
        Log.d("GetConversation", "Service Started");
        String status = "";
        try {
            if (DatabaseHelper.db == null) {
                SQLiteDatabase.loadLibs(context);
                DatabaseHelper.db = DatabaseHelper.getInstance(context).
                        getWritableDatabase(SC.reveal(CommonClass.DBPassword));
            }

            cursor = DatabaseHelper.db.query(context.getResources().getString(R.string.TBL_iCandidate),
                    new String[]{"CandidateLoginId", "ParentId", "InscType", "UserType"},
                    null, null, null, null, null);
            if (cursor != null && cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {

                    if (cursor.getString(3).equals("T")) {
                        CandidateId = "";
                        TrainerId = cursor.getString(0);
                        StaticVariables.UserLoginId = TrainerId;
                    } else {
                        CandidateId = cursor.getString(0);
                        TrainerId = cursor.getString(1);
                        StaticVariables.UserLoginId = CandidateId;
                    }
                    InscType = cursor.getString(2);
                    UserType = cursor.getString(3);

                    status = getMessages();
                }
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            status = "Failed";
        }
        return status;
    }

    public String getMessages() {

        try {

            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.GetConversation));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            String strReqXML = reqBuild();
            inputPI1.setName("objXMLDoc");
            inputPI1.setValue(strReqXML);
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));

            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                    context.getString(R.string.GetConversation), envelope);
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            String strResp = response.toString();


            JSONObject result = new JSONObject(strResp);
            JSONArray jsonArray = result.getJSONArray("Table");


            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                if (jsonObject.has("ResponseCode") && jsonObject.getString("ResponseCode").equals("1")) {

                } else if (jsonObject.has("ResponseCode") && jsonObject.getString("ResponseCode").equals("0")) {
                    if (DatabaseHelper.isMessagesAvailable(context)) {
//                        CommonClass.createNotification(context, "Training Manager", "Restoring chat", new Intent());
                        isFirstTime = true;
                    }
                    if (!DatabaseHelper.isMessageAvailable(context, DatabaseHelper.db, jsonObject.getString("MobRefId"))) {
                        if (!isFirstTime) {
                            if (UserType.equals("C")) {
                                Intent intent = new Intent(context, LPI_FAQ_ConversationActivity.class);
                                intent.putExtra("question", jsonObject.getString("QuesDesc"));
                                intent.putExtra("question_id", jsonObject.getString("QuesId"));
                                intent.putExtra("user_id", jsonObject.getString("CandidateId"));
                                intent.putExtra("trainer_id", jsonObject.getString("TrainerId"));
                                intent.putExtra("user_name", jsonObject.getString("TrainerName"));
                                intent.putExtra("profile_pic", "");
                                intent.putExtra("isClosed", jsonObject.getString("isClosed"));

                                if (CandidateId != jsonObject.getString("RespFrom")
                                        && (jsonObject.getString("RespDesc") != null
                                        || !jsonObject.getString("RespDesc").equals(""))) {
                                    if (jsonObject.getString("isAttachment").equalsIgnoreCase("Y")) {
                                        if (!CommonClass.isActivityRunning(context, LPI_FAQ_ConversationActivity.class)
                                                && StaticVariables.Notification.equals("Yes")) {
//                                            CommonClass.createNotification(context,
//                                                    jsonObject.getString("TrainerName"),
//                                                    jsonObject.getString("mFileName"),
//                                                    intent);
                                        }
                                    } else {
                                        if (!CommonClass.isActivityRunning(context, LPI_FAQ_ConversationActivity.class)
                                                && StaticVariables.Notification.equals("Yes")) {
//                                            CommonClass.createNotification(context,
//                                                    jsonObject.getString("TrainerName"),
//                                                    jsonObject.getString("RespDesc"),
//                                                    intent);
                                        }
                                    }
                                }
                            } else {

                                Intent intent = new Intent(context, LPI_FAQ_ConversationActivity.class);
                                intent.putExtra("question", jsonObject.getString("QuesDesc"));
                                intent.putExtra("question_id", jsonObject.getString("QuesId"));
                                intent.putExtra("user_id", jsonObject.getString("CandidateId"));
                                intent.putExtra("trainer_id", jsonObject.getString("TrainerId"));
                                intent.putExtra("user_name", jsonObject.getString("CandidateName"));
                                intent.putExtra("profile_pic", "");
                                intent.putExtra("isClosed", jsonObject.getString("isClosed"));

                                if (jsonObject.getString("isAttachment").equalsIgnoreCase("Y")) {
                                    if (!CommonClass.isActivityRunning(context, LPI_FAQ_ConversationActivity.class)
                                            && StaticVariables.Notification.equals("Yes")) {
//                                        CommonClass.createNotification(context,
//                                                jsonObject.getString("CandidateName"),
//                                                jsonObject.getString("mFileName"),
//                                                intent);
                                    }
                                } else {
                                    if (!CommonClass.isActivityRunning(context, LPI_FAQ_ConversationActivity.class)
                                            && StaticVariables.Notification.equals("Yes")) {
//                                        CommonClass.createNotification(context,
//                                                jsonObject.getString("CandidateName"),
//                                                jsonObject.getString("RespDesc"),
//                                                intent);
                                    }
                                }
                            }
                        }
                    }
                    String isShow = "Y";
                    if (jsonObject.getString("RespStatus").equals("R"))
                        isShow = "N";
                    DatabaseHelper.SaveConversation(context, DatabaseHelper.db,
                            jsonObject.getString("isClosed"), jsonObject.getString("RespDesc"),
                            jsonObject.getString("TrainerId"), jsonObject.getString("CandidateId"),
                            jsonObject.getString("QuesDesc"), jsonObject.getString("RespFrom"),
                            jsonObject.getString("RespDtime").toString(), jsonObject.getString("QuesId"),
                            jsonObject.getString("MobRefId"),
                            /*jsonObject.getString("MsgType"), jsonObject.getString("DocLocation")*/"", "",
                            "success", jsonObject.getString("RespStatus"), isShow,
                            jsonObject.getString("isAttachment"), "", jsonObject.getString("mFileName"),
                            jsonObject.getString("mFilePath"), jsonObject.getString("mFileType"));

                }
            }
            return "Success";
        } catch (Exception e) {
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "GetConversation", "getFAQConversationFromServer", e.toString());
            return "Failed";
        }

    }

    private String reqBuild() {

        String str_XML = "";
        try {
            nu.xom.Element root = new nu.xom.Element("GetConversation");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(context));

            nu.xom.Element InscType = new nu.xom.Element("InscType");
            InscType.appendChild("" + GetConversation.InscType);

            nu.xom.Element RespDtime = new nu.xom.Element("RespDtime");
            String strRespDtime = "";

            if (GetConversation.UserType.equals("T")) {
                cursor = DatabaseHelper.db.query(context.getResources().getString(R.string.TBL_FAQ_Conversation),
                        new String[]{"RespDtime"}, "MsgFrom<>?",
                        new String[]{TrainerId}, null, null, "RespDtime desc", "1");
            } else {
                cursor = DatabaseHelper.db.query(context.getResources().getString(R.string.TBL_FAQ_Conversation),
                        new String[]{"RespDtime"}, "CandidateId=? and TrainerId=? and MsgFrom<>?",
                        new String[]{CandidateId, TrainerId, CandidateId}, null, null, "RespDtime desc", "1");
            }

            if (cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    strRespDtime = cursor.getString(0);
                    isFirstTime = false;
                }
            } else {
                strRespDtime = "";
                isFirstTime = true;
            }
            RespDtime.appendChild(strRespDtime);

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
            root.appendChild(InscType);
            root.appendChild(RespDtime);

            nu.xom.Element CandidateId = new nu.xom.Element("CandidateId");
            CandidateId.appendChild(GetConversation.CandidateId);
            root.appendChild(CandidateId);

            nu.xom.Element TrainerId = new nu.xom.Element("TrainerId");
            TrainerId.appendChild(GetConversation.TrainerId);
            root.appendChild(TrainerId);

            nu.xom.Element UserType = new nu.xom.Element("UserType");
            UserType.appendChild(GetConversation.UserType);
            root.appendChild(UserType);


            nu.xom.Element QuesId = new nu.xom.Element("QuesId");
            QuesId.appendChild("");
            root.appendChild(QuesId);

            nu.xom.Document doc = new nu.xom.Document(root);

            OutputStream out = new ByteArrayOutputStream();
            Serializer serializer = new Serializer(System.out, "UTF-8");
            serializer.setOutputStream(out);
            serializer.setIndent(4);
            serializer.setMaxLength(64);
            serializer.write(doc);
            str_XML = out.toString();
            str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
            str_XML = str_XML.replaceAll("\n", "");
            str_XML = str_XML.replaceAll("\r", "");
            str_XML = str_XML.replaceAll("     ", "");
            str_XML = str_XML.replaceAll("         ", "");
            str_XML = str_XML.trim();
//            Log.e("", "XMLStr==" + str_XML);
        } catch (IOException e) {
            Log.e("", "getFAQFromServer Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AsyncSearchCandidates", "getFAQFromServer", e.toString());
        }
        cursor.close();
        return str_XML;
    }


}


