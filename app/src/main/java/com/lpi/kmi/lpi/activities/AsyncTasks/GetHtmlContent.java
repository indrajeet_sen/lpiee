package com.lpi.kmi.lpi.activities.AsyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.adapter.GetterSetter.ZipContent;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mustafakachwalla on 06/10/17.
 */

public class GetHtmlContent extends AsyncTask<Object, TaskProgress, Boolean> {
    String SDCardRoot = "";
    //Environment.getExternalStoragePublicDirectory(Environment.MEDIA_SHARED).toString() + "/.www/";
    //    String fileName = "HTMLContent.zip";
//    String fileName = "E10013_30102017.zip";
    File directory;
    List<ZipContent> zipList = new ArrayList<ZipContent>();
    //    List<String> pathList = new ArrayList<String>();
    boolean download = false;
    Context context;
    ProgressDialog progressDialog;
    String strResponse = "", initFrom = "";
    Callback callback;
    //SimpleDateFormat format = new SimpleDateFormat("DD-MM-YYYY", Locale.ENGLISH);

    public GetHtmlContent(Context context, String initFrom) {
        this.context = context;
        this.initFrom = initFrom;
        initialiseSDCard();
    }

    public GetHtmlContent(Context context, String initFrom, Callback callback) {
        this.context = context;
        this.initFrom = initFrom;
        this.callback = callback;
        this.initFrom = initFrom;
        initialiseSDCard();
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressDialog = new ProgressDialog(context, R.style.AppTheme_Dark_Dialog);
        } else {
            progressDialog = new ProgressDialog(context);
        }
        if (initFrom.equalsIgnoreCase("ProductFragment")) {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    SDCardRoot = Environment.getExternalStoragePublicDirectory(Environment.MEDIA_SHARED).toString()
                            + StaticVariables.PostLicStoragePath;
                } else {
                    SDCardRoot = Environment.getExternalStorageDirectory() + "//Documents" + StaticVariables.PostLicStoragePath;
                }
            } catch (Exception e) {
                SDCardRoot = Environment.getExternalStorageDirectory() + "//Documents" + StaticVariables.PostLicStoragePath;
            }
        } else if (initFrom.equalsIgnoreCase("LPI_Lesson")) {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    SDCardRoot = Environment.getExternalStoragePublicDirectory(Environment.MEDIA_SHARED).toString()
                            + StaticVariables.LessonStoragePath;
                } else {
                    SDCardRoot = Environment.getExternalStorageDirectory() + "//Documents" + StaticVariables.LessonStoragePath;
                }
            } catch (Exception e) {
                SDCardRoot = Environment.getExternalStorageDirectory() + "//Documents" + StaticVariables.LessonStoragePath;
            }
        }*/
    }

    private void initialiseSDCard() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressDialog = new ProgressDialog(context, R.style.AppTheme_Dark_Dialog);
        } else {
            progressDialog = new ProgressDialog(context);
        }
        this.initFrom = initFrom;

        if (initFrom.equalsIgnoreCase("ProductFragment")) {
            /*try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    SDCardRoot = Environment.getExternalStoragePublicDirectory(Environment.).toString()
                            + StaticVariables.PostLicStoragePath;
                } else {
                    SDCardRoot = Environment.getExternalStorageDirectory() + "/Documents" + StaticVariables.PostLicStoragePath;
                }
            } catch (Exception e) {
                SDCardRoot = Environment.getExternalStorageDirectory() + "/Android/media" + StaticVariables.PostLicStoragePath;
            }*/

            SDCardRoot = context.getExternalFilesDir(Environment.MEDIA_SHARED) + StaticVariables.PostLicStoragePath;

            /*if (android.os.Build.VERSION.SDK_INT >= android.os. Build.VERSION_CODES.Q) {
                SDCardRoot = context.getExternalFilesDirs(Environment.MEDIA_SHARED).toString()
                        + StaticVariables.PostLicStoragePath;
            } else {
                SDCardRoot = context.getExternalFilesDir(null) + "/Documents" + StaticVariables.PostLicStoragePath;
            }*/

        } else if (initFrom.equalsIgnoreCase("LPI_Lesson")) {
            /*try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    SDCardRoot = Environment.getExternalStoragePublicDirectory(Environment.MEDIA_SHARED).toString()
                            + StaticVariables.LessonStoragePath;
                } else {
                    SDCardRoot = context.getExternalFilesDir(null) + "/Documents" + StaticVariables.LessonStoragePath;
                }
            } catch (Exception e) {
                SDCardRoot = context.getExternalFilesDir(null) + "/Documents" + StaticVariables.LessonStoragePath;
            }*/

            SDCardRoot = context.getExternalFilesDir(Environment.MEDIA_SHARED) + StaticVariables.LessonStoragePath;

            /*if (android.os.Build.VERSION.SDK_INT >= android.os. Build.VERSION_CODES.Q) {
                SDCardRoot = context.getExternalFilesDirs(Environment.MEDIA_SHARED).toString()
                        + StaticVariables.LessonStoragePath;
            } else {
                SDCardRoot = context.getExternalFilesDir(null) + "/Documents" + StaticVariables.LessonStoragePath;
            }*/
        }
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Synchronising Data\nPlease wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected Boolean doInBackground(Object... strings) {
        try {
            publishProgress(new TaskProgress(0, "Synchronising Data\nPlease wait..."));
            String result = "";
            Cursor cursor = DatabaseHelper.db.query(context.getResources().getString(R.string.TblPostLicModuleData),
                    null, null, null, null, null, null);
            if (cursor.getCount() == 0) {
                getModuleData(context);
            }
            cursor.close();

            if (initFrom.equalsIgnoreCase("ProductFragment")) {
                cursor = DatabaseHelper.db.query(context.getResources().getString(R.string.TBL_ZipContent),
                        null, null, null, null, null, null);
                publishProgress(new TaskProgress(0, "Searching for updates\nPlease wait..."));
                if (cursor.getCount() > 0) {
                    getZipFiles(context, 1);
                } else {
                    getZipFiles(context, 0);
                }
                cursor.close();
                Log.e("", "zipList.size()=" + zipList.size());

                for (int i = 0; i < zipList.size(); i++) {
                    publishProgress(new TaskProgress(i, "Downloading files " + (i + 1) + " / " + zipList.size() + "\nPlease wait..."));
                    // progressDialog.setMessage("Downloading "+(i+1)+" of "+zipList.size()+"\nPlease Wait...");
                    result = downloadZip(directory, zipList.get(i).getFileName() + ".zip", context.getString(R.string.ContentURL));
                    if (result.equals("Success")) {
                        DatabaseHelper.UpdateZipContent(context, DatabaseHelper.db, zipList.get(i).getBatchId(), "Y", new Date().toString());
                        updateZipContent(zipList.get(i).getBatchId(), "Y");
                        CommonClass.unzip(SDCardRoot + zipList.get(i).getFileName() + ".zip", SDCardRoot);
                    }
                }
                return true;
            } else if (initFrom.equalsIgnoreCase("LPI_Lesson")) {
                result = downloadZip(directory, "LPILessons.zip", context.getString(R.string.ContentURL));
                if (result.equals("Success")) {
                    CommonClass.unzip(SDCardRoot + "LPILessons.zip", SDCardRoot);
                    return true;
                }

            }
            return false;
        } catch (Exception e) {
            Log.e("GetHtmlContent", "doInBackground err=" + e.toString());
            progressDialog.dismiss();
            return false;
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    private void updateZipContent(String batchId, String synced) {
        org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                context.getResources().getString(R.string.Exam_NAMESPACE), context.getResources().getString(R.string.UpdateZipSynced));
        //property which holds input parameters
        PropertyInfo inputPI1 = new PropertyInfo();
        inputPI1.setName("objXMLDoc");
        try {
            String objXMLDoc = CommonClass.GenerateXML(context, 4, "UpdateZipSynced",
                    new String[]{"UserID", "DeviceID", "BatchId", "Synced"},
                    new String[]{StaticVariables.UserLoginId, Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID), batchId, synced});
            inputPI1.setValue(objXMLDoc);
        } catch (Exception e1) {
            //TODO Auto-generated catch block
            e1.printStackTrace();
        }
        inputPI1.setType(String.class);
        request.addProperty(inputPI1);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        //Set output SOAP object
        envelope.setOutputSoapObject(request);
        //Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getResources().getString(R.string.Exam_URL));
//        InputStream isResponse = null;
        try {
            //Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getResources().getString(R.string.Exam_SOAP_ACTION) +
                    context.getResources().getString(R.string.UpdateZipSynced), envelope);
            //Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            strResponse = response.toString();

        } catch (Exception e) {
            Log.e("updateZipContent", "updateZipContent Error=" + e.toString());
        }
    }

    @Override
    protected void onProgressUpdate(TaskProgress... values) {
        progressDialog.setMessage(values[0].message);
        progressDialog.setProgress(values[0].percentage);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        progressDialog.dismiss();

        if (initFrom.equals("ProductFragment")) {

        } else if (initFrom.equalsIgnoreCase("LPI_Lesson")) {
            if (callback != null) {
                callback.execute(aBoolean, "");
            }
        }

//        if (aBoolean) {
////                unpackZip(SDCardRoot,fileName);
////            try {
//
////            } catch (IOException e) {
////                e.printStackTrace();
////            }
//            progressDialog.cancel();
//        }else {
//
//        }
    }

    public void getZipFiles(Context context, int initFlag) {

        directory = new File(SDCardRoot);

        if (!directory.exists()) {
            directory.mkdir();
        }

        zipList.clear();

        org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                context.getResources().getString(R.string.Exam_NAMESPACE), context.getResources().getString(R.string.GetZipFiles));
        //property which holds input parameters
        PropertyInfo inputPI1 = new PropertyInfo();
        inputPI1.setName("objXMLDoc");
        try {
            String objXMLDoc = CommonClass.GenerateXML(context, 2, "getZipFiles",
                    new String[]{"DeviceID", "initFlag"},
                    new String[]{Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID),
                            "" + initFlag});
            inputPI1.setValue(objXMLDoc);
        } catch (Exception e1) {
            //TODO Auto-generated catch block
            e1.printStackTrace();
        }
        inputPI1.setType(String.class);
        request.addProperty(inputPI1);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.dotNet = true;
        //Set output SOAP object
        envelope.setOutputSoapObject(request);
        //Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getResources().getString(R.string.Exam_URL));
//        InputStream isResponse = null;
        try {
            //Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getResources().getString(R.string.Exam_SOAP_ACTION) +
                    context.getResources().getString(R.string.GetZipFiles), envelope);
            //Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            strResponse = response.toString();

            JSONObject objJsonObject = new JSONObject(strResponse);
            JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");

            for (int index = 0; index < objJsonArray.length(); index++) {
                JSONObject jsonobject = objJsonArray.getJSONObject(index);

                if (jsonobject.has("FileName")) {
//                    if (new File(SDCardRoot, jsonobject.getString("FileName")+".zip").exists()) {
//                        new File(SDCardRoot, jsonobject.getString("FileName")).delete();
//                    }

                    DatabaseHelper.InsertZipContent(context, DatabaseHelper.db, jsonobject.getString("BatchId"),
                            jsonobject.getString("FileName"), jsonobject.getString("Path"),
                            jsonobject.getString("DnldFlag"), StaticVariables.UserLoginId,
                            new Date().toString(), "");

                    ZipContent zipContent = new ZipContent();
                    zipContent.setBatchId(jsonobject.getString("BatchId"));
                    zipContent.setFileName(jsonobject.getString("FileName"));
                    zipContent.setPath(jsonobject.getString("Path"));
                    zipContent.setSynced(jsonobject.getString("DnldFlag"));

                    zipList.add(zipContent);
                }
            }

        } catch (Exception e) {
            Log.e("getZipFiles", "getZipFiles Error=" + e.toString());
        }
    }

    public String downloadZip(File directory, String fileName, String URL) {

        /*if (initFrom.equalsIgnoreCase("LPI_Lesson")) {
            return CommonClass.saveFilesToPath(context, fileName, "application/zip", StaticVariables.LessonStoragePath, URL);
        }else {
            return CommonClass.saveFilesToPath(context, fileName, "application/zip", StaticVariables.ParentStoragePath, URL);
        }*/

        try {
            if (fileName.equalsIgnoreCase("LPILessons.zip")) {
//                CommonClass.DeleteFileFolders(new File(context.getExternalFilesDir(null) + "/Documents" + StaticVariables.LessonStoragePath));
                CommonClass.DeleteFileFolders(new File(context.getExternalFilesDir(Environment.MEDIA_SHARED) + StaticVariables.LessonStoragePath));
                directory = null;
            }

            if (directory == null) {
                directory = new File(SDCardRoot);
                if (!directory.exists()) {
                    boolean isCreated = directory.mkdirs();
                    Log.d("", "");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        InputStream input = null;
        try {
//            URL url = new URL("http://122.169.117.148/CandidateExam_Insc/HtmlContent/HTMLContent.zip"); // link of the song which you want to download like (http://...)
            URL url = new URL(URL + fileName); // link of the song which you want to download like (http://...)
            input = url.openStream();

            File zip = new File(directory, fileName);
//            File zip = CommonClass.getOutputMediaFile(context,SDCardRoot,fileName);
            if (zip.exists()) {
                zip.delete();
            }
            OutputStream output = new FileOutputStream(zip, false);
            download = true;
            try {
                byte[] buffer = new byte[1024];
                int bytesRead = 0;
                while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
                    output.write(buffer, 0, bytesRead);
                    download = true;
                }
                output.close();
                return "Success";
            } catch (Exception exception) {
                output.close();
                Log.e("", "Error=" + exception.toString());
                return "Failed";
            }
        } catch (Exception exception) {
            Log.e("downloadZip", "downloadZip Error=" + exception.toString());
            return "Failed";
        }
    }

    public void getModuleData(Context context) {
        org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                context.getResources().getString(R.string.Exam_NAMESPACE), context.getResources().getString(R.string.GetModuleData));
        //property which holds input parameters
        PropertyInfo inputPI1 = new PropertyInfo();
        inputPI1.setName("objXMLDoc");
        try {
            String objXMLDoc = CommonClass.GenerateXML(context, 0, "GetSectionData", new String[]{""}, new String[]{""});
            inputPI1.setValue(objXMLDoc);
        } catch (Exception e1) {
            //TODO Auto-generated catch block
            e1.printStackTrace();
        }
        inputPI1.setType(String.class);
        request.addProperty(inputPI1);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        //Set output SOAP object
        envelope.setOutputSoapObject(request);
        //Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getResources().getString(R.string.Exam_URL));
//        InputStream isResponse = null;
        try {
            //Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getResources().getString(R.string.Exam_SOAP_ACTION) + context.getResources().getString(R.string.GetModuleData), envelope);
            //Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            strResponse = response.toString();

            JSONObject objJsonObject = new JSONObject(strResponse);
            JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");

            for (int index = 0; index < objJsonArray.length(); index++) {
                JSONObject jsonobject = objJsonArray.getJSONObject(index);

                DatabaseHelper.PostLicModuleData(context, DatabaseHelper.db,
                        jsonobject.getString("SectionId"),
                        jsonobject.getString("ParentSectionId"),
                        jsonobject.getString("RootSectionId"),
                        jsonobject.getString("Desc01"),
                        jsonobject.getString("Desc02"),
                        jsonobject.getString("Desc03"),
                        jsonobject.getString("Desc04"),
                        jsonobject.getString("Desc05"),
                        jsonobject.getString("GenDesc01"),
                        jsonobject.getString("GenDesc02"),
                        jsonobject.getString("GenDesc03"),
                        jsonobject.getString("GenDesc04"),
                        jsonobject.getString("GenDesc05"),
                        jsonobject.getString("AdvDesc01"),
                        jsonobject.getString("AdvDesc02"),
                        jsonobject.getString("AdvDesc03"),
                        jsonobject.getString("AdvDesc04"),
                        jsonobject.getString("AdvDesc05"),
                        jsonobject.getString("ImageDesc01"),
                        jsonobject.getString("ImageDesc02"),
                        jsonobject.getString("ImageDesc03"),
                        jsonobject.getString("ImageDesc04"),
                        jsonobject.getString("ImageDesc05"),
                        jsonobject.getString("hasChild"),
                        jsonobject.getString("hasData"),
                        jsonobject.getString("IsActive"),
                        jsonobject.getString("IsVideoAvailable"),
                        jsonobject.getString("videoLink"),
                        "70011354", "08-08-2017", "", "");

                if (jsonobject.getString("AdvDesc01").contains("<body>")) {
                    String html = "<!DOCTYPE html><html><head><meta name=\"viewport\" " +
                            "content=\"width=device-width, initial-scale=1.0\">" +
                            jsonobject.getString("AdvDesc01") + "</html>";
                    String fName = "A01" + jsonobject.getString("SectionId") + ".html";
                    CommonClass.saveHtmlInStorage(context, fName, html);
                }

                if (jsonobject.getString("GenDesc01").contains("<body>")) {
                    String html = "<!DOCTYPE html><html><head><meta name=\"viewport\" " +
                            "content=\"width=device-width, initial-scale=1.0\">" +
                            jsonobject.getString("GenDesc01") + "</html>";
                    String fName = "B01" + jsonobject.getString("SectionId") + ".html";
                    CommonClass.saveHtmlInStorage(context, fName, html);
                }
            }

        } catch (Exception e) {
            Log.e("getModuleData", "getModuleData Error=" + e.toString());
        }
    }


}
