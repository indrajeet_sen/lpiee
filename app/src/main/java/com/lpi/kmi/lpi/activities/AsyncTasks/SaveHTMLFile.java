package com.lpi.kmi.lpi.activities.AsyncTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by mustafakachwalla on 13/09/17.
 */

public class SaveHTMLFile extends AsyncTask<String,String,String> {
    Context context;
    String fName;
    String html;
    public SaveHTMLFile(Context context, String fName, String html){
        this.context=context;
        this.fName=fName;
        this.html=html;
    }
    @Override
    protected String doInBackground(String[] params) {
        File mDir = null;
        if (CommonClass.isExternalStorageWritable()) {
//            String root = Environment.getExternalStoragePublicDirectory(Environment.MEDIA_SHARED)
//                    .getAbsolutePath() + "/.www";
            String root = context.getExternalFilesDir(Environment.MEDIA_SHARED) + StaticVariables.PostLicStoragePath;
//            String root = context.getFilesDir()+"/www";
            mDir = new File(root);
            if (!mDir.exists()) {
                mDir.mkdirs();
                mDir.setReadable(true);
                mDir.setWritable(true);
            }
            try {
                File mFile = new File(mDir, fName);
                if (mFile.exists()) {
                    mFile.delete();
                }
                FileOutputStream out = new FileOutputStream(mFile);
                byte[] data = html.getBytes();
                out.write(data);
                out.close();
                Log.e("", "File Save : " + mFile.getPath());
            } catch (Exception e) {
                Log.e("createImageFile", "Error while creating " + fName + e.toString());
                return "Failed";
            }
        } else {
            return "Failed";
        }
        return "Success";
    }

    @Override
    protected void onPostExecute(String s) {
        StaticVariables.fileCreated = s;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}
