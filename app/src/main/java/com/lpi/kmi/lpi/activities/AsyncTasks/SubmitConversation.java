package com.lpi.kmi.lpi.activities.AsyncTasks;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import nu.xom.Serializer;

@SuppressLint("Range")
public class SubmitConversation extends AsyncTask<String, Integer, JSONObject> {

    Context context;
    String filePath, MobRefId;
    String strResponse = "";
    Callback<String> callback;

    public SubmitConversation(Context context, String filePath, String MobRefId) {
        this.context = context;
        this.filePath = filePath;
        this.MobRefId = MobRefId;
    }

    public SubmitConversation(Callback<String> callback, Context context, String filePath, String MobRefId) {
        this.context = context;
        this.filePath = filePath;
        this.MobRefId = MobRefId;
        this.callback = callback;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected JSONObject doInBackground(String... strings) {
        String strData = "";
        int timeout = 10000;
        byte[] inputData = null;
        try {
//            if (!filePath.equals("")) {
//                if (filePath.contains("/document/primary:")) {
//                    String strFileParentPath = Environment.getExternalStorageDirectory().getPath();
//                    filePath = filePath.replace("/document/primary:", strFileParentPath + "/");
//                }
////                    InputStream stream = context.getApplicationContext().getContentResolver().openInputStream(Uri.parse(filePath));
////                    inputData = getBytes(stream);
////                }else {
//                inputData = getBytes(filePath);
////                }
//
//                strData = Base64.encodeToString(inputData, Base64.NO_WRAP);
//                timeout = inputData.length;
//            }
            Cursor mCursor = DatabaseHelper.db.query(context.getResources().getString(R.string.TBL_FAQ_Conversation),
                    null, "MobRefId=?", new String[]{MobRefId}, null, null, "", "1");
            if (mCursor.getCount() > 0) {
                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {

                    org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                            context.getString(R.string.Exam_NAMESPACE),
                            context.getString(R.string.SubmitConversation));

                    // property which holds input parameters
                    PropertyInfo inputPI1 = new PropertyInfo();
                    PropertyInfo inputPI2 = new PropertyInfo();

                    inputPI1.setName("objXMLDoc");
                    String objXMLDoc = reqSubmitBuild(mCursor);
                    inputPI1.setValue(objXMLDoc);
                    inputPI1.setType(String.class);
                    request.addProperty(inputPI1);

                    inputPI2.setName("inputData");
                    inputPI2.setValue(strData);
                    inputPI2.setType(byte.class);
                    request.addProperty(inputPI2);

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    // Set output SOAP object
                    envelope.setOutputSoapObject(request);
                    // Create HTTP call object
                    HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL), timeout);
                    // Involve Web service
                    LPIEEX509TrustManager.allowAllSSL();
//                    FakeX509TrustManager.trustSSLCertificate((Activity) context);
                    androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                            context.getString(R.string.SubmitConversation), envelope);
                    // Get the response
                    SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

                    // Assign it to static variable
                    strResponse = response.toString();
                }
                return new JSONObject(strResponse);
            } else {
                return null;
            }
        } catch (Exception e) {
            Log.e("", "doInBackground Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "SubmitConversation", "doInBackground", e.toString());
            return null;
        }
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        if (jsonObject != null) {
            try {
                String strJsonArray = jsonObject.getJSONArray("Table").toString();
                JSONArray jsonArray = new JSONArray(strJsonArray);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jObject = new JSONObject(jsonArray.getString(i));
                    if (jObject.has("ResponseCode") && jObject.getString("ResponseCode").equals("0")) {
                        ContentValues cv = new ContentValues();
                        cv.clear();
                        cv.put("SyncStatus", "Success");
                        int iRec = DatabaseHelper.db.update(context.getResources().getString(R.string.TBL_FAQ_Conversation), cv,
                                "MobRefId=?", new String[]{jObject.getString("MobRefId").toString()});
                        Log.e("Updated record: ", iRec + "");
                    }
//                    if (!filePath.equals(""))
//                        CommonClass.createNotification(context.getApplicationContext(), "Upload Complete", "Your attachment is uploaded successfully", "");
                }
            } catch (Exception e) {
                Log.e("", "onPostExecute Error=" + e.toString());
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "SubmitConversation", "onPostExecute", e.toString());
            }
            callback.execute(MobRefId, "Success");
        } else {
            callback.execute(MobRefId, "Failed");
        }
    }

    public byte[] getBytes(String filePath) {
        byte[] bytes = null;
        try {
            File file = new File(filePath);
            int size = (int) file.length();
            bytes = new byte[size];
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (Exception e) {
            e.printStackTrace();
            bytes = null;
        }
        return bytes;
    }

    public byte[] getBytes(InputStream inputStream) {
        try {
            ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
            int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];

            int len = 0;
            while ((len = inputStream.read(buffer)) != -1) {
                byteBuffer.write(buffer, 0, len);
            }
            return byteBuffer.toByteArray();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    private String reqSubmitBuild(Cursor mCursor) {

        String str_XML = "";
        try {
            nu.xom.Element root = new nu.xom.Element("SubmitConversation");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(context));

            nu.xom.Element InscType = new nu.xom.Element("InscType");
            InscType.appendChild("" + StaticVariables.crnt_InscType);

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
            root.appendChild(InscType);
//            if (mCursor.getCount() > 0) {
//                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {

            nu.xom.Element TrainerId = new nu.xom.Element("TrainerId");
            TrainerId.appendChild(mCursor.getString(mCursor.getColumnIndex("TrainerId")));
            root.appendChild(TrainerId);
            nu.xom.Element CandidateId = new nu.xom.Element("CandidateId");
            CandidateId.appendChild(mCursor.getString(mCursor.getColumnIndex("CandidateId")));
            root.appendChild(CandidateId);
            nu.xom.Element QuesId = new nu.xom.Element("QuesId");
            QuesId.appendChild(mCursor.getString(mCursor.getColumnIndex("QuesId")));
            root.appendChild(QuesId);
            nu.xom.Element QuesDesc = new nu.xom.Element("QuesDesc");
            QuesDesc.appendChild(mCursor.getString(mCursor.getColumnIndex("QuesDesc")));
            root.appendChild(QuesDesc);
            nu.xom.Element RespDesc = new nu.xom.Element("RespDesc");
            RespDesc.appendChild(mCursor.getString(mCursor.getColumnIndex("RespDesc")));
            root.appendChild(RespDesc);
            nu.xom.Element RespDtime = new nu.xom.Element("RespDtime");
            RespDtime.appendChild(mCursor.getString(mCursor.getColumnIndex("RespDtime")));
            root.appendChild(RespDtime);
            nu.xom.Element RespFrom = new nu.xom.Element("RespFrom");
            RespFrom.appendChild(mCursor.getString(mCursor.getColumnIndex("MsgFrom")));
            root.appendChild(RespFrom);
            nu.xom.Element isClosed = new nu.xom.Element("isClosed");
            isClosed.appendChild(mCursor.getString(mCursor.getColumnIndex("isClosed")));
            root.appendChild(isClosed);
            nu.xom.Element CreatedBy = new nu.xom.Element("CreatedBy");
            CreatedBy.appendChild(mCursor.getString(mCursor.getColumnIndex("CreatedBy")));
            root.appendChild(CreatedBy);
            nu.xom.Element MobRefId = new nu.xom.Element("MobRefId");
            MobRefId.appendChild(mCursor.getString(mCursor.getColumnIndex("MobRefId")));
            root.appendChild(MobRefId);

            nu.xom.Element RespStatus = new nu.xom.Element("RespStatus");
            RespStatus.appendChild(mCursor.getString(mCursor.getColumnIndex("RespStatus")));
            root.appendChild(RespStatus);

            nu.xom.Element isAttachment = new nu.xom.Element("isAttachment");
            isAttachment.appendChild(mCursor.getString(mCursor.getColumnIndex("isAttachment")));
            root.appendChild(isAttachment);

            nu.xom.Element mFile = new nu.xom.Element("mFile");
            mFile.appendChild(mCursor.getString(mCursor.getColumnIndex("mFile")));
            root.appendChild(mFile);

            nu.xom.Element mFileName = new nu.xom.Element("mFileName");
            mFileName.appendChild(mCursor.getString(mCursor.getColumnIndex("mFileName")));
            root.appendChild(mFileName);

            nu.xom.Element mFilePath = new nu.xom.Element("mFilePath");
            mFilePath.appendChild(mCursor.getString(mCursor.getColumnIndex("mFilePath")));
            root.appendChild(mFilePath);

            nu.xom.Element mFileType = new nu.xom.Element("mFileType");
            mFileType.appendChild(mCursor.getString(mCursor.getColumnIndex("mFileType")));
            root.appendChild(mFileType);
//                }
//            }
            nu.xom.Document doc = new nu.xom.Document(root);

            OutputStream out = new ByteArrayOutputStream();
            Serializer serializer = new Serializer(System.out, "UTF-8");
            serializer.setOutputStream(out);
            serializer.setIndent(4);
            serializer.setMaxLength(64);
            serializer.write(doc);
            str_XML = out.toString();
            str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
            str_XML = str_XML.replaceAll("\n", "");
            str_XML = str_XML.replaceAll("\r", "");
            str_XML = str_XML.replaceAll("     ", "");
            str_XML = str_XML.replaceAll("         ", "");
            str_XML = str_XML.trim();
            Log.e("", "XMLStr==" + str_XML);
        } catch (IOException e) {
            Log.e("", "getFAQFromServer Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AsyncSearchCandidates", "getFAQFromServer", e.toString());
        }
        return str_XML;
    }

//    private void createNotification(String contentTitle, String contentText) {
//        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        //Build the notification using Notification.Builder
//        Notification.Builder builder = new Notification.Builder(context)
//                .setSmallIcon(android.R.drawable.ic_dialog_info)
//                .setAutoCancel(true)
//                .setContentTitle(contentTitle)
//                .setContentText(contentText);
//        manager.notify(10, builder.build());
//    }
}
