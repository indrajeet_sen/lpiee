package com.lpi.kmi.lpi.activities.AsyncTasks;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@SuppressLint("Range")
public class SubmitExamResultFdback extends AsyncTask<String, String, InputStream> {
    public Context context;
    public Callback callback;
    public int ResponseCode=0;
    public long crnt_ExmId;
    public ProgressDialog progressDialog;

    
    public SubmitExamResultFdback(Callback callback, Context context,String crnt_ExmId) {
        this.callback = callback;
        this.context = context;
        progressDialog = new ProgressDialog(context, R.style.AppTheme_Dark_Dialog);
        try {
            this.crnt_ExmId = Long.valueOf(crnt_ExmId);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            this.crnt_ExmId = Integer.parseInt(crnt_ExmId);
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected void onPreExecute() {
        showProgress(true, "Submitting...");
    }

    @SuppressLint("NewApi")
    @Override
    protected InputStream doInBackground(String... params) {
        org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                context.getString(R.string.Exam_NAMESPACE),
                context.getString(R.string.SubmitFeedBackforResultOfExam));

        String str_XML = CommonClass.SubmitExamFeedXML(context, StaticVariables.UserLoginId, "" + crnt_ExmId);

        // property which holds input parameters
        PropertyInfo inputPI1 = new PropertyInfo();
        inputPI1.setName("objXMLDoc");
        try {
            inputPI1.setValue(str_XML);
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        inputPI1.setType(String.class);
        request.addProperty(inputPI1);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.dotNet = true;
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));
        InputStream isResponse = null;
//        InputStream SubmitExmInputStream = null;


        try {
            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                    context.getString(R.string.SubmitFeedBackforResultOfExam), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            String strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString();
            Log.e("", "response=" + strResponse);

            isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
//            SubmitExmInputStream = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));

            if (CommonClass.IsCorrectXMLResponse(isResponse)){
                ResponseCode=0;
            }else {
                ResponseCode=1;
            }
            Log.e("", "Response=" + isResponse);

//            DatabaseHelper.SaveXMLData(context, DatabaseHelper.db,
//                    str_XML, context.getString(R.string.SubmitFeedBackforResultOfExam),"Complete");
        } catch (Exception e) {
            Log.e("", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "SubmitExamResultFdback", "doInBackground()", e.toString());
            ResponseCode=2;
//            DatabaseHelper.SaveXMLData(context, DatabaseHelper.db,
//                    str_XML, context.getString(R.string.SubmitFeedBackforResultOfExam),"Pending");
        }
        return isResponse;
    }

    @Override
    protected void onPostExecute(InputStream inputStream) {
        super.onPostExecute(inputStream);
        showProgress(false, "");
        callback.execute(inputStream,""+ResponseCode);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show, String Msg) {
        if (show) {
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(Msg);
            progressDialog.show();
        } else
            progressDialog.dismiss();
    }
}
