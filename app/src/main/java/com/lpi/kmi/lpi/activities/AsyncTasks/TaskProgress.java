package com.lpi.kmi.lpi.activities.AsyncTasks;

/**
 * Created by mustafakachwalla on 21/11/17.
 */

public  class TaskProgress {
    final int percentage;
    final String message;

    TaskProgress(int percentage, String message) {
        this.percentage = percentage;
        this.message = message;
    }
}