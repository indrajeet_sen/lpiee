package com.lpi.kmi.lpi.activities.AsyncTasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.ParticipantHomeActivity;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class UserLogin extends AsyncTask<String, String, String> {
    Context context;
    String EmailID, Passwd;
    ProgressDialog progressDialog;

    public UserLogin(Context context, String EmailID, String Passwd) {
        this.context = context;
        this.EmailID = EmailID;
        this.Passwd = Passwd;

        progressDialog = new ProgressDialog(context);
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                context.getString(R.string.Exam_NAMESPACE),
                context.getString(R.string.AuthenticateUser_Email));

        // property which holds input parameters
        PropertyInfo inputPI1 = new PropertyInfo();
        PropertyInfo inputPI2 = new PropertyInfo();
        PropertyInfo inputPI3 = new PropertyInfo();
        PropertyInfo inputPI4 = new PropertyInfo();
        PropertyInfo inputPI5 = new PropertyInfo();

        inputPI1.setName("SAPCode");
        try {
            Log.e("", "UserID=" + EmailID);
            //inputPI1.setValue(EncrptDcrpt.XMLEncryptiton(edtTxt_UserID.getText().toString()));
            inputPI1.setValue(EmailID);
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        inputPI1.setType(String.class);
        request.addProperty(inputPI1);

        inputPI2.setName("Password");
        try {
            //inputPI2.setValue(EncrptDcrpt.XMLEncryptiton(edtTxt_Pwd.getText().toString()));
            inputPI2.setValue(Passwd);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        inputPI2.setType(String.class);
        request.addProperty(inputPI2);

        inputPI3.setName("IMEIstring");
        inputPI3.setValue(CommonClass.getIMEINumber(context));
        inputPI3.setType(String.class);
        request.addProperty(inputPI3);

        inputPI4.setName("AppVersion");
        try {
            inputPI4.setValue((context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName));
//            inputPI4.setValue("51");
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        inputPI4.setType(String.class);
        request.addProperty(inputPI4);

        inputPI5.setName("RegDevice");
        inputPI5.setValue("Y");
        inputPI5.setType(String.class);
        request.addProperty(inputPI5);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.dotNet = true;
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL), 90000);


        try {
            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
            androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                    context.getString(R.string.AuthenticateUser_Email), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            // Assign it to static variable
            Log.e("", "response=" + response.toString());
            return response.toString();

        } catch (Exception e) {
            Log.e("", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "UserLogin", "returnLoginStatus", e.toString());
            return "";
        }
    }


    protected void onPostExecute(String strLoginResponse) {
        try {
            boolean isSuccess = false;
            JSONObject objJsonObject = new JSONObject(strLoginResponse);
            JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");
            for (int index = 0; index < objJsonArray.length(); index++) {
                JSONObject jsonobject = objJsonArray.getJSONObject(index);

                String str_login = jsonobject.getString("Message");
                if (str_login.equals("Device authentication failed")) {
                    alertPIN("Device authentication failed", "Device authentication failed...!\nDo you want to register this device?");
                } else if (str_login.equals("Outdated version found")) {
                    alertPIN("Outdated version found", "\nYou are currently using an outdated version of app+, "
                            + "please install the latest version from below link\n\n-" + jsonobject.getString("AppLink"));

                } else if (str_login.equals("User Created")) {
                    String imei = CommonClass.getIMEINumber(context);
                    isSuccess = true;
                    DatabaseHelper.UserName(context, DatabaseHelper.db, jsonobject.getString("UserId"));
                    StaticVariables.crnt_InscType = jsonobject.getString("Ins_Type");
                    StaticVariables.isLogin = true;
                    StaticVariables.UserName = jsonobject.getString("LegalName");
                    StaticVariables.UserLoginId = jsonobject.getString("UserId");
                    StaticVariables.ParentId = jsonobject.getString("ParentId");
                    StaticVariables.ParentName = jsonobject.getString("ParentName");
                    StaticVariables.UserMobileNo = jsonobject.getString("UserMobileNo1");
                    StaticVariables.offlineMode = "N";
                    StaticVariables.AppLink = jsonobject.getString("AppLink");
                    StaticVariables.UserType = jsonobject.getString("UserType");
                    DatabaseHelper.SaveUserInfo(context, DatabaseHelper.db,
                            jsonobject.getString("UserId"),
                            Passwd,
                            jsonobject.getString("LegalName"),
                            StaticVariables.crnt_InscType,
                            jsonobject.getString("AllowOfflineMode"),
                            "" + jsonobject.get("PermisableDays"),
                            jsonobject.getString("LastAccessDate"),
                            jsonobject.getString("UserType"),
                            jsonobject.getString("UserEMailAddress"),
                            jsonobject.getString("UserMobileNo1"),
                            jsonobject.getString("ParentId"),
                            jsonobject.getString("ParentName"),
                            null,
                            jsonobject.getString("AppLink"));
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    Intent intent = new Intent(context, ParticipantHomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
                    ((Activity) context).finish();
                } else if (str_login.equals("Outdated version found")) {
                    alertOutdatedVersion(context, "\nYou are currently using an outdated version of Candidate Manager App, "
                                    + "\nPlease install the latest version from below link-",
                            jsonobject.getString("AndroidAppLink") + "");
                } else {
                    //Toast.makeText(context, "Invalid login credentials", Toast.LENGTH_SHORT).show();
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    alertPIN("Oops", "Server not responding...\nPlease try again later");
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("", "error=" + e.toString());
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            //Toast.makeText(context, "Server not responding....\nPlease try again later", Toast.LENGTH_LONG).show();
            alertPIN("Oops", "Server not responding...\nPlease try again later");
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "Login", "CandidateLogin.onPostExecute()", e.toString());
        }

    }

    private void alertPIN(String Title, String Message) {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_exit_dialog, null);
            TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            Button btn_ok = (Button) dialogView.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialogView.findViewById(R.id.btn_no);
            btn_no.setVisibility(View.GONE);
//            promt_title.setText("MPIN Authentication");Invalid credentials,
//            txt_message.setText("Do you want to set a 4 digit authentication MPIN for future Login?");
            promt_title.setText(Title);
            txt_message.setText(Message);
            btn_ok.setText("OK");
            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void alertOutdatedVersion(final Context context, String Message, final String App_Link) {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_outdated_version_dialog, null);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            TextView txt_app_link = (TextView) dialogView.findViewById(R.id.txt_app_link);
            Button btn_no = (Button) dialogView.findViewById(R.id.btn_no);
            btn_no.setVisibility(View.GONE);
            txt_app_link.setText(App_Link);
            txt_message.setText(Message);
            txt_app_link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(App_Link));
                    webIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    webIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    webIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(webIntent);
                }
            });
            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
