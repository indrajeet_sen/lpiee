package com.lpi.kmi.lpi.activities.AsyncTasks;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by mustafakachwalla on 10/01/18.
 */

@SuppressLint("Range")
public class syncContentQueries extends AsyncTask<String, String, String> {
    Context context;
    String query = "", strResponse = "",uniqueId;
    ProgressDialog progressDialog;

    public syncContentQueries(Context context, String query,String uniqueId) {
        this.context = context;
        this.query = query;
        this.uniqueId=uniqueId;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressDialog = new ProgressDialog(context, R.style.AppTheme_Dark_Dialog);
        } else {
            progressDialog = new ProgressDialog(context);
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Submitting your " + query.toLowerCase() + " \nPlease wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            Cursor cursor = DatabaseHelper.getContentQueries(context, DatabaseHelper.db, StaticVariables.UserLoginId);
            if (cursor.getCount() > 0) {
                org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                        context.getResources().getString(R.string.Exam_NAMESPACE), context.getResources().getString(R.string.syncContentQueries));

                //property which holds input parameters
                PropertyInfo inputPI1 = new PropertyInfo();
                inputPI1.setName("objXMLDoc");
                inputPI1.setValue(CommonClass.ContentQueryXML(context,uniqueId));
                inputPI1.setType(String.class);
                request.addProperty(inputPI1);
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                //Set output SOAP object
                envelope.setOutputSoapObject(request);
                //Create HTTP call object
                HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getResources().getString(R.string.Exam_URL));
//        InputStream isResponse = null;

                //Involve Web service
                LPIEEX509TrustManager.allowAllSSL();
//                FakeX509TrustManager.trustSSLCertificate((Activity) context);
                androidHttpTransport.call(context.getResources().getString(R.string.Exam_SOAP_ACTION) +
                        context.getResources().getString(R.string.syncContentQueries), envelope);
                //Get the response
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strResponse = response.toString();
            }
            cursor.close();
        } catch (Exception ioe) {
            Log.d("doInBackground", "syncContentQueries error=" + ioe.getMessage());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "syncContentQueries", "doInBackground", ioe.toString());
        }
        return strResponse;
    }

    @Override
    protected void onPostExecute(String s) {
        progressDialog.dismiss();

        try {
            JSONObject objJsonObject = new JSONObject(strResponse);
            JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");

            for (int index = 0; index < objJsonArray.length(); index++) {
                JSONObject jsonobject = objJsonArray.getJSONObject(index);
                if (jsonobject.has("Status")) {
                    DatabaseHelper.UpdtContentQueries(context, DatabaseHelper.db,
                            "" + jsonobject.getInt("MobRefId"), jsonobject.getString("UserId"),
                            jsonobject.getString("Status"));
                } else {
                    DatabaseHelper.UpdtContentQueries(context, DatabaseHelper.db,
                            "" + jsonobject.getInt("MobRefId"), StaticVariables.UserLoginId,
                            jsonobject.getString("Pending"));
                }
            }
            Toast.makeText(context, "Your " + query.toLowerCase() + " submitted successfully.", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(context, "Your " + query.toLowerCase() + " saved successfully.", Toast.LENGTH_SHORT).show();
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "syncContentQueries", "onPostExecute", e.toString());
        }

    }


}
