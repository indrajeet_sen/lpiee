package com.lpi.kmi.lpi.activities.AsyncTasks;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by mustafakachwalla on 28/11/17.
 */

@SuppressLint("Range")
public class syncExams extends AsyncTask<String, String, String> {
    public final Callback<String> callback;
    Context context;
    ProgressDialog progressDialog;
    String strResponse = "";
    String activityName = "";
    String ExamID = "", CandidateId = "", ExamType = "", InitFrom = "";

    public syncExams(Context context, String name, String CandidateId) {
        this.context = context;
        this.activityName = name;
        this.CandidateId = CandidateId;
//        progressDialog = new ProgressDialog(this.context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressDialog = new ProgressDialog(this.context, R.style.AppTheme_Dark_Dialog);
        } else {
            progressDialog = new ProgressDialog(this.context);
        }

        callback = null;
    }

    public syncExams(Callback<String> callback, Context context, String name, String CandidateId, String initFrom) {
        this.context = context;
        this.activityName = name;
        this.callback = callback;
        this.CandidateId = CandidateId;
        this.InitFrom = initFrom;
//        progressDialog = new ProgressDialog(this.context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressDialog = new ProgressDialog(this.context, R.style.AppTheme_Dark_Dialog);
        } else {
            progressDialog = new ProgressDialog(this.context);
        }
    }

    public syncExams(Callback<String> callback, Context context, String name, String CandidateId, String ExamID, String ExamType) {
        this.context = context;
        this.activityName = name;
        this.callback = callback;
        this.CandidateId = CandidateId;
        this.ExamID = ExamID;
        this.ExamType = ExamType;
//        progressDialog = new ProgressDialog(this.context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressDialog = new ProgressDialog(this.context, R.style.AppTheme_Dark_Dialog);
        } else {
            progressDialog = new ProgressDialog(this.context);
        }
    }

    @Override
    protected void onCancelled(String s) {
        super.onCancelled(s);
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setIndeterminate(false);
        if (InitFrom.equals("LPILessons")) {
            progressDialog.setMessage("Synchronising Quiz Status...\nPlease wait...");
        }else {
            progressDialog.setMessage("Synchronising Data\nPlease wait...");
        }
        progressDialog.setCancelable(false);
        progressDialog.show();
    }


    @Override
    protected String doInBackground(String... strings) {
        strResponse = "";
        if (InitFrom.equals("LPILessons")) {
            GetSectionExamMapping();
        } else {
            boolean IsExamMasterAvailable = DatabaseHelper.IsExamMasterAvailable(context, DatabaseHelper.db);
            boolean IsExamAvailable = DatabaseHelper.IsExamTypeAvailable(context, DatabaseHelper.db, ExamType);
            if (!IsExamMasterAvailable || !IsExamAvailable) {
                GetExamMaster();
            } else if (ExamID != null || !ExamID.equals("")) {
                GetQueMaster(ExamID, ExamType);
            }
        }
        return strResponse;
    }

    @Override
    protected void onPostExecute(String result) {
//        super.onPostExecute(s);
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

        /*if (activityName.equals("Login")) {
            Intent intent = new Intent(context, ParticipantHomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        } else
        if (activityName.equals("MCQ_QuestionActivity")) {
            Intent intent;
            if (StaticVariables.UserType.equalsIgnoreCase("C")) {
                intent = new Intent(context, ParticipantMenuActivity.class);
            } else {
                intent = new Intent(context, LPI_TrainerMenuActivity.class);
            }
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        }
*/
        if ((activityName.equals("Login") || activityName.equals("LPI_TrainerMenuActivity")
                || activityName.equals("MCQ_QuestionActivity") || activityName.equals("ParticipantHomeActivity")
                || activityName.equals("ViewResultActivity")|| activityName.equals("LPILessonQuizActivity")
                || activityName.equals("CandidateRegistration") || activityName.equals("LPI_LessonActivity")
                || activityName.equals("EmotionalExcellenceListActivity") || activityName.equals("LPI_Lesson_Trip"))
                && callback != null) {
            strResponse = (result.toString());
            callback.execute(strResponse, "");
        }
//        else {
//            Intent intent = new Intent(context, ParticipantHomeActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            context.startActivity(intent);
//        }

    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @SuppressLint("NewApi")
    public void GetExamMaster() {

        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.GetAssignedExamSetForUser));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();

            inputPI1.setName("objXMLDoc");
            try {
                String objXMLDoc = CommonClass.GenerateXML(context, 1, "ExamSetRequest", new String[]{"LoginID"},
                        new String[]{CandidateId});
                inputPI1.setValue(objXMLDoc);
            } catch (Exception e1) {
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "SyncExamMaster", "GetExamMaster()", e1.toString());
            }
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            // Set output SOAP object
            envelope.setOutputSoapObject(request);
            // Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));

            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                    context.getString(R.string.GetAssignedExamSetForUser), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            // Assign it to static variable
            strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString();
            Log.e("", "response=" + strResponse);

            InputStream isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            InputStream is = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            if (IsCorrectXMLResponse(isResponse)) {
                NodeList nList = getExamDatafrmXML(is, "Exam");
                for (int i = 0; i < nList.getLength(); i++) {
                    Node node = nList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;
                        DatabaseHelper.SaveExamMaster(context, DatabaseHelper.db, getValue("ExamID", element),
                                getValue("ExamCode", element), getValue("ExamType", element), getValue("ExamDesc", element),
                                getValue("ExamDetails", element), getValue("AllowNavigationFlag", element), getValue("OptionCount", element),
                                getValue("ShowRandomQuestions", element), getValue("TotalQuestions", element), getValue("TotalTimeAllowedInMinutes", element),
                                getValue("IsAllowResultFeedback", element), getValue("IsPracticeExam", element),
                                getValue("AllowedAttempt", element), getValue("EffectiveDate", element),
                                getValue("IsAllQuesMendatory", element), getValue("NoOfAttempts", element));

                        ExamID = getValue("ExamID", element);
                        ExamType = getValue("ExamType", element);
                    }
                    if (!DatabaseHelper.CheckExamQueCount(context, DatabaseHelper.db, BigInteger.valueOf(Long.valueOf(ExamID)))) {
                        GetQueMaster(ExamID, ExamType);
                    }
                }
            }

        } catch (Exception e) {
            Log.e("", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "SyncExamMaster", "IntentService > GetExamMaster", e.toString());
        }
    }

    @SuppressLint("NewApi")
    public void GetQueMaster(String ExamId, String ExamType) {

        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.GetQuestionSetForExam));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();

            inputPI1.setName("objXMLDoc");
            try {
                String objXMLDoc = CommonClass.GenerateXML(context, 1, "QuestionSetRequest", new String[]{"ExamID"}, new String[]{ExamId});
                inputPI1.setValue(objXMLDoc);
            } catch (Exception e1) {
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "SyncExamMaster", "GetQueMaster()", e1.toString());
            }
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            // Set output SOAP object
            envelope.setOutputSoapObject(request);
            // Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));


            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                    context.getString(R.string.GetQuestionSetForExam), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            // Assign it to static variable
            strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString();
            Log.e("", "response=" + strResponse);

            InputStream isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            InputStream is = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            if (IsCorrectXMLResponse(isResponse)) {

                NodeList nList = getExamDatafrmXML(is, "Questions");
                for (int i = 0; i < nList.getLength(); i++) {

                    Node node = nList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;
                        DatabaseHelper.SaveQuestionMaster(context, DatabaseHelper.db,
                                ExamId, getValue("QuestionId", element),
                                getValue("QuestionCode", element), getValue("Question", element),
                                getValue("NoOfAttemptAllow", element), getValue("CorrectAnsRemark", element),
                                getValue("WrongAnsRemark", element), getValue("OptionCount", element));
//                        GetAnsMaster(ExamId);
                    }
                }

                GetAnsMaster(ExamId);

//                if (ExamType.equals("5")){
//                    getCandidatesFeedback(ExamId,CandidateId);
//                }
            }

        } catch (Exception e) {
            Log.e("", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "SyncExamMaster", "IntentService > GetQueMaster", e.toString());
        }
    }

    @SuppressLint("NewApi")
    public void GetAnsMaster(String ExamId) {

        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.GetAnswerSetExam));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();

            inputPI1.setName("objXMLDoc");
            try {
                String objXMLDoc = CommonClass.GenerateXML(context, 1, "AnswerSetRequest", new String[]{"ExamID"}, new String[]{ExamId});
                inputPI1.setValue(objXMLDoc);
            } catch (Exception e1) {
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "SyncExamMaster", "GetAnsMaster()", e1.toString());
            }
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            // Set output SOAP object
            envelope.setOutputSoapObject(request);
            // Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));


            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                    context.getString(R.string.GetAnswerSetExam), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            // Assign it to static variable
            strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString();
            Log.e("", "response=" + strResponse);


            InputStream isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            InputStream is = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
//            if (IsCorrectXMLResponse(isResponse)) {
            String QueId = "";
            NodeList nList = getExamDatafrmXML(is, "Answers");
            for (int i = 0; i < nList.getLength(); i++) {

                Node node = nList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    QueId = getValue("QuestionId", element);
                    DatabaseHelper.SaveAnswerMaster(context, DatabaseHelper.db,
                            ExamId, QueId, getValue("AnswerId", element),
                            getValue("AnswerCode", element), getValue("AnswerDesc", element));
                }
            }

            // TODO: Get Correct question-answer mapping
            if (!ExamId.equals("4001")) {
                GetCorrectQueAnsMapping(ExamId);
            }

//            if (ExamId.equals("6001")) {
//                GetCandidateAttemptedExamsDetails(ExamId, CandidateId);
//            }

//            }
        } catch (Exception e) {
            Log.e("", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "SyncExamMaster", "IntentService > GetAnsMaster", e.toString());
        }
    }

    @SuppressLint("NewApi")
    public void GetCorrectQueAnsMapping(String ExamId) {

        org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                context.getString(R.string.Exam_NAMESPACE),
                context.getString(R.string.GetCurrectQueAnsSet));

        // property which holds input parameters
        PropertyInfo inputPI1 = new PropertyInfo();

        inputPI1.setName("objXMLDoc");
        try {
            String objXMLDoc = CommonClass.GenerateXML(context, 1, "CorrectQueAnsSet", new String[]{"ExamID"}, new String[]{ExamId});
            inputPI1.setValue(objXMLDoc);
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        inputPI1.setType(String.class);
        request.addProperty(inputPI1);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.dotNet = true;
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));


        try {
            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                    context.getString(R.string.GetCurrectQueAnsSet), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            // Assign it to static variable
            strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString();
            Log.e("", "response=" + strResponse);

            InputStream isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            InputStream is = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            if (IsCorrectXMLResponse(isResponse)) {
                String QueId = "";
                NodeList nList = getExamDatafrmXML(is, "QuesAnsSet");
                for (int i = 0; i < nList.getLength(); i++) {
                    Node node = nList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;
                        QueId = getValue("QuestionId", element);
                        DatabaseHelper.SaveCorctQueAnsMapping(context, DatabaseHelper.db,
                                ExamId, QueId, getValue("QuestionCode", element),
                                getValue("AnswerId", element), getValue("AnswerCode", element));
                    }
                }
                DatabaseHelper.SaveExamQueAns_SyncMaster(context, DatabaseHelper.db, ExamId, QueId);
            }

//            Cursor cquery;
//            cquery = DatabaseHelper.db.query(context.getResources().getString(R.string.TBL_LPIQuestionCorrectAnswerMapping), null,
//                    null, null, null, null, null);
//            int cntr = cquery.getCount();
//            Log.e("", "Counter=" + cntr);

//            }
        } catch (Exception e) {
            Log.e("", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "SyncExamMaster", "IntentService > GetCorrectQueAnsMapping", e.toString());
        }
    }

    // TODO: Get the List of Node from XML
    public NodeList getExamDatafrmXML(InputStream strXML, String NodeListItem) {
        NodeList nList = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(strXML);
            Element element = doc.getDocumentElement();
            element.normalize();
            nList = doc.getElementsByTagName(NodeListItem);
        } catch (Exception e) {
            Log.e("", "error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "SyncExamMaster", "getExamDatafrmXML()", e.toString());
        }

        return nList;
    }

    // TODO: Check the Response Code
    public boolean IsCorrectXMLResponse(InputStream strXML) {
        boolean isZeroResponseCode = false;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(strXML);
            Element element = doc.getDocumentElement();
            element.normalize();
            String ResponseCode = getValue("ResponseCode", element);
            if (ResponseCode.equals("0")) {
                isZeroResponseCode = true;
            } else {
                isZeroResponseCode = false;
            }
        } catch (Exception e) {
            Log.e("", "error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "SyncExamMaster", "IsCorrectXMLResponse()", e.toString());
            isZeroResponseCode = false;
        }
        return isZeroResponseCode;
    }

    // TODO: Get the Value of Element
    public String getValue(String tag, Element element) {
        try {
            NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
            Node node = nodeList.item(0);
            return node.getNodeValue();
        } catch (Exception e) {
            Log.e("", "error=" + e.toString());
//            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
//                    "SyncExamMaster", "getValue()", e.toString());
            return "";
        }
    }

    @SuppressLint("NewApi")
    public void GetSectionExamMapping() {
        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.GetSectionExamMapping));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();

            inputPI1.setName("objXMLDoc");
            try {
                String objXMLDoc = CommonClass.GenerateXML(context, 1, "GetSectionExamMapping", new String[]{"UserId"},
                        new String[]{CandidateId});
                inputPI1.setValue(objXMLDoc);
            } catch (Exception e1) {
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "SyncExams", "GetSectionExamMapping()", e1.toString());
            }
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            // Set output SOAP object
            envelope.setOutputSoapObject(request);
            // Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));

            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
//            FakeX509TrustManager.trustSSLCertificate((Activity) context);
            androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                    context.getString(R.string.GetSectionExamMapping), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            // Assign it to static variable
            strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString();
            Log.e("", "response=" + strResponse);

            InputStream isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            InputStream is = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
            if (IsCorrectXMLResponse(isResponse)) {
                NodeList nList = getExamDatafrmXML(is, "Exam");
                for (int i = 0; i < nList.getLength(); i++) {
                    Node node = nList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;
                        DatabaseHelper.SaveSectionExamMapping(context, DatabaseHelper.db,
                                getValue("SectionId", element),
                                getValue("ExamId", element),
                                getValue("SectionDesc", element),
                                getValue("ExamDesc", element),
                                getValue("IsActive", element),
                                getValue("CreatedBy", element),
                                getValue("CreateDtime", element),
                                getValue("UpdatedBy", element),
                                getValue("UpdateDtime", element));
                    }
                }
            }
            GetExamMaster();
//            GetSectionExamMST();

        } catch (Exception e) {
            Log.e("", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "SyncExams", "GetSectionExamMapping()", e.toString());
        }
    }
}
