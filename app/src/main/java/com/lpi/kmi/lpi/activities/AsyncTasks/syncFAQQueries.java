package com.lpi.kmi.lpi.activities.AsyncTasks;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

@SuppressLint("Range")
public class syncFAQQueries extends AsyncTask<String, String, String> {
    Context context;
    String query="", strResponse = "", mobref = "";
    ProgressDialog progressDialog;
    Callback<String> callback;

    public syncFAQQueries(Callback<String> callback, Context activity, String query, String mobref) {
        this.context = activity;
        this.query = query;
        this.mobref = mobref;
        this.callback = callback;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressDialog = new ProgressDialog(context, R.style.AppTheme_Dark_Dialog);
        } else {
            progressDialog = new ProgressDialog(context);
        }
    }


    public syncFAQQueries( Context activity, String query, String mobref) {
        this.context = activity;
        this.query = query;
        this.mobref = mobref;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressDialog = new ProgressDialog(context, R.style.AppTheme_Dark_Dialog);
        } else {
            progressDialog = new ProgressDialog(context);
        }
    }

    public syncFAQQueries() {

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Submitting your query -\n" + query.toLowerCase() + " \nPlease wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getResources().getString(R.string.Exam_NAMESPACE),
                    context.getResources().getString(R.string.SubmitFAQTopic));
            //property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            inputPI1.setName("objXMLDoc");
            inputPI1.setValue(CommonClass.FAQXML(context, query, mobref));
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            //Set output SOAP object
            envelope.setOutputSoapObject(request);
            //Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getResources().getString(R.string.Exam_URL));
            //Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
            androidHttpTransport.call(context.getResources().getString(R.string.Exam_SOAP_ACTION) +
                    context.getResources().getString(R.string.SubmitFAQTopic), envelope);
            //Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            strResponse = response.toString();
        } catch (Exception ioe) {
            Log.d("doInBackground", "syncContentQueries error=" + ioe.getMessage());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "syncFAQQueries", "doInBackground", ioe.toString());
        }
        return strResponse;
    }

    @Override
    protected void onPostExecute(String s) {
        try {
            JSONObject objJsonObject = new JSONObject(strResponse);
            JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");
            for (int index = 0; index < objJsonArray.length(); index++) {
                JSONObject jsonobject = objJsonArray.getJSONObject(index);
                if (jsonobject.has("ResponseCode") && jsonobject.getString("ResponseCode").equals("0")) {
                    DatabaseHelper.UpdateFAQList(context, DatabaseHelper.db,
                            ""+ jsonobject.get("QuesId"), jsonobject.getString("MobRefId"));
                }
            }
            Toast.makeText(context, "Your query \n" + query.toLowerCase() + "\nsubmitted successfully.", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(context, "Your query\n" + query.toLowerCase() + "\nsaved successfully.", Toast.LENGTH_SHORT).show();
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "syncFAQQueries", "onPostExecute", e.toString());
        }
        progressDialog.dismiss();
        callback.execute("","Success");
    }
}
