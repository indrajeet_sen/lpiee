package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.AsyncSubmitExams;
import com.lpi.kmi.lpi.adapter.Adapters.AttempetedAxmListBaseAdapter;
import com.lpi.kmi.lpi.adapter.GetterSetter.GetterSetter;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;

import net.sqlcipher.Cursor;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

@SuppressLint("Range")
public class AttemtedExamActivity extends AppCompatActivity {

    public static ArrayList<GetterSetter> arrlst_allExams;
    public static ArrayList<String> arrlst_AttemptedExamID = new ArrayList<String>();
    public static ArrayList<String> arrlst_AttemptedCandidateID = new ArrayList<String>();
    public static ArrayList<String> arrlst_AttemptedExamDate = new ArrayList<String>();
    public static ArrayList<String> arrlst_AttemptedExamResult = new ArrayList<String>();
    public static ArrayList<String> arrlst_AttemptedExamName = new ArrayList<String>();
    public static ArrayList<String> arrlst_AttemptedExamDetail = new ArrayList<String>();
    public static ArrayList<String> arrlst_AttemptedExamType = new ArrayList<String>();
    public static ArrayList<String> arrlst_AttemptedID = new ArrayList<String>();
    public static ArrayList<String> arrlst_AttemptedExamIsAllowFdback = new ArrayList<String>();
    public static ArrayList<String> arrlst_IsAllQuesMendatory = new ArrayList<String>();

    public static AttempetedAxmListBaseAdapter custAdapter;
    InputStream AttemptedExmInputStream;
    int ResponseCode;

    ListView list_Exam;
    LinearLayout Lnr_AttemptExmInit;
    TextView txt_AttemptExmInit;
    ProgressBar Exm_Attemptprogress_bar;
    CoordinatorLayout coordinatorLayout = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_attemted_exam);

        list_Exam = (ListView) findViewById(R.id.Lst_AttemptExamList);
        Lnr_AttemptExmInit = (LinearLayout) findViewById(R.id.Lnr_AttemptExmInit);
        txt_AttemptExmInit = (TextView) findViewById(R.id.txt_AttemptExmInit);
        Exm_Attemptprogress_bar = (ProgressBar) findViewById(R.id.Exm_Attemptprogress_bar);

        list_Exam.setVisibility(View.GONE);
        Lnr_AttemptExmInit.setVisibility(View.VISIBLE);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.cordinatorLayout);

//        getAttemptedExam();
    }

    @Override
    protected void onResume() {
        super.onResume();
        list_Exam.setVisibility(View.GONE);
        Lnr_AttemptExmInit.setVisibility(View.VISIBLE);
        getAttemptedExam();

        try {
            if (coordinatorLayout == null) {
                coordinatorLayout = (CoordinatorLayout) findViewById(R.id.cordinatorLayout);
            }
            if (CommonClass.isConnected(AttemtedExamActivity.this)) {
                Cursor cursor = DatabaseHelper.GetXMLData(getApplicationContext(), DatabaseHelper.db);
                if ((cursor.getCount() > 0) && (CommonClass.isConnected(AttemtedExamActivity.this))) {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Offline Exam data available,\nPlease Sync to submit.", Snackbar.LENGTH_INDEFINITE)
                            .setAction("Sync Data", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (CommonClass.isConnected(AttemtedExamActivity.this)) {
                                        new AsyncSubmitExams(AttemtedExamActivity.this, "ParticipantMenuActivity").execute();
                                    } else {
                                        Toast.makeText(AttemtedExamActivity.this, "No internet connectivity", Toast.LENGTH_SHORT);
                                    }
                                }
                            });

                    snackbar.show();
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getAttemptedExam() {
        try {
            clearList();
            if (CommonClass.isConnected(AttemtedExamActivity.this)) {
                new GetCandidateAtemptedExams(0, AttemtedExamActivity.this).execute();
            } else {
                txt_AttemptExmInit.setText("No Internet Connection Found, \nPlease connect to the internet and try again.");
                Exm_Attemptprogress_bar.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
            DatabaseHelper.SaveErroLog(AttemtedExamActivity.this, DatabaseHelper.db,
                    "AttemtedExamActivity", "onResume()", e.toString());
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(AttemtedExamActivity.this, ParticipantMenuActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        AttemtedExamActivity.this.finish();
    }

    void clearList() {
        arrlst_AttemptedExamID.clear();
        arrlst_AttemptedCandidateID.clear();
        arrlst_AttemptedExamDate.clear();
        arrlst_AttemptedExamResult.clear();
        arrlst_AttemptedExamName.clear();
        arrlst_AttemptedExamDetail.clear();
        arrlst_AttemptedExamType.clear();
        arrlst_AttemptedID.clear();
        arrlst_AttemptedExamIsAllowFdback.clear();
        arrlst_IsAllQuesMendatory.clear();
    }

    public void custList() {
        try {
            arrlst_allExams = getLstData();
            custAdapter = new AttempetedAxmListBaseAdapter(this, arrlst_allExams);
            list_Exam.setAdapter(custAdapter);
            custAdapter.notifyDataSetChanged();
            int index = list_Exam.getFirstVisiblePosition();
            View v = list_Exam.getChildAt(0);
            int top = (v == null) ? 0 : v.getTop();
            list_Exam.setSelectionFromTop(index, top);
            if (arrlst_allExams.size() <= 0) {
                list_Exam.setVisibility(View.GONE);
                Exm_Attemptprogress_bar.setVisibility(View.GONE);
                txt_AttemptExmInit.setText("Any attempted exam not found!!!");
                Lnr_AttemptExmInit.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            Log.e("Tag", "cust error=" + e.toString());
        }

    }

    private ArrayList<GetterSetter> getLstData() {
        // TODO Auto-generated method stub
        ArrayList<GetterSetter> arrlst = new ArrayList<GetterSetter>();
        for (int i = 0; i < arrlst_AttemptedExamID.size(); i++) {
            try {
                GetterSetter candi = new GetterSetter("" + (i + 1), arrlst_AttemptedExamID.get(i), arrlst_AttemptedCandidateID.get(i),
                        arrlst_AttemptedExamDate.get(i), arrlst_AttemptedExamResult.get(i), arrlst_AttemptedExamName.get(i),
                        arrlst_AttemptedExamDetail.get(i), arrlst_AttemptedExamType.get(i), arrlst_AttemptedExamIsAllowFdback.get(i),
                        arrlst_AttemptedID.get(i), arrlst_IsAllQuesMendatory.get(i));
                arrlst.add(candi);
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();

                DatabaseHelper.SaveErroLog(AttemtedExamActivity.this, DatabaseHelper.db,
                        "AttemtedExamActivity", "getLstData", e.toString());
            }
        }
        return arrlst;
    }

    public class GetCandidateAtemptedExams extends AsyncTask<String, String, InputStream> {
        public int position;
        public Context context;

        public GetCandidateAtemptedExams(int position, Context con) {
            this.position = position;
            this.context = con;
        }


        @SuppressLint("NewApi")
        @Override
        protected InputStream doInBackground(String... params) {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    AttemtedExamActivity.this.getString(R.string.Exam_NAMESPACE),
                    AttemtedExamActivity.this.getString(R.string.GetCandidateAtemptedExams));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            inputPI1.setName("objXMLDoc");
            try {
                String objXMLDoc = CommonClass.GenerateXML(context, 1, "GetCandAttemtedExamRequest",
                        new String[]{"LoginId"},
                        new String[]{StaticVariables.UserLoginId});
                inputPI1.setValue(objXMLDoc);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            // Set output SOAP object
            envelope.setOutputSoapObject(request);
            // Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(AttemtedExamActivity.this.getString(R.string.Exam_URL));
            InputStream isResponse = null;


            try {
                // Involve Web service
                LPIEEX509TrustManager.allowAllSSL();
                androidHttpTransport.call(AttemtedExamActivity.this.getString(R.string.Exam_SOAP_ACTION) +
                        AttemtedExamActivity.this.getString(R.string.GetCandidateAtemptedExams), envelope);
                // Get the response
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

                String strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString();
                Log.e("", "response=" + strResponse);

                isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
                AttemptedExmInputStream = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
                Log.e("", "Response=" + isResponse);
            } catch (Exception e) {
                ResponseCode = 2;
                Log.e("", "Login Error=" + e.toString());
                DatabaseHelper.SaveErroLog(AttemtedExamActivity.this, DatabaseHelper.db,
                        "AttemtedExamActivity", "GetCandidateAtemptedExams.doInBackground()", e.toString());
            }
            return isResponse;
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            try {
                if (CommonClass.IsCorrectXMLResponse(inputStream)) {
                    NodeList nList = CommonClass.GetXMLElementNodeList(AttemptedExmInputStream, "Exams");
                    if (nList == null) {

                    } else if (nList.getLength() > 0) {
                        for (int i = 0; i < nList.getLength(); i++) {
                            Node node = nList.item(i);
                            if (node.getNodeType() == Node.ELEMENT_NODE) {
                                Element element = (Element) node;
                                if (CommonClass.getXMLElementValue("ExamID", element).equals("4001")) {

                                } else {
                                    arrlst_AttemptedExamID.add("" + CommonClass.getXMLElementValue("ExamID", element));
                                    arrlst_AttemptedCandidateID.add("" + CommonClass.getXMLElementValue("CandidateID", element));
                                    arrlst_AttemptedExamDate.add("" + CommonClass.getXMLElementValue("ExamDate", element));
                                    arrlst_AttemptedExamResult.add("" + CommonClass.getXMLElementValue("Result", element));
                                    arrlst_AttemptedExamName.add("" + CommonClass.getXMLElementValue("ExamName", element));
                                    arrlst_AttemptedExamDetail.add("" + CommonClass.getXMLElementValue("ExamDetail", element));
                                    arrlst_AttemptedExamType.add("" + CommonClass.getXMLElementValue("ExamType", element));
                                    arrlst_AttemptedID.add("" + CommonClass.getXMLElementValue("ExamAttempt", element));
                                    arrlst_AttemptedExamIsAllowFdback.add("" + CommonClass.getXMLElementValue("IsAllowResultFeedback", element));
                                    arrlst_IsAllQuesMendatory.add("" + CommonClass.getXMLElementValue("IsAllQuesMendatory", element));
                                }
                            }
                        }
                    }
                    list_Exam.setVisibility(View.VISIBLE);
                    Lnr_AttemptExmInit.setVisibility(View.GONE);
                    custList();
                } else if (ResponseCode == 2) {
                    txt_AttemptExmInit.setText("Server is not responding...");
                    Exm_Attemptprogress_bar.setVisibility(View.GONE);
                } else {
                    txt_AttemptExmInit.setText("Any attempted exam not found!!!");
                    Exm_Attemptprogress_bar.setVisibility(View.GONE);
                }

            } catch (Exception e) {
                DatabaseHelper.SaveErroLog(AttemtedExamActivity.this, DatabaseHelper.db,
                        "AttemtedExamActivity", "GetCandidateAtemptedExams.onPostExecute()", e.toString());
            }
        }
    }


}
