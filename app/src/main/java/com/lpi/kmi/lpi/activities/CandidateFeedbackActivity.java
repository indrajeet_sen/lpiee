package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.GetCandidateFeedback;
import com.lpi.kmi.lpi.adapter.GetterSetter.ModelGroupClass;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("Range")
public class CandidateFeedbackActivity extends AppCompatActivity implements Animation.AnimationListener, SwipeRefreshLayout.OnRefreshListener {

    TextView no_data_foundTextView, txt_Title;
    LinearLayoutManager linearLayoutManager;
    FeedBack_Adapter feedBack_adapter;
    String CandidateId, ExamType, initFrom, CandidateName;
    SwipeRefreshLayout mSwipeRefreshLayout;

    FeedBack_Adapter.onClickListener onClickListener = new FeedBack_Adapter.onClickListener() {

        @Override
        public void onLinearCardViewClick(int position, View view) {

        }
    };
    private RecyclerView recyclerView;
    private List<ModelGroupClass> ExpListItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_candidate_feedback);
        txt_Title = (TextView) findViewById(R.id.txt_Title);

        initUI();
    }

    @Override
    public void onBackPressed() {
        Intent intent;
        if (initFrom.equals("Search")) {
            intent = new Intent(CandidateFeedbackActivity.this, LPI_SearchActivity.class);
        } else if (initFrom.equals("Status")) {
            intent = new Intent(CandidateFeedbackActivity.this, LPI_StatusActivity.class);
        } else {
            intent = new Intent(CandidateFeedbackActivity.this, LPI_TrainerMenuActivity.class);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        CandidateFeedbackActivity.this.finish();
    }

    private void initUI() {
        try {

            CandidateId = getIntent().getStringExtra("CandidateId");
            CandidateName = getIntent().getStringExtra("CandidateName");
            ExamType = getIntent().getStringExtra("ExamType");
            initFrom = getIntent().getStringExtra("initFrom");
            txt_Title.setText(CandidateName);
            //actionBarSetup();

            recyclerView = (RecyclerView) findViewById(R.id.recy_list);
            no_data_foundTextView = (TextView) findViewById(R.id.no_data_foundTextView);
            no_data_foundTextView.setVisibility(View.GONE);
            recyclerView.setNestedScrollingEnabled(true);
// SwipeRefreshLayout
            mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
            mSwipeRefreshLayout.setOnRefreshListener(this);
            mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                    android.R.color.holo_green_dark,
                    android.R.color.holo_orange_dark,
                    android.R.color.holo_blue_dark);

            ExpListItems = getCndFeedback();

            if (ExpListItems.size() == 0 && CommonClass.isConnected(CandidateFeedbackActivity.this)) {
                if (CommonClass.isConnected(CandidateFeedbackActivity.this)) {
                    mSwipeRefreshLayout.setRefreshing(true);
                    new GetCandidateFeedback(getApplicationContext(), new Callback() {
                        @Override
                        public void execute(Object result, String status) {
                            mSwipeRefreshLayout.setRefreshing(false);
                            if (status.equals("Success")) {
                                ExpListItems = getCndFeedback();
                                if (ExpListItems.size() == 0) {
                                    no_data_foundTextView.setVisibility(View.VISIBLE);
                                    recyclerView.setVisibility(View.GONE);
                                } else {
                                    no_data_foundTextView.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                    setRecyclerView();
                                }

                            } else {
                                ExpListItems = new ArrayList<ModelGroupClass>();
                                recyclerView.setVisibility(View.GONE);
                                no_data_foundTextView.setVisibility(View.VISIBLE);
                            }
                        }
                    }, CandidateId, ExamType).execute();
                } else {
                    Toast.makeText(CandidateFeedbackActivity.this, "No internet connectivity found for get lesson data.", Toast.LENGTH_SHORT);
                }
            } else {
                no_data_foundTextView.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                setRecyclerView();
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("initUI", "err = " + e.toString());
        }
    }

    private List<ModelGroupClass> getCndFeedback() {
        List<ModelGroupClass> list = new ArrayList<ModelGroupClass>();
        list.clear();
        try {
            Cursor cursor = DatabaseHelper.db.query(CandidateFeedbackActivity.this.getResources().getString(R.string.TBL_CndFeedback),
                    null, "CandidateId =?",
                    new String[]{CandidateId}, null, null, "CAST (ExamId AS INTEGER) ASC", null);
            if (cursor.getCount() > 0) {//ExamId,ExamDesc,ExamDetails,QuestionId,Question,Feedback
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    ModelGroupClass groupClass = new ModelGroupClass();
                    groupClass.setStrExam_id(cursor.getString(cursor.getColumnIndex("ExamId")));
                    groupClass.setStrExam_name(cursor.getString(cursor.getColumnIndex("ExamDesc")));
                    groupClass.setStrExam_desc(cursor.getString(cursor.getColumnIndex("ExamDetails")));
                    groupClass.setStrQuestion_id(cursor.getString(cursor.getColumnIndex("QuestionId")));
                    groupClass.setStrQuestion(cursor.getString(cursor.getColumnIndex("Question")));
                    groupClass.setStrFeedback(cursor.getString(cursor.getColumnIndex("Feedback")));
                    list.add(groupClass);
                }
            } else {
                list = new ArrayList<ModelGroupClass>();
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        try {
            if (CommonClass.isConnected(CandidateFeedbackActivity.this)) {
                new GetCandidateFeedback(getApplicationContext(), new Callback() {
                    @Override
                    public void execute(Object result, String status) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        if (status.equals("Success")) {
                            ExpListItems = getCndFeedback();
                            if (ExpListItems.size() == 0) {
                                no_data_foundTextView.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                            } else {
                                no_data_foundTextView.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                setRecyclerView();
                            }
                        } else {
                            ExpListItems = new ArrayList<ModelGroupClass>();
                            recyclerView.setVisibility(View.GONE);
                            no_data_foundTextView.setVisibility(View.VISIBLE);
                        }

                    }
                }, CandidateId, ExamType).execute();
            } else {
                Toast.makeText(CandidateFeedbackActivity.this, "No internet connectivity found for get lesson data.", Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            e.printStackTrace();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setTitle(CandidateName);
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setRecyclerView() {
        try {
            feedBack_adapter = new FeedBack_Adapter(CandidateFeedbackActivity.this, ExpListItems);
            linearLayoutManager = new LinearLayoutManager(CandidateFeedbackActivity.this);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(feedBack_adapter);
            feedBack_adapter.setOnItemClickListener(onClickListener);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("setRecyclerView", "err = " + e.toString());
        }
    }


    static class FeedBack_Adapter extends RecyclerView.Adapter {
        List<ModelGroupClass> searchArrayList;
        Context context;
        private onClickListener onClickListener;

        public FeedBack_Adapter(Context context, List<ModelGroupClass> listItems) {
            this.context = context;
            this.searchArrayList = listItems;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.feedback_list_row, parent, false);
            return new VHItem(v);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            try {
                if (searchArrayList.size() > 0) {
                    if (searchArrayList.get(position) != null) {
                        VHItem ee_data = (VHItem) holder;
                        ee_data.tv_exam_id.setText(searchArrayList.get(position).getStrExam_id());
                        ee_data.tv_exam_name.setText(searchArrayList.get(position).getStrExam_name());
                        ee_data.tv_exam_desc.setText(searchArrayList.get(position).getStrExam_desc());
                        ee_data.tv_question_id.setText(searchArrayList.get(position).getStrQuestion_id());
                        ee_data.tv_question.setText(searchArrayList.get(position).getStrQuestion());
                        ee_data.tv_question_desc.setText(searchArrayList.get(position).getStrFeedback());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return searchArrayList.size();
        }

        public void setOnItemClickListener(onClickListener onItemClickListener) {
            this.onClickListener = onItemClickListener;
        }

        public interface onClickListener {
            void onLinearCardViewClick(int position, View view);
        }

        class VHItem extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView tv_exam_id;
            TextView tv_exam_name;
            TextView tv_exam_desc;
            TextView tv_question_id;
            TextView tv_question;
            TextView tv_question_desc;
            LinearLayout cardView;

            public VHItem(View itemView) {
                super(itemView);
                tv_exam_id = (TextView) itemView.findViewById(R.id.tv_exam_id);
                tv_exam_name = (TextView) itemView.findViewById(R.id.tv_exam_name);
                tv_exam_desc = (TextView) itemView.findViewById(R.id.tv_exam_desc);
                tv_question_id = (TextView) itemView.findViewById(R.id.tv_question_id);
                tv_question = (TextView) itemView.findViewById(R.id.tv_question);
                tv_question_desc = (TextView) itemView.findViewById(R.id.tv_question_desc);
                cardView = (LinearLayout) itemView.findViewById(R.id.cardView);
                tv_exam_desc.setVisibility(View.GONE);
                cardView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {

                switch (v.getId()) {
                    case R.id.cardView: {
                        onClickListener.onLinearCardViewClick(getAdapterPosition(), v);
                    }
                    break;
                }
            }
        }
    }
}
