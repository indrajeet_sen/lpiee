package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.syncExams;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.lpi.kmi.lpi.fragment.NewsDetailsFragment;
import com.lpi.kmi.lpi.fragment.NewsFeedsListFragment;

@SuppressLint("Range")
public class EmotionalExcellenceListActivity extends AppCompatActivity {

    public static String CandidateId = "";
    public static ImageView RefreshGrpTopic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_news_feeds);
        RefreshGrpTopic = (ImageView) findViewById(R.id.RefreshGrpTopic);
        intUI();
        UIListener();
        setToolbar();
    }

    private void UIListener() {
        RefreshGrpTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CommonClass.checkSettings(EmotionalExcellenceListActivity.this, "C");
                if (StaticVariables.SyncMobile.equals("Yes")
                        && CommonClass.networkType(EmotionalExcellenceListActivity.this).equals("MobileData")) {
                    alertCustomDialog("Mobile Data!", "You are connected to mobile data, Do you want continue syncing?");
                } else if (StaticVariables.SyncWIFI.equals("Yes")
                        && CommonClass.networkType(EmotionalExcellenceListActivity.this).equals("WiFi")) {
                    syncServices(EmotionalExcellenceListActivity.this);
                } else if (CommonClass.isConnected(EmotionalExcellenceListActivity.this)) {
                    syncServices(EmotionalExcellenceListActivity.this);
                } else {
                    alertCustomDialog("No Internet!", "You are not connected to internet");
                }
            }
        });
    }

    private void intUI() {

        try {


            CandidateId = getIntent().getStringExtra("CandidateId");

            loadFragment(new NewsFeedsListFragment(), "NewsFeedsListFragment");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void loadFragment(Fragment fragment, String strTag) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment, strTag);
        fragmentTransaction.commit(); // save the changes
    }

    public Fragment getVisibleFragment() {
        FragmentManager fragmentManager = EmotionalExcellenceListActivity.this.getFragmentManager();
        Fragment currentFragment = fragmentManager.findFragmentById(R.id.frameLayout);
        return currentFragment;
    }

    private void setToolbar() {
        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setTitle(getApplicationContext().getResources().getString(R.string.app_name));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        try {
            Fragment visibleFragment = getVisibleFragment();
            if (visibleFragment.getTag().equalsIgnoreCase("Feedback")) {
                loadFragment(new NewsFeedsListFragment(), "NewsFeedsListFragment");
            } else {
                Intent i;
                if (StaticVariables.UserType.equals("T")) {
                    i = new Intent(EmotionalExcellenceListActivity.this, LPI_TrainerMenuActivity.class);
                } else {
                    i = new Intent(EmotionalExcellenceListActivity.this, ParticipantMenuActivity.class);
                }
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                EmotionalExcellenceListActivity.this.finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
            super.onBackPressed();
        }
    }


    private void alertCustomDialog(String title, String message) {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(EmotionalExcellenceListActivity.this).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_exit_dialog, null);
            TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            Button btn_ok = (Button) dialogView.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialogView.findViewById(R.id.btn_no);

            promt_title.setText(title);
            txt_message.setText(message);
            if (title.equalsIgnoreCase("Mobile Data!")) {
                btn_ok.setText("YES");
                btn_no.setText("Not Now");
                btn_no.setPadding(5, 0, 5, 0);
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        syncServices(EmotionalExcellenceListActivity.this);
                    }
                });
            } else if (title.equalsIgnoreCase("Data Cleared!")) {
                btn_ok.setText("YES");
                btn_no.setText("Not Now");
                btn_no.setPadding(5, 0, 5, 0);
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        syncServices(EmotionalExcellenceListActivity.this);
                    }
                });
            }
            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });

            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void syncServices(Context context) {
        new syncExams(new Callback<String>() {
            @Override
            public void execute(String result, String status) {
                Fragment visibleFragment = getVisibleFragment();
                if (visibleFragment.getTag().equalsIgnoreCase("Feedback") && NewsFeedsListFragment.CurrPosition != null &&
                        !NewsFeedsListFragment.CurrPosition.trim().equalsIgnoreCase("")) {
                    loadFragment(new NewsDetailsFragment(NewsFeedsListFragment.CurrPosition), "Feedback");
                } else {
                    loadFragment(new NewsFeedsListFragment(), "NewsFeedsListFragment");
                }
//                intUI();
            }
        }, context, "EmotionalExcellenceListActivity", StaticVariables.UserLoginId,"").execute();

    }
}