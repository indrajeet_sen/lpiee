package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.syncExams;
import com.lpi.kmi.lpi.adapter.Adapters.ExamTypeList_BaseAdapter;
import com.lpi.kmi.lpi.adapter.GetterSetter.GetterSetter;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.math.BigInteger;
import java.util.ArrayList;

@SuppressLint("Range")
public class ExamList_Activity extends AppCompatActivity {

    public static ArrayList<GetterSetter> arrlst_allExams;
    public static ArrayList<String> arrlst_ExamId = new ArrayList<String>();
    public static ArrayList<String> arrlst_ExamCode = new ArrayList<String>();
    public static ArrayList<String> arrlst_ExamDetails = new ArrayList<String>();
    public static ArrayList<String> arrlst_ExamType = new ArrayList<String>();
    public static ArrayList<String> arrlst_ExamTypeName = new ArrayList<String>();
    public static ArrayList<String> arrlst_AllowNavigationFlag = new ArrayList<String>();
    public static ArrayList<String> arrlst_ShowRandomQuestions = new ArrayList<String>();
    public static ArrayList<String> arrlst_OptionCount = new ArrayList<String>();
    public static ArrayList<String> arrlst_TotalQuestions = new ArrayList<String>();
    public static ArrayList<String> arrlst_TotalTimeAllowedInMinutes = new ArrayList<String>();
    public static ArrayList<String> arrlst_IsAllowResultFeedback = new ArrayList<String>();
    public static ArrayList<String> arrlst_IsPracticeExam = new ArrayList<String>();
    public static ArrayList<String> arrlst_IsAllQuesMendatory = new ArrayList<String>();
    public static ArrayList<String> arrlst_AllowedAttempt = new ArrayList<String>();
    public static ArrayList<String> arrlst_EffectiveDate = new ArrayList<String>();

    public static ExamTypeList_BaseAdapter custAdapter;

    ListView list_Exam;
    LinearLayout Lnr_ExmInit, lin_sync_status;
    TextView txt_sync;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_exam_list);

        list_Exam = (ListView) findViewById(R.id.Lst_ExamList);
        Lnr_ExmInit = (LinearLayout) findViewById(R.id.Lnr_ExmInit);
        lin_sync_status = (LinearLayout) findViewById(R.id.lin_sync_status);
        txt_sync = (TextView) findViewById(R.id.txt_sync);
        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_ExmList);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setTitle("Assigned Practices");

        list_Exam.setVisibility(View.VISIBLE);
        Lnr_ExmInit.setVisibility(View.VISIBLE);

        boolean IsExamMasterAvailable = DatabaseHelper.IsExamMasterAvailable(ExamList_Activity.this, DatabaseHelper.db);
        if (IsExamMasterAvailable) {
            list_Exam.setVisibility(View.VISIBLE);
            Lnr_ExmInit.setVisibility(View.GONE);
            lin_sync_status.setVisibility(View.GONE);
        } else {
            Lnr_ExmInit.setVisibility(View.GONE);
            list_Exam.setVisibility(View.GONE);
            lin_sync_status.setVisibility(View.VISIBLE);
        }

        txt_sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CommonClass.isConnected(ExamList_Activity.this)) {

//                    Intent examIntent = new Intent(ExamList_Activity.this, SyncExamMaster.class);
//                    startService(examIntent);
//
//                    Intent newIntent = new Intent(ExamList_Activity.this, SyncPostLicModuleData.class);
//                    startService(newIntent);
//
//                    new GetHtmlContent(ExamList_Activity.this,"ExamList_Activity").execute();
                    new syncExams(ExamList_Activity.this, "ExamList", StaticVariables.UserLoginId).execute();

                    lin_sync_status.setVisibility(View.GONE);
                    onBackPressed();
                } else {
                    Toast.makeText(ExamList_Activity.this, "Please check your internet connection.. and try again.", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ExamList_Activity.this, ParticipantMenuActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        ExamList_Activity.this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            clearList();
            Cursor cquery = DatabaseHelper.GetExamList(ExamList_Activity.this, DatabaseHelper.db);
            String str;
            for (cquery.moveToFirst(); !cquery.isAfterLast(); cquery.moveToNext()) {
                arrlst_ExamId.add("" + cquery.getString(0));
                arrlst_ExamCode.add("" + cquery.getString(1));
                arrlst_ExamDetails.add("" + cquery.getString(2));
                arrlst_ExamType.add("" + cquery.getString(3));
                arrlst_ExamTypeName.add("" + cquery.getString(13));
                arrlst_AllowNavigationFlag.add("" + cquery.getString(4));
                arrlst_OptionCount.add("" + cquery.getString(5));
                arrlst_ShowRandomQuestions.add("" + cquery.getString(6));
//                arrlst_TotalQuestions.add(""+cquery.getString(7));
                arrlst_TotalQuestions.add("" + DatabaseHelper.TotalQueCount(ExamList_Activity.this,
                        DatabaseHelper.db, BigInteger.valueOf(Integer.parseInt(cquery.getString(0)))));
                arrlst_TotalTimeAllowedInMinutes.add("" + cquery.getString(8));
                arrlst_IsAllowResultFeedback.add("" + cquery.getString(9));
                arrlst_IsPracticeExam.add("" + cquery.getString(10));
                arrlst_AllowedAttempt.add("" + cquery.getString(11));
                arrlst_EffectiveDate.add("" + cquery.getString(12));
                arrlst_IsAllQuesMendatory.add("" + cquery.getString(13));

//ExamId,ExamCode,ExamDetails,ExamType,AllowNavigationFlag,OptionCount,ShowRandomQuestions," +
//                "TotalQuestions,TotalTimeAllowedInMinutes,IsAllowResultFeedback,IsPracticeExam,AllowedAttempt,EffectiveDate,ExamDesc

                str = "ExamType=" + cquery.getString(3);
                Log.e("", "ExamType=" + str);
                str = "AllowNavigationFlag=" + cquery.getString(4);
                str = "OptionCount=" + cquery.getString(5);
                str = "IsAllowResultFeedback=" + cquery.getString(9);
                str = "IsPracticeExam=" + cquery.getString(10);
                str = "AllowedAttempt=" + cquery.getString(11);

            }
            cquery.close();
            custList();
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
            DatabaseHelper.SaveErroLog(ExamList_Activity.this, DatabaseHelper.db,
                    "ExamList_Activity", "onResume", e.toString());
        }

    }

    void clearList() {
        arrlst_ExamId.clear();
        arrlst_ExamCode.clear();
        arrlst_ExamDetails.clear();
        arrlst_ExamType.clear();
        arrlst_ExamTypeName.clear();
        arrlst_AllowNavigationFlag.clear();
        arrlst_OptionCount.clear();
        arrlst_TotalQuestions.clear();
        arrlst_TotalTimeAllowedInMinutes.clear();
        arrlst_IsAllowResultFeedback.clear();
        arrlst_IsPracticeExam.clear();
        arrlst_AllowedAttempt.clear();
        arrlst_EffectiveDate.clear();
        arrlst_IsAllQuesMendatory.clear();

    }

    public void custList() {
        try {
            arrlst_allExams = getLstData();
            custAdapter = new ExamTypeList_BaseAdapter(this, arrlst_allExams, lin_sync_status);
            list_Exam.setAdapter(custAdapter);
            custAdapter.notifyDataSetChanged();
//            int index = list_Exam.getFirstVisiblePosition();
//            View v = list_Exam.getChildAt(0);
//            int top = (v == null) ? 0 : v.getTop();
//            list_Exam.setSelectionFromTop(index, top);
        } catch (Exception e) {
            Log.e("Tag", "cust error=" + e.toString());
            DatabaseHelper.SaveErroLog(ExamList_Activity.this, DatabaseHelper.db,
                    "ExamList_Activity", "custList", e.toString());
        }

    }

    private ArrayList<GetterSetter> getLstData() {
        ArrayList<GetterSetter> arrlst = new ArrayList<GetterSetter>();
        for (int i = 0; i < arrlst_ExamCode.size(); i++) {
            try {
                GetterSetter candi = new GetterSetter("" + (i + 1), arrlst_ExamId.get(i), arrlst_ExamCode.get(i),
                        arrlst_ExamDetails.get(i), arrlst_ExamType.get(i), arrlst_ExamTypeName.get(i),
                        arrlst_AllowNavigationFlag.get(i), arrlst_OptionCount.get(i),
                        arrlst_ShowRandomQuestions.get(i), arrlst_TotalQuestions.get(i),
                        arrlst_TotalTimeAllowedInMinutes.get(i), arrlst_IsAllowResultFeedback.get(i),
                        arrlst_IsPracticeExam.get(i), arrlst_AllowedAttempt.get(i),
                        arrlst_EffectiveDate.get(i), arrlst_IsAllQuesMendatory.get(i));

                arrlst.add(candi);
            } catch (Exception e) {
                e.printStackTrace();
                DatabaseHelper.SaveErroLog(ExamList_Activity.this, DatabaseHelper.db,
                        "ExamList_Activity", "getLstData", e.toString());
            }
        }
        return arrlst;
    }
}
