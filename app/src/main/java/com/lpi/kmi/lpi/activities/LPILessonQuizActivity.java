package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.GetCandidateAttemptedExamsDetails;
import com.lpi.kmi.lpi.activities.AsyncTasks.syncExams;
import com.lpi.kmi.lpi.adapter.Adapters.LPI_QuizListAdapter;
import com.lpi.kmi.lpi.adapter.GetterSetter.QuizPojo;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

@SuppressLint("Range")
public class LPILessonQuizActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    TextView no_data_foundTextView;
    List<QuizPojo> ExpListItems = new ArrayList<QuizPojo>();
    LinearLayoutManager linearLayoutManager;
    LPI_QuizListAdapter quizListAdapter;
    LPI_QuizListAdapter.onClickListener onClickListener;
    static private String strSectionId = "";
    static private String Lesson = "", LessonDesc = "", Trip = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_list_emotional_excellence);

        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.quizToolbar);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toolbar.setElevation(5);
            }
            setSupportActionBar(toolbar);
            actionBarSetup();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            strSectionId = getIntent().getExtras().getString("SectionId");
            Lesson = getIntent().getExtras().getString("Lesson");
            LessonDesc = getIntent().getExtras().getString("LessonDesc");
            Trip = getIntent().getExtras().getString("Trip");
        } catch (Exception e) {
            e.printStackTrace();
        }
//        if (CommonClass.isConnected(LPILessonQuizActivity.this)) {
//            new syncExams(new Callback<String>() {
//                @Override
//                public void execute(String result, String status) {
//                    new GetAtemptedExams(LPILessonQuizActivity.this, StaticVariables.UserLoginId
//                            , new Callback() {
//                        @Override
//                        public void execute(Object result, String status) {
//                            initUI();
//                        }
//                    }).execute();
//                }
//            }, LPILessonQuizActivity.this,
//                    "LPILessonQuizActivity", StaticVariables.UserLoginId, "LPILessons").execute();
//        } else {
//            initUI();
//        }

        initUI();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        try {
            getSupportActionBar().setTitle("QUIZ LIST");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LPILessonQuizActivity.this, LPI_LessonDetailActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("initFrom", "LPILessonQuizActivity");
        intent.putExtra("SectionId", strSectionId);
        intent.putExtra("Lesson", Lesson);
        intent.putExtra("LessonDesc", LessonDesc);
        intent.putExtra("Trip", Trip);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        LPILessonQuizActivity.this.finish();
    }

    private void initUI() {
        try {

            new GetCandidateAttemptedExamsDetails(LPILessonQuizActivity.this, "" + StaticVariables.crnt_ExmId, StaticVariables.UserLoginId,
                    "", new Callback() {
                @Override
                public void execute(Object result, String status) {
                    if (result != null && result.equals("Success")) {
                    } else {
                    }
                }
            }).execute();

            recyclerView = (RecyclerView) findViewById(R.id.recy_list);
            no_data_foundTextView = (TextView) findViewById(R.id.no_data_foundTextView);
            no_data_foundTextView.setVisibility(View.GONE);
            recyclerView.setNestedScrollingEnabled(true);
//            ExpListItems = SetStandardGroups();
            ExpListItems = SetDummyData();
            quizListAdapter = new LPI_QuizListAdapter(LPILessonQuizActivity.this, ExpListItems);
            linearLayoutManager = new LinearLayoutManager(LPILessonQuizActivity.this);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(quizListAdapter);

            try {
                onClickListener = new LPI_QuizListAdapter.onClickListener() {

                    @Override
                    public void onLinearCardViewClick(int position, View view) {
                        if (DatabaseHelper.GetAttemptedExam(LPILessonQuizActivity.this, DatabaseHelper.db,
                                "" + ExpListItems.get(position).getTv_quiz_id(), StaticVariables.UserLoginId) > 1) {
                            Toast.makeText(getApplicationContext(), "If you wish to repeat " + ExpListItems.get(position).getTv_quiz_desc() + ",\n" +
                                    "please contact your administrator", Toast.LENGTH_LONG).show();
                        } else {
                            StaticVariables.crnt_QueId = 1;
                            StaticVariables.TblNameForCount = DatabaseHelper.TotalQueCount(LPILessonQuizActivity.this,
                                    DatabaseHelper.db, BigInteger.valueOf(Long.parseLong(ExpListItems.get(position).getTv_quiz_id())));
                            //                StaticVariables.crnt_ExmId = ExpListItems.get(position).getEe_crnt_ExmId();
                            StaticVariables.crnt_ExmId = BigInteger.valueOf(Long.parseLong(ExpListItems.get(position).getTv_quiz_id()));
                            StaticVariables.crnt_ExmTypeName = Lesson + " " + Trip;
                            StaticVariables.crnt_ExmName = ExpListItems.get(position).getTv_quiz_name();
                            StaticVariables.crnt_ExamType = 6;
                            StaticVariables.crnt_ExamDuration = "30";
                            StaticVariables.IsPracticeExam = "N";
                            StaticVariables.IsAllQuesMendatory = "Y";
                            StaticVariables.allowedAttempts = "1";

                            Intent i = new Intent(LPILessonQuizActivity.this, MCQ_QuestionActivity.class);
                            i.putExtra("initFrom", "LPILessonQuizActivity");
                            try {
                                i.putExtra("SectionId", strSectionId);
                                i.putExtra("Lesson", Lesson);
                                i.putExtra("LessonDesc", LessonDesc);
                                i.putExtra("Trip", Trip);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        }
                    }

                    @Override
                    public void onSummeryClick(int position, View view) {

                    }
                };

                quizListAdapter.setOnItemClickListener(onClickListener);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<QuizPojo> SetDummyData() {
        ExpListItems.clear();
        List<QuizPojo> groups = new ArrayList<QuizPojo>();
        ArrayList<String> arrResult = new ArrayList<String>();
        ArrayList<String> arrAnsStatus = new ArrayList<String>();
        ArrayList<String> arrSelectedAns = new ArrayList<String>();
        ArrayList<String> arrRightAns = new ArrayList<String>();
        ArrayList<String> arrQuestion = new ArrayList<String>();
        String curExamId = "";
        Cursor cursor = DatabaseHelper.db.query(getResources().getString(R.string.Tbl_SectionExamMapping),
                null, "SectionId=?", new String[]{strSectionId}, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                String strExmId = cursor.getString(cursor.getColumnIndex("ExamId"));
                Cursor cursor1 = DatabaseHelper.db.query(getResources().getString(R.string.TBL_LPIExamMST),
                        null, "ExamId=?",
                        new String[]{strExmId}, null, null, null);
                if (cursor1 != null && cursor1.getCount() > 0) {

                    int i = 1;
                    for (cursor1.moveToFirst(); !cursor1.isAfterLast(); cursor1.moveToNext()) {
                        curExamId = cursor1.getString(cursor1.getColumnIndex("ExamId"));
                        QuizPojo group = new QuizPojo();
                        arrAnsStatus.clear();
                        arrQuestion.clear();
                        arrResult.clear();
                        arrRightAns.clear();
                        arrSelectedAns.clear();

                        group.setTv_quiz_name("Quiz " + i);
                        group.setTv_quiz_desc(cursor1.getString(cursor1.getColumnIndex("ExamDesc")));
                        group.setTv_quiz_id(curExamId);

                        int attemptId = DatabaseHelper.CheckAttempts(LPILessonQuizActivity.this,
                                DatabaseHelper.db, "" + curExamId, StaticVariables.UserLoginId);
                        int AllowedAttempt = DatabaseHelper.AllowedAttempt(LPILessonQuizActivity.this,
                                DatabaseHelper.db, "" + curExamId, StaticVariables.UserLoginId);

                        if (AllowedAttempt - attemptId > 0) {
                            group.setTv_quiz_completed("N");
                            group.setTv_quiz_nextid("");
                            group.setTv_quiz_result("");
                            group.setTv_quiz_tquiz("");
                            group.setTv_quiz_attempted("");
                            group.setTv_quiz_correct("");
                            group.setTv_quiz_accuracy("");
                            groups.add(group);
                        } else {
                            group.setTv_quiz_completed("Y");
                            try {
//                                arrQuestion.add(cursor1.getString(cursor1.getColumnIndex("TotalQuestions")));
                                Cursor mCursor = DatabaseHelper.GetAllAttemptedAnsDetails(LPILessonQuizActivity.this, DatabaseHelper.db,
                                        Integer.parseInt(curExamId), StaticVariables.UserLoginId);
                                if (mCursor.getCount() > 0) {
                                    for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                                        String queID = mCursor.getString(mCursor.getColumnIndex("QuestionId"));
                                        String attemptID = mCursor.getString(mCursor.getColumnIndex("AttemptId"));

                                        String strAttemptedAns = DatabaseHelper.GetAttemptedAns(LPILessonQuizActivity.this, DatabaseHelper.db,
                                                "" + curExamId, "" + queID, StaticVariables.UserLoginId, Integer.parseInt(attemptID));
                                        String strRightAns = DatabaseHelper.GetRightAns_Exm1(LPILessonQuizActivity.this, DatabaseHelper.db,
                                                "" + curExamId, "" + queID);

                                        arrSelectedAns.add(strAttemptedAns);
                                        arrRightAns.add(strRightAns);
                                        if (strRightAns.trim().equalsIgnoreCase(strAttemptedAns)) {
                                            arrAnsStatus.add("Correct");
                                        } else {
                                            arrAnsStatus.add("Wrong");
                                        }
                                        Cursor cquery_que = DatabaseHelper.GetQue(LPILessonQuizActivity.this, DatabaseHelper.db, BigInteger.valueOf(Integer.parseInt(curExamId)),
                                                Integer.parseInt(queID));
                                        if (cquery_que.getCount() > 0) {
                                            for (cquery_que.moveToFirst(); !cquery_que.isAfterLast(); cquery_que.moveToNext()) {
                                                arrQuestion.add(cquery_que.getString(3));
                                            }
                                        }
                                        cquery_que.close();
                                    }
                                }
                                if (arrAnsStatus.size() > 0) {
                                    int iCorrect = CommonClass.GetCountArrayResultSet(arrAnsStatus, "Correct");
                                    int iWrong = CommonClass.GetCountArrayResultSet(arrAnsStatus, "Wrong");
                                    if (arrAnsStatus.size() == iCorrect + iWrong) {
                                        float fResult = (float) (0.5 * arrAnsStatus.size());
                                        if (iCorrect > fResult) {
                                            arrResult.add("Good");
                                        } else {
                                            arrResult.add("Poor");
                                        }
                                    }
                                    group.setTv_quiz_nextid("");
                                    group.setTv_quiz_result(arrResult.get(0));
                                    group.setTv_quiz_tquiz("" + arrQuestion.size());
                                    group.setTv_quiz_attempted("" + (iCorrect + iWrong));
                                    group.setTv_quiz_correct("" + iCorrect);
                                    float result = ((float) iCorrect / (float) arrQuestion.size()) * 100.0f;

                                    try {
                                        DecimalFormat df2 = new DecimalFormat("#.##");
                                        group.setTv_quiz_accuracy("" + df2.format(result));
                                        ;
                                    } catch (Exception e) {
                                        group.setTv_quiz_accuracy("" + result);
                                    }
                                }
                                mCursor.close();
                            } catch (Exception e) {
                                Log.e("", e.getMessage());
                            }

                            groups.add(group);
                        }
                        i = +1;
                    }
                } else {
                    custom_SubmitTestAlert("Alert!", "Quiz set not available, " +
                            "please make sure that you are connected to the internet to download quiz set");
                }
                cursor1.close();
            }
        } else {
            custom_SubmitTestAlert("Alert!", "Quiz set not available, " +
                    "please make sure that you are connected to the internet to download quiz set");
        }

        cursor.close();

        return groups;
    }

    private void custom_SubmitTestAlert(final String Title, String Msg) {

        final AlertDialog alertD;
        LayoutInflater logout_inflator = LayoutInflater.from(LPILessonQuizActivity.this);
        final View openDialog = logout_inflator.inflate(R.layout.custom_exit_dialog, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LPILessonQuizActivity.this);
        alertDialogBuilder.setView(openDialog);
        alertD = alertDialogBuilder.create();
        alertD.setCancelable(false);
        alertD.show();

        final TextView alrtText_ResultRemark = (TextView) openDialog.findViewById(R.id.txt_message);
        final TextView Txt_AlertTitle = (TextView) openDialog.findViewById(R.id.promt_title);
        final Button alrtBtn_cancle = (Button) openDialog.findViewById(R.id.btn_no);
        final Button alrtBtn_done = (Button) openDialog.findViewById(R.id.btn_yes);
        alrtBtn_cancle.setVisibility(View.GONE);
        if (Title.equals("No internet")) {
            alrtBtn_done.setText("OK");
        } else if (Title.equals("Alert!")) {
            alrtBtn_done.setText("SYNC");
            alrtBtn_cancle.setVisibility(View.VISIBLE);
        }


        Txt_AlertTitle.setText(Title);
        alrtText_ResultRemark.setText(Msg);

        alrtBtn_done.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    alertD.dismiss();

                    if (Title.equals("Alert!")) {
                        if (CommonClass.isConnected(LPILessonQuizActivity.this)) {
                            new syncExams(new Callback<String>() {
                                @Override
                                public void execute(String result, String status) {
                                    initUI();
                                }
                            }, LPILessonQuizActivity.this,
                                    "LPILessonQuizActivity", StaticVariables.UserLoginId, "LPILessons").execute();
                        } else {
                            custom_SubmitTestAlert("No internet",
                                    "Please make sure that you are connected to the internet");
                        }
                    }
                } catch (Exception e) {
                    Log.e("", "custom_SubmitTestAlert Error=" + e.toString());
                    DatabaseHelper.SaveErroLog(LPILessonQuizActivity.this, DatabaseHelper.db,
                            "LPILessonQuizActivity", "custom_SubmitTestAlert", e.toString());
                }
            }
        });

        alrtBtn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Title.equals("Alert!")) {
                    alertD.dismiss();
                } else {
                    alertD.dismiss();
                }
            }
        });
    }


    public List<QuizPojo> SetStandardGroups() {
        Cursor mCursor = null;
        ArrayList<String> arrHeader = new ArrayList<String>();
        ArrayList<String> arrDesc = new ArrayList<String>();
        ArrayList<String> arrAnsStatus = new ArrayList<String>();
        ArrayList<String> arrResult = new ArrayList<String>();
        ArrayList<String> arrExamId = new ArrayList<String>();
        ArrayList<String> arrSelectedAns = new ArrayList<String>();
        ArrayList<String> arrRightAns = new ArrayList<String>();
        ArrayList<String> arrQuestion = new ArrayList<String>();
        arrHeader.clear();
        try {
            mCursor = DatabaseHelper.GetExamCursor(LPILessonQuizActivity.this, DatabaseHelper.db, 6);
            if (mCursor.getCount() > 0) {
                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    String ExamDesc = mCursor.getString(mCursor.getColumnIndex("ExamDesc"));
                    String ExamDetails = mCursor.getString(mCursor.getColumnIndex("ExamDetails"));
                    String ExamId = mCursor.getString(mCursor.getColumnIndex("ExamId"));
                    if (ExamDesc != null && !ExamDesc.trim().equalsIgnoreCase("")) {
                        arrHeader.add(ExamDesc);
                        arrDesc.add(ExamDetails);
                        arrExamId.add(ExamId);
                    }
                }
            }
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }
        mCursor.close();
//
        for (int i = 0; i < arrExamId.size(); i++) {
            try {
                mCursor = DatabaseHelper.GetAllAttemptedAnsDetails(LPILessonQuizActivity.this, DatabaseHelper.db,
                        Integer.parseInt(arrExamId.get(i)), StaticVariables.UserLoginId);
                if (mCursor.getCount() > 0) {
                    for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                        String queID = mCursor.getString(mCursor.getColumnIndex("QuestionId"));
                        String attemptID = mCursor.getString(mCursor.getColumnIndex("AttemptId"));

                        String strAttemptedAns = DatabaseHelper.GetAttemptedAns(LPILessonQuizActivity.this, DatabaseHelper.db,
                                "" + arrExamId.get(i), "" + queID, StaticVariables.UserLoginId, Integer.parseInt(attemptID));
                        String strRightAns = DatabaseHelper.GetRightAns_Exm1(LPILessonQuizActivity.this, DatabaseHelper.db,
                                "" + arrExamId.get(i), "" + queID);

                        arrSelectedAns.add(strAttemptedAns);
                        arrRightAns.add(strRightAns);
                        if (strRightAns.trim().equalsIgnoreCase(strAttemptedAns)) {
                            arrAnsStatus.add("Correct");
                        } else {
                            arrAnsStatus.add("Wrong");
                        }
                        Cursor cquery_que = DatabaseHelper.GetQue(LPILessonQuizActivity.this, DatabaseHelper.db, BigInteger.valueOf(Integer.parseInt(arrExamId.get(i))),
                                Integer.parseInt(queID));
                        if (cquery_que.getCount() > 0) {
                            for (cquery_que.moveToFirst(); !cquery_que.isAfterLast(); cquery_que.moveToNext()) {
                                arrQuestion.add(cquery_que.getString(3));
                            }
                        }
                    }
                }
                if (arrAnsStatus.size() > 0) {
                    int iCorrect = CommonClass.GetCountArrayResultSet(arrAnsStatus, "Correct");
                    int iWrong = CommonClass.GetCountArrayResultSet(arrAnsStatus, "Wrong");
                    if (arrAnsStatus.size() == iCorrect + iWrong) {
                        float fResult = (float) (0.5 * arrAnsStatus.size());
                        if (iCorrect > fResult) {
                            arrResult.add("Good");
                        } else {
                            arrResult.add("Poor");
                        }
                    }
                }
            } catch (Exception e) {
                Log.e("", e.getMessage());
            }
        }


//
        List<QuizPojo> groups = new ArrayList<QuizPojo>();
        try {
            for (int i = 0; i < arrHeader.size(); i++) {
                QuizPojo group = new QuizPojo();
                group.setTv_quiz_name(arrHeader.get(i));
                group.setTv_quiz_desc(arrDesc.get(i));
                group.setTv_quiz_id(arrExamId.get(i));
                if (arrResult.size() > 0 && arrResult.get(i).trim().length() > 0) {
                    group.setTv_quiz_result(arrResult.get(i));
                }
                groups.add(group);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return groups;
    }
}
