package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Training.Adapters.TViewGroup;
import com.lpi.kmi.lpi.adapter.Adapters.EE_Adapter;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.math.BigInteger;
import java.util.ArrayList;

@SuppressLint("Range")
public class LPIPracticeLessonActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    TextView no_data_foundTextView;
    private ArrayList<TViewGroup> ExpListItems;
    LinearLayoutManager linearLayoutManager;
    EE_Adapter ee_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_list_emotional_excellence);

        initUI();
        UIListeners();
    }

    @Override
    public void onBackPressed() {

        Intent intent;
        if (StaticVariables.UserType.equals("C"))
            intent = new Intent(LPIPracticeLessonActivity.this, ParticipantMenuActivity.class);
        else
            intent = new Intent(LPIPracticeLessonActivity.this, LPI_TrainerMenuActivity.class);

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        LPIPracticeLessonActivity.this.finish();

    }

    private void UIListeners() {
        ee_adapter.setOnItemClickListener(onClickListener);
    }

    EE_Adapter.onClickListener onClickListener = new EE_Adapter.onClickListener() {

        @Override
        public void onLinearCardViewClick(int position, View view) {
            if (DatabaseHelper.GetAttemptedExam(LPIPracticeLessonActivity.this, DatabaseHelper.db,
                    "6001", StaticVariables.UserLoginId) > 1) {
                Toast.makeText(getApplicationContext(), "If you wish to repeat " + ExpListItems.get(position).getEe_Name() + ",\n" +
                        "please contact your administrator", Toast.LENGTH_LONG).show();
            } else {
                StaticVariables.crnt_QueId = 1;
                StaticVariables.TblNameForCount = 5;
//                StaticVariables.crnt_ExmId = ExpListItems.get(position).getEe_crnt_ExmId();
                StaticVariables.crnt_ExmId = BigInteger.valueOf(6001);
                StaticVariables.crnt_ExmTypeName = "Emotional Excellence";
                StaticVariables.crnt_ExmName = "Emotional Excellence";
                StaticVariables.crnt_ExamType = 6;
                StaticVariables.crnt_ExamDuration = "15";
                StaticVariables.IsPracticeExam = "N";
                StaticVariables.IsAllQuesMendatory = "Y";
                StaticVariables.allowedAttempts = "1";

                Intent i = new Intent(LPIPracticeLessonActivity.this, MCQ_QuestionActivity.class);
                i.putExtra("initFrom", "Custom_GridAdapter");
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        }

        @Override
        public void onSummeryClick(int position, View view) {

        }
    };

    private void initUI() {
        try {
            recyclerView = (RecyclerView) findViewById(R.id.recy_list);
            no_data_foundTextView = (TextView) findViewById(R.id.no_data_foundTextView);
            no_data_foundTextView.setVisibility(View.GONE);
            recyclerView.setNestedScrollingEnabled(true);
            ExpListItems = SetStandardGroups();
            ee_adapter = new EE_Adapter(LPIPracticeLessonActivity.this, ExpListItems);
            linearLayoutManager = new LinearLayoutManager(LPIPracticeLessonActivity.this);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(ee_adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ArrayList<TViewGroup> SetStandardGroups() {
        Cursor mCursor = null;
        ArrayList<TViewGroup> childGroups = new ArrayList<TViewGroup>();
        ArrayList<String> arrHeader = new ArrayList<String>();
        ArrayList<String> arrDesc = new ArrayList<String>();
        ArrayList<String> arrAnsStatus = new ArrayList<String>();
        ArrayList<String> arrResult = new ArrayList<String>();
        ArrayList<String> arrExamId = new ArrayList<String>();
        ArrayList<String> arrSelectedAns = new ArrayList<String>();
        ArrayList<String> arrRightAns = new ArrayList<String>();
        ArrayList<String> arrQuestion = new ArrayList<String>();
        arrHeader.clear();
        try {
            mCursor = DatabaseHelper.GetExamCursor(LPIPracticeLessonActivity.this, DatabaseHelper.db, 6);
            if (mCursor.getCount() > 0) {
                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    String ExamDesc = mCursor.getString(mCursor.getColumnIndex("ExamDesc"));
                    String ExamDetails = mCursor.getString(mCursor.getColumnIndex("ExamDetails"));
                    String ExamId = mCursor.getString(mCursor.getColumnIndex("ExamId"));
                    if (ExamDesc != null && !ExamDesc.trim().equalsIgnoreCase("")) {
                        arrHeader.add(ExamDesc);
                        arrDesc.add(ExamDetails);
                        arrExamId.add(ExamId);
                    }
                }
            }
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }
        mCursor.close();
//
        for (int i = 0; i < arrExamId.size(); i++) {
            try {
                mCursor = DatabaseHelper.GetAllAttemptedAnsDetails(LPIPracticeLessonActivity.this, DatabaseHelper.db,
                        Integer.parseInt(arrExamId.get(i)), StaticVariables.UserLoginId);
                if (mCursor.getCount() > 0) {
                    for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                        String queID = mCursor.getString(mCursor.getColumnIndex("QuestionId"));
                        String attemptID = mCursor.getString(mCursor.getColumnIndex("AttemptId"));

                        String strAttemptedAns = DatabaseHelper.GetAttemptedAns(LPIPracticeLessonActivity.this, DatabaseHelper.db,
                                ""+arrExamId.get(i), ""+queID, StaticVariables.UserLoginId, Integer.parseInt(attemptID));
                        String strRightAns = DatabaseHelper.GetRightAns_Exm1(LPIPracticeLessonActivity.this, DatabaseHelper.db,
                                ""+arrExamId.get(i), ""+queID);

                        arrSelectedAns.add(strAttemptedAns);
                        arrRightAns.add(strRightAns);
                        if (strRightAns.trim().equalsIgnoreCase(strAttemptedAns)) {
                            arrAnsStatus.add("Correct");
                        } else {
                            arrAnsStatus.add("Wrong");
                        }
                        Cursor cquery_que = DatabaseHelper.GetQue(LPIPracticeLessonActivity.this, DatabaseHelper.db, BigInteger.valueOf(Integer.parseInt(arrExamId.get(i))),
                                Integer.parseInt(queID));
                        if (cquery_que.getCount() > 0) {
                            for (cquery_que.moveToFirst(); !cquery_que.isAfterLast(); cquery_que.moveToNext()) {
                                arrQuestion.add(cquery_que.getString(3));
                            }
                        }
                    }
                }
                if (arrAnsStatus.size() > 0) {
                    int iCorrect = CommonClass.GetCountArrayResultSet(arrAnsStatus, "Correct");
                    int iWrong = CommonClass.GetCountArrayResultSet(arrAnsStatus, "Wrong");
                    if (arrAnsStatus.size() == iCorrect + iWrong) {
                        float fResult = (float) (0.5 * arrAnsStatus.size());
                        if (iCorrect > fResult) {
                            arrResult.add("Good");
                        } else {
                            arrResult.add("Poor");
                        }
                    }
                }
            } catch (Exception e) {
                Log.e("", e.getMessage());
            }
        }


//
        ArrayList<TViewGroup> groups = new ArrayList<TViewGroup>();
        try {
            for (int i = 0; i < arrHeader.size(); i++) {
                TViewGroup group = new TViewGroup();
                group.setEe_Name(arrHeader.get(i));
                group.setEe_Details(arrDesc.get(i));
                group.setEe_examID(arrExamId.get(i));
                if (arrResult.size() > 0 && arrResult.get(i).trim().length() > 0) {
                    group.setEe_Result(arrResult.get(i));
                    group.setEe_Status("Y");
                }
                groups.add(group);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return groups;
    }
}
