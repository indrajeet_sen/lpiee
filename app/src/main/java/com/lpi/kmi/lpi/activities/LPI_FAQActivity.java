
package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.AsyncGetFAQ;
import com.lpi.kmi.lpi.activities.PublicUser.SearchModel;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.ImageNicer;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.lpi.kmi.lpi.fragment.CompleteFAQFragment;
import com.lpi.kmi.lpi.fragment.IncompleteFAQFragment;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@SuppressLint("Range")
public class LPI_FAQActivity extends AppCompatActivity {

    //    static boolean isOnline;
    //    FloatingActionButton addFQATopic;
    static public String strInitFrom = "", strCandidateId = "", strTrainerId = "", strQuestion = "", strQuesID = "", strUserName = "", strIsClosed = "";//,strUserType="";
    ArrayList<SearchModel> mArraylistEventslistComplete;
    ArrayList<SearchModel> mArraylistEventslistIncomplete;
    ImageView fab;
    int currentVisibleTab = 0;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_lpi_faq);
        initUI();
    }

    /*@Override
    protected void onResume() {
        super.onResume();
        getFAQList();
    }*/

    private void initUI() {
        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_menu);
            setSupportActionBar(toolbar);
            actionBarSetup();
            viewPager = (ViewPager) findViewById(R.id.pager_faq);
            mArraylistEventslistComplete = new ArrayList<SearchModel>();
            mArraylistEventslistIncomplete = new ArrayList<SearchModel>();
            tabLayout = (TabLayout) findViewById(R.id.tabs);
            fab = (ImageView) findViewById(R.id.RefreshFaqTopic);
            tabLayout.setupWithViewPager(viewPager);
            tabLayout.setVisibility(View.VISIBLE);

            try {
                strCandidateId = getIntent().getStringExtra("CandidateId");
                strTrainerId = getIntent().getStringExtra("TrainerId");
                strInitFrom = getIntent().getStringExtra("InitFrom");
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (getFAQList()) {
                final AsyncGetFAQ getFAQ = new AsyncGetFAQ();
                if (getFAQ.getStatus() != AsyncTask.Status.RUNNING) {
                    if (CommonClass.isConnected(LPI_FAQActivity.this)) {
                        new AsyncGetFAQ(new Callback<String>() {
                            @Override
                            public void execute(String result, String status) {
                                if (status.equals("Success")) {
                                    getFAQList();
                                } else {
                                    setupViewPager(viewPager);
                                }
                            }
                        }, LPI_FAQActivity.this, "", strCandidateId, strTrainerId, StaticVariables.UserType).execute();
                    } else {
                        Toast.makeText(LPI_FAQActivity.this, "Please check you have active internet connection", Toast.LENGTH_SHORT).show();
                        alertCustomDialog("Alert", "Please check you have active internet connection");
                    }
                } else {
                    Toast.makeText(LPI_FAQActivity.this, "Please wait for previous request to complete", Toast.LENGTH_SHORT).show();
                }
            } else {
                setupViewPager(viewPager);
            }

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentVisibleTab = viewPager.getCurrentItem();
                    final AsyncGetFAQ getFAQ = new AsyncGetFAQ();
                    if (getFAQ.getStatus() != AsyncTask.Status.RUNNING) {
                        if (CommonClass.isConnected(LPI_FAQActivity.this)) {
                            new AsyncGetFAQ(new Callback<String>() {
                                @Override
                                public void execute(String result, String status) {
                                    if (status.equals("Success")) {
                                        getFAQList();
                                    } else {
                                        setupViewPager(viewPager);
                                    }
                                }
                            }, LPI_FAQActivity.this, "", strCandidateId, strTrainerId, StaticVariables.UserType).execute();
                        } else {
                            alertCustomDialog("Alert", "Please check you have active internet connection");
                            Toast.makeText(LPI_FAQActivity.this, "Please check you have active internet connection", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(LPI_FAQActivity.this, "Please wait for previous request to complete", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            try {
                DatabaseHelper.InsertActivityTracker(getApplicationContext(), DatabaseHelper.db,
                        StaticVariables.UserLoginId,
                        "A80018", "A80018", "FAQ page called",
                        "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                        StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                        "", "", "Pending");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean getFAQList() {

        try {
            Cursor mCursor = DatabaseHelper.getFAQListCursor(LPI_FAQActivity.this, DatabaseHelper.db,
                    strCandidateId, strTrainerId, StaticVariables.UserType);

            if (CommonClass.isValidCursor(mCursor)) {
                mArraylistEventslistComplete.clear();
                mArraylistEventslistIncomplete.clear();
                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    SearchModel searchModel = new SearchModel();
                    searchModel.setName(mCursor.getString(mCursor.getColumnIndex("Name")));
                    byte[] strIMG = mCursor.getBlob(mCursor.getColumnIndex("Images"));
                    if (strIMG == null || strIMG.equals(null) || strIMG.equals("null")) {
                        try {
                            Bitmap bitmap = ImageNicer.DecodeBitmapFromResource(LPI_FAQActivity.this.getResources(), (R.drawable.profile_blank_m), 100, 100);
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                            searchModel.setImage(stream.toByteArray());
                            stream.flush();
                            stream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        searchModel.setImage(mCursor.getBlob(mCursor.getColumnIndex("Images")));
                    }
                    searchModel.setMobile_no(mCursor.getString(mCursor.getColumnIndex("Mobile")));
                    searchModel.setStatus(mCursor.getString(mCursor.getColumnIndex("QuesDesc")));
                    searchModel.setUser_id(mCursor.getString(mCursor.getColumnIndex("CandidateId")));
                    searchModel.setTrainer_id(mCursor.getString(mCursor.getColumnIndex("TrainerId")));
                    searchModel.setQuesId(mCursor.getString(mCursor.getColumnIndex("QuesId")));
                    Cursor cCursor = DatabaseHelper.db.query(getResources().getString(R.string.TBL_FAQ_Conversation), null,
                            "MsgFrom <>? and isShow = ? and QuesId=?",
                            new String[]{StaticVariables.UserLoginId, "Y", mCursor.getString(mCursor.getColumnIndex("QuesId"))}, null, null, null, null);
                    if (cCursor.getCount() > 0)
                        searchModel.setRespCount("" + cCursor.getCount());
                    else
                        searchModel.setRespCount("");
                    if (mCursor.getString(mCursor.getColumnIndex("isClosed")).equalsIgnoreCase("Y")) {
                        mArraylistEventslistComplete.add(searchModel);
                    } else {
                        mArraylistEventslistIncomplete.add(searchModel);
                    }
                }
                setupViewPager(viewPager);
                return false;
            } else {
                //            setupViewPager(viewPager);
                return true;
            }
        } catch (Exception e) {
            Log.e("LPI_FAQActivity", "error" + e.toString());
            DatabaseHelper.SaveErroLog(LPI_FAQActivity.this, DatabaseHelper.db,
                    "LPI_FAQActivity", "getFAQList", e.toString());
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().setTitle("FAQ");
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void showError() {

        /*AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(LPI_FAQActivity.this, R.style.myCoolDialog);
        } else {
            builder = new AlertDialog.Builder(LPI_FAQActivity.this);
        }
        builder.setTitle("Alert!");
        builder.setMessage("No internet connection found please connect internet and try again.");*/
        AlertDialog.Builder builder = CommonClass.DialogOpen(LPI_FAQActivity.this, "Alert!", "No internet connection found, please connect internet and try again.");
        builder.setCancelable(true);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onBackPressed();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        //CompleteConersation();
        Intent intent;

        if (strInitFrom == null) {
            if (StaticVariables.UserType.equals("C"))
                intent = new Intent(LPI_FAQActivity.this, ParticipantMenuActivity.class);
            else
                intent = new Intent(LPI_FAQActivity.this, LPI_FAQGroupedActivity.class);
        } else if (strInitFrom.equals("FAQGRP"))
            intent = new Intent(LPI_FAQActivity.this, LPI_FAQGroupedActivity.class);
        else if (StaticVariables.UserType.equals("C"))
            intent = new Intent(LPI_FAQActivity.this, ParticipantMenuActivity.class);
        else
            intent = new Intent(LPI_FAQActivity.this, LPI_FAQGroupedActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        LPI_FAQActivity.this.finish();

    }

    private void setupViewPager(ViewPager viewPager) {
        try {
            ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
            if (mArraylistEventslistIncomplete.size() > 0) {
                adapter.addFragment(new IncompleteFAQFragment(mArraylistEventslistIncomplete), "Incomplete");
                adapter.addFragment(new CompleteFAQFragment(mArraylistEventslistComplete), "Completed");
                adapter.notifyDataSetChanged();
            } else {
                adapter.addFragment(new IncompleteFAQFragment(), "Incomplete");
                adapter.addFragment(new CompleteFAQFragment(mArraylistEventslistComplete), "Completed");
            }
            if (currentVisibleTab == 1) {
                viewPager.setCurrentItem(currentVisibleTab);
            }
            viewPager.setAdapter(adapter);
            viewPager.invalidate();
            viewPager.refreshDrawableState();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void alertCustomDialog(final String title, String message) {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(LPI_FAQActivity.this).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_exit_dialog, null);
            TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            Button btn_ok = (Button) dialogView.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialogView.findViewById(R.id.btn_no);
            promt_title.setText(title);
            txt_message.setText(message);
            btn_ok.setText("OK");
            btn_no.setVisibility(View.GONE);
            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });

            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
            mFragmentList.clear();
            mFragmentTitleList.clear();
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}