package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.AsyncGetFAQ;
import com.lpi.kmi.lpi.activities.PublicUser.SearchModel;
import com.lpi.kmi.lpi.adapter.Adapters.LPI_FAQAdapter;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.ImageNicer;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

@SuppressLint("Range")
public class LPI_FAQGroupedActivity extends AppCompatActivity {

    private static RecyclerView recyclerView;
    TextView no_feeds_foundTextView;
    private static LPI_FAQAdapter statusAdapter;
    LinearLayoutManager linearLayoutManager;
    ImageView RefreshGrpTopic;
    private static ArrayList<SearchModel> mArraylistEventslist = new ArrayList<SearchModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_lpi__faqgrouped);

        Toolbar toolbar = (Toolbar) findViewById(R.id.faqGroup_toolbar_menu);
        setSupportActionBar(toolbar);
        actionBarSetup();

        recyclerView = (RecyclerView) findViewById(R.id.faqGroup_list);
        no_feeds_foundTextView = (TextView) findViewById(R.id.no_feeds_foundTextView);
        RefreshGrpTopic = (ImageView) findViewById(R.id.RefreshGrpTopic);

        boolean isDataAvailable = false;
        try {
            Cursor mCursor = DatabaseHelper.db.query(getResources().getString(R.string.TBL_FAQ_Group), null, "TrainerId=?",
                    new String[]{StaticVariables.UserLoginId}, null, null, null, null);
            if (mCursor.getCount() > 0) {
                isDataAvailable = true;
            }
            mCursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!isDataAvailable) {
            final AsyncGetFAQ getFAQ = new AsyncGetFAQ();
            if (getFAQ.getStatus() != AsyncTask.Status.RUNNING) {
                if (CommonClass.isConnected(LPI_FAQGroupedActivity.this)) {
                    new AsyncGetFAQ(new Callback<String>() {
                        @Override
                        public void execute(String result, String status) {
                            if (result.equals("Success")) {
//                            CommonClass.createNotification(LPI_FAQGroupedActivity.this, "Training Manager",
//                                    "Topic is available for conversation", "");
                                getFAQList();
                            } else {
                                setupGroupList();
                            }
                        }
                    }, LPI_FAQGroupedActivity.this, "G", "", StaticVariables.UserLoginId, StaticVariables.UserType).execute();
                } else {
                    Toast.makeText(LPI_FAQGroupedActivity.this, "Please check you have active internet connection", Toast.LENGTH_SHORT).show();
                }
            } else {
//                no_feeds_foundTextView.setVisibility(View.VISIBLE);
                Toast.makeText(LPI_FAQGroupedActivity.this, "Please wait for previous request to complete", Toast.LENGTH_SHORT).show();
            }
        } else {
            getFAQList();
        }

//        statusAdapter.setOnItemClickListener(new LPI_FAQAdapter.onClickListener() {
//            @Override
//            public void onCardViewClick(int position, View view) {
//                Intent intent= new Intent(LPI_FAQGroupedActivity.this,LPI_FAQActivity.class);
//                intent.putExtra("CandidateId",""+mArraylistEventslist.get(position).getUser_id());
//                intent.putExtra("TrainerId",""+mArraylistEventslist.get(position).getTrainer_id());
//                LPI_FAQGroupedActivity.this.startActivity(intent);
//            }
//
//            @Override
//            public void onProfileClick(int position, View view) {
//
//            }
//        });

        RefreshGrpTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AsyncGetFAQ getFAQ = new AsyncGetFAQ();
                if (getFAQ.getStatus() != AsyncTask.Status.RUNNING) {
                    if (CommonClass.isConnected(LPI_FAQGroupedActivity.this)) {
                        new AsyncGetFAQ(new Callback<String>() {
                            @Override
                            public void execute(String result, String status) {
                                getFAQList();
                            }
                        }, LPI_FAQGroupedActivity.this, "G", "",
                                StaticVariables.UserLoginId, StaticVariables.UserType).execute();
                    } else {
                        Toast.makeText(LPI_FAQGroupedActivity.this, "No internet connectivity", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(LPI_FAQGroupedActivity.this, "Please wait for previous request to complete", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    LPI_FAQAdapter.onClickListener onClickListener = new LPI_FAQAdapter.onClickListener() {

        @Override
        public void onCardViewClick(int position, View view) {
            Intent intent = new Intent(LPI_FAQGroupedActivity.this, LPI_FAQActivity.class);
            intent.putExtra("CandidateId", "" + mArraylistEventslist.get(position).getUser_id());
            intent.putExtra("TrainerId", "" + mArraylistEventslist.get(position).getTrainer_id());
            intent.putExtra("ListType", "G");
            intent.putExtra("InitFrom", "FAQGRP");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            LPI_FAQGroupedActivity.this.startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            LPI_FAQGroupedActivity.this.finish();
        }

        @Override
        public void onProfileClick(int position, View view) {

        }
    };

    private void setupGroupList() {
        if (mArraylistEventslist.size() > 0) {
            no_feeds_foundTextView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            statusAdapter = new LPI_FAQAdapter(LPI_FAQGroupedActivity.this, mArraylistEventslist, "FAQGroup");
            linearLayoutManager = new LinearLayoutManager(LPI_FAQGroupedActivity.this);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemViewCacheSize(20);
            recyclerView.setDrawingCacheEnabled(true);
            recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            recyclerView.setAdapter(statusAdapter);
            statusAdapter.setOnItemClickListener(onClickListener);
        } else {
            recyclerView.setVisibility(View.GONE);
            no_feeds_foundTextView.setVisibility(View.VISIBLE);
        }
    }


    public void getFAQList() {

        Cursor mCursor = DatabaseHelper.db.query(getResources().getString(R.string.TBL_FAQ_Group), null, "TrainerId=?",
                new String[]{StaticVariables.UserLoginId}, null, null, "LastRespDtime DESC", null);

        try {
            if (mCursor.getCount() > 0) {
                mArraylistEventslist.clear();
                int ctr = 0;
                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    SearchModel searchModel = new SearchModel();
                    searchModel.setName(mCursor.getString(mCursor.getColumnIndex("CandidateName")));
                    byte[] strIMG = mCursor.getBlob(mCursor.getColumnIndex("Images"));
                    if (strIMG == null || strIMG.equals(null) || strIMG.equals("null")) {
                        try {
                            Bitmap bitmap = ImageNicer.DecodeBitmapFromResource(LPI_FAQGroupedActivity.this.getResources(), (R.drawable.profile_blank_m), 100, 100);
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                            searchModel.setImage(stream.toByteArray());
                            stream.flush();
                            stream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        searchModel.setImage(mCursor.getBlob(mCursor.getColumnIndex("Images")));
                    }
                    searchModel.setMobile_no(mCursor.getString(mCursor.getColumnIndex("CndMobile")));
                    searchModel.setStatus("Total subjects : " + mCursor.getString(mCursor.getColumnIndex("QuesCount")));
                    searchModel.setUser_id(mCursor.getString(mCursor.getColumnIndex("CandidateId")));
                    searchModel.setTrainer_id(mCursor.getString(mCursor.getColumnIndex("TrainerId")));
                    searchModel.setQuesId("");

                    Cursor cCursor = DatabaseHelper.db.query(getResources().getString(R.string.TBL_FAQ_Conversation), null,
                            "MsgFrom<>? and isShow=? and CandidateId=?",
                            new String[]{StaticVariables.UserLoginId, "Y", mCursor.getString(mCursor.getColumnIndex("CandidateId"))},
                            "", null, null, null);
                    if (cCursor.getCount() > 0)
                        searchModel.setRespCount("" + cCursor.getCount());
                    else
                        searchModel.setRespCount("");

                    mArraylistEventslist.add(searchModel);

                    cCursor.close();
                }
                setupGroupList();
                mCursor.close();
            } else {
                //            setupViewPager(viewPager);
                mCursor.close();
            }
        } catch (Exception e) {
            Log.e("LPI_FAQGrP", "error" + e.toString());
            DatabaseHelper.SaveErroLog(LPI_FAQGroupedActivity.this, DatabaseHelper.db,
                    "LPI_FAQGroupedActivity", "getFAQList", e.toString());
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().setTitle("FAQ");
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;

            default:
                onBackPressed();
                return true;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LPI_FAQGroupedActivity.this, LPI_TrainerMenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        LPI_FAQGroupedActivity.this.finish();
    }
}
