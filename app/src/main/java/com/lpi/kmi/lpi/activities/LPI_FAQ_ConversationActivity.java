package com.lpi.kmi.lpi.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AppOpsManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.BuildConfig;
import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Services.SyncFileDocuments;
import com.lpi.kmi.lpi.activities.AsyncTasks.AsyncUpdateRespStatus;
import com.lpi.kmi.lpi.activities.AsyncTasks.SubmitConversation;
import com.lpi.kmi.lpi.adapter.Adapters.MessageListAdapter;
import com.lpi.kmi.lpi.adapter.GetterSetter.MessageContent;
import com.lpi.kmi.lpi.classes.BottomDialog;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CircleImageView;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.CompressImage;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.MarshmallowPermission;
import com.lpi.kmi.lpi.classes.MessageComparator;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.stringcare.library.SC;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import nu.xom.Serializer;

@SuppressLint("Range")
public class LPI_FAQ_ConversationActivity extends AppCompatActivity {
    public static String MobRefId = "";
    static boolean isFirstTime;
    final List<MessageContent> messageListTemp = new ArrayList<MessageContent>();
    final Handler handler = new Handler();
    //    final Handler listhandler = new Handler();
    MarshmallowPermission marshmallowPermission;
    final int delay = 15000;
    //    Map<String, String> mapTemp = new HashMap<String, String>();
    List<MessageContent> messageList = new ArrayList<MessageContent>();
    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
    //    LinearLayout layout;
//    RelativeLayout layout_2;
    ImageView refreshIcon, sendButton, img_complete, attachButton;
    CircleImageView profile;
    EditText messageArea;
    LinearLayout linCloseTopic;
    //    ScrollView scrollView;
    RecyclerView mMessageRecycler;
    MessageListAdapter mMessageAdapter;
    TextView tv_question, tv_name, topicdate;
    //    Map<String, String> map = new HashMap<String, String>();
    byte[] bitmapdata = null;
    private Uri fileUri;
    String mCurrentPhotoPath;
    public static final int PERMISSION_ALL = 1;
    public static final String[] PERMISSIONS = {
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_lpi_faq_conversation);
        marshmallowPermission = new MarshmallowPermission(this);
        initUI();
        UIListener();
        if (!marshmallowPermission.checkPermissionForExternalStorage()) {
            marshmallowPermission.requestPermissionForExternalStorage();
        }
        /*if(arePermissionDenied()){

        }*/

    }

    /*@RequiresApi(api = Build.VERSION_CODES.R)
    boolean checkStorageApi30() {
        AppOpsManager appOps = getApplicationContext().getSystemService(AppOpsManager.class);
        int mode = appOps.unsafeCheckOpNoThrow(
                Manifest.permission.MANAGE_EXTERNAL_STORAGE,
                getApplicationContext().getApplicationInfo().uid,
                getApplicationContext().getPackageName()
        );
        return mode != AppOpsManager.MODE_ALLOWED;
    }

    private boolean arePermissionDenied() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            return checkStorageApi30();
        }

        for (String permissions : PERMISSIONS) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), permissions) != PackageManager.PERMISSION_GRANTED) {
                return true;
            }
        }
        return false;
    }
    */
    private void UIListener() {

        try {
            sendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        DatabaseHelper.InsertActivityTracker(getApplicationContext(), DatabaseHelper.db,
                                StaticVariables.UserLoginId,
                                "A80022", "A80018", "Send message button clicked",
                                "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                                StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                                "", "", "Pending");
                        String messageText = messageArea.getText().toString();
                        MobRefId = UUID.randomUUID().toString();
                        if (!messageText.trim().equals("")) {//May 17 2018  1:16AM
                            MessageContent content = new MessageContent();
                            content.setQuesId(LPI_FAQActivity.strQuesID);
                            content.setCandidateId(LPI_FAQActivity.strCandidateId);
                            content.setTrainerId(LPI_FAQActivity.strTrainerId);
                            content.setIsClosed("N");
                            content.setMobRefId(MobRefId);
                            content.setQuesDesc("");
                            content.setRespDesc(messageText);
                            content.setRespFrom(StaticVariables.UserLoginId);
                            //                    content.setRespDtime(StaticVariables.sdf.format(new Date()));
                            content.setRespDtime(StaticVariables.sdf.format(new Date()));
                            content.setRespStatus("S");
                            content.setIsShow("Y");
                            content.setIsAttachment("N");
                            content.setShowProgress(false);
                            content.setmFile("");
                            content.setmFileName("");
                            content.setmFilePath("");
                            content.setmFileType("");
                            messageList.add(content);
                            setRecyclerView();

                            insertInDatabase(messageText, 1, "text", "", MobRefId, "N",
                                    "N", "", "", "", "");

                            messageArea.setText("");
                            if (CommonClass.isConnected(LPI_FAQ_ConversationActivity.this)) {
                                new SubmitConversation(new Callback<String>() {
                                    @Override
                                    public void execute(String result, String status) {
                                        if (messageList.size() > 0) {
                                            for (int k = 0; k < messageList.size(); k++) {
                                                if (messageList.get(k).getMobRefId().equals(result)) {
                                                    messageList.get(k).setRespStatus("S");
                                                }
                                            }
                                        }
                                    }
                                }, getApplicationContext(), "", MobRefId).execute();
                            }
                            try {
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                DatabaseHelper.UpdateSubjectTimeFAQList(LPI_FAQ_ConversationActivity.this,
                                        DatabaseHelper.db, LPI_FAQActivity.strCandidateId, LPI_FAQActivity.strTrainerId,
                                        LPI_FAQActivity.strQuesID, simpleDateFormat.format(Calendar.getInstance().getTime()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(LPI_FAQ_ConversationActivity.this, "Please write message!!!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            attachButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MobRefId = UUID.randomUUID().toString();
                    if (!marshmallowPermission.checkPermissionForExternalStorage()) {
                        marshmallowPermission.requestPermissionForExternalStorage();
                    } else if (!marshmallowPermission.checkPermissionForCamera()) {
                        marshmallowPermission.requestPermissionForCamera();
                    } else {
                        BottomDialog dialog = BottomDialog.newInstance("UPLOAD", new String[]{"Camera", "Gallery", "File"}, new int[]{R.drawable.ic_camera_dailog, R.drawable.ic_gallery_dailog, R.drawable.ic_document_dailog}, 3);
                        dialog.show(LPI_FAQ_ConversationActivity.this.getSupportFragmentManager(), "dialog");
                        //add item click listener
                        dialog.setListener(new BottomDialog.OnClickListener() {
                            @Override
                            public void click(int position) {
                                if (position == 0) {
                                    captureImage();
                                } else if (position == 1) {
                                    browsePicture();
                                } else if (position == 2) {
                                    showFileChooser();
                                }
                            }
                        });
                    }
                }
            });
            img_complete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CompleteConversation();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void CompleteConversation() {
        try {
            alertCustomDialog("Close Conversation!", "Do you want to Complete the conversation?");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void alertCustomDialog(final String title, String message) {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(LPI_FAQ_ConversationActivity.this).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_exit_dialog, null);
            TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            Button btn_ok = (Button) dialogView.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialogView.findViewById(R.id.btn_no);
            promt_title.setText(title);
            txt_message.setText(message);
            btn_ok.setText("OK");

            if (title.equalsIgnoreCase("Close Conversation!")) {
                btn_ok.setText("Yes");
                btn_no.setText("No");
                btn_no.setVisibility(View.VISIBLE);
            } else {
                btn_no.setVisibility(View.GONE);
            }


            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    if (title.equalsIgnoreCase("Close Conversation!")) {
                        try {
                            DatabaseHelper.CloseConversationDetails(LPI_FAQ_ConversationActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                                    StaticVariables.ParentId, LPI_FAQActivity.strQuesID, "Y");
                            LPI_FAQActivity.strIsClosed = "Y";
                            MobRefId = UUID.randomUUID().toString();
                            insertInDatabase("", 1, "text", "", MobRefId, "N",
                                    "N", "", "", "", "");
                            new SubmitConversation(new Callback<String>() {
                                @Override
                                public void execute(String result, String status) {
                                    Intent intent = new Intent(LPI_FAQ_ConversationActivity.this, LPI_FAQActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.putExtra("CandidateId", "" + LPI_FAQActivity.strCandidateId);
                                    intent.putExtra("TrainerId", "" + LPI_FAQActivity.strTrainerId);
                                    intent.putExtra("InitFrom", "Conversation");
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    LPI_FAQ_ConversationActivity.this.finish();
                                    Toast.makeText(getApplicationContext(), "Conversation close successfully.", Toast.LENGTH_SHORT).show();
                                }
                            }, getApplicationContext(), "", MobRefId).execute();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initUI() {
        String isClosed = "";

        tv_question = (TextView) findViewById(R.id.tv_question);
        tv_name = (TextView) findViewById(R.id.tv_name);
        topicdate = (TextView) findViewById(R.id.topicdate);

        try {
            Bundle bundle = getIntent().getExtras();
            LPI_FAQActivity.strQuestion = bundle.getString("question");
            LPI_FAQActivity.strQuesID = bundle.getString("question_id");
            LPI_FAQActivity.strCandidateId = bundle.getString("user_id");
            LPI_FAQActivity.strTrainerId = bundle.getString("trainer_id");
            LPI_FAQActivity.strUserName = bundle.getString("user_name");

            bitmapdata = bundle.getByteArray("profile_pic");
            isClosed = bundle.getString("isClosed");

            Cursor cursor = DatabaseHelper.db.query(getResources().getString(R.string.TBL_FAQ_List),
                    new String[]{"RespDtime"}, "CandidateId=? and TrainerId=? and QuesId=?",
                    new String[]{LPI_FAQActivity.strCandidateId, LPI_FAQActivity.strTrainerId, LPI_FAQActivity.strQuesID},
                    null, null, null, "1");
            if (cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    Date d = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(cursor.getString(0));
                    topicdate.setText(StaticVariables.sdf.format(d));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_menu);
        setSupportActionBar(toolbar);
        actionBarSetup();

//        layout = (LinearLayout) findViewById(R.id.layout1);
//        layout_2 = (RelativeLayout) findViewById(R.id.layout2);
        linCloseTopic = (LinearLayout) findViewById(R.id.linCloseTopic);
        refreshIcon = (ImageView) findViewById(R.id.refreshIcon);
        sendButton = (ImageView) findViewById(R.id.sendButton);
        attachButton = (ImageView) findViewById(R.id.attachButton);
        img_complete = (ImageView) findViewById(R.id.img_complete);
        profile = (CircleImageView) findViewById(R.id.profile);
        messageArea = (EditText) findViewById(R.id.messageArea);
//        scrollView = (ScrollView) findViewById(R.id.scrollView);

        refreshIcon.setVisibility(View.GONE);
        mMessageRecycler = (RecyclerView) findViewById(R.id.reyclerview_message_list);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setSmoothScrollbarEnabled(true);
        layoutManager.setStackFromEnd(true);
        mMessageRecycler.setLayoutManager(layoutManager);

        tv_name.setText(LPI_FAQActivity.strUserName);
        tv_question.setText(LPI_FAQActivity.strQuestion);
        LinearLayout layout_messageArea = (LinearLayout) findViewById(R.id.msgArea);
        if (isClosed != null && isClosed.equalsIgnoreCase("Y")) {
            linCloseTopic.setVisibility(View.GONE);
            layout_messageArea.setVisibility(View.GONE);
        } else {
            layout_messageArea.setVisibility(View.VISIBLE);
            if (StaticVariables.UserType.equals("C")) {
                linCloseTopic.setVisibility(View.VISIBLE);
            } else if (StaticVariables.UserType.equals("T")) {
                linCloseTopic.setVisibility(View.GONE);
            }
        }

        try {
            messageList.clear();
            isFirstTime = getConversation();
            if (isFirstTime) {

                mMessageAdapter = new MessageListAdapter(LPI_FAQ_ConversationActivity.this, messageList);
                mMessageRecycler.setAdapter(mMessageAdapter);
                if (CommonClass.isConnected(LPI_FAQ_ConversationActivity.this)) {
                    new AsyncGetFAQConversation(LPI_FAQ_ConversationActivity.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    triggerGetApi();
                } else {
                    alertCustomDialog("Alert!", "No internet connection found please connect internet and try again.\nUnable to get conversation detail.");
                }
            } else {
                mMessageAdapter = new MessageListAdapter(LPI_FAQ_ConversationActivity.this, messageList);
                mMessageRecycler.setAdapter(mMessageAdapter);
                setRecyclerView();
                triggerGetApi();
            }
        } catch (Exception e) {
            Log.e("", "error=" + e.toString());
        }

        try {
            if (bitmapdata != null) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);
                profile.setImageBitmap(bitmap);
            } else {
                profile.setImageDrawable(getResources().getDrawable(R.drawable.profile_blank_m));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            DatabaseHelper.InsertActivityTracker(getApplicationContext(), DatabaseHelper.db,
                    StaticVariables.UserLoginId,
                    "A80021", "A80018", "FAQ Conversation page called",
                    "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                    StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                    "", "", "Pending");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkAndRequestPermissions()) {

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkAndRequestPermissions() {
        int permissionCamera = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);
        int permissionStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionPhone = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionCamera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (permissionStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionPhone != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSION_ALL);
            return false;
        }
        return true;
    }

    private void setUPUI() {
        ContentValues values = new ContentValues();
        if (messageList.size() > 0) {
            for (int i = 0; i < messageList.size(); i++) {
                if (messageList.get(i).getIsShow().equals("Y")) {
                    messageList.get(i).setIsShow("N");
                    values.clear();
                    values.put("isShow", "N");
                    DatabaseHelper.db.update(getResources().getString(R.string.TBL_FAQ_Conversation),
                            values, "MobRefId=?", new String[]{messageList.get(i).getMobRefId()});
                }

                if (!messageList.get(i).getRespStatus().equals("R") &&
                        !messageList.get(i).getRespFrom().equals(StaticVariables.UserLoginId)) {
                    messageList.get(i).setRespStatus("R");
                    values.clear();
                    values.put("RespStatus", "R");
                    DatabaseHelper.db.update(getResources().getString(R.string.TBL_FAQ_Conversation),
                            values, "MobRefId=?", new String[]{messageList.get(i).getMobRefId()});
                }
            }
        }
    }

    private void triggerGetApi() {

        AsyncGetFAQConversation asyncGetFAQConversation = new AsyncGetFAQConversation();
        if (asyncGetFAQConversation.getStatus() != AsyncTask.Status.RUNNING) {
            handler.postDelayed(new Runnable() {
                public void run() {
                    if (CommonClass.isConnected(LPI_FAQ_ConversationActivity.this)) {
                        AsyncGetFAQConversation asyncGetFAQConversation = new AsyncGetFAQConversation();
                        if (asyncGetFAQConversation.getStatus() != AsyncTask.Status.RUNNING) {
                            new AsyncGetFAQConversation(LPI_FAQ_ConversationActivity.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                    handler.postDelayed(this, delay);
                }
            }, delay);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().setTitle("");
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        handler.removeCallbacksAndMessages(null);
//        listhandler.removeCallbacksAndMessages(null);
        Intent intent = new Intent(LPI_FAQ_ConversationActivity.this, LPI_FAQActivity.class);
        intent.putExtra("CandidateId", LPI_FAQActivity.strCandidateId);
        intent.putExtra("TrainerId", LPI_FAQActivity.strTrainerId);
        intent.putExtra("InitFrom", "Conversation");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        LPI_FAQ_ConversationActivity.this.finish();
    }

    public void addMessageBox(String message, int from, String isShow, String MobRefId) {
        if (isShow.equals("Y")) {
            TextView textView = new TextView(LPI_FAQ_ConversationActivity.this);
            ImageView imgView = new ImageView(LPI_FAQ_ConversationActivity.this);
            textView.setText(message);


            LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp2.weight = 2.0f;

            if (from == 1) {
                lp2.setMargins(15, 10, 10, 10);
                lp2.gravity = Gravity.RIGHT;
                textView.setBackgroundResource(R.drawable.shape_outcoming_message);
                textView.setPadding(50, 50, 40, 50);
//            imgView.setImageDrawable(this.getResources().getDrawable(R.drawable.arrow_bg1));
            } else {
                lp2.setMargins(10, 10, 15, 10);
                lp2.gravity = Gravity.LEFT;
                textView.setBackgroundResource(R.drawable.shape_incoming_message);
                textView.setPadding(40, 50, 50, 50);
//            imgView.setImageDrawable(this.getResources().getDrawable(R.drawable.arrow_bg2));

            }

            DatabaseHelper.updateShowFAQConversation(getApplicationContext(), DatabaseHelper.db,
                    LPI_FAQActivity.strCandidateId, LPI_FAQActivity.strTrainerId, LPI_FAQActivity.strQuesID, MobRefId);
        }
    }

    private void insertInDatabase(String message, int from, String msgType, String docLocation,
                                  String MobRefId, String isShow, String isAttachment,
                                  String mFile, String mFileName, String mFilePath, String mFileType) {
        String strFrom = "";
        if (from == 1) {
            strFrom = StaticVariables.UserLoginId;
        } else {
            strFrom = LPI_FAQActivity.strCandidateId;
        }
        String curDate = StaticVariables.sdf.format(Calendar.getInstance().getTime());
        int i = DatabaseHelper.SaveConversation(LPI_FAQ_ConversationActivity.this, DatabaseHelper.db,
                LPI_FAQActivity.strIsClosed, message, LPI_FAQActivity.strTrainerId, LPI_FAQActivity.strCandidateId,
                LPI_FAQActivity.strQuestion, strFrom, curDate,
                LPI_FAQActivity.strQuesID, MobRefId, msgType, docLocation, "pending", "S",
                isShow, isAttachment, mFile, mFileName, mFilePath, mFileType);

    }

    @SuppressLint("Range")
    public boolean getConversation() {
        Cursor mCursor = null;
        boolean callApi = false;
        try {
            if (DatabaseHelper.db == null) {
                SQLiteDatabase.loadLibs(getApplicationContext());
                DatabaseHelper.db = DatabaseHelper.getInstance(getApplicationContext()).
                        getWritableDatabase(SC.reveal(CommonClass.DBPassword));
            }
            mCursor = DatabaseHelper.getFAQConversation(LPI_FAQ_ConversationActivity.this,
                    DatabaseHelper.db, LPI_FAQActivity.strCandidateId, LPI_FAQActivity.strTrainerId, LPI_FAQActivity.strQuesID);

            if (mCursor.getCount() > 0) {
//                layout.removeAllViews();
                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    MessageContent content = new MessageContent();
                    content.setQuesId(mCursor.getString(mCursor.getColumnIndex("QuesId")));
                    content.setCandidateId(mCursor.getString(mCursor.getColumnIndex("CandidateId")));
                    content.setTrainerId(mCursor.getString(mCursor.getColumnIndex("TrainerId")));
                    content.setIsClosed(mCursor.getString(mCursor.getColumnIndex("isClosed")));
                    content.setMobRefId(mCursor.getString(mCursor.getColumnIndex("MobRefId")));
                    content.setQuesDesc(mCursor.getString(mCursor.getColumnIndex("QuesDesc")));
                    content.setRespDesc(mCursor.getString(mCursor.getColumnIndex("RespDesc")));
                    content.setRespFrom(mCursor.getString(mCursor.getColumnIndex("MsgFrom")));
                    content.setRespDtime(mCursor.getString(mCursor.getColumnIndex("RespDtime")));
                    content.setRespStatus(mCursor.getString(mCursor.getColumnIndex("RespStatus")));
                    if (mCursor.isNull(mCursor.getColumnIndex("isShow"))) {
                        content.setIsShow("Y");
                    } else {
                        content.setIsShow(mCursor.getString(mCursor.getColumnIndex("isShow")));
                    }
                    content.setShowProgress(false);
                    content.setIsAttachment(mCursor.getString(mCursor.getColumnIndex("isAttachment")));
//                    content.setmFile(mCursor.getString(mCursor.getColumnIndex("mFile")));
                    content.setmFileName(mCursor.getString(mCursor.getColumnIndex("mFileName")));
                    content.setmFilePath(mCursor.getString(mCursor.getColumnIndex("mFilePath")));
                    content.setmFileType(mCursor.getString(mCursor.getColumnIndex("mFileType")));

                    messageList.add(content);
                }
                if (messageList.size() > 0)
                    callApi = false;
            } else {
                callApi = true;
            }
            mCursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            messageList.clear();
            callApi = true;
        }
        return callApi;
    }

    private void captureImage() {
//        fileUri = Uri.fromFile(CompressImage.getOutputMediaFile());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                fileUri = getOutputMediaFileUri();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            fileUri = getOutputMediaFileUriBelowL();
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, 555);
    }

    private void browsePicture() {
//        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        startActivityForResult(intent, 333);
//        fileUri = Uri.fromFile(CompressImage.getOutputMediaFile());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                fileUri = getOutputMediaFileUri();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            fileUri = getOutputMediaFileUriBelowL();
        }
        Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 333);
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    888);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        } catch (Exception ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 333 && resultCode == Activity.RESULT_OK) {
            Bitmap thePic;
            final android.net.Uri selectedImage = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                thePic = (BitmapFactory.decodeStream(inputStream));
                if (thePic != null) {
                    storeImage(thePic);
//                    fileUri = selectedImage;
                    setCustomPreview(fileUri, data, 333);
                } else {
                    try {
                        thePic = BitmapFactory.decodeFile(CompressImage.compressImage(fileUri.getPath(),
                                selectedImage.getPath(),LPI_FAQ_ConversationActivity.this));
                        if (thePic != null) {
                            storeImage(thePic);
                            setCustomPreview(selectedImage, data, 333);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 555 && resultCode == Activity.RESULT_OK) {
            try {
                try {
                    Bitmap photo = decodeFile(mCurrentPhotoPath);
                    if (photo == null) {
                        try {
                            Uri imageUri = Uri.parse(mCurrentPhotoPath);
                            File file = new File(imageUri.getPath());
                            InputStream ims = new FileInputStream(file);
                            photo = (BitmapFactory.decodeStream(ims));
                            if (photo != null) {
                                fileUri = imageUri;
                                setCustomPreview(imageUri, data, 555);
                            }
                        } catch (Exception a) {
                            String errorMessage = "Sorry - Image capturing failed, Please try again.";
                            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_LONG);
                            toast.show();
                        }
                    } else {
                        setCustomPreview(fileUri, null, 555);
                    }
//                cropImage(fileUri);
                } catch (Exception aNFE) {
                    String errorMessage = "Sorry - your device doesn't support the crop action!";
                    Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_LONG);
                    toast.show();
                }
               /* Uri imageUri = Uri.parse(mCurrentPhotoPath);
                setCustomPreview(imageUri, null, 555);*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == 666 && resultCode == Activity.RESULT_OK) {
            try {
                setCustomPreview(fileUri, data, 666);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (resultCode == RESULT_OK && requestCode == 888) {
            String strFilePath = "";
            try {
                Uri uri = data.getData();
                new saveFileInDB(getApplicationContext(), uri).execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static Bitmap decodeFile(String photoPath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoPath, options);

        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inPreferQualityOverSpeed = true;

        return BitmapFactory.decodeFile(photoPath, options);
    }

    public String getPathFromUri(Uri uri, ContentResolver contentResolver) {
        String filePath = null;
        String scheme = uri.getScheme();
        if (ContentResolver.SCHEME_CONTENT.equals(scheme)) {
            String[] filePathColumn = {MediaStore.MediaColumns.DATA};
            android.database.Cursor cursor = contentResolver.query(uri, filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                filePath = cursor.getString(columnIndex);
                cursor.close();
            }
        } else {
            filePath = uri.getPath();
        }
        return filePath;
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        @SuppressWarnings("deprecation")
        android.database.Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }


    private void setCustomPreview(final Uri selectedImage, Intent data, int requestCode) {
        final Dialog dialog = new Dialog(LPI_FAQ_ConversationActivity.this);
        dialog.setContentView(R.layout.dialog_preview_screen);
        ImageView img_screen = (ImageView) dialog.findViewById(R.id.img_screen);
        ImageView img_crop = (ImageView) dialog.findViewById(R.id.iv_crop);
        Button btn_send = (Button) dialog.findViewById(R.id.btn_send);
        img_crop.setVisibility(View.GONE);

        String fileName = "", strFilePath = "";

        try {
            strFilePath = fileUri.getPath();
            int cut = fileUri.getPath().lastIndexOf('/');
            if (cut != -1) {
                fileName = fileUri.getPath().substring(cut + 1);
            }

            final MessageContent content = new MessageContent();
            content.setQuesId(LPI_FAQActivity.strQuesID);
            content.setCandidateId(LPI_FAQActivity.strCandidateId);
            content.setTrainerId(LPI_FAQActivity.strTrainerId);
            content.setIsClosed("N");
            content.setMobRefId(MobRefId);
            content.setQuesDesc("");
            content.setRespDesc("Uploading..." + fileName);
            content.setRespFrom(StaticVariables.UserLoginId);
            content.setRespDtime(StaticVariables.sdf.format(new Date()));
            content.setRespStatus("S");
            content.setIsShow("Y");
            content.setShowProgress(true);
            content.setIsAttachment("Y");
            content.setmFile("");
            content.setmFileName(fileName);
            content.setmFilePath(strFilePath);
            content.setmFileType(CommonClass.getMimeType(strFilePath));

            insertInDatabase("", 1, "image", fileUri.getPath(), MobRefId, "Y",
                    "", "", fileName, fileUri.getPath(),
                    CommonClass.getMimeType(fileUri.getPath()));


            if (requestCode == 333) {
                try {
                    Bitmap thePic;
                    Uri selectedPhoto = data.getData();
                    try {
                        InputStream inputStream = LPI_FAQ_ConversationActivity.this.getContentResolver().openInputStream(data.getData());
                        thePic = (BitmapFactory.decodeStream(inputStream));
                        if (thePic != null) {
                            //                        storeImage(thePic);
                            img_screen.setImageBitmap(thePic);
                        } else {
                            try {
                                thePic = BitmapFactory.decodeFile(CompressImage.compressImage(fileUri.getPath(),
                                        selectedPhoto.getPath(),LPI_FAQ_ConversationActivity.this));
                                if (thePic != null) {
                                    //                                storeImage(thePic);
                                    img_screen.setImageBitmap(thePic);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.dismiss();
                    Toast.makeText(LPI_FAQ_ConversationActivity.this,
                            "Preview not available, sending attachment", Toast.LENGTH_SHORT).show();
                    messageList.add(content);
                    setRecyclerView();
                    //                sendAttachment(fileUri);
                    new saveFileInDB(getApplicationContext(), fileUri).execute();
                }
                btn_send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        messageList.add(content);
                        setRecyclerView();
                        sendAttachment(fileUri);
                    }
                });

                img_crop.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cropCapturedImage(selectedImage);
                    }
                });

            } else if (requestCode == 555) {
                try {
                    Bitmap scaledBitmap = BitmapFactory.decodeFile(CompressImage.compressImage(fileUri.getPath(),
                            LPI_FAQ_ConversationActivity.this));
                    img_screen.setImageBitmap(scaledBitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.dismiss();
                    Toast.makeText(LPI_FAQ_ConversationActivity.this,
                            "Preview not available, sending attachment", Toast.LENGTH_SHORT).show();
                    messageList.add(content);
                    setRecyclerView();
                    //                sendAttachment(fileUri);
                    new saveFileInDB(getApplicationContext(), fileUri).execute();
                }
                btn_send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        messageList.add(content);
                        setRecyclerView();
                        sendAttachment(fileUri);
                    }
                });
                img_crop.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cropCapturedImage(selectedImage);
                    }
                });
            } else if (requestCode == 666) {
                img_crop.setVisibility(View.GONE);
                Bundle extras = data.getExtras();
                Bitmap thePic = null;
                try {
                    if (extras != null) {
                        thePic = extras.getParcelable("data");
                        Bitmap thePicq = (Bitmap) extras.get("data");
                        if (thePic != null && !thePic.toString().trim().equalsIgnoreCase("")) {
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            thePic.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                        } else {
                            Uri imageUri = data.getData();
                            thePic = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                            if (thePic != null) {
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                thePic.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                                //saveBitmap(thePic);
                            }
                        }
                    } else {
                        if (data != null) {
                            Uri imageUri = data.getData();
                            thePic = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                            if (thePic != null) {
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                thePic.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                                //saveBitmap(thePic);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                img_screen.setImageBitmap(thePic);

                btn_send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        messageList.add(content);
                        setRecyclerView();
                        sendAttachment(fileUri);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        dialog.show();
    }

    private void sendAttachment(Uri fileUri) {
        if (CommonClass.isConnected(LPI_FAQ_ConversationActivity.this.getApplicationContext())) {
            new SubmitConversation(new Callback<String>() {
                @Override
                public void execute(String result, String status) {

                    for (int j = 0; j < messageList.size(); j++) {
                        if (messageList.get(j).getMobRefId().equals(result) && status.equals("Success")) {
                            messageList.get(j).setRespDesc("");
                            messageList.get(j).setRespStatus("S");
                            messageList.get(j).setShowProgress(false);
                            mMessageAdapter.notifyItemChanged(j);
                        } else {
                            messageList.get(j).setShowProgress(false);
                        }
                    }
                    if (mMessageAdapter != null) {
                        mMessageAdapter.notifyDataSetChanged();
                    } else {
                        setRecyclerView();
                    }
                }
            }, LPI_FAQ_ConversationActivity.this.getApplicationContext(),
                    fileUri.getPath(), MobRefId).execute();
            Intent intent = new Intent(getApplicationContext(), SyncFileDocuments.class);
            intent.putExtra("filePath", fileUri.getPath());
            intent.putExtra("MobRefId", MobRefId);
//            getApplicationContext().startService(intent);
            CommonClass.StartService(getApplicationContext(), intent);
        } else {
            CommonClass.createNotification(getApplicationContext(), "No internet", "Your attachment will be auto uploaded once internet connectivity is back", "");
        }

    }

    private void readFileToString(String filePath)
            throws java.io.IOException {

        StringBuffer fileData = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(
                new FileReader(filePath));
        char[] buf = new char[1024];

        int numRead = 0;
        while ((numRead = reader.read(buf)) != -1) {
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }

        reader.close();
        System.out.println(fileData.toString());
//        return fileData.toString();
//        addMessageBoxFiles("" + filePath, 1);
//        insertInDatabase(fileData.toString(), 1, "file", filePath, "", "");
    }

    public void cropCapturedImage(Uri picUri) {
        //call the standard crop action intent
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(picUri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            cropIntent.putExtra("return-data", true);
            startActivityForResult(cropIntent, 666);

        } catch (Exception e) {

        }

    }

    public String BitMapToString(Bitmap bitmap) {
        String temp = "";
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            byte[] b = baos.toByteArray();
            temp = Base64.encodeToString(b, Base64.DEFAULT);
            baos.flush();
            baos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    private void setRecyclerView() {
        try {
            setUPUI();

            if (messageList.size() > 0) {
                mMessageAdapter.notifyDataSetChanged();
                mMessageRecycler.smoothScrollToPosition(mMessageAdapter.getItemCount());
                mMessageAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                    @Override
                    public void onChanged() {
                        if (mMessageAdapter != null && mMessageAdapter.getItemCount() > 0) {
//                            mMessageAdapter.notifyItemChanged(mMessageAdapter.getItemCount());
                            mMessageRecycler.smoothScrollToPosition(mMessageAdapter.getItemCount());
                        }
                    }
                });
            }
        } catch (Exception e) {
            Log.e("setRecyclerView", "error = " + e.toString());
            e.printStackTrace();
        }
    }

    public Uri getOutputMediaFileUri() {
//        return Uri.fromFile(getOutputMediaFile(type));
        return FileProvider.getUriForFile(LPI_FAQ_ConversationActivity.this,
                BuildConfig.APPLICATION_ID + ".provider",
                getOutputMediaFile());
    }

    public Uri getOutputMediaFileUriBelowL() {
        return Uri.fromFile(getOutputMediaFile());
    }

    private File getOutputMediaFile() {
        File mediaStorageDir = new File(LPI_FAQ_ConversationActivity.this.getExternalFilesDir(Environment.MEDIA_SHARED),
                StaticVariables.ParentStoragePath + "Images");
        if (!mediaStorageDir.exists()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                try {
                    Files.createDirectory(Paths.get(mediaStorageDir.getAbsolutePath()));
                } catch (IOException e) {
                    e.printStackTrace();
                    mediaStorageDir.mkdirs();
                } catch (Exception e) {
                    e.printStackTrace();
                    mediaStorageDir.mkdirs();
                }
            } else {
                mediaStorageDir.mkdirs();
            }
            if (!mediaStorageDir.exists()) {
                return null;
            }
        } else {
            try {
                File file = new File(mediaStorageDir.getPath() + File.separator + ".nomedia");
                if (!file.exists()) {
                    File output = new File(mediaStorageDir, ".nomedia");
                    boolean fileCreated = output.createNewFile();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;

        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");

        mCurrentPhotoPath = "file:" + mediaFile.getAbsolutePath();

        return mediaFile;
    }

    private boolean storeImage(Bitmap image) {
        File pictureFile = CompressImage.getOutputMediaFile(LPI_FAQ_ConversationActivity.this);
        fileUri = Uri.fromFile(pictureFile);
        if (pictureFile == null) {
            return false;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.JPEG, 80, fos);
            fos.close();
            return true;
        } catch (FileNotFoundException e) {
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public class saveFileInDB extends AsyncTask<String, String, String> {

        String filePath = "Failed";
        Context context;
        Uri fileUri;
        InputStream inputStream = null;
        OutputStream outputStream = null;

        public saveFileInDB(Context context, Uri fileUri) {
            this.context = context;
            this.fileUri = fileUri;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                ContentResolver content = context.getContentResolver();
                inputStream = content.openInputStream(fileUri);
                CommonClass.CreateDirectories(context);
                String strFileParentPath = context.getExternalFilesDir(Environment.MEDIA_SHARED).getPath() + StaticVariables.ParentStoragePath;

                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String mFileName = "";
                String parentPath = "";
                /*String strFilePath = CompressImage.getPath(LPI_FAQ_ConversationActivity.this, fileUri);
                //getRealPathFromURI(LPI_FAQ_ConversationActivity.this, fileUri);
                if (strFilePath != null && strFilePath != "") {
                    if (strFilePath.contains("/raw//")) {
                        strFilePath = strFilePath.replace("/raw//", "/");
                    } else if (strFilePath.contains("/document//")) {
                        strFilePath = strFilePath.replace("/document//", "/");
                    } else if (strFilePath.contains("/document/0000-0000:")) {
                        strFilePath = strFilePath.replace("/document/0000-0000:", context.getExternalFilesDir(Environment.MEDIA_SHARED).getPath() + "/");
                    } else if (strFilePath.contains("/document/primary:")) {
                        strFilePath = strFilePath.replace("/document/primary:", context.getExternalFilesDir(Environment.MEDIA_SHARED).getPath() + "/");
                    } else if (strFilePath.contains("/document/image:")) {
                        strFilePath = strFilePath.replace("/document/image:", context.getExternalFilesDir(Environment.MEDIA_SHARED).getPath() + "/");
                    }
                } else {
                    mFileName = "File_" + timeStamp.trim();
                    parentPath = strFileParentPath + "Images/" + mFileName + ".jpeg";
                }*/


               /*
                // commented for android 12
                //

                String strFilePath = null;
                try {
                    strFilePath = CompressImage.getUriRealPath(LPI_FAQ_ConversationActivity.this, fileUri);
                } catch (Exception e) {
                    e.printStackTrace();
//                    strFilePath = CompressImage.getFilePathFromURI(context, fileUri);
                }*/

                String getFileType = "";
                getFileType = CommonClass.getMimeType(fileUri.getPath());

                if (getFileType != null && getFileType != "") {
                    if (getFileType.equalsIgnoreCase("image/ief") ||
                            getFileType.equalsIgnoreCase("image/jpeg") ||
                            getFileType.equalsIgnoreCase("image/png")) {
                        mFileName = "IMG_" + timeStamp.trim() + ".jpeg";
                        parentPath = strFileParentPath + "Images/" + mFileName;
                    } else if (getFileType.equalsIgnoreCase("application/vnd.ms-excel") ||
                            getFileType.equalsIgnoreCase("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                        mFileName = "EXL_" + timeStamp.trim() + ".xls";
                        parentPath = strFileParentPath + "Documents/" + mFileName;
                    } else if (getFileType.trim().equalsIgnoreCase("application/pdf")) {
                        mFileName = "PDF_" + timeStamp.trim() + ".pdf";
                        parentPath = strFileParentPath + "Documents/" + mFileName;
                    } else if (getFileType.trim().equalsIgnoreCase("text/plain")) {
                        mFileName = "TEXT_" + timeStamp.trim() + ".txt";
                        parentPath = strFileParentPath + "Documents/" + mFileName;
                    } else if (getFileType.trim().equalsIgnoreCase("application/msword")) {
                        mFileName = "DOC_" + timeStamp.trim() + ".doc";
                        parentPath = strFileParentPath + "Documents/" + mFileName;
                    } else if (getFileType.trim().equalsIgnoreCase("application/vnd.ms-powerpoint") ||
                            getFileType.trim().equalsIgnoreCase("application/vnd.openxmlformats-officedocument.presentationml.presentation")) {
                        mFileName = "PPT_" + timeStamp.trim() + ".ppt";
                        parentPath = strFileParentPath + "Documents/" + mFileName;
                    } else if (getFileType.trim().equalsIgnoreCase("audio/x-aac") ||
                            getFileType.trim().equalsIgnoreCase("audio/mpeg") ||
                            getFileType.trim().equalsIgnoreCase("audio/amr")) {
                        mFileName = "ADO_" + timeStamp.trim() + ".mp3";
                        parentPath = strFileParentPath + "Audios/" + mFileName;
                    } else if (getFileType.trim().equalsIgnoreCase("video/mp4") ||
                            getFileType.trim().equalsIgnoreCase("video/x-msvideo") ||
                            getFileType.trim().equalsIgnoreCase("video/3gpp") ||
                            getFileType.trim().equalsIgnoreCase("video/x-msvideo")) {
                        mFileName = "VDO_" + timeStamp.trim() + ".mp4";
                        parentPath = strFileParentPath + "Videos/" + mFileName;
                    }

                    insertInDatabase("", 1, getFileType, parentPath, MobRefId, "Y",
                            "Y", "", mFileName, parentPath, getFileType);

                    MessageContent mContent = new MessageContent();
                    mContent.setQuesId(LPI_FAQActivity.strQuesID);
                    mContent.setCandidateId(LPI_FAQActivity.strCandidateId);
                    mContent.setTrainerId(LPI_FAQActivity.strTrainerId);
                    mContent.setIsClosed("N");
                    mContent.setMobRefId(MobRefId);
                    mContent.setQuesDesc("");
                    mContent.setRespDesc("Uploading..." + mFileName);
                    mContent.setRespFrom(StaticVariables.UserLoginId);
                    mContent.setRespDtime(StaticVariables.sdf.format(new Date()));
                    mContent.setRespStatus("S");
                    mContent.setIsShow("Y");
                    mContent.setShowProgress(true);
                    mContent.setIsAttachment("Y");
//            mContent.setmFile(strBitmap);
                    mContent.setmFileName(mFileName);
                    mContent.setmFilePath(parentPath);
                    mContent.setmFileType(getFileType);

                    messageList.add(mContent);

                    outputStream = new FileOutputStream(parentPath); // filename.png, .mp3, .mp4 ...
                    if (outputStream != null) {
                        //Log.e( TAG, "Output Stream Opened successfully");
                    }
                    byte[] buffer = new byte[1024];
                    int bytesRead = 0;
                    while ((bytesRead = inputStream.read(buffer, 0, buffer.length)) >= 0) {
                        outputStream.write(buffer, 0, buffer.length);
                    }

                    filePath = parentPath;
                } else {
                    filePath = "";
                }
            } catch (Exception e) {
                //Log.e( TAG, "Exception occurred " + e.getMessage());
                e.printStackTrace();
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "ConversationActivity", "SaveFileInDB", e.toString());

                filePath = "";
            } finally {
                try {
                    outputStream.flush();
                    outputStream.close();
                    inputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return filePath;
        }

        @Override
        protected void onPostExecute(String filePath) {
            CommonClass.createNotification(getApplicationContext(), "Uploading attachment", "Your attachment is being uploaded, and will be displayed once done", "");
            if (!filePath.equals("")) {
                setRecyclerView();

                Intent intent = new Intent(getApplicationContext(), SyncFileDocuments.class);
                intent.putExtra("filePath", filePath);
                intent.putExtra("MobRefId", MobRefId);
//                getApplicationContext().startService(intent);
                CommonClass.StartService(getApplicationContext(), intent);
//
//                if (CommonClass.isConnected(context))
//                    new SubmitConversation(new Callback<String>() {
//                        @Override
//                        public void execute(String result, String status) {
//                            for (int j = 0; j < messageList.size(); j++) {
//                                if (messageList.get(j).getMobRefId().equals(MobRefId)) {
//                                    messageList.get(j).setRespStatus("S");
//                                    messageList.get(j).setShowProgress(false);
//                                }
//                            }
//                        }
//                    }, context.getApplicationContext(), fileUri.getPath(), MobRefId).execute();
            } else {
                Toast.makeText(context.getApplicationContext(), "File format does not support to share file.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class saveBitmap extends AsyncTask<String, String, String> {
        public Callback<String> callback;
        Bitmap bitmap;
        Context context;
        String fileName;
        String parentPath;
        int item = 0;

        public saveBitmap(Context context, Bitmap bitmap, String timeStamp, String fileName, String parentPath) {
            this.context = context;
            this.bitmap = bitmap;
            this.fileName = fileName;
            this.parentPath = parentPath;
        }

        public saveBitmap(Callback<String> callback, Context context, Bitmap bitmap, String fileName, String parentPath) {
            this.context = context;
            this.bitmap = bitmap;
            this.fileName = fileName;
            this.parentPath = parentPath;
            this.callback = callback;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... strings) {
            FileOutputStream out = null;
            try {
                //parentPath + fileName
                //compressImage(parentPath + fileName)

                File mediaFile = new File(parentPath);
                out = new FileOutputStream(mediaFile);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance

                // PNG is a lossless format, the compression factor (100) is ignored
                if (out != null) {
                    out.flush();
                    out.close();
                }
//                String strBitmap = BitMapToString(bitmap);
                //uploadPicture(strBitmap);
//            addMessageBoxImage(strBitmap, 1);
                Log.e("", "File Path =" + mediaFile.getAbsolutePath());
                insertInDatabase("", 1, "image", parentPath, MobRefId, "Y",
                        "", "", fileName, parentPath,
                        CommonClass.getMimeType(mediaFile.getAbsolutePath()));

                return "";
            } catch (Exception e) {
                e.printStackTrace();
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "ConversationActivity", "SaveBitmap", e.toString());
            } finally {
                try {
                    if (out != null) {
                        out.flush();
                        out.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return "Success";
        }

        @Override
        protected void onPostExecute(String s) {
//            if (s.equals("Success")) {
//                try {
//                    File file = new File(fileUri.getPath());
//                    file.delete();
//                    if (file.exists()) {
//                        file.getCanonicalFile().delete();
//                        if (file.exists()) {
//                            getApplicationContext().deleteFile(file.getName());
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
            callback.execute("", "");
        }
    }


    class AsyncGetFAQConversation extends AsyncTask<String, Void, String> {
        Context context;
        ProgressDialog mProgressDialog;
        boolean isAdded = false;
//        ArrayList<String> listMsg = new ArrayList<String>();
//        ArrayList<String> listFrom = new ArrayList<String>();
//        ArrayList<String> listType = new ArrayList<String>();
//        ArrayList<String> listMobRefId = new ArrayList<String>();
//        ArrayList<String> isShow = new ArrayList<String>();

        public AsyncGetFAQConversation(Context context) {
            this.context = context;
        }

        public AsyncGetFAQConversation() {
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            if (isFirstTime) {
//                isFirstTime = false;
                messageList.clear();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Initiating conversation..\nPlease wait..");
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }
        }


        @SuppressLint({"WrongThread", "Range"})
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            String strResponse = "";
            try {
                strResponse = getFAQConversationFromServer();
                if (strResponse != null && !strResponse.equalsIgnoreCase("{}")
                        && !strResponse.equalsIgnoreCase("null")
                        && !strResponse.equalsIgnoreCase("")) {
                    isFirstTime = false;
                    JSONObject result = new JSONObject(strResponse);
                    JSONArray jsonArray = result.getJSONArray("Table");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                        if (jsonObject.has("ResponseCode") && jsonObject.getString("ResponseCode").equals("0")) {
                            String isShow = "Y";
                            if (jsonObject.getString("RespStatus").equals("R"))
                                isShow = "N";
                            MessageContent content = new MessageContent();
                            MessageContent messageContent = new MessageContent();
                            messageContent.setQuesId(jsonObject.getString("QuesId"));
                            messageContent.setQuesDesc(jsonObject.getString("QuesDesc"));
                            messageContent.setTrainerId(jsonObject.getString("TrainerId"));
                            messageContent.setCandidateId(jsonObject.getString("CandidateId"));
                            messageContent.setRespDesc(jsonObject.getString("RespDesc"));
                            messageContent.setRespDtime(jsonObject.getString("RespDtime"));
                            messageContent.setRespFrom(jsonObject.getString("RespFrom"));
                            messageContent.setIsClosed(jsonObject.getString("isClosed"));
                            messageContent.setMobRefId(jsonObject.getString("MobRefId"));
                            messageContent.setRespStatus(jsonObject.getString("RespStatus"));
                            messageContent.setIsShow(isShow);
                            messageContent.setShowProgress(false);
                            messageContent.setIsAttachment(jsonObject.getString("isAttachment"));
//                            messageContent.setmFile(jsonObject.getString("mFile"));
                            messageContent.setmFileName(jsonObject.getString("mFileName"));
                            messageContent.setmFilePath(jsonObject.getString("mFilePath"));
                            messageContent.setmFileType(jsonObject.getString("mFileType"));
                            Cursor mCursor = DatabaseHelper.db.query(context.getResources().getString(R.string.TBL_FAQ_Conversation),
                                    null, "MobRefId=?",
                                    new String[]{"" + jsonObject.getString("MobRefId")}, null, null, "RespDtime desc", "1");
                            if (mCursor.getCount() > 0) {
                                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                                    content.setQuesId(mCursor.getString(mCursor.getColumnIndex("QuesId")));
                                    content.setQuesDesc(mCursor.getString(mCursor.getColumnIndex("QuesDesc")));
                                    content.setTrainerId(mCursor.getString(mCursor.getColumnIndex("TrainerId")));
                                    content.setCandidateId(mCursor.getString(mCursor.getColumnIndex("CandidateId")));
                                    content.setRespDesc(mCursor.getString(mCursor.getColumnIndex("RespDesc")));
                                    content.setRespDtime(mCursor.getString(mCursor.getColumnIndex("RespDtime")));
                                    content.setRespFrom(mCursor.getString(mCursor.getColumnIndex("MsgFrom")));
                                    content.setIsClosed(mCursor.getString(mCursor.getColumnIndex("isClosed")));
                                    content.setMobRefId(mCursor.getString(mCursor.getColumnIndex("MobRefId")));
                                    content.setRespStatus(mCursor.getString(mCursor.getColumnIndex("RespStatus")));
                                    content.setIsShow(mCursor.getString(mCursor.getColumnIndex("isShow")));
                                    content.setShowProgress(false);
                                    content.setIsAttachment(mCursor.getString(mCursor.getColumnIndex("isAttachment")));
//                                  content.setmFile(mCursor.getString(mCursor.getColumnIndex("mFile"));
                                    content.setmFileName(mCursor.getString(mCursor.getColumnIndex("mFileName")));
                                    content.setmFilePath(mCursor.getString(mCursor.getColumnIndex("mFilePath")));
                                    content.setmFileType(mCursor.getString(mCursor.getColumnIndex("mFileType")));
                                }
                                MessageComparator comparator = new MessageComparator();
                                if (comparator.compare(content, messageContent) != 0) {
                                    if (comparator.compare(content, messageContent) == 2) {
                                        int index = -1;
                                        for (int iii = 0; iii < messageList.size(); iii++) {
                                            if (messageList.get(iii).getMobRefId().equalsIgnoreCase(messageContent.getMobRefId())) {
                                                index = iii;
                                            }
                                        }
                                        messageList.set(index, messageContent);
                                    } else {
                                        messageList.add(messageContent);
                                    }
                                    isAdded = true;
                                }
                            } else {
                                messageList.add(messageContent);
                                isAdded = true;
                            }
                            mCursor.close();

                            DatabaseHelper.SaveConversation(LPI_FAQ_ConversationActivity.this, DatabaseHelper.db,
                                    jsonObject.getString("isClosed"), jsonObject.getString("RespDesc"),
                                    jsonObject.getString("TrainerId"), jsonObject.getString("CandidateId"),
                                    jsonObject.getString("QuesDesc"), jsonObject.getString("RespFrom"),
                                    jsonObject.getString("RespDtime").toString(), jsonObject.getString("QuesId"),
                                    jsonObject.getString("MobRefId"),
                                    /*jsonObject.getString("MsgType"), jsonObject.getString("DocLocation")*/"", "",
                                    "success", jsonObject.getString("RespStatus"), isShow,
                                    jsonObject.getString("isAttachment"), "", jsonObject.getString("mFileName"),
                                    jsonObject.getString("mFilePath"), jsonObject.getString("mFileType"));

                        }
                    }
                    if (CommonClass.isConnected(context))
                        new AsyncUpdateRespStatus(context.getApplicationContext()).execute();
                    return result.toString();
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            try {
                mMessageAdapter.notifyDataSetChanged();
                if (result != null && !result.equals("") && !result.equals("{}") && isAdded)
                    setRecyclerView();

                if (mProgressDialog != null) {
                    if (mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                setRecyclerView();
            }

        }

        private String getFAQConversationFromServer() {
            String strResp = "";
            try {
                org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                        context.getString(R.string.Exam_NAMESPACE),
                        context.getString(R.string.GetConversation));

                // property which holds input parameters
                PropertyInfo inputPI1 = new PropertyInfo();
                String strReqXML = reqBuild();
                inputPI1.setName("objXMLDoc");
                inputPI1.setValue(strReqXML);
                inputPI1.setType(String.class);
                request.addProperty(inputPI1);

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                        SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);
                HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));


                LPIEEX509TrustManager.allowAllSSL();
                androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                        context.getString(R.string.GetConversation), envelope);
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strResp = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "Conversation", "getFAQConversationFromServer", e.toString());
            }
            return strResp;
        }

        @SuppressLint("Range")
        private String reqBuild() {

            String str_XML = "";
            try {
                nu.xom.Element root = new nu.xom.Element("GetConversation");

                nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
                AppUserId.appendChild("LPIUser");

                nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
                AppPassword.appendChild("pass@123");

                nu.xom.Element AppID = new nu.xom.Element("AppID");
                AppID.appendChild("1");

                nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
                CallingMode.appendChild("Mobile");

                nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
                ServiceVersionID.appendChild("1");

                nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
                UniqRefID.appendChild("121");

                nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
                IMEINo.appendChild(CommonClass.getIMEINumber(context));

                nu.xom.Element InscType = new nu.xom.Element("InscType");
                InscType.appendChild("" + StaticVariables.crnt_InscType);

                nu.xom.Element RespDtime = new nu.xom.Element("RespDtime");
                String strRespDtime = "";
                Cursor mCursor = DatabaseHelper.db.query(context.getResources().getString(R.string.TBL_FAQ_Conversation),
                        new String[]{"RespDtime"}, "CandidateId=? and TrainerId=? and QuesId=? and MsgFrom<>?",
                        new String[]{LPI_FAQActivity.strCandidateId, LPI_FAQActivity.strTrainerId,
                                LPI_FAQActivity.strQuesID, StaticVariables.UserLoginId}, null, null, "RespDtime desc", "1");
                if (mCursor.getCount() > 0) {
                    for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                        strRespDtime = mCursor.getString(mCursor.getColumnIndex("RespDtime"));
                    }
                } else {
                    strRespDtime = "";
                }
                mCursor.close();
                RespDtime.appendChild(strRespDtime);

                root.appendChild(AppUserId);
                root.appendChild(AppPassword);
                root.appendChild(AppID);
                root.appendChild(CallingMode);
                root.appendChild(ServiceVersionID);
                root.appendChild(UniqRefID);
                root.appendChild(IMEINo);
                root.appendChild(InscType);
                root.appendChild(RespDtime);

                nu.xom.Element TrainerId = new nu.xom.Element("TrainerId");
                TrainerId.appendChild(LPI_FAQActivity.strTrainerId);
                root.appendChild(TrainerId);
                nu.xom.Element CandidateId = new nu.xom.Element("CandidateId");
                CandidateId.appendChild(LPI_FAQActivity.strCandidateId);
                root.appendChild(CandidateId);

                nu.xom.Element UserType = new nu.xom.Element("UserType");
                UserType.appendChild(StaticVariables.UserType);
                root.appendChild(UserType);

                nu.xom.Element QuesId = new nu.xom.Element("QuesId");
                QuesId.appendChild(LPI_FAQActivity.strQuesID);
                root.appendChild(QuesId);

                nu.xom.Document doc = new nu.xom.Document(root);

                OutputStream out = new ByteArrayOutputStream();
                Serializer serializer = new Serializer(System.out, "UTF-8");
                serializer.setOutputStream(out);
                serializer.setIndent(4);
                serializer.setMaxLength(64);
                serializer.write(doc);
                str_XML = out.toString();
                str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
                str_XML = str_XML.replaceAll("\n", "");
                str_XML = str_XML.replaceAll("\r", "");
                str_XML = str_XML.replaceAll("     ", "");
                str_XML = str_XML.replaceAll("         ", "");
                str_XML = str_XML.trim();
                Log.e("", "XMLStr==" + str_XML);
            } catch (IOException e) {
                Log.e("", "getFAQFromServer Error=" + e.toString());
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "AsyncSearchCandidates", "getFAQFromServer", e.toString());
            }
            return str_XML;
        }

    }
}