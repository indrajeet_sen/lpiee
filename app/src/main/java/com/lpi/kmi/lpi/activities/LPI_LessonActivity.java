package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.GetHtmlContent;
import com.lpi.kmi.lpi.activities.AsyncTasks.syncExams;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import nu.xom.Serializer;

@SuppressLint("Range")
public class LPI_LessonActivity extends AppCompatActivity implements Animation.AnimationListener, SwipeRefreshLayout.OnRefreshListener {

    RecyclerView recyclerView_lesson;
    LessonAdapter lessonAdapter;
    LinearLayoutManager linearLayoutManager;
    TextView txt_no_lesson;
    ArrayList<LessonModel> mArrayLessonList = new ArrayList<LessonModel>();
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_lpi_lesson);
        InitUI();
        UIClickListener();
//        dummyData();
//        with dummy data
        initDataFromDB();
        setAdapter();
    }

    private void InitUI() {

        try {
            DatabaseHelper.InsertActivityTracker(getApplicationContext(), DatabaseHelper.db,
                    StaticVariables.UserLoginId,
                    "A111", "A111", "Emotional Excellence Program",
                    "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                    StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                    "", "", "Pending");
        } catch (Exception e) {
            e.printStackTrace();
        }

        recyclerView_lesson = (RecyclerView) findViewById(R.id.lv_lesson);
        txt_no_lesson = (TextView) findViewById(R.id.txt_no_lesson);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_menu);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(5);
        }
        setSupportActionBar(toolbar);
        actionBarSetup();
        // SwipeRefreshLayout
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
    }

    private void UIClickListener() {
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        getSupportActionBar().setTitle("LPI Lessons");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setAdapter() {
        if (mArrayLessonList.size() > 0) {
            recyclerView_lesson.setVisibility(View.VISIBLE);
            txt_no_lesson.setVisibility(View.GONE);
            lessonAdapter = new LessonAdapter(LPI_LessonActivity.this, mArrayLessonList);
            linearLayoutManager = new LinearLayoutManager(LPI_LessonActivity.this);
            recyclerView_lesson.setLayoutManager(linearLayoutManager);
            recyclerView_lesson.setHasFixedSize(true);
            recyclerView_lesson.setAdapter(lessonAdapter);
        } else {
            recyclerView_lesson.setVisibility(View.GONE);
            txt_no_lesson.setVisibility(View.VISIBLE);
        }
    }

    void dummyData() {
        mArrayLessonList.clear();
        LessonModel model;
        model = new LessonModel("0" + 1, "Lesson_01",
                "Lesson 1: Understanding Personality- The Journey Inward",
                "6001", "6", "N", "");
        mArrayLessonList.add(model);
        model = new LessonModel("0" + 2, "Lesson_02",
                "Lesson 2: Understanding your Personality Strengths and Weaknesses-  Part I",
                "6002", "6", "N", "");
        mArrayLessonList.add(model);
        model = new LessonModel("0" + 3, "Lesson_03",
                "Lesson 3: Understanding Personality Strengths and Weaknesses- Part II",
                "6003", "6", "N", "");
        mArrayLessonList.add(model);
        model = new LessonModel("0" + 4, "Lesson_04",
                "Lesson 4: What is Emotional Intelligence (EQ) ?",
                "6004", "6", "N", "");
        mArrayLessonList.add(model);
        model = new LessonModel("0" + 5, "Lesson_05",
                "Lesson 5: Developing Emotional Excellence",
                "6005", "6", "N", "");
        mArrayLessonList.add(model);

    }

    void initDataFromDB() {
        Cursor cursor;
        try {
            if (StaticVariables.UserLoginId.startsWith("E")) {
                cursor = DatabaseHelper.db.query(getResources().getString(R.string.TblPostLicModuleData),
                        new String[]{"SectionId", "Desc01", "Desc02", "Desc03", "IsVideoAvailable", "RootSectionId", "videoLink"},
                        "RootSectionId in( ?,?) and SectionId not in (?,?) and ParentSectionId in (?,?) ",
                        new String[]{"7", "6", "6000", "7000", "6000", "7000"}, null, null, "SectionId");
            } else {
                cursor = DatabaseHelper.db.query(getResources().getString(R.string.TblPostLicModuleData),
                        new String[]{"SectionId", "Desc01", "Desc02", "Desc03", "IsVideoAvailable", "RootSectionId", "videoLink"},
                        "(RootSectionId = ? and SectionId !=? and ParentSectionId = ?)",
                        new String[]{"6", "6000", "6000"}, null, null, "SectionId");
            }
            if (cursor.getCount() > 0) {
                int i = 0;
                mArrayLessonList.clear();
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    i = i + 1;
                    LessonModel model = new LessonModel("0" + i, cursor.getString(cursor.getColumnIndex("Desc01")),
                            cursor.getString(cursor.getColumnIndex("Desc02")), cursor.getString(cursor.getColumnIndex("SectionId")),
                            cursor.getString(cursor.getColumnIndex("RootSectionId")), cursor.getString(cursor.getColumnIndex("IsVideoAvailable")),
                            cursor.getString(cursor.getColumnIndex("videoLink")));
                    mArrayLessonList.add(model);
                }
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        if (CommonClass.isConnected(LPI_LessonActivity.this) && (DatabaseHelper.CheckAttempts(LPI_LessonActivity.this,
                DatabaseHelper.db, "" + StaticVariables.crnt_ExmId, StaticVariables.UserLoginId) == 0)) {
            new syncExams(new Callback<String>() {
                @Override
                public void execute(String result, String status) {
                    if (CommonClass.isConnected(LPI_LessonActivity.this)) {
                        new GetHtmlContent(LPI_LessonActivity.this, "LPI_Lesson", new Callback() {
                            @Override
                            public void execute(Object result, String status) {
                                initDataFromDB();
                            }
                        }).execute();
                    } else {
                        initDataFromDB();
                    }
//                    dummyData();
//                    setAdapter();
//                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }, LPI_LessonActivity.this,
                    "LPI_LessonActivity", StaticVariables.UserLoginId,
                    "" + StaticVariables.crnt_ExmId, "" + StaticVariables.crnt_ExamType).execute();
//            setAdapter();
            mSwipeRefreshLayout.setRefreshing(false);
        } else {
//            setAdapter();
            mSwipeRefreshLayout.setRefreshing(false);
            Toast.makeText(LPI_LessonActivity.this, "No internet connectivity found for get lesson data.", Toast.LENGTH_SHORT);
        }
        setAdapter();
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }


    public class LessonAdapter extends RecyclerView.Adapter {

        ArrayList<LessonModel> arrLessonList;
        Context context;

        public LessonAdapter(Context context, ArrayList<LessonModel> arrLessonList) {
            this.context = context;
            this.arrLessonList = arrLessonList;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v;
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lesson_row, parent, false);
            return new VHItem(v);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            try {
                if (arrLessonList.size() > 0) {
                    if (arrLessonList.get(position) != null) {
                        VHItem token_data = (VHItem) holder;
                        token_data.txt_lesson_no.setText("" + (position + 1));
                        token_data.txt_lesson_name.setText("" + (arrLessonList.get(position).getLessonName()).replace("_"," "));
                        token_data.txt_lesson_desc.setText("" + arrLessonList.get(position).getDescription());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return arrLessonList.size();
        }

        class VHItem extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView txt_lesson_no, txt_lesson_name, txt_lesson_desc;
            CardView cardView_lesson;

            public VHItem(View itemView) {
                super(itemView);
                txt_lesson_no = (TextView) itemView.findViewById(R.id.txt_lesson_no);
                txt_lesson_name = (TextView) itemView.findViewById(R.id.txt_lesson_name);
                txt_lesson_desc = (TextView) itemView.findViewById(R.id.txt_lesson_desc);
                cardView_lesson = (CardView) itemView.findViewById(R.id.cardView_lesson);

                cardView_lesson.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {

                switch (v.getId()) {
                    case R.id.cardView_lesson: {
//                        Intent intent = new Intent(context, LPI_LessonDetailActivity.class);

                        try {
                            String ActCode = "A111";
                            if (getAdapterPosition()>9){
                                ActCode = ActCode+getAdapterPosition();
                            }else {
                                ActCode = ActCode+"0"+getAdapterPosition();
                            }
                            DatabaseHelper.InsertActivityTracker(getApplicationContext(), DatabaseHelper.db,
                                    StaticVariables.UserLoginId,
                                    ""+ActCode, "A111", arrLessonList.get(getAdapterPosition()).getLessonName(),
                                    "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                                    StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                                    "", "", "Pending");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (StaticVariables.UserLoginId.startsWith("E")
                        && arrLessonList.get(getAdapterPosition()).getSectionId().startsWith("7")) {
                            Intent intent = new Intent(context, LPI_LessonDetailActivity.class);
                            intent.putExtra("SectionId", arrLessonList.get(getAdapterPosition()).getSectionId());
                            intent.putExtra("Lesson", arrLessonList.get(getAdapterPosition()).getLessonName());
                            intent.putExtra("LessonDesc", arrLessonList.get(getAdapterPosition()).getDescription());
                            context.startActivity(intent);
                        } else {
                            Intent intent = new Intent(context, LPI_Lesson_Trip.class);
                            intent.putExtra("SectionId", arrLessonList.get(getAdapterPosition()).getSectionId());
                            intent.putExtra("Lesson", arrLessonList.get(getAdapterPosition()).getLessonName());
                            intent.putExtra("LessonDesc", arrLessonList.get(getAdapterPosition()).getDescription());
                            context.startActivity(intent);
                        }
                    }
                    break;
                }
            }
        }
    }

    public class LessonModel {

        public LessonModel() {
        }

        String lessonId = "", lessonName, description = "";
        String SectionId = "", RootSectionId = "", IsVideoAvailable = "", videoLink = "";

        public LessonModel(String lessonId, String lessonName, String description, String SectionId, String RootSectionId,
                           String IsVideoAvailable, String videoLink) {
            this.lessonId = lessonId;
            this.lessonName = lessonName;
            this.description = description;
            this.SectionId = SectionId;
            this.RootSectionId = RootSectionId;
            this.IsVideoAvailable = IsVideoAvailable;
            this.videoLink = videoLink;
        }

        public String getLessonId() {
            return lessonId;
        }

        public String getSectionId() {
            return SectionId;
        }

        public String getRootSectionId() {
            return RootSectionId;
        }

        public String getIsVideoAvailable() {
            return IsVideoAvailable;
        }

        public String getVideoLink() {
            return videoLink;
        }

        public String getLessonName() {
            return lessonName;
        }

        public String getDescription() {
            return description;
        }

    }

    public class GettingLessonsList extends AsyncTask<String, String, JSONObject> {

        public Context context;
        ProgressDialog mProgressDialog;

        public GettingLessonsList(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Getting Lesson Details...\nPlease wait..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            // TODO Auto-generated method stub
            getCandidatesFromServer();
            JSONObject objRet = new JSONObject();
            return objRet;
        }

        @Override
        protected void onCancelled() {
            mProgressDialog.dismiss();
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            try {
                boolean isSuccess = false;
                JSONObject objJsonObject = new JSONObject("");
                JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");
                for (int index = 0; index < objJsonArray.length(); index++) {
                    JSONObject jsonobject = objJsonArray.getJSONObject(index);
//                    String str_login = jsonobject.getString("Message");
                }
            } catch (Exception e) {
                // TODO: handle exception
                Log.e("", "error=" + e.toString());
            }
            mProgressDialog.dismiss();
        }

        private String getCandidatesFromServer() {

            String strResp = "";
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.GetCandidateSearch));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            String strReqXML = reqBuild();
            inputPI1.setName("objXMLDoc");
            try {
                inputPI1.setValue(strReqXML);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));

            try {
                LPIEEX509TrustManager.allowAllSSL();
                androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                        context.getString(R.string.GetCandidateSearch), envelope);
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strResp = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return strResp;
        }

        private String reqBuild() {

            String str_XML = "";
            nu.xom.Element root = new nu.xom.Element("GetCandidateSearch");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(context));

            nu.xom.Element InscType = new nu.xom.Element("InscType");
            InscType.appendChild("" + StaticVariables.crnt_InscType);

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
            root.appendChild(InscType);

            nu.xom.Element Name = new nu.xom.Element("Name");
            Name.appendChild("");
            root.appendChild(Name);

            nu.xom.Element Mobile = new nu.xom.Element("Mobile");
            Mobile.appendChild("");
            root.appendChild(Mobile);

            nu.xom.Document doc = new nu.xom.Document(root);
            try {
                OutputStream out = new ByteArrayOutputStream();
                Serializer serializer = new Serializer(System.out, "UTF-8");
                serializer.setOutputStream(out);
                serializer.setIndent(4);
                serializer.setMaxLength(64);
                serializer.write(doc);
                str_XML = out.toString();
                str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
                str_XML = str_XML.replaceAll("\n", "");
                str_XML = str_XML.replaceAll("\r", "");
                str_XML = str_XML.replaceAll("     ", "");
                str_XML = str_XML.replaceAll("         ", "");
                str_XML = str_XML.trim();
                Log.e("", "XMLStr==" + str_XML);
            } catch (IOException e) {
                Log.e("", "GetCandidateSearch Error=" + e.toString());
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "AsyncSearchCandidates", "GetCandidateSearch", e.toString());
            }
            return str_XML;
        }
    }
}
