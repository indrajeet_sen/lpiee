package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.GetHtmlContent;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;

import cn.jzvd.JzvdStd;
import nu.xom.Serializer;

@SuppressLint("Range")
public class LPI_LessonDetailActivity extends AppCompatActivity {

    ArrayList<String> arrayList = new ArrayList<>();
    int index = 0;
    TextView txt_lesson_name, txt_lesson_desc, txt_view_more, txt_lesson_details, txt_lesson_gen_desc, txt_lesson_adv_desc;
    LinearLayout lin_more, lin_desc, lin_vid2;
    boolean isVideoAvailable = false;
    Button btn_go_ques;
    cn.jzvd.JzvdStd videoplayer, videoplayer2;
    RelativeLayout web_layout, hybrid_layout;
    WebView contentView;
    Button btn_ques;
    ProgressBar progressBar;
    static private String strSectionId = "";
    static private String Lesson = "", LessonDesc = "", Trip = "";
    String rootDIR = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_lpi_lesson_description);

        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            rootDIR = Environment.getExternalStoragePublicDirectory(Environment.MEDIA_SHARED).toString() + StaticVariables.LessonStoragePath + "LPILessons";
        } else {
            rootDIR = Environment.getExternalStorageDirectory() + "/Documents" + StaticVariables.LessonStoragePath + "LPILessons";
        }*/
        rootDIR = getExternalFilesDir(Environment.MEDIA_SHARED) + StaticVariables.LessonStoragePath + "LPILessons";
        InitUI();
        UIClickListener();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void InitUI() {

        try {
            strSectionId = getIntent().getExtras().getString("SectionId");
            Lesson = getIntent().getExtras().getString("Lesson");
            LessonDesc = getIntent().getExtras().getString("LessonDesc");
            Trip = getIntent().getExtras().getString("Trip");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_menu);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(5);
        }
        setSupportActionBar(toolbar);
        actionBarSetup();

        try {
            DatabaseHelper.InsertActivityTracker(getApplicationContext(), DatabaseHelper.db,
                    StaticVariables.UserLoginId,
                    strSectionId, "A111", Trip,
                    "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                    StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                    "", "", "Pending");
        } catch (Exception e) {
            e.printStackTrace();
        }

        web_layout = (RelativeLayout) findViewById(R.id.web_layout);
        hybrid_layout = (RelativeLayout) findViewById(R.id.hybrid_layout);
        hybrid_layout.setVisibility(View.GONE);
        web_layout.setVisibility(View.VISIBLE);
        contentView = (WebView) findViewById(R.id.contentView);
        txt_lesson_name = (TextView) findViewById(R.id.txt_lesson_name);
        txt_lesson_desc = (TextView) findViewById(R.id.txt_lesson_desc);
        txt_lesson_details = (TextView) findViewById(R.id.txt_lesson_details);
        txt_lesson_gen_desc = (TextView) findViewById(R.id.txt_lesson_gen_desc);
        txt_lesson_adv_desc = (TextView) findViewById(R.id.txt_lesson_adv_desc);
        txt_view_more = (TextView) findViewById(R.id.txt_view_more);
        lin_more = (LinearLayout) findViewById(R.id.lin_more);
        lin_vid2 = (LinearLayout) findViewById(R.id.lin_vid2);
        lin_desc = (LinearLayout) findViewById(R.id.lin_desc);
        btn_go_ques = (Button) findViewById(R.id.btn_go_ques);
        btn_ques = (Button) findViewById(R.id.btn_ques);
        videoplayer = (cn.jzvd.JzvdStd) findViewById(R.id.videoplayer);
        videoplayer2 = (cn.jzvd.JzvdStd) findViewById(R.id.videoplayer2);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        progressBar.setMax(100);
        initWebView();

        if (StaticVariables.UserLoginId.startsWith("E")
                && strSectionId.startsWith("7")) {
            hybrid_layout.setVisibility(View.VISIBLE);
            web_layout.setVisibility(View.GONE);
        } else {
            hybrid_layout.setVisibility(View.GONE);
            web_layout.setVisibility(View.VISIBLE);
            InitContent();
        }

//        contentView.loadData(str, mimeType, encoding);
//        File file = new File(mFile);
//        contentView.loadData("file:///" + file.getAbsolutePath());
//        contentView.loadUrl("file:///android_asset/html/" + mFile);


        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txt_lesson_gen_desc.setText(Html.fromHtml(str, Html.FROM_HTML_MODE_COMPACT));
        } else {
            txt_lesson_gen_desc.setText(Html.fromHtml(str));
        }
        if (strSectionId.equalsIgnoreCase("11")){
            hybrid_layout.setVisibility(View.VISIBLE);
            web_layout.setVisibility(View.GONE);
        }else if (strSectionId.equalsIgnoreCase("12")){
            hybrid_layout.setVisibility(View.GONE);
            web_layout.setVisibility(View.VISIBLE);
            initWebView();
            contentView.loadData(str,mimeType,encoding);
        }else {
            hybrid_layout.setVisibility(View.VISIBLE);
            web_layout.setVisibility(View.GONE);
        }*/


//        initDataFromDB();
        /*if (isVideoAvailable) {
            videoplayer.setVisibility(View.VISIBLE);
            initVideo();
        } else {
            videoplayer.setVisibility(View.GONE);
        }*/
    }

    private void InitContent() {
        final String mFile = Lesson + "_" + Trip + ".html";

        File file = CommonClass.CheckHtml(LPI_LessonDetailActivity.this, rootDIR, mFile);
        if (file.exists()) {
            if (file.getName().equalsIgnoreCase("404.html")) {
                if (CommonClass.isConnected(LPI_LessonDetailActivity.this)) {
                    new GetHtmlContent(LPI_LessonDetailActivity.this, "LPI_Lesson", new Callback() {
                        @Override
                        public void execute(Object result, String status) {
                            LoadWebView(LPI_LessonDetailActivity.this, mFile);
                        }
                    }).execute();
                }
            } else {
                LoadWebView(LPI_LessonDetailActivity.this, mFile);
            }
        } else {
            if (CommonClass.isConnected(LPI_LessonDetailActivity.this)) {
                new GetHtmlContent(LPI_LessonDetailActivity.this, "LPI_Lesson", new Callback() {
                    @Override
                    public void execute(Object result, String status) {
                        LoadWebView(LPI_LessonDetailActivity.this, mFile);
                    }
                }).execute();
            } else {
                LoadWebView(LPI_LessonDetailActivity.this, mFile);
            }
        }
    }

    private void initWebView() {

        setUpWebViewDefaults(contentView);

        contentView.setWebViewClient(new MyWebViewClient());
        contentView.setVerticalScrollBarEnabled(false);
        contentView.setHorizontalScrollBarEnabled(false);
        contentView.clearCache(true);
        contentView.clearHistory();
        contentView.clearView();
        contentView.reload();

        /*contentView.getSettings().setLoadWithOverviewMode(true);
        contentView.getSettings().setJavaScriptEnabled(true);
        contentView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        //improve webView performance
//        WebSettings webSettings = contentView.getSettings();
        contentView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        contentView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        contentView.getSettings().setAppCacheEnabled(true);
        contentView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        contentView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        contentView.getSettings().setSavePassword(true);
        contentView.getSettings().setSaveFormData(true);
        contentView.getSettings().setEnableSmoothTransition(true);
        contentView.getSettings().setAllowContentAccess(true);
        contentView.getSettings().setAllowFileAccess(true);

        contentView.getSettings().setDomStorageEnabled(true);
        contentView.getSettings().setSupportMultipleWindows(true);
        contentView.getSettings().setUseWideViewPort(true);
        contentView.getSettings().setLoadWithOverviewMode(true);
        contentView.getSettings().setSupportZoom(false);
        contentView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        contentView.getSettings().setBuiltInZoomControls(false);
        contentView.getSettings().setDisplayZoomControls(false);
        contentView.getSettings().setPluginState(WebSettings.PluginState.ON);
        contentView.setVerticalScrollBarEnabled(false);
        contentView.setHorizontalScrollBarEnabled(false);
        contentView.clearCache(true);
        contentView.clearHistory();
        contentView.clearView();
        contentView.reload();*/

        contentView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    WebView webView = (WebView) v;
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_BACK:
                            if (webView.canGoBack()) {
                                webView.goBack();
                                return true;
                            }
                            break;
                    }
                }
                return false;
            }
        });
//
        contentView.setWebChromeClient(new WebChromeClient() {

            //                ProgressDialog mProgress;;

            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                Log.d("TAG", "onPermissionRequest");
                LPI_LessonDetailActivity.this.runOnUiThread(new Runnable() {
                    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void run() {
//                        if(request.getOrigin().toString().equals("https://apprtc-m.appspot.com/")) {
                            request.grant(request.getResources());
//                        } else {
//                            request.deny();
//                        }
                    }
                });
            }


            @Override
            public void onProgressChanged(WebView view, int newProgress) {
//                    if (progressBar == null) {
////                        mProgress = new ProgressDialog(context);
////                        mProgress.show();
//                        pr
//                    }
                progressBar.setProgress(newProgress);
//                    mProgress.setMessage("Loading " + String.valueOf(newProgress) + "%");
                if (newProgress == 100) {
                    progressBar.setVisibility(View.GONE);
//                        mProgress.dismiss();
//                        mProgress = null;
                }
            }
        });
    }

    private void setUpWebViewDefaults(WebView webView) {
        WebSettings settings = webView.getSettings();

        // Enable Javascript
        settings.setJavaScriptEnabled(true);
        settings.setAllowContentAccess(true);
        settings.setAllowFileAccess(true);
        // Use WideViewport and Zoom out if there is no viewport defined
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);

        // Enable pinch to zoom without the zoom buttons
//        settings.setBuiltInZoomControls(true);

        settings.setSupportZoom(false);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setBuiltInZoomControls(false);
        settings.setDisplayZoomControls(false);
        // Allow use of Local Storage
        settings.setDomStorageEnabled(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        // AppRTC requires third party cookies to work
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptThirdPartyCookies(contentView, true);
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return true;
        }
    }

    private void LoadWebView(Context context, String mFile) {
        File file = CommonClass.CheckHtml(context, rootDIR, mFile);
        progressBar.setVisibility(View.VISIBLE);
        contentView.setVisibility(View.VISIBLE);
        if (file != null) {
            contentView.loadUrl("file:///" + file.getAbsolutePath());
        } else {
//            webViewBasic.loadDataWithBaseURL(null, htmlHeader + "<p>Page Not Found</p>"+ htmlFooter, "text/html", "utf-8", null);
            String str = "<!doctype html>\n" +
                    "<html>\n" +
                    "<head>\n" +
                    "<title>Lesson Not Found</title>\n" +
                    "\t<meta charset=\"utf-8\">\n" +
                    "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                    "  <style>\n" +
                    "*{\n" +
                    "    transition: all 0.6s;\n" +
                    "}\n" +
                    "\n" +
                    "html {\n" +
                    "    height: 100%;\n" +
                    "}\n" +
                    "\n" +
                    "body{\n" +
                    "    font-family: 'Lato', sans-serif;\n" +
                    "    color: #888;\n" +
                    "    margin: 0;\n" +
                    "}\n" +
                    "\n" +
                    "#main{\n" +
                    "    display: table;\n" +
                    "    width: 100%;\n" +
                    "    height: 100vh;\n" +
                    "    text-align: center;\n" +
                    "}\n" +
                    "\n" +
                    ".fof{\n" +
                    "\t  display: table-cell;\n" +
                    "\t  vertical-align: middle;\n" +
                    "}\n" +
                    "\n" +
                    ".fof h1{\n" +
                    "\t  font-size: 50px;\n" +
                    "\t  display: inline-block;\n" +
                    "\t  padding-right: 12px;\n" +
                    "\t  animation: type .5s alternate infinite;\n" +
                    "}\n" +
                    "\n" +
                    "@keyframes type{\n" +
                    "\t  from{box-shadow: inset -3px 0px 0px #888;}\n" +
                    "\t  to{box-shadow: inset -3px 0px 0px transparent;}\n" +
                    "}\n" +
                    "</style>\n" +
                    "</head>\n" +
                    "\n" +
                    "<body>\n" +
                    "<div id=\"main\">\n" +
                    "    \t<div class=\"fof\">\n" +
                    "        \t\t<h1>Error 404</h1><br/>\n" +
                    "\t\t\t\t<h3>Oops! The page you are looking for dose not exists.</h3><br/>\n" +
                    "\t\t\t\t<h5>“Sometimes you have to get lost before you find what you are looking for.”</h5>\n" +
                    "    \t</div>\n" +
                    "</div>\n" +
                    "</body>\n" +
                    "</html>";
            contentView.loadDataWithBaseURL(null, "<html><body><p>Page Not Found</p></html></body>", "text/html", "utf-8", null);
        }

//        ContentActivity.scrollButtons.setVisibility(View.VISIBLE);
//        ContentActivity.scrollButtons.bringToFront();
    }

    @SuppressLint("NewApi")
    public String CheckLocalHtml(Context context, String fName) {
        try {
            File mFile, mDir, content;
            mDir = new File(rootDIR);
            if (mDir.exists()) {
                mFile = new File(mDir, fName);
                if (mFile.exists()) {
                    StringBuilder contentBuilder = new StringBuilder();
                    BufferedReader in = new BufferedReader(new FileReader(mFile));
                    String str = "";
                    while ((str = in.readLine()) != null) {
                        contentBuilder.append(str);
                        contentBuilder.append("\n");
                    }
                    in.close();
                    str = contentBuilder.toString();
//                    s = str.substring(str.indexOf("<body>") + 6,
//                            str.indexOf("</body>") - 6);
//                    s = s.replace("../Images/","Images/");
                    str = str.replace("\\", "/");
//                    str = str.replace("//<![CDATA[","//<![CDATA[\n");
//                    str = str.replace("//]]>","//]]>\n");

                    Log.e("", "html string =" + str);
                    return str;
                } else {
                    Toast.makeText(context, "Page not found", Toast.LENGTH_SHORT).show();
                    return "Error 404 Page Not Found";
                }
            } else {
                Toast.makeText(context, "Directory not found", Toast.LENGTH_SHORT).show();
                return "Error 404 Page Not Found";
            }
        } catch (Exception e) {
            Log.e("ReadHTML", "Error=" + e.toString());
            return "Error 404 Page Not Found";
        }
    }


    @SuppressLint("Range")
    void initDataFromDB() {
        Cursor cursor = DatabaseHelper.db.query(getResources().getString(R.string.TblPostLicModuleData),
                new String[]{"SectionId", "Desc01", "Desc02", "Desc03", "IsVideoAvailable", "RootSectionId", "videoLink", "GenDesc01", "AdvDesc01"},
                "SectionId = ?", new String[]{strSectionId}, null, null, "SectionId");
        if (cursor.getCount() > 0) {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {

                getSupportActionBar().setTitle(cursor.getString(cursor.getColumnIndex("Desc01")));
                txt_lesson_name.setText(cursor.getString(cursor.getColumnIndex("Desc01")));
                txt_lesson_desc.setText(cursor.getString(cursor.getColumnIndex("Desc02")));
                if (cursor.getString(cursor.getColumnIndex("IsVideoAvailable")) != null &&
                        cursor.getString(cursor.getColumnIndex("IsVideoAvailable")).trim().equalsIgnoreCase("Y")) {
                    isVideoAvailable = true;
                    arrayList.add(getString(R.string.Exam_NAMESPACE) + cursor.getString(cursor.getColumnIndex("videoLink")));
//                    arrayList.add("https://archive.org/download/BigBuckBunny_328/BigBuckBunny_512kb.mp4");
                }
                txt_lesson_details.setText(cursor.getString(cursor.getColumnIndex("RootSectionId")));
                if (cursor.getString(cursor.getColumnIndex("GenDesc01")) != null &&
                        !cursor.getString(cursor.getColumnIndex("GenDesc01")).equalsIgnoreCase("null") &&
                        cursor.getString(cursor.getColumnIndex("GenDesc01")).length() > 0) {
                    txt_lesson_gen_desc.setText(cursor.getString(cursor.getColumnIndex("GenDesc01")));
                    txt_lesson_gen_desc.setVisibility(View.VISIBLE);
                }
                if (cursor.getString(cursor.getColumnIndex("AdvDesc01")) != null &&
                        !cursor.getString(cursor.getColumnIndex("AdvDesc01")).equalsIgnoreCase("null") &&
                        cursor.getString(cursor.getColumnIndex("AdvDesc01")).length() > 0) {
                    txt_lesson_adv_desc.setText(cursor.getString(cursor.getColumnIndex("AdvDesc01")));
                    txt_lesson_adv_desc.setVisibility(View.VISIBLE);
                }
            }
        }
        cursor.close();
    }

    private void initVideo() {
        try {
            cn.jzvd.JzvdStd jzVideoPlayerStandard = (cn.jzvd.JzvdStd) findViewById(R.id.videoplayer);
            jzVideoPlayerStandard.setUp(arrayList.get(index)
                    , "https://www.youtube.com/watch?v=KJSSyWDRlLM");
            cn.jzvd.JzvdStd jzVideoPlayerStandard1 = (cn.jzvd.JzvdStd) findViewById(R.id.videoplayer2);
            jzVideoPlayerStandard1.setUp(arrayList.get(index)
                    , "https://www.youtube.com/watch?v=PtVyiDH85Vg");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void UIClickListener() {

        btn_go_ques.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StaticVariables.crnt_ExmId = new BigInteger(strSectionId);
                StaticVariables.crnt_QueId = 1;
                StaticVariables.TblNameForCount = DatabaseHelper.TotalQueCount(LPI_LessonDetailActivity.this,
                        DatabaseHelper.db, StaticVariables.crnt_ExmId);
                StaticVariables.crnt_ExmTypeName = "" + txt_lesson_name.getText().toString().trim();
                StaticVariables.crnt_ExmName = "" + txt_lesson_name.getText().toString().trim();
                StaticVariables.crnt_ExamType = 6;
                StaticVariables.crnt_ExamDuration = "30";
                StaticVariables.allowedAttempts = "1";
                StaticVariables.IsPracticeExam = "N";
                StaticVariables.IsAllQuesMendatory = "Y";
                Intent i = new Intent(LPI_LessonDetailActivity.this, MCQ_QuestionActivity.class);
                i.putExtra("initFrom", "LPI_LessonDetailActivity");
                try {
                    i.putExtra("SectionId", strSectionId);
                    i.putExtra("Lesson", Lesson);
                    i.putExtra("LessonDesc", LessonDesc);
                    i.putExtra("Trip", Trip);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                LPI_LessonDetailActivity.this.finish();
            }
        });

        btn_ques.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LPI_LessonDetailActivity.this, LPILessonQuizActivity.class);
                i.putExtra("initFrom", "LPI_LessonDetailActivity");
                try {
                    i.putExtra("SectionId", strSectionId);
                    i.putExtra("Lesson", Lesson);
                    i.putExtra("LessonDesc", LessonDesc);
                    i.putExtra("Trip", Trip);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                LPI_LessonDetailActivity.this.finish();

                /*StaticVariables.crnt_ExmId = new BigInteger(strSectionId);
                StaticVariables.crnt_QueId = 1;
                StaticVariables.TblNameForCount = DatabaseHelper.TotalQueCount(LPI_LessonDetailActivity.this,
                        DatabaseHelper.db, StaticVariables.crnt_ExmId);
                StaticVariables.crnt_ExmTypeName = "" + txt_lesson_name.getText().toString().trim();
                StaticVariables.crnt_ExmName = "" + txt_lesson_name.getText().toString().trim();
                StaticVariables.crnt_ExamType = 6;
                StaticVariables.crnt_ExamDuration = "30";
                StaticVariables.IsPracticeExam = "N";
                StaticVariables.IsAllQuesMendatory = "Y";
                StaticVariables.allowedAttempts = "1";
                Intent i = new Intent(LPI_LessonDetailActivity.this, MCQ_QuestionActivity.class);
                i.putExtra("initFrom", "LPI_LessonDetailActivity");
                try {
                    i.putExtra("SectionId", strSectionId);
                    i.putExtra("Lesson", Lesson);
                    i.putExtra("LessonDesc", LessonDesc);
                    i.putExtra("Trip", Trip);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                LPI_LessonDetailActivity.this.finish();*/
            }
        });

        /*txt_view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (lin_more.getVisibility() == View.GONE) {
                    lin_more.setVisibility(View.VISIBLE);
                    txt_view_more.setText("View Less");
                    startStopVideo(true);
                } else {
                    lin_more.setVisibility(View.GONE);
                    txt_view_more.setText("View More");
                    startStopVideo(false);
                }
            }
        });*/
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {

        try {
            getSupportActionBar().setTitle("Hey There " + StaticVariables.UserName);
            getSupportActionBar().setSubtitle("You are on " + Trip.replace("_", " ") + " of " + Lesson.replace("_", " "));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        } catch (Exception e) {
            getSupportActionBar().setTitle("Hey There " + StaticVariables.UserName);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }


    @SuppressLint("Range")
    @Override
    public void onBackPressed() {
        /*if (JZVideoPlayer.backPress()) {
            return;
        }*/
        Cursor cursor = DatabaseHelper.db.query(getResources().getString(R.string.TblPostLicModuleData),
                null, "SectionId=?", new String[]{strSectionId}, null, null, null, "1");
        if (cursor != null && cursor.getCount() > 0) {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                strSectionId = cursor.getString(cursor.getColumnIndex("ParentSectionId"));
            }
        }

        Intent intent = new Intent(LPI_LessonDetailActivity.this, LPI_Lesson_Trip.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("initFrom", "LPI_LessonDetailActivity");
        intent.putExtra("SectionId", strSectionId);
        intent.putExtra("Lesson", Lesson);
        intent.putExtra("LessonDesc", LessonDesc);
        intent.putExtra("Trip", Trip);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        LPI_LessonDetailActivity.this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (contentView != null) {
                contentView.resumeTimers();
                contentView.onResume();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        try {
            contentView.onPause();
            contentView.pauseTimers();
            cn.jzvd.JzvdStd.releaseAllVideos();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        try {
            contentView.loadUrl("about:blank");
            contentView.reload();
            contentView.destroy();
            contentView = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class TokenAdapter extends RecyclerView.Adapter {

        ArrayList<LessonModel> arrLessonList;
        Context context;

        public TokenAdapter(Context context, ArrayList<LessonModel> arrLessonList) {
            this.context = context;
            this.arrLessonList = arrLessonList;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v;
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lesson_row, parent, false);
            return new VHItem(v);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            try {
                if (arrLessonList.size() > 0) {
                    if (arrLessonList.get(position) != null) {
                        VHItem token_data = (VHItem) holder;
                        token_data.txt_lesson_no.setText("" + (position + 1));
                        token_data.txt_lesson_name.setText("" + arrLessonList.get(position).getLessonName());
                        token_data.txt_lesson_desc.setText("" + arrLessonList.get(position).getDescription());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return arrLessonList.size();
        }

        class VHItem extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView txt_lesson_no, txt_lesson_name, txt_lesson_desc;
            CardView cardView_lesson;

            public VHItem(View itemView) {
                super(itemView);
                txt_lesson_no = (TextView) itemView.findViewById(R.id.txt_lesson_no);
                txt_lesson_name = (TextView) itemView.findViewById(R.id.txt_lesson_name);
                txt_lesson_desc = (TextView) itemView.findViewById(R.id.txt_lesson_desc);
                cardView_lesson = (CardView) itemView.findViewById(R.id.cardView_lesson);

                cardView_lesson.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {

                switch (v.getId()) {
                    case R.id.cardView_lesson: {
                    }
                    break;
                }
            }
        }
    }

    public class LessonModel {

        public LessonModel() {
        }

        String lessonId = "", lessonName, description = "";
        boolean isVideo = false;

        public LessonModel(String lessonId, String lessonName, String description, boolean isVideo) {
            this.lessonId = lessonId;
            this.lessonName = lessonName;
            this.description = description;
            this.isVideo = isVideo;
        }

        public String getLessonId() {
            return lessonId;
        }

        public String getLessonName() {
            return lessonName;
        }

        public String getDescription() {
            return description;
        }

        public boolean isVideo() {
            return isVideo;
        }
    }

    public class GettingLessonsList extends AsyncTask<String, String, JSONObject> {

        public Context context;
        ProgressDialog mProgressDialog;

        public GettingLessonsList(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Getting Lesson Details...\nPlease wait..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            // TODO Auto-generated method stub
            getCandidatesFromServer();
            JSONObject objRet = new JSONObject();
            return objRet;
        }

        @Override
        protected void onCancelled() {
            mProgressDialog.dismiss();
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            try {
                boolean isSuccess = false;
                JSONObject objJsonObject = new JSONObject("");
                JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");
                for (int index = 0; index < objJsonArray.length(); index++) {
                    JSONObject jsonobject = objJsonArray.getJSONObject(index);
//                    String str_login = jsonobject.getString("Message");
                }
            } catch (Exception e) {
                // TODO: handle exception
                Log.e("", "error=" + e.toString());
            }
            mProgressDialog.dismiss();
        }

        private String getCandidatesFromServer() {

            String strResp = "";
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.GetCandidateSearch));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            String strReqXML = reqBuild();
            inputPI1.setName("objXMLDoc");
            try {
                inputPI1.setValue(strReqXML);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));

            try {
                LPIEEX509TrustManager.allowAllSSL();
                androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                        context.getString(R.string.GetCandidateSearch), envelope);
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strResp = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return strResp;
        }

        private String reqBuild() {

            String str_XML = "";
            nu.xom.Element root = new nu.xom.Element("GetCandidateSearch");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(context));

            nu.xom.Element InscType = new nu.xom.Element("InscType");
            InscType.appendChild("" + StaticVariables.crnt_InscType);

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
            root.appendChild(InscType);

            nu.xom.Element Name = new nu.xom.Element("Name");
            Name.appendChild("");
            root.appendChild(Name);

            nu.xom.Element Mobile = new nu.xom.Element("Mobile");
            Mobile.appendChild("");
            root.appendChild(Mobile);

            nu.xom.Document doc = new nu.xom.Document(root);
            try {
                OutputStream out = new ByteArrayOutputStream();
                Serializer serializer = new Serializer(System.out, "UTF-8");
                serializer.setOutputStream(out);
                serializer.setIndent(4);
                serializer.setMaxLength(64);
                serializer.write(doc);
                str_XML = out.toString();
                str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
                str_XML = str_XML.replaceAll("\n", "");
                str_XML = str_XML.replaceAll("\r", "");
                str_XML = str_XML.replaceAll("     ", "");
                str_XML = str_XML.replaceAll("         ", "");
                str_XML = str_XML.trim();
                Log.e("", "XMLStr==" + str_XML);
            } catch (IOException e) {
                Log.e("", "GetCandidateSearch Error=" + e.toString());
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "AsyncSearchCandidates", "GetCandidateSearch", e.toString());
            }
            return str_XML;
        }
    }
}