package com.lpi.kmi.lpi.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;

@SuppressLint("Range")
public class LPI_Lesson_Intro extends AppCompatActivity {

    Button begin;
    WebView webViewVideo;
    ProgressBar progsVid;
    TextView txt_noInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_lpi__lesson__intro);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_menu);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(5);
        }
        setSupportActionBar(toolbar);
        actionBarSetup();

        if (ActivityCompat.checkSelfPermission(LPI_Lesson_Intro.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LPI_Lesson_Intro.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }

        begin = (Button) findViewById(R.id.btn_go_lessons);
        webViewVideo = (WebView) findViewById(R.id.webViewVideo);
        progsVid = (ProgressBar) findViewById(R.id.progsVid);
        txt_noInternet = (TextView) findViewById(R.id.txt_noInternet);


        /*JZVideoPlayerStandard jzVideoPlayerStandard = (JZVideoPlayerStandard) findViewById(R.id.videoplayer);
        jzVideoPlayerStandard.setUp("https://www.youtube.com/embed/Ebs6lk9oXG8", JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL);*/

        setVideoToUI();

        begin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LPI_Lesson_Intro.this, LPI_LessonActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (webViewVideo != null) {
                webViewVideo.resumeTimers();
                webViewVideo.onResume();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        try {
            webViewVideo.loadUrl("about:blank");
            webViewVideo.reload();
            webViewVideo.destroy();
            webViewVideo = null;
            clearWebViewAllCache();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    static void clearWebViewAllCache() {
        try {
            WebStorage.getInstance().deleteAllData();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
        } catch (Exception ignore) {
            //ignore.printStackTrace();
        }

    }

    @Override
    public void onPause() {
        try {
            webViewVideo.onPause();
            webViewVideo.pauseTimers();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();
    }

    private void setVideoToUI() {
        progsVid.setVisibility(ProgressBar.VISIBLE);
        progsVid.bringToFront();
        webViewVideo.setVisibility(View.VISIBLE);

        WebSettings settings = webViewVideo.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(false);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setDomStorageEnabled(true);
        webViewVideo.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webViewVideo.setScrollbarFadingEnabled(true);

//            settings.setJavaScriptCanOpenWindowsAutomatically(true);
            /*settings.setPluginState(WebSettings.PluginState.ON);
            settings.setLoadWithOverviewMode(true);
            settings.setUseWideViewPort(true);
            settings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);*/
            /*webView.clearCache(true);
            webView.clearHistory();
            settings.setMediaPlaybackRequiresUserGesture(false);*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(0);
            webViewVideo.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webViewVideo.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webViewVideo.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        webViewVideo.clearCache(true);
        webViewVideo.clearHistory();
//            webView.setWebViewClient(new WebViewClient());
        webViewVideo.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
            }

            @Override
            public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback) {
            }

            @Override
            public void onHideCustomView() {
                //do stuff
            }
        });
        webViewVideo.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
//                    progsVid.setVisibility(ProgressBar.VISIBLE);
//                    webView.setVisibility(View.INVISIBLE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url != null && !url.equalsIgnoreCase("https://www.youtube.com/signin?context=popup&next=https%3A%2F%2Fwww.youtube.com%2Fpost_login&feature=wl_button")) {
                    progsVid.setVisibility(ProgressBar.VISIBLE);
                    webViewVideo.setVisibility(View.VISIBLE);
                }
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);
//                    progsVid.setVisibility(ProgressBar.GONE);
//                    webView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progsVid.setVisibility(ProgressBar.GONE);
                webViewVideo.setVisibility(View.VISIBLE);
                super.onPageFinished(view, url);
            }
        });
        if (CommonClass.isConnected(LPI_Lesson_Intro.this)) {
            progsVid.setVisibility(View.VISIBLE);
            webViewVideo.setVisibility(View.VISIBLE);
            txt_noInternet.setVisibility(View.GONE);
            String url = "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/Hwi4cmsHWaQ\" frameborder=\"0\" allowfullscreen></iframe>";
            String strBaseURL = "https://img.youtube.com/vi/MK50cGWG0VU/0.jpg";
            webViewVideo.loadDataWithBaseURL(strBaseURL, url, "text/html; charset=utf-8", "utf-8", null);
        } else {
            txt_noInternet.setVisibility(View.VISIBLE);
            progsVid.setVisibility(View.GONE);
            webViewVideo.setVisibility(View.INVISIBLE);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        getSupportActionBar().setTitle("LPI Practice Lessons");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
