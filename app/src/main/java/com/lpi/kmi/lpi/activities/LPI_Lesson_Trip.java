package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.syncExams;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import nu.xom.Serializer;

@SuppressLint("Range")
public class LPI_Lesson_Trip extends AppCompatActivity implements Animation.AnimationListener, SwipeRefreshLayout.OnRefreshListener {

    RecyclerView recyclerView_lesson_trip;
    LPI_Lesson_Trip.lessonAdapter_trip lessonAdapter_trip;
    LinearLayoutManager linearLayoutManager_trip;
    TextView txt_no_lesson_trip;
    ArrayList<LPI_Lesson_Trip.LessonModel> mArrayLessonList = new ArrayList<LPI_Lesson_Trip.LessonModel>();
    SwipeRefreshLayout mSwipeRefreshLayout_trip;
    static private String strSectionId = "";
    static private String Lesson = "", LessonDesc = "", Trip = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_lpi__lesson__trip);

        InitUI();
        UIClickListener();
//        dummyData();
        initDataFromDB();
        setAdapter();

    }

    private void InitUI() {

        try {
            Lesson = getIntent().getExtras().getString("Lesson");
            LessonDesc = getIntent().getExtras().getString("LessonDesc");
            strSectionId = getIntent().getExtras().getString("SectionId");
        } catch (Exception e) {
            e.printStackTrace();
        }

        recyclerView_lesson_trip = (RecyclerView) findViewById(R.id.trip_lv_lesson);
        txt_no_lesson_trip = (TextView) findViewById(R.id.trip_txt_no_lesson);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_menu);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(5);
        }
        setSupportActionBar(toolbar);
        actionBarSetup();
        // SwipeRefreshLayout
        mSwipeRefreshLayout_trip = (SwipeRefreshLayout) findViewById(R.id.trip_swipe_container);
        mSwipeRefreshLayout_trip.setOnRefreshListener(this);
        mSwipeRefreshLayout_trip.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);


    }

    private void UIClickListener() {
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        getSupportActionBar().setTitle(Lesson.replace("_", " "));
        getSupportActionBar().setSubtitle(LessonDesc.replace("_", " "));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setAdapter() {
        if (mArrayLessonList.size() > 0) {
            recyclerView_lesson_trip.setVisibility(View.VISIBLE);
            txt_no_lesson_trip.setVisibility(View.GONE);
            lessonAdapter_trip = new lessonAdapter_trip(LPI_Lesson_Trip.this, mArrayLessonList);
            linearLayoutManager_trip = new LinearLayoutManager(LPI_Lesson_Trip.this);
            recyclerView_lesson_trip.setLayoutManager(linearLayoutManager_trip);
            recyclerView_lesson_trip.setHasFixedSize(true);
            recyclerView_lesson_trip.setAdapter(lessonAdapter_trip);
        } else {
            recyclerView_lesson_trip.setVisibility(View.GONE);
            txt_no_lesson_trip.setVisibility(View.VISIBLE);
        }
    }

    void dummyData() {
        if (Lesson.equalsIgnoreCase("Lesson_01")) {
            mArrayLessonList.clear();
            LPI_Lesson_Trip.LessonModel model;
            model = new LPI_Lesson_Trip.LessonModel("0" + 1, "Trip_01",
                    "Personality Measures", "600101", "6", "Y",
                    "https://www.youtube.com/watch?v=KJSSyWDRlLM");
            mArrayLessonList.add(model);
            model = new LPI_Lesson_Trip.LessonModel("0" + 2, "Trip_02",
                    "Introduction to LPI", "600102", "6", "Y",
                    "videoLink");
            mArrayLessonList.add(model);
            model = new LPI_Lesson_Trip.LessonModel("0" + 3, "Trip_03",
                    "LPI Profiles at a Glance", "600103", "6", "Y",
                    "videoLink");
            mArrayLessonList.add(model);
            model = new LPI_Lesson_Trip.LessonModel("0" + 4, "Trip_04",
                    " Using LPI scores  to identify Personality Types", "600104", "6", "IsVideoAvailable",
                    "videoLink");
            mArrayLessonList.add(model);
            model = new LPI_Lesson_Trip.LessonModel("0" + 5, "Trip_05",
                    "Why conflicts occur?", "600105", "6", "Y",
                    "videoLink");
            mArrayLessonList.add(model);
        }

        if (Lesson.equalsIgnoreCase("Lesson_02")) {
            mArrayLessonList.clear();
            LPI_Lesson_Trip.LessonModel model;
            model = new LPI_Lesson_Trip.LessonModel("0" + 1, "Trip_01",
                    "Characteristic Traits of the 5 Main LPI Profiles", "600201",
                    "6", "Y", "https://www.youtube.com/watch?v=KJSSyWDRlLM");
            mArrayLessonList.add(model);
            model = new LPI_Lesson_Trip.LessonModel("0" + 2, "Trip_02",
                    "Discovering  The Openness Individual", "600202",
                    "6", "Y", "videoLink");
            mArrayLessonList.add(model);
            model = new LPI_Lesson_Trip.LessonModel("0" + 3, "Trip_03",
                    "Discovering  The Neutral Individual", "600203",
                    "6", "Y", "videoLink");
            mArrayLessonList.add(model);
        }

        if (Lesson.equalsIgnoreCase("Lesson_03")) {
            mArrayLessonList.clear();
            LPI_Lesson_Trip.LessonModel model;
            model = new LPI_Lesson_Trip.LessonModel("0" + 1, "Trip_01",
                    "Discovering  The Analytical Individual", "600301",
                    "6", "Y", "https://www.youtube.com/watch?v=KJSSyWDRlLM");
            mArrayLessonList.add(model);
            model = new LPI_Lesson_Trip.LessonModel("0" + 2, "Trip_02",
                    "Discovering The Relational Individual", "600302",
                    "6", "Y", "videoLink");
            mArrayLessonList.add(model);
            model = new LPI_Lesson_Trip.LessonModel("0" + 3, "Trip_03",
                    "Discovering The Decisive Individual", "600303",
                    "6", "Y", "videoLink");
            mArrayLessonList.add(model);
        }

        if (Lesson.equalsIgnoreCase("Lesson_04")) {
            mArrayLessonList.clear();
            LPI_Lesson_Trip.LessonModel model;
            model = new LPI_Lesson_Trip.LessonModel("0" + 1, "Trip_01",
                    "What is Emotional Intelligence (EQ)?", "600401",
                    "6", "Y", "https://www.youtube.com/watch?v=KJSSyWDRlLM");
            mArrayLessonList.add(model);
            model = new LPI_Lesson_Trip.LessonModel("0" + 2, "Trip_02",
                    "The Four Capabilities that define our EQ", "600402",
                    "1", "Y", "videoLink");
            mArrayLessonList.add(model);
            model = new LPI_Lesson_Trip.LessonModel("0" + 3, "Trip_03",
                    "Understanding Perception, Interpersonal and Intrapersonal Skills", "600403",
                    "6", "Y", "videoLink");
            mArrayLessonList.add(model);
        }

        if (Lesson.equalsIgnoreCase("Lesson_05")) {
            mArrayLessonList.clear();
            LPI_Lesson_Trip.LessonModel model;
            model = new LPI_Lesson_Trip.LessonModel("0" + 1, "Trip_01",
                    "Emotional Intelligence and Emotional Excellence", "600501",
                    "6", "Y", "https://www.youtube.com/watch?v=KJSSyWDRlLM");
            mArrayLessonList.add(model);
            model = new LPI_Lesson_Trip.LessonModel("0" + 2, "Trip_02",
                    "LPI Holistic Model and the Well Spirit", "600502",
                    "6", "Y", "videoLink");
            mArrayLessonList.add(model);
            model = new LPI_Lesson_Trip.LessonModel("0" + 3, "Trip_03",
                    "The  Importance of a Cleansed Conscience", "600503",
                    "6", "Y", "videoLink");
            mArrayLessonList.add(model);
            model = new LPI_Lesson_Trip.LessonModel("0" + 4, "Trip_04",
                    " The Wellness of the Whole Person", "600504",
                    "6", "Y", "videoLink");
            mArrayLessonList.add(model);
            model = new LPI_Lesson_Trip.LessonModel("0" + 5, "Trip_05",
                    "CBT and LPI", "600505", "6", "Y",
                    "videoLink");
            mArrayLessonList.add(model);
        }

    }

    void initDataFromDB() {
        Cursor cursor;
        try {
            cursor = DatabaseHelper.db.query(getResources().getString(R.string.TblPostLicModuleData),
                    new String[]{"SectionId", "Desc01", "Desc02", "Desc03", "IsVideoAvailable", "RootSectionId", "videoLink"},
                    "(RootSectionId = ? and ParentSectionId = ?)",
                    new String[]{"6", strSectionId}, null, null, "SectionId");
            if (cursor.getCount() > 0) {
                int i = 0;
                mArrayLessonList.clear();
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    i = i + 1;
                    @SuppressLint("Range") LPI_Lesson_Trip.LessonModel model = new LPI_Lesson_Trip.LessonModel("0" + i, cursor.getString(cursor.getColumnIndex("Desc01")),
                            cursor.getString(cursor.getColumnIndex("Desc02")), cursor.getString(cursor.getColumnIndex("SectionId")),
                            cursor.getString(cursor.getColumnIndex("RootSectionId")), cursor.getString(cursor.getColumnIndex("IsVideoAvailable")),
                            cursor.getString(cursor.getColumnIndex("videoLink")));
                    mArrayLessonList.add(model);
                }
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout_trip.setRefreshing(true);
        if (CommonClass.isConnected(LPI_Lesson_Trip.this) && (DatabaseHelper.CheckAttempts(LPI_Lesson_Trip.this,
                DatabaseHelper.db, "" + StaticVariables.crnt_ExmId, StaticVariables.UserLoginId) == 0)) {
            new syncExams(new Callback<String>() {
                @Override
                public void execute(String result, String status) {
                    initDataFromDB();
//                    dummyData();
                    setAdapter();
                    mSwipeRefreshLayout_trip.setRefreshing(false);
                }
            }, LPI_Lesson_Trip.this,
                    "LPI_Lesson_Trip", StaticVariables.UserLoginId,
                    "" + StaticVariables.crnt_ExmId, "" + StaticVariables.crnt_ExamType).execute();
        } else {
            mSwipeRefreshLayout_trip.setRefreshing(false);
            Toast.makeText(LPI_Lesson_Trip.this, "No internet connectivity found for get lesson data.", Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }


    public class lessonAdapter_trip extends RecyclerView.Adapter {

        ArrayList<LPI_Lesson_Trip.LessonModel> arrLessonList;
        Context context;

        public lessonAdapter_trip(Context context, ArrayList<LPI_Lesson_Trip.LessonModel> arrLessonList) {
            this.context = context;
            this.arrLessonList = arrLessonList;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v;
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lesson_row, parent, false);
            return new LPI_Lesson_Trip.lessonAdapter_trip.VHItem(v);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            try {
                if (arrLessonList.size() > 0) {
                    if (arrLessonList.get(position) != null) {
                        LPI_Lesson_Trip.lessonAdapter_trip.VHItem token_data = (LPI_Lesson_Trip.lessonAdapter_trip.VHItem) holder;
                        token_data.txt_lesson_no.setText("" + (position + 1));
                        token_data.txt_lesson_name.setText("" + (arrLessonList.get(position).getLessonName()).replace("_", " "));
                        token_data.txt_lesson_desc.setText("" + arrLessonList.get(position).getDescription());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return arrLessonList.size();
        }

        class VHItem extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView txt_lesson_no, txt_lesson_name, txt_lesson_desc, txt_lesson_progress;
            CardView cardView_lesson;

            public VHItem(View itemView) {
                super(itemView);
                txt_lesson_no = (TextView) itemView.findViewById(R.id.txt_lesson_no);
                txt_lesson_name = (TextView) itemView.findViewById(R.id.txt_lesson_name);
                txt_lesson_desc = (TextView) itemView.findViewById(R.id.txt_lesson_desc);
                txt_lesson_progress = (TextView) itemView.findViewById(R.id.txt_lesson_progress);
                cardView_lesson = (CardView) itemView.findViewById(R.id.cardView_lesson);
                txt_lesson_progress.setVisibility(View.GONE);
                cardView_lesson.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {

                switch (v.getId()) {
                    case R.id.cardView_lesson: {
                        strSectionId = arrLessonList.get(getAdapterPosition()).getSectionId();
                        Trip = arrLessonList.get(getAdapterPosition()).getLessonName();

                        try {
                            DatabaseHelper.InsertActivityTracker(getApplicationContext(), DatabaseHelper.db,
                                    StaticVariables.UserLoginId,
                                    strSectionId, "A111", "Clicked on "+Trip,
                                    "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                                    StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                                    "", "", "Pending");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Intent intent = new Intent(context, LPI_LessonDetailActivity.class);
                        intent.putExtra("SectionId", strSectionId);
                        intent.putExtra("RootSectionId", arrLessonList.get(getAdapterPosition()).getRootSectionId());
                        intent.putExtra("Lesson", Lesson);
                        intent.putExtra("LessonDesc", LessonDesc);
                        intent.putExtra("Trip", Trip);
                        context.startActivity(intent);
                        ((Activity) context).finish();
                    }
                    break;
                }
            }
        }
    }

    public class LessonModel {

        public LessonModel() {
        }

        String lessonId = "", lessonName, description = "";
        String SectionId = "", RootSectionId = "", IsVideoAvailable = "", videoLink = "";

        public LessonModel(String lessonId, String lessonName, String description, String SectionId, String RootSectionId,
                           String IsVideoAvailable, String videoLink) {
            this.lessonId = lessonId;
            this.lessonName = lessonName;
            this.description = description;
            this.SectionId = SectionId;
            this.RootSectionId = RootSectionId;
            this.IsVideoAvailable = IsVideoAvailable;
            this.videoLink = videoLink;
        }

        public String getLessonId() {
            return lessonId;
        }

        public String getSectionId() {
            return SectionId;
        }

        public String getRootSectionId() {
            return RootSectionId;
        }

        public String getIsVideoAvailable() {
            return IsVideoAvailable;
        }

        public String getVideoLink() {
            return videoLink;
        }

        public String getLessonName() {
            return lessonName;
        }

        public String getDescription() {
            return description;
        }

    }

    public class GettingLessonsList extends AsyncTask<String, String, JSONObject> {

        public Context context;
        ProgressDialog mProgressDialog;

        public GettingLessonsList(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Getting Lesson Details...\nPlease wait..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            // TODO Auto-generated method stub
            getCandidatesFromServer();
            JSONObject objRet = new JSONObject();
            return objRet;
        }

        @Override
        protected void onCancelled() {
            mProgressDialog.dismiss();
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            try {
                boolean isSuccess = false;
                JSONObject objJsonObject = new JSONObject("");
                JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");
                for (int index = 0; index < objJsonArray.length(); index++) {
                    JSONObject jsonobject = objJsonArray.getJSONObject(index);
//                    String str_login = jsonobject.getString("Message");
                }
            } catch (Exception e) {
                // TODO: handle exception
                Log.e("", "error=" + e.toString());
            }
            mProgressDialog.dismiss();
        }

        private String getCandidatesFromServer() {

            String strResp = "";
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.GetCandidateSearch));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            String strReqXML = reqBuild();
            inputPI1.setName("objXMLDoc");
            try {
                inputPI1.setValue(strReqXML);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));

            try {
                LPIEEX509TrustManager.allowAllSSL();
                androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                        context.getString(R.string.GetCandidateSearch), envelope);
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strResp = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return strResp;
        }

        private String reqBuild() {

            String str_XML = "";
            nu.xom.Element root = new nu.xom.Element("GetCandidateSearch");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(context));

            nu.xom.Element InscType = new nu.xom.Element("InscType");
            InscType.appendChild("" + StaticVariables.crnt_InscType);

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
            root.appendChild(InscType);

            nu.xom.Element Name = new nu.xom.Element("Name");
            Name.appendChild("");
            root.appendChild(Name);

            nu.xom.Element Mobile = new nu.xom.Element("Mobile");
            Mobile.appendChild("");
            root.appendChild(Mobile);

            nu.xom.Document doc = new nu.xom.Document(root);
            try {
                OutputStream out = new ByteArrayOutputStream();
                Serializer serializer = new Serializer(System.out, "UTF-8");
                serializer.setOutputStream(out);
                serializer.setIndent(4);
                serializer.setMaxLength(64);
                serializer.write(doc);
                str_XML = out.toString();
                str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
                str_XML = str_XML.replaceAll("\n", "");
                str_XML = str_XML.replaceAll("\r", "");
                str_XML = str_XML.replaceAll("     ", "");
                str_XML = str_XML.replaceAll("         ", "");
                str_XML = str_XML.trim();
                Log.e("", "XMLStr==" + str_XML);
            } catch (IOException e) {
                Log.e("", "GetCandidateSearch Error=" + e.toString());
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "AsyncSearchCandidates", "GetCandidateSearch", e.toString());
            }
            return str_XML;
        }
    }
}
