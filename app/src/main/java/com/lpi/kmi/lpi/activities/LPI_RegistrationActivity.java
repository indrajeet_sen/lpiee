package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.exifinterface.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import com.google.android.material.textfield.TextInputLayout;
import com.lpi.kmi.lpi.BuildConfig;
import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.AsyncCandidateRegistration;
import com.lpi.kmi.lpi.activities.AsyncTasks.AsyncGetTokenDetails;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CircleImageView;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.CompressImage;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.MarshmallowPermission;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.stringcare.library.SC;
import com.yalantis.ucrop.UCrop;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nu.xom.Serializer;

@SuppressLint("Range")
public class LPI_RegistrationActivity extends AppCompatActivity {

    static final int CUSTOM_DIALOG_ID = 0;
    CircleImageView circleImageView;
    EditText input_FName, input_email, input_mobile, input_nationality,
            input_profession;
    //input_LName,input_DOB,input_password,
//    RadioGroup rdogrp_gender;
//    RadioButton rdo_male, rdo_female;
    TextInputLayout inputLayout;
    EditText input_reference;
    static String mCurrentPhotoPath;
    Button btn_Submit;
    MarshmallowPermission marshmallowPermission;
    List<String> arrProfession = new ArrayList<>();
    List<String> Country_list = new ArrayList<>();
    List<String> Country_code_list = new ArrayList<>();
    ListView dialog_ListView;
    boolean isNotVisible = true;
    String strToolbarTitle = "", candidate_id = "", mobile_rec_id;
    byte[] img_byteArray = null;
    //    LinearLayout lin_password;
    String strMobileCountryCode = "";
    String strAppLink = "";
    boolean modificationFlag = false;
    Dialog dialog = null;
    String MobRefId;
    private Uri fileUri;
    public static int balanceToken = -1;
    public static String strToken = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_lpi_registration);
        MobRefId = UUID.randomUUID().toString();
        initUI();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_menu);
        toolbar.setElevation(5);
        setSupportActionBar(toolbar);
        actionBarSetup();
        UIListener();

    }

    @Override
    protected void onResume() {
        super.onResume();
        initDropDown();
    }

    private void actionBarSetup() {
        try {
            if (strToolbarTitle != null && strToolbarTitle.trim().length() > 1 &&
                    (strToolbarTitle.equalsIgnoreCase("LPI_SearchActivity")
                            || strToolbarTitle.equalsIgnoreCase("ParticipantHomeActivity"))) {
                getSupportActionBar().setTitle("Modification");
            } else {
                getSupportActionBar().setTitle("Registration");
            }
        } catch (Exception e) {
            e.printStackTrace();
            getSupportActionBar().setTitle("Registration");
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void UIListener() {
        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!marshmallowPermission.checkPermissionForExternalStorage()) {
                    marshmallowPermission.requestPermissionForExternalStorage();
                } else if (!marshmallowPermission.checkPermissionForCamera()) {
                    marshmallowPermission.requestPermissionForCamera();
                } else {
                    ProfileImage();
                }
            }
        });
//        viewPass.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (isNotVisible) {
//                    isNotVisible = false;
//                    input_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
//                    input_password.setSelection(input_password.length());
//                    viewPass.setImageDrawable(getResources().getDrawable(R.drawable.ic_hide_pass));
//                } else {
//                    isNotVisible = true;
//                    input_password.setTransformationMethod(PasswordTransformationMethod.getInstance());
//                    input_password.setSelection(input_password.length());
//                    viewPass.setImageDrawable(getResources().getDrawable(R.drawable.ic_view_pass));
//                }
//            }
//        });

        input_nationality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearFocus();
                if (Country_list.size() > 0)
                    showDialog(CUSTOM_DIALOG_ID);
            }
        });
        input_profession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearFocus();
                if (arrProfession.size() > 0)
                    showDialog(222);
            }
        });

        btn_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidate()) {
                    saveInDB();
                    try {
                        DatabaseHelper.InsertActivityTracker(getApplication(), DatabaseHelper.db, StaticVariables.UserLoginId,
                                "A8003", "A8003", "Registration Submit",
                                "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                                StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                                "", "", "Pending");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        /*circleImageView.setLongClickable(true);
        circleImageView.setClickable(true);
        circleImageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(
                        LPI_RegistrationActivity.this);
                alert.setTitle("Alert!!");
                alert.setMessage("Are you sure to remove profile picture!");
                alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        circleImageView.setImageDrawable(getResources().getDrawable(R.drawable.profile_blank_m));
                        try {
                            Drawable drawable = null;
                            drawable = getResources().getDrawable(R.drawable.profile_blank_m);
                            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                            img_byteArray = stream.toByteArray();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                });
                alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
                return true;
            }
        });*/
    }

    private void saveInDB() {
        try {
            String strGender = "";
//            if (rdo_male.isChecked()) {
//                strGender = "M";
//            } else if (rdo_female.isChecked()) {
//                strGender = "F";
//            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String curDate = sdf.format(Calendar.getInstance().getTime());
            if (DatabaseHelper.db == null) {
                SQLiteDatabase.loadLibs(getApplicationContext());
                DatabaseHelper.db = DatabaseHelper.getInstance(getApplicationContext()).
                        getWritableDatabase(SC.reveal(CommonClass.DBPassword));
            }
//            StaticVariables.UserLoginId = "70011354";
            String Parent = "", refCode = "";
            /*try {
                android.database.Cursor cursor = DatabaseHelper.db.query(getResources().getString(R.string.Tbl_TokenMst), null, "UserId=? and TokenType=? and isActive=?",
                        new String[]{StaticVariables.UserLoginId, "P", "Y"}, null, null, null);
                if (cursor != null && cursor.getCount() > 0) {
                    for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                        refCode = cursor.getString(cursor.getColumnIndex("Token"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }*/
            refCode = (strToken);
            if (refCode == null || refCode.trim().equalsIgnoreCase("")) {
                refCode = input_reference.getText().toString().trim();
            }

            if (StaticVariables.UserType.equals("T")) {
                Parent = StaticVariables.UserLoginId;
            } else {
                Parent = StaticVariables.ParentId;
            }

            int i = DatabaseHelper.RegisterCandidate(LPI_RegistrationActivity.this, DatabaseHelper.db, candidate_id,
                    input_FName.getText().toString().trim(), "", "",
                    "", input_email.getText().toString().trim(),
                    input_mobile.getText().toString().trim(), "pass@123",
                    input_profession.getText().toString().trim(), input_nationality.getText().toString().trim(),
                    img_byteArray, Parent, curDate, curDate, MobRefId, strMobileCountryCode, strAppLink, modificationFlag,
                    "pending", refCode);
            if (i > 0) {
                if (CommonClass.isConnected(LPI_RegistrationActivity.this)) {
                    if (modificationFlag) {
//                        if (StaticVariables.UserType.equals("C")) {
//                            StaticVariables.UserName = input_FName.getText().toString().trim();
//                            ContentValues values = new ContentValues();
//                            values.put("FirstName", "" + input_FName.getText().toString().trim());
//                            values.put("LastName", "" );
//                            DatabaseHelper.db.update(getResources().getString(R.string.TBL_iCandidate), values,
//                                    "CandidateId=?", new String[]{StaticVariables.UserLoginId});
//                        }
                        new AsyncCandidateRegistration(LPI_RegistrationActivity.this,
                                LPI_RegistrationActivity.this, candidate_id, input_email.getText().toString(),
                                getResources().getString(R.string.ModifyCandidate), "ForModification").execute();

                    } else if (strToolbarTitle.equals("LoginActivity")) {
                        new AsyncCandidateRegistration(LPI_RegistrationActivity.this,
                                LPI_RegistrationActivity.this, MobRefId,
                                input_email.getText().toString().trim(),
                                getResources().getString(R.string.RegisterCandidateEmail)).execute();
                    } else {
                        new AsyncCandidateRegistration(LPI_RegistrationActivity.this,
                                LPI_RegistrationActivity.this, MobRefId,
                                input_email.getText().toString().trim(),
                                getResources().getString(R.string.RegisterCandidate)).execute();
                    }
                } else {
                    AlertDialog.Builder builder = CommonClass.DialogOpen(LPI_RegistrationActivity.this,
                            "Offline Candidate Registration", "No Network available.\nCandidate data save successfully.");
                    builder.setCancelable(false);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            onBackPressed();
                            LPI_RegistrationActivity.this.finish();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getTokenDetails() {
        try {
            android.database.Cursor cursor = DatabaseHelper.db.query(getResources().getString(R.string.Tbl_TokenMst), null,
                    "UserId=? and TokenType=? and isActive=?",
                    new String[]{StaticVariables.UserLoginId, "P", "Y"}, null, null, null);
            if (cursor != null && cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    int total = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TotalCount")));
                    int used = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TokenUsed")));
                    if (total - used > 0) {
                        balanceToken = total - used;
                /*      TokenModel model = new TokenModel(cursor.getString(cursor.getColumnIndex("TokenType")),
                        cursor.getString(cursor.getColumnIndex("ValidFrom")),
                        cursor.getString(cursor.getColumnIndex("ValidTo")),*/

                        strToken = cursor.getString(cursor.getColumnIndex("Token"));
                        input_reference.setText(strToken);
                    }
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isValidate() {
        try {
            clearFocus();

//            if (input_LName == null || input_LName.getText().toString().trim().length() < 1) {
//                ToastMsg(getResources().getString(R.string.last_name_field_required));
//                input_LName.setError(getResources().getString(R.string.error_field_required));
//                input_LName.requestFocus();
//                return false;
//            }
//            if (rdogrp_gender == null || rdogrp_gender.getCheckedRadioButtonId() == -1) {
//                ToastMsg(getResources().getString(R.string.gender_field_required));
//                return false;
//            }
//            if (input_DOB == null || input_DOB.getText().toString().trim().length() < 1) {
//                ToastMsg(getResources().getString(R.string.DOB_field_required));
//                input_DOB.setError(getResources().getString(R.string.error_field_required));
//                return false;
//            }
//            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//            String curDate = sdf.format(Calendar.getInstance().getTime());
//            if (input_DOB != null && input_DOB.getText().toString().trim().equalsIgnoreCase(curDate.trim())) {
//                ToastMsg(getResources().getString(R.string.DOB_curr_field_required));
//                input_DOB.setError(getResources().getString(R.string.DOB_curr_field_required));
//                return false;
//            }
            if (input_FName == null || input_FName.getText().toString().trim().length() < 1) {
                ToastMsg(getResources().getString(R.string.first_name_field_required));
                input_FName.setError(getResources().getString(R.string.error_field_required));
                input_FName.requestFocus();
                return false;
            }
            if (input_email == null || input_email.getText().toString().trim().length() < 1) {
                ToastMsg(getResources().getString(R.string.email_field_required));
                input_email.setError(getResources().getString(R.string.error_field_required));
                input_email.requestFocus();
                return false;
            }
            String eMailValid = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            Pattern Email_pattern = Pattern.compile(eMailValid);
            Matcher Email1_matcher = Email_pattern.matcher(input_email.getText().toString());
            boolean IsValidEmail = Email1_matcher.matches();
            if (!IsValidEmail) {
                ToastMsg(getResources().getString(R.string.email_field_valid));
                input_email.setError(getResources().getString(R.string.email_field_valid));
                input_email.requestFocus();
                return false;
            }

            if (input_mobile == null || input_mobile.getText().toString().trim().length() < 1) {
                ToastMsg(getResources().getString(R.string.mobile_field_required));
                input_mobile.setError(getResources().getString(R.string.error_field_required));
                input_mobile.requestFocus();
                return false;
            }
//            if (Integer.parseInt(input_mobile.getText().toString())==0){
//                ToastMsg(getResources().getString(R.string.mobile_valid_field_required));
//                input_mobile.setError(getResources().getString(R.string.mobile_valid_field_required));
//                input_mobile.requestFocus();
//                return false;
//            }
            if (input_mobile == null || input_mobile.getText().toString().matches("[0]+")) {
                ToastMsg(getResources().getString(R.string.mobile_valid_field_required));
                input_mobile.setError(getResources().getString(R.string.mobile_valid_field_required));
                input_mobile.requestFocus();
                return false;
            }
//            if (!modificationFlag && (input_password == null || input_password.getText().toString().trim().length() < 1)) {
//                ToastMsg(getResources().getString(R.string.password_field_required));
//                input_password.setError(getResources().getString(R.string.error_field_required));
//                input_password.requestFocus();
//                return false;
//            }
//            if (!modificationFlag && (input_password.getText().toString().trim().length() < 7)) {
//                ToastMsg(getResources().getString(R.string.password_field_valid));
//                input_password.setError(getResources().getString(R.string.password_field_valid));
//                input_password.requestFocus();
//                return false;
//            }
            if (input_nationality == null || input_nationality.getText().toString().trim().length() < 1) {
                ToastMsg(getResources().getString(R.string.nationality_field_required));
                input_nationality.setError(getResources().getString(R.string.error_field_required));
                return false;
            }
            if (input_profession == null || input_profession.getText().toString().trim().length() < 1) {
                ToastMsg(getResources().getString(R.string.profession_field_required));
                input_profession.setError(getResources().getString(R.string.error_field_required));
                return false;
            }

            if ((input_reference.getVisibility() == View.VISIBLE)
                    && (input_reference == null || input_reference.getText().toString().trim().length() < 1)) {
                ToastMsg(getResources().getString(R.string.ref_field_required));
                input_reference.setError(getResources().getString(R.string.error_field_required));
                input_reference.requestFocus();
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            ToastMsg("Something went wrong,\nPlease make sure that all fields are correct");
            return false;
        }
        return true;
    }

    void ToastMsg(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

    void clearFocus() {
        try {
            input_FName.clearFocus();
//            input_LName.clearFocus();
//            input_DOB.clearFocus();
            input_email.clearFocus();
            input_mobile.clearFocus();
            if (input_reference.getVisibility() == View.VISIBLE) {
                input_reference.clearFocus();
            }
//            input_password.clearFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private File getOutputMediaFile() {
        try {
            File mediaStorageDir = new File(LPI_RegistrationActivity.this.getExternalFilesDir(null).getAbsolutePath() +
                    "/AgentManager/");
            if (!mediaStorageDir.exists()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    try {
                        Files.createDirectory(Paths.get(mediaStorageDir.getAbsolutePath()));
                    } catch (IOException e) {
                        e.printStackTrace();
                        mediaStorageDir.mkdirs();
                    } catch (Exception e) {
                        e.printStackTrace();
                        mediaStorageDir.mkdirs();
                    }
                } else {
                    mediaStorageDir.mkdirs();
                }
            }

            File file = new File(mediaStorageDir, "/.nomedia");
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            File mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "profile.jpg");
            mCurrentPhotoPath = "file:" + mediaFile.getAbsolutePath();
            return mediaFile;
        } catch (Exception e) {
            Log.e("", "getOutputMediaFile err " + e.toString());
            return null;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.trainer_menu, menu);
        return true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
//        new initCountry(LPI_RegistrationActivity.this).execute();
    }

    private void initUI() {
        strToken = "";
        if (DatabaseHelper.db == null) {
            SQLiteDatabase.loadLibs(getApplicationContext());
            DatabaseHelper.db = DatabaseHelper.getInstance(getApplicationContext()).
                    getWritableDatabase(SC.reveal(CommonClass.DBPassword));
        }
        try {
            strToolbarTitle = getIntent().getExtras().getString("initFrom");
            try {
                candidate_id = getIntent().getExtras().getString("candidate_id");
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                mobile_rec_id = getIntent().getExtras().getString("mobile_rec_id");
                if (mobile_rec_id == null || mobile_rec_id.equals("")) {
                    mobile_rec_id = MobRefId;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (candidate_id == null || candidate_id.trim().equals(null) ||
                candidate_id.trim().equalsIgnoreCase("null")) {
            candidate_id = "";
        }
        marshmallowPermission = new MarshmallowPermission(this);
        circleImageView = (CircleImageView) findViewById(R.id.circleImageView);
        input_FName = (EditText) findViewById(R.id.input_FName);
//        input_LName = (EditText) findViewById(R.id.input_LName);
//        input_DOB = (EditText) findViewById(R.id.input_DOB);
        input_email = (EditText) findViewById(R.id.input_email);
        input_email.setEnabled(false);
        input_mobile = (EditText) findViewById(R.id.input_mobile);
        input_nationality = (EditText) findViewById(R.id.input_nationality);
        input_profession = (EditText) findViewById(R.id.input_profession);
//        input_password = (EditText) findViewById(R.id.input_password);
//        rdo_male = (RadioButton) findViewById(R.id.rdo_male);
//        rdo_female = (RadioButton) findViewById(R.id.rdo_female);
//        rdogrp_gender = (RadioGroup) findViewById(R.id.rdogrp_gender);
//        imgCalender = (ImageView) findViewById(R.id.imgCalender);
//        viewPass = (ImageView) findViewById(R.id.viewPass);
        inputLayout = (TextInputLayout) findViewById(R.id.input_layout_ref);
        input_reference = (EditText) findViewById(R.id.input_reference);

        btn_Submit = (Button) findViewById(R.id.btn_Submit);


        inputLayout.setVisibility(View.GONE);
        input_reference.setVisibility(View.GONE);


        initDropDown();

//        lin_password = (LinearLayout) findViewById(R.id.lin_password);
        //calender
//        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
//
//            @Override
//            public void onDateSet(DatePicker view, int year, int monthOfYear,
//                                  int dayOfMonth) {
//                // TODO Auto-generated method stub
//                myCalendar.set(Calendar.YEAR, year);
//                myCalendar.set(Calendar.MONTH, monthOfYear);
//                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//                updateLabel();
//            }
//
//        };
//        imgCalender.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                /*DatePickerDialog dialog = new DatePickerDialog(LPI_RegistrationActivity.this, date, myCalendar
//                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                        myCalendar.get(Calendar.DAY_OF_MONTH));
//                dialog.getDatePicker().setMaxDate(new Date().getTime());
//                dialog.show();
//                clearFocus();*/
//
//                DatePickerDialog dialog = new DatePickerDialog(LPI_RegistrationActivity.this,
//                        android.R.style.Theme_Holo_Light_Dialog, date, myCalendar
//                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                        myCalendar.get(Calendar.DAY_OF_MONTH));
//                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//                dialog.getDatePicker().setMaxDate(new Date().getTime());
//                dialog.show();
//                clearFocus();
//            }
//        });

        //initDummyArray();


        if (strToolbarTitle != null && strToolbarTitle.trim().length() > 1 &&
                (strToolbarTitle.equalsIgnoreCase("LPI_SearchActivity")
                        || strToolbarTitle.equalsIgnoreCase("ParticipantHomeActivity"))
                && (candidate_id != null && candidate_id.trim().length() > 0)) {
//            setModifiedDataToUI(); mobile_rec_id
            modificationFlag = true;
            if (CommonClass.isConnected(LPI_RegistrationActivity.this)) {
                new AsyncGetCandidateDetails(LPI_RegistrationActivity.this).execute();
            } else {
                setModifiedDataToUI();
            }
            try {
                DatabaseHelper.InsertActivityTracker(LPI_RegistrationActivity.this, DatabaseHelper.db,
                        StaticVariables.UserLoginId,
                        "A80013", "A8006", "Modify profile page called",
                        "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                        StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                        "", "", "Pending");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                DatabaseHelper.InsertActivityTracker(getApplication(), DatabaseHelper.db, StaticVariables.UserLoginId,
                        "A8003", "A8003", "Registration Page",
                        "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                        StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                        "", "", "Pending");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        checkTokenValid();
    }

    private void initDropDown() {
        Cursor country = DatabaseHelper.db.query(getString(R.string.TBL_Country), null, null,
                null, null, null, "CountryName asc");

        if (country.getCount() > 0) {
            Country_list.clear();
            Country_code_list.clear();
            for (country.moveToFirst(); !country.isAfterLast(); country.moveToNext()) {
                Country_list.add(country.getString(country.getColumnIndex("CountryName")));
                Country_code_list.add(country.getString(country.getColumnIndex("CountryCode")));
            }
        } else {
            new initCountry(LPI_RegistrationActivity.this).execute();
        }
        country.close();
        Cursor cursor = DatabaseHelper.db.query(getString(R.string.TBL_Profession), null, null,
                null, null, null, "ProfName asc");
        if (cursor.getCount() > 0) {
            arrProfession.clear();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                arrProfession.add(cursor.getString(cursor.getColumnIndex("ProfName")));
            }
        }else {
            new initProfession(LPI_RegistrationActivity.this).execute();
        }
        cursor.close();
    }

    private void checkTokenValid() {
        if (!modificationFlag) {
            getTokenDetails();
            input_email.setEnabled(true);
            if (balanceToken == 0) {
                strToken = "";
                input_reference.setText("");
                alertCustomDialog("Token Limit Alert", "Token use limit has been reached, please generate new token");
            } else if (strToken == null || strToken.trim().equalsIgnoreCase("")) {
                strToken = "";
                input_reference.setText("");
                alertCustomDialog("No Token Alert", "Token details not available, please sync token details from server");
            }
        }
    }

    private void setModifiedDataToUI() {
        Cursor mCursor = null;
        try {
            if (DatabaseHelper.db == null) {
                SQLiteDatabase.loadLibs(getApplicationContext());
                DatabaseHelper.db = DatabaseHelper.getInstance(getApplicationContext()).
                        getWritableDatabase(SC.reveal(CommonClass.DBPassword));
            }
            mCursor = DatabaseHelper.getOfflineCandidateInfo(LPI_RegistrationActivity.this,
                    DatabaseHelper.db, candidate_id);
            if (mCursor.getCount() > 0) {
                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    input_FName.setText(mCursor.getString(mCursor.getColumnIndex("FirstName")));
//                    input_LName.setText(mCursor.getString(mCursor.getColumnIndex("LastName")));
//                    input_DOB.setText(mCursor.getString(mCursor.getColumnIndex("DOB")));
                    input_email.setText(mCursor.getString(mCursor.getColumnIndex("EmailID")));
                    input_email.setEnabled(true);
                    input_mobile.setText(mCursor.getString(mCursor.getColumnIndex("MobileNo")));
                    strMobileCountryCode = (mCursor.getString(mCursor.getColumnIndex("MobileCountryCode")));
//                    input_password.setText(mCursor.getString(mCursor.getColumnIndex("Password")));
                    input_nationality.setText(mCursor.getString(mCursor.getColumnIndex("Nationality")));
                    input_profession.setText(mCursor.getString(mCursor.getColumnIndex("Profession")));
//                    String gender = mCursor.getString(mCursor.getColumnIndex("Gender"));
//                    if (gender.trim().equalsIgnoreCase("M")) {
//                        rdo_male.setChecked(true);
//                        rdo_female.setChecked(false);
//                    } else if (gender.trim().equalsIgnoreCase("F")) {
//                        rdo_male.setChecked(false);
//                        rdo_female.setChecked(true);
//                    }
                    img_byteArray = mCursor.getBlob(mCursor.getColumnIndex("ImageByte"));
                    Bitmap bitmap = BitmapFactory.decodeByteArray(img_byteArray, 0, img_byteArray.length);
                    if (bitmap != null) {
                        circleImageView.setImageBitmap(bitmap);
                    } else {
                        img_byteArray = null;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private void initDummyArray() {
//
//        try {
//            Cursor mCursor = DatabaseHelper.db.query(LPI_RegistrationActivity.this.getResources().getString(R.string.TBL_Country),
//                    null, null, null, null, null, null);
//            if (mCursor.getCount() < 1) {
//                CommonClass.initDataArray(LPI_RegistrationActivity.this);
//            } else {
//                arrProfession = new String[]{"Retired", "Software Professional", "Supervisor", "Sales Person", "Self Employed Professional", "Shop Owner", "Student", "Not Working", "Housewife", "Advocate/Lawyer", "Chartered Accounts", "Clerical", "Service", "Others"};
//                Arrays.sort(arrProfession);
//                String strCounty = "{\"country_list\":[{\"id\":\"344\",\"title\":\"Afghanistan\",\"code\":\"93\"},{\"id\":\"345\",\"title\":\"Albania\",\"code\":\"355\"},{\"id\":\"346\",\"title\":\"Algeria\",\"code\":\"213\"},{\"id\":\"347\",\"title\":\"American Samoa\",\"code\":\"1684\"},{\"id\":\"348\",\"title\":\"Andorra\",\"code\":\"376\"},{\"id\":\"349\",\"title\":\"Angola\",\"code\":\"244\"},{\"id\":\"350\",\"title\":\"Anguilla\",\"code\":\"1264\"},{\"id\":\"351\",\"title\":\"Antarctica\",\"code\":\"672\"},{\"id\":\"352\",\"title\":\"Antigua and Barbuda\",\"code\":\"1268\"},{\"id\":\"353\",\"title\":\"Argentina\",\"code\":\"54\"},{\"id\":\"586\",\"title\":\"Ariba\",\"code\":\"66\"},{\"id\":\"354\",\"title\":\"Armenia\",\"code\":\"374\"},{\"id\":\"355\",\"title\":\"Aruba\",\"code\":\"297\"},{\"id\":\"356\",\"title\":\"Australia\",\"code\":\"61\"},{\"id\":\"357\",\"title\":\"Austria\",\"code\":\"43\"},{\"id\":\"358\",\"title\":\"Azerbaijan\",\"code\":\"994\"},{\"id\":\"359\",\"title\":\"Bahamas\",\"code\":\"1242\"},{\"id\":\"360\",\"title\":\"Bahrain\",\"code\":\"973\"},{\"id\":\"361\",\"title\":\"Bangladesh\",\"code\":\"880\"},{\"id\":\"362\",\"title\":\"Barbados\",\"code\":\"1246\"},{\"id\":\"363\",\"title\":\"Belarus\",\"code\":\"375\"},{\"id\":\"364\",\"title\":\"Belgium\",\"code\":\"32\"},{\"id\":\"365\",\"title\":\"Belize\",\"code\":\"501\"},{\"id\":\"366\",\"title\":\"Benin\",\"code\":\"229\"},{\"id\":\"367\",\"title\":\"Bermuda\",\"code\":\"1441\"},{\"id\":\"368\",\"title\":\"Bhutan\",\"code\":\"975\"},{\"id\":\"369\",\"title\":\"Bolivia\",\"code\":\"591\"},{\"id\":\"371\",\"title\":\"Botswana\",\"code\":\"267\"},{\"id\":\"373\",\"title\":\"Brazil\",\"code\":\"55\"},{\"id\":\"374\",\"title\":\"British Indian Ocean Territory\",\"code\":\"246\"},{\"id\":\"376\",\"title\":\"Bulgaria\",\"code\":\"359\"},{\"id\":\"377\",\"title\":\"Burkina Faso\",\"code\":\"226\"},{\"id\":\"378\",\"title\":\"Burundi\",\"code\":\"257\"},{\"id\":\"379\",\"title\":\"Cambodia\",\"code\":\"855\"},{\"id\":\"380\",\"title\":\"Cameroon\",\"code\":\"237\"},{\"id\":\"381\",\"title\":\"Canada\",\"code\":\"1\"},{\"id\":\"382\",\"title\":\"Cape Verde\",\"code\":\"238\"},{\"id\":\"383\",\"title\":\"Cayman Islands\",\"code\":\"1345\"},{\"id\":\"384\",\"title\":\"Central African Republic\",\"code\":\"236\"},{\"id\":\"385\",\"title\":\"Chad\",\"code\":\"235\"},{\"id\":\"386\",\"title\":\"Chile\",\"code\":\"56\"},{\"id\":\"387\",\"title\":\"China\",\"code\":\"86\"},{\"id\":\"388\",\"title\":\"Christmas Island\",\"code\":\"61\"},{\"id\":\"390\",\"title\":\"Colombia\",\"code\":\"57\"},{\"id\":\"391\",\"title\":\"Comoros\",\"code\":\"269\"},{\"id\":\"394\",\"title\":\"Cook Islands\",\"code\":\"682\"},{\"id\":\"395\",\"title\":\"Costa Rica\",\"code\":\"506\"},{\"id\":\"398\",\"title\":\"Cuba\",\"code\":\"53\"},{\"id\":\"399\",\"title\":\"Cyprus\",\"code\":\"357\"},{\"id\":\"400\",\"title\":\"Czech Republic\",\"code\":\"420\"},{\"id\":\"401\",\"title\":\"Denmark\",\"code\":\"45\"},{\"id\":\"402\",\"title\":\"Djibouti\",\"code\":\"253\"},{\"id\":\"403\",\"title\":\"Dominica\",\"code\":\"1767\"},{\"id\":\"404\",\"title\":\"Dominican Republic\",\"code\":\"1809, 1829, 1849\"},{\"id\":\"405\",\"title\":\"East Timor\",\"code\":\"670\"},{\"id\":\"406\",\"title\":\"Ecuador\",\"code\":\"593\"},{\"id\":\"407\",\"title\":\"Egypt\",\"code\":\"20\"},{\"id\":\"408\",\"title\":\"El Salvador\",\"code\":\"503\"},{\"id\":\"409\",\"title\":\"Equatorial Guinea\",\"code\":\"240\"},{\"id\":\"410\",\"title\":\"Eritrea\",\"code\":\"291\"},{\"id\":\"411\",\"title\":\"Estonia\",\"code\":\"372\"},{\"id\":\"412\",\"title\":\"Ethiopia\",\"code\":\"251\"},{\"id\":\"414\",\"title\":\"Faroe Islands\",\"code\":\"298\"},{\"id\":\"415\",\"title\":\"Fiji\",\"code\":\"679\"},{\"id\":\"416\",\"title\":\"Finland\",\"code\":\"358\"},{\"id\":\"417\",\"title\":\"France\",\"code\":\"33\"},{\"id\":\"420\",\"title\":\"French Polynesia\",\"code\":\"689\"},{\"id\":\"422\",\"title\":\"Gabon\",\"code\":\"241\"},{\"id\":\"423\",\"title\":\"Gambia\",\"code\":\"220\"},{\"id\":\"424\",\"title\":\"Georgia\",\"code\":\"995\"},{\"id\":\"425\",\"title\":\"Germany\",\"code\":\"49\"},{\"id\":\"426\",\"title\":\"Ghana\",\"code\":\"233\"},{\"id\":\"427\",\"title\":\"Gibraltar\",\"code\":\"350\"},{\"id\":\"428\",\"title\":\"Greece\",\"code\":\"30\"},{\"id\":\"429\",\"title\":\"Greenland\",\"code\":\"299\"},{\"id\":\"430\",\"title\":\"Grenada\",\"code\":\"1473\"},{\"id\":\"432\",\"title\":\"Guam\",\"code\":\"1671\"},{\"id\":\"433\",\"title\":\"Guatemala\",\"code\":\"502\"},{\"id\":\"434\",\"title\":\"Guinea\",\"code\":\"224\"},{\"id\":\"435\",\"title\":\"Guinea-Bissau\",\"code\":\"245\"},{\"id\":\"436\",\"title\":\"Guyana\",\"code\":\"592\"},{\"id\":\"437\",\"title\":\"Haiti\",\"code\":\"509\"},{\"id\":\"440\",\"title\":\"Honduras\",\"code\":\"504\"},{\"id\":\"441\",\"title\":\"Hong Kong\",\"code\":\"852\"},{\"id\":\"442\",\"title\":\"Hungary\",\"code\":\"36\"},{\"id\":\"443\",\"title\":\"Iceland\",\"code\":\"354\"},{\"id\":\"444\",\"title\":\"India\",\"code\":\"91\"},{\"id\":\"445\",\"title\":\"Indonesia\",\"code\":\"62\"},{\"id\":\"447\",\"title\":\"Iraq\",\"code\":\"964\"},{\"id\":\"448\",\"title\":\"Ireland\",\"code\":\"353\"},{\"id\":\"449\",\"title\":\"Israel\",\"code\":\"972\"},{\"id\":\"450\",\"title\":\"Italy\",\"code\":\"39\"},{\"id\":\"451\",\"title\":\"Jamaica\",\"code\":\"1876\"},{\"id\":\"452\",\"title\":\"Japan\",\"code\":\"81\"},{\"id\":\"453\",\"title\":\"Jordan\",\"code\":\"962\"},{\"id\":\"454\",\"title\":\"Kazakhstan\",\"code\":\"7\"},{\"id\":\"455\",\"title\":\"Kenya\",\"code\":\"254\"},{\"id\":\"456\",\"title\":\"Kiribati\",\"code\":\"686\"},{\"id\":\"459\",\"title\":\"Kuwait\",\"code\":\"965\"},{\"id\":\"460\",\"title\":\"Kyrgyzstan\",\"code\":\"996\"},{\"id\":\"462\",\"title\":\"Latvia\",\"code\":\"371\"},{\"id\":\"463\",\"title\":\"Lebanon\",\"code\":\"961\"},{\"id\":\"464\",\"title\":\"Lesotho\",\"code\":\"266\"},{\"id\":\"465\",\"title\":\"Liberia\",\"code\":\"231\"},{\"id\":\"467\",\"title\":\"Liechtenstein\",\"code\":\"423\"},{\"id\":\"468\",\"title\":\"Lithuania\",\"code\":\"370\"},{\"id\":\"469\",\"title\":\"Luxembourg\",\"code\":\"352\"},{\"id\":\"470\",\"title\":\"Macau\",\"code\":\"853\"},{\"id\":\"472\",\"title\":\"Madagascar\",\"code\":\"261\"},{\"id\":\"473\",\"title\":\"Malawi\",\"code\":\"265\"},{\"id\":\"474\",\"title\":\"Malaysia\",\"code\":\"60\"},{\"id\":\"475\",\"title\":\"Maldives\",\"code\":\"960\"},{\"id\":\"476\",\"title\":\"Mali\",\"code\":\"223\"},{\"id\":\"477\",\"title\":\"Malta\",\"code\":\"356\"},{\"id\":\"478\",\"title\":\"Marshall Islands\",\"code\":\"692\"},{\"id\":\"480\",\"title\":\"Mauritania\",\"code\":\"222\"},{\"id\":\"481\",\"title\":\"Mauritius\",\"code\":\"230\"},{\"id\":\"482\",\"title\":\"Mayotte\",\"code\":\"262\"},{\"id\":\"483\",\"title\":\"Mexico\",\"code\":\"52\"},{\"id\":\"486\",\"title\":\"Monaco\",\"code\":\"377\"},{\"id\":\"487\",\"title\":\"Mongolia\",\"code\":\"976\"},{\"id\":\"488\",\"title\":\"Montserrat\",\"code\":\"1664\"},{\"id\":\"489\",\"title\":\"Morocco\",\"code\":\"212\"},{\"id\":\"490\",\"title\":\"Mozambique\",\"code\":\"258\"},{\"id\":\"491\",\"title\":\"Myanmar\",\"code\":\"95\"},{\"id\":\"492\",\"title\":\"Namibia\",\"code\":\"264\"},{\"id\":\"493\",\"title\":\"Nauru\",\"code\":\"674\"},{\"id\":\"494\",\"title\":\"Nepal\",\"code\":\"977\"},{\"id\":\"495\",\"title\":\"Netherlands\",\"code\":\"31\"},{\"id\":\"496\",\"title\":\"Netherlands Antilles\",\"code\":\"599\"},{\"id\":\"497\",\"title\":\"New Caledonia\",\"code\":\"687\"},{\"id\":\"498\",\"title\":\"New Zealand\",\"code\":\"64\"},{\"id\":\"499\",\"title\":\"Nicaragua\",\"code\":\"505\"},{\"id\":\"500\",\"title\":\"Niger\",\"code\":\"227\"},{\"id\":\"501\",\"title\":\"Nigeria\",\"code\":\"234\"},{\"id\":\"502\",\"title\":\"Niue\",\"code\":\"683\"},{\"id\":\"504\",\"title\":\"Northern Mariana Islands\",\"code\":\"1670\"},{\"id\":\"505\",\"title\":\"Norway\",\"code\":\"47\"},{\"id\":\"506\",\"title\":\"Oman\",\"code\":\"968\"},{\"id\":\"507\",\"title\":\"Pakistan\",\"code\":\"92\"},{\"id\":\"508\",\"title\":\"Palau\",\"code\":\"680\"},{\"id\":\"509\",\"title\":\"Panama\",\"code\":\"507\"},{\"id\":\"510\",\"title\":\"Papua New Guinea\",\"code\":\"675\"},{\"id\":\"511\",\"title\":\"Paraguay\",\"code\":\"595\"},{\"id\":\"512\",\"title\":\"Peru\",\"code\":\"51\"},{\"id\":\"513\",\"title\":\"Philippines\",\"code\":\"63\"},{\"id\":\"514\",\"title\":\"Pitcairn\",\"code\":\"64\"},{\"id\":\"515\",\"title\":\"Poland\",\"code\":\"48\"},{\"id\":\"516\",\"title\":\"Portugal\",\"code\":\"351\"},{\"id\":\"517\",\"title\":\"Puerto Rico\",\"code\":\"1787, 1939\"},{\"id\":\"518\",\"title\":\"Qatar\",\"code\":\"974\"},{\"id\":\"519\",\"title\":\"Reunion\",\"code\":\"262\"},{\"id\":\"520\",\"title\":\"Romania\",\"code\":\"40\"},{\"id\":\"522\",\"title\":\"Rwanda\",\"code\":\"250\"},{\"id\":\"523\",\"title\":\"Saint Kitts and Nevis\",\"code\":\"1869\"},{\"id\":\"524\",\"title\":\"Saint Lucia\",\"code\":\"1758\"},{\"id\":\"525\",\"title\":\"Saint Vincent and the Grenadines\",\"code\":\"1784\"},{\"id\":\"526\",\"title\":\"Samoa\",\"code\":\"685\"},{\"id\":\"527\",\"title\":\"San Marino\",\"code\":\"378\"},{\"id\":\"528\",\"title\":\"Sao Tome and Principe\",\"code\":\"239\"},{\"id\":\"529\",\"title\":\"Saudi Arabia\",\"code\":\"966\"},{\"id\":\"530\",\"title\":\"Senegal\",\"code\":\"221\"},{\"id\":\"531\",\"title\":\"Seychelles\",\"code\":\"248\"},{\"id\":\"532\",\"title\":\"Sierra Leone\",\"code\":\"232\"},{\"id\":\"533\",\"title\":\"Singapore\",\"code\":\"65\"},{\"id\":\"535\",\"title\":\"Slovenia\",\"code\":\"386\"},{\"id\":\"536\",\"title\":\"Solomon Islands\",\"code\":\"677\"},{\"id\":\"537\",\"title\":\"Somalia\",\"code\":\"252\"},{\"id\":\"538\",\"title\":\"South Africa\",\"code\":\"27\"},{\"id\":\"540\",\"title\":\"Spain\",\"code\":\"34\"},{\"id\":\"541\",\"title\":\"Sri Lanka\",\"code\":\"94\"},{\"id\":\"544\",\"title\":\"Sudan\",\"code\":\"249\"},{\"id\":\"545\",\"title\":\"Suriname\",\"code\":\"597\"},{\"id\":\"547\",\"title\":\"Swaziland\",\"code\":\"268\"},{\"id\":\"548\",\"title\":\"Sweden\",\"code\":\"46\"},{\"id\":\"549\",\"title\":\"Switzerland\",\"code\":\"41\"},{\"id\":\"552\",\"title\":\"Tajikistan\",\"code\":\"992\"},{\"id\":\"554\",\"title\":\"Thailand\",\"code\":\"66\"},{\"id\":\"555\",\"title\":\"Togo\",\"code\":\"228\"},{\"id\":\"556\",\"title\":\"Tokelau\",\"code\":\"690\"},{\"id\":\"557\",\"title\":\"Tonga\",\"code\":\"676\"},{\"id\":\"558\",\"title\":\"Trinidad and Tobago\",\"code\":\"1868\"},{\"id\":\"559\",\"title\":\"Tunisia\",\"code\":\"216\"},{\"id\":\"560\",\"title\":\"Turkey\",\"code\":\"90\"},{\"id\":\"561\",\"title\":\"Turkmenistan\",\"code\":\"993\"},{\"id\":\"562\",\"title\":\"Turks and Caicos Islands\",\"code\":\"1649\"},{\"id\":\"563\",\"title\":\"Tuvalu\",\"code\":\"688\"},{\"id\":\"564\",\"title\":\"Uganda\",\"code\":\"256\"},{\"id\":\"565\",\"title\":\"Ukraine\",\"code\":\"380\"},{\"id\":\"566\",\"title\":\"United Arab Emirates\",\"code\":\"971\"},{\"id\":\"567\",\"title\":\"United Kingdom\",\"code\":\"44\"},{\"id\":\"568\",\"title\":\"United States\",\"code\":\"1\"},{\"id\":\"570\",\"title\":\"Uruguay\",\"code\":\"598\"},{\"id\":\"571\",\"title\":\"Uzbekistan\",\"code\":\"998\"},{\"id\":\"572\",\"title\":\"Vanuatu\",\"code\":\"678\"},{\"id\":\"573\",\"title\":\"Venezuela\",\"code\":\"58\"},{\"id\":\"574\",\"title\":\"Vietnam\",\"code\":\"84\"},{\"id\":\"578\",\"title\":\"Western Sahara\",\"code\":\"212\"},{\"id\":\"579\",\"title\":\"Yemen\",\"code\":\"967\"},{\"id\":\"581\",\"title\":\"Zambia\",\"code\":\"260\"},{\"id\":\"582\",\"title\":\"Zimbabwe\",\"code\":\"263\"}]}\n";
//
//                JSONObject DataJsonObject = null;
//                try {
//                    DataJsonObject = new JSONObject(strCounty);
//                    JSONArray jsonArray = DataJsonObject.getJSONArray("country_list");
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        JSONObject jsonObject = new JSONObject(jsonArray.get(i).toString());
//                        Country_list.add(i, jsonObject.getString("title"));
//                        Country_code_list.add(i, jsonObject.getString("code"));
//
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//            mCursor.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//            initDummyArray();
//        }
//    }

//    private void updateLabel() {
//        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//        input_DOB.setText(sdf.format(myCalendar.getTime()));
//        input_DOB.setHintTextColor(getResources().getColor(R.color.colorPrimary));
//        input_DOB.setError(null);
//    }

    protected void ProfileImage() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(LPI_RegistrationActivity.this).create();
        alertDialog.setTitle("Photo upload method");
        alertDialog.setMessage("Please select photo upload method.");
        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        captureImage();
                    }
                });
        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "Browse",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        browsePicture();
                    }
                });
        byte[] tempImg_byteArray = null;
        try {
            Drawable drawable = null;
            drawable = getResources().getDrawable(R.drawable.profile_blank_m);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
            tempImg_byteArray = stream.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (img_byteArray != null && !Arrays.equals(img_byteArray, tempImg_byteArray)) {
            alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "Remove Photo",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            removePicture();
                        }
                    });
        }
        alertDialog.show();
    }

    private void removePicture() {
        AlertDialog.Builder alert = new AlertDialog.Builder(
                LPI_RegistrationActivity.this);
        alert.setTitle("Alert!!");
        alert.setMessage("Are you sure, you want to remove profile photo?");
        alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                circleImageView.setImageDrawable(getResources().getDrawable(R.drawable.profile_blank_m));
                try {
                    Drawable drawable = null;
                    drawable = getResources().getDrawable(R.drawable.profile_blank_m);
                    Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                    img_byteArray = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                fileUri = getOutputMediaFileUri(1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            fileUri = getOutputMediaFileUriBelowL(1);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, 555);
    }

    private void browsePicture() {
       /* Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, 333);*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                fileUri = getOutputMediaFileUri(1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            fileUri = getOutputMediaFileUriBelowL(1);
        }
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 333);
    }

    public Uri getOutputMediaFileUri(int type) {
//        return Uri.fromFile(getOutputMediaFile(type));
        return FileProvider.getUriForFile(LPI_RegistrationActivity.this,
                BuildConfig.APPLICATION_ID + ".provider",
                getOutputMediaFile());
    }

    public Uri getOutputMediaFileUriBelowL(int type) {
        return Uri.fromFile(getOutputMediaFile());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 333 && resultCode == Activity.RESULT_OK) {

            Bitmap thePic;
            final Uri selectedImage = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                thePic = (BitmapFactory.decodeStream(inputStream));
                if (thePic != null) {
                    saveBitmap(thePic);
//                    String strLocation = getRecentPicFileLocation(fileUri);
                    startCrop(selectedImage);

                } else {
                    try {
                        thePic = BitmapFactory.decodeFile(CompressImage.compressImage(fileUri.getPath(),
                                selectedImage.getPath(),LPI_RegistrationActivity.this));
                        if (thePic != null) {
                            saveBitmap(thePic);
//                            String strLocation = getRecentPicFileLocation(fileUri);
                            cropCapturedImage(fileUri);
                            startCrop(fileUri);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 555 && resultCode == Activity.RESULT_OK) {
            Bitmap photo = decodeImage(mCurrentPhotoPath);
            if (photo == null) {
                try {
                    Uri imageUri = Uri.parse(mCurrentPhotoPath);
                    File file = new File(imageUri.getPath());
                    InputStream ims = new FileInputStream(file);
                    photo = (BitmapFactory.decodeStream(ims));
                    if (photo != null) {
                        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            NcropImage(fileUri);
                        } else {
                            cropCapturedImage(imageUri);
                        }*/
                        fileUri = imageUri;
                        startCrop(fileUri);
                    }
                } catch (Exception a) {
                    String errorMessage = "Sorry - Image capturing failed, Please try again.";
                    Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_LONG);
                    toast.show();
                }
            } else {
//                    cropCapturedImage(fileUri);
                /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    NcropImage(fileUri);
                } else {
                    cropCapturedImage(fileUri);
                }*/
                startCrop(fileUri);
            }
//            cropCapturedImage(fileUri);

        } else if (requestCode == UCrop.REQUEST_CROP && resultCode == Activity.RESULT_OK) {
            try {
                final Uri imageUri = UCrop.getOutput(data);
                if (imageUri != null) {
                    Bitmap thePic = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                    if (thePic != null) {
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        thePic.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                        img_byteArray = stream.toByteArray();
                        if (thePic != null) {
                            circleImageView.setImageBitmap(thePic);
                        }
                        saveBitmap(thePic);
                    }
                }
            } catch (Exception e) {
                try {
                    if (data != null) {
                        Uri imageUri = data.getData();
                        if (imageUri != null) {
                            Bitmap thePic = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                            if (thePic != null) {
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                thePic.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                                img_byteArray = stream.toByteArray();
                                if (thePic != null) {
                                    circleImageView.setImageBitmap(thePic);
                                }
                                saveBitmap(thePic);
                            }
                        }
                    }
                } catch (Exception ee) {
                }
            }
        } else if (requestCode == UCrop.REQUEST_CROP && resultCode == Activity.RESULT_CANCELED) {
            Toast.makeText(LPI_RegistrationActivity.this, "Profile crop cancelled by user.", Toast.LENGTH_SHORT).show();
        } else if (requestCode == 666) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    Bundle extras = data.getExtras();
                    if (extras != null) {
                        Bitmap thePic = extras.getParcelable("data");
                        if (thePic != null) {
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            thePic.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                            img_byteArray = stream.toByteArray();
                            if (thePic != null) {
                                circleImageView.setImageBitmap(thePic);
                            }
                            saveBitmap(thePic);
                        } else {
                            Uri imageUri = data.getData();
                            if (imageUri != null) {
                                thePic = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                                if (thePic != null) {
                                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                    thePic.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                                    img_byteArray = stream.toByteArray();
                                    if (thePic != null) {
                                        circleImageView.setImageBitmap(thePic);
                                    }
                                    saveBitmap(thePic);
                                }
                            }
                        }
                    } else {
                        if (data != null) {
                            Uri imageUri = data.getData();
                            if (imageUri != null) {
                                Bitmap thePic = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                                if (thePic != null) {
                                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                    thePic.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                                    img_byteArray = stream.toByteArray();
                                    if (thePic != null) {
                                        circleImageView.setImageBitmap(thePic);
                                    }
                                    saveBitmap(thePic);
                                }
                            } else {
                                if (data != null) {
                                    Bitmap thePic = MediaStore.Images.Media.getBitmap(this.getContentResolver(), fileUri);
                                    if (thePic != null) {
                                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                        thePic.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                                        img_byteArray = stream.toByteArray();
                                        if (thePic != null) {
                                            circleImageView.setImageBitmap(thePic);
                                        }
                                        saveBitmap(thePic);
                                    }
                                } else {
                                    alertCustomDialog("Crop Image failed", "Please try cropping using another tool");
                                }
                            }
                        } else {
                            String errorMessage = "Sorry - your device doesn't support the crop action!";
                            Toast toast = Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG);
                            toast.show();
                            onBackPressed();
                        }
                    }

                } catch (Exception e) {
                    Log.e("", "act res err " + e.toString());
                }
            }
        }
    }

    private void startCrop(@NonNull Uri uri) {
        try {
            UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), "profile.jpg")));
            uCrop = advancedConfig(uCrop);
            uCrop.start(LPI_RegistrationActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);
        options.setFreeStyleCropEnabled(true);

        return uCrop.withOptions(options);
    }

    private Bitmap decodeImage(String f) {
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
            //The new size we want to scale to
            final int REQUIRED_SIZE = 50;
            //Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;
            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Bitmap image_bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
            Matrix matrix = new Matrix();
            ExifInterface exifInterface = new ExifInterface("" + f);
            int rotation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (rotation) {
                case ExifInterface.ORIENTATION_NORMAL: {
                }
                break;
                case ExifInterface.ORIENTATION_ROTATE_90: {
                    matrix.postRotate(90);
                    image_bitmap = Bitmap.createBitmap(image_bitmap, 0, 0, image_bitmap.getWidth(), image_bitmap.getHeight(),
                            matrix, true);
                }
                break;
                case ExifInterface.ORIENTATION_ROTATE_180: {
                    matrix.postRotate(180);
                    image_bitmap = Bitmap.createBitmap(image_bitmap, 0, 0, image_bitmap.getWidth(), image_bitmap.getHeight(),
                            matrix, true);
                }
                break;
                case ExifInterface.ORIENTATION_ROTATE_270: {
                    matrix.postRotate(270);
                    image_bitmap = Bitmap.createBitmap(image_bitmap, 0, 0, image_bitmap.getWidth(), image_bitmap.getHeight(),
                            matrix, true);
                }
                break;
            }
            return image_bitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    boolean saveBitmap(Bitmap bitmap) {

        File pictureFile = getOutputMediaFile();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            fileUri = FileProvider.getUriForFile(LPI_RegistrationActivity.this,
                    BuildConfig.APPLICATION_ID + ".provider", pictureFile);
        } else {
            fileUri = Uri.fromFile(pictureFile);
        }
        if (pictureFile == null) {
            return false;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
            fos.close();
            return true;
        } catch (FileNotFoundException e) {
            return false;
        } catch (Exception e) {
        }
        return true;
    }

    public void cropCapturedImage(Uri picUri) {
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(picUri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);

            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(new File(picUri.getPath()).getAbsolutePath(), options);
                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;
                cropIntent.putExtra("outputX", imageHeight);
                cropIntent.putExtra("outputY", imageWidth);
            } catch (Exception e) {
                e.printStackTrace();
                cropIntent.putExtra("outputX", 256);
                cropIntent.putExtra("outputY", 256);
            }
            cropIntent.putExtra("return-data", true);
//            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
            startActivityForResult(cropIntent, 666);
        } catch (ActivityNotFoundException e) {
            cropImage(fileUri);
        } catch (Exception e) {
            String errorMessage = "Sorry - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_LONG);
            toast.show();
            alertCustomDialog("Crop Feature Not Allowed", "Do you want to proceed without cropping a photo?");
        }
    }

    private void cropImage(Uri picUri) {
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(picUri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(new File(picUri.getPath()).getAbsolutePath(), options);
                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;
                cropIntent.putExtra("outputX", imageHeight);
                cropIntent.putExtra("outputY", imageWidth);
            } catch (Exception e) {
                e.printStackTrace();
                cropIntent.putExtra("outputX", 256);
                cropIntent.putExtra("outputY", 256);
            }
            cropIntent.putExtra("return-data", true);
            cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
            startActivityForResult(cropIntent, 666);
        } catch (ActivityNotFoundException e) {
            //display an error message if user device doesn't support
            alertCustomDialog("Cropping Alert", "Cropping failed, please use another tool/app for crop image.");
        } catch (Exception e) {
            alertCustomDialog("Cropping Alert", "Cropping failed, please use another tool/app for crop image.");
        }
    }

    private void NcropImage(final Uri sourceImage) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        intent.setType("image/*");

        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, 0);
        int size = list.size();
        if (size == 0) {
            //Utils.showToast(mContext, mContext.getString(R.string.error_cant_select_cropping_app));
            fileUri = sourceImage;
            intent.putExtra(MediaStore.EXTRA_OUTPUT, sourceImage);
            startActivityForResult(intent, 666);
            return;
        } else {
            intent.setDataAndType(sourceImage, "image/*");
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
//                BitmapFactory.decodeFile(new File(sourceImage.getPath()).getAbsolutePath(), options);
                BitmapFactory.decodeStream(LPI_RegistrationActivity.this.getContentResolver().openInputStream(sourceImage),
                        null, options);
                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;
                intent.putExtra("outputX", imageHeight);
                intent.putExtra("outputY", imageWidth);
            } catch (Exception e) {
                e.printStackTrace();
                intent.putExtra("outputX", 256);
                intent.putExtra("outputY", 256);
            }
            intent.putExtra("scale", true);

            //intent.putExtra("return-data", true);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, sourceImage);
            fileUri = sourceImage;
            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);
                i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                startActivityForResult(intent, 666);
            } else {
                Intent i = new Intent(intent);
                i.putExtra(Intent.EXTRA_INITIAL_INTENTS, list.toArray(new Parcelable[list.size()]));
                startActivityForResult(intent, 666);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MarshmallowPermission.CAMERA_PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ProfileImage();
                }
                return;
            }
            case MarshmallowPermission.EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (!marshmallowPermission.checkPermissionForCamera()) {
                    marshmallowPermission.requestPermissionForCamera();
                } else if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ProfileImage();
                }
            }
            return;
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog = new Dialog(this, R.style.AppTheme_Dark_Dialog);
        } else {
            dialog = new Dialog(this);
        }
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_list_item);
        dialog.setCanceledOnTouchOutside(true);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.90);
        int screenHeight = (int) (metrics.widthPixels * 1.3);

        Window window = dialog.getWindow();
        window.setLayout(screenWidth, screenHeight);

        //Prepare ListView in dialog
        dialog_ListView = (ListView) dialog.findViewById(R.id.dialoglist);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.iv_close);
        iv_close.setVisibility(View.GONE);

        switch (id) {

            case CUSTOM_DIALOG_ID:

                TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                tv_title.setText("Select Country");

                if (Country_list.size() > 0) {
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (LPI_RegistrationActivity.this, R.layout.country_list_item, Country_list);
                    dialog_ListView.setAdapter(adapter);

                    dialog_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
                            input_nationality.setText(Country_list.get(position));
                            input_nationality.setError(null);
                            strMobileCountryCode = Country_code_list.get(position);
                            input_nationality.setHintTextColor(getResources().getColor(R.color.colorPrimary));
                            dismissDialog(CUSTOM_DIALOG_ID);
                        }
                    });
                }


                /*iv_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });*/

                break;
            case 222:
                tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                tv_title.setText("Select Profession");
                if (arrProfession.size() > 0) {
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (LPI_RegistrationActivity.this, R.layout.country_list_item, arrProfession);
                    dialog_ListView.setAdapter(adapter);

                    dialog_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
                            input_profession.setText(arrProfession.get(position));
                            input_profession.setHintTextColor(getResources().getColor(R.color.colorPrimary));
                            input_profession.setError(null);
                            dismissDialog(222);
                        }
                    });
                }
                break;
        }
        return dialog;
    }

    @Override
    protected void onPrepareDialog(int id, Dialog dialog, Bundle bundle) {
        super.onPrepareDialog(id, dialog, bundle);

        switch (id) {
            case CUSTOM_DIALOG_ID:
                break;
            case 222:
                break;
        }
    }

    public void showDoctorDialog(View view) {
        showDialog(CUSTOM_DIALOG_ID);
    }

    public void showOrganisationDialog(View view) {
        showDialog(CUSTOM_DIALOG_ID);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();

        if (strToolbarTitle.equals("LoginActivity")) {
            intent = new Intent(LPI_RegistrationActivity.this, Login.class);
        } else {
            if (StaticVariables.UserType.equals("C")) {
                intent = new Intent(LPI_RegistrationActivity.this, ParticipantHomeActivity.class);
            } else {
                intent = new Intent(LPI_RegistrationActivity.this, LPI_TrainerMenuActivity.class);
            }
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        LPI_RegistrationActivity.this.finish();
    }

    class AsyncGetCandidateDetails extends AsyncTask<String, Void, JSONObject> {
        Context context;
        ProgressDialog mProgressDialog;

        public AsyncGetCandidateDetails(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading..\nPlease wait..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            // TODO Auto-generated method stub
            String strResponse = "";
            try {
                strResponse = getCandidateDetails();
                if (strResponse != null && !strResponse.equalsIgnoreCase("{}")
                        && !strResponse.equalsIgnoreCase("null")) {
                    return new JSONObject(strResponse);
                } else {
                    return null;
                }
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            // TODO Auto-generated method stub
            try {
                if (result != null && !result.toString().equalsIgnoreCase("{}")
                        && !result.toString().equalsIgnoreCase("null")
                        && !result.toString().equalsIgnoreCase("")) {
                    String strJsonArray = result.getJSONArray("Table").toString();
                    JSONArray jsonArray = new JSONArray(strJsonArray);
                    JSONObject jsonObjectError = new JSONObject();
                    byte[] imageBytes = null;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                        jsonObjectError = jsonObject;

                        input_FName.setText(jsonObject.getString("UserName"));
//                        input_LName.setText(jsonObject.getString("UserName2"));
//                        String gender = (jsonObject.getString("Gender"));
//                        if (gender.trim().equalsIgnoreCase("M")) {
//                            rdo_male.setChecked(true);
//                            rdo_female.setChecked(false);
//                        } else if (gender.trim().equalsIgnoreCase("F")) {
//                            rdo_male.setChecked(false);
//                            rdo_female.setChecked(true);
//                        }

//                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//                        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
//                        SimpleDateFormat sdfInput = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//                        Date d = sdfInput.parse(jsonObject.getString("DOB"));
//                        String serverDate = sdf.format(d);
//                        input_DOB.setText(serverDate);
                        input_email.setText(jsonObject.getString("UserEmailId"));
                        input_mobile.setText(jsonObject.getString("UserMobileNo1"));
//                        lin_password.setVisibility(View.GONE);
                        input_nationality.setText(jsonObject.getString("Nationality"));
                        input_profession.setText(jsonObject.getString("Profession"));
                        if (!jsonObject.isNull("Images") && !jsonObject.get("Images").equals(null)
                                && !jsonObject.get("Images").equals("null") && !jsonObject.get("Images").equals("")) {
                            imageBytes = Base64.decode(jsonObject.getString("Images"), Base64.DEFAULT);
                            if (imageBytes != null &&
                                    !(jsonObject.get("Images").toString().trim().equalsIgnoreCase("null"))) {
                                img_byteArray = imageBytes;
                                Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                                circleImageView.setImageBitmap(bitmap);
                            } else {
                                img_byteArray = null;
                            }
                        } else {
                            img_byteArray = null;
                        }
                        try {
                            int pos = Country_list.indexOf(jsonObject.getString("Nationality"));
                            strMobileCountryCode = Country_code_list.get(pos);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        strAppLink = jsonObject.getString("AppLink");
                    }
                    mProgressDialog.dismiss();
                } else {
                    mProgressDialog.dismiss();
                    alertCustomDialog("Alert!", "Server not responding");
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                }
                alertCustomDialog("Alert!", "Something went wrong, please try again.");
            }
        }


        private String getCandidateDetails() {

            String strResp = "";
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.GetCandidateDetails));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            String strReqXML = reqBuild();
            inputPI1.setName("objXMLDoc");
            try {
                inputPI1.setValue(strReqXML);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));

            try {
                LPIEEX509TrustManager.allowAllSSL();
//                FakeX509TrustManager.trustSSLCertificate(LPI_RegistrationActivity.this);
                androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                        context.getString(R.string.GetCandidateDetails), envelope);
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strResp = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return strResp;
        }

        private String reqBuild() {

            String str_XML = "";
            nu.xom.Element root = new nu.xom.Element("GetCandidateDetails");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(context));

            nu.xom.Element InscType = new nu.xom.Element("InscType");
            InscType.appendChild("" + StaticVariables.crnt_InscType);

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
            root.appendChild(InscType);

            nu.xom.Element CandidateId = new nu.xom.Element("CandidateId");
            CandidateId.appendChild(candidate_id.trim());
            root.appendChild(CandidateId);

            nu.xom.Document doc = new nu.xom.Document(root);
            try {
                OutputStream out = new ByteArrayOutputStream();
                Serializer serializer = new Serializer(System.out, "UTF-8");
                serializer.setOutputStream(out);
                serializer.setIndent(4);
                serializer.setMaxLength(64);
                serializer.write(doc);
                str_XML = out.toString();
                str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
                str_XML = str_XML.replaceAll("\n", "");
                str_XML = str_XML.replaceAll("\r", "");
                str_XML = str_XML.replaceAll("     ", "");
                str_XML = str_XML.replaceAll("         ", "");
                str_XML = str_XML.trim();
                Log.e("", "XMLStr==" + str_XML);
            } catch (IOException e) {
                Log.e("", "getCandidateDetails Error=" + e.toString());
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "AsyncGetCandidateDetails", "getCandidateDetails", e.toString());
            }
            return str_XML;
        }

    }

    public class initCountry extends AsyncTask<String, String, String> {
        Context context;
        ProgressDialog mProgressDialog;

        public initCountry(LPI_RegistrationActivity lpi_registrationActivity) {
            this.context = lpi_registrationActivity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Initialising..\nPlease wait..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
//            android.database.Cursor cursor = DatabaseHelper.db.query(context.getResources().getString(R.string.TBL_Country),
//                    null, null, null, null, null, null);
//            if (cursor.getCount() < 1) {
//                CommonClass.initDataArray(context);
            CommonClass.initCDataArray(context);
//            }
//            cursor.close();
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            Cursor country = DatabaseHelper.db.query(getString(R.string.TBL_Country), null, null,
                    null, null, null, "CountryName asc");

            if (country.getCount() > 0) {
                Country_list.clear();
                Country_code_list.clear();
                for (country.moveToFirst(); !country.isAfterLast(); country.moveToNext()) {
                    Country_list.add(country.getString(country.getColumnIndex("CountryName")));
                    Country_code_list.add(country.getString(country.getColumnIndex("CountryCode")));
                }
            }
            country.close();
            Cursor cursor = DatabaseHelper.db.query(getString(R.string.TBL_Profession), null, null,
                    null, null, null, "ProfName asc");
            if (cursor.getCount() > 0) {
                arrProfession.clear();
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    arrProfession.add(cursor.getString(cursor.getColumnIndex("ProfName")));
                }
            }
            cursor.close();
        }
    }

    public class initProfession extends AsyncTask<String, String, String> {
        Context context;
        ProgressDialog mProgressDialog;

        public initProfession(LPI_RegistrationActivity lpi_registrationActivity) {
            this.context = lpi_registrationActivity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Initialising..\nPlease wait..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
//            android.database.Cursor cursor = DatabaseHelper.db.query(context.getResources().getString(R.string.TBL_Country),
//                    null, null, null, null, null, null);
//            if (cursor.getCount() < 1) {
//            CommonClass.initCDataArray(context);
            CommonClass.initPDataArray(context);
//            }
//            cursor.close();
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            Cursor cursor = DatabaseHelper.db.query(getString(R.string.TBL_Profession), null, null,
                    null, null, null, "ProfName asc");
            if (cursor.getCount() > 0) {
                arrProfession.clear();
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    arrProfession.add(cursor.getString(cursor.getColumnIndex("ProfName")));
                }
            }
            cursor.close();
        }
    }


    private void alertCustomDialog(final String title, String message) {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(LPI_RegistrationActivity.this).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_exit_dialog, null);
            TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            Button btn_ok = (Button) dialogView.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialogView.findViewById(R.id.btn_no);
            promt_title.setText(title);
            txt_message.setText(message);
            btn_ok.setText("OK");
            if (title.equalsIgnoreCase("No Token Alert")) {
                btn_ok.setText("Sync");
            }
            btn_no.setVisibility(View.GONE);

            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    if (title.equalsIgnoreCase("Alert!")) {
                        onBackPressed();
                    } else if (title.equalsIgnoreCase("No Token Alert")) {
                        if (CommonClass.isConnected(LPI_RegistrationActivity.this)) {
                            new AsyncGetTokenDetails(new Callback<String>() {
                                @Override
                                public void execute(String result, String status) {
                                    if (result != null && result.equals("success")) {
                                        Cursor cursor = DatabaseHelper.db.query(
                                                getResources().getString(R.string.Tbl_TokenMst), null,
                                                "UserId=? and TokenType=? and isActive=?",
                                                new String[]{StaticVariables.UserLoginId, "P", "Y"}, null,
                                                null, null);
                                        if (cursor != null && cursor.getCount() > 0) {
                                            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                                                int total = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TotalCount")));
                                                int used = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TokenUsed")));
                                                if (total - used > 0) {
                                                    balanceToken = total - used;
                                                    strToken = cursor.getString(cursor.getColumnIndex("Token"));
                                                    input_reference.setText(strToken);
                                                    checkTokenValid();
                                                }
                                            }
                                        } else {
                                            alertCustomDialog("Token Alert", "Token is not available, please generate the token");
                                        }
                                        cursor.close();
                                    } else if (result != null && result.equals("error")) {
                                        Toast.makeText(LPI_RegistrationActivity.this, "Server not responding please " +
                                                "try again.", Toast.LENGTH_SHORT).show();
                                        alertCustomDialog("Alert", "Server not responding please try again.");
                                    } else if (result != null && !result.equals("")) {
                                        alertCustomDialog("Alert", result);
                                    } else {
                                        alertCustomDialog("Alert", "No result found.");
                                        Toast.makeText(LPI_RegistrationActivity.this, "No result found",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }, LPI_RegistrationActivity.this, StaticVariables.UserLoginId).execute();
                        } else {
                            alertCustomDialog("No Internet", "You are not connected to internet");
                        }
                    } else {
                        onBackPressed();
                    }
                }
            });

            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
