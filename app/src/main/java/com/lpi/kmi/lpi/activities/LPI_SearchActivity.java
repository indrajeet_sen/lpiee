package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.PublicUser.SearchModel;
import com.lpi.kmi.lpi.adapter.Adapters.LPI_SearchAdapter;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.stringcare.library.SC;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import nu.xom.Serializer;

@SuppressLint("Range")
public class LPI_SearchActivity extends AppCompatActivity {
    private static RecyclerView recyclerView;
    private static LPI_SearchAdapter searchsAdapter;
    private static LinearLayoutManager linearLayoutManager;
    private static ArrayList<SearchModel> mArraylistEventslist;
    Button btn_Search;
    View viewLayout;
    EditText input_Name;
    EditText input_MobileNo;
    EditText input_EmailID;
    EditText input_Date;
    TextView no_feeds_foundTextView;
    Calendar myCalendar = Calendar.getInstance();
    JSONObject errJSONObject = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_lpi_search);
        try {
            initUI();
//        initLocalCandidateData();
            UIListener();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initLocalCandidateData() {
        Cursor mCursor = null;

        try {
            if (DatabaseHelper.db == null) {
                SQLiteDatabase.loadLibs(getApplicationContext());
                DatabaseHelper.db = DatabaseHelper.getInstance(getApplicationContext()).
                        getWritableDatabase(SC.reveal(CommonClass.DBPassword));
            }
            mCursor = DatabaseHelper.db.query(getResources().getString(R.string.TBL_LPICandidateRegistration),
                    null, "SyncStatus=?",
                    new String[]{"pending"}, null, null, null, null);
            if (mCursor.getCount() > 0) {
                mArraylistEventslist.clear();
                byte[] imageBytes = null;
                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    SearchModel searchModel = new SearchModel();
                    String strName = (mCursor.getString(mCursor.getColumnIndex("FirstName"))) + " " + (mCursor.getString(mCursor.getColumnIndex("LastName")));
                    searchModel.setName(strName);
                    searchModel.setEmail_id(mCursor.getString(mCursor.getColumnIndex("EmailID")));
                    imageBytes = mCursor.getBlob(mCursor.getColumnIndex("ImageByte"));
//                                if (jsonObject.get("Images") != null && !(jsonObject.get("Images").toString().trim().equalsIgnoreCase("null"))) {
//                                    arrlst_image.add(jsonObject.get("Images").toString().getBytes("UTF-8"));
                    if (imageBytes != null) {
                        searchModel.setImage(imageBytes);
                    } else {
                        searchModel.setImage(imageBytes);
                    }
                    searchModel.setMobile_no(mCursor.getString(mCursor.getColumnIndex("MobileNo")));
                    searchModel.setStatus("Candidate Not Registered");
                    searchModel.setUser_id(mCursor.getString(mCursor.getColumnIndex("CandidateId")));
                    searchModel.setMobile_rec_id(mCursor.getString(mCursor.getColumnIndex("MobRefId")));
                    mArraylistEventslist.add(searchModel);
                }
                setRecyclerView();
                mCursor.close();
            } else {
                mCursor = DatabaseHelper.db.query(getResources().getString(R.string.TBL_SearchResult),
                        null, null, null, null, null, null, null);
                if (mCursor.getCount() > 0) {
                    mArraylistEventslist.clear();
                    byte[] imageBytes = null;
                    for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                        SearchModel searchModel = new SearchModel();
                        String strName = (mCursor.getString(mCursor.getColumnIndex("Name")));
                        searchModel.setName(strName);
                        imageBytes = mCursor.getBlob(mCursor.getColumnIndex("Images"));
//                                if (jsonObject.get("Images") != null && !(jsonObject.get("Images").toString().trim().equalsIgnoreCase("null"))) {
//                                    arrlst_image.add(jsonObject.get("Images").toString().getBytes("UTF-8"));
                        if (imageBytes != null) {
                            searchModel.setImage(imageBytes);
                        }
                        searchModel.setMobile_no(mCursor.getString(mCursor.getColumnIndex("Mobile")));
                        searchModel.setStatus(mCursor.getString(mCursor.getColumnIndex("PPAttempted")));
                        searchModel.setUser_id(mCursor.getString(mCursor.getColumnIndex("CandidateId")));
                        mArraylistEventslist.add(searchModel);
                    }
                    setRecyclerView();
                }
                mCursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void UIListener() {
        btn_Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    DatabaseHelper.InsertActivityTracker(LPI_SearchActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                            "A8007", "A8002", "Search button clicked",
                            "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                            StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                            "", "", "Pending");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (CommonClass.isConnected(LPI_SearchActivity.this)) {
                    new AsyncSearchCandidates(LPI_SearchActivity.this).execute();
                } else {
                    alertCustomDialog("Alert!", "No internet connection found, please connect internet and try again.");
                }


            }
        });

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        input_Date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*DatePickerDialog dialog = new DatePickerDialog(LPI_RegistrationActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setMaxDate(new Date().getTime());
                dialog.show();
                clearFocus();*/

                DatePickerDialog dialog = new DatePickerDialog(LPI_SearchActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.getDatePicker().setMaxDate(new Date().getTime());
                dialog.show();
            }
        });

    }

    LPI_SearchAdapter.onClickListener onClickListener = new LPI_SearchAdapter.onClickListener() {

        @Override
        public void onCardViewClick(View v) {

            try {
                DatabaseHelper.InsertActivityTracker(LPI_SearchActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                        "A8006", "A8006", "Search page called",
                        "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                        StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                        "", "", "Pending");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onProfileClick(int position, View view) {

        }

        @Override
        public void onStatusBarChartClick(int position, View view) {
//            StaticVariables.UserLoginId = mArraylistEventslist.get(position).getUser_id();
            try {
                DatabaseHelper.InsertActivityTracker(LPI_SearchActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                        "A8008", "A8009", "Psychometric test Status button click",
                        "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                        StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                        "", "", "Pending");
            } catch (Exception e) {
                e.printStackTrace();
            }

            Intent i = new Intent(LPI_SearchActivity.this, ViewResultActivity.class);
            i.putExtra("ExamId", "" + 4001);
            i.putExtra("AttemptedId", "");
            i.putExtra("CandidateId", "" + mArraylistEventslist.get(position).getUser_id());
            i.putExtra("initFrom", "LPI_SearchActivity");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            LPI_SearchActivity.this.finish();
        }

        @Override
        public void onViewClick(int position, View view) {
            try {
                DatabaseHelper.InsertActivityTracker(LPI_SearchActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                        "A8009", "A8009", "Click on View candidate information button",
                        "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                        StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                        "", "", "Pending");
            } catch (Exception e) {
                e.printStackTrace();
            }

            Intent i = new Intent(LPI_SearchActivity.this, LPI_ViewProfileActivity.class);
            i.putExtra("candidate_id", mArraylistEventslist.get(position).getUser_id());
            i.putExtra("initFrom", "LPI_SearchActivity");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            LPI_SearchActivity.this.finish();
        }

        @Override
        public void onFeedbackClick(int position, View view) {
            Intent intent = new Intent(LPI_SearchActivity.this, CandidateFeedbackActivity.class);
            intent.putExtra("CandidateId", mArraylistEventslist.get(position).getUser_id());
            intent.putExtra("CandidateName", mArraylistEventslist.get(position).getName());
            intent.putExtra("ExamType", "5");
            intent.putExtra("initFrom", "Search");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            LPI_SearchActivity.this.finish();
        }


        @Override
        public void onModifyClick(int position, View view) {
            try {
                DatabaseHelper.InsertActivityTracker(LPI_SearchActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                        "A8009", "A8009", "Click on Modify button for modify candidate information",
                        "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                        StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                        "", "", "Pending");
            } catch (Exception e) {
                e.printStackTrace();
            }
            Intent i = new Intent(LPI_SearchActivity.this, LPI_RegistrationActivity.class);
            i.putExtra("candidate_id", mArraylistEventslist.get(position).getUser_id());
            i.putExtra("mobile_rec_id", mArraylistEventslist.get(position).getMobile_rec_id());
            i.putExtra("initFrom", "LPI_SearchActivity");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            LPI_SearchActivity.this.finish();
        }

    };

    private void setRecyclerView() {
        if (mArraylistEventslist.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
//                    viewLayout.setVisibility(View.VISIBLE);
            no_feeds_foundTextView.setVisibility(View.GONE);
            searchsAdapter = new LPI_SearchAdapter(LPI_SearchActivity.this, mArraylistEventslist);
            linearLayoutManager = new LinearLayoutManager(LPI_SearchActivity.this);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemViewCacheSize(20);
            recyclerView.setDrawingCacheEnabled(true);
            recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
//            searchsAdapter.setHasStableIds(true);
            recyclerView.setAdapter(searchsAdapter);
            searchsAdapter.setOnItemClickListener(onClickListener);
        }else {
            recyclerView.setVisibility(View.GONE);
            no_feeds_foundTextView.setVisibility(View.VISIBLE);
        }
    }

    private void updateLabel() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        input_Date.setText(sdf.format(myCalendar.getTime()));
        input_Date.setHintTextColor(getResources().getColor(R.color.colorPrimary));
        input_Date.setError(null);
    }


    private void initUI() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_menu);
        setSupportActionBar(toolbar);
        actionBarSetup();
        mArraylistEventslist = new ArrayList<SearchModel>();
        btn_Search = (Button) findViewById(R.id.btn_Search);
        recyclerView = (RecyclerView) findViewById(R.id.lv_search);
        recyclerView.setVisibility(View.GONE);
        viewLayout = (View) findViewById(R.id.view);
        viewLayout.setVisibility(View.GONE);
        input_Name = (EditText) findViewById(R.id.input_Name);
        input_EmailID = (EditText) findViewById(R.id.input_EmailID);
        input_MobileNo = (EditText) findViewById(R.id.input_MobileNo);
        input_Date = (EditText) findViewById(R.id.input_Date);
        no_feeds_foundTextView = (TextView) findViewById(R.id.no_feeds_foundTextView);

        try {
            DatabaseHelper.InsertActivityTracker(LPI_SearchActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                    "A8006", "A8006", "Search page called",
                    "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                    StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                    "", "", "Pending");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().setTitle("Search");
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LPI_SearchActivity.this, LPI_TrainerMenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        LPI_SearchActivity.this.finish();
    }

    class AsyncSearchCandidates extends AsyncTask<String, Void, JSONObject> {
        Context context;
        ProgressDialog mProgressDialog;

        public AsyncSearchCandidates(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading..\nPlease wait..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            // TODO Auto-generated method stub
            String strResponse = "";
            try {
                strResponse = getCandidatesFromServer();
                if (strResponse != null && !strResponse.equalsIgnoreCase("{}")
                        && !strResponse.equalsIgnoreCase("null")) {

                    String strJsonArray = new JSONObject(strResponse).getJSONArray("Table").toString();
                    JSONArray jsonArray = new JSONArray(strJsonArray);
                    mArraylistEventslist.clear();
                    byte[] imageBytes = null;
                    try {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                            SearchModel searchModel = new SearchModel();
                            errJSONObject = jsonObject;
                            imageBytes = Base64.decode(jsonObject.getString("Images"), Base64.DEFAULT);
//                                if (jsonObject.get("Images") != null && !(jsonObject.get("Images").toString().trim().equalsIgnoreCase("null"))) {
//                                    arrlst_image.add(jsonObject.get("Images").toString().getBytes("UTF-8"));
                            if (imageBytes != null && !(jsonObject.get("Images").toString().trim().equalsIgnoreCase("null"))) {
                                searchModel.setImage(imageBytes);
                            }
                            searchModel.setName(jsonObject.getString("Name"));
                            searchModel.setMobile_no(jsonObject.getString("Mobile"));
                            searchModel.setStatus(jsonObject.getString("PPAttempted"));
                            searchModel.setUser_id(jsonObject.getString("CandidateId"));
                            searchModel.setProfileName(jsonObject.getString("ProfileName"));
                            mArraylistEventslist.add(searchModel);

                            /*DatabaseHelper.SaveSearchResult(context, DatabaseHelper.db, jsonObject.getString("CandidateId")
                                    , jsonObject.getString("Name"), jsonObject.getString("Mobile"),
                                    imageBytes, jsonObject.getString("ExamId"),
                                    jsonObject.getString("PPAttempted"),
                                    jsonObject.getString("CreateDte"), jsonObject.getString("ProfileName"));*/
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return new JSONObject(strResponse);
                } else {
                    return null;
                }
            } catch (Exception e) {
                return null;
            }
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(JSONObject result) {
            // TODO Auto-generated method stub
//            JSONObject errJSONObject = null;
            try {
                if (result != null && !result.toString().equalsIgnoreCase("{}")
                        && !result.toString().equalsIgnoreCase("null")) {
                    try {
                        /*String strJsonArray = result.getJSONArray("Table").toString();
                        JSONArray jsonArray = new JSONArray(strJsonArray);
                        mArraylistEventslist.clear();
                        byte[] imageBytes = null;

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                            SearchModel searchModel = new SearchModel();
                            errJSONObject = jsonObject;
                            imageBytes = Base64.decode(jsonObject.getString("Images"), Base64.DEFAULT);
//                                if (jsonObject.get("Images") != null && !(jsonObject.get("Images").toString().trim().equalsIgnoreCase("null"))) {
//                                    arrlst_image.add(jsonObject.get("Images").toString().getBytes("UTF-8"));
                            if (imageBytes != null && !(jsonObject.get("Images").toString().trim().equalsIgnoreCase("null"))) {
                                searchModel.setImage(imageBytes);
                            }
                            searchModel.setName(jsonObject.getString("Name"));
                            searchModel.setMobile_no(jsonObject.getString("Mobile"));
                            searchModel.setStatus(jsonObject.getString("PPAttempted"));
                            searchModel.setUser_id(jsonObject.getString("CandidateId"));
                            searchModel.setProfileName(jsonObject.getString("ProfileName"));
                            mArraylistEventslist.add(searchModel);

                            *//*DatabaseHelper.SaveSearchResult(context, DatabaseHelper.db, jsonObject.getString("CandidateId")
                                    , jsonObject.getString("Name"), jsonObject.getString("Mobile"),
                                    imageBytes, jsonObject.getString("ExamId"),
                                    jsonObject.getString("PPAttempted"),
                                    jsonObject.getString("CreateDte"), jsonObject.getString("ProfileName"));*//*
                        }*/
                        setRecyclerView();
                        mProgressDialog.dismiss();
                    } catch (Exception e) {
                        if (mProgressDialog != null) {
                            mProgressDialog.dismiss();
                        }
                        recyclerView.setVisibility(View.GONE);
                        no_feeds_foundTextView.setVisibility(View.VISIBLE);
                        if (errJSONObject.has("ResponseCode") && errJSONObject.get("ResponseCode").equals("1")) {
                            if ((errJSONObject.has("ErrorDescription")) && ("" + errJSONObject.get("ErrorDescription")).length() > 0) {
                                no_feeds_foundTextView.setText("" + errJSONObject.get("ErrorDescription"));
                                alertCustomDialog("Alert!", ("" + errJSONObject.get("ErrorDescription")));
                            } else {
                                no_feeds_foundTextView.setText("Server not responding, Please try again after sometime.");
                            }
                        } else {
                            no_feeds_foundTextView.setText("Something went wrong please try again.");
                        }
                        e.printStackTrace();
                    }
                } else {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    recyclerView.setVisibility(View.GONE);
                    no_feeds_foundTextView.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                }
                recyclerView.setVisibility(View.GONE);
                no_feeds_foundTextView.setVisibility(View.VISIBLE);
                no_feeds_foundTextView.setText("Something went wrong please try again.");
                e.printStackTrace();
            }
        }


        private String getCandidatesFromServer() {

            String strResp = "";
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.GetCandidateSearch));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            String strReqXML = reqBuild();
            inputPI1.setName("objXMLDoc");
            try {
                inputPI1.setValue(strReqXML);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL), 90000);

            try {
                LPIEEX509TrustManager.allowAllSSL();
//                FakeX509TrustManager.trustSSLCertificate(LPI_SearchActivity.this);
                androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                        context.getString(R.string.GetCandidateSearch), envelope);
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strResp = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return strResp;
        }

        private String reqBuild() {

            String str_XML = "";
            nu.xom.Element root = new nu.xom.Element("GetCandidateSearch");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(context));

            nu.xom.Element InscType = new nu.xom.Element("InscType");
            InscType.appendChild("" + StaticVariables.crnt_InscType);

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
            root.appendChild(InscType);

            nu.xom.Element Name = new nu.xom.Element("Name");
            Name.appendChild(input_Name.getText().toString().trim());
            root.appendChild(Name);

            nu.xom.Element Mobile = new nu.xom.Element("Mobile");
            Mobile.appendChild(input_MobileNo.getText().toString().trim());
            root.appendChild(Mobile);

            nu.xom.Element EmailId = new nu.xom.Element("EmailId");
            EmailId.appendChild(input_EmailID.getText().toString().trim());
            root.appendChild(EmailId);

            nu.xom.Element Date = new nu.xom.Element("Date");
            Date.appendChild(input_Date.getText().toString().trim());
            root.appendChild(Date);

            nu.xom.Element UserId = new nu.xom.Element("UserId");
            UserId.appendChild(StaticVariables.UserLoginId);
//            UserId.appendChild("70011354");
            root.appendChild(UserId);

            nu.xom.Document doc = new nu.xom.Document(root);
            try {
                OutputStream out = new ByteArrayOutputStream();
                Serializer serializer = new Serializer(System.out, "UTF-8");
                serializer.setOutputStream(out);
                serializer.setIndent(4);
                serializer.setMaxLength(64);
                serializer.write(doc);
                str_XML = out.toString();
                str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
                str_XML = str_XML.replaceAll("\n", "");
                str_XML = str_XML.replaceAll("\r", "");
                str_XML = str_XML.replaceAll("     ", "");
                str_XML = str_XML.replaceAll("         ", "");
                str_XML = str_XML.trim();
                Log.e("", "XMLStr==" + str_XML);
            } catch (IOException e) {
                Log.e("", "GetCandidateSearch Error=" + e.toString());
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "AsyncSearchCandidates", "GetCandidateSearch", e.toString());
            }
            return str_XML;
        }
    }

    private void alertCustomDialog(final String title, String message) {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(LPI_SearchActivity.this).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_exit_dialog, null);
            TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            Button btn_ok = (Button) dialogView.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialogView.findViewById(R.id.btn_no);
            promt_title.setText(title);
            txt_message.setText(message);
            btn_ok.setText("OK");
            btn_no.setVisibility(View.GONE);

            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });

            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}