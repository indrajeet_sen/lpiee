package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.AsyncGetTokenDetails;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.net.URLEncoder;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressLint("Range")
public class LPI_ShareAppActivity extends AppCompatActivity {
    //app_userID, app_userPassword,
    TextView app_link, app_refId;
    ImageButton btn_sms, btn_whats_app, btn_email;
    EditText app_emailid, edt_mobNo;
    ImageView contact_logo;
    String strCandidateID = "";
    String strFrom = "";
    private static final int RESULT_PICK_CONTACT = 8127;
    public static String strToken = "";
    public static int balanceToken = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_lpi_share_app);

        try {
            strFrom = getIntent().getExtras().getString("initFrom");
            strCandidateID = getIntent().getExtras().getString("candidate_id");

            if (strFrom != null && strFrom.trim().equalsIgnoreCase("notification") && !strFrom.trim().equalsIgnoreCase("")) {
                int NOTIFICATION_ID = getIntent().getExtras().getInt("notification_id");
                clearNotification(NOTIFICATION_ID);
            }
        } catch (Exception e) {
            try {
                strFrom = getIntent().getExtras().getString("initFrom");
                strCandidateID = getIntent().getExtras().getString("candidate_id");
            } catch (Exception ex) {
                strFrom = "";
                strCandidateID = "";
            }
        }

        initUI();
        uiClickListener();
    }

    String getMessage() {
        String message = "";
        if (strFrom != null && (strFrom.equals("LPI_RegistrationActivity") || strFrom.equals("LPI_ViewProfileActivity"))) {
            message = "Hi there,\n I am glad to share the LPI app with you, please download the app from " + app_link.getText().toString() + "\n"
                    + " and please log in with your registered email address " + app_emailid.getText().toString() + " & password: pass@123";
        } else {
            message = "Hi there,\n I am glad to share the LPI app with you, please download the app from " + app_link.getText().toString() + "\n"
                    + " and create your account by using your trainer reference code as " + app_refId.getText().toString() +
                    " and for subsequent login please use your registered email address & password as pass@123";
        }
        return message;
    }

    private void uiClickListener() {

        btn_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {
                    try {
                        Intent email = new Intent(Intent.ACTION_SEND);
                        email.putExtra(Intent.EXTRA_EMAIL, new String[]{app_emailid.getText().toString()});
                        email.putExtra(Intent.EXTRA_SUBJECT, "LPI EE Mobile App Link");
                        email.putExtra(Intent.EXTRA_TEXT, getMessage());
                        email.setType("message/rfc822");
                        startActivity(Intent.createChooser(email, "Choose an Email client :"));
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(LPI_ShareAppActivity.this, "Unable to send email", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btn_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    DatabaseHelper.InsertActivityTracker(LPI_ShareAppActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                            "A8005", "A8002", "Send link via SMS button clicked",
                            "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                            StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                            "", "", "Pending");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (isValidate()) {
                    if (app_link != null && edt_mobNo != null && edt_mobNo.getText().toString().trim().length() > 0 && app_link.getText().toString().toString().length() > 0) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + edt_mobNo.getText().toString().trim()));
                            intent.putExtra("sms_body", getMessage());
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (app_link != null && app_link.getText().toString().toString().length() > 0) {
                        try {
                            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                            sendIntent.putExtra("sms_body", getMessage());
                            sendIntent.setType("vnd.android-dir/mms-sms");
                            startActivity(sendIntent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        btn_whats_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    DatabaseHelper.InsertActivityTracker(LPI_ShareAppActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                            "A8005", "A8002", "Send link via WhatsApp button clicked",
                            "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                            StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                            "", "", "Pending");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                boolean isWhatsappInstalled = whatsappInstalledOrNot("com.whatsapp");
                if (isValidate()) {
                    if (isWhatsappInstalled) {
                        if (app_link != null && edt_mobNo != null && edt_mobNo.getText().toString().trim().length() > 0 && app_link.getText().toString().toString().length() > 0) {
//                            ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
//                            ClipData clip = ClipData.newPlainText("Candidate Info. ", "User ID: " + app_userID.getText().toString().toString()
//                                    + "\nPassword: " + app_userPassword.getText().toString().toString() + "\nLink: \n" + app_link.getText().toString().toString());
//                            clipboard.setPrimaryClip(clip);
//
//                            Uri mUri = Uri.parse("smsto:" + edt_mobNo.getText().toString().trim());
//                            Intent mIntent = new Intent(Intent.ACTION_SENDTO, mUri);
//                            //mIntent.setType("text/plain");
//                            mIntent.setPackage("com.whatsapp");
//                            mIntent.putExtra("sms_body", "User ID: " + app_userID.getText().toString().toString() + "\nPassword: " +
//                                    app_userPassword.getText().toString().toString() + "\nLink: \n" + app_link.getText().toString().toString());
//                            mIntent.putExtra("chat", true);
//                            startActivity(Intent.createChooser(mIntent, "Share with"));
//                            ToastMsg("Candidate information copied, Please pest details in WhatsApp conversation's message area and send.");

                            PackageManager packageManager = getPackageManager();
                            Intent i = new Intent(Intent.ACTION_VIEW);

                            try {
                                String url = "https://api.whatsapp.com/send?phone=" + edt_mobNo.getText().toString().trim() + "&text=" + URLEncoder.encode(getMessage(), "UTF-8");
                                i.setPackage("com.whatsapp");
                                i.setData(Uri.parse(url));
                                if (i.resolveActivity(packageManager) != null) {
                                    startActivity(i);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            /*try {
                                Intent sendIntent = new Intent();
                                sendIntent.setAction(Intent.ACTION_SEND);
                                sendIntent.putExtra(Intent.EXTRA_TEXT, "User ID: " + app_userID.getText().toString().toString() + "\n Password: " + app_userPassword.getText().toString().toString() + "\n Link: \n" + app_link.getText().toString().toString());
                                sendIntent.setType("text/plain");
                                sendIntent.setPackage("com.whatsapp");
                                startActivity(sendIntent);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }*/

                        } else if (app_link != null && app_link.getText().toString().toString().length() > 0) {

                            try {
                                Intent sendIntent = new Intent();
                                sendIntent.setAction(Intent.ACTION_SEND);
                                sendIntent.putExtra(Intent.EXTRA_TEXT, getMessage());
                                sendIntent.setType("text/plain");
                                sendIntent.setPackage("com.whatsapp");
                                startActivity(sendIntent);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, getMessage());
                        sendIntent.setType("text/plain");
                        startActivity(sendIntent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }
                }
            }
        });
        contact_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickContact();
            }
        });
    }

    private boolean isValidate() {

//        if (app_userID == null || app_userID.getText().toString().toString().length() < 1) {
//            ToastMsg("User Id not found!");
//            return false;
//        }
//        if (app_userPassword == null || app_userPassword.getText().toString().trim().length() < 1) {
//            ToastMsg("User Password not found!");
//            return false;
//        }
        if (app_link == null || app_link.getText().toString().toString().length() < 1) {
            ToastMsg("Mobile App link not found!");
            return false;
        }

        String eMailValid = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern Email_pattern = Pattern.compile(eMailValid);
        Matcher Email1_matcher = Email_pattern.matcher(app_emailid.getText().toString());
        boolean IsValidEmail = Email1_matcher.matches();
        if (!IsValidEmail) {
            ToastMsg(getResources().getString(R.string.email_field_valid));
            app_emailid.setError(getResources().getString(R.string.email_field_valid));
            app_emailid.requestFocus();
            return false;
        }

        if (app_refId == null || app_refId.getText().toString().trim().length() < 1) {
            ToastMsg("Reference Code/ Token not found!");
            showAlert("Alert", "Reference Code/ Token not found.");
            return false;
        }
        return true;
    }

    void ToastMsg(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

    private void initUI() {
        strToken = "";
        balanceToken = -1;
        app_link = (TextView) findViewById(R.id.app_link); //TM100019
//        app_userID = (TextView) findViewById(R.id.app_userID);
//        app_userPassword = (TextView) findViewById(R.id.app_userPassword);
        app_emailid = (EditText) findViewById(R.id.app_emailid);
        app_refId = (TextView) findViewById(R.id.app_refId);
        app_emailid.setText("");

//        app_refId.setText(StaticVariables.UserLoginId);
        app_link.setText(StaticVariables.AppLink);

        edt_mobNo = (EditText) findViewById(R.id.edt_mobNo);
        btn_sms = (ImageButton) findViewById(R.id.btn_sms);
        btn_whats_app = (ImageButton) findViewById(R.id.btn_whats_app);
        btn_email = (ImageButton) findViewById(R.id.btn_email);
        contact_logo = (ImageView) findViewById(R.id.contact_logo);


        String strMobileNo = "";
        String strMobileCountryCode = "";
        String strPassword = "";
        String strAppLink = "";
        String strCountry = "";
        String strEmail = "";

        try {
            if (strCandidateID != null && !strCandidateID.equals("")) {
                Cursor mCursor = DatabaseHelper.getCandidateInfo(LPI_ShareAppActivity.this, DatabaseHelper.db, strCandidateID);
                if (mCursor.getCount() > 0) {
                    for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {

                        strMobileNo = (mCursor.getString(mCursor.getColumnIndex("MobileNo")));
                        strMobileCountryCode = (mCursor.getString(mCursor.getColumnIndex("MobileCountryCode")));
                        strPassword = (mCursor.getString(mCursor.getColumnIndex("Password")));
                        strAppLink = (mCursor.getString(mCursor.getColumnIndex("AppLink")));
                        strCountry = (mCursor.getString(mCursor.getColumnIndex("Nationality")));
                        strEmail = (mCursor.getString(mCursor.getColumnIndex("EmailID")));
                    }
                    if (strMobileCountryCode == null || strMobileCountryCode.trim().equalsIgnoreCase("")) {
                        getCountryCode(strCountry.trim());
                    }
                    strMobileNo = strMobileNo.replace(" ", "");
                    edt_mobNo.setText(strMobileCountryCode + strMobileNo);
//                app_userPassword.setText(strPassword);
//                app_userID.setText(strCandidateID);
                    app_link.setText(strAppLink);
                    app_emailid.setText(strEmail);
//                    app_refId.setText(StaticVariables.UserLoginId);
                } else {
                    app_link.setText(StaticVariables.AppLink);
                    app_emailid.setText("");
//                    app_refId.setText(StaticVariables.UserLoginId);
                }
                mCursor.close();
            } else {
                app_link.setText(StaticVariables.AppLink);
                app_emailid.setText("");
//                app_refId.setText(StaticVariables.UserLoginId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            app_link.setText(StaticVariables.AppLink);
            app_emailid.setText("");
        }
//        if (strFrom != null && (strFrom.equals("LPI_TrainerMenuActivity") || strFrom.equals("ParticipantHomeActivity"))) {
//            app_refId.setText(StaticVariables.UserLoginId);
//        }
        getTokenDetails();

        if (balanceToken == 0) {
            showAlert("Referral Limit Alert", "Token use limit has been reached, please generate new referral code.");
        } else if (strToken == null || strToken.trim().equalsIgnoreCase("")) {
            showAlert("Referral Code Alert", "Please click on sync button below to proceed.");
        }
//        try {
//            int refId = DatabaseHelper.GetReferCount(LPI_ShareAppActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId);
//            DatabaseHelper.InsertReferenceDetails(LPI_ShareAppActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
//                    StaticVariables.UserLoginId, strCandidateID, refId + "", StaticVariables.UserLoginId, "N",
//                    "NA", "Free", "NA");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        try {
            DatabaseHelper.InsertActivityTracker(LPI_ShareAppActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                    "A8004", "A8002", "ShareApp Activity opened",
                    "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                    StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                    "", "", "Pending");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getTokenDetails() {
        try {
            Cursor cursor = DatabaseHelper.db.query(getResources().getString(R.string.Tbl_TokenMst), null,
                    "UserId=? and TokenType=? and isActive=?",
                    new String[]{StaticVariables.UserLoginId, "P", "Y"}, null, null, null, null);
            if (cursor != null && cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    int total = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TotalCount")));
                    int used = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TokenUsed")));
                    if (total - used > 0) {
                        balanceToken = total - used;
                /*TokenModel model = new TokenModel(cursor.getString(cursor.getColumnIndex("TokenType")),
                        cursor.getString(cursor.getColumnIndex("ValidFrom")),
                        cursor.getString(cursor.getColumnIndex("ValidTo")),*/
                        strToken = cursor.getString(cursor.getColumnIndex("Token"));
                    }
                }
            }

            app_refId.setText(strToken);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showAlert(final String title, String message) {
        AlertDialog.Builder builder = CommonClass.DialogOpen(LPI_ShareAppActivity.this, title, message);
        builder.setCancelable(false);
        String sTitle = "OK";
        if (title.equalsIgnoreCase("Referral Code Alert")) {
            sTitle = "Sync";
        }
        builder.setNegativeButton(sTitle, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (title.equals("Referral Limit Alert")) {

                } else if (title.equalsIgnoreCase("Referral Code Alert")) {
                    if (CommonClass.isConnected(LPI_ShareAppActivity.this)) {
                        new AsyncGetTokenDetails(new Callback<String>() {
                            @Override
                            public void execute(String result, String status) {
                                if (result != null && result.equals("success")) {
                                    Cursor cursor = DatabaseHelper.db.query(getResources().getString(R.string.Tbl_TokenMst), null, "UserId=? and TokenType=? and isActive=?",
                                            new String[]{StaticVariables.UserLoginId, "P", "Y"}, null, null, null, null);
                                    if (cursor != null && cursor.getCount() > 0) {
                                        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                                            int total = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TotalCount")));
                                            int used = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TokenUsed")));
                                            if (total - used > 0) {
                                                balanceToken = total - used;
                                                strToken = cursor.getString(cursor.getColumnIndex("Token"));
                                                app_refId.setText(strToken);
                                            }
                                        }
                                    } else {
                                        cursor = DatabaseHelper.db.query(getResources().getString(R.string.Tbl_TokenMst), null, "UserId=? and TokenType=?",
                                                new String[]{StaticVariables.UserLoginId, "P"}, null, null, null, null);
                                        if (cursor != null && cursor.getCount() > 0) {
                                            showAlert("Referral Limit Alert", "There is not any active Token or limit has been exceeded, please generate new Token/referral code.");
                                        } else {
                                            showAlert("Referral Code Alert", "Please click on sync button below to proceed.");
                                        }
                                    }
                                } else if (result != null && result.equals("error")) {
                                    Toast.makeText(LPI_ShareAppActivity.this, "Server not responding please try again.", Toast.LENGTH_SHORT).show();
                                    showAlert("Alert", "Server not responding please try again.");
                                } else if (result != null && !result.equals("")) {
                                    showAlert("Alert", result);
                                } else {
                                    showAlert("Alert", "No result found.");
                                    Toast.makeText(LPI_ShareAppActivity.this, "No result found", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, LPI_ShareAppActivity.this, StaticVariables.UserLoginId).execute();
                    } else {
                        showAlert("No Internet", "You are not connected to internet");
                    }
                } else if (title.equalsIgnoreCase("Alert") && strFrom != null &&
                        (strFrom.equals("LPI_TrainerMenuActivity") || strFrom.equals("ParticipantHomeActivity"))) {

                } else {
                    onBackPressed();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void clearNotification(int NOTIFICATION_ID) {
        NotificationManager notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
    }

    @Override
    public void onBackPressed() {
        if ((!StaticVariables.isLogin) && (strFrom != null && strFrom.trim().equalsIgnoreCase("notification") && !strFrom.trim().equalsIgnoreCase(""))) {
            Intent intent = new Intent(LPI_ShareAppActivity.this, Login.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else {
            if (StaticVariables.UserType.trim().equalsIgnoreCase("C")) {
                Intent intent = new Intent(LPI_ShareAppActivity.this, ParticipantHomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if (StaticVariables.UserType.trim().equalsIgnoreCase("T")) {
                Intent intent = new Intent(LPI_ShareAppActivity.this, LPI_TrainerMenuActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else {
                super.onBackPressed();
            }
        }
        LPI_ShareAppActivity.this.finish();
    }

    private boolean whatsappInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    public void pickContact() {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, RESULT_PICK_CONTACT);
    }

    String getCountryCode(String Country) {

        try {
            Cursor mCursor = DatabaseHelper.db.query(LPI_ShareAppActivity.this.getResources().getString(R.string.TBL_Country),
                    null, null, null, null, null, null);
            if (mCursor.getCount() < 1) {
                CommonClass.initPDataArray(LPI_ShareAppActivity.this);
            }
            String CountryCode = "";
            CountryCode = DatabaseHelper.GetCountryCode(LPI_ShareAppActivity.this, DatabaseHelper.db, Country);
            return CountryCode;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RESULT_PICK_CONTACT:
                    contactPicked(data);
                    break;
            }
        } else {
            Log.e("MainActivity", "Failed to pick contact");
        }
    }

    private void contactPicked(Intent data) {
        Cursor cursor = null;
        try {
            String phoneNo = null;
            String name = null;
            Uri uri = data.getData();
            cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            phoneNo = cursor.getString(phoneIndex);
            name = cursor.getString(nameIndex);
            //textView1.setText(name);
            phoneNo = phoneNo.replace(" ", "");
            edt_mobNo.setText(phoneNo);
            edt_mobNo.setSelection(edt_mobNo.length());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
