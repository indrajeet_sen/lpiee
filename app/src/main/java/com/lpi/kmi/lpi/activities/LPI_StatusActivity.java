package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.PublicUser.SearchModel;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.lpi.kmi.lpi.fragment.AttemptedStatusFragment;
import com.lpi.kmi.lpi.fragment.NonAttemptedStatusFragment;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import nu.xom.Serializer;

@SuppressLint("Range")
public class LPI_StatusActivity extends AppCompatActivity {

    private static ArrayList<SearchModel> mArraylistEventslistComplete;
    private static ArrayList<SearchModel> mArraylistEventslistIncomplete;
    ImageView RefreshGrpTopic, RefreshDate;
    TextView refDate;
    private static TabLayout tabLayout;
    private static ViewPager viewPager;
    Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_lpi_status);

        initUI();
    }

    private void initUI() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_menu);
        setSupportActionBar(toolbar);
        actionBarSetup();
        viewPager = (ViewPager) findViewById(R.id.pager_status);
        mArraylistEventslistComplete = new ArrayList<SearchModel>();
        mArraylistEventslistIncomplete = new ArrayList<SearchModel>();
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setVisibility(View.VISIBLE);
        RefreshGrpTopic = (ImageView) findViewById(R.id.RefreshGrpTopic);
        RefreshDate = (ImageView) findViewById(R.id.RefreshDate);
        refDate = (TextView) findViewById(R.id.refDate);
        if (StaticVariables.statusDate.equals("")) {
            refDate.setText("Select Date");
            try {
                Cursor mCursor = DatabaseHelper.db.query(getResources().getString(R.string.TBL_SearchResult),
                        null, null, null, null, null, null, null);
                if (mCursor.getCount() > 0) {
                    mArraylistEventslistComplete.clear();
                    mArraylistEventslistIncomplete.clear();
                    byte[] imageBytes = null;
                    for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                        SearchModel searchModel = new SearchModel();
                        String strName = (mCursor.getString(mCursor.getColumnIndex("Name")));
                        searchModel.setName(strName);
                        imageBytes = mCursor.getBlob(mCursor.getColumnIndex("Images"));
                        //                                if (jsonObject.get("Images") != null && !(jsonObject.get("Images").toString().trim().equalsIgnoreCase("null"))) {
                        //                                    arrlst_image.add(jsonObject.get("Images").toString().getBytes("UTF-8"));
                        if (imageBytes != null) {
                            searchModel.setImage(imageBytes);
                        }

                        searchModel.setMobile_no(mCursor.getString(mCursor.getColumnIndex("Mobile")));
                        searchModel.setStatus(mCursor.getString(mCursor.getColumnIndex("PPAttempted")));
                        searchModel.setUser_id(mCursor.getString(mCursor.getColumnIndex("CandidateId")));
                        searchModel.setProfileName(mCursor.getString(mCursor.getColumnIndex("ProfileName")));
                        if (mCursor.getString(mCursor.getColumnIndex("PPAttempted")).equalsIgnoreCase("Y")) {
                            mArraylistEventslistComplete.add(searchModel);
                        } else {
                            mArraylistEventslistIncomplete.add(searchModel);
                        }
                    }
                    setupViewPager(viewPager);
                    mCursor.close();
                } else {
                    AsyncGetCandidatesStatus status = new AsyncGetCandidatesStatus();
                    if (status.getStatus() != AsyncTask.Status.RUNNING)
                        new AsyncGetCandidatesStatus(LPI_StatusActivity.this).execute();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (CommonClass.isConnected(LPI_StatusActivity.this)) {
            refDate.setText(StaticVariables.statusDate);
            AsyncGetCandidatesStatus status = new AsyncGetCandidatesStatus();
            if (status.getStatus() != AsyncTask.Status.RUNNING)
                new AsyncGetCandidatesStatus(LPI_StatusActivity.this).execute();
        } else {
            alertCustomDialog("Alert!", "No internet connection found, please connect internet and try again.");
        }


        RefreshGrpTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonClass.isConnected(LPI_StatusActivity.this)) {
                    AsyncGetCandidatesStatus status = new AsyncGetCandidatesStatus();
                    if (status.getStatus() != AsyncTask.Status.RUNNING)
                        new AsyncGetCandidatesStatus(LPI_StatusActivity.this).execute();
                } else {
                    alertCustomDialog("Alert!", "No internet connection found, please connect internet and try again.");
                }
            }
        });

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                try {
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    StaticVariables.statusDate = sdf.format(myCalendar.getTime());
                    refDate.setText(StaticVariables.statusDate);

                    if (CommonClass.isConnected(LPI_StatusActivity.this)) {
                        AsyncGetCandidatesStatus status = new AsyncGetCandidatesStatus();
                        if (status.getStatus() != AsyncTask.Status.RUNNING)
                            new AsyncGetCandidatesStatus(LPI_StatusActivity.this).execute();
                    } else {
                        alertCustomDialog("Alert!", "No internet connection found, please connect internet and try again.");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        };

        RefreshDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dialog = new DatePickerDialog(LPI_StatusActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.getDatePicker().setMaxDate(new Date().getTime());
                dialog.show();
            }
        });

        try {
            DatabaseHelper.InsertActivityTracker(getApplicationContext(), DatabaseHelper.db,
                    StaticVariables.UserLoginId,
                    "A80014", "A80014", "Status page called",
                    "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                    StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                    "", "", "Pending");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().setTitle("Status");
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }


    private void alertCustomDialog(final String title, String message) {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(LPI_StatusActivity.this).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_exit_dialog, null);
            TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            Button btn_ok = (Button) dialogView.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialogView.findViewById(R.id.btn_no);
            promt_title.setText(title);
            txt_message.setText(message);
            btn_ok.setText("OK");
            btn_no.setVisibility(View.GONE);

            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }
            });

            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LPI_StatusActivity.this, LPI_TrainerMenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        LPI_StatusActivity.this.finish();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new AttemptedStatusFragment(mArraylistEventslistComplete), "Attempted");
        adapter.addFragment(new NonAttemptedStatusFragment(mArraylistEventslistIncomplete), "Not Attempted");
        viewPager.setAdapter(adapter);
        viewPager.invalidate();
        viewPager.refreshDrawableState();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    class AsyncGetCandidatesStatus extends AsyncTask<String, Void, JSONObject> {
        Context context;
        ProgressDialog mProgressDialog;

        public AsyncGetCandidatesStatus() {

        }

        public AsyncGetCandidatesStatus(Context context) {
            this.context = context;
        }

        @Override
        protected void onCancelled(JSONObject jsonObject) {
            super.onCancelled(jsonObject);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading..\nPlease wait..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            // TODO Auto-generated method stub
            String strResponse = "";
            try {
                strResponse = getCandidatesStatusFromServer();
                if (strResponse != null && !strResponse.equalsIgnoreCase("{}")
                        && !strResponse.equalsIgnoreCase("null")) {
                    //return new JSONObject(strResponse);

                    try {
                        mArraylistEventslistComplete.clear();
                        mArraylistEventslistIncomplete.clear();
                        if (strResponse != null && !strResponse.toString().equalsIgnoreCase("{}")
                                && !strResponse.toString().equalsIgnoreCase("null")) {
//                    ArrayList<String> arrlst_name = new ArrayList<String>();
//                    ArrayList<String> arrlst_userid = new ArrayList<String>();
//                    ArrayList<byte[]> arrlst_image = new ArrayList<byte[]>();
//                    ArrayList<String> arrlst_mob = new ArrayList<String>();
//                    ArrayList<String> arrlst_status = new ArrayList<String>();
                            String strJsonArray = new JSONObject(strResponse).getJSONArray("Table").toString();
                            JSONArray jsonArray = new JSONArray(strJsonArray);

                            byte[] imageBytes = null;
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                                if (jsonObject.has("ExamId") && jsonObject.getString("ExamId").equals("4001")) {
                                    imageBytes = Base64.decode(jsonObject.getString("Images"), Base64.DEFAULT);
                                    SearchModel searchModel = new SearchModel();

                                    searchModel.setName(jsonObject.getString("Name"));
                                    if (imageBytes != null && !(jsonObject.get("Images").toString().trim().equalsIgnoreCase("null"))) {
                                        searchModel.setImage(imageBytes);
                                    }
                                    searchModel.setMobile_no(jsonObject.getString("Mobile"));
                                    searchModel.setStatus(jsonObject.getString("PPAttempted"));
                                    searchModel.setUser_id(jsonObject.getString("CandidateId"));
                                    searchModel.setProfileName(jsonObject.getString("ProfileName"));
                                    if (jsonObject.getString("PPAttempted").equalsIgnoreCase("Y")) {
                                        mArraylistEventslistComplete.add(searchModel);
                                    } else {
                                        mArraylistEventslistIncomplete.add(searchModel);
                                    }
                                    DatabaseHelper.SaveSearchResult(context, DatabaseHelper.db, jsonObject.getString("CandidateId")
                                            , jsonObject.getString("Name"), jsonObject.getString("Mobile"),
                                            imageBytes, jsonObject.getString("ExamId"),
                                            jsonObject.getString("PPAttempted"),
                                            jsonObject.getString("CreateDte"), jsonObject.getString("ProfileName"));
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return new JSONObject();
                } else {
                    return null;
                }
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            // TODO Auto-generated method stub
            mProgressDialog.dismiss();
            tabLayout.setVisibility(View.VISIBLE);

            setupViewPager(viewPager);
        }


        private String getCandidatesStatusFromServer() {
            String strResp = "";
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.GetCandidateSearch));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            String strReqXML = reqBuild();
            inputPI1.setName("objXMLDoc");
            try {
                inputPI1.setValue(strReqXML);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));

            try {
                LPIEEX509TrustManager.allowAllSSL();
//                FakeX509TrustManager.trustSSLCertificate(LPI_StatusActivity.this);
                androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                        context.getString(R.string.GetCandidateSearch), envelope);
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strResp = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return strResp;
        }

        private String reqBuild() {

            String str_XML = "";
            nu.xom.Element root = new nu.xom.Element("GetCandidateSearch");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(context));

            nu.xom.Element InscType = new nu.xom.Element("InscType");
            InscType.appendChild("" + StaticVariables.crnt_InscType);

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
            root.appendChild(InscType);

            nu.xom.Element FirstName = new nu.xom.Element("Name");
            FirstName.appendChild("");
            root.appendChild(FirstName);

            nu.xom.Element LastName = new nu.xom.Element("Mobile");
            LastName.appendChild("");
            root.appendChild(LastName);

            nu.xom.Element Gender = new nu.xom.Element("EmailId");
            Gender.appendChild("");
            root.appendChild(Gender);

            nu.xom.Element strDate = new nu.xom.Element("Date");
            if (refDate.getText().toString().equalsIgnoreCase("Select Date")) {
                strDate.appendChild("");
            } else {
                strDate.appendChild(refDate.getText().toString());
            }
            root.appendChild(strDate);

            nu.xom.Element UserId = new nu.xom.Element("UserId");
            UserId.appendChild(StaticVariables.UserLoginId);
//            UserId.appendChild("70011354");
            root.appendChild(UserId);

            nu.xom.Document doc = new nu.xom.Document(root);
            try {
                OutputStream out = new ByteArrayOutputStream();
                Serializer serializer = new Serializer(System.out, "UTF-8");
                serializer.setOutputStream(out);
                serializer.setIndent(4);
                serializer.setMaxLength(64);
                serializer.write(doc);
                str_XML = out.toString();
                str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
                str_XML = str_XML.replaceAll("\n", "");
                str_XML = str_XML.replaceAll("\r", "");
                str_XML = str_XML.replaceAll("     ", "");
                str_XML = str_XML.replaceAll("         ", "");
                str_XML = str_XML.trim();
                Log.e("", "XMLStr==" + str_XML);
            } catch (IOException e) {
                Log.e("", "GetCandidateSearch Error=" + e.toString());
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "AsyncSearchCandidates", "GetCandidateSearch", e.toString());
            }
            return str_XML;
        }
    }
}