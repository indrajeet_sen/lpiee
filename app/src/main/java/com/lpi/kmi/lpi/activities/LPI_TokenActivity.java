package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.AsyncGetTokenDetails;
import com.lpi.kmi.lpi.activities.AsyncTasks.AsyncSubmitTokenDetails;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.CustomDialog;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import nu.xom.Serializer;

@SuppressLint("Range")
public class LPI_TokenActivity extends AppCompatActivity {

    public static LinearLayout linToken, lin_heading;
    public static ImageView RefreshToken, btnGenerateToken;
    public static Calendar myCalendar = Calendar.getInstance();
    public static RecyclerView recyclerView_token;
    public static TokenAdapter tokenAdapter;
    public static LinearLayoutManager linearLayoutManager;
    public static ArrayList<TokenModel> mArrayTokenList = new ArrayList<TokenModel>();
    public static Dialog dialog = null;
    public static ArrayList<String> arrTokenValues = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_lpi__token);
        InitUI();
        UIClickListener();
        GetTokenDetails();
    }

    private void GetTokenDetails() {
        Cursor cursor;
        try {
            cursor = DatabaseHelper.db.query(getResources().getString(R.string.Tbl_TokenMst), null, "UserId=?",
                    new String[]{StaticVariables.UserLoginId}, null, null, "date(validto) desc");
            if (cursor.getCount() > 0) {
                setTokenDataToUI();
            } else {
                if (CommonClass.isConnected(LPI_TokenActivity.this)) {
                    new AsyncGetTokenDetails(new Callback<String>() {
                        @Override
                        public void execute(String result, String status) {
                            if (result != null && result.equals("success")) {
                                setTokenDataToUI();
                            } else if (result != null && result.equals("error")) {
                                Toast.makeText(LPI_TokenActivity.this, "Server not responding please try again.", Toast.LENGTH_SHORT).show();
                            } else if (result != null && !result.equals("")) {
                                Toast.makeText(LPI_TokenActivity.this, result, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(LPI_TokenActivity.this, "No result found", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, LPI_TokenActivity.this, StaticVariables.UserLoginId).execute();
                } else {
                    Toast.makeText(LPI_TokenActivity.this, "No Internet connection found, please try again after connecting to internet", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTokenDataToUI() {
        mArrayTokenList.clear();
        try {
            Cursor cursor = DatabaseHelper.db.query(getResources().getString(R.string.Tbl_TokenMst), null, "UserId=?",
                    new String[]{StaticVariables.UserLoginId}, null, null, "date(validto) desc");
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int total = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TotalCount")));
                int used = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TokenUsed")));
                int bal = total - used;
                TokenModel model = new TokenModel(cursor.getString(cursor.getColumnIndex("TokenType")),
                        cursor.getString(cursor.getColumnIndex("ValidFrom")),
                        cursor.getString(cursor.getColumnIndex("ValidTo")),
                        cursor.getString(cursor.getColumnIndex("Token")),
                        "" + used, "" + bal, "" + total,
                        cursor.getString(cursor.getColumnIndex("isActive")),
                        cursor.getString(cursor.getColumnIndex("isExpired")));
                mArrayTokenList.add(model);
            }
            if (mArrayTokenList.size() > 0) {
                setAdapter();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void InitUI() {

//        linToken = (LinearLayout) findViewById(R.id.linToken);
        RefreshToken = (ImageView) findViewById(R.id.btnRefreshToken);
        btnGenerateToken = (ImageView) findViewById(R.id.btnGenerateToken);
        recyclerView_token = (RecyclerView) findViewById(R.id.lv_token);
        lin_heading = (LinearLayout) findViewById(R.id.lin_heading);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_menu);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(5);
        }
        setSupportActionBar(toolbar);
        actionBarSetup();

        arrTokenValues.clear();
        if(StaticVariables.UserLoginId.equalsIgnoreCase("LPISUP01")){
            arrTokenValues.add("10");
            arrTokenValues.add("20");
            arrTokenValues.add("30");
            arrTokenValues.add("40");
            arrTokenValues.add("50");
            arrTokenValues.add("60");
            arrTokenValues.add("70");
            arrTokenValues.add("80");
            arrTokenValues.add("90");
            arrTokenValues.add("100");
        }else {
            arrTokenValues.add("10");
            arrTokenValues.add("20");
        }


    }

    private void showAlert(final String Title, String Message) {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(this).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_ok_dialog, null);
            final TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            final TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            Button btn_ok = (Button) dialogView.findViewById(R.id.btn_yes);
            final ProgressBar progressBar = (ProgressBar)dialogView.findViewById(R.id.token_progress);
            final ImageView imageView = (ImageView)dialogView.findViewById(R.id.img_success);
            progressBar.setVisibility(View.GONE);
            imageView.setVisibility(View.GONE);

            promt_title.setText(Title);
            txt_message.setText(Message);
            btn_ok.setText("OK");
            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Title.equalsIgnoreCase("Alert")) {
                        alertDialog.dismiss();
                        showGenerareToken();
                    }else {
                        alertDialog.dismiss();
                        setTokenDataToUI();
                    }
                }
            });

            if (Title.equalsIgnoreCase("Token Request")) {
                progressBar.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        txt_message.setText("Your request is accepted, please wait...");
                    }

                }, 4000);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        txt_message.setText("Your request will be approved shortly, please wait...");
                    }

                }, 4000);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        txt_message.setTypeface(txt_message.getTypeface(), Typeface.BOLD);
                        txt_message.setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
                        txt_message.setText("Congratulations! Your request is approved");
                        progressBar.setVisibility(View.GONE);
                        imageView.setVisibility(View.VISIBLE);
                    }

                }, 5000);
            }

            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void UIClickListener() {
        btnGenerateToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean active = false;
                for (int i = 0; i < mArrayTokenList.size(); i++) {
                    if (mArrayTokenList.get(i).isActive.equalsIgnoreCase("Y"))
                        active = true;
                }
                if (active) {
                    showAlert("Active Token", "You already have an active token available to consume, please raise a new request after consuming the current active token.");
                } else {
                    showAlert("Alert", "You have exhausted all the tokens required to register your trainees (respondents). Raise a new token request by clicking on the ( OK )  button");
                }
//                showGenerareToken();
            }
        });
        RefreshToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CommonClass.isConnected(LPI_TokenActivity.this)) {
                    new AsyncGetTokenDetails(new Callback<String>() {
                        @Override
                        public void execute(String result, String status) {
                            if (result != null && result.equals("success")) {
                                setTokenDataToUI();
                            } else if (result != null && result.equals("error")) {
                                Toast.makeText(LPI_TokenActivity.this, "Server not responding please try again.", Toast.LENGTH_SHORT).show();
                            } else if (result != null && !result.equals("")) {
                                Toast.makeText(LPI_TokenActivity.this, result, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(LPI_TokenActivity.this, "No result found", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, LPI_TokenActivity.this, StaticVariables.UserLoginId).execute();
                } else {
                    Toast.makeText(LPI_TokenActivity.this, "No Internet connection found, please try again after connecting to internet", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showGenerareToken() {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(LPI_TokenActivity.this).create();
        alertDialog.setCancelable(false);
        try {
            myCalendar = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            StaticVariables.statusDate = sdf.format(myCalendar.getTime());

            LayoutInflater inflater = LPI_TokenActivity.this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_token_dialog, null);
            TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            final EditText selector_date = (EditText) dialogView.findViewById(R.id.selector_date);
            final EditText selector_token = (EditText) dialogView.findViewById(R.id.selector_token);
            final EditText selector_To_date = (EditText) dialogView.findViewById(R.id.selector_To_date);
            ImageView btn_date = (ImageView) dialogView.findViewById(R.id.btn_date);
            ImageView btn_To_date = (ImageView) dialogView.findViewById(R.id.btn_To_date);
            Button btn_tokenGenerate = (Button) dialogView.findViewById(R.id.btn_tokenGenerate);
            Button btn_no = (Button) dialogView.findViewById(R.id.btn_no);
            final RadioButton rdo_public = (RadioButton) dialogView.findViewById(R.id.rdo_public);
            final RadioButton rdo_project = (RadioButton) dialogView.findViewById(R.id.rdo_project);
            RadioGroup rdogrp_tokenType = (RadioGroup) dialogView.findViewById(R.id.rdogrp_tokenType);
            final LinearLayout lin_To_date = (LinearLayout) dialogView.findViewById(R.id.lin_To_date);
            final TextInputLayout input_layout_date = (TextInputLayout) dialogView.findViewById(R.id.input_layout_date);
            final String[] callFrom = {""};

            selector_date.setText(StaticVariables.statusDate);
            myCalendar.add(Calendar.DATE, 15);
            selector_To_date.setText(sdf.format(myCalendar.getTime()));

            final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    // TODO Auto-generated method stub
                    try {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        StaticVariables.statusDate = sdf.format(myCalendar.getTime());
                        if (callFrom[0].equalsIgnoreCase("FromDate")) {
                            selector_date.setText(StaticVariables.statusDate);
                        } else {
                            selector_To_date.setText(StaticVariables.statusDate);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            };

            btn_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatePickerDialog dialog = new DatePickerDialog(LPI_TokenActivity.this,
                            android.R.style.Theme_Holo_Light_Dialog, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH));
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.getDatePicker().setMinDate(new Date().getTime());
                    callFrom[0] = "FromDate";
                    dialog.show();
                }
            });
            btn_To_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatePickerDialog dialog = new DatePickerDialog(LPI_TokenActivity.this,
                            android.R.style.Theme_Holo_Light_Dialog, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH));
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.getDatePicker().setMinDate(new Date().getTime());
                    callFrom[0] = "ToDate";
                    dialog.show();
                }
            });
            rdogrp_tokenType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {

                    if (rdo_project.isChecked()) {
                        lin_To_date.setVisibility(View.GONE);
                        input_layout_date.setHint("Select Date(DD-MM-YYYY)");
                    } else if (rdo_public.isChecked()) {
                        lin_To_date.setVisibility(View.VISIBLE);
                        input_layout_date.setHint("Select Date From(DD-MM-YYYY)");
                    }
                }
            });
            btn_tokenGenerate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (selector_date != null || selector_date.getText().toString().trim().length() > 7) {
                        String value = selector_date.getText().toString().trim();
                        String[] arrValue = value.split("-");
                        if (arrValue.length == 3) {
                            String day = "", month = "";
                            if (Integer.parseInt(arrValue[0]) < 10 && (arrValue[0].length() < 2)) {
                                day = ("0" + (arrValue[0]));
                            } else {
                                day = ("" + (arrValue[0]));
                            }
                            if (Integer.parseInt(arrValue[1]) < 10 && (arrValue[0].length() < 2)) {
                                month = ("0" + (arrValue[1]));
                            } else {
                                month = ("" + (arrValue[1]));
                            }
                            value = (day + "-" + month + "-" + (arrValue[2]));
                            selector_date.setText(value);
                        }
                    }

                    if (selector_date == null || selector_date.getText().toString().trim().length() < 1) {
                        Toast toast = Toast.makeText(LPI_TokenActivity.this, "Please select date", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    } else if (!isValidFormat(selector_date.getText().toString().trim())) {
                        Toast toast = Toast.makeText(LPI_TokenActivity.this, "Please select valid date", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    } else if (selector_token.getText().toString().trim().length() < 1) {
                        Toast toast = Toast.makeText(LPI_TokenActivity.this, "Please token from list.", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    } else if (selector_date != null && isValidFormat(selector_date.getText().toString().trim())) {
                        String strTokenType = "";
                        if (rdo_public.isChecked()) {
                            strTokenType = "P";
                        } else if (rdo_project.isChecked()) {
                            strTokenType = "C";
                        }
                        alertDialog.dismiss();
                        try {
                            if (CommonClass.isConnected(LPI_TokenActivity.this)) {
                                new AsyncSubmitTokenDetails(LPI_TokenActivity.this,
                                        StaticVariables.UserLoginId, strTokenType, selector_date.getText().toString().trim(),
                                        selector_To_date.getText().toString().trim(), selector_token.getText().toString().trim(), new Callback<String>() {
                                    @Override
                                    public void execute(String result, String status) {
                                        new AsyncGetTokenDetails(new Callback<String>() {
                                            @Override
                                            public void execute(String result, String status) {
                                                if (result != null && result.equals("success")) {
//                                                    showAlert("Request Submitted","A request has been raised for 10 new tokens, you will receive a notification when the request is approved. It may take a day or two.");
                                                    showAlert("Token Request","A request is being raised for "+selector_token.getText().toString().trim()+" new tokens. Please wait...");
                                                } else if (result != null && result.equals("error")) {
                                                    Toast.makeText(LPI_TokenActivity.this, "Server not responding please try again.", Toast.LENGTH_SHORT).show();
                                                } else if (result != null && !result.equals("")) {
                                                    Toast.makeText(LPI_TokenActivity.this, result, Toast.LENGTH_SHORT).show();
                                                } else {
                                                    Toast.makeText(LPI_TokenActivity.this, "No result found", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }, LPI_TokenActivity.this, StaticVariables.UserLoginId).execute();
                                    }
                                }).execute();
                            } else {
                                Toast.makeText(LPI_TokenActivity.this, "No Internet connection found, please try again after connecting to internet", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                                /*TokenModel model = new TokenModel(strTokenType, selector_date.getText().toString().trim(),
                                        selector_To_date.getText().toString().trim(), selector_token.getText().toString().trim(),
                                        1 + "", "0");
                                mArrayTokenList.add(model);
                                tokenAdapter.notifyDataSetChanged();*/
                    }
                }
            });
            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });
            selector_token.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new CustomDialog(LPI_TokenActivity.this, arrTokenValues, "Select Token", 111, selector_token, (new Callback<String>() {
                        @Override
                        public void execute(String result, String status) {
//                                    txt_prefix_error.setVisibility(View.GONE);
                            selector_token.setText(result.trim());
                        }
                    })).show();
                }
            });
            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        getSupportActionBar().setTitle("My Tokens");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setAdapter() {
        if (mArrayTokenList.size() > 0) {
            recyclerView_token.setVisibility(View.VISIBLE);
            lin_heading.setVisibility(View.VISIBLE);
            tokenAdapter = new TokenAdapter(LPI_TokenActivity.this, mArrayTokenList);
            linearLayoutManager = new LinearLayoutManager(LPI_TokenActivity.this);
            recyclerView_token.setLayoutManager(linearLayoutManager);
            recyclerView_token.setHasFixedSize(true);
            recyclerView_token.setAdapter(tokenAdapter);
//            tokenAdapter.notifyDataSetChanged();
        } else {
            recyclerView_token.setVisibility(View.GONE);
            lin_heading.setVisibility(View.GONE);
        }
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LPI_TokenActivity.this, LPI_TrainerMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        LPI_TokenActivity.this.finish();
    }

//    void dummyData() {
//        mArrayTokenList.clear();
//        for (int i = 0; i < 5; i++) {
//            TokenModel model = new TokenModel("Project " + i, (i + 1) + "-10-2018", (i + 5) + "-10-2018", "12334" + i, i + "/20",
//                    (5 - i) + "");
//            mArrayTokenList.add(model);
//        }
//    }

    public static boolean isValidFormat(String value) {

        Date date = null;
        String format = "dd-MM-yyyy";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            date = sdf.parse(value);
            if (!value.equals(sdf.format(date))) {
                date = null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return date != null;
    }

    public class TokenAdapter extends RecyclerView.Adapter {

        ArrayList<TokenModel> tokenArrayList;
        Context context;

        public TokenAdapter(Context context, ArrayList<TokenModel> tokenArrayList) {
            this.context = context;
            this.tokenArrayList = tokenArrayList;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v;
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.token_row, parent, false);
            return new VHItem(v);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            try {
                if (tokenArrayList.size() > 0) {
                    if (tokenArrayList.get(position) != null) {
                        VHItem token_data = (VHItem) holder;
                        String tType = "";
                        if (tokenArrayList.get(position).getToken_type() != null && tokenArrayList.get(position).getToken_type().trim().equals("P")) {
                            tType = "Public";
                        } else {
                            tType = "ClassRoom";
                        }
                        token_data.txt_token_type.setText(tType);
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                        SimpleDateFormat sdfInput = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                        Date fDate = sdfInput.parse(tokenArrayList.get(position).getToken_date());
                        Date tDate = sdfInput.parse(tokenArrayList.get(position).getToken_To_date());

                        token_data.txt_token_date.setText("From Date : " + sdf.format(fDate));
                        token_data.txt_token_To_date.setText("To Date :      " + sdf.format(tDate));
                        token_data.txt_token.setText("Your Token : " + tokenArrayList.get(position).getToken_no());
                        token_data.txt_token_count.setText(tokenArrayList.get(position).getToken_count() + " / " +
                                tokenArrayList.get(position).getStrTotalToken());
                        token_data.txt_balance.setText(tokenArrayList.get(position).getToken_balance());

                        if (tokenArrayList.get(position).isExpired.equalsIgnoreCase("Y")) {
                            token_data.txt_status.setText("Expired");
                            token_data.cardView.setBackgroundColor(ContextCompat.getColor(LPI_TokenActivity.this, R.color.lightred));
                            token_data.img_copyToken.setVisibility(View.GONE);
                        } else if (tokenArrayList.get(position).isActive.equalsIgnoreCase("Y")) {
                            token_data.txt_status.setText("Active");
                            token_data.cardView.setBackgroundColor(ContextCompat.getColor(LPI_TokenActivity.this, R.color.white));
                            token_data.img_copyToken.setVisibility(View.VISIBLE);
                        } else if (tokenArrayList.get(position).isActive.equalsIgnoreCase("N")) {
                            token_data.txt_status.setText("Inactive");
                            token_data.cardView.setBackgroundColor(ContextCompat.getColor(LPI_TokenActivity.this, R.color.lightred));
                            token_data.img_copyToken.setVisibility(View.GONE);
                        } else {
                            token_data.txt_status.setText("NA");
                            token_data.cardView.setBackgroundColor(ContextCompat.getColor(LPI_TokenActivity.this, R.color.lightred));
                            token_data.img_copyToken.setVisibility(View.GONE);
                        }

                        /*if ((tokenArrayList.get(position).isExpired.equalsIgnoreCase("Y")
                        || tokenArrayList.get(position).isActive.equalsIgnoreCase("N"))&&
                                Integer.parseInt(tokenArrayList.get(position).getToken_balance())>0){

                        }else {

                        }*/
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return tokenArrayList.size();
        }

        class VHItem extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView txt_token_type, txt_token_date, txt_token_To_date, txt_token,
                    txt_token_count, txt_balance, txt_status;
            ImageView img_copyToken;
            CardView cardView;

            //&& tokenArrayList.get(position).isActive.equalsIgnoreCase("Y")

            public VHItem(View itemView) {
                super(itemView);
                txt_token_type = (TextView) itemView.findViewById(R.id.txt_token_type);
                txt_token_date = (TextView) itemView.findViewById(R.id.txt_token_date);
                txt_token_To_date = (TextView) itemView.findViewById(R.id.txt_token_To_date);
                txt_token = (TextView) itemView.findViewById(R.id.txt_token);
                txt_token_count = (TextView) itemView.findViewById(R.id.txt_token_count);
                txt_balance = (TextView) itemView.findViewById(R.id.txt_balance);
                img_copyToken = (ImageView) itemView.findViewById(R.id.img_copyToken);
                cardView = (CardView) itemView.findViewById(R.id.token_cardView);
                txt_status = (TextView) itemView.findViewById(R.id.txt_status);
                img_copyToken.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {

                switch (v.getId()) {
                    case R.id.img_copyToken: {
//                        Toast.makeText(context, "Token copied successfully", Toast.LENGTH_SHORT).show();
                        try {
                            String srcText = ("" + tokenArrayList.get(getAdapterPosition()).getToken_no()).toString();
                            Object clipboardService = getSystemService(CLIPBOARD_SERVICE);
                            final ClipboardManager clipboardManager = (ClipboardManager) clipboardService;
                            ClipData clipData = ClipData.newPlainText("Token", srcText);
                            clipboardManager.setPrimaryClip(clipData);
                            Snackbar snackbar = Snackbar.make(v, "Token has been copied successfully.", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                }
            }
        }
    }

    public class TokenModel {
        String token_type;
        String token_date;
        String token_To_date;
        String token_no;
        String isActive;
        String isExpired;

        public String getIsActive() {
            return isActive;
        }

        public void setIsActive(String isActive) {
            this.isActive = isActive;
        }

        public String getIsExpired() {
            return isExpired;
        }

        public void setIsExpired(String isExpired) {
            this.isExpired = isExpired;
        }

        public String getToken_type() {
            return token_type;
        }

        public String getToken_date() {
            return token_date;
        }

        public String getToken_no() {
            return token_no;
        }

        public String getToken_count() {
            return token_count;
        }

        public String getToken_balance() {
            return token_balance;
        }

        public String getToken_To_date() {
            return token_To_date;
        }

        public TokenModel(String token_type, String token_date, String token_To_date, String token_no, String token_count,
                          String token_balance, String strTotalToken, String isActive, String isExpired) {

            this.token_type = token_type;
            this.token_date = token_date;
            this.token_To_date = token_To_date;
            this.token_no = token_no;
            this.token_count = token_count;
            this.token_balance = token_balance;
            this.strTotalToken = strTotalToken;
            this.isActive = isActive;
            this.isExpired = isExpired;
        }

        String token_count;
        String token_balance;
        String strTotalToken;

        public String getStrTotalToken() {
            return strTotalToken;
        }

        public void setStrTotalToken(String strTotalToken) {
            this.strTotalToken = strTotalToken;
        }

        public TokenModel() {
        }
    }

    public class GettingTokenDetails extends AsyncTask<String, String, JSONObject> {

        public Context context;
        ProgressDialog mProgressDialog;

        public GettingTokenDetails(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Getting Tokens Details...\nPlease wait..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            // TODO Auto-generated method stub
            getCandidatesFromServer();
            JSONObject objRet = new JSONObject();
            return objRet;
        }

        @Override
        protected void onCancelled() {
            mProgressDialog.dismiss();
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            try {
                boolean isSuccess = false;
                JSONObject objJsonObject = new JSONObject("");
                JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");
                for (int index = 0; index < objJsonArray.length(); index++) {
                    JSONObject jsonobject = objJsonArray.getJSONObject(index);
//                    String str_login = jsonobject.getString("Message");
                }
            } catch (Exception e) {
                // TODO: handle exception
                Log.e("", "error=" + e.toString());
            }
            mProgressDialog.dismiss();
        }

        private String getCandidatesFromServer() {

            String strResp = "";
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.GetCandidateSearch));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            String strReqXML = reqBuild();
            inputPI1.setName("objXMLDoc");
            try {
                inputPI1.setValue(strReqXML);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));

            try {
                LPIEEX509TrustManager.allowAllSSL();
//                FakeX509TrustManager.trustSSLCertificate(LPI_TokenActivity.this);
                androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                        context.getString(R.string.GetCandidateSearch), envelope);
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strResp = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return strResp;
        }

        private String reqBuild() {

            String str_XML = "";
            nu.xom.Element root = new nu.xom.Element("GetCandidateSearch");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(context));

            nu.xom.Element InscType = new nu.xom.Element("InscType");
            InscType.appendChild("" + StaticVariables.crnt_InscType);

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
            root.appendChild(InscType);

            nu.xom.Element Name = new nu.xom.Element("Name");
            Name.appendChild("");
            root.appendChild(Name);

            nu.xom.Element Mobile = new nu.xom.Element("Mobile");
            Mobile.appendChild("");
            root.appendChild(Mobile);

            nu.xom.Document doc = new nu.xom.Document(root);
            try {
                OutputStream out = new ByteArrayOutputStream();
                Serializer serializer = new Serializer(System.out, "UTF-8");
                serializer.setOutputStream(out);
                serializer.setIndent(4);
                serializer.setMaxLength(64);
                serializer.write(doc);
                str_XML = out.toString();
                str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
                str_XML = str_XML.replaceAll("\n", "");
                str_XML = str_XML.replaceAll("\r", "");
                str_XML = str_XML.replaceAll("     ", "");
                str_XML = str_XML.replaceAll("         ", "");
                str_XML = str_XML.trim();
                Log.e("", "XMLStr==" + str_XML);
            } catch (IOException e) {
                Log.e("", "GetCandidateSearch Error=" + e.toString());
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "AsyncSearchCandidates", "GetCandidateSearch", e.toString());
            }
            return str_XML;
        }
    }


}
