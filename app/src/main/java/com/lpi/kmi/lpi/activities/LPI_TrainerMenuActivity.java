package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Services.SyncActivityDetails;
import com.lpi.kmi.lpi.Services.SyncErrorLog;
import com.lpi.kmi.lpi.Services.SyncExamMaster;
import com.lpi.kmi.lpi.Services.SyncOfflineData;
import com.lpi.kmi.lpi.activities.AsyncTasks.AsyncGetFAQ;
import com.lpi.kmi.lpi.activities.AsyncTasks.GetAtemptedExams;
import com.lpi.kmi.lpi.activities.AsyncTasks.syncExams;
import com.lpi.kmi.lpi.adapter.Adapters.Custom_TrainerGridAdapter;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CircleImageView;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.stringcare.library.SC;

import net.sqlcipher.database.SQLiteDatabase;

import java.io.File;
import java.math.BigInteger;
import java.util.Calendar;

@SuppressLint("Range")
public class LPI_TrainerMenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static String[] osNameList = {"Psychometric Profiling", "View Personality Profile", "Registration", "Search", "Status",
            "LPI Lessons", "Emotional Excellence Program", "FAQs with your trainees"};
    public static int[] osImages = {R.mipmap.ic_profiling, R.mipmap.icons_bar_chart, R.mipmap.ic_register,
            R.mipmap.ic_search, R.mipmap.ic_chart_statistics, R.mipmap.ic_practice_lessons, R.mipmap.icons_mental_health, R.mipmap.ic_faq};
    boolean doubleBackToExitPressedOnce = false;
    GridView gridview;
    private static Toolbar toolbar;
    CoordinatorLayout coordinatorLayout = null;
    CircleImageView iv_profile;
    TextView tv_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_lpi_trainer_menu);
        try {
            if (!CommonClass.isMyServiceRunning(getApplicationContext(), "SyncActivityDetails")) {
                Intent trackIntent = new Intent(getApplicationContext(), SyncActivityDetails.class);
//                getApplicationContext().startService(trackIntent);
                CommonClass.StartService(getApplicationContext(), trackIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        initUI();
        initDrawer();

        try {
            AppUpdateManager appUpdateManager;
            appUpdateManager = AppUpdateManagerFactory.create(getApplicationContext());
            Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();
            appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                    Intent intent = new Intent(LPI_TrainerMenuActivity.this,Flexible.class);
                    startActivity(intent);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        iv_profile = (CircleImageView) header.findViewById(R.id.iv_profile);
        tv_name = (TextView) header.findViewById(R.id.tv_name);
        tv_name.setText(StaticVariables.UserName);
        iv_profile.setImageDrawable(getResources().getDrawable(R.drawable.profile_blank_m));
        setProfileData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (DatabaseHelper.db == null) {
                SQLiteDatabase.loadLibs(LPI_TrainerMenuActivity.this);
                DatabaseHelper.db = DatabaseHelper.getInstance(LPI_TrainerMenuActivity.this).
                        getWritableDatabase(SC.reveal(CommonClass.DBPassword));
            }
        } catch (Exception e) {
            Log.e("", "db error=" + e.toString());
        }
        if (StaticVariables.UserLoginId == null) {
            getUserDetails();
        }
    }

    private void initUI() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBarSetup();

        gridview = (GridView) findViewById(R.id.customgrid);
        gridview.setAdapter(new Custom_TrainerGridAdapter(this, osNameList, osImages));
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.cordinatorLayout);

        try {
            if (DatabaseHelper.db == null) {
                SQLiteDatabase.loadLibs(LPI_TrainerMenuActivity.this);
                DatabaseHelper.db = DatabaseHelper.getInstance(LPI_TrainerMenuActivity.this).
                        getWritableDatabase(SC.reveal(CommonClass.DBPassword));
            }
        } catch (Exception e) {
            Log.e("", "db error=" + e.toString());
        }
        try {
            if (StaticVariables.UserLoginId == null) {
                getUserDetails();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            DatabaseHelper.InsertActivityTracker(getApplication(), DatabaseHelper.db, StaticVariables.UserLoginId,
                    "A8001", "8011", "Trainer Menu Activity",
                    "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                    StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                    "", "", "Pending");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            int attemptId = DatabaseHelper.CheckAttempts(LPI_TrainerMenuActivity.this,
                    DatabaseHelper.db, "4001", StaticVariables.UserLoginId);
            int count = DatabaseHelper.PPExamQueCount(LPI_TrainerMenuActivity.this, DatabaseHelper.db, "4001");
            if (attemptId <= 0 && count >= 20) {
                alertCustomDialog("Complete Profiling", "Your Psychometric Profiling is pending, Do you want to complete Psychometric Profiling?");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getUserDetails() {

        try {
            Cursor mCursor = DatabaseHelper.db.query(getString(R.string.TBL_iCandidate), null,
                    null, null, null, null, null, "1");
            if (mCursor.getCount() > 0) {

                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    StaticVariables.crnt_InscType = "";
                    StaticVariables.isLogin = true;
                    StaticVariables.UserEmail = mCursor.getString(mCursor.getColumnIndex("CandidateEmailId"));
                    StaticVariables.UserName = mCursor.getString(mCursor.getColumnIndex("FirstName"));
                    StaticVariables.UserLoginId = mCursor.getString(mCursor.getColumnIndex("CandidateLoginId"));
                    StaticVariables.ParentId = mCursor.getString(mCursor.getColumnIndex("ParentId"));
                    StaticVariables.ParentName = mCursor.getString(mCursor.getColumnIndex("ParentName"));
                    StaticVariables.UserMobileNo = mCursor.getString(mCursor.getColumnIndex("CandidateMobileNo1"));
                    StaticVariables.offlineMode = "N";
                    StaticVariables.AppLink = mCursor.getString(mCursor.getColumnIndex("AppLink"));
                    StaticVariables.UserType = mCursor.getString(mCursor.getColumnIndex("UserType"));
                    StaticVariables.profileBitmap = mCursor.getBlob(mCursor.getColumnIndex("Image"));
                }
                mCursor.close();
                if (StaticVariables.UserType.trim().equalsIgnoreCase("C")) {
                    Intent intent = new Intent(LPI_TrainerMenuActivity.this, ParticipantHomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
                setProfileData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setProfileData() {
        try {
            if (StaticVariables.UserName != null) {
                getSupportActionBar().setSubtitle("Welcome " + StaticVariables.UserName);
                if (tv_name != null) {
                    tv_name.setText(StaticVariables.UserName);
                }
            }
            if (iv_profile != null) {
                if ((StaticVariables.profileBitmap != null) && !StaticVariables.profileBitmap.equals("null") && !StaticVariables.profileBitmap.equals("")) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(StaticVariables.profileBitmap, 0, StaticVariables.profileBitmap.length);
                    if (bitmap == null) {
                        iv_profile.setImageDrawable(LPI_TrainerMenuActivity.this.getResources().getDrawable(R.drawable.profile_blank_m));
                    } else {
                        iv_profile.setImageBitmap(bitmap);
                    }
                } else {
                    iv_profile.setImageDrawable(LPI_TrainerMenuActivity.this.getResources().getDrawable(R.drawable.profile_blank_m));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void alertCustomDialog(String title, String message) {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(LPI_TrainerMenuActivity.this).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_exit_dialog, null);
            TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            Button btn_ok = (Button) dialogView.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialogView.findViewById(R.id.btn_no);

            promt_title.setText(title);
            txt_message.setText(message);

            if (title.equalsIgnoreCase("Clear App Data!")) {
                btn_ok.setText("YES");
                btn_no.setText("No");
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        boolean tablesDroped = CommonClass.onDropCustomTables(DatabaseHelper.db, LPI_TrainerMenuActivity.this);
                        boolean tablesCreated = false;
                        if (tablesDroped) {
                            tablesCreated = CommonClass.onCreateCustomTables(DatabaseHelper.db, LPI_TrainerMenuActivity.this);
                        }
//                        CommonClass.DeleteFileFolders(new File(context.getExternalFilesDir(null) + "/Documents" + StaticVariables.LessonStoragePath));
                        CommonClass.DeleteFileFolders(new File(LPI_TrainerMenuActivity.this.getExternalFilesDir(null) + StaticVariables.LessonStoragePath));
                        try {
                            if (tablesCreated && tablesCreated) {
                                CommonClass.checkSettings(LPI_TrainerMenuActivity.this, "C");
                                if (StaticVariables.SyncMobile.equals("Yes")
                                        && CommonClass.networkType(LPI_TrainerMenuActivity.this).equals("MobileData")) {
                                    alertCustomDialog("Data Cleared!", "App's data is cleared, You are connected to mobile data, Do you want continue syncing?");
                                } else if (StaticVariables.SyncWIFI.equals("Yes")
                                        && CommonClass.networkType(LPI_TrainerMenuActivity.this).equals("WiFi")) {
                                    syncServices(LPI_TrainerMenuActivity.this);
                                } else if (CommonClass.isConnected(LPI_TrainerMenuActivity.this)) {
                                    syncServices(LPI_TrainerMenuActivity.this);
                                } else {
                                    alertCustomDialog("No Internet!", "You are not connected to internet");
                                }
                            } else {
                                Toast.makeText(LPI_TrainerMenuActivity.this, "Something went wrong!", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

            } else if (title.equalsIgnoreCase("Exit!")) {
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        LPI_TrainerMenuActivity.this.finish();
                        System.gc();
                        System.exit(0);
                    }
                });
            } else if (title.equalsIgnoreCase("Logout!")) {
                btn_ok.setText("YES");
                btn_no.setText("No");
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        boolean tablesDroped = CommonClass.onDropCustomTables(DatabaseHelper.db, LPI_TrainerMenuActivity.this);
                        boolean tablesCreated = false;
                        if (tablesDroped) {
                            tablesCreated = CommonClass.onCreateCustomTables(DatabaseHelper.db, LPI_TrainerMenuActivity.this);
                        }
                        if (tablesCreated && tablesCreated) {
                            DatabaseHelper.db.delete(getResources().getString(R.string.TBL_iCandidate), null, null);
                        }
//                        CommonClass.DeleteFileFolders(new File(context.getExternalFilesDir(null) + "/Documents" + StaticVariables.LessonStoragePath));
                        CommonClass.DeleteFileFolders(new File(LPI_TrainerMenuActivity.this.getExternalFilesDir(null) + StaticVariables.LessonStoragePath));
                        System.gc();
                        Intent i = new Intent(getApplicationContext(), Login.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        LPI_TrainerMenuActivity.this.finish();
                    }
                });
            } else if (title.equalsIgnoreCase("Complete Profiling")) {
                btn_ok.setText("YES");
                btn_no.setText("Not Now");
                btn_no.setPadding(5, 0, 5, 0);
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        StaticVariables.TestPage_flag = "PsychometricTest";
                        StaticVariables.crnt_QueId = 401;
                        StaticVariables.TblNameForCount = 20;
                        StaticVariables.crnt_ExmId = BigInteger.valueOf(4001);
                        StaticVariables.crnt_ExmTypeName = "Psychometric Profiling";
                        StaticVariables.crnt_ExmName = "Psychometric Profiling";
                        StaticVariables.crnt_ExamType = 4;
                        StaticVariables.crnt_ExamDuration = "30";
                        StaticVariables.IsPracticeExam = "N";
                        StaticVariables.IsAllQuesMendatory = "Y";
                        StaticVariables.allowedAttempts = "1";

                        Intent i = new Intent(LPI_TrainerMenuActivity.this, MCQ_QuestionActivity.class);
                        i.putExtra("initFrom", "TrainerGridAdapter");
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);

                    }
                });
            } else if (title.equalsIgnoreCase("Mobile Data!")) {
                btn_ok.setText("YES");
                btn_no.setText("Not Now");
                btn_no.setPadding(5, 0, 5, 0);
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        syncServices(LPI_TrainerMenuActivity.this);
                    }
                });
            } else if (title.equalsIgnoreCase("Data Cleared!")) {
                btn_ok.setText("YES");
                btn_no.setText("Not Now");
                btn_no.setPadding(5, 0, 5, 0);
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        syncServices(LPI_TrainerMenuActivity.this);
                    }
                });
            }
            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });

            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().setTitle("LPI Trainer");
            getSupportActionBar().setSubtitle("Welcome " + StaticVariables.UserName);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
//            Logout();
            alertCustomDialog("Exit!", "Are you sure, you want to exit?");
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_UpdateProfile) {
            // Handle the camera action
        } else if (id == R.id.nav_sync) {
            CommonClass.checkSettings(LPI_TrainerMenuActivity.this, "C");
            if (StaticVariables.SyncMobile.equals("Yes")
                    && CommonClass.networkType(LPI_TrainerMenuActivity.this).equals("MobileData")) {
                alertCustomDialog("Mobile Data!", "You are connected to mobile data, Do you want continue syncing?");
            } else if (StaticVariables.SyncWIFI.equals("Yes")
                    && CommonClass.networkType(LPI_TrainerMenuActivity.this).equals("WiFi")) {
                syncServices(LPI_TrainerMenuActivity.this);
            } else if (CommonClass.isConnected(LPI_TrainerMenuActivity.this)) {
                syncServices(LPI_TrainerMenuActivity.this);
            } else {
                alertCustomDialog("No Internet!", "You are not connected to internet");
            }
        } else if (id == R.id.nav_share) {
            Intent intent = new Intent(LPI_TrainerMenuActivity.this, LPI_ShareAppActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("initFrom", "LPI_TrainerMenuActivity");
            intent.putExtra("candidate_id", "");
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            LPI_TrainerMenuActivity.this.finish();
        } else if (id == R.id.nav_clearData) {
            alertCustomDialog("Clear App Data!", "Are you sure, you want to clear apps data?");
        } else if (id == R.id.nav_logout) {
//            Logout();
            alertCustomDialog("Logout!", "Are you sure, you want to logout?");
        } else if (id == R.id.nav_Exit) {
//            Logout();
            alertCustomDialog("Exit!", "Are you sure, you want to exit?");
        } else if (id == R.id.nav_setting) {
            Intent intent = new Intent(LPI_TrainerMenuActivity.this, SettingActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            LPI_TrainerMenuActivity.this.finish();
        } else if (id == R.id.nav_genToken) {
            Intent intent = new Intent(LPI_TrainerMenuActivity.this, LPI_TokenActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            LPI_TrainerMenuActivity.this.finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void syncServices(Context context) {
        try {
            if (CommonClass.isConnected(context)) {
                new syncExams(new Callback<String>() {
                    @Override
                    public void execute(String result, String status) {
                        new GetAtemptedExams(LPI_TrainerMenuActivity.this, StaticVariables.UserLoginId, new Callback() {
                            @Override
                            public void execute(Object result, String status) {
                                new AsyncGetFAQ(new Callback<String>() {
                                    @Override
                                    public void execute(String result, String status) {
                                        /*new AsyncGetFAQ(new Callback<String>() {
                                            @Override
                                            public void execute(String result, String status) {

                                            }
                                        }, LPI_TrainerMenuActivity.this, "", StaticVariables.UserLoginId, StaticVariables.ParentId, StaticVariables.UserType).execute();*/
                                    }
                                }, LPI_TrainerMenuActivity.this, "G", "", StaticVariables.UserLoginId, StaticVariables.UserType).execute();
                            }
                        }).execute();
                    }
                }, LPI_TrainerMenuActivity.this, "LPI_TrainerMenuActivity", StaticVariables.UserLoginId, "").execute();

                if (CommonClass.networkType(context).equals("WiFi")) {
                    if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncExamMaster")) {
                        Intent newIntent = new Intent(context.getApplicationContext(), SyncExamMaster.class);
//                        context.getApplicationContext().startService(newIntent);
                        CommonClass.StartService(getApplicationContext(), newIntent);
                    }

//                if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncFAQModuleData")) {
//                    Intent newIntent = new Intent(context.getApplicationContext(), SyncFAQModuleData.class);
//                    context.getApplicationContext().startService(newIntent);
//                }

                    if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncActivityDetails")) {
                        Intent trackIntent = new Intent(context.getApplicationContext(), SyncActivityDetails.class);
//                        context.getApplicationContext().startService(trackIntent);
                        CommonClass.StartService(getApplicationContext(), trackIntent);
                    }

                    if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncErrorLog")) {
                        Intent newIntent = new Intent(context.getApplicationContext(), SyncErrorLog.class);
//                        context.getApplicationContext().startService(newIntent);
                        CommonClass.StartService(getApplicationContext(), newIntent);
                    }

                    if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncOfflineData")) {
                        Intent newIntent = new Intent(context.getApplicationContext(), SyncOfflineData.class);
//                        context.getApplicationContext().startService(newIntent);
                        CommonClass.StartService(getApplicationContext(), newIntent);
                    }
                } else {
                }
            }

        } catch (Exception e) {
            Log.e("", "wewewe" + e.toString());
        }
    }

    public void Logout() {
        AlertDialog.Builder builder = CommonClass.DialogOpen(LPI_TrainerMenuActivity.this, "Logout!", "Are you sure, you want to logout?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                boolean isAvailableData = false;
                android.database.Cursor mCursor = DatabaseHelper.Get_mPINCursor(LPI_TrainerMenuActivity.this,
                        DatabaseHelper.db, StaticVariables.UserLoginId);
                if (mCursor.getCount() > 0) {
                    for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                        String isPIN = mCursor.getString(mCursor.getColumnIndex("is_mPIN_Data"));
                        if (isPIN != null && isPIN.trim().equalsIgnoreCase("Y")) {
                            isAvailableData = true;
                        } else {
                            isAvailableData = false;
                        }
                    }
                }
                if (isAvailableData) {
                    Intent i = new Intent(getApplicationContext(), PIN_AuthActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra("user_id", StaticVariables.UserLoginId);
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    LPI_TrainerMenuActivity.this.finish();

                } else {
                    Intent i = new Intent(getApplicationContext(), Login.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    LPI_TrainerMenuActivity.this.finish();
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
