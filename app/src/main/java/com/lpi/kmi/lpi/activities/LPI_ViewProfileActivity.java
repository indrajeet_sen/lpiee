package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.CircleImageView;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.stringcare.library.SC;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

import nu.xom.Serializer;

@SuppressLint("Range")
public class LPI_ViewProfileActivity extends AppCompatActivity {

    CircleImageView circleImageView;
    ImageView iv_status;
    TextView input_FName, input_LName, input_UserID, input_email, input_mobile, input_currentStatus;
    String candidate_id = "";
    Button btn_ShareLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_lpi_view_profile);

        initUI();
        UIListener();
    }

    private void UIListener() {
        iv_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    DatabaseHelper.InsertActivityTracker(LPI_ViewProfileActivity.this, DatabaseHelper.db,
                            StaticVariables.UserLoginId, "A80012", "A8006",
                            "View candidate Psychometric test status button clicked",
                            "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                            StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                            "", "", "Pending");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent i = new Intent(LPI_ViewProfileActivity.this, ViewResultActivity.class);
                i.putExtra("ExamId", "" + 4001);
                i.putExtra("AttemptedId", ""+1);
                i.putExtra("CandidateId", candidate_id);
                i.putExtra("initFrom", "LPI_ViewProfileActivity");
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                LPI_ViewProfileActivity.this.finish();
            }
        });

        btn_ShareLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LPI_ViewProfileActivity.this, LPI_ShareAppActivity.class);
                intent.putExtra("initFrom", "LPI_ViewProfileActivity");
                intent.putExtra("candidate_id", candidate_id);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                LPI_ViewProfileActivity.this.finish();
            }
        });
    }

    private void initUI() {
        try {
            candidate_id = getIntent().getExtras().getString("candidate_id");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_menu);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(5);
        }
        setSupportActionBar(toolbar);
        actionBarSetup();

        circleImageView = (CircleImageView) findViewById(R.id.circleImageView);
        iv_status = (ImageView) findViewById(R.id.iv_status);
        input_FName = (EditText) findViewById(R.id.input_FName);
        input_LName = (EditText) findViewById(R.id.input_LName);
        input_UserID = (EditText) findViewById(R.id.input_UserID);
        input_email = (EditText) findViewById(R.id.input_email);
        input_mobile = (EditText) findViewById(R.id.input_mobile);
        input_currentStatus = (EditText) findViewById(R.id.input_currentStatus);
        btn_ShareLink = (Button) findViewById(R.id.btn_ShareLink);
        if (candidate_id != null && candidate_id.trim().length() > 0) {
//            setDataToUI();
            if (CommonClass.isConnected(LPI_ViewProfileActivity.this)) {
                new AsyncGetCandidateDetails(LPI_ViewProfileActivity.this).execute();
            } else {
                showError();
            }
        }
        try {
            DatabaseHelper.InsertActivityTracker(LPI_ViewProfileActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                    "A80011", "A8006", "View candidate profile page called",
                    "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                    StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                    "", "", "Pending");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showError() {

        AlertDialog.Builder builder = CommonClass.DialogOpen(LPI_ViewProfileActivity.this,
                "Alert!", "No internet connection found, please connect internet and try again.");
        builder.setCancelable(true);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onBackPressed();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void setDataToUI() {
        Cursor mCursor = null;
        try {
            if (DatabaseHelper.db == null) {
                SQLiteDatabase.loadLibs(getApplicationContext());
                DatabaseHelper.db = DatabaseHelper.getInstance(getApplicationContext()).
                        getWritableDatabase(SC.reveal(CommonClass.DBPassword));
            }
            mCursor = DatabaseHelper.getCandidateInfo(LPI_ViewProfileActivity.this, DatabaseHelper.db, candidate_id);
            if (mCursor.getCount() > 0) {
                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    input_FName.setText(mCursor.getString(mCursor.getColumnIndex("FirstName")));
                    input_LName.setText(mCursor.getString(mCursor.getColumnIndex("LastName")));
                    input_UserID.setText("XYZ1703");
                    input_email.setText(mCursor.getString(mCursor.getColumnIndex("EmailID")));
                    input_mobile.setText(mCursor.getString(mCursor.getColumnIndex("MobileNo")));
                    byte[] img_byteArray = mCursor.getBlob(mCursor.getColumnIndex("ImageByte"));
                    Bitmap bitmap = BitmapFactory.decodeByteArray(img_byteArray, 0, img_byteArray.length);
                    circleImageView.setImageBitmap(bitmap);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().setTitle("Profile");
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LPI_ViewProfileActivity.this,LPI_SearchActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        LPI_ViewProfileActivity.this.finish();
    }

    class AsyncGetCandidateDetails extends AsyncTask<String, Void, JSONObject> {
        Context context;
        ProgressDialog mProgressDialog;

        public AsyncGetCandidateDetails(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading..\nPlease wait..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            // TODO Auto-generated method stub
            String strResponse = "";
            try {
                strResponse = getCandidateDetails();
                if (strResponse != null && !strResponse.equalsIgnoreCase("{}")
                        && !strResponse.equalsIgnoreCase("null")) {
                    return new JSONObject(strResponse);
                } else {
                    return null;
                }
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            // TODO Auto-generated method stub
            try {
                mProgressDialog.dismiss();
                if (result != null && !result.toString().equalsIgnoreCase("{}")
                        && !result.toString().equalsIgnoreCase("null")) {
                    try {
                        String strJsonArray = result.getJSONArray("Table").toString();
                        JSONArray jsonArray = new JSONArray(strJsonArray);
                        JSONObject jsonObjectError = new JSONObject();
                        byte[] imageBytes = null;
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                            jsonObjectError = jsonObject;
                            imageBytes = Base64.decode(jsonObject.getString("Images"), Base64.DEFAULT);
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            String curDate = sdf.format(Calendar.getInstance().getTime());
                            int count = DatabaseHelper.RegisterCandidate(LPI_ViewProfileActivity.this, DatabaseHelper.db,
                                    jsonObject.getString("CandidateId"), jsonObject.getString("UserName"),
                                    jsonObject.getString("UserName2"), jsonObject.getString("Gender"),
                                    jsonObject.getString("DOB"), jsonObject.getString("UserEmailId"),
                                    jsonObject.getString("UserMobileNo1"), jsonObject.getString("PassWd"),
                                    jsonObject.getString("Profession"), jsonObject.getString("Nationality"),
                                    imageBytes, jsonObject.getString("ParentId"), curDate, curDate, UUID.randomUUID().toString(),
                                    DatabaseHelper.GetCountryCode(LPI_ViewProfileActivity.this, DatabaseHelper.db,
                                            jsonObject.getString("Nationality")), jsonObject.getString("AppLink"),
                                    false,"Success","");

                            input_FName.setText(jsonObject.getString("UserName"));
                            input_LName.setText(jsonObject.getString("UserName2"));
                            input_UserID.setText(jsonObject.getString("CandidateId"));
                            input_email.setText(jsonObject.getString("UserEmailId"));
                            input_mobile.setText(jsonObject.getString("UserMobileNo1"));
                            if (!jsonObject.isNull("Images") && !jsonObject.get("Images").equals(null)
                                    && !jsonObject.get("Images").equals("null") && !jsonObject.get("Images").equals("")) {
                                imageBytes = Base64.decode(jsonObject.getString("Images"), Base64.DEFAULT);
                                if (imageBytes != null && !(jsonObject.get("Images").toString().trim().
                                        equalsIgnoreCase("null"))) {
                                    Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                                    circleImageView.setImageBitmap(bitmap);
                                }
                            }
//                            if (jsonObject.getBoolean("isTestTaken") == false) {
                            if ((jsonObject.getString("PPAttempted")).equalsIgnoreCase("Y")) {
                                iv_status.setVisibility(View.VISIBLE);
                                input_currentStatus.setText("Psychometric test completed");
                            } else if ((jsonObject.getString("PPAttempted")).equalsIgnoreCase("N")) {
                                iv_status.setVisibility(View.INVISIBLE);
                                input_currentStatus.setText("Psychometric test not completed");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        showError("Something went wrong, please try again.");
                    }
                }else {
                    showError("Server not responding");
                }
            } catch (Exception e) {
                e.printStackTrace();
                showError("Something went wrong, please try again.");
            }
        }

        private void showError(String msg) {

            /*AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(LPI_ViewProfileActivity.this, R.style.myCoolDialog);
            } else {
                builder = new AlertDialog.Builder(LPI_ViewProfileActivity.this);
            }
            builder.setTitle("Alert!");
            builder.setMessage("Something went wrong please try again.");*/
            AlertDialog.Builder builder = CommonClass.DialogOpen(LPI_ViewProfileActivity.this,
                    "Alert!", msg);
            builder.setCancelable(true);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    onBackPressed();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

        private String getCandidateDetails() {

            String strResp = "";
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.GetCandidateDetails));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            String strReqXML = reqBuild();
            inputPI1.setName("objXMLDoc");
            try {
                inputPI1.setValue(strReqXML);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL));

            try {
                LPIEEX509TrustManager.allowAllSSL();
//                FakeX509TrustManager.trustSSLCertificate(LPI_ViewProfileActivity.this);
                androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                        context.getString(R.string.GetCandidateDetails), envelope);
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strResp = response.toString();

                Log.e("Request: ", request.toString());
                Log.e("REsponse: ", response.toString());
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("REsponse: ", e.getMessage().toString());
            }
            return strResp;
        }

        private String reqBuild() {

            String str_XML = "";
            nu.xom.Element root = new nu.xom.Element("GetCandidateDetails");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(context));

            nu.xom.Element InscType = new nu.xom.Element("InscType");
            InscType.appendChild("" + StaticVariables.crnt_InscType);

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
            root.appendChild(InscType);

            nu.xom.Element CandidateId = new nu.xom.Element("CandidateId");
            CandidateId.appendChild(candidate_id.trim());
            root.appendChild(CandidateId);

            nu.xom.Document doc = new nu.xom.Document(root);
            try {
                OutputStream out = new ByteArrayOutputStream();
                Serializer serializer = new Serializer(System.out, "UTF-8");
                serializer.setOutputStream(out);
                serializer.setIndent(4);
                serializer.setMaxLength(64);
                serializer.write(doc);
                str_XML = out.toString();
                str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
                str_XML = str_XML.replaceAll("\n", "");
                str_XML = str_XML.replaceAll("\r", "");
                str_XML = str_XML.replaceAll("     ", "");
                str_XML = str_XML.replaceAll("         ", "");
                str_XML = str_XML.trim();
                Log.e("", "XMLStr==" + str_XML);
            } catch (IOException e) {
                Log.e("", "getCandidateDetails Error=" + e.toString());
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "AsyncGetCandidateDetails", "getCandidateDetails", e.toString());
            }
            return str_XML;
        }

    }
}
