package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Services.AlarmReceiver;
import com.lpi.kmi.lpi.Services.SyncPostLicModuleData;
import com.lpi.kmi.lpi.activities.AsyncTasks.AsyncCandidateRegistration;
import com.lpi.kmi.lpi.activities.AsyncTasks.AsyncGenerateOTP;
import com.lpi.kmi.lpi.activities.AsyncTasks.GetAtemptedExams;
import com.lpi.kmi.lpi.activities.AsyncTasks.SubmitResetPassword;
import com.lpi.kmi.lpi.activities.AsyncTasks.syncExams;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.stringcare.library.SC;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

@SuppressLint("Range")
public class Login extends AppCompatActivity implements View.OnClickListener {
    static final int CUSTOM_DIALOG_ID = 0;
//    public static String seedValue, seedValue_XML, strLoginId = "";
    boolean isNotVisible = false;
    boolean isNotVisibleForgot = false;
    ProgressDialog mProgressBar = null;
    private boolean isConnected = false;
    public static final String MY_PREFS_NAME = "LPIPrefsFile";
    public static final String GOOGLE_API_VERIFY_URL = "https://www.googleapis.com/androidcheck/v1/attestations/";
    List<String> Country_list = new ArrayList<>();
    List<String> Country_code_list = new ArrayList<>();
    Dialog dialog = null;
    ListView dialog_ListView;
    String strMobileCountryCode = "";

    //@BindView(R.id.input_userID)
    EditText edtTxt_UserID;
    //@BindView(R.id.input_password)
    EditText edtTxt_Pwd;

    //@BindView(R.id.input_userename)
    EditText edtTxt_userename;
    //@BindView(R.id.input_useremail)
    EditText edtTxt_useremail;
    // //@BindView(R.id.input_createpasswd)
//    EditText edtTxt_createpasswd;
    //@BindView(R.id.input_treiner)
    EditText edtTxt_treiner;
    // //@BindView(R.id.txt_newUser)
//    TextView txt_newUser;
    //@BindView(R.id.btn_login)
    Button btn_Login;
    //@BindView(R.id.btn_signin)
    Button btn_signin;
    //@BindView(R.id.btn_signup)
    Button btn_signup;
    //@BindView(R.id.btn_createUser)
    Button btn_createUser;
    //@BindView(R.id.btn_Cancel)
    Button btn_Cancel;
    //@BindView(R.id.btn_CancleUser)
    Button btn_CancleUser;
    //@BindView(R.id.lin_userType)
    LinearLayout lin_userType;
    //@BindView(R.id.lin_userlogin)
    LinearLayout lin_userlogin;
    //@BindView(R.id.lin_createUser)
    LinearLayout lin_createUser;
    //@BindView(R.id.text_forgot)
    TextView text_forgot;
    //@BindView(R.id.lin_foruserlogin)
    LinearLayout lin_foruserlogin;
    //@BindView(R.id.viewPass)
    ImageView viewPass;


    //@BindView(R.id.input_foruserID)
    EditText input_foruserID;
    //@BindView(R.id.viewPass_forgot)
    ImageView viewPass_forgot;
    //@BindView(R.id.input_forotp)
    EditText input_forotp;
    //@BindView(R.id.input_cnfpassword)
    EditText input_cnfpassword;
    //@BindView(R.id.btn_forlogin)
    Button btn_forlogin;
    //@BindView(R.id.btn_forCancel)
    Button btn_forCancel;
    //@BindView(R.id.txt_app_version)
    TextView txt_app_version;

    //@BindView(R.id.input_nationality)
    TextView input_nationality;

    boolean doubleBackToExitPressedOnce = false;
    String MobRefId = "";
    String strLoginResponse = "";
    String MasterChangeVer = "";
    int ResponseCode = 1;
    InputStream MasterInputStream, LookupInputStream, GetModuleData;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_login);
//        ButterKnife.bind(this);
        initUI();
        lin_userType.setVisibility(View.VISIBLE);
        lin_userlogin.setVisibility(View.GONE);
        lin_createUser.setVisibility(View.GONE);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            if (!hasPermissions(Login.this, PERMISSIONS)) {
//                ActivityCompat.requestPermissions(Login.this, PERMISSIONS, PERMISSION_ALL);
//            }
//        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressDialog = new ProgressDialog(Login.this, R.style.AppTheme_Dark_Dialog);
        } else {
            progressDialog = new ProgressDialog(Login.this);
        }

        try {
            if (DatabaseHelper.db == null) {
                SQLiteDatabase.loadLibs(Login.this);
                DatabaseHelper.db = DatabaseHelper.getInstance(Login.this).
                        getWritableDatabase(SC.reveal(CommonClass.DBPassword));
            }
            PackageInfo pInfo = Login.this.getPackageManager().getPackageInfo(Login.this.getPackageName(), 0);
            String AppVersion = "" + pInfo.versionName;
            txt_app_version.setText("LPIEE Ver " + AppVersion);
        } catch (Exception e) {
            Log.e("", "db error=" + e.toString());
        }

        edtTxt_UserID.setText("");
        edtTxt_Pwd.setText("");
        edtTxt_userename.setText("");
        edtTxt_useremail.setText("");
//        edtTxt_createpasswd.setText("");
        edtTxt_treiner.setText("");

        EditText_NullErr(edtTxt_UserID);
        EditText_NullErr(edtTxt_Pwd);

//        try {
//            btn_signin.setText(Translator.execute(btn_signin.getText().toString(),Language.ENGLISH,Language.HINDI));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        btn_Login.setOnClickListener(this);
        btn_signin.setOnClickListener(this);
        btn_signup.setOnClickListener(this);
        btn_createUser.setOnClickListener(this);
        btn_Cancel.setOnClickListener(this);
        btn_CancleUser.setOnClickListener(this);
        text_forgot.setOnClickListener(this);
        viewPass.setOnClickListener(this);
        viewPass_forgot.setOnClickListener(this);
        lin_foruserlogin.setOnClickListener(this);
        btn_forlogin.setOnClickListener(this);
        btn_forCancel.setOnClickListener(this);


//        txt_newUser.setOnClickListener(this);
//        edtTxt_UserID.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

//        seedValue = "" + (Login.this.getResources().getString(R.string.cipher));
//        seedValue_XML = "" + (Login.this.getResources().getString(R.string.seed));

        input_nationality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Country_list.size() > 0)
                    showDialog(CUSTOM_DIALOG_ID);
            }
        });


//        try {
////            Cursor exam_cquery = DatabaseHelper.db.query(getResources().getString(R.string.TBL_LPIExamQuestionMST),
////                    new String[]{"ExamId,QuestionId,QuestionCode,Question"},
////                    null,null, null, null, "QuestionId ASC");
////            if (exam_cquery.getCount()>0){
////                for(exam_cquery.moveToFirst();!exam_cquery.isAfterLast();exam_cquery.moveToNext()) {
////                    int i = 0;
////                    while (i <= exam_cquery.getColumnCount()) {
////                        Log.e("","Column="+exam_cquery.getColumnName(i)+" - value ="+exam_cquery.getString(i));
////                        i+=1;
////                    }
////                }
////            }
//
//            Cursor cursor = DatabaseHelper.db.query(getResources().getString(R.string.TBL_LPIExamQuestionMST),
//                    new String[]{"ExamId,QuestionId,QuestionCode,Question"},
//                    "ExamId=?",new String[]{""+2001}, null, null, "QuestionId ASC");
//            if (cursor.moveToFirst()) {
//                do {
//                    HashMap<String, String> map = new HashMap<String, String>();
//                    for(int i=0; i<cursor.getColumnCount();i++)
//                    {
//                       Log.e("","Column = "+ cursor.getColumnName(i) + "Value = " +cursor.getString(i));
//
//                    }
//                } while (cursor.moveToNext());
//            }
//            cursor.close();
//        } catch (Exception e) {
//            Log.e("","err="+e.toString());
//        }
//        edtTxt_UserID.setText("70011354");
//        edtTxt_Pwd.setText("pass@123");

        /*try {
            SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            String isRootVerify = prefs.getString("isRootVerify", null);
            int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
            // Showing status
            mProgressBar = new ProgressDialog(Login.this);
            if (status == ConnectionResult.SUCCESS && (isRootVerify == null || isRootVerify.equalsIgnoreCase("N"))) {
                initClient();
            } else if (status != ConnectionResult.SUCCESS && (isRootVerify == null)) {
                showDialog("Security Alert", "Unsecured device has been detected, Google Play Services are not available in this device. \nCan\'t login into the app through this Device!", "");
            }
        } catch (Exception e) {
            e.printStackTrace();
            //
            if (mProgressBar != null)
                mProgressBar.dismiss();
        }*/

    }

    private void initUI() {

        edtTxt_UserID = (EditText) findViewById(R.id.input_userID);
        edtTxt_Pwd = (EditText) findViewById(R.id.input_password);
        edtTxt_userename = (EditText) findViewById(R.id.input_userename);
        edtTxt_useremail = (EditText) findViewById(R.id.input_useremail);
        edtTxt_treiner = (EditText) findViewById(R.id.input_treiner);
        btn_Login = (Button) findViewById(R.id.btn_login);
        btn_signin = (Button) findViewById(R.id.btn_signin);
        btn_signup = (Button) findViewById(R.id.btn_signup);
        btn_createUser = (Button) findViewById(R.id.btn_createUser);
        btn_Cancel = (Button) findViewById(R.id.btn_Cancel);
        btn_CancleUser = (Button) findViewById(R.id.btn_CancleUser);
        lin_userType = (LinearLayout) findViewById(R.id.lin_userType);
        lin_userlogin = (LinearLayout) findViewById(R.id.lin_userlogin);
        lin_createUser = (LinearLayout) findViewById(R.id.lin_createUser);
        lin_foruserlogin = (LinearLayout) findViewById(R.id.lin_foruserlogin);
        text_forgot = (TextView) findViewById(R.id.text_forgot);
        viewPass = (ImageView) findViewById(R.id.viewPass);
        viewPass_forgot = (ImageView) findViewById(R.id.viewPass_forgot);
        input_foruserID = (EditText) findViewById(R.id.input_foruserID);
        input_forotp = (EditText) findViewById(R.id.input_forotp);
        input_cnfpassword = (EditText) findViewById(R.id.input_cnfpassword);
        btn_forlogin = (Button) findViewById(R.id.btn_forlogin);
        btn_forCancel = (Button) findViewById(R.id.btn_forCancel);
        txt_app_version = (TextView) findViewById(R.id.txt_app_version);
        input_nationality = (EditText) findViewById(R.id.input_nationality);

        android.database.Cursor cursor = DatabaseHelper.db.query(Login.this.getResources().getString(R.string.TBL_Country),
                null, null, null, null, null, null);
        if (cursor.getCount() < 1) {
            CommonClass.initCDataArray(Login.this);
        }
        cursor.close();

        Cursor country = DatabaseHelper.db.query(getString(R.string.TBL_Country), null, null,
                null, null, null, "CountryName asc");

        if (country.getCount() > 0) {
            Country_list.clear();
            Country_code_list.clear();
            for (country.moveToFirst(); !country.isAfterLast(); country.moveToNext()) {
                Country_list.add(country.getString(country.getColumnIndex("CountryName")));
                Country_code_list.add(country.getString(country.getColumnIndex("CountryCode")));
            }
        }
        country.close();
    }

    void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog = new Dialog(this, R.style.AppTheme_Dark_Dialog);
        } else {
            dialog = new Dialog(this);
        }
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_list_item);
        dialog.setCanceledOnTouchOutside(true);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.90);
        int screenHeight = (int) (metrics.widthPixels * 1.3);

        Window window = dialog.getWindow();
        window.setLayout(screenWidth, screenHeight);

        //Prepare ListView in dialog
        dialog_ListView = (ListView) dialog.findViewById(R.id.dialoglist);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.iv_close);
        iv_close.setVisibility(View.GONE);

        switch (id) {
            case CUSTOM_DIALOG_ID:

                TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                tv_title.setText("Select Country");

                if (Country_list.size() > 0) {
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (Login.this, R.layout.country_list_item, Country_list);
                    dialog_ListView.setAdapter(adapter);

                    dialog_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
                            input_nationality.setText(Country_list.get(position));
                            input_nationality.setError(null);
                            strMobileCountryCode = Country_code_list.get(position);
                            input_nationality.setHintTextColor(getResources().getColor(R.color.colorPrimary));
                            dismissDialog(CUSTOM_DIALOG_ID);
                        }
                    });
                }
                break;

        }
        return dialog;
    }

    @Override
    protected void onPrepareDialog(int id, Dialog dialog, Bundle bundle) {
        super.onPrepareDialog(id, dialog, bundle);

        switch (id) {
            case CUSTOM_DIALOG_ID:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void EditText_NullErr(final EditText edt) {

        edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                edt.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:

                // using method from above
                String deviceName = android.os.Build.MODEL;
                String deviceMan = android.os.Build.MANUFACTURER;
                Log.e("deviceName", "deviceName=" + deviceName);
                Log.e("deviceMan", "deviceMan=" + deviceMan);

                if (validateEmail()) {
                    hideKeyboard();
                    if (CommonClass.isConnected(Login.this)) {
                        new CandidateLogin(0, Login.this, "").execute();
                    } else {
//                        offlineLogin();
                        alertPIN("No Internet", "No internet connectivity, please try again in connected mode");
//                        showAlert(Login.this, , );
                    }
                }
                break;
            case R.id.btn_signin:
                lin_userType.setVisibility(View.GONE);
                lin_createUser.setVisibility(View.GONE);
                lin_userlogin.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_signup:
                MobRefId = UUID.randomUUID().toString();
                lin_userType.setVisibility(View.GONE);
                lin_userlogin.setVisibility(View.GONE);
                lin_createUser.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_createUser:
                if (validateCreateAcc()) {
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                        String curDate = sdf.format(Calendar.getInstance().getTime());
                        int i = DatabaseHelper.RegisterCandidate(Login.this, DatabaseHelper.db, "",
                                edtTxt_userename.getText().toString().trim(), "", "",
                                "", edtTxt_useremail.getText().toString().trim(),
                                "", "pass@123", "", input_nationality.getText().toString().trim(),
                                new byte[]{}, edtTxt_treiner.getText().toString(), curDate, curDate, MobRefId, strMobileCountryCode, "", false,
                                "pending", edtTxt_treiner.getText().toString());

                        if (CommonClass.isConnected(Login.this)) {
                            new AsyncCandidateRegistration(new Callback<String>() {
                                @Override
                                public void execute(String response, String status) {
                                    if (status.equalsIgnoreCase("success")) {
                                        try {
                                            JSONObject result = new JSONObject(response);
                                            String strJsonArray = result.getJSONArray("Table1").toString();
                                            JSONArray jsonArray = new JSONArray(strJsonArray);
                                            JSONObject jsonObjectError = new JSONObject();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                                                jsonObjectError = jsonObject;
                                                if (jsonObject != null && jsonObject.has("ResponseCode") && jsonObject.getString("ResponseCode").equalsIgnoreCase("0")) {

                                                    ContentValues cv = new ContentValues();
                                                    cv.clear();
                                                    cv.put("SyncStatus", "Success");
                                                    DatabaseHelper.db.update(getResources().getString(R.string.TBL_LPICandidateRegistration), cv,
                                                            "MobRefId=?", new String[]{MobRefId});
//                                                    alertPIN("User Registration", "User Registration has been successfully completed.\nPlease use following credential for login.\n\n" +
//                                                            "UserId: " + jsonObject.getString("UserId") + "\nPassword: pass@123 \n");
                                                    edtTxt_UserID.setText(edtTxt_useremail.getText().toString().trim());
                                                    edtTxt_Pwd.setText("pass@123");
                                                    if (CommonClass.isConnected(Login.this)) {
                                                        new CandidateLogin(0, Login.this, "").execute();
                                                    }
                                                } else if (jsonObject != null && jsonObject.has("RespCode")) {
                                                    if (jsonObject.get("RespCode").equals("2")) {
                                                        alertPIN("Registration Alert", "" + jsonObject.get("message"));
                                                    } else {
                                                        alertPIN("Registration Failed", "Server not responding please try again.");
                                                    }
                                                } else if (jsonObject != null && jsonObject.getString("RespCode").equalsIgnoreCase("1")) {
                                                    alertPIN("User Registration Failed", jsonObject.getString("message"));
                                                } else {
                                                    alertPIN("Registration Failed", "Server not responding please try again.");
                                                }
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
//                                            alertPIN("User Registration Failed", "Server not responding please try again.");
                                            try {
                                                JSONObject resJsonObject = new JSONObject(response);
                                                String strJsonArray = resJsonObject.getJSONArray("Table").toString();
                                                JSONArray jsonArray = new JSONArray(strJsonArray);
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                                                    if (jsonObject != null && jsonObject.has("RespCode")) {
                                                        if (jsonObject.has("RespCode") && jsonObject.get("RespCode").equals("1") && jsonObject.has("message")) {
                                                            alertPIN("Registration Alert", "" + jsonObject.get("message") + "\n" +
                                                                    "CandidateId: " + jsonObject.get("CandidateId"));
                                                        } else if (jsonObject.has("RespCode") && jsonObject.get("RespCode").equals("2") && jsonObject.has("message")) {
                                                            alertPIN("Registration Alert", "" + jsonObject.get("message"));
                                                        } else {
                                                            alertPIN("Registration Failed", "Server not responding please try again.");
                                                        }
                                                    }
                                                }
                                            } catch (Exception ee) {
                                                ee.printStackTrace();
                                                alertPIN("User Registration Failed", "Server not responding please try again.");
                                            }
                                        }
                                    }
                                }
                            }, Login.this,
                                    Login.this, MobRefId, edtTxt_useremail.getText().toString().trim(),
                                    getResources().getString(R.string.RegisterCandidate), "Login").execute();
                        } else {
                            alertPIN("No Internet", "No internet connectivity, please try again in connected mode");
                        }
                    } catch (Exception e) {
                        Log.e("ucreate", "error " + e.toString());
                    }
                } /*else {
                    try {
                        edtTxt_userename.clearFocus();
                        edtTxt_useremail.clearFocus();
                        edtTxt_treiner.clearFocus();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }*/
                break;
            case R.id.btn_Cancel:
                edtTxt_UserID.setText("");
                edtTxt_Pwd.setText("");
                lin_userType.setVisibility(View.VISIBLE);
                lin_userlogin.setVisibility(View.GONE);
                lin_createUser.setVisibility(View.GONE);
                break;
            case R.id.btn_CancleUser:
                edtTxt_userename.setText("");
                edtTxt_useremail.setText("");
                edtTxt_treiner.setText("");
                lin_userType.setVisibility(View.VISIBLE);
                lin_userlogin.setVisibility(View.GONE);
                lin_createUser.setVisibility(View.GONE);
                break;
            case R.id.text_forgot:
                if (validateForgotEmail()) {
                    edtTxt_Pwd.setText("");
                    if (CommonClass.isConnected(Login.this)) {
                        new AsyncGenerateOTP(new Callback<String>() {
                            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                            @Override
                            public void execute(String result, String status) {
                                try {
                                    JSONObject jObject = new JSONObject(result);
                                    JSONArray jsonArray = jObject.getJSONArray("Table");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = new JSONObject(jsonArray.get(i).toString());
                                        if (jsonObject.has("ResponseCode"))
                                            if (jsonObject.getString("ResponseCode").equals("0")) {
                                                alertPIN("Reset Password", jsonObject.getString("ErrorDescription"));
                                            } else {
                                                alertPIN("Oops", jsonObject.getString("ErrorDescription"));
                                            }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    alertPIN("Alert", "Server not responding, please try after sometime");
                                }
                            }
                        }, Login.this, edtTxt_UserID.getText().toString()).execute();
                    } else {
                        alertPIN("No Internet", "No internet connectivity, please try again in connected mode");
                    }
                }
                break;
            case R.id.btn_forlogin:
//                lin_userType.setVisibility(View.GONE);
//                lin_userlogin.setVisibility(View.GONE);
//                lin_createUser.setVisibility(View.GONE);

                if (validateForgotPass()) {
                    if (CommonClass.isConnected(Login.this)) {
                        new SubmitResetPassword(new Callback<String>() {
                            @Override
                            public void execute(String result, String status) {
                                try {
                                    JSONObject jObject = new JSONObject(result);
                                    JSONArray jsonArray = jObject.getJSONArray("Table");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = new JSONObject(jsonArray.get(i).toString());
                                        if (jsonObject.has("ResponseCode"))
                                            if (jsonObject.getString("ResponseCode").equals("0")) {
                                                alertPIN("Password Reset", jsonObject.getString("Message"));
                                            } else {
                                                alertPIN("Oops", jsonObject.getString("Message"));
                                            }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, Login.this, input_foruserID.getText().toString(),
                                input_forotp.getText().toString(),
                                input_cnfpassword.getText().toString()).execute();
                    } else {
                        alertPIN("No Internet", "No internet connectivity, please try again in connected mode");
                    }
                } else {

                }
                break;
            case R.id.btn_forCancel:
                lin_foruserlogin.setVisibility(View.GONE);
                lin_userType.setVisibility(View.GONE);
                lin_userlogin.setVisibility(View.VISIBLE);
                lin_createUser.setVisibility(View.GONE);
                break;
            case R.id.viewPass:
                if (isNotVisible) {
                    isNotVisible = false;
                    edtTxt_Pwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    edtTxt_Pwd.setSelection(edtTxt_Pwd.length());
                    viewPass.setImageDrawable(getResources().getDrawable(R.drawable.ic_view_pass));
                } else {
                    isNotVisible = true;
                    edtTxt_Pwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    edtTxt_Pwd.setSelection(edtTxt_Pwd.length());
                    viewPass.setImageDrawable(getResources().getDrawable(R.drawable.ic_hide_pass));
                }
                break;
            case R.id.viewPass_forgot:
                if (isNotVisibleForgot) {
                    isNotVisibleForgot = false;
                    input_cnfpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    input_cnfpassword.setSelection(input_cnfpassword.length());
                    viewPass.setImageDrawable(getResources().getDrawable(R.drawable.ic_view_pass));
                } else {
                    isNotVisibleForgot = true;
                    input_cnfpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    input_cnfpassword.setSelection(input_cnfpassword.length());
                    viewPass.setImageDrawable(getResources().getDrawable(R.drawable.ic_hide_pass));
                }
                break;
        }
    }

    private boolean validateForgotPass() {
        String eMailValid = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern Email_pattern = Pattern.compile(eMailValid);
        Matcher Email1_matcher = Email_pattern.matcher(input_foruserID.getText().toString());
        boolean IsValidEmail = Email1_matcher.matches();
        if (!IsValidEmail) {
            ToastMsg(getResources().getString(R.string.email_field_valid));
            input_foruserID.setError(getResources().getString(R.string.email_field_valid));
            input_foruserID.requestFocus();
            return false;
        } else if (input_forotp == null || input_forotp.getText().toString().trim().length() < 1) {
            ToastMsg(getResources().getString(R.string.OTP_field_required));
            input_forotp.setError(getResources().getString(R.string.OTP_field_valid));
            input_forotp.requestFocus();
            return false;
        } else if (input_cnfpassword == null || input_cnfpassword.getText().toString().trim().length() < 8) {
            ToastMsg(getResources().getString(R.string.password_field_required));
            input_cnfpassword.setError(getResources().getString(R.string.password_field_valid));
            input_cnfpassword.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    private boolean validateForgotEmail() {
        String eMailValid = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern Email_pattern = Pattern.compile(eMailValid);
        Matcher Email1_matcher = Email_pattern.matcher(edtTxt_UserID.getText().toString());
        boolean IsValidEmail = Email1_matcher.matches();
        if (!IsValidEmail) {
            ToastMsg(getResources().getString(R.string.email_field_valid));
            edtTxt_UserID.setError(getResources().getString(R.string.email_field_valid));
            edtTxt_UserID.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    private boolean validateEmail() {
        String eMailValid = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern Email_pattern = Pattern.compile(eMailValid);
        Matcher Email1_matcher = Email_pattern.matcher(edtTxt_UserID.getText().toString());
        boolean IsValidEmail = Email1_matcher.matches();
        if (!IsValidEmail) {
            ToastMsg(getResources().getString(R.string.email_field_valid));
            edtTxt_UserID.setError(getResources().getString(R.string.email_field_valid));
            edtTxt_UserID.requestFocus();
            return false;
        } else if (edtTxt_Pwd == null || edtTxt_Pwd.getText().toString().trim().length() < 1) {
            ToastMsg(getResources().getString(R.string.password_field_required));
//            edtTxt_Pwd.setError(getResources().getString(R.string.password_field_valid));
            edtTxt_Pwd.setError(getResources().getString(R.string.password_field_required));
            edtTxt_Pwd.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    void ToastMsg(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

    private boolean validateCreateAcc() {
        String eMailValid = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern Email_pattern = Pattern.compile(eMailValid);
        Matcher Email1_matcher = Email_pattern.matcher(edtTxt_useremail.getText().toString());
        boolean IsValidEmail = Email1_matcher.matches();
        if (edtTxt_userename == null || edtTxt_userename.getText().toString().trim().length() < 1) {
            ToastMsg(getResources().getString(R.string.first_name_field_required));
            edtTxt_userename.setError(getResources().getString(R.string.error_field_required));
            edtTxt_userename.requestFocus();
            return false;
        } else if (edtTxt_useremail == null || edtTxt_useremail.getText().toString().trim().length() < 1) {
            ToastMsg(getResources().getString(R.string.email_field_required));
            edtTxt_useremail.setError(getResources().getString(R.string.error_field_required));
            edtTxt_useremail.requestFocus();
            return false;
        } else if (!IsValidEmail) {
            ToastMsg(getResources().getString(R.string.email_field_valid));
            edtTxt_useremail.setError(getResources().getString(R.string.email_field_valid));
            edtTxt_useremail.requestFocus();
            return false;
        } else if (edtTxt_treiner == null || edtTxt_treiner.getText().toString().trim().length() < 1) {
            ToastMsg(getResources().getString(R.string.trainer_field_required));
            edtTxt_treiner.setError(getResources().getString(R.string.trainer_field_required));
            edtTxt_treiner.requestFocus();
            return false;
        } else if (input_nationality == null || input_nationality.getText().toString().trim().length() < 1) {
            ToastMsg(getResources().getString(R.string.nationality_field_required));
            input_nationality.setError(getResources().getString(R.string.nationality_field_required));
            input_nationality.requestFocus();
            return false;
        } else {

            return true;

        }
    }

//    public void offlineLogin() {
//        try {
//            Cursor cursor = DatabaseHelper.db.query(getString(R.string.TBL_iCandidate),
//                    new String[]{"CandidatePin", "AllowOfflineMode", "PermisableDays", "LastAccessDate", "FirstName", "UserType"},
//                    "CandidateLoginId=?", new String[]{edtTxt_UserID.getText().toString()},
//                    null, null, null);
//
//            if (cursor.getCount() > 0) {
//                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
//                    if (cursor.getString(1).equals("Y") &&
//                            cursor.getString(0).equals(edtTxt_Pwd.getText().toString())) {
//
//                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//                        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
////                        String serverDate = sdf.format(sdf.parse(cursor.getString(3)));
//
//                        //remove this if time issue resolved  and uncomment upper line
//                        SimpleDateFormat sdfInput = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//                        Date d = sdfInput.parse(cursor.getString(3));
//                        String serverDate = sdf.format(d);
//
//
//                        Calendar c = Calendar.getInstance();
//                        c.setTime(sdf.parse(serverDate));
//                        c.add(Calendar.DATE, Integer.parseInt(cursor.getString(2)));
//
//                        String argDate = sdf.format(c.getTime());
//                        String curDate = sdf.format(Calendar.getInstance().getTime());
//
//                        if (sdf.parse(curDate).compareTo(sdf.parse(argDate)) <= 0) {
//                            StaticVariables.UserName = cursor.getString(4);
//                            StaticVariables.offlineMode = "Y";
//                            StaticVariables.UserLoginId = edtTxt_UserID.getText().toString();
//                            StaticVariables.isLogin = true;
//                            if (cursor.getString(5).trim().equalsIgnoreCase("C")) {
//                                Intent intent = new Intent(Login.this, ParticipantHomeActivity.class);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                startActivity(intent);
//                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                                Login.this.finish();
//                            } else if (cursor.getString(5).trim().equalsIgnoreCase("T")) {
//                                Intent intent = new Intent(Login.this, LPI_TrainerMenuActivity.class);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                startActivity(intent);
//                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                                Login.this.finish();
//                            } else {
//                                showAlert(Login.this, "Login expired", "Offline login expired, please login with internet connectivity");
//                            }
//                        } else {
//                            showAlert(Login.this, "Login expired", "Offline login expired, please login with internet connectivity");
//                        }
//                    } else {
//                        showAlert(Login.this, "Invalid credentials", "User credentials dose not match, please try again.");
//                    }
//                }
//            } else {
//                showAlert(Login.this, "Login expired", "Offline login expired, please login with internet connectivity");
//            }
//            cursor.close();
//        } catch (Exception e) {
//            Log.e("offlineLogin", "err=" + e.toString());
//            showAlert(Login.this, "Login expired", "Offline login expired, please login with internet connectivity");
//        }
//    }

    public void returnLoginStatus(String regDevice) {
        // Create request

        org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                Login.this.getString(R.string.Exam_NAMESPACE),
                Login.this.getString(R.string.AuthenticateUser_Email));

        // property which holds input parameters
        PropertyInfo inputPI1 = new PropertyInfo();
        PropertyInfo inputPI2 = new PropertyInfo();
        PropertyInfo inputPI3 = new PropertyInfo();
        PropertyInfo inputPI4 = new PropertyInfo();
        PropertyInfo inputPI5 = new PropertyInfo();

        inputPI1.setName("EmailId");
        try {
            Log.e("", "UserID=" + edtTxt_UserID.getText().toString());
            //inputPI1.setValue(EncrptDcrpt.XMLEncryptiton(edtTxt_UserID.getText().toString()));
            inputPI1.setValue(edtTxt_UserID.getText().toString());
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        inputPI1.setType(String.class);
        request.addProperty(inputPI1);

        inputPI2.setName("Password");
        try {
            //inputPI2.setValue(EncrptDcrpt.XMLEncryptiton(edtTxt_Pwd.getText().toString()));
            inputPI2.setValue(edtTxt_Pwd.getText().toString().trim());
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        inputPI2.setType(String.class);
        request.addProperty(inputPI2);

        inputPI3.setName("IMEIstring");
        inputPI3.setValue(CommonClass.getIMEINumber(Login.this));
        inputPI3.setType(String.class);
        request.addProperty(inputPI3);

        inputPI4.setName("AppVersion");
        try {
            inputPI4.setValue((getPackageManager().getPackageInfo(getPackageName(), 0).versionCode));
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        inputPI4.setType(String.class);
        request.addProperty(inputPI4);

        inputPI5.setName("RegDevice");
        inputPI5.setValue(regDevice);
        inputPI5.setType(String.class);
        request.addProperty(inputPI5);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.dotNet = true;
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(Login.this.getString(R.string.Exam_URL), 90000);


        try {
            // Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
            androidHttpTransport.call(Login.this.getString(R.string.Exam_SOAP_ACTION) +
                    Login.this.getString(R.string.AuthenticateUser_Email), envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            // Assign it to static variable
            strLoginResponse = response.toString();
            Log.e("", "response=" + strLoginResponse);
        } catch (Exception e) {
            Log.e("", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(Login.this, DatabaseHelper.db,
                    "Login", "returnLoginStatus", e.toString());
        }
    }

    private void alertPIN(final String Title, String Message) {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(Login.this).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_exit_dialog, null);
            TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            Button btn_ok = (Button) dialogView.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialogView.findViewById(R.id.btn_no);
            btn_no.setVisibility(View.GONE);
//            promt_title.setText("MPIN Authentication");Invalid credentials,
//            txt_message.setText("Do you want to set a 4 digit authentication MPIN for future Login?");
            promt_title.setText(Title);
            txt_message.setText(Message);
            btn_ok.setText("OK");
            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    if (Title.equals("Reset Password")) {
                        lin_userType.setVisibility(View.GONE);
                        lin_userlogin.setVisibility(View.GONE);
                        lin_createUser.setVisibility(View.GONE);
                        lin_foruserlogin.setVisibility(View.VISIBLE);
                        input_foruserID.setText(edtTxt_UserID.getText().toString().trim());
                        input_forotp.setText("");
                        input_cnfpassword.setText("");
                    }

                    if (Title.equals("Password Reset")) {
                        lin_foruserlogin.setVisibility(View.GONE);
                        lin_createUser.setVisibility(View.GONE);
                        lin_userType.setVisibility(View.GONE);
                        lin_userlogin.setVisibility(View.VISIBLE);
                    }
                    if (Title.equals("User Registration")) {
                        lin_foruserlogin.setVisibility(View.GONE);
                        lin_createUser.setVisibility(View.GONE);
                        lin_userType.setVisibility(View.GONE);
                        lin_userlogin.setVisibility(View.VISIBLE);
                    }
//                    Intent intent = new Intent(Login.this, PIN_AuthActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    intent.putExtra("user_id", edtTxt_UserID.getText().toString().trim());
//                    startActivity(intent);
//                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                    Login.this.finish();
                }
            });
//            btn_no.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    alertDialog.dismiss();
//                    if (StaticVariables.UserType.trim().equalsIgnoreCase("C")) {
//                        Intent intent = new Intent(Login.this, ParticipantHomeActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        startActivity(intent);
//                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                        Login.this.finish();
//                    } else if (StaticVariables.UserType.trim().equalsIgnoreCase("T")) {
//                        Intent intent = new Intent(Login.this, LPI_TrainerMenuActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        startActivity(intent);
//                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                        Login.this.finish();
//                    } else {
//                        alertPIN("Invalid credentials", "Please login using valid credentials with internet connectivity");
//                    }
//                }
//            });

            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finishAffinity();
            System.exit(0);
        }

        this.doubleBackToExitPressedOnce = true;
        Snackbar.make(findViewById(R.id.activity_login), "Please click BACK again to exit", Snackbar.LENGTH_LONG).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show, final String Msg) {
        if (show) {
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(Msg);
            progressDialog.setCancelable(false);
            progressDialog.show();
        } else
            progressDialog.dismiss();
    }

//    public void showAlert(final Context context, final String title, String msg) {
//
//        AlertDialog.Builder builder = CommonClass.DialogOpen(context, title, msg);
//        builder.setCancelable(false);
//        showProgress(false, "");
//        if (title.equals("Device authentication failed")) {
//            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    dialog.dismiss();
//                    DatabaseHelper.db.delete(context.getResources().getString(R.string.TBL_iCandidate), null, null);
//                    new CandidateLogin(0, Login.this.getApplication(), "Y").execute();
//                }
//            });
//
//            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    dialog.dismiss();
//                    showProgress(false, "");
////                    if (title.equals("Offline Login")) {
////                        offlineLogin();
////                    } else
//                    if (title.equals("Device authentication failed")) {
//                        Login.this.recreate();
//                    }
//                }
//            });
//        } else if (title.equals("Offline Login")) {
//            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    dialog.dismiss();
//                    showProgress(false, "");
//                }
//            });
//        } else {
//            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    dialog.dismiss();
//                    showProgress(false, "");
////                    if (title.equals("Offline Login")) {
////                        offlineLogin();
////                    }
//                }
//            });
//        }
//        AlertDialog dialog = builder.create();
//        dialog.show();
//    }

    private void syncData() {
        try {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    getResources().getString(R.string.Exam_NAMESPACE), getResources().getString(R.string.GetActivityMaster));
            //property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            inputPI1.setName("objXMLDoc");
            String objXMLDoc = CommonClass.GenerateXML(getApplicationContext(), 0, "GetActivityMaster", new String[]{""}, new String[]{""});
            inputPI1.setValue(objXMLDoc);
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);
            envelope.dotNet = true;
            //Set output SOAP object
            envelope.setOutputSoapObject(request);
            //Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(getResources().getString(R.string.Exam_URL));
//        InputStream isResponse = null;

            //Involve Web service
            LPIEEX509TrustManager.allowAllSSL();
            androidHttpTransport.call(getResources().getString(R.string.Exam_SOAP_ACTION) + getResources().getString(R.string.GetActivityMaster), envelope);
            //Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            JSONObject objJsonObject = new JSONObject(response.toString());
            JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");
            for (int index = 0; index < objJsonArray.length(); index++) {
                JSONObject jsonobject = objJsonArray.getJSONObject(index);
                DatabaseHelper.InsertActivityMaster(getApplicationContext(), DatabaseHelper.db,
                        jsonobject.getString("ActivityCode"),
                        jsonobject.getString("ActivityDesc"));
            }


        } catch (Exception e) {
            Log.e("Login", "Login Error=" + e.toString());
            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                    "Login", "onHandleIntent", e.toString());
//            onDestroy();
        }
    }

    public class CandidateLogin extends AsyncTask<String, String, JSONObject> {

        public int position;
        public Context context;
        public String regDevice;

        public CandidateLogin(int position, Context con, String regDevice) {
            this.position = position;
            this.context = con;
            this.regDevice = regDevice;
        }

        @Override
        protected void onPreExecute() {
            showProgress(true, "Authenticating.....");
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            // TODO Auto-generated method stub

            syncData();
            returnLoginStatus(regDevice);
            JSONObject objRet = new JSONObject();
            return objRet;
        }

        @Override
        protected void onCancelled() {
            showProgress(false, "");
        }

        @SuppressLint("WrongThread")
        @Override
        protected void onPostExecute(JSONObject result) {
            try {
                boolean isSuccess = false;
                JSONObject objJsonObject = new JSONObject(strLoginResponse);
                JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");
                for (int index = 0; index < objJsonArray.length(); index++) {
                    JSONObject jsonobject = objJsonArray.getJSONObject(index);

                    String str_login = jsonobject.getString("Message");
                    if (str_login.equals("Device authentication failed")) {
                        alertPIN("Device authentication failed", "Device authentication failed...!\nDo you want to register this device?");
                    } else if (str_login.equals("Outdated version found")) {
                        showProgress(false, "");
                        String[] arrAppLink = ("" + jsonobject.get("AppLink")).split(" and ");
                        String strAppLink = arrAppLink[0].replace("Android - ", "");
                        alertOutdatedVersion(context, "\nYou are currently using an outdated version of App, "
                                        + "\nPlease install the latest version of Android App from below options-",
                                strAppLink + "");
                    } else if (str_login.equals("Login Success")) {
                        isSuccess = true;
                        DatabaseHelper.UserName(Login.this, DatabaseHelper.db, edtTxt_UserID.getText().toString());
                        StaticVariables.crnt_InscType = "" + jsonobject.get("Ins_Type");
                        StaticVariables.isLogin = true;
                        StaticVariables.UserName = "" + jsonobject.get("LegalName");
                        StaticVariables.UserLoginId = "" + jsonobject.get("UserId");
                        StaticVariables.ParentId = "" + jsonobject.get("ParentId");
                        StaticVariables.ParentName = "" + jsonobject.get("ParentName");
                        StaticVariables.UserMobileNo = "" + jsonobject.get("UserMobileNo1");
                        StaticVariables.offlineMode = "N";
                        StaticVariables.AppLink = "" + jsonobject.get("AppLink");
                        StaticVariables.UserType = "" + jsonobject.get("UserType");
                        byte[] imageBytes = null;
                        try {
                            imageBytes = Base64.decode(jsonobject.getString("Image"), Base64.DEFAULT);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (imageBytes != null) {
                        } else {
                            try {
                                Drawable drawable = null;
                                drawable = context.getResources().getDrawable(R.drawable.profile_blank_m);
                                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                                imageBytes = stream.toByteArray();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        StaticVariables.profileBitmap = imageBytes;
                        DatabaseHelper.SaveUserInfo(Login.this, DatabaseHelper.db,
                                "" + jsonobject.get("UserId"),
                                edtTxt_Pwd.getText().toString().trim(),
                                "" + jsonobject.get("LegalName"),
                                StaticVariables.crnt_InscType,
                                "" + jsonobject.get("AllowOfflineMode"),
                                "" + jsonobject.get("PermisableDays"),
                                "" + jsonobject.get("LastAccessDate"),
                                "" + jsonobject.get("UserType"),
                                "" + jsonobject.get("UserEMailAddress"),
                                "" + jsonobject.get("UserMobileNo1"),
                                "" + jsonobject.get("ParentId"),
                                "" + jsonobject.get("ParentName"),
                                imageBytes,
                                "" + jsonobject.get("AppLink"));

                        //                        DatabaseHelper.InsertActivityTracker(context,DatabaseHelper.db,StaticVariables.UserLoginId,code,"Login",
//                                "","","",);
                        String code = DatabaseHelper.getActivityMaster(context, DatabaseHelper.db, "Login");
                        DatabaseHelper.InsertActivityTracker(context, DatabaseHelper.db, StaticVariables.UserLoginId,
                                code, "", "Login", "", "", "",
                                StaticVariables.sdf.format(new Date()), StaticVariables.sdf.format(new Date()), "", "", "", "", "", "Pending");
                    } else {
                        //Toast.makeText(Login.this, "Invalid login credentials", Toast.LENGTH_SHORT).show();
                        showProgress(false, "");
                        alertPIN("Invalid credentials", "User credentials dose not match, please try again.");
                    }
                }
                if (isSuccess && CommonClass.isConnected(context)) {
                    /*if (StaticVariables.UserType.trim().equalsIgnoreCase("C")) {*/
                    if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncPostLicModuleData")) {
                        Intent intent = new Intent(getApplicationContext(), SyncPostLicModuleData.class);
//                        startService(intent);
                        CommonClass.StartService(getApplicationContext(), intent);
                        /*  }*/
                    }
                    new CheckForChangeInMasters(0, Login.this).execute();

                    new syncExams(new Callback<String>() {
                        @Override
                        public void execute(String result, String status) {
                            showProgress(false, "");
//                        CommonClass.startFAQJob(getApplicationContext());
                            if (CommonClass.isConnected(Login.this)) {
//                                showProgress(false, "");
                                new GetAtemptedExams(Login.this, StaticVariables.UserLoginId, new Callback() {
                                    @Override
                                    public void execute(Object result, String status) {
                                        Intent alarmIntent = new Intent(Login.this, AlarmReceiver.class);
                                        PendingIntent pendingIntent;// = PendingIntent.getBroadcast(Login.this, 0, alarmIntent, 0);
//                                        pendingIntent= PendingIntent.getBroadcast(context.getApplicationContext(), 0, alarmIntent, 0);
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                                            pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), 0, alarmIntent, PendingIntent.FLAG_IMMUTABLE);
                                        } else {
                                            pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), 0, alarmIntent, PendingIntent.FLAG_MUTABLE);
                                        }
                                        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                                        int interval = 5000;
                                        manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
                                        if (StaticVariables.UserType.trim().equalsIgnoreCase("C")) {
                                            Intent intent = new Intent(Login.this, ParticipantHomeActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                            Login.this.finish();
                                        } else if (StaticVariables.UserType.trim().equalsIgnoreCase("T")) {
                                            Intent intent = new Intent(Login.this, LPI_TrainerMenuActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                            Login.this.finish();
                                        } else {
                                            alertPIN("Invalid credentials", "Please login using valid credentials with internet connectivity");
                                        }
                                    }
                                }).execute();
                            }
                        }
                    }, Login.this, "Login", StaticVariables.UserLoginId, "").execute();
                }
            } catch (Exception e) {
                // TODO: handle exception
                Log.e("", "error=" + e.toString());
                showProgress(false, "");
                //Toast.makeText(Login.this, "Server not responding....\nPlease try again later", Toast.LENGTH_LONG).show();
                alertPIN("Oops", "Server not responding...\nPlease try again later");
                DatabaseHelper.SaveErroLog(Login.this, DatabaseHelper.db,
                        "Login", "CandidateLogin.onPostExecute()", e.toString());
            }

        }
    }

    public class CheckForChangeInMasters extends AsyncTask<String, String, InputStream> {
        public int position;
        public Context context;

        public CheckForChangeInMasters(int position, Context con) {
            this.position = position;
            this.context = con;
        }


        @SuppressLint("NewApi")
        @Override
        protected InputStream doInBackground(String... params) {
            try {
                org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                        Login.this.getString(R.string.Exam_NAMESPACE),
                        Login.this.getString(R.string.GetChangesInMasters));

                // property which holds input parameters
                PropertyInfo inputPI1 = new PropertyInfo();
                inputPI1.setName("objXMLDoc");

//                String objXMLDoc = CommonClass.GenerateXML(context, 0,
//                        "MasterChanges", new String[]{"MasterChangesVer"},
//                        new String[]{""+DatabaseHelper.ChkMstSUChangeVer(Login.this, DatabaseHelper.db)});
                String objXMLDoc = CommonClass.GenerateXML(context, 0,
                        "MasterChanges", new String[]{""}, new String[]{""});
                inputPI1.setValue(objXMLDoc);
                inputPI1.setType(String.class);
                request.addProperty(inputPI1);

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                        SoapEnvelope.VER12);
                envelope.dotNet = true;
                // Set output SOAP object
                envelope.setOutputSoapObject(request);
                // Create HTTP call object
                HttpTransportSE androidHttpTransport = new HttpTransportSE(Login.this.getString(R.string.Exam_URL));
                InputStream isResponse = null;
                // Involve Web service
                LPIEEX509TrustManager.allowAllSSL();
                androidHttpTransport.call(Login.this.getString(R.string.Exam_SOAP_ACTION) +
                        Login.this.getString(R.string.GetChangesInMasters), envelope);
                // Get the response
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

                String strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString();
                Log.e("", "response=" + strResponse);

                isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
                MasterInputStream = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
                Log.e("", "Response=" + isResponse);
                return isResponse;
            } catch (Exception e) {
                ResponseCode = 2;
                Log.e("", "Login Error=" + e.toString());
                DatabaseHelper.SaveErroLog(Login.this, DatabaseHelper.db,
                        "Login", "CheckForChangeInMasters.doInBackground()", e.toString());
                return null;
            }

        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            boolean isSuccess = false;
            try {
                if (inputStream != null) {
                    if (CommonClass.IsCorrectXMLResponse(inputStream)) {
                        NodeList nList = CommonClass.GetXMLElementNodeList(MasterInputStream, "MasterChanged");
                        for (int i = 0; i < nList.getLength(); i++) {
                            Node node = nList.item(i);
                            if (node.getNodeType() == Node.ELEMENT_NODE) {
                                Element element = (Element) node;
                                MasterChangeVer = CommonClass.getXMLElementValue("MasterChangeVer", element);

                                isSuccess = true;
                                DatabaseHelper.SaveChangeMaster(Login.this, DatabaseHelper.db,
                                        CommonClass.getXMLElementValue("MasterChangeFlag", element),
                                        MasterChangeVer, CommonClass.getXMLElementValue("MasterTableChangeName", element));
                            }
                        }
                        if (isSuccess) {
                            if (DatabaseHelper.NeedToFetchMaster(Login.this, DatabaseHelper.db, MasterChangeVer)) {
                                DatabaseHelper.db.delete(getApplicationContext().getResources().getString(R.string.TBL_LPIChangeMaster), null, null);
                                new GetlookupMasters(0, Login.this).execute();

                            }
                        }

                    }
                } else if (ResponseCode == 2) {
                    showProgress(false, "");
                    //Toast.makeText(Login.this, "Server is not responding...", Toast.LENGTH_LONG).show();
                    alertPIN("Oops", "Server not responding...\nPlease try again later");
                } else {
                    showProgress(false, "");
                    //Toast.makeText(Login.this, "Something went worng...", Toast.LENGTH_LONG).show();
                    alertPIN("Oops", "Server not responding...\nPlease try again later");
                }

            } catch (Exception e) {
                e.printStackTrace();
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "CheckForChangeInMasters", "postexcecute", e.toString());
            }
        }
    }


    public class GetlookupMasters extends AsyncTask<String, String, InputStream> {
        public int position;
        public Context context;

        public GetlookupMasters(int position, Context con) {
            this.position = position;
            this.context = con;
        }

        @SuppressLint("NewApi")
        @Override
        protected InputStream doInBackground(String... params) {
            try {
                org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                        Login.this.getString(R.string.Exam_NAMESPACE),
                        Login.this.getString(R.string.GetLookUpMaster));

                // property which holds input parameters
                PropertyInfo inputPI1 = new PropertyInfo();
                inputPI1.setName("objXMLDoc");
                String objXMLDoc = CommonClass.GenerateXML(context, 0, "LookupMaster", new String[]{""}, new String[]{""});
                inputPI1.setValue(objXMLDoc);
                inputPI1.setType(String.class);
                request.addProperty(inputPI1);

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                        SoapEnvelope.VER12);
                envelope.dotNet = true;
                // Set output SOAP object
                envelope.setOutputSoapObject(request);
                // Create HTTP call object
                HttpTransportSE androidHttpTransport = new HttpTransportSE(Login.this.getString(R.string.Exam_URL));
                InputStream isResponse = null;


                // Involve Web service
                LPIEEX509TrustManager.allowAllSSL();
                androidHttpTransport.call(Login.this.getString(R.string.Exam_SOAP_ACTION) +
                        Login.this.getString(R.string.GetLookUpMaster), envelope);
                // Get the response
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

                String strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString();
                Log.e("", "response=" + strResponse);

                isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
                LookupInputStream = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));


                Log.e("", "Response=" + isResponse);
                return isResponse;
            } catch (Exception e) {
                Log.e("", "Login Error=" + e.toString());
                ResponseCode = 2;
                DatabaseHelper.SaveErroLog(Login.this, DatabaseHelper.db,
                        "Login", "GetlookupMasters.doInBackground()", e.toString());
                return null;
            }
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            try {
                if (inputStream != null) {
                    if (CommonClass.IsCorrectXMLResponse(inputStream)) {
                        NodeList nList = CommonClass.GetXMLElementNodeList(LookupInputStream, "LookupData");
                        for (int i = 0; i < nList.getLength(); i++) {
                            Node node = nList.item(i);
                            if (node.getNodeType() == Node.ELEMENT_NODE) {
                                Element element = (Element) node;
                                DatabaseHelper.SaveIsysLookupParam(Login.this, DatabaseHelper.db,
                                        CommonClass.getXMLElementValue("LookupCode", element),
                                        CommonClass.getXMLElementValue("ParamValue", element),
                                        CommonClass.getXMLElementValue("ParamDesc01", element));
                            }
                        }
                    }
                } else if (ResponseCode == 2) {
                    //Toast.makeText(Login.this, "Server not responding...", Toast.LENGTH_LONG).show();
                    showProgress(false, "");
                    alertPIN("Oops", "Server not responding...\nPlease try again later");
                } else {
//                Toast.makeText(Login.this, "Something went worng...", Toast.LENGTH_LONG).show();
                    showProgress(false, "");
                    alertPIN("Oops", "Server not responding...\nPlease try again later");
                }
            } catch (Exception e) {
                DatabaseHelper.SaveErroLog(Login.this, DatabaseHelper.db,
                        "Login", "GetlookupMasters.onPostExecute()", e.toString());
            }

        }
    }

    private void alertOutdatedVersion(final Context context, String Message, final String App_Link) {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_outdated_version_dialog, null);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            TextView txt_app_link = (TextView) dialogView.findViewById(R.id.txt_app_link);
            Button btn_no = (Button) dialogView.findViewById(R.id.btn_no);
            ImageView iv_play_store = (ImageView) dialogView.findViewById(R.id.iv_play_store);
            ImageView iv_g_drive = (ImageView) dialogView.findViewById(R.id.iv_g_drive);
//            btn_no.setVisibility(View.GONE);
            txt_app_link.setText(App_Link);
            txt_message.setText(Message);
            txt_app_link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    } catch (Exception e) {
                        Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(App_Link));
                        webIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        webIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        webIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(webIntent);
                    }
                }
            });
            iv_play_store.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    } catch (Exception e) {
                        Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(App_Link));
                        webIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        webIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        webIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(webIntent);
                    }
                }
            });
            iv_g_drive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(App_Link));
                    webIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    webIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    webIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(webIntent);
                }
            });
            txt_app_link.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("AppLink", App_Link);
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(context.getApplicationContext(), "App link copied to clipboard", Toast.LENGTH_LONG).show();
                    return true;
                }
            });
            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDialog(final String title, String message, final String link) {
        final AlertDialog alertDialog = new AlertDialog.Builder(Login.this).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = Login.this.getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.custom_exit_dialog, null);
            TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            Button btn_ok = (Button) dialogView.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialogView.findViewById(R.id.btn_no);
            btn_no.setVisibility(View.GONE);
            btn_ok.setText("Ok");
            if (title.equalsIgnoreCase("Security Alert")) {
                btn_no.setVisibility(View.GONE);
            }
            promt_title.setText(title);
            txt_message.setText(message);

            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    if (title.equalsIgnoreCase("Security Alert")) {
                        Login.this.finishAffinity();
                    }
                }
            });
            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.setView(dialogView);
        } catch (Exception e) {

        }
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
    }

}