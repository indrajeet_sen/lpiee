package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.GetCandidateAttemptedExamsDetails;
import com.lpi.kmi.lpi.activities.AsyncTasks.SubmitExamResultFdback;
import com.lpi.kmi.lpi.activities.AsyncTasks.syncExams;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;

@SuppressLint("Range")
public class MCQ_QuestionActivity extends AppCompatActivity implements View.OnClickListener {

//    Button btn_SDisagree,btn_LDisagree,btn_NDisagree,btn_LAgree,btn_SAgree;

    TextView txt_SrNo_MCQue, txt_MCQue, textBtn_ReviewLater, txt_progress;
    ProgressBar Exm_progressBar;
    TextView txt_A, txt_B, txt_C, txt_D, txt_E;
    //    Button btn_1stAnsOpt,btn_2ndAnsOpt,btn_3rdAnsOpt,btn_4thAnsOpt,btn_psycho5thOpt;
    LinearLayout btn_1stAnsOpt, btn_2ndAnsOpt, btn_3rdAnsOpt, btn_4thAnsOpt, btn_psycho5thOpt;
    Button btn_Next, btn_Submit, btn_allowPrevious, btn_allowNext;
    TextView txt_1stAnsOpt, txt_2ndAnsOpt, txt_3rdAnsOpt, txt_4thAnsOpt, txt_5thAnsOpt, txt_ExamType, txt_QueCount;
    TextView txt_hint;
    RelativeLayout rel_WithNav, rel_NoNav;
    //    LinearLayout lnr_psycho5thOpt;str_CrntQuestion,
//    ImageView img_exmType1Icon, img_exmType4Icon;
    String str_SelectedAnswer, str_QuestionAttempted_Flag, str_SelectedAnswerText, str_SelectedAnswerId, str_SelectedAnswerSequence;
    boolean ShowAlert_Flag, isAllowNav;//, isSubmit = false;
    int QueSrNo = 0, ResponseCode;
    InputStream SubmitExmInputStream;
    RelativeLayout activity_mcq_question;
    static String initFrom = "";
    static private String strSectionId = "";
    static private String Lesson = "", LessonDesc = "", Trip = "";
    int attemptId = 0;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_mcq_question);
        activity_mcq_question = (RelativeLayout) findViewById(R.id.activity_mcq_question);
        activity_mcq_question.setVisibility(View.GONE);

        try {
            initFrom = getIntent().getStringExtra("initFrom");
        } catch (Exception e) {
        }
        try {
            strSectionId = getIntent().getExtras().getString("SectionId");
            Lesson = getIntent().getExtras().getString("Lesson");
            LessonDesc = getIntent().getExtras().getString("LessonDesc");
            Trip = getIntent().getExtras().getString("Trip");
        } catch (Exception e) {
            e.printStackTrace();
        }

//        if( StaticVariables.offlineMode.equals("Y")){
        checkExamSet();
//        }else {
//            ExamInfo();
//        }
    }

    public void checkExamSet() {

//        if (StaticVariables.crnt_ExamType==5) {
//            init();
//        }else {
        attemptId = DatabaseHelper.CheckAttempts(MCQ_QuestionActivity.this,
                DatabaseHelper.db, "" + StaticVariables.crnt_ExmId, StaticVariables.UserLoginId);
        int AllowedAttempt = DatabaseHelper.AllowedAttempt(MCQ_QuestionActivity.this,
                DatabaseHelper.db, "" + StaticVariables.crnt_ExmId, StaticVariables.UserLoginId);
        if (AllowedAttempt - attemptId > 0) {
            if (DatabaseHelper.PPExamQueCount(MCQ_QuestionActivity.this, DatabaseHelper.db, "" + StaticVariables.crnt_ExmId) < 20 &&
                    StaticVariables.crnt_ExamType == 4) {
                custom_SubmitTestAlert("Alert!", "Exam set not available, " +
                        "please make sure that you are connected to the internet to download exam set");
            } else if (!DatabaseHelper.CheckExamQueCount(MCQ_QuestionActivity.this, DatabaseHelper.db, StaticVariables.crnt_ExmId)) {
                custom_SubmitTestAlert("Alert!", "Exam set not available, " +
                        "please make sure that you are connected to the internet to download exam set");
            } else if (StaticVariables.crnt_ExamType == 5) {
                init();
            } else {
                ExamInfo();
            }
        } else if (StaticVariables.crnt_ExamType == 6 && attemptId > 0) {
            //&& StaticVariables.UserType.trim().equalsIgnoreCase("C")
            init();
            attemptId = 0; // for 1 attempted exam
            Cursor mCursor = DatabaseHelper.GetAllAttemptedAnsDetails(MCQ_QuestionActivity.this, DatabaseHelper.db,
                    Integer.parseInt(StaticVariables.crnt_ExmId + ""), StaticVariables.UserLoginId);
            if (mCursor != null && mCursor.getCount() > 0) {
                custom_SubmitTestAlert(StaticVariables.crnt_ExmName, "" + StaticVariables.crnt_ExmName + " practice exam already submitted by user.");
            } else {
                if (CommonClass.isConnected(MCQ_QuestionActivity.this)) {
                    new GetCandidateAttemptedExamsDetails(MCQ_QuestionActivity.this, "" + StaticVariables.crnt_ExmId, StaticVariables.UserLoginId,
                            "MCQ_QuestionActivity", new Callback() {
                        @Override
                        public void execute(Object result, String status) {
                            if (result != null && result.equals("Success")) {
                                custom_SubmitTestAlert(StaticVariables.crnt_ExmName, "" + StaticVariables.crnt_ExmName + " practice exam already submitted by user.");
                            } else {
                                alertCustomDialog("Exam Alert", "Exam details not found, please try again.");
                            }
                        }
                    }).execute();
                } else {
                    alertCustomDialog("No Internet",
                            "Attempted exam sync from server failed. \nThere is no internet connection found, Please connect to internet and try again.");
                }
            }
        } else {
            if (StaticVariables.crnt_ExamType == 6 && attemptId == 0 && AllowedAttempt == 0) {
                custom_SubmitTestAlert("Attention", "Exam set is currently not available.");
            } else {
                custom_SubmitTestAlert("Attention", "You are out of permissible attempts, " +
                        "please contact administrator if you wish to repeat the LPI test again.");
            }
        }
//        }
    }

    public void ExamInfo() {
        try {

            final AlertDialog alertD;
            LayoutInflater logout_inflator = LayoutInflater.from(MCQ_QuestionActivity.this);
            final View openDialog = logout_inflator.inflate(R.layout.exam_info, null);
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MCQ_QuestionActivity.this);
            alertDialogBuilder.setView(openDialog);
            alertD = alertDialogBuilder.create();
            alertD.setCancelable(false);
            alertD.show();

            final TextView dtitle = (TextView) openDialog.findViewById(R.id.dtitle);
            final TextView txtExamName = (TextView) openDialog.findViewById(R.id.txtExamName);
            final TextView txtExamDesc = (TextView) openDialog.findViewById(R.id.txtExamDesc);
            final TextView txtExamType = (TextView) openDialog.findViewById(R.id.txtExamType);
            final TextView txtNoOfQue = (TextView) openDialog.findViewById(R.id.txtNoOfQue);
            final TextView txtMandet = (TextView) openDialog.findViewById(R.id.txtMandet);
            final TextView txtAttempt = (TextView) openDialog.findViewById(R.id.txtAttempt);
            final TextView txtMinPassing = (TextView) openDialog.findViewById(R.id.txtMinPassing);
            final TextView txtExamDuration = (TextView) openDialog.findViewById(R.id.txtExamDuration);
            final Button btnStartExam = (Button) openDialog.findViewById(R.id.btnStartExam);
            final Button btnCancelExam = (Button) openDialog.findViewById(R.id.btnCancelExam);

            if (StaticVariables.crnt_ExamType == 6){
                dtitle.setText("QUIZ DETAILS");
                btnStartExam.setText("START QUIZ");
            }else {
                btnStartExam.setText("START TEST");
            }


            if (StaticVariables.crnt_ExamType == 4) {
                txtExamName.setText("Test Name : LPI " + StaticVariables.crnt_ExmName);
                txtExamDesc.setText("Test Description : Psychometric profiling with 20 questions to " +
                        "be taken by candidate only once, please contact administrator if you wish to repeat the profiling");
            } else if (StaticVariables.crnt_ExamType == 6){
                txtExamName.setText("Quiz Name : " + StaticVariables.crnt_ExmName);
                txtExamDesc.setText("Quiz Description : " + StaticVariables.crnt_ExmTypeName);
            }else {
                txtExamName.setText("Test Name : " + StaticVariables.crnt_ExmName);
                txtExamDesc.setText("Test Description : " + StaticVariables.crnt_ExmTypeName);
            }

            if (StaticVariables.crnt_ExamType == 1) {
                txtExamType.setText("Test Type : Practice exam with preselected answers");
            } else if (StaticVariables.crnt_ExamType == 2) {
                txtExamType.setText("Test Type : Practice exam with corrections");
            } else if (StaticVariables.crnt_ExamType == 3 || StaticVariables.crnt_ExamType == 4) {
                txtExamType.setText("Test Type : Regular Exam");
            } else if (StaticVariables.crnt_ExamType == 6) {
                txtExamType.setText("Quiz Type : " + StaticVariables.crnt_ExmName);
            }

            txtNoOfQue.setText("Total Questions : " + StaticVariables.TblNameForCount);

            txtAttempt.setText("Permissible Attempts : " + StaticVariables.allowedAttempts);

            if (StaticVariables.IsAllQuesMendatory.equals("Y")) {
                txtMandet.setText("Questions attempt mandatory : Yes");
            } else {
                txtMandet.setText("Questions attempt mandatory : NO");
            }
            if (StaticVariables.crnt_ExamType == 4 || StaticVariables.crnt_ExamType == 6) {
                txtMinPassing.setVisibility(View.GONE);
            } else {
                txtMinPassing.setText("Minimum passing criterion : 40");
            }

            if (StaticVariables.crnt_ExamType == 6) {
                txtExamDuration.setText("Quiz Duration (in minutes) : " + StaticVariables.crnt_ExamDuration);
            }else {
                txtExamDuration.setText("Test Duration (in minutes) : " + StaticVariables.crnt_ExamDuration);
            }


            btnStartExam.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertD.dismiss();
                    init();
                }
            });

            btnCancelExam.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertD.dismiss();
                    GoToPreviousCall();
                }
            });

        } catch (Exception e) {
            Log.e("ExamInfo", "error@ExamInfo=" + e.toString());
            DatabaseHelper.SaveErroLog(MCQ_QuestionActivity.this, DatabaseHelper.db,
                    "MCQ_QuestionActivity", "ExamInfo", e.toString());
        }
    }

    protected void GoToPreviousCall() {
        Intent intent;
        if (initFrom.equals("ParticipantTest")) {
            intent = new Intent(MCQ_QuestionActivity.this, ParticipantTest_Personal_info.class);
        } else if (initFrom.equals("ViewResultActivity")) {
            StaticVariables.crnt_ExmId = BigInteger.valueOf(4001);
            intent = new Intent(MCQ_QuestionActivity.this, ViewResultActivity.class);
            intent.putExtra("ExamId", "" + StaticVariables.crnt_ExmId);
            intent.putExtra("AttemptedId", "");
            intent.putExtra("CandidateId", "" + StaticVariables.UserLoginId);
            intent.putExtra("initFrom", "MCQ_QuestionActivity");
        } else if (initFrom.equals("Custom_GridAdapter") && StaticVariables.UserType.trim().equalsIgnoreCase("C")) {
            intent = new Intent(MCQ_QuestionActivity.this, ParticipantMenuActivity.class);
        } else if (initFrom.equals("TrainerGridAdapter") && StaticVariables.UserType.trim().equalsIgnoreCase("T")) {
            intent = new Intent(MCQ_QuestionActivity.this, LPI_TrainerMenuActivity.class);
        } else if (initFrom.equals("ExamTypeList")) {
            intent = new Intent(MCQ_QuestionActivity.this, ExamList_Activity.class);
        } else if (initFrom.equals("LPILessonQuizActivity")) {
            intent = new Intent(MCQ_QuestionActivity.this, LPILessonQuizActivity.class);
            intent.putExtra("SectionId", strSectionId);
            intent.putExtra("Lesson", Lesson);
            intent.putExtra("LessonDesc", LessonDesc);
            intent.putExtra("Trip", Trip);
        } else {
            if (StaticVariables.UserType.equalsIgnoreCase("C")) {
                intent = new Intent(MCQ_QuestionActivity.this, ParticipantMenuActivity.class);
            } else {
                intent = new Intent(MCQ_QuestionActivity.this, LPI_TrainerMenuActivity.class);
            }
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        MCQ_QuestionActivity.this.startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        MCQ_QuestionActivity.this.finish();
    }

    public void init() {

        progressDialog = new ProgressDialog(MCQ_QuestionActivity.this, R.style.AppTheme_Dark_Dialog);
        txt_SrNo_MCQue = (TextView) findViewById(R.id.txt_SrNo_MCQue);
        txt_MCQue = (TextView) findViewById(R.id.txt_MCQue);
        textBtn_ReviewLater = (TextView) findViewById(R.id.textBtn_ReviewLater);
        txt_ExamType = (TextView) findViewById(R.id.txt_ExamType);
        txt_QueCount = (TextView) findViewById(R.id.txt_QueCount);

        btn_1stAnsOpt = (LinearLayout) findViewById(R.id.btn_1stAnsOpt);
        btn_2ndAnsOpt = (LinearLayout) findViewById(R.id.btn_2ndAnsOpt);
        btn_3rdAnsOpt = (LinearLayout) findViewById(R.id.btn_3rdAnsOpt);
        btn_4thAnsOpt = (LinearLayout) findViewById(R.id.btn_4thAnsOpt);
        btn_psycho5thOpt = (LinearLayout) findViewById(R.id.btn_psycho5thOpt);

        btn_Next = (Button) findViewById(R.id.btn_Next);
        btn_Submit = (Button) findViewById(R.id.btn_Submit);
        btn_allowPrevious = (Button) findViewById(R.id.btn_allowPrevious);
        btn_allowNext = (Button) findViewById(R.id.btn_allowNext);
        btn_allowNext.setVisibility(View.VISIBLE);

        txt_1stAnsOpt = (TextView) findViewById(R.id.txt_1stAnsOpt);
        txt_2ndAnsOpt = (TextView) findViewById(R.id.txt_2ndAnsOpt);
        txt_3rdAnsOpt = (TextView) findViewById(R.id.txt_3rdAnsOpt);
        txt_4thAnsOpt = (TextView) findViewById(R.id.txt_4thAnsOpt);
        txt_5thAnsOpt = (TextView) findViewById(R.id.txt_5thAnsOpt);

        txt_hint = (TextView) findViewById(R.id.txt_hint);
        txt_hint.setText("Please click on options to select an answer");

//        img_exmType1Icon = (ImageView) findViewById(R.id.img_exmType1Icon);
//        img_exmType4Icon = (ImageView) findViewById(R.id.img_exmType4Icon);

        txt_progress = (TextView) findViewById(R.id.txt_progress);
        Exm_progressBar = (ProgressBar) findViewById(R.id.Exm_progressBar);
        setDisableClickableMQCOption(false);
//        lnr_psycho5thOpt = (LinearLayout) findViewById(R.id.lnr_psycho5thOpt);


        txt_A = (TextView) findViewById(R.id.txt_A);
        txt_B = (TextView) findViewById(R.id.txt_B);
        txt_C = (TextView) findViewById(R.id.txt_C);
        txt_D = (TextView) findViewById(R.id.txt_D);
        txt_E = (TextView) findViewById(R.id.txt_E);

        rel_WithNav = (RelativeLayout) findViewById(R.id.rel_WithNav);
        rel_NoNav = (RelativeLayout) findViewById(R.id.rel_NoNav);


        btn_1stAnsOpt.setOnClickListener(this);
        btn_2ndAnsOpt.setOnClickListener(this);
        btn_3rdAnsOpt.setOnClickListener(this);
        btn_4thAnsOpt.setOnClickListener(this);
        btn_psycho5thOpt.setOnClickListener(this);

//        txt_A.setOnClickListener(this);
//        txt_B.setOnClickListener(this);
//        txt_C.setOnClickListener(this);
//        txt_D.setOnClickListener(this);
//        txt_E.setOnClickListener(this);

        btn_Next.setOnClickListener(this);
        btn_Submit.setOnClickListener(this);
        btn_allowPrevious.setOnClickListener(this);
        btn_allowNext.setOnClickListener(this);

        str_SelectedAnswerText = "";
        str_QuestionAttempted_Flag = "N";
//        str_CrntQuestion = "";
        str_SelectedAnswerId = "";
        str_SelectedAnswerSequence = "";

//        QueSrNo = 1;

        activity_mcq_question.setVisibility(View.VISIBLE);

//        if (StaticVariables.crnt_ExamType == 1)
//            img_exmType1Icon.setVisibility(View.INVISIBLE);
//        else if (StaticVariables.crnt_ExamType == 4 || StaticVariables.crnt_ExamType == 5)
//            img_exmType1Icon.setVisibility(View.INVISIBLE);
//        else
//            img_exmType1Icon.setVisibility(View.GONE);
//
//        if (StaticVariables.crnt_ExamType == 4)
//            img_exmType4Icon.setVisibility(View.GONE);
//        else
//            img_exmType4Icon.setVisibility(View.GONE);

        initExam();
    }

    public void initExam() {
//        DatabaseHelper.db.delete(getResources().getString(R.string.TBL_LPICandidateAttemptQueAns), "LoginId=? and ExamId=?",
//                new String[]{StaticVariables.UserLoginId, "" + StaticVariables.crnt_ExmId});

//        if (StaticVariables.crnt_ExmId == 4001 || StaticVariables.crnt_ExmId == 5001) {
//            DatabaseHelper.db.delete(getResources().getString(R.string.TBL_LPICandidateAttemptQueAns), "LoginId=? and ExamId=?",
//                    new String[]{StaticVariables.UserLoginId, "" + StaticVariables.crnt_ExmId});
//        }

        if (StaticVariables.crnt_QueId == 0) {
            StaticVariables.crnt_QueId = DatabaseHelper.GetTopQuestionId(MCQ_QuestionActivity.this, DatabaseHelper.db,
                    "" + StaticVariables.crnt_ExmId);
        }
        QueSrNo = 1;

        if (DatabaseHelper.TotalOptionCount(MCQ_QuestionActivity.this, DatabaseHelper.db, StaticVariables.crnt_ExmId) == 5) {
            btn_psycho5thOpt.setVisibility(View.VISIBLE);
//            btn_psycho5thOpt.setVisibility(View.VISIBLE);
        } else {
            btn_psycho5thOpt.setVisibility(View.GONE);
//            btn_psycho5thOpt.setVisibility(View.GONE);
        }

        setQueAndAnsOpt(StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId);

        Exm_progressBar.setMax(StaticVariables.TblNameForCount);
        setExmProgress(QueSrNo);

        if (StaticVariables.crnt_ExamType == 4)
            textBtn_ReviewLater.setVisibility(View.VISIBLE);
        else
            textBtn_ReviewLater.setVisibility(View.GONE);

        rel_WithNav.setVisibility(View.GONE);
        rel_NoNav.setVisibility(View.GONE);

        if (DatabaseHelper.isAllowNav(MCQ_QuestionActivity.this, DatabaseHelper.db, StaticVariables.crnt_ExmId)) {
            isAllowNav = true;
            rel_WithNav.setVisibility(View.VISIBLE);
        } else {
            isAllowNav = false;
            rel_NoNav.setVisibility(View.VISIBLE);
        }


        if (QueSrNo == 1) {
            btn_allowPrevious.setVisibility(View.GONE);
        } else {
            btn_allowPrevious.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        if (QueSrNo <= StaticVariables.TblNameForCount) {

            custom_SubmitTestAlert("Leave Exam", "Are you sure, you want to leave exam?");

//            AlertDialog.Builder builder = CommonClass.DialogOpen(MCQ_QuestionActivity.this,
//                    "Leave Exam!", "Are you sure, you want to leave exam?");
//
//            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    Intent i = new Intent(getApplicationContext(), ParticipantMenuActivity.class);
//                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(i);
//                    finish();
//                }
//            });
//            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.cancel();
//                }
//            });
//            AlertDialog alert = builder.create();
//            alert.show();
        } else {
            GoToPreviousCall();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    void setQueAndAnsOpt(BigInteger ExmId, int QueId) {
        try {
            Cursor cquery_que, cquery_ans;
            cquery_que = DatabaseHelper.GetQue(MCQ_QuestionActivity.this, DatabaseHelper.db, ExmId, QueId);
            if (cquery_que.getCount() > 0) {
                for (cquery_que.moveToFirst(); !cquery_que.isAfterLast(); cquery_que.moveToNext()) {
                    if (QueSrNo < 10 && QueSrNo > 0)
                        txt_SrNo_MCQue.setText("Q. 0" + QueSrNo);
                    else
                        txt_SrNo_MCQue.setText("Q. " + QueSrNo);

                    txt_MCQue.setText(cquery_que.getString(3));
                    //                str_CrntQuestion = cquery_que.getString(3);
                    StaticVariables.crnt_ExmId = ExmId;
                    StaticVariables.crnt_QueId = QueId;
                }

                SetAnsForExamType1("" + ExmId, "" + QueId);

//                if (StaticVariables.crnt_ExamType == 5 || StaticVariables.crnt_ExamType == 4) {
//                    cquery_ans = DatabaseHelper.GetFeedBackAnsOptions(MCQ_QuestionActivity.this, DatabaseHelper.db);
//                    int i = 0;
//                    for (cquery_ans.moveToFirst(); !cquery_ans.isAfterLast(); cquery_ans.moveToNext()) {
//                        //                    str_SelectedAnswer=""+cquery_ans.getString(0);
//                        ShowAlert_Flag = false;
//                        if (i == 0)
//                            txt_1stAnsOpt.setText(cquery_ans.getString(0));
//                        else if (i == 1)
//                            txt_2ndAnsOpt.setText(cquery_ans.getString(0));
//                        else if (i == 2)
//                            txt_3rdAnsOpt.setText(cquery_ans.getString(0));
//                        else if (i == 3)
//                            txt_4thAnsOpt.setText(cquery_ans.getString(0));
//                        else if (i == 4)
//                            txt_5thAnsOpt.setText(cquery_ans.getString(0));
//
//                        i++;
//                    }
//                    cquery_ans.close();
//                } else {
//                    SetAnsForExamType1("" + ExmId, "" + QueId);
//                }
                //StaticVariables.UserType.equalsIgnoreCase("C") &&
                if (StaticVariables.crnt_ExamType == 6 &&
                        StaticVariables.isSummary) {
                    SetEE_AttemptedQuetsAns("" + ExmId, "" + QueId);
                } else {
                    SetAttemptedQuetsAns("" + ExmId, "" + QueId);
                }
            } else {
                custom_SubmitTestAlert("Alert!", "Exam set not available, " +
                        "please make sure that you are connected to the internet to download exam set");
            }
            cquery_que.close();
        } catch (Exception e) {
            Log.e("", "setQueAndAnsOpt err=" + e.toString());
            DatabaseHelper.SaveErroLog(MCQ_QuestionActivity.this, DatabaseHelper.db,
                    "MCQ_QuestionActivity", "setQueAndAnsOpt()", e.toString());
        }
    }

    void SetAnsForExamType1(String ExmId, String QueId) {
        try {
            Cursor cquery_ans;
            String str_RightAns = "", str_SelectedAns = "", option = "";
            cquery_ans = DatabaseHelper.GetAnsOptions(MCQ_QuestionActivity.this, DatabaseHelper.db, ExmId, QueId);
            int i = 0;
            btn_psycho5thOpt.setVisibility(View.GONE);
            for (cquery_ans.moveToFirst(); !cquery_ans.isAfterLast(); cquery_ans.moveToNext()) {
                //                    str_SelectedAnswer=""+cquery_ans.getString(4);
                option = "" + cquery_ans.getString(4);
                ShowAlert_Flag = false;
                if (i == 0) {
                    txt_1stAnsOpt.setText(option);
                } else if (i == 1) {
                    txt_2ndAnsOpt.setText(option);
                    btn_3rdAnsOpt.setVisibility(View.GONE);
                    btn_4thAnsOpt.setVisibility(View.GONE);
                    btn_psycho5thOpt.setVisibility(View.GONE);
                } else if (i == 2) {
                    btn_3rdAnsOpt.setVisibility(View.VISIBLE);
                    txt_3rdAnsOpt.setText(option);
                    btn_4thAnsOpt.setVisibility(View.GONE);
                    //                if (StaticVariables.crnt_ExamType == 1)
                    //                    setRightAns_bckgrnd(StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId, btn_3rdAnsOpt);
                } else if (i == 3) {
                    txt_4thAnsOpt.setText(option);
                    btn_4thAnsOpt.setVisibility(View.VISIBLE);
                    //                if (StaticVariables.crnt_ExamType == 1)
                    //                    setRightAns_bckgrnd(StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId, btn_4thAnsOpt);
                } else if (i == 4) {
                    txt_5thAnsOpt.setText(option);
                    btn_psycho5thOpt.setVisibility(View.VISIBLE);
                }
                //int ansId = Integer.parseInt(cquery_ans.getString(2));
                i++;
            }
            if (StaticVariables.crnt_ExamType == 1) {
                str_RightAns = DatabaseHelper.GetRightAns_Exm1(MCQ_QuestionActivity.this, DatabaseHelper.db,
                        "" + ExmId, "" + QueId);
                setRightAns_bckgrnd(str_RightAns);
            }
            //StaticVariables.UserType.equalsIgnoreCase("C") &&
            if (StaticVariables.crnt_ExamType == 6 &&
                    StaticVariables.isSummary) {
                str_SelectedAns = DatabaseHelper.GetAttemptedAns(MCQ_QuestionActivity.this, DatabaseHelper.db,
                        "" + ExmId, "" + QueId, StaticVariables.UserLoginId, (attemptId + 1));
                if (!str_RightAns.trim().equalsIgnoreCase(str_SelectedAns.trim())) {
                    setSelectedAns_bckgrnd(str_SelectedAns);
                }
            }
            cquery_ans.close();
        } catch (Exception e) {
            Log.e("", "SetAnsForExamType1 err=" + e.toString());
            DatabaseHelper.SaveErroLog(MCQ_QuestionActivity.this, DatabaseHelper.db,
                    "MCQ_QuestionActivity", "SetAnsForExamType1()", e.toString());
        }

    }

//    void setRightAns_bckgrnd(int ExamId, int QuestionId, TextView TxtVw_Ans, TextView TxtVw_AnsOpt) {
//        String str_RightAns = DatabaseHelper.GetRightAns_Exm1(MCQ_QuestionActivity.this, DatabaseHelper.db, ExamId, QuestionId);
//        if (str_RightAns.equals(TxtVw_Ans.getText().toString())) {
//            //setTxt_bckgrnd(TxtVw_AnsOpt, true);
//        } else {
//            //setTxt_bckgrnd(TxtVw_AnsOpt, false);
//        }
//    }

//    void setRightAns_bckgrnd(int ExamId, int QuestionId, LinearLayout btn, TextView TxtVw_Ans) {
//        String str_RightAns = DatabaseHelper.GetRightAns_Exm1(MCQ_QuestionActivity.this, DatabaseHelper.db, ExamId, QuestionId);
//        if (str_RightAns.equals(TxtVw_Ans.getText().toString())) {
//            btn.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.selected_rectangle_right));
//            //setTxt_bckgrnd(TxtVw_AnsOpt,true);
//        } else {
//            btn.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.unselected_rectangle));
//            //setTxt_bckgrnd(TxtVw_AnsOpt,false);
//        }
//    }

    void setRightAns_bckgrnd(String str_RightAns) {

        if (txt_1stAnsOpt.getText().toString().equals(str_RightAns)) {
            setBtn_bckgrnd(btn_1stAnsOpt, true);
            setBtn_bckgrnd(btn_2ndAnsOpt, false);
            setBtn_bckgrnd(btn_3rdAnsOpt, false);
            setBtn_bckgrnd(btn_4thAnsOpt, false);
            setBtn_bckgrnd(btn_psycho5thOpt, false);
            str_SelectedAnswerSequence = "1";
        } else if (txt_2ndAnsOpt.getText().toString().equals(str_RightAns)) {
            setBtn_bckgrnd(btn_1stAnsOpt, false);
            setBtn_bckgrnd(btn_2ndAnsOpt, true);
            setBtn_bckgrnd(btn_3rdAnsOpt, false);
            setBtn_bckgrnd(btn_4thAnsOpt, false);
            setBtn_bckgrnd(btn_psycho5thOpt, false);
            str_SelectedAnswerSequence = "2";
        } else if (txt_3rdAnsOpt.getText().toString().equals(str_RightAns)) {
            setBtn_bckgrnd(btn_1stAnsOpt, false);
            setBtn_bckgrnd(btn_2ndAnsOpt, false);
            setBtn_bckgrnd(btn_3rdAnsOpt, true);
            setBtn_bckgrnd(btn_4thAnsOpt, false);
            setBtn_bckgrnd(btn_psycho5thOpt, false);
            str_SelectedAnswerSequence = "3";
        } else if (txt_4thAnsOpt.getText().toString().equals(str_RightAns)) {
            setBtn_bckgrnd(btn_1stAnsOpt, false);
            setBtn_bckgrnd(btn_2ndAnsOpt, false);
            setBtn_bckgrnd(btn_3rdAnsOpt, false);
            setBtn_bckgrnd(btn_4thAnsOpt, true);
            setBtn_bckgrnd(btn_psycho5thOpt, false);
            str_SelectedAnswerSequence = "4";
        } else {
            setBtn_bckgrnd(btn_1stAnsOpt, false);
            setBtn_bckgrnd(btn_2ndAnsOpt, false);
            setBtn_bckgrnd(btn_3rdAnsOpt, false);
            setBtn_bckgrnd(btn_4thAnsOpt, false);
            setBtn_bckgrnd(btn_psycho5thOpt, true);
            str_SelectedAnswerSequence = "5";
        }

    }

    void setEE_RightAns_bckgrnd(String str_RightAns) {

        if (txt_1stAnsOpt.getText().toString().equals(str_RightAns)) {
            setBtn_bckgrnd(btn_1stAnsOpt, true);
        } else if (txt_2ndAnsOpt.getText().toString().equals(str_RightAns)) {
            setBtn_bckgrnd(btn_2ndAnsOpt, true);
        } else if (txt_3rdAnsOpt.getText().toString().equals(str_RightAns)) {
            setBtn_bckgrnd(btn_3rdAnsOpt, true);
        } else if (txt_4thAnsOpt.getText().toString().equals(str_RightAns)) {
            setBtn_bckgrnd(btn_4thAnsOpt, true);
        } else {
            setBtn_bckgrnd(btn_psycho5thOpt, true);
        }
    }

    void setSelectedAns_bckgrnd(String str_SelectedAns) {
        if (txt_1stAnsOpt.getText().toString().equals(str_SelectedAns)) {
            setBtn_WrongAnsBckgrnd(btn_1stAnsOpt, true);
            str_SelectedAnswerSequence = "1";
        } else if (txt_2ndAnsOpt.getText().toString().equals(str_SelectedAns)) {
            setBtn_WrongAnsBckgrnd(btn_2ndAnsOpt, true);
            str_SelectedAnswerSequence = "2";
        } else if (txt_3rdAnsOpt.getText().toString().equals(str_SelectedAns)) {
            setBtn_WrongAnsBckgrnd(btn_3rdAnsOpt, true);
            str_SelectedAnswerSequence = "3";
        } else if (txt_4thAnsOpt.getText().toString().equals(str_SelectedAns)) {
            setBtn_WrongAnsBckgrnd(btn_4thAnsOpt, true);
            str_SelectedAnswerSequence = "4";
        } else if (txt_5thAnsOpt.getText().toString().equals(str_SelectedAns)) {
            setBtn_WrongAnsBckgrnd(btn_psycho5thOpt, true);
            str_SelectedAnswerSequence = "5";
        }
    }

    void setBtn_bckgrnd(LinearLayout btn, boolean isSelected) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (isSelected)
                btn.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ripple_effect_button_selected));
            else
                btn.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ripple_effect_button));
        } else {
            if (isSelected)
                btn.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.selector_button_selected));
            else
                btn.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.selector_button));
        }
    }

    void setBtn_WrongAnsBckgrnd(LinearLayout btn, boolean isSelected) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (isSelected)
                btn.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ripple_effect_button_wrong_selected));
        } else {
            if (isSelected)
                btn.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.selected_wrong_ans_rectangle));
        }
        /*else{
            btn.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.unselected_rectangle));
            }*/
    }

//    void setTxt_bckgrnd(TextView txt, boolean isSelected) {
//        if (isSelected) {
//            txt.setTextColor(getApplicationContext().getResources().getColor(R.color.white));
//            txt.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.draw_filledcircle));
//        } else {
//            txt.setTextColor(getApplicationContext().getResources().getColor(R.color.balck));
//            txt.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.draw_circle));
//        }
//    }

    void ResetBtn_bckgrnd() {
        setBtn_bckgrnd(btn_1stAnsOpt, false);
        setBtn_bckgrnd(btn_2ndAnsOpt, false);
        setBtn_bckgrnd(btn_3rdAnsOpt, false);
        setBtn_bckgrnd(btn_4thAnsOpt, false);
        setBtn_bckgrnd(btn_psycho5thOpt, false);

//        setTxt_bckgrnd(txt_A, false);
//        setTxt_bckgrnd(txt_B, false);
//        setTxt_bckgrnd(txt_C, false);
//        setTxt_bckgrnd(txt_D, false);
//        setTxt_bckgrnd(txt_E, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_1stAnsOpt:
                if (StaticVariables.crnt_ExamType == 2) {
                    setBtn_bckgrnd(btn_1stAnsOpt, true);
                    setBtn_bckgrnd(btn_2ndAnsOpt, false);
                    setBtn_bckgrnd(btn_3rdAnsOpt, false);
                    setBtn_bckgrnd(btn_4thAnsOpt, false);
                    setBtn_bckgrnd(btn_psycho5thOpt, false);

//                    setTxt_bckgrnd(txt_A, true);
//                    setTxt_bckgrnd(txt_B, false);
//                    setTxt_bckgrnd(txt_C, false);
//                    setTxt_bckgrnd(txt_D, false);
//                    setTxt_bckgrnd(txt_E, false);
                    ShowAlert_Flag = true;
                } else if (StaticVariables.crnt_ExamType >= 3) {
                    setBtn_bckgrnd(btn_1stAnsOpt, true);
                    setBtn_bckgrnd(btn_2ndAnsOpt, false);
                    setBtn_bckgrnd(btn_3rdAnsOpt, false);
                    setBtn_bckgrnd(btn_4thAnsOpt, false);
                    setBtn_bckgrnd(btn_psycho5thOpt, false);

//                    setTxt_bckgrnd(txt_A, true);
//                    setTxt_bckgrnd(txt_B, false);
//                    setTxt_bckgrnd(txt_C, false);
//                    setTxt_bckgrnd(txt_D, false);
//                    setTxt_bckgrnd(txt_E, false);
                }
                str_SelectedAnswer = txt_1stAnsOpt.getText().toString();
                str_SelectedAnswerText = txt_1stAnsOpt.getText().toString();
                str_SelectedAnswerId = "" + DatabaseHelper.GetAnsId(MCQ_QuestionActivity.this, DatabaseHelper.db,
                        str_SelectedAnswerText, StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId);
                str_QuestionAttempted_Flag = "Y";
                str_SelectedAnswerSequence = "1";
                break;
            case R.id.btn_2ndAnsOpt:
                if (StaticVariables.crnt_ExamType == 2) {
                    setBtn_bckgrnd(btn_1stAnsOpt, false);
                    setBtn_bckgrnd(btn_2ndAnsOpt, true);
                    setBtn_bckgrnd(btn_3rdAnsOpt, false);
                    setBtn_bckgrnd(btn_4thAnsOpt, false);
                    setBtn_bckgrnd(btn_psycho5thOpt, false);
//                    setTxt_bckgrnd(txt_A, false);
//                    setTxt_bckgrnd(txt_B, true);
//                    setTxt_bckgrnd(txt_C, false);
//                    setTxt_bckgrnd(txt_D, false);
//                    setTxt_bckgrnd(txt_E, false);

                    ShowAlert_Flag = true;
                } else if (StaticVariables.crnt_ExamType >= 3) {
                    setBtn_bckgrnd(btn_1stAnsOpt, false);
                    setBtn_bckgrnd(btn_2ndAnsOpt, true);
                    setBtn_bckgrnd(btn_3rdAnsOpt, false);
                    setBtn_bckgrnd(btn_4thAnsOpt, false);
                    setBtn_bckgrnd(btn_psycho5thOpt, false);

                    //setTxt_bckgrnd(txt_A, false);
                    //setTxt_bckgrnd(txt_B, true);
                    //setTxt_bckgrnd(txt_C, false);
                    //setTxt_bckgrnd(txt_D, false);
                    //setTxt_bckgrnd(txt_E, false);
                }
                str_SelectedAnswer = txt_2ndAnsOpt.getText().toString();
                str_SelectedAnswerText = txt_2ndAnsOpt.getText().toString();
                str_SelectedAnswerId = "" + DatabaseHelper.GetAnsId(MCQ_QuestionActivity.this, DatabaseHelper.db,
                        str_SelectedAnswerText, StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId);
                str_QuestionAttempted_Flag = "Y";
                str_SelectedAnswerSequence = "2";
                break;
            case R.id.btn_3rdAnsOpt:
                if (StaticVariables.crnt_ExamType == 2) {
                    setBtn_bckgrnd(btn_1stAnsOpt, false);
                    setBtn_bckgrnd(btn_2ndAnsOpt, false);
                    setBtn_bckgrnd(btn_3rdAnsOpt, true);
                    setBtn_bckgrnd(btn_4thAnsOpt, false);
                    setBtn_bckgrnd(btn_psycho5thOpt, false);

                    //setTxt_bckgrnd(txt_A, false);
                    //setTxt_bckgrnd(txt_B, false);
                    //setTxt_bckgrnd(txt_C, true);
                    //setTxt_bckgrnd(txt_D, false);
                    //setTxt_bckgrnd(txt_E, false);

                    ShowAlert_Flag = true;
                } else if (StaticVariables.crnt_ExamType >= 3) {
                    setBtn_bckgrnd(btn_1stAnsOpt, false);
                    setBtn_bckgrnd(btn_2ndAnsOpt, false);
                    setBtn_bckgrnd(btn_3rdAnsOpt, true);
                    setBtn_bckgrnd(btn_4thAnsOpt, false);
                    setBtn_bckgrnd(btn_psycho5thOpt, false);

                    //setTxt_bckgrnd(txt_A, false);
                    //setTxt_bckgrnd(txt_B, false);
                    //setTxt_bckgrnd(txt_C, true);
                    //setTxt_bckgrnd(txt_D, false);
                    //setTxt_bckgrnd(txt_E, false);
                }

                str_SelectedAnswer = txt_3rdAnsOpt.getText().toString();
                str_SelectedAnswerText = txt_3rdAnsOpt.getText().toString();
                str_SelectedAnswerId = "" + DatabaseHelper.GetAnsId(MCQ_QuestionActivity.this, DatabaseHelper.db,
                        str_SelectedAnswerText, StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId);
                str_QuestionAttempted_Flag = "Y";
                str_SelectedAnswerSequence = "3";
                break;
            case R.id.btn_4thAnsOpt:
                if (StaticVariables.crnt_ExamType == 2) {
                    setBtn_bckgrnd(btn_1stAnsOpt, false);
                    setBtn_bckgrnd(btn_2ndAnsOpt, false);
                    setBtn_bckgrnd(btn_3rdAnsOpt, false);
                    setBtn_bckgrnd(btn_4thAnsOpt, true);
                    setBtn_bckgrnd(btn_psycho5thOpt, false);

                    //setTxt_bckgrnd(txt_A, false);
                    //setTxt_bckgrnd(txt_B, false);
                    //setTxt_bckgrnd(txt_C, false);
                    //setTxt_bckgrnd(txt_D, true);
                    //setTxt_bckgrnd(txt_E, false);

                    ShowAlert_Flag = true;
                } else if (StaticVariables.crnt_ExamType >= 3) {
                    setBtn_bckgrnd(btn_1stAnsOpt, false);
                    setBtn_bckgrnd(btn_2ndAnsOpt, false);
                    setBtn_bckgrnd(btn_3rdAnsOpt, false);
                    setBtn_bckgrnd(btn_4thAnsOpt, true);
                    setBtn_bckgrnd(btn_psycho5thOpt, false);

                    //setTxt_bckgrnd(txt_A, false);
                    //setTxt_bckgrnd(txt_B, false);
                    //setTxt_bckgrnd(txt_C, false);
                    //setTxt_bckgrnd(txt_D, true);
                    //setTxt_bckgrnd(txt_E, false);
                }
                str_SelectedAnswer = txt_4thAnsOpt.getText().toString();
                str_SelectedAnswerText = txt_4thAnsOpt.getText().toString();
                str_SelectedAnswerId = "" + DatabaseHelper.GetAnsId(MCQ_QuestionActivity.this, DatabaseHelper.db,
                        str_SelectedAnswerText, StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId);
                str_QuestionAttempted_Flag = "Y";
                str_SelectedAnswerSequence = "4";


                break;
            case R.id.btn_psycho5thOpt:
                if (StaticVariables.crnt_ExamType == 4 || StaticVariables.crnt_ExamType == 5 || StaticVariables.crnt_ExamType == 6) {
                    setBtn_bckgrnd(btn_1stAnsOpt, false);
                    setBtn_bckgrnd(btn_2ndAnsOpt, false);
                    setBtn_bckgrnd(btn_3rdAnsOpt, false);
                    setBtn_bckgrnd(btn_4thAnsOpt, false);
                    setBtn_bckgrnd(btn_psycho5thOpt, true);

                    //setTxt_bckgrnd(txt_A, false);
                    //setTxt_bckgrnd(txt_B, false);
                    //setTxt_bckgrnd(txt_C, false);
                    //setTxt_bckgrnd(txt_D, false);
                    //setTxt_bckgrnd(txt_E, true);
                }
                str_SelectedAnswer = txt_5thAnsOpt.getText().toString();
                str_SelectedAnswerText = txt_5thAnsOpt.getText().toString();
                str_SelectedAnswerId = "" + DatabaseHelper.GetAnsId(MCQ_QuestionActivity.this, DatabaseHelper.db,
                        str_SelectedAnswerText, StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId);
                str_QuestionAttempted_Flag = "Y";
                str_SelectedAnswerSequence = "5";

                break;
            case R.id.btn_Next:
                if (btn_Next.getText().toString().equals("SUBMIT")) {
                    if (DatabaseHelper.IsExmQueMendatory(MCQ_QuestionActivity.this, DatabaseHelper.db, "" + StaticVariables.crnt_ExmId)) {
                        if (str_SelectedAnswerSequence.equals("")) {
                            Toast.makeText(getApplicationContext(), "All questions are mandatory", Toast.LENGTH_SHORT).show();
                        } else {
                            alertCustomDialog("Submit Exam", "Do you want to submit " + StaticVariables.crnt_ExmName + " exam?");
                        }
                    }
                } else {
                    if (ShowAlert_Flag)
                        custom_ResultRemrk();
                    else
                        moveToNextQue();
                }
                break;
            case R.id.btn_allowNext:
                if (ShowAlert_Flag)
                    custom_ResultRemrk();
                else
                    moveToNextQue();
                break;
            case R.id.btn_allowPrevious:
                moveToPrivQue();
                break;
            case R.id.btn_Submit:
                //StaticVariables.UserType.equalsIgnoreCase("C") &&
                if (StaticVariables.crnt_ExamType == 6 &&
                        StaticVariables.isSummary) {
                    StaticVariables.isSummary = false;
                    /*Intent intent = new Intent(MCQ_QuestionActivity.this, LPILessonQuizActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    MCQ_QuestionActivity.this.finish();*/
                    GoToPreviousCall();
                } else if (str_SelectedAnswerSequence.equals("")) {
                    Toast.makeText(getApplicationContext(), "All questions are mandatory", Toast.LENGTH_SHORT).show();
                } else {
                    alertCustomDialog("Submit Exam", "Do you want to submit " + StaticVariables.crnt_ExmName + " exam?");
                }
                break;
            case R.id.textBtn_ReviewLater:
                break;
        }
    }

    public void submitExam() {
        //isSubmit = true;
        DatabaseHelper.SaveAttemptedQueAns(MCQ_QuestionActivity.this, DatabaseHelper.db, "" + StaticVariables.UserLoginId,
                "" + StaticVariables.crnt_ExmId, StaticVariables.crnt_ExmName, "" + StaticVariables.crnt_QueId,
                str_QuestionAttempted_Flag, str_SelectedAnswerId, str_SelectedAnswerSequence, str_SelectedAnswer, "" + (attemptId + 1));

        DatabaseHelper.SaveAttemptedExam(MCQ_QuestionActivity.this, DatabaseHelper.db,
                "" + StaticVariables.crnt_ExmId, StaticVariables.crnt_ExmName, "N",
                "" + (attemptId + 1), StaticVariables.UserLoginId);

        if (StaticVariables.crnt_ExamType == 2 && ShowAlert_Flag) {
            custom_ResultRemrk();
        } else if (StaticVariables.crnt_ExamType == 3) {
            custom_SubmitTestAlert("Submit Exam", "This will submit exam.\nAre you sure?");
        } else if (StaticVariables.crnt_ExamType == 4) {
            if (QueSrNo == StaticVariables.TblNameForCount && str_SelectedAnswerSequence.equals("")) {
                Toast.makeText(MCQ_QuestionActivity.this, "Please select answer first.", Toast.LENGTH_SHORT).show();
            } else if (CommonClass.isConnected(MCQ_QuestionActivity.this)) {
                new SubmitExam(0, MCQ_QuestionActivity.this).execute();
            } else {
                DatabaseHelper.SaveXMLData(MCQ_QuestionActivity.this, DatabaseHelper.db,
                        CommonClass.SubmitExamXML(MCQ_QuestionActivity.this,
                                StaticVariables.UserLoginId, "" + StaticVariables.crnt_ExmId),
                        MCQ_QuestionActivity.this.getString(R.string.SubmitExam));
                custom_SubmitTestAlert("No internet",
                        "" + StaticVariables.crnt_ExmName + " exam saved successfully,Thank you...");
            }
        } else if (StaticVariables.crnt_ExamType == 5) {
            if (CommonClass.isConnected(MCQ_QuestionActivity.this)) {
                new SubmitExamResultFdback(new Callback() {
                    @Override
                    public void execute(Object result, String status) {
                        processFeedback((InputStream) result, status);
                    }
                }, MCQ_QuestionActivity.this, "" + StaticVariables.crnt_ExmId).execute();
            } else {
                DatabaseHelper.SaveXMLData(MCQ_QuestionActivity.this, DatabaseHelper.db,
                        CommonClass.SubmitExamFeedXML(MCQ_QuestionActivity.this,
                                StaticVariables.UserLoginId, "" + StaticVariables.crnt_ExmId),
                        MCQ_QuestionActivity.this.getString(R.string.SubmitFeedBackforResultOfExam));
                custom_SubmitTestAlert("No internet",
                        "Feedback saved successfully,Thank you...");
            }
        } else if (StaticVariables.crnt_ExamType == 6) {
            if (QueSrNo == StaticVariables.TblNameForCount && str_SelectedAnswerSequence.equals("")) {
                Toast.makeText(MCQ_QuestionActivity.this, "Please select answer first.", Toast.LENGTH_SHORT).show();
            } else if (CommonClass.isConnected(MCQ_QuestionActivity.this)) {
                new SubmitExam(0, MCQ_QuestionActivity.this).execute();
            } else {
                DatabaseHelper.SaveXMLData(MCQ_QuestionActivity.this, DatabaseHelper.db,
                        CommonClass.SubmitExamXML(MCQ_QuestionActivity.this,
                                StaticVariables.UserLoginId, "" + StaticVariables.crnt_ExmId),
                        MCQ_QuestionActivity.this.getString(R.string.SubmitExam));
                custom_SubmitTestAlert("No internet",
                        "" + StaticVariables.crnt_ExmName + " exam saved successfully,Thank you...");
            }
        } else {
            Toast.makeText(MCQ_QuestionActivity.this, "Practice Exam submitted successfully ...", Toast.LENGTH_LONG).show();
            Intent i = new Intent(MCQ_QuestionActivity.this, ExamList_Activity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            MCQ_QuestionActivity.this.finish();
        }
    }

    private void processFeedback(InputStream inputStream, String ResponseCode) {
        try {
            showProgress(false, "");
            if (ResponseCode.equals("0")) {
                if (StaticVariables.crnt_ExamType == 5) {
                    deleteAttemptDetails();
//                        DatabaseHelper.db.delete(getResources().getString(R.string.TBL_LPICandidateAttemptQueAns), "LoginId=? and ExamId=?",
//                                new String[]{StaticVariables.UserLoginId, "" + StaticVariables.crnt_ExmId});
                    custom_SubmitTestAlert("Feedback", "Thank you for giving the feedback of your psychometric traits...");
                    DatabaseHelper.InsertActivityTracker(MCQ_QuestionActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                            "A10401", "A104", "Feedback Given", "", "",
                            "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "", StaticVariables.UserName, "",
                            "", "", "Pending");
                }
            } else if (ResponseCode.equals("2")) {
                if (StaticVariables.crnt_ExamType == 5)
                    custom_SubmitTestAlert("Feedback",
                            "Feedback data saved successfully.\nThank you for giving the feedback of your psychometric traits...");
                else
                    custom_SubmitTestAlert(StaticVariables.crnt_ExmName,
                            "" + StaticVariables.crnt_ExmName + " exam feedback submitted successfully, Thank you...");

                DatabaseHelper.UpdateAttemptedExam(MCQ_QuestionActivity.this, DatabaseHelper.db,
                        "" + StaticVariables.crnt_ExmId, StaticVariables.crnt_ExmName, "N",
                        "" + (attemptId + 1), StaticVariables.UserLoginId);
            } else {
//                    DatabaseHelper.SaveAttemptedExam(MCQ_QuestionActivity.this, DatabaseHelper.db,
//                            "" + StaticVariables.crnt_ExmId, StaticVariables.crnt_ExmName, "Y", "");
                DatabaseHelper.UpdateAttemptedExam(MCQ_QuestionActivity.this, DatabaseHelper.db,
                        "" + StaticVariables.crnt_ExmId, StaticVariables.crnt_ExmName, "N",
                        "" + (attemptId + 1), StaticVariables.UserLoginId);
                custom_SubmitTestAlert("Server not responding!",
                        "Something went wrong. Please try again..");
            }
        } catch (Exception e) {
//                DatabaseHelper.SaveAttemptedExam(MCQ_QuestionActivity.this, DatabaseHelper.db,
//                        "" + StaticVariables.crnt_ExmId, StaticVariables.crnt_ExmName, "Y", "");
            DatabaseHelper.UpdateAttemptedExam(MCQ_QuestionActivity.this, DatabaseHelper.db,
                    "" + StaticVariables.crnt_ExmId, StaticVariables.crnt_ExmName, "N",
                    "" + (attemptId + 1), StaticVariables.UserLoginId);
            DatabaseHelper.SaveErroLog(MCQ_QuestionActivity.this, DatabaseHelper.db,
                    "MCQ_QuestionActivity", "SubmitExamResultFdback.onPostExecute()", e.toString());
            custom_SubmitTestAlert("Server not responding!",
                    "Something went wrong. Please try again..");
        }
    }

    public String highligtCrrectAns_Exm(BigInteger ExamId, int QuestionId, String SelectedAnswer) {
        String returnStr;
        String str_CrrectAns = "" + DatabaseHelper.GetRightAns_Exm2(MCQ_QuestionActivity.this, DatabaseHelper.db, ExamId,
                QuestionId, SelectedAnswer);
        if (str_CrrectAns.equals("")) {
            returnStr = "Wrong";
        } else {
            returnStr = "Correct";
        }
        return returnStr;
    }

    void moveToNextQue() {
        try {

            if (DatabaseHelper.IsExmQueMendatory(MCQ_QuestionActivity.this, DatabaseHelper.db, "" + StaticVariables.crnt_ExmId)) {
                if (str_SelectedAnswerSequence.equals(""))
                    Toast.makeText(getApplicationContext(), "All questions are mandatory", Toast.LENGTH_SHORT).show();
                else
                    NextQuestion();
            } else {
                NextQuestion();
            }


        } catch (Exception e) {
            Log.e("", "Error at fetching que=" + e.toString());
            DatabaseHelper.SaveErroLog(MCQ_QuestionActivity.this, DatabaseHelper.db,
                    "MCQ_QuestionActivity", "moveToNextQue", e.toString());
        }

    }

    void NextQuestion() {
        try {
            if (StaticVariables.crnt_ExamType >= 3) {
                //            if (StaticVariables.crnt_ExamType == 5)
                //                DatabaseHelper.SaveAttemptedQueAns(MCQ_QuestionActivity.this, DatabaseHelper.db, "" + StaticVariables.UserLoginId,
                //                        "" + StaticVariables.crnt_ExmId, StaticVariables.crnt_ExmName, "" + StaticVariables.crnt_QueId,
                //                        str_CrntQuestion, str_SelectedAnswerId, str_SelectedAnswerSequence, str_SelectedAnswer);
                //            else
                DatabaseHelper.SaveAttemptedQueAns(MCQ_QuestionActivity.this, DatabaseHelper.db, "" + StaticVariables.UserLoginId,
                        "" + StaticVariables.crnt_ExmId, StaticVariables.crnt_ExmName, "" + StaticVariables.crnt_QueId,
                        str_QuestionAttempted_Flag, str_SelectedAnswerId, str_SelectedAnswerSequence, str_SelectedAnswer, "" + (attemptId + 1));
            }
            StaticVariables.crnt_QueId = DatabaseHelper.GetNextQuestionId(MCQ_QuestionActivity.this, DatabaseHelper.db,
                    "" + StaticVariables.crnt_ExmId, "" + StaticVariables.crnt_QueId);
            Cursor cquery;
//        cquery = DatabaseHelper.GetQue(MCQ_QuestionActivity.this, DatabaseHelper.db, StaticVariables.crnt_ExmId,
//                StaticVariables.crnt_QueId + 1);
            cquery = DatabaseHelper.GetQue(MCQ_QuestionActivity.this, DatabaseHelper.db, StaticVariables.crnt_ExmId,
                    StaticVariables.crnt_QueId);
            if (cquery.getCount() > 0) {
                if (StaticVariables.crnt_ExamType >= 2)
                    ResetBtn_bckgrnd();

                ++QueSrNo;
                //            setQueAndAnsOpt(StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId + 1);
                setQueAndAnsOpt(StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId);
                // Set Progress of exam attend
                setExmProgress(QueSrNo);
            } else {
                if (isAllowNav) {
                    btn_allowNext.setVisibility(View.GONE);
                    btn_Submit.setVisibility(View.VISIBLE);
                    btn_Submit.setText("Submit");
                    //StaticVariables.UserType.equalsIgnoreCase("C") &&
                    if (StaticVariables.crnt_ExamType == 6 &&
                            StaticVariables.isSummary) {
                        setEE_SubmitButton();
                    }
                } else
                    btn_Next.setText("SUBMIT");
            }
            if (QueSrNo == 1) {
                btn_allowPrevious.setVisibility(View.GONE);
            } else {
                btn_allowPrevious.setVisibility(View.VISIBLE);
            }
            if (QueSrNo == StaticVariables.TblNameForCount) {
                if (StaticVariables.crnt_ExamType == 5) {
                    btn_Next.setText("SUBMIT");
                }
                btn_allowNext.setVisibility(View.GONE);
                btn_Submit.setVisibility(View.VISIBLE);
                btn_Submit.setText("Submit");
//                StaticVariables.UserType.equalsIgnoreCase("C") &&
                if (StaticVariables.crnt_ExamType == 6 &&
                        StaticVariables.isSummary) {
                    setEE_SubmitButton();
                }
            } else {
                btn_allowNext.setVisibility(View.VISIBLE);
                btn_Submit.setVisibility(View.GONE);
            }
            txt_ExamType.setText("" + StaticVariables.crnt_ExmTypeName);
            cquery.close();
        } catch (Exception e) {
            Log.e("", "NextQuestion err=" + e.toString());
            DatabaseHelper.SaveErroLog(MCQ_QuestionActivity.this, DatabaseHelper.db,
                    "MCQ_QuestionActivity", "NextQuestion()", e.toString());
        }
    }

    void moveToPrivQue() {
        try {
            if (QueSrNo == 1) {
                btn_allowPrevious.setVisibility(View.GONE);
            } else {
                btn_allowPrevious.setVisibility(View.VISIBLE);
            }

            if (btn_Submit.getVisibility() == View.VISIBLE) {
                btn_allowNext.setVisibility(View.VISIBLE);
                btn_Submit.setVisibility(View.GONE);
                StaticVariables.crnt_QueId = DatabaseHelper.GetLastQuestionId(MCQ_QuestionActivity.this, DatabaseHelper.db,
                        "" + StaticVariables.crnt_ExmId);
            } else if (btn_allowNext.getVisibility() == View.GONE) {
                btn_allowNext.setVisibility(View.VISIBLE);
                btn_Submit.setVisibility(View.GONE);
            }

            StaticVariables.crnt_QueId = DatabaseHelper.GetPrevQuestionId(MCQ_QuestionActivity.this, DatabaseHelper.db,
                    "" + StaticVariables.crnt_ExmId, "" + StaticVariables.crnt_QueId);

            Cursor cquery;
//            cquery = DatabaseHelper.GetQue(MCQ_QuestionActivity.this, DatabaseHelper.db, StaticVariables.crnt_ExmId,
//                    StaticVariables.crnt_QueId - 1);
            cquery = DatabaseHelper.GetQue(MCQ_QuestionActivity.this, DatabaseHelper.db, StaticVariables.crnt_ExmId,
                    StaticVariables.crnt_QueId);
            if (cquery.getCount() > 0) {
                --QueSrNo;
//                setQueAndAnsOpt(StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId - 1);
                setQueAndAnsOpt(StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId);
                // Set Progress of exam attend
                setExmProgress(QueSrNo);
            } else if (StaticVariables.crnt_QueId == 0) {
                initExam();
            }
            txt_ExamType.setText("" + StaticVariables.crnt_ExmTypeName);
            cquery.close();

        } catch (Exception e) {
            Log.e("", "Error at fetching que=" + e.toString());
            DatabaseHelper.SaveErroLog(MCQ_QuestionActivity.this, DatabaseHelper.db,
                    "MCQ_QuestionActivity", "moveToPrivQue", e.toString());
        }

    }

    void setEE_SubmitButton() {
//        StaticVariables.UserType.equalsIgnoreCase("C") &&
        if (StaticVariables.crnt_ExamType == 6 &&
                StaticVariables.isSummary) {
            btn_Submit.setText("Close");
        }
    }

    void SetAttemptedQuetsAns(String ExmId, String QueId) {
        try {
            String AttemptedAnsSeq = "0";
            if (StaticVariables.crnt_ExamType != 1) {
                AttemptedAnsSeq = "" + DatabaseHelper.GetAtmptdAnsSeq(MCQ_QuestionActivity.this,
                        DatabaseHelper.db, StaticVariables.UserLoginId, ExmId, "" + QueId, "" + (attemptId + 1));
                ResetBtn_bckgrnd();
                if (AttemptedAnsSeq.equals("1")) {
                    setBtn_bckgrnd(btn_1stAnsOpt, true);
                    str_SelectedAnswerSequence = "1";
                    str_SelectedAnswerText = txt_1stAnsOpt.getText().toString();
                    str_QuestionAttempted_Flag = "Y";
                    str_SelectedAnswerId = "" + DatabaseHelper.GetAnsId(MCQ_QuestionActivity.this, DatabaseHelper.db,
                            str_SelectedAnswerText, StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId);
                    //setTxt_bckgrnd(txt_A, true);
                } else if (AttemptedAnsSeq.equals("2")) {
                    setBtn_bckgrnd(btn_2ndAnsOpt, true);
                    str_SelectedAnswerSequence = "2";
                    str_SelectedAnswerText = txt_2ndAnsOpt.getText().toString();
                    str_QuestionAttempted_Flag = "Y";
                    str_SelectedAnswerId = "" + DatabaseHelper.GetAnsId(MCQ_QuestionActivity.this, DatabaseHelper.db,
                            str_SelectedAnswerText, StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId);
                    //setTxt_bckgrnd(txt_B, true);
                } else if (AttemptedAnsSeq.equals("3")) {
                    setBtn_bckgrnd(btn_3rdAnsOpt, true);
                    str_SelectedAnswerSequence = "3";
                    str_SelectedAnswerText = txt_3rdAnsOpt.getText().toString();
                    str_QuestionAttempted_Flag = "Y";
                    str_SelectedAnswerId = "" + DatabaseHelper.GetAnsId(MCQ_QuestionActivity.this, DatabaseHelper.db,
                            str_SelectedAnswerText, StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId);
                    //setTxt_bckgrnd(txt_C, true);
                } else if (AttemptedAnsSeq.equals("4")) {
                    setBtn_bckgrnd(btn_4thAnsOpt, true);
                    str_SelectedAnswerSequence = "4";
                    str_SelectedAnswerText = txt_4thAnsOpt.getText().toString();
                    str_QuestionAttempted_Flag = "Y";
                    str_SelectedAnswerId = "" + DatabaseHelper.GetAnsId(MCQ_QuestionActivity.this, DatabaseHelper.db,
                            str_SelectedAnswerText, StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId);
                    //setTxt_bckgrnd(txt_D, true);
                } else if (AttemptedAnsSeq.equals("5")) {
                    setBtn_bckgrnd(btn_psycho5thOpt, true);
                    str_SelectedAnswerSequence = "5";
                    str_SelectedAnswerText = txt_5thAnsOpt.getText().toString();
                    str_QuestionAttempted_Flag = "Y";
                    str_SelectedAnswerId = "" + DatabaseHelper.GetAnsId(MCQ_QuestionActivity.this, DatabaseHelper.db,
                            str_SelectedAnswerText, StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId);
                    //setTxt_bckgrnd(txt_E, true);
                } else {
                    str_SelectedAnswerText = "";
                    str_QuestionAttempted_Flag = "";
                    str_SelectedAnswerId = "";
                    str_SelectedAnswerSequence = "";
                }

            } else {
                SetAnsForExamType1("" + ExmId, "" + QueId);
            }
        } catch (Exception e) {
            Log.e("", "SetAttemptedQuetsAns err=" + e.toString());
            DatabaseHelper.SaveErroLog(MCQ_QuestionActivity.this, DatabaseHelper.db,
                    "MCQ_QuestionActivity", "SetAttemptedQuetsAns()", e.toString());
        }
    }

    void SetEE_AttemptedQuetsAns(String ExmId, String QueId) {
        try {
            String AttemptedAnsSeq = "0";
            if (StaticVariables.crnt_ExamType != 1) {
                AttemptedAnsSeq = "" + DatabaseHelper.GetAtmptdAnsSeq(MCQ_QuestionActivity.this,
                        DatabaseHelper.db, StaticVariables.UserLoginId, ExmId, "" + QueId, "" + (attemptId + 1));
                ResetBtn_bckgrnd();
                if (AttemptedAnsSeq.equals("1")) {
                    str_SelectedAnswerText = txt_1stAnsOpt.getText().toString();
                    String str_RightAns = DatabaseHelper.GetRightAns_Exm1(MCQ_QuestionActivity.this, DatabaseHelper.db,
                            "" + ExmId, "" + QueId);
                    if (!str_RightAns.trim().equalsIgnoreCase(str_SelectedAnswerText.trim())) {
                        setEE_RightAns_bckgrnd(str_RightAns);
                        setSelectedAns_bckgrnd(str_SelectedAnswerText);
                        txt_hint.setText("Sorry, Selected answer is wrong, \"" + str_RightAns + "\" is the right answer.");
                    } else {
                        setBtn_bckgrnd(btn_1stAnsOpt, true);
                        txt_hint.setText("Congratulations, Selected answer is right.");
                    }
                    str_QuestionAttempted_Flag = "Y";
                    str_SelectedAnswerId = "" + DatabaseHelper.GetAnsId(MCQ_QuestionActivity.this, DatabaseHelper.db,
                            str_SelectedAnswerText, StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId);
                } else if (AttemptedAnsSeq.equals("2")) {
                    str_SelectedAnswerText = txt_2ndAnsOpt.getText().toString();
                    String str_RightAns = DatabaseHelper.GetRightAns_Exm1(MCQ_QuestionActivity.this, DatabaseHelper.db,
                            "" + ExmId, "" + QueId);
                    if (!str_RightAns.trim().equalsIgnoreCase(str_SelectedAnswerText.trim())) {
                        setEE_RightAns_bckgrnd(str_RightAns);
                        setSelectedAns_bckgrnd(str_SelectedAnswerText);
                        txt_hint.setText("Sorry, Selected answer is wrong, \"" + str_RightAns + "\" is the right answer.");
                    } else {
                        setBtn_bckgrnd(btn_2ndAnsOpt, true);
                        txt_hint.setText("Congratulations, Selected answer is right.");
                    }
                    str_QuestionAttempted_Flag = "Y";
                    str_SelectedAnswerId = "" + DatabaseHelper.GetAnsId(MCQ_QuestionActivity.this, DatabaseHelper.db,
                            str_SelectedAnswerText, StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId);
                } else if (AttemptedAnsSeq.equals("3")) {
                    str_SelectedAnswerText = txt_3rdAnsOpt.getText().toString();
                    String str_RightAns = DatabaseHelper.GetRightAns_Exm1(MCQ_QuestionActivity.this, DatabaseHelper.db,
                            "" + ExmId, "" + QueId);
                    if (!str_RightAns.trim().equalsIgnoreCase(str_SelectedAnswerText.trim())) {
                        setEE_RightAns_bckgrnd(str_RightAns);
                        setSelectedAns_bckgrnd(str_SelectedAnswerText);
                        txt_hint.setText("Sorry, Selected answer is wrong, \"" + str_RightAns + "\" is the right answer.");
                    } else {
                        setBtn_bckgrnd(btn_3rdAnsOpt, true);
                        txt_hint.setText("Congratulations, Selected answer is right.");
                    }
                    str_QuestionAttempted_Flag = "Y";
                    str_SelectedAnswerId = "" + DatabaseHelper.GetAnsId(MCQ_QuestionActivity.this, DatabaseHelper.db,
                            str_SelectedAnswerText, StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId);
                } else if (AttemptedAnsSeq.equals("4")) {
                    str_SelectedAnswerText = txt_4thAnsOpt.getText().toString();
                    String str_RightAns = DatabaseHelper.GetRightAns_Exm1(MCQ_QuestionActivity.this, DatabaseHelper.db,
                            "" + ExmId, "" + QueId);
                    if (!str_RightAns.trim().equalsIgnoreCase(str_SelectedAnswerText.trim())) {
                        setEE_RightAns_bckgrnd(str_RightAns);
                        setSelectedAns_bckgrnd(str_SelectedAnswerText);
                        txt_hint.setText("Sorry, Selected answer is wrong, \"" + str_RightAns + "\" is the right answer.");
                    } else {
                        setBtn_bckgrnd(btn_4thAnsOpt, true);
                        txt_hint.setText("Congratulations, Selected answer is right.");
                    }
                    str_QuestionAttempted_Flag = "Y";
                    str_SelectedAnswerId = "" + DatabaseHelper.GetAnsId(MCQ_QuestionActivity.this, DatabaseHelper.db,
                            str_SelectedAnswerText, StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId);
                } else if (AttemptedAnsSeq.equals("5")) {
                    str_SelectedAnswerText = txt_5thAnsOpt.getText().toString();
                    String str_RightAns = DatabaseHelper.GetRightAns_Exm1(MCQ_QuestionActivity.this, DatabaseHelper.db,
                            "" + ExmId, "" + QueId);
                    if (!str_RightAns.trim().equalsIgnoreCase(str_SelectedAnswerText.trim())) {
                        setEE_RightAns_bckgrnd(str_RightAns);
                        setSelectedAns_bckgrnd(str_SelectedAnswerText);
                        txt_hint.setText("Sorry, Selected answer is wrong, \"" + str_RightAns + "\" is the right answer.");
                    } else {
                        setBtn_bckgrnd(btn_psycho5thOpt, true);
                        txt_hint.setText("Congratulations, Selected answer is right.");
                    }
                    str_QuestionAttempted_Flag = "Y";
                    str_SelectedAnswerId = "" + DatabaseHelper.GetAnsId(MCQ_QuestionActivity.this, DatabaseHelper.db,
                            str_SelectedAnswerText, StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId);
                } else {
                    str_SelectedAnswerText = "";
                    str_QuestionAttempted_Flag = "";
                    str_SelectedAnswerId = "";
                    str_SelectedAnswerSequence = "";
                }
            }
        } catch (Exception e) {
            Log.e("", "SetEE_AttemptedQuetsAns err=" + e.toString());
            DatabaseHelper.SaveErroLog(MCQ_QuestionActivity.this, DatabaseHelper.db,
                    "MCQ_QuestionActivity", "SetEE_AttemptedQuetsAns()", e.toString());
        }
    }

    private void setExmProgress(int progressCnt) {
        // Set Progress of exam attend
        txt_ExamType.setText("" + StaticVariables.crnt_ExmTypeName);
        if (QueSrNo < 0)
            QueSrNo = 1;

        txt_QueCount.setText(QueSrNo + " / " + StaticVariables.TblNameForCount);
        Exm_progressBar.getProgressDrawable().setColorFilter(
                Color.WHITE, android.graphics.PorterDuff.Mode.SRC_IN);
        Exm_progressBar.setProgress(QueSrNo);
        txt_progress.setText(QueSrNo + "/" + Exm_progressBar.getMax());

    }

    private void custom_ResultRemrk() {

        final AlertDialog alertD;
        LayoutInflater logout_inflator = LayoutInflater.from(MCQ_QuestionActivity.this);
        final View openDialog = logout_inflator.inflate(R.layout.alert_ansalertdialog, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MCQ_QuestionActivity.this);
        alertDialogBuilder.setView(openDialog);
        alertD = alertDialogBuilder.create();
        alertD.setCancelable(false);
        alertD.show();

        final TextView alrtText_ResultRemark = (TextView) openDialog.findViewById(R.id.alrtText_ResultRemart);
        final TextView alrtText_CrrectResult = (TextView) openDialog.findViewById(R.id.alrtText_CrrectResult);
        final TextView Txt_AlertTitle = (TextView) openDialog.findViewById(R.id.Txt_AlertTitle);

        final Button alrtBtn_done = (Button) openDialog.findViewById(R.id.alrtbtn_Done);
        alrtText_CrrectResult.setVisibility(View.GONE);

        if (highligtCrrectAns_Exm(StaticVariables.crnt_ExmId, StaticVariables.crnt_QueId, str_SelectedAnswer).equals("Wrong")) {
            String str_RightAns = DatabaseHelper.GetRightAns_Exm1(MCQ_QuestionActivity.this,
                    DatabaseHelper.db, "" + StaticVariables.crnt_ExmId, "" + StaticVariables.crnt_QueId);
            alrtText_ResultRemark.setText("Sorry...\n" +
                    "You have selected the wrong answer!\n");

            alrtText_CrrectResult.setVisibility(View.VISIBLE);
            alrtText_CrrectResult.setText("Correct Answer is:\n" + str_RightAns);
        } else {
            alrtText_CrrectResult.setVisibility(View.GONE);
            alrtText_ResultRemark.setText("Congratulations...\nYour selected answer is absolutely correct!");
        }

        alrtBtn_done.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    alertD.dismiss();

                    if (QueSrNo == StaticVariables.TblNameForCount) {
                        Toast.makeText(MCQ_QuestionActivity.this, "Practice Exam submitted successfully ...", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(MCQ_QuestionActivity.this, ExamList_Activity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        MCQ_QuestionActivity.this.finish();
                    } else {
                        moveToNextQue();
                    }

                } catch (Exception e) {
                    Log.e("", "err=" + e.toString());
                }
            }
        });
    }

    private void custom_SubmitTestAlert(final String Title, String Msg) {

        final AlertDialog alertD;
        LayoutInflater logout_inflator = LayoutInflater.from(MCQ_QuestionActivity.this);
        final View openDialog = logout_inflator.inflate(R.layout.custom_exit_dialog, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MCQ_QuestionActivity.this);
        alertDialogBuilder.setView(openDialog);
        alertD = alertDialogBuilder.create();
        alertD.setCancelable(false);
        alertD.show();

        final TextView alrtText_ResultRemark = (TextView) openDialog.findViewById(R.id.txt_message);
        final TextView Txt_AlertTitle = (TextView) openDialog.findViewById(R.id.promt_title);
        final Button alrtBtn_cancle = (Button) openDialog.findViewById(R.id.btn_no);
        final Button alrtBtn_done = (Button) openDialog.findViewById(R.id.btn_yes);
        alrtBtn_cancle.setVisibility(View.GONE);
        if (Title.equals("No internet")) {
            alrtBtn_done.setText("OK");
        } else if (Title.equals("Server not responding!")) {
            alrtBtn_done.setText("OK");
        } else if (Title.equals("Submit Exam")) {
            alrtBtn_done.setText("SUBMIT");
            alrtBtn_cancle.setVisibility(View.VISIBLE);
        } else if (Title.equals("Leave Exam")) {
            alrtBtn_done.setText("LEAVE");
            alrtBtn_cancle.setVisibility(View.VISIBLE);
        } else if (Title.equals("Alert!")) {
            alrtBtn_done.setText("SYNC");
            alrtBtn_cancle.setVisibility(View.VISIBLE);
        } else if (Title.equals("Attention")) {
            alrtBtn_done.setText("OK");
            alrtBtn_cancle.setVisibility(View.GONE);
        } else if (StaticVariables.crnt_ExamType == 6) {
            alrtBtn_done.setText("View Summary");
            alrtBtn_done.setPadding(15, 0, 15, 0);
            alrtBtn_cancle.setVisibility(View.GONE);
        } else if (Title.trim().equalsIgnoreCase("Feedback")
                || Title.trim().equalsIgnoreCase("Psychometric Profiling")) {
            alrtBtn_done.setText("OK");
            alrtBtn_cancle.setVisibility(View.GONE);
        }


        Txt_AlertTitle.setText(Title);
        alrtText_ResultRemark.setText(Msg);

        alrtBtn_done.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    alertD.dismiss();

                    if (StaticVariables.crnt_ExamType == 1)
                        DatabaseHelper.InsertActivityTracker(MCQ_QuestionActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                                "A1060101", "A10601", "Attempted Exam", "", "",
                                "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "", StaticVariables.UserName, "",
                                "", "", "Pending");
                    if (StaticVariables.crnt_ExamType == 2)
                        DatabaseHelper.InsertActivityTracker(MCQ_QuestionActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                                "A1060201", "A10602", "Attempted Exam", "", "",
                                "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "", StaticVariables.UserName, "",
                                "", "", "Pending");

                    if (StaticVariables.crnt_ExamType == 3)
                        DatabaseHelper.InsertActivityTracker(MCQ_QuestionActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                                "A1060301", "A10603", "Attempted Exam", "", "",
                                "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "", StaticVariables.UserName, "",
                                "", "", "Pending");
                    if (StaticVariables.crnt_ExamType == 4) {
                        DatabaseHelper.InsertActivityTracker(MCQ_QuestionActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                                "A10301", "A103", "Psychometric Profiling", "", "",
                                "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "", StaticVariables.UserName, "",
                                "", "", "Pending");
                    }
                    if (StaticVariables.crnt_ExamType == 6) {
                        DatabaseHelper.InsertActivityTracker(MCQ_QuestionActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                                strSectionId, "A111", Trip + " quiz attempted", "", "",
                                "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "", StaticVariables.UserName, "",
                                "", "", "Pending");
                    }
                    if (StaticVariables.crnt_ExamType == 7) {
                        DatabaseHelper.InsertActivityTracker(MCQ_QuestionActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                                strSectionId, "A112", LessonDesc, "", "",
                                "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "", StaticVariables.UserName, "",
                                "", "", "Pending");
                    }

//                    if (Title.equals("Attempt not allowed")){
//
//                    }else
                    if (Title.equals("No internet") || Title.equals("Attention")) {
                        DatabaseHelper.db.delete(getResources().getString(R.string.TBL_LPICandidateAttemptQueAns), "LoginId=? and ExamId=? and AttemptId=?",
                                new String[]{StaticVariables.UserLoginId, "" + StaticVariables.crnt_ExmId, "" + (attemptId + 1)});
//                        Intent i;
//                        i = new Intent(MCQ_QuestionActivity.this, ParticipantMenuActivity.class);
//                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        startActivity(i);
//                        MCQ_QuestionActivity.this.finish();
                        GoToPreviousCall();
                    } else if (Title.equals("Leave Exam")) {
//                        DatabaseHelper.db.delete(getResources().getString(R.string.TBL_LPICandidateAttemptQueAns), "LoginId=? and ExamId=? and AttemptId=?",
//                                new String[]{StaticVariables.UserLoginId, "" + StaticVariables.crnt_ExmId, "" + (attemptId + 1)});
//                        if (StaticVariables.crnt_ExamType == 4) {
//                            DatabaseHelper.DeleteAttemptedExam(MCQ_QuestionActivity.this, DatabaseHelper.db,
//                                    "" + StaticVariables.crnt_ExmId, StaticVariables.UserLoginId);
//                        } else
//                        StaticVariables.UserType.equalsIgnoreCase("C")
                        if (StaticVariables.crnt_ExamType == 6 && StaticVariables.isSummary) {

                        } else {
                            deleteAttemptDetails();
                        }
                        StaticVariables.isSummary = false;
                        GoToPreviousCall();
//                        Intent i = new Intent(getApplicationContext(), ParticipantMenuActivity.class);
//                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        startActivity(i);
//                        MCQ_QuestionActivity.this.finish();
                    } else if (StaticVariables.crnt_ExamType == 3 && Title.equals("Submit Exam")) {
                        if (CommonClass.isConnected(MCQ_QuestionActivity.this)) {
                            new SubmitExam(0, MCQ_QuestionActivity.this).execute();
                        } else {
                            DatabaseHelper.SaveXMLData(MCQ_QuestionActivity.this, DatabaseHelper.db,
                                    CommonClass.SubmitExamXML(MCQ_QuestionActivity.this,
                                            StaticVariables.UserLoginId, "" + StaticVariables.crnt_ExmId),
                                    MCQ_QuestionActivity.this.getString(R.string.SubmitExam));

//                            deleteAttemptDetails();
//                            DatabaseHelper.db.delete(getResources().getString(R.string.TBL_LPICandidateAttemptQueAns), "LoginId=? and ExamId=? and AttemptId=?",
//                                    new String[]{StaticVariables.UserLoginId, "" + StaticVariables.crnt_ExmId, "" + (attemptId + 1)});
                            custom_SubmitTestAlert("No internet",
                                    "Exam data saved successfully,\nPlease Sync to send your exam data.\nThank you...");
                        }
                    } else if (Title.equals("Alert!")) {
                        if (CommonClass.isConnected(MCQ_QuestionActivity.this)) {
                            new syncExams(new Callback<String>() {
                                @Override
                                public void execute(String result, String status) {
                                    init();
                                }
                            }, MCQ_QuestionActivity.this,
                                    "MCQ_QuestionActivity", StaticVariables.UserLoginId,
                                    "" + StaticVariables.crnt_ExmId, "" + StaticVariables.crnt_ExamType).execute();
                        } else {
                            custom_SubmitTestAlert("No internet",
                                    "Please make sure that you are connected to the internet");
                        }
                    } else if (StaticVariables.crnt_ExamType == 6) {
                        /*if (Title.equals("Server not responding!")) {
                         *//*Intent intent = new Intent(MCQ_QuestionActivity.this, LPILessonQuizActivity.class);
                            startActivity(intent);
                            finish();*//*
                        } else {
                            StaticVariables.isSummary = true;
                            btn_Submit.setVisibility(View.GONE);
                            initExam();
                        }*/
                        setDisableClickableMQCOption(true);
                        StaticVariables.crnt_QueId = 1;
                        StaticVariables.isSummary = true;
                        btn_Submit.setVisibility(View.GONE);
                        btn_allowPrevious.setVisibility(View.VISIBLE);
                        btn_allowNext.setVisibility(View.VISIBLE);
                        initExam();
                    } else {
                        if (StaticVariables.crnt_ExamType == 4) {
                            Intent i;
                            i = new Intent(MCQ_QuestionActivity.this, ViewResultActivity.class);
                            i.putExtra("ExamId", "" + StaticVariables.crnt_ExmId);
                            i.putExtra("AttemptedId", "");
                            i.putExtra("CandidateId", "" + StaticVariables.UserLoginId);
                            i.putExtra("initFrom", "MCQ_QuestionActivity");
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            MCQ_QuestionActivity.this.finish();
                        } else {
                            GoToPreviousCall();
                        }
                    }
                } catch (Exception e) {
                    Log.e("", "custom_SubmitTestAlert Error=" + e.toString());
                    DatabaseHelper.SaveErroLog(MCQ_QuestionActivity.this, DatabaseHelper.db,
                            "MCQ_QuestionActivity", "custom_SubmitTestAlert", e.toString());
                }
            }
        });

        alrtBtn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Title.equals("Alert!")) {
                    alertD.dismiss();
                    GoToPreviousCall();
                } else {
                    alertD.dismiss();
                }
//                if (Title.equals("Alert!")) {
//                    Intent i;
//                    i = new Intent(MCQ_QuestionActivity.this, ParticipantMenuActivity.class);
//                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(i);
//                    MCQ_QuestionActivity.this.finish();
//                }
//                GoToPreviousCall();
            }
        });
    }

    public void deleteAttemptDetails() {
        DatabaseHelper.db.delete(getResources().getString(R.string.TBL_LPICandidateAttemptQueAns), "LoginId=? and ExamId=? and AttemptId=?",
                new String[]{StaticVariables.UserLoginId, "" + StaticVariables.crnt_ExmId, "" + (attemptId + 1)});
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show, String Msg) {
        if (show) {
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(Msg);
            progressDialog.show();
        } else
            progressDialog.dismiss();

    }

    void setDisableClickableMQCOption(boolean flag) {
        if (flag) {
            LinearLayout myLayout = (LinearLayout) findViewById(R.id.lin_mqc_options_parent);
            for (int i = 0; i < myLayout.getChildCount(); i++) {
                View view = myLayout.getChildAt(i);
                view.setEnabled(false);
            }
        } else {
            LinearLayout myLayout = (LinearLayout) findViewById(R.id.lin_mqc_options_parent);
            for (int i = 0; i < myLayout.getChildCount(); i++) {
                View view = myLayout.getChildAt(i);
                view.setEnabled(true);
            }
        }
    }

    public class SubmitExam extends AsyncTask<String, String, InputStream> {
        public int position;
        public Context context;

        public SubmitExam(int position, Context con) {
            this.position = position;
            this.context = con;
        }

        @Override
        protected void onPreExecute() {
            showProgress(true, "Submitting...");
        }

        @SuppressLint("NewApi")
        @Override
        protected InputStream doInBackground(String... params) {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    MCQ_QuestionActivity.this.getString(R.string.Exam_NAMESPACE),
                    MCQ_QuestionActivity.this.getString(R.string.SubmitExam));

            String str_XML = CommonClass.SubmitExamXML(context, StaticVariables.UserLoginId, "" + StaticVariables.crnt_ExmId);

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            inputPI1.setName("objXMLDoc");
            try {
                inputPI1.setValue(str_XML);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            // Set output SOAP object
            envelope.setOutputSoapObject(request);
            // Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(MCQ_QuestionActivity.this.getString(R.string.Exam_URL));
            InputStream isResponse = null;


            try {
                // Involve Web service
                LPIEEX509TrustManager.allowAllSSL();
//                FakeX509TrustManager.trustSSLCertificate(MCQ_QuestionActivity.this);
                androidHttpTransport.call(MCQ_QuestionActivity.this.getString(R.string.Exam_SOAP_ACTION) +
                        MCQ_QuestionActivity.this.getString(R.string.SubmitExam), envelope);
                // Get the response
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

                String strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString();
                Log.e("", "response=" + strResponse);

                isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
                SubmitExmInputStream = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));


                Log.e("", "Response=" + isResponse);
            } catch (Exception e) {
                Log.e("", "Login Error=" + e.toString());
                ResponseCode = 2;

//                DatabaseHelper.SaveAttemptedExam(MCQ_QuestionActivity.this, DatabaseHelper.db,
//                        "" + StaticVariables.crnt_ExmId, StaticVariables.crnt_ExmName, "N", "");
                DatabaseHelper.UpdateAttemptedExam(MCQ_QuestionActivity.this, DatabaseHelper.db,
                        "" + StaticVariables.crnt_ExmId, StaticVariables.crnt_ExmName, "N",
                        "" + (attemptId + 1), StaticVariables.UserLoginId);
                DatabaseHelper.SaveErroLog(MCQ_QuestionActivity.this, DatabaseHelper.db,
                        "MCQ_QuestionActivity", "SubmitExam.doInBackground()", e.toString());

                DatabaseHelper.SaveXMLData(MCQ_QuestionActivity.this, DatabaseHelper.db,
                        str_XML, MCQ_QuestionActivity.this.getString(R.string.SubmitExam));

            }
            return isResponse;
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            showProgress(false, "");
            NodeList nList = null;
            try {
                if (CommonClass.IsCorrectXMLResponse(inputStream)) {
                    nList = CommonClass.GetXMLElementNodeList(SubmitExmInputStream, "SubmitExam");
                    if (nList == null) {

                    } else if (nList.getLength() > 0) {
                        for (int i = 0; i < nList.getLength(); i++) {
                            Node node = nList.item(i);
                            if (node.getNodeType() == Node.ELEMENT_NODE) {
                                Element element = (Element) node;
                                if (CommonClass.getValue(context, "ResponseCode", element).equals("0")) {
//                                    deleteAttemptDetails();
//                                DatabaseHelper.db.delete(getResources().getString(R.string.TBL_LPICandidateAttemptQueAns), "LoginId=? and ExamId=?",
//                                        new String[]{StaticVariables.UserLoginId, "" + StaticVariables.crnt_ExmId});
//                                attemptId=Integer.parseInt(CommonClass.getValue(context, "AttemptId",element));
                                    DatabaseHelper.UpdateAttemptedExam(MCQ_QuestionActivity.this, DatabaseHelper.db,
                                            "" + StaticVariables.crnt_ExmId, StaticVariables.crnt_ExmName, "Y",
                                            "" + (attemptId + 1), StaticVariables.UserLoginId);

                                    if (StaticVariables.crnt_ExamType == 4) {
                                        custom_SubmitTestAlert("Psychometric Profiling", "Thank you for submitting your Psychometric profile assessment.");
                                    } else if (StaticVariables.crnt_ExamType == 3) {
                                        custom_SubmitTestAlert(StaticVariables.crnt_ExmName, "" + StaticVariables.crnt_ExmName + " exam submitted successfully,Thank you...");
                                    } else if (StaticVariables.crnt_ExamType == 6) {
                                        custom_SubmitTestAlert(StaticVariables.crnt_ExmName, "" + StaticVariables.crnt_ExmName + " exam submitted successfully, Thank you...");
                                    }
                                } else {
//                                DatabaseHelper.SaveAttemptedExam(MCQ_QuestionActivity.this, DatabaseHelper.db,
//                                        "" + StaticVariables.crnt_ExmId, StaticVariables.crnt_ExmName, "N", "");
                                    DatabaseHelper.UpdateAttemptedExam(MCQ_QuestionActivity.this, DatabaseHelper.db,
                                            "" + StaticVariables.crnt_ExmId, StaticVariables.crnt_ExmName, "N",
                                            "" + (attemptId + 1), StaticVariables.UserLoginId);
                                    custom_SubmitTestAlert("Server not responding!",
                                            "Something went wrong. Please try again..");
                                }
                            }
                        }
                    }

                } else if (ResponseCode == 2) {
//                DatabaseHelper.SaveAttemptedExam(MCQ_QuestionActivity.this, DatabaseHelper.db,
//                        "" + StaticVariables.crnt_ExmId, StaticVariables.crnt_ExmName, "N", "");
                    DatabaseHelper.UpdateAttemptedExam(MCQ_QuestionActivity.this, DatabaseHelper.db,
                            "" + StaticVariables.crnt_ExmId, StaticVariables.crnt_ExmName, "N",
                            "" + (attemptId + 1), StaticVariables.UserLoginId);
                    if (StaticVariables.crnt_ExamType == 4)
                        custom_SubmitTestAlert("Psychometric Profiling", "" +
                                "Server not responding. Psychometric data saved successfully.");
                    else
                        custom_SubmitTestAlert("Server not responding!",
                                "" + StaticVariables.crnt_ExmName + " exam data saved successfully,Thank you...");
//                Toast.makeText(MCQ_QuestionActivity.this, "Exam submitted successfully ...", Toast.LENGTH_SHORT).show();
                } else {
//                DatabaseHelper.SaveAttemptedExam(MCQ_QuestionActivity.this, DatabaseHelper.db,
//                        "" + StaticVariables.crnt_ExmId, StaticVariables.crnt_ExmName, "N", "");
                    DatabaseHelper.UpdateAttemptedExam(MCQ_QuestionActivity.this, DatabaseHelper.db,
                            "" + StaticVariables.crnt_ExmId, StaticVariables.crnt_ExmName, "N",
                            "" + (attemptId + 1), StaticVariables.UserLoginId);
                    custom_SubmitTestAlert("Server not responding!",
                            "Something went wrong. Please try again..");
                }
            } catch (Exception e) {
                e.printStackTrace();
                DatabaseHelper.UpdateAttemptedExam(MCQ_QuestionActivity.this, DatabaseHelper.db,
                        "" + StaticVariables.crnt_ExmId, StaticVariables.crnt_ExmName, "N",
                        "" + (attemptId + 1), StaticVariables.UserLoginId);
                custom_SubmitTestAlert("Server not responding!",
                        "Something went wrong. Please try again..");
                DatabaseHelper.SaveErroLog(MCQ_QuestionActivity.this, DatabaseHelper.db,
                        "MCQ_QuestionActivity", "SubmitExam.onPostExecute()", e.toString());

            }
        }
    }

    private void alertCustomDialog(final String title, String message) {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(MCQ_QuestionActivity.this).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_exit_dialog, null);
            TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            Button btn_ok = (Button) dialogView.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialogView.findViewById(R.id.btn_no);

            promt_title.setText(title);
            txt_message.setText(message);
            if (title.equalsIgnoreCase("Exam Alert")) {
                btn_ok.setText("OK");
                btn_no.setVisibility(View.GONE);
            } else if (title.equalsIgnoreCase("No Internet")) {
                btn_ok.setText("OK");
                btn_no.setVisibility(View.GONE);
            } else {
                btn_ok.setText("YES");
                btn_no.setText("No");
            }
            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    if (title.equalsIgnoreCase("Submit Exam")) {
                        submitExam();
                    } else if (title.equalsIgnoreCase("Exam Alert")) {
                        GoToPreviousCall();
                    } else if (title.equalsIgnoreCase("No Internet")) {
                        GoToPreviousCall();
                    }
                }
            });

            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });

            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
