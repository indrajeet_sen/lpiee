package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

@SuppressLint("Range")
public class PIN_AuthActivity extends AppCompatActivity {

    protected static String strLoginId = "";
    TextView radio1, radio2, radio3, radio4;
    Button pin_code_button_1, pin_code_button_2, pin_code_button_3, pin_code_button_4, pin_code_button_5, pin_code_button_6,
            pin_code_button_7, pin_code_button_8, pin_code_button_9, pin_code_button_0;
    ImageButton pin_code_button_clear;
    TextView passMain, confirm_pass, txt_inputType;
    String StrPass = "";
    String StrPassTemp = "";
    LinearLayout lin_forgot;
    TextView txt_forgot;
    boolean doubleBackToExitPressedOnce = false;
    ImageButton pin_code_button_cancle, pin_code_button_hash;
    //    public static final String MyPREFERENCES = "PIN_Prefs";
//    SharedPreferences sharedpreferences;
    boolean isAvailableData = false;
    boolean isFirstTime = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finishAffinity();
            System.exit(0);
        }

        this.doubleBackToExitPressedOnce = true;
        Snackbar.make(findViewById(R.id.lin_pin), "Please click BACK again to exit", Snackbar.LENGTH_LONG).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_pin_auth);
        InitUI();
        UIListener();
    }

    private void UIListener() {
        pin_code_button_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backSpacePressed();
            }
        });
        pin_code_button_hash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Cursor mCursor = DatabaseHelper.Get_mPINCursor(PIN_AuthActivity.this, DatabaseHelper.db, strLoginId);
                if (mCursor.getCount() > 0) {
                    for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                        String isPIN = mCursor.getString(mCursor.getColumnIndex("is_mPIN_Data"));
                        if (isPIN != null && isPIN.trim().equalsIgnoreCase("Y")) {
                            isAvailableData = true;
                        } else {
                            isAvailableData = false;
                        }
                    }
                } else {
                    isAvailableData = false;
                }
                if (isAvailableData) {
                    isValidCredentials();
                } else {
                    lin_forgot.setVisibility(View.VISIBLE);

                    Animation shake = AnimationUtils.loadAnimation(PIN_AuthActivity.this, R.anim.shake);
                    findViewById(R.id.radio).startAnimation(shake);
                    passMain.setText("");
                    StrPass = "";
                    radio1.setBackground(getResources().getDrawable(R.drawable.circle_radio));
                    radio2.setBackground(getResources().getDrawable(R.drawable.circle_radio));
                    radio3.setBackground(getResources().getDrawable(R.drawable.circle_radio));
                    radio4.setBackground(getResources().getDrawable(R.drawable.circle_radio));
                }

            }
        });
        pin_code_button_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passMain.setText(StrPass.trim() + "1");
                StrPass = passMain.getText().toString().trim();
                fillRadios();
            }
        });
        pin_code_button_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passMain.setText(StrPass.trim() + "2");
                StrPass = passMain.getText().toString().trim();
                fillRadios();
            }
        });
        pin_code_button_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passMain.setText(StrPass.trim() + "3");
                StrPass = passMain.getText().toString().trim();
                fillRadios();
            }
        });
        pin_code_button_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passMain.setText(StrPass.trim() + "4");
                StrPass = passMain.getText().toString().trim();
                fillRadios();
            }
        });
        pin_code_button_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passMain.setText(StrPass.trim() + "5");
                StrPass = passMain.getText().toString().trim();
                fillRadios();
            }
        });
        pin_code_button_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passMain.setText(StrPass.trim() + "6");
                StrPass = passMain.getText().toString().trim();
                fillRadios();
            }
        });
        pin_code_button_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passMain.setText(StrPass.trim() + "7");
                StrPass = passMain.getText().toString().trim();
                fillRadios();
            }
        });
        pin_code_button_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passMain.setText(StrPass.trim() + "8");
                StrPass = passMain.getText().toString().trim();
                fillRadios();
            }
        });
        pin_code_button_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passMain.setText(StrPass.trim() + "9");
                StrPass = passMain.getText().toString().trim();
                fillRadios();
            }
        });
        pin_code_button_0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passMain.setText(StrPass.trim() + "0");
                StrPass = passMain.getText().toString().trim();
                fillRadios();
            }
        });
        /*pin_code_button_clear.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                passMain.setText("");
                StrPass = "";
                radio1.setBackground(getResources().getDrawable(R.drawable.circle_radio));
                radio2.setBackground(getResources().getDrawable(R.drawable.circle_radio));
                radio3.setBackground(getResources().getDrawable(R.drawable.circle_radio));
                radio4.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            }
        });*/
        lin_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PIN_AuthActivity.this, Login.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                PIN_AuthActivity.this.finish();
            }
        });
    }

    @SuppressLint("NewApi")
    private void fillRadios() {

        Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibe.vibrate(50);

        if (passMain.getText().toString().trim().length() == 1) {
            radio1.setBackground(getResources().getDrawable(R.drawable.circle_radio_filled));
        } else if (passMain.getText().toString().trim().length() == 2) {
            radio2.setBackground(getResources().getDrawable(R.drawable.circle_radio_filled));
        } else if (passMain.getText().toString().trim().length() == 3) {
            radio3.setBackground(getResources().getDrawable(R.drawable.circle_radio_filled));
        } else if (passMain.getText().toString().trim().length() == 4) {
            radio4.setBackground(getResources().getDrawable(R.drawable.circle_radio_filled));
            try {
                Cursor mCursor = DatabaseHelper.Get_mPINCursor(PIN_AuthActivity.this, DatabaseHelper.db, strLoginId);
                if (mCursor.getCount() > 0) {
                    for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                        String isPIN = mCursor.getString(mCursor.getColumnIndex("is_mPIN_Data"));
                        if (isPIN != null && isPIN.trim().equalsIgnoreCase("Y")) {
                            isAvailableData = true;
                        } else {
                            isAvailableData = false;
                        }
                    }
                } else {
                    isAvailableData = false;
                }
                if (isAvailableData) {
                    isValidCredentials();
                } else {
                    if (isFirstTime) {
                        if ((StrPassTemp != null || !StrPassTemp.equals(""))
                                && StrPassTemp.trim().equalsIgnoreCase(passMain.getText().toString().trim())) {
                            DatabaseHelper.SavePINInfo(PIN_AuthActivity.this, DatabaseHelper.db, strLoginId, passMain.getText().toString().trim(), "Y");
                            isValidCredentials();
                        } else {
                            //Toast.makeText(PIN_AuthActivity.this,"MPIN not match")
                            showAlert(PIN_AuthActivity.this, "MPIN not match", "Do you want to reset MPIN?");
                            lin_forgot.setVisibility(View.VISIBLE);
                            Animation shake = AnimationUtils.loadAnimation(PIN_AuthActivity.this, R.anim.shake);
                            findViewById(R.id.radio).startAnimation(shake);
                        }
                    } else {
                        saveCredentials();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (passMain.getText().toString().trim().length() > 4) {
            passMain.setText("");
            StrPass = "";
            radio1.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            radio2.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            radio3.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            radio4.setBackground(getResources().getDrawable(R.drawable.circle_radio));
        }
    }

    @SuppressLint("NewApi")
    private void saveCredentials() {
        // DatabaseHelper.SavePINInfo(PIN_AuthActivity.this, DatabaseHelper.db, strLoginId, passMain.getText().toString().trim(), "Y");
        isFirstTime = true;
        StrPassTemp = passMain.getText().toString().trim();
        passMain.setText("");
        StrPass = "";
        txt_inputType.setText("Re-enter 4 digit to confirm MPIN");
        radio1.setBackground(getResources().getDrawable(R.drawable.circle_radio));
        radio2.setBackground(getResources().getDrawable(R.drawable.circle_radio));
        radio3.setBackground(getResources().getDrawable(R.drawable.circle_radio));
        radio4.setBackground(getResources().getDrawable(R.drawable.circle_radio));
    }

    @SuppressLint("NewApi")
    private void isValidCredentials() {
        String savedPass = "";
        Cursor mCursor = DatabaseHelper.Get_mPINCursor(PIN_AuthActivity.this, DatabaseHelper.db, strLoginId);
        if (mCursor.getCount() > 0) {
            for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                savedPass = mCursor.getString(mCursor.getColumnIndex("mPIN"));
            }
        }
        if ((savedPass != null || !savedPass.equals(""))
                && savedPass.trim().equalsIgnoreCase(passMain.getText().toString().trim())) {
            if (isFirstTime) {
                showAlert(PIN_AuthActivity.this, "MPIN Authentication",
                        "You have successfully generated a 4 digit MPIN, please remember the MPIN No. for future login.");
            } else {
                mPIN_Login();
            }
        } else {
            lin_forgot.setVisibility(View.VISIBLE);
            Animation shake = AnimationUtils.loadAnimation(PIN_AuthActivity.this, R.anim.shake);
            findViewById(R.id.radio).startAnimation(shake);
            passMain.setText("");
            StrPass = "";
            radio1.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            radio2.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            radio3.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            radio4.setBackground(getResources().getDrawable(R.drawable.circle_radio));
        }
    }

    public void mPIN_Login() {
        try {
            Cursor cursor = DatabaseHelper.db.query(getString(R.string.TBL_iCandidate), null,
                    "CandidateLoginId=?", new String[]{strLoginId},
                    null, null, null);

            if (cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    /*if (                            cursor.getString(0).equals(strLoginId)) {
                     */
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
//                        String serverDate = sdf.format(sdf.parse(cursor.getString(3)));

                    /*//remove this if time issue resolved  and uncomment upper line
                    SimpleDateFormat sdfInput = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    Date d = sdfInput.parse(cursor.getString(cursor.getColumnIndex("LastAccessDate")));
                    String serverDate = sdf.format(d);

                    Calendar c = Calendar.getInstance();
                    c.setTime(sdf.parse(serverDate));
                    c.add(Calendar.DATE, Integer.parseInt(cursor.getString(cursor.getColumnIndex("PermisableDays"))));

                    String argDate = sdf.format(c.getTime());
                    String curDate = sdf.format(Calendar.getInstance().getTime());*/

                    StaticVariables.UserName = cursor.getString(cursor.getColumnIndex("FirstName"));
                    StaticVariables.offlineMode = "Y";
                    StaticVariables.UserLoginId = strLoginId;
                    StaticVariables.isLogin = true;
                    StaticVariables.ParentId = cursor.getString(cursor.getColumnIndex("ParentId")).trim();
                    StaticVariables.ParentName = cursor.getString(cursor.getColumnIndex("ParentName")).trim();
                    StaticVariables.UserType = cursor.getString(cursor.getColumnIndex("UserType")).trim();
                    StaticVariables.UserMobileNo = cursor.getString(cursor.getColumnIndex("CandidateMobileNo1")).trim();

                    if (cursor.getString(cursor.getColumnIndex("UserType")).trim().equalsIgnoreCase("C")) {
                        Intent intent = new Intent(PIN_AuthActivity.this, ParticipantHomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        PIN_AuthActivity.this.finish();
                    } else if (cursor.getString(cursor.getColumnIndex("UserType")).trim().equalsIgnoreCase("T")) {
                        Intent intent = new Intent(PIN_AuthActivity.this, LPI_TrainerMenuActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        PIN_AuthActivity.this.finish();
                    } else {
                        showAlert(PIN_AuthActivity.this, "MPIN login expired", "MPIN login expired, please login with internet connectivity");
                    }
                }
            } else {
                showAlert(PIN_AuthActivity.this, "MPIN login expired", "MPIN login expired, please login with internet connectivity");
            }
            cursor.close();
        } catch (Exception e) {
            Log.e("offlineLogin", "err=" + e.toString());
            showAlert(PIN_AuthActivity.this, "MPIN login expired", "MPIN login expired, please login with internet connectivity");
        }
    }

    public void showAlert(Context context, final String title, String msg) {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(PIN_AuthActivity.this).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_ok_dialog, null);
            TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            Button btn_ok = (Button) dialogView.findViewById(R.id.btn_yes);

            promt_title.setText(title);
            txt_message.setText(msg);
            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    if (title.equals("MPIN Authentication")) {
                        mPIN_Login();
                    } else if (title.equals("MPIN not match")) {
                        StrPass = "";
                        StrPassTemp = "";
                        passMain.setText("");
                        isFirstTime = false;
                        isAvailableData = false;
                        radio1.setBackground(getResources().getDrawable(R.drawable.circle_radio));
                        radio2.setBackground(getResources().getDrawable(R.drawable.circle_radio));
                        radio3.setBackground(getResources().getDrawable(R.drawable.circle_radio));
                        radio4.setBackground(getResources().getDrawable(R.drawable.circle_radio));
                        InitUI();
                    }else if (title.equals("MPIN login expired")){
                        Intent intent = new Intent(PIN_AuthActivity.this, Login.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        PIN_AuthActivity.this.finish();
                    }

                }
            });

            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void InitUI() {
        txt_inputType = (TextView) findViewById(R.id.txt_inputType);
        passMain = (TextView) findViewById(R.id.passMain);
        confirm_pass = (TextView) findViewById(R.id.confirm_pass);
        radio1 = (TextView) findViewById(R.id.radio1);
        radio2 = (TextView) findViewById(R.id.radio2);
        radio3 = (TextView) findViewById(R.id.radio3);
        radio4 = (TextView) findViewById(R.id.radio4);
        pin_code_button_1 = (Button) findViewById(R.id.pin_code_button_1);
        pin_code_button_2 = (Button) findViewById(R.id.pin_code_button_2);
        pin_code_button_3 = (Button) findViewById(R.id.pin_code_button_3);
        pin_code_button_4 = (Button) findViewById(R.id.pin_code_button_4);
        pin_code_button_5 = (Button) findViewById(R.id.pin_code_button_5);
        pin_code_button_6 = (Button) findViewById(R.id.pin_code_button_6);
        pin_code_button_7 = (Button) findViewById(R.id.pin_code_button_7);
        pin_code_button_8 = (Button) findViewById(R.id.pin_code_button_8);
        pin_code_button_9 = (Button) findViewById(R.id.pin_code_button_9);
        pin_code_button_0 = (Button) findViewById(R.id.pin_code_button_0);
        txt_forgot = (TextView) findViewById(R.id.txt_forgot);
        pin_code_button_cancle = (ImageButton) findViewById(R.id.pin_code_button_cancle);
        pin_code_button_hash = (ImageButton) findViewById(R.id.pin_code_button_hash);

        lin_forgot = (LinearLayout) findViewById(R.id.lin_forgot);
        lin_forgot.setVisibility(View.GONE);
        /*pin_code_button_clear = (ImageButton) findViewById(R.id.pin_code_button_clear);*/
        try {
            strLoginId = getIntent().getExtras().getString("user_id");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Cursor mCursor = DatabaseHelper.Get_mPINCursor(PIN_AuthActivity.this, DatabaseHelper.db, strLoginId);
            if (mCursor.getCount() > 0) {
                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    strLoginId = mCursor.getString(mCursor.getColumnIndex("CandidateLoginId"));
                    String isPIN = mCursor.getString(mCursor.getColumnIndex("is_mPIN_Data"));
                    if (isPIN != null && isPIN.trim().equalsIgnoreCase("Y")) {
                        isAvailableData = true;
                    } else {
                        isAvailableData = false;
                    }
                }
            } else {
                isAvailableData = false;
            }

            if (!isAvailableData) {
                txt_inputType.setText("Set 4 digit MPIN for authentication.");
            } else {
                txt_inputType.setText("Enter 4 digit MPIN");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void backSpacePressed() {
        Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibe.vibrate(50);
        if (passMain.getText().toString().trim().length() == 1) {
            radio1.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            radio2.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            radio3.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            radio4.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            passMain.setText("");
            StrPass = "";
        } else if (passMain.getText().toString().trim().length() == 2) {
            radio1.setBackground(getResources().getDrawable(R.drawable.circle_radio_filled));
            radio2.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            radio3.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            radio4.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            StrPass = passMain.getText().toString().trim();
            StrPass = StrPass.substring(0, StrPass.length() - 1);
            passMain.setText(StrPass);
        } else if (passMain.getText().toString().trim().length() == 3) {
            radio1.setBackground(getResources().getDrawable(R.drawable.circle_radio_filled));
            radio2.setBackground(getResources().getDrawable(R.drawable.circle_radio_filled));
            radio3.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            radio4.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            StrPass = passMain.getText().toString().trim();
            StrPass = StrPass.substring(0, StrPass.length() - 1);
            passMain.setText(StrPass);
        } else if (passMain.getText().toString().trim().length() == 4) {
            radio1.setBackground(getResources().getDrawable(R.drawable.circle_radio_filled));
            radio2.setBackground(getResources().getDrawable(R.drawable.circle_radio_filled));
            radio3.setBackground(getResources().getDrawable(R.drawable.circle_radio_filled));
            radio4.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            StrPass = passMain.getText().toString().trim();
            StrPass = StrPass.substring(0, StrPass.length() - 1);
            passMain.setText(StrPass);
        } else if (passMain.getText().toString().trim().length() > 4) {
            passMain.setText("");
            StrPass = "";
            radio1.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            radio2.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            radio3.setBackground(getResources().getDrawable(R.drawable.circle_radio));
            radio4.setBackground(getResources().getDrawable(R.drawable.circle_radio));
        }
    }
}
