package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Services.SyncActivityDetails;
import com.lpi.kmi.lpi.Services.SyncErrorLog;
import com.lpi.kmi.lpi.Services.SyncExamMaster;
import com.lpi.kmi.lpi.Services.SyncOfflineData;
import com.lpi.kmi.lpi.Services.SyncPostLicModuleData;
import com.lpi.kmi.lpi.activities.AsyncTasks.AsyncGetFAQ;
import com.lpi.kmi.lpi.activities.AsyncTasks.GetAtemptedExams;
import com.lpi.kmi.lpi.activities.AsyncTasks.syncExams;
import com.lpi.kmi.lpi.activities.AsyncTasks.syncFAQQueries;
import com.lpi.kmi.lpi.adapter.Adapters.HomeScreenBaseAdapter;
import com.lpi.kmi.lpi.adapter.GetterSetter.GetterSetter;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CircleImageView;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.stringcare.library.SC;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import java.io.File;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;

@SuppressLint("Range")
public class ParticipantHomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static ArrayList<GetterSetter> arrlst_Msg;
    public static ArrayList<String> arrlst_msgID = new ArrayList<String>();
    public static ArrayList<String> arrlst_msgHeader = new ArrayList<String>();
    public static ArrayList<String> arrlst_msgDesc = new ArrayList<String>();
    public static HomeScreenBaseAdapter custAdapter;
    public static Toolbar toolbar;
    ImageView fab;
    ListView MsgList;
    CoordinatorLayout coordinatorLayout = null;
    CircleImageView iv_profile;
    TextView tv_name;
//    InstallStateUpdatedListener installStateUpdatedListener=null;


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        /*if (mAppUpdateManager != null) {
            mAppUpdateManager.unregisterListener(installStateUpdatedListener);
        }*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (DatabaseHelper.db == null) {
                SQLiteDatabase.loadLibs(ParticipantHomeActivity.this);
                DatabaseHelper.db = DatabaseHelper.getInstance(ParticipantHomeActivity.this).
                        getWritableDatabase(SC.reveal(CommonClass.DBPassword));
            }
        } catch (Exception e) {
            Log.e("", "db error=" + e.toString());
        }
        if (StaticVariables.UserLoginId == null) {
            getUserDetails();
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        try {
            AppUpdateManager appUpdateManager;
            appUpdateManager = AppUpdateManagerFactory.create(getApplicationContext());
            Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();
            appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                    Intent intent = new Intent(ParticipantHomeActivity.this,Flexible.class);
                    startActivity(intent);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_participant);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBarSetup();
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.cordinatorLayout);
        MsgList = (ListView) findViewById(R.id.list_HomeMsgList);
        fab = (ImageView) findViewById(R.id.fab_start);

        arrlst_msgID.clear();
        arrlst_msgHeader.clear();
        arrlst_msgDesc.clear();

        arrlst_msgID.add("msg-0");
        arrlst_msgID.add("msg-1");
        arrlst_msgID.add("msg-2");
//        arrlst_msgID.add("msg-3");
//        arrlst_msgID.add("msg-4");
//        arrlst_msgID.add("msg-5");


        arrlst_msgHeader.add("" + getResources().getString(R.string.str_msgIntro));
//        arrlst_msgHeader.add("" + getResources().getString(R.string.str_msgadmstred));
        arrlst_msgHeader.add("" + getResources().getString(R.string.str_msgLEONARD));
        arrlst_msgHeader.add("Training");
//        arrlst_msgHeader.add("" + getResources().getString(R.string.str_msgPrsnalityTyps));
//        arrlst_msgHeader.add("" + getResources().getString(R.string.str_msgBnfts));


        arrlst_msgDesc.add("" + getResources().getString(R.string.BasicLPIinfo));
//        arrlst_msgDesc.add("" + getResources().getString(R.string.AdminLPIinfo));
        arrlst_msgDesc.add("" + getResources().getString(R.string.LEONARD));
//        arrlst_msgDesc.add("https://www.youtube.com/watch?v=MK50cGWG0VU");
        arrlst_msgDesc.add("https://www.youtube.com/embed/MK50cGWG0VU");
//        arrlst_msgDesc.add("" + getResources().getString(R.string.PersonalityTypes));
//        arrlst_msgDesc.add("" + getResources().getString(R.string.Benifits));
        try {
            if (DatabaseHelper.db == null) {
                SQLiteDatabase.loadLibs(ParticipantHomeActivity.this);
                DatabaseHelper.db = DatabaseHelper.getInstance(ParticipantHomeActivity.this).
                        getWritableDatabase(SC.reveal(CommonClass.DBPassword));
            }
        } catch (Exception e) {
            Log.e("", "db error=" + e.toString());
        }
        try {
            if (StaticVariables.UserLoginId == null) {
                getUserDetails();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        initDrawer();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ProgressDialog progressDialog = new ProgressDialog(ParticipantHomeActivity.this,
//                        R.style.AppTheme_Dark_Dialog);
//                progressDialog.setIndeterminate(true);
//                progressDialog.setMessage("Getting Things Ready...");
//                progressDialog.show();
                if (DatabaseHelper.db == null) {
                    SQLiteDatabase.loadLibs(ParticipantHomeActivity.this);
                    DatabaseHelper.db = DatabaseHelper.getInstance(ParticipantHomeActivity.this).
                            getWritableDatabase(SC.reveal(CommonClass.DBPassword));
                }

                String code = "";
//                code=DatabaseHelper.insertProdData(ParticipantHomeActivity.this,DatabaseHelper.db);
                code = DatabaseHelper.getActivityMaster(ParticipantHomeActivity.this, DatabaseHelper.db, "Welcome");
                DatabaseHelper.InsertActivityTracker(ParticipantHomeActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                        code, "", "Welcome", "", "",
                        "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "", "", "",
                        "", "", "Pending");
                if (CommonClass.isConnected(ParticipantHomeActivity.this)) {
                    Cursor cursor = DatabaseHelper.GetXMLData(getApplicationContext(), DatabaseHelper.db);
                    if (cursor.getCount() > 0) {
                        custom_ResultRemrk();
                    } else {
                        gotoMenu();
                    }
                    cursor.close();
                } else {
                    gotoMenu();
                }
//                progressDialog.dismiss();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

//        boolean isPostLicDataAvailable= DatabaseHelper.IsPostLicDataAvailable(this, DatabaseHelper.db);
//        if (!isPostLicDataAvailable){
//            Intent intent = new Intent(this, SyncPostLicModuleData.class);
//            this.startService(intent);
//        }

        custList();

        try {
            int attemptId = DatabaseHelper.CheckAttempts(ParticipantHomeActivity.this,
                    DatabaseHelper.db, "4001", StaticVariables.UserLoginId);
            int count = DatabaseHelper.PPExamQueCount(ParticipantHomeActivity.this, DatabaseHelper.db, "4001");
            if (attemptId <= 0 && count >= 20) {
//                alertCustomDailog("Complete Profiling", "Your Psychometric Profiling is pending, Please proceed for Psychometric Profiling.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();
    }

    private void initDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        iv_profile = (CircleImageView) header.findViewById(R.id.iv_profile);
        tv_name = (TextView) header.findViewById(R.id.tv_name);
        tv_name.setText(StaticVariables.UserName);
        iv_profile.setImageDrawable(getResources().getDrawable(R.drawable.profile_blank_m));
        setProfileData();
    }

    void setProfileData() {
        try {
            tv_name.setText(StaticVariables.UserName);
            if ((StaticVariables.profileBitmap != null) && !StaticVariables.profileBitmap.equals("null") && !StaticVariables.profileBitmap.equals("")) {
//                InputStream inputStream = new ByteArrayInputStream(StaticVariables.profileBitmap);
//                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                Bitmap bitmap = BitmapFactory.decodeByteArray(StaticVariables.profileBitmap, 0, StaticVariables.profileBitmap.length);
                if (bitmap == null) {
                    iv_profile.setImageDrawable(ParticipantHomeActivity.this.getResources().getDrawable(R.drawable.profile_blank_m));
                } else {
                    Glide.with(this)
                            .load(bitmap)
                            .into(iv_profile);
//                    iv_profile.setImageBitmap(bitmap);
                }
            } else {
                iv_profile.setImageDrawable(ParticipantHomeActivity.this.getResources().getDrawable(R.drawable.profile_blank_m));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("Range")
    private void getUserDetails() {

        try {
            android.database.Cursor mCursor = DatabaseHelper.db.query(getString(R.string.TBL_iCandidate), null,
                    null, null, null, null, null, "1");
            if (mCursor.getCount() > 0) {

                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    StaticVariables.crnt_InscType = "";
                    StaticVariables.isLogin = true;
                    StaticVariables.UserEmail = mCursor.getString(mCursor.getColumnIndex("CandidateEmailId"));
                    StaticVariables.UserName = mCursor.getString(mCursor.getColumnIndex("FirstName"));
                    StaticVariables.UserLoginId = mCursor.getString(mCursor.getColumnIndex("CandidateLoginId"));
                    StaticVariables.ParentId = mCursor.getString(mCursor.getColumnIndex("ParentId"));
                    StaticVariables.ParentName = mCursor.getString(mCursor.getColumnIndex("ParentName"));
                    StaticVariables.UserMobileNo = mCursor.getString(mCursor.getColumnIndex("CandidateMobileNo1"));
                    StaticVariables.offlineMode = "N";
                    StaticVariables.AppLink = mCursor.getString(mCursor.getColumnIndex("AppLink"));
                    StaticVariables.UserType = mCursor.getString(mCursor.getColumnIndex("UserType"));
                    StaticVariables.profileBitmap = mCursor.getBlob(mCursor.getColumnIndex("Image"));
                }
                mCursor.close();
                if (StaticVariables.UserType.trim().equalsIgnoreCase("T")) {
                    Intent intent = new Intent(ParticipantHomeActivity.this, LPI_TrainerMenuActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
                setProfileData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void alertCustomDailog(String title, String message) {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(ParticipantHomeActivity.this).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_exit_dialog, null);
            TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            Button btn_ok = (Button) dialogView.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialogView.findViewById(R.id.btn_no);
            promt_title.setText(title);
            txt_message.setText(message);

            if (title.equalsIgnoreCase("Clear App Data!")) {
                btn_ok.setText("YES");
                btn_no.setText("No");
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        boolean tablesDroped = CommonClass.onDropCustomTables(DatabaseHelper.db, ParticipantHomeActivity.this);
                        boolean tablesCreated = false;
                        if (tablesDroped) {
                            tablesCreated = CommonClass.onCreateCustomTables(DatabaseHelper.db, ParticipantHomeActivity.this);
                        }
//                        CommonClass.DeleteFileFolders(new File(context.getExternalFilesDir(Environment.MEDIA_SHARED) + "/Documents" + StaticVariables.LessonStoragePath));
                        CommonClass.DeleteFileFolders(new File(ParticipantHomeActivity.this.getExternalFilesDir(Environment.MEDIA_SHARED) + StaticVariables.LessonStoragePath));
                        try {
                            if (tablesCreated && tablesCreated) {
                                CommonClass.checkSettings(ParticipantHomeActivity.this, "C");
                                if (StaticVariables.SyncMobile.equals("Yes")
                                        && CommonClass.networkType(ParticipantHomeActivity.this).equals("MobileData")) {
                                    alertCustomDailog("Data Cleared!", "App's data is cleared, You are connected to mobile data, Do you want continue syncing?");
                                } else if (StaticVariables.SyncWIFI.equals("Yes")
                                        && CommonClass.networkType(ParticipantHomeActivity.this).equals("WiFi")) {
                                    syncServices(ParticipantHomeActivity.this);
                                } else if (CommonClass.isConnected(ParticipantHomeActivity.this)) {
                                    syncServices(ParticipantHomeActivity.this);
                                } else {
                                    alertCustomDailog("No Internet!", "You are not connected to internet");
                                }
                            } else {
                                Toast.makeText(ParticipantHomeActivity.this, "Something went wrong!", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

            } else if (title.equalsIgnoreCase("Exit!")) {
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        ParticipantHomeActivity.this.finish();
                        System.gc();
                        System.exit(0);
//                        boolean isAvailableData = false;
//                        Cursor mCursor = DatabaseHelper.Get_mPINCursor(ParticipantHomeActivity.this,
//                                DatabaseHelper.db, StaticVariables.UserLoginId);
//                        if (mCursor.getCount() > 0) {
//                            for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
//                                String isPIN = mCursor.getString(mCursor.getColumnIndex("is_mPIN_Data"));
//                                if (isPIN != null && isPIN.trim().equalsIgnoreCase("Y")) {
//                                    isAvailableData = true;
//                                } else {
//                                    isAvailableData = false;
//                                }
//                            }
//                        }
//                        if (isAvailableData) {
//                            Intent i = new Intent(getApplicationContext(), PIN_AuthActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            i.putExtra("user_id", StaticVariables.UserLoginId);
//                            startActivity(i);
//                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                            ParticipantHomeActivity.this.finish();
//
//                        } else {
//                            Intent i = new Intent(getApplicationContext(), Login.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(i);
//                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                            ParticipantHomeActivity.this.finish();
//                        }
                    }
                });
            } else if (title.equalsIgnoreCase("Logout!")) {
                btn_ok.setText("YES");
                btn_no.setText("No");
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        boolean tablesDroped = CommonClass.onDropCustomTables(DatabaseHelper.db, ParticipantHomeActivity.this);
                        boolean tablesCreated = false;
                        if (tablesDroped) {
                            tablesCreated = CommonClass.onCreateCustomTables(DatabaseHelper.db, ParticipantHomeActivity.this);
                        }
                        if (tablesCreated && tablesCreated) {
                            DatabaseHelper.db.delete(getResources().getString(R.string.TBL_iCandidate), null, null);
                        }
//                        CommonClass.DeleteFileFolders(new File(context.getExternalFilesDir(Environment.MEDIA_SHARED) + "/Documents" + StaticVariables.LessonStoragePath));
                        CommonClass.DeleteFileFolders(new File(ParticipantHomeActivity.this.getExternalFilesDir(Environment.MEDIA_SHARED) + StaticVariables.LessonStoragePath));
                        System.gc();
                        Intent i = new Intent(getApplicationContext(), Login.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        ParticipantHomeActivity.this.finish();
                    }
                });
            } else if (title.equalsIgnoreCase("Complete Profiling")) {
//                btn_no.setVisibility(View.GONE);
                btn_ok.setText("Proceed");
                btn_no.setText("Not Now");
                btn_no.setVisibility(View.VISIBLE);
//                btn_no.setPadding(5, 0, 5, 0);
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        StaticVariables.TestPage_flag = "PsychometricTest";
                        StaticVariables.crnt_QueId = 401;
                        StaticVariables.TblNameForCount = 20;
                        StaticVariables.crnt_ExmId = BigInteger.valueOf(4001);
                        StaticVariables.crnt_ExmTypeName = "Psychometric Profiling";
                        StaticVariables.crnt_ExmName = "Psychometric Profiling";
                        StaticVariables.crnt_ExamType = 4;
                        StaticVariables.crnt_ExamDuration = "30";
                        StaticVariables.IsPracticeExam = "N";
                        StaticVariables.IsAllQuesMendatory = "Y";
                        StaticVariables.allowedAttempts = "1";

                        Intent i = new Intent(ParticipantHomeActivity.this, MCQ_QuestionActivity.class);
                        i.putExtra("initFrom", "Custom_GridAdapter");
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);

                    }
                });

            } else if (title.equalsIgnoreCase("Mobile Data!")) {
                btn_ok.setText("YES");
                btn_no.setText("Not Now");
                btn_no.setPadding(5, 0, 5, 0);
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        syncServices(ParticipantHomeActivity.this);
                    }
                });
                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
            } else if (title.equalsIgnoreCase("Data Cleared!")) {
                btn_ok.setText("YES");
                btn_no.setText("Not Now");
                btn_no.setPadding(5, 0, 5, 0);
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        syncServices(ParticipantHomeActivity.this);
                    }
                });
            } else {
                btn_ok.setText("OK");
                btn_no.setVisibility(View.GONE);
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        syncServices(ParticipantHomeActivity.this);
                    }
                });
            }
            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });

            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void gotoMenu() {
        if (StaticVariables.UserType.trim().equalsIgnoreCase("C")) {
            Intent i = new Intent(getApplicationContext(), ParticipantMenuActivity.class);
            startActivity(i);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            ParticipantHomeActivity.this.finish();
        } else {
            Intent intent = new Intent(ParticipantHomeActivity.this, LPI_TrainerMenuActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            ParticipantHomeActivity.this.finish();
        }
    }


    public void custList() {
        try {
            arrlst_Msg = getLstData();
            custAdapter = new HomeScreenBaseAdapter(this, arrlst_Msg);
            MsgList.setAdapter(custAdapter);
            int index = MsgList.getFirstVisiblePosition();
            View v = MsgList.getChildAt(0);
            int top = (v == null) ? 0 : v.getTop();
            MsgList.setSelectionFromTop(index, top);
        } catch (Exception e) {
            Log.e("Tag", "cust error=" + e.toString());
        }

    }

    private ArrayList<GetterSetter> getLstData() {
        // TODO Auto-generated method stub
        ArrayList<GetterSetter> arrlst = new ArrayList<GetterSetter>();
        for (int i = 0; i < arrlst_msgHeader.size(); i++) {
            try {
                GetterSetter candi = new GetterSetter(arrlst_msgID.get(i), arrlst_msgHeader.get(i), arrlst_msgDesc.get(i));
                arrlst.add(candi);
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }
        return arrlst;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
//            Logout();
            alertCustomDailog("Exit!", "Are you sure, you want to exit?");
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().setTitle("LPI Respondent");
            getSupportActionBar().setSubtitle("Welcome " + StaticVariables.UserName);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.participant, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_UpdateProfile) {
            Intent intent = new Intent(ParticipantHomeActivity.this, LPI_RegistrationActivity.class);
            intent.putExtra("initFrom", "ParticipantHomeActivity");
            intent.putExtra("candidate_id", StaticVariables.UserLoginId);
            intent.putExtra("mobile_rec_id", "");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            // Handle the camera action
        } /*else if (id == R.id.nav_registrationProfile) {
            Intent intent = new Intent(ParticipantHomeActivity.this, LPI_TrainerMenuActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }*/ else if (id == R.id.nav_sync) {
//            if (CommonClass.isConnected(ParticipantHomeActivity.this)) {
//                syncServices(ParticipantHomeActivity.this);
////                new GetHtmlContent(ParticipantHomeActivity.this, "ParticipantHomeActivity").execute();
//                new syncExams(ParticipantHomeActivity.this, "", StaticVariables.UserLoginId).execute();
//            } else {
//                Toast.makeText(ParticipantHomeActivity.this, "No internet connectivity", Toast.LENGTH_SHORT);
//            }
            CommonClass.checkSettings(ParticipantHomeActivity.this, "C");
            if (StaticVariables.SyncMobile.equals("Yes")
                    && CommonClass.networkType(ParticipantHomeActivity.this).equals("MobileData")) {
                alertCustomDailog("Mobile Data!", "You are connected to mobile data, Do you want continue syncing?");
            } else if (StaticVariables.SyncWIFI.equals("Yes")
                    && CommonClass.networkType(ParticipantHomeActivity.this).equals("WiFi")) {
                syncServices(ParticipantHomeActivity.this);
            } else {
                syncServices(ParticipantHomeActivity.this);
            }
        } else if (id == R.id.nav_share) {
//            if (DatabaseHelper.GetReferCount(ParticipantHomeActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId) >= 3) {
//                showCustomAlert();
//            } else {
            Intent intent = new Intent(ParticipantHomeActivity.this, LPI_ShareAppActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("initFrom", "ParticipantHomeActivity");
            intent.putExtra("candidate_id", "");
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            ParticipantHomeActivity.this.finish();
//            }
        } else if (id == R.id.nav_clearData) {
            alertCustomDailog("Clear App Data!", "Are you sure, you want to clear apps data?");
        } else if (id == R.id.nav_logout) {
//            Logout();
            alertCustomDailog("Logout!", "Are you sure, you want to logout?");
        } else if (id == R.id.nav_Exit) {
//            Logout();
            alertCustomDailog("Exit!", "Are you sure, you want to exit?");
        } else if (id == R.id.nav_setting) {
            Intent intent = new Intent(ParticipantHomeActivity.this, SettingActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            ParticipantHomeActivity.this.finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showCustomAlert() {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(ParticipantHomeActivity.this).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_ok_dialog, null);
            TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            Button btn_ok = (Button) dialogView.findViewById(R.id.btn_yes);

            promt_title.setText("Refer App");
            txt_message.setText("Please contact your Trainer for upgrading the services.");
            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String mobref = UUID.randomUUID().toString();
                    alertDialog.dismiss();
                    Cursor mCursor = DatabaseHelper.getFAQListRefDetails(ParticipantHomeActivity.this, DatabaseHelper.db,
                            StaticVariables.UserLoginId, StaticVariables.UserType, "How can I upgrade to premium version to share the app?");
                    if (mCursor == null || mCursor.getCount() == 0) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        DatabaseHelper.SaveFAQList(ParticipantHomeActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId.trim(),
                                "", "" + StaticVariables.ParentId,
                                "How can I upgrade to premium version to share the app?", "C",
                                StaticVariables.sdf.format(Calendar.getInstance().getTime()), StaticVariables.UserLoginId,
                                "N", StaticVariables.UserName,
                                StaticVariables.UserMobileNo, null, "", mobref, simpleDateFormat.format(Calendar.getInstance().getTime()), "Pending");
                    }
                    if (CommonClass.isConnected(ParticipantHomeActivity.this)) {
                        new syncFAQQueries(new Callback<String>() {
                            @Override
                            public void execute(String result, String status) {

                            }
                        }, ParticipantHomeActivity.this, "How can I upgrade to premium version to share the app?", mobref).execute();
                    }
                }
            });

            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Logout() {
        AlertDialog.Builder builder = CommonClass.DialogOpen(ParticipantHomeActivity.this, "Logout!", "Are you sure, you want to logout?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                boolean isAvailableData = false;
                Cursor mCursor = DatabaseHelper.Get_mPINCursor(ParticipantHomeActivity.this,
                        DatabaseHelper.db, StaticVariables.UserLoginId);
                if (mCursor.getCount() > 0) {
                    for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                        String isPIN = mCursor.getString(mCursor.getColumnIndex("is_mPIN_Data"));
                        if (isPIN != null && isPIN.trim().equalsIgnoreCase("Y")) {
                            isAvailableData = true;
                        } else {
                            isAvailableData = false;
                        }
                    }
                }
                if (isAvailableData) {
                    Intent i = new Intent(getApplicationContext(), PIN_AuthActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra("user_id", StaticVariables.UserLoginId);
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    ParticipantHomeActivity.this.finish();

                } else {
                    Intent i = new Intent(getApplicationContext(), Login.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    ParticipantHomeActivity.this.finish();
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void syncServices(Context context) {

        if (CommonClass.isConnected(ParticipantHomeActivity.this)) {
//            syncServices(ParticipantHomeActivity.this);
            new syncExams(new Callback<String>() {
                @Override
                public void execute(String result, String status) {
                    new GetAtemptedExams(ParticipantHomeActivity.this, StaticVariables.UserLoginId
                            , new Callback() {
                        @Override
                        public void execute(Object result, String status) {
                            new AsyncGetFAQ(new Callback<String>() {
                                @Override
                                public void execute(String result, String status) {

                                }
                            }, ParticipantHomeActivity.this, "", StaticVariables.UserLoginId, StaticVariables.ParentId, StaticVariables.UserType).execute();
                        }
                    }).execute();
                }
            }, ParticipantHomeActivity.this, "ParticipantHomeActivity", StaticVariables.UserLoginId, "").execute();
        }

        if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncExamMaster")) {
            Intent newIntent = new Intent(context.getApplicationContext(), SyncExamMaster.class);
//            context.getApplicationContext().startService(newIntent);
            CommonClass.StartService(getApplicationContext(), newIntent);
        }
        if (CommonClass.networkType(context).equals("WiFi")) {
//        if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncFAQModuleData")) {
//            Intent newIntent = new Intent(context.getApplicationContext(), SyncFAQModuleData.class);
//            context.getApplicationContext().startService(newIntent);
//        }

            if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncActivityDetails")) {
                Intent trackIntent = new Intent(context.getApplicationContext(), SyncActivityDetails.class);
//                context.getApplicationContext().startService(trackIntent);
                CommonClass.StartService(getApplicationContext(), trackIntent);
            }

            if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncErrorLog")) {
                Intent newIntent = new Intent(context.getApplicationContext(), SyncErrorLog.class);
//                context.getApplicationContext().startService(newIntent);
                CommonClass.StartService(getApplicationContext(), newIntent);
            }


            if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncOfflineData")) {
                Intent newIntent = new Intent(context.getApplicationContext(), SyncOfflineData.class);
//                context.getApplicationContext().startService(newIntent);
                CommonClass.StartService(getApplicationContext(), newIntent);
            }
            if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncPostLicModuleData")) {
                Intent intent = new Intent(getApplicationContext(), SyncPostLicModuleData.class);
//                startService(intent);
                CommonClass.StartService(getApplicationContext(), intent);
            }
        } else {
        }
    }


    public void custom_ResultRemrk() {
        try {
            final AlertDialog alertD;
            LayoutInflater logout_inflator = LayoutInflater.from(ParticipantHomeActivity.this);
            final View openDialog = logout_inflator.inflate(R.layout.alert_ansalertdialog, null);
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ParticipantHomeActivity.this);
            alertDialogBuilder.setView(openDialog);
            alertD = alertDialogBuilder.create();
            alertD.setCancelable(false);
            alertD.show();

            final TextView alrtText_ResultRemark = (TextView) openDialog.findViewById(R.id.alrtText_ResultRemart);
            final TextView alrtText_CrrectResult = (TextView) openDialog.findViewById(R.id.alrtText_CrrectResult);
            final TextView Txt_AlertTitle = (TextView) openDialog.findViewById(R.id.Txt_AlertTitle);

            final Button alrtBtn_done = (Button) openDialog.findViewById(R.id.alrtbtn_Done);
            final Button alrtBtn_cncl = (Button) openDialog.findViewById(R.id.alrtbtn_cancle);
            alrtText_CrrectResult.setVisibility(View.GONE);

            Txt_AlertTitle.setText("Unsynced data");
            alrtText_ResultRemark.setText("Unsynced data is available,the data will be submitted in background, so can explore the while syncing.\n" +
                    "Would you like to sync with server?");
            alrtBtn_done.setText("Sync");
            alrtBtn_cncl.setText("Ask me later");

            alrtBtn_done.setVisibility(View.VISIBLE);
            alrtBtn_cncl.setVisibility(View.GONE);

            alrtBtn_done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertD.dismiss();
//                    syncServices(ParticipantHomeActivity.this);
                    if (!CommonClass.isMyServiceRunning(getApplicationContext(), "SyncOfflineData")) {
                        Intent newIntent = new Intent(getApplicationContext(), SyncOfflineData.class);
//                        getApplicationContext().startService(newIntent);
                        CommonClass.StartService(getApplicationContext(), newIntent);
                    }
                    gotoMenu();
                }
            });

            alrtBtn_cncl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertD.dismiss();
                    gotoMenu();
                }
            });
        } catch (Exception e) {
            Log.e("", "err=" + e.toString());
        }
    }
}
