package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Services.SyncActivityDetails;
import com.lpi.kmi.lpi.activities.AsyncTasks.AsyncSubmitExams;
import com.lpi.kmi.lpi.adapter.Adapters.Custom_GridAdapter;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.stringcare.library.SC;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

@SuppressLint("Range")
public class ParticipantMenuActivity extends AppCompatActivity {

    GridView gridview;

    CoordinatorLayout coordinatorLayout = null;
//"Pre licence Training",
//            "Training and Exam Schedule",
//            "Training and Exam Results",
//            "Appointment and Licence",
//            "Post Licence Training",

//    public static String[] osNameList = {
//            "Psychometric Profiling",
//            "View Personality Profile",
//            "Emotional Excellence Program",
//            "FAQs",
//            "News Feeds"
//    };
//    public static int[] osImages = {
//            R.mipmap.icons_anonymous_mask,
//            R.mipmap.icons_bar_chart,
//            R.mipmap.icons_mental_health,
//            R.mipmap.ic_faq,
//            R.mipmap.icons_rss
//    };

    public static String[] osNameList = {
            "Psychometric Profiling",
            "View Personality Profile",
            "LPI Lessons",
            "FAQs to your trainer",
            "Emotional Excellence Program",
            "News Feeds"
    };
    public static int[] osImages = {
            R.mipmap.ic_profiling,
            R.mipmap.icons_bar_chart,
            R.mipmap.ic_practice_lessons,
            R.mipmap.ic_faq,
            R.mipmap.icons_mental_health,
            R.mipmap.icons_rss
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_participant_menu);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_menu);
        setSupportActionBar(toolbar);
        actionBarSetup();

        try {
            if (!CommonClass.isMyServiceRunning(getApplicationContext(), "SyncActivityDetails")) {
                Intent trackIntent = new Intent(getApplicationContext(), SyncActivityDetails.class);
//                getApplicationContext().startService(trackIntent);
                CommonClass.StartService(getApplicationContext(), trackIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        gridview = (GridView) findViewById(R.id.customgrid);
        gridview.setAdapter(new Custom_GridAdapter(this, osNameList, osImages));
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.cordinatorLayout);

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().setTitle(R.string.app_name);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (DatabaseHelper.db == null) {
                SQLiteDatabase.loadLibs(ParticipantMenuActivity.this);
                DatabaseHelper.db = DatabaseHelper.getInstance(ParticipantMenuActivity.this).
                        getWritableDatabase(SC.reveal(CommonClass.DBPassword));
            }
        } catch (Exception e) {
            Log.e("", "db error=" + e.toString());
        }
        try {
            if (StaticVariables.UserLoginId == null) {
                getUserDetails();
            }
            if (coordinatorLayout == null) {
                coordinatorLayout = (CoordinatorLayout) findViewById(R.id.cordinatorLayout);
            }
            if (CommonClass.isConnected(ParticipantMenuActivity.this)) {
                Cursor cursor = DatabaseHelper.GetXMLData(getApplicationContext(), DatabaseHelper.db);
                if ((cursor != null) && (cursor.getCount() > 0) && (CommonClass.isConnected(ParticipantMenuActivity.this))) {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Offline Exam data available,\nPlease Sync to submit.", Snackbar.LENGTH_INDEFINITE)
                            .setAction("Sync Data", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (CommonClass.isConnected(ParticipantMenuActivity.this)) {
                                        new AsyncSubmitExams(ParticipantMenuActivity.this, "ParticipantMenuActivity").execute();
                                    } else {
                                        Toast.makeText(ParticipantMenuActivity.this, "No internet connectivity", Toast.LENGTH_SHORT);
                                    }
                                }
                            });

                    snackbar.show();
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getUserDetails() {

        try {
            android.database.Cursor mCursor = DatabaseHelper.db.query(getString(R.string.TBL_iCandidate), null,
                    null, null, null, null, null, "1");
            if (mCursor.getCount() > 0) {

                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    StaticVariables.crnt_InscType = "";
                    StaticVariables.isLogin = true;
                    StaticVariables.UserEmail = mCursor.getString(mCursor.getColumnIndex("CandidateEmailId"));
                    StaticVariables.UserName = mCursor.getString(mCursor.getColumnIndex("FirstName"));
                    StaticVariables.UserLoginId = mCursor.getString(mCursor.getColumnIndex("CandidateLoginId"));
                    StaticVariables.ParentId = mCursor.getString(mCursor.getColumnIndex("ParentId"));
                    StaticVariables.ParentName = mCursor.getString(mCursor.getColumnIndex("ParentName"));
                    StaticVariables.UserMobileNo = mCursor.getString(mCursor.getColumnIndex("CandidateMobileNo1"));
                    StaticVariables.offlineMode = "N";
                    StaticVariables.AppLink = mCursor.getString(mCursor.getColumnIndex("AppLink"));
                    StaticVariables.UserType = mCursor.getString(mCursor.getColumnIndex("UserType"));
                    StaticVariables.profileBitmap = mCursor.getBlob(mCursor.getColumnIndex("Image"));
                }
                mCursor.close();
                if (StaticVariables.UserType.trim().equalsIgnoreCase("T")) {
                    Intent intent = new Intent(ParticipantMenuActivity.this, LPI_TrainerMenuActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.participant, menu);
        return true;
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ParticipantMenuActivity.this, ParticipantHomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        ParticipantMenuActivity.this.finish();
    }
}
