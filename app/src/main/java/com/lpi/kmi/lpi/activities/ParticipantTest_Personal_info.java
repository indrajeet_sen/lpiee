package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.math.BigInteger;

@SuppressLint("Range")
public class ParticipantTest_Personal_info extends AppCompatActivity {

    MaterialBetterSpinner spin_Language;
    Button btn_NextToExam;
    Button btn_Next;

    EditText edt_UserName,edt_Organisation,edt_JD,edt_emailID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_participant_test);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.participant_toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setTitle("Personal information");

        btn_NextToExam = (Button) findViewById(R.id.btn_NextToExam);
//        btn_Next = (Button) findViewById(R.id.btn_Next);

        edt_UserName = (EditText) findViewById(R.id.edt_UserName);
        edt_Organisation = (EditText) findViewById(R.id.edt_Organisation);
        edt_JD = (EditText) findViewById(R.id.edt_JD);
        edt_emailID = (EditText) findViewById(R.id.edt_emailID);

        EditText_NullErr(edt_UserName);
        EditText_NullErr(edt_Organisation);
        EditText_NullErr(edt_JD);
        EditText_NullErr(edt_emailID);

        spin_Language= (MaterialBetterSpinner) findViewById(R.id.spin_Language);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(ParticipantTest_Personal_info.this,
                android.R.layout.simple_dropdown_item_1line,getResources().getStringArray(R.array.arr_language));
        spin_Language.setAdapter(arrayAdapter);

        btn_NextToExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StaticVariables.TestPage_flag.equals("PsychometricTest")){

//                    Intent i=new Intent(getApplicationContext(), QuestionActivity.class);
//                    startActivity(i);

                    if(edt_UserName.getText().toString().equals("")){
                        edt_UserName.setError("This field is required");
                    }
                    else if(edt_Organisation.getText().toString().equals("")){
                        edt_Organisation.setError("This field is required");
                    }
                    else if(edt_JD.getText().toString().equals("")){
                        edt_JD.setError("This field is required");
                    }
                    else if(spin_Language.getText().toString().equals("") || spin_Language.getText().toString().equals("Language")){
                        spin_Language.setError("This field is required");
                        Toast.makeText(ParticipantTest_Personal_info.this,"Language is missing..",Toast.LENGTH_LONG).show();
                    }
                    else if(edt_emailID.getText().toString().equals("")){
                        edt_emailID.setError("This field is required");
                    }
                    else if (!CommonClass.isValidEmail(edt_emailID.getText().toString())){
                        edt_emailID.setError("Invalid email ID");
                    }
                    else{
                        StaticVariables.crnt_QueId = 401;
//                        StaticVariables.QuestionId.clear();
//                        StaticVariables.QuestionId.add(401);
                        StaticVariables.crnt_ExmId= BigInteger.valueOf(4001);
                        StaticVariables.TblNameForCount= DatabaseHelper.TotalQueCount(ParticipantTest_Personal_info.this, DatabaseHelper.db,StaticVariables.crnt_ExmId);
//                        StaticVariables.crnt_ExmTypeName = DatabaseHelper.GetExamName(ParticipantTest_Personal_info.this, DatabaseHelper.db,StaticVariables.crnt_ExmId);;

                        Intent i =new Intent(ParticipantTest_Personal_info.this, MCQ_QuestionActivity.class);
                        i.putExtra("initFrom","ParticipantTest");
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        ParticipantTest_Personal_info.this.finish();
                    }
                }
                else{
                    Intent i=new Intent(getApplicationContext(), ExamList_Activity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    ParticipantTest_Personal_info.this.finish();
                }

            }
        });

    }

    private void EditText_NullErr(final EditText edt){

        edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                edt.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
