package com.lpi.kmi.lpi.activities.PublicUser;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.lpi.kmi.lpi.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mustafakachwalla on 08/05/17.
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List<ExpandedMenuModel> mListDataHeader; // header titles

    // child data in format of header title, child title
    private HashMap<ExpandedMenuModel, List<String>> mListDataChild;
    ExpandableListView expandList;

    public ExpandableListAdapter(Context context, List<ExpandedMenuModel> listDataHeader, HashMap<ExpandedMenuModel, List<String>> listChildData, ExpandableListView mView) {
        this.mContext = context;
        this.mListDataHeader = listDataHeader;
        this.mListDataChild = listChildData;
        this.expandList = mView;
    }

    @Override
    public int getGroupCount() {
        int i = mListDataHeader.size();
        return this.mListDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int childCount = 0;
        if (groupPosition == 1) {
            childCount = this.mListDataChild.get(this.mListDataHeader.get(groupPosition))
                    .size();
        }
        return childCount;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.mListDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.mListDataChild.get(this.mListDataHeader.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ExpandedMenuModel headerTitle = (ExpandedMenuModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.publicnav_listheader, null);
        }
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.submenu);
        ImageView headerIcon = (ImageView) convertView.findViewById(R.id.iconimage);
        ImageView NavIcon = (ImageView) convertView.findViewById(R.id.pnav_icon);
        lblListHeader.setTypeface(null, Typeface.NORMAL);
        lblListHeader.setText(headerTitle.getIconName());

        if(lblListHeader.getText().equals("Home"))
            NavIcon.setImageResource(R.drawable.ic_nav_home);
        else if(lblListHeader.getText().equals("Background"))
            NavIcon.setImageResource(R.drawable.ic_nav_background);
        else if(lblListHeader.getText().equals("News Articles"))
            NavIcon.setImageResource(R.drawable.ic_nav_newarticle);
        else if(lblListHeader.getText().equals("About Author"))
            NavIcon.setImageResource(R.drawable.ic_nav_author);
        else if(lblListHeader.getText().equals("Help"))
            NavIcon.setImageResource(R.drawable.ic_nav_help);
        else if(lblListHeader.getText().equals("Disclaimer"))
            NavIcon.setImageResource(R.drawable.ic_nav_disclaimer);
        else if(lblListHeader.getText().equals("Privacy"))
            NavIcon.setImageResource(R.drawable.ic_nav_privacy);
        else if(lblListHeader.getText().equals("Login"))
            NavIcon.setImageResource(R.drawable.ic_login_gyra);
        else if(lblListHeader.getText().equals("Feedback"))
            NavIcon.setImageResource(R.drawable.ic_nav_feedback);

        if (isExpanded && lblListHeader.getText().equals("Background")) {
            headerIcon.setImageResource(R.drawable.ic_collapse);
        } else if ((!isExpanded) && lblListHeader.getText().equals("Background")) {
            headerIcon.setImageResource(R.drawable.ic_expand);
        }
//        headerIcon.setImageResource(headerTitle.getIconImg());
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.publicnav_list_submenu, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.submenu);

        ImageView imgListChild = (ImageView) convertView
                .findViewById(R.id.nav_subicon);

        txtListChild.setText(childText);

        if(txtListChild.getText().equals("What is LPI about?"))
            imgListChild.setImageResource(R.drawable.ic_nav_about_lpi);
        else if(txtListChild.getText().equals("What is EQ?"))
            imgListChild.setImageResource(R.drawable.ic_nav_about_eq);
        else if(txtListChild.getText().equals("EQ in the workplace"))
            imgListChild.setImageResource(R.drawable.ic_nav_eqworkplace);
        else if(txtListChild.getText().equals("EQ success factor"))
            imgListChild.setImageResource(R.drawable.ic_nav_eq_success);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
