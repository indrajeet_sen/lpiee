package com.lpi.kmi.lpi.activities.PublicUser;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.Login;
import com.lpi.kmi.lpi.adapter.Adapters.Custom_GridAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PublicHomeScrnActivity extends AppCompatActivity implements View.OnClickListener
        , NavigationView.OnNavigationItemSelectedListener {

//    public static SQLiteDatabase db;
    private DrawerLayout mDrawerLayout;
    ExpandableListAdapter mMenuAdapter;
    ExpandableListView expandableList;
    List<ExpandedMenuModel> listDataHeader;
    HashMap<ExpandedMenuModel, List<String>> listDataChild;

    RelativeLayout rel_BookInfo,rel_BookMsgDesc;
    RelativeLayout rel_abtLpiInfo, rel_abtLpiDesc;
    RelativeLayout rel_sccsLpiInfo,rel_sccsLpiDesc;
    RelativeLayout rel_PrpseLpiInfo,rel_PrpseLpiDesc;
    ImageView img_BookMsgDrpdwn,img_abtLpiDrpdwn,img_sccsLpiDrpdwn,img_PrpseLpiDrpdwn;

    GridView grid_lpinews;

    ScrollView menu_home,menu_LPIabout,menu_LPIEQ,menu_EQWrkplce,menu_EQSuccssFctr,menu_Author,menu_Disclaimer,menu_Privacy;
    RelativeLayout menu_LPINews;

    public String[] osNameList = {
            "Kosmo, 4th Sept 2004",
            "Sin Chew, 2 Oct 2007",
            "Sin Chew, 3 Aug 2007",
            "The Star, 7 May 2007",
            "The Star, 15 Sept 2004",
            "Sin Chew, 6 May 2007",
            "The Star, 8 Oct 2007",
            "The Star, 8 Oct 2007"
    };
    public int[] osImages = {
            R.drawable.img_lpinews_kosmo040904_small,
            R.drawable.img_lpinews_sinchew021007_small,
            R.drawable.img_lpinews_sinchew030807_small,
            R.drawable.img_lpinews_thestar070507_small,
            R.drawable.img_lpinews_thestar150904_small,
            R.drawable.img_lpinews_sinchew060507_small,
            R.drawable.img_lpinews_thestar081007_small,
            R.drawable.img_lpinews_thestar081007full_small
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_public_home_scrn);
        Toolbar toolbar = (Toolbar) findViewById(R.id.Hometoolbar);
        setSupportActionBar(toolbar);
        actionBarSetup();

//        SQLiteDatabase.loadLibs(PublicHomeScrnActivity.this);
//        db = DatabaseHelper.getInstance(PublicHomeScrnActivity.this).
//                getWritableDatabase(getResources().getString(R.string.DBPassword));

        // Menu Layouts
        menu_home = (ScrollView) findViewById(R.id.menu_home);
        menu_LPIabout = (ScrollView) findViewById(R.id.menu_LPIabout);
        menu_LPIEQ = (ScrollView) findViewById(R.id.menu_LPIEQ);
        menu_EQWrkplce = (ScrollView) findViewById(R.id.menu_EQWrkplce);
        menu_EQSuccssFctr = (ScrollView) findViewById(R.id.menu_EQSuccssFctr);
        menu_Author = (ScrollView) findViewById(R.id.menu_Author);
        menu_Disclaimer = (ScrollView) findViewById(R.id.menu_Disclaimer);
        menu_Privacy = (ScrollView) findViewById(R.id.menu_Privacy);
        menu_LPINews = (RelativeLayout) findViewById(R.id.menu_LPINews);

        rel_BookInfo = (RelativeLayout) findViewById(R.id.rel_BookInfo);
        rel_BookMsgDesc = (RelativeLayout) findViewById(R.id.rel_BookMsgDesc);
        rel_abtLpiInfo = (RelativeLayout) findViewById(R.id.rel_abtLpiInfo);
        rel_abtLpiDesc = (RelativeLayout) findViewById(R.id.rel_abtLpiDesc);
        rel_sccsLpiInfo = (RelativeLayout) findViewById(R.id.rel_sccsLpiInfo);
        rel_sccsLpiDesc = (RelativeLayout) findViewById(R.id.rel_sccsLpiDesc);
        rel_PrpseLpiInfo = (RelativeLayout) findViewById(R.id.rel_PrpseLpiInfo);
        rel_PrpseLpiDesc = (RelativeLayout) findViewById(R.id.rel_PrpseLpiDesc);

        img_BookMsgDrpdwn = (ImageView) findViewById(R.id.img_BookMsgDrpdwn);
        img_abtLpiDrpdwn = (ImageView) findViewById(R.id.img_abtLpiDrpdwn);
        img_sccsLpiDrpdwn = (ImageView) findViewById(R.id.img_sccsLpiDrpdwn);
        img_PrpseLpiDrpdwn = (ImageView) findViewById(R.id.img_PrpseLpiDrpdwn);

        grid_lpinews = (GridView) findViewById(R.id.grid_lpinews);
        grid_lpinews.setAdapter(new Custom_GridAdapter(this, osNameList, osImages));

        rel_BookMsgDesc.setVisibility(View.VISIBLE);
        img_BookMsgDrpdwn.setImageResource(R.mipmap.ic_dropup_png1);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_publicnav);
        expandableList = (ExpandableListView) findViewById(R.id.navigationmenu);

        rel_BookInfo.setOnClickListener(this);
        rel_abtLpiInfo.setOnClickListener(this);
        rel_sccsLpiInfo.setOnClickListener(this);
        rel_PrpseLpiInfo.setOnClickListener(this);

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Login_intnt= new Intent(getApplicationContext(), Login.class);
                startActivity(Login_intnt);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                PublicHomeScrnActivity.this.finish();
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_publicnav);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        prepareListData();
        mMenuAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild, expandableList);

        // setting list adapter
        expandableList.setAdapter(mMenuAdapter);



        expandableList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
                intialMenuVisibility(Integer.parseInt("10"+i1));
                mDrawerLayout.closeDrawer(GravityCompat.START);
                return false;
            }
        });
        expandableList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                //Log.d("DEBUG", "heading clicked");
                intialMenuVisibility(i);
                if (i!=1)
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                return false;
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().setTitle(R.string.app_name);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        menu_home.setVisibility(View.VISIBLE);
        menu_Author.setVisibility(View.GONE);
        menu_Disclaimer.setVisibility(View.GONE);
        menu_EQSuccssFctr.setVisibility(View.GONE);
        menu_EQWrkplce.setVisibility(View.GONE);
        menu_LPIabout.setVisibility(View.GONE);
        menu_LPIEQ.setVisibility(View.GONE);
        menu_LPINews.setVisibility(View.GONE);
        menu_Privacy.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_publicnav);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.public_home_scrn, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<ExpandedMenuModel>();
        listDataChild = new HashMap<ExpandedMenuModel, List<String>>();

        ExpandedMenuModel item1 = new ExpandedMenuModel();
        item1.setIconName("Home");
        listDataHeader.add(item1);

        ExpandedMenuModel item2 = new ExpandedMenuModel();
        item2.setIconName("Background");
        listDataHeader.add(item2);

        ExpandedMenuModel item3 = new ExpandedMenuModel();
        item3.setIconName("News Articles");
        listDataHeader.add(item3);

        ExpandedMenuModel item4 = new ExpandedMenuModel();
        item4.setIconName("About Author");
        listDataHeader.add(item4);

        ExpandedMenuModel item5 = new ExpandedMenuModel();
        item5.setIconName("Help");
        listDataHeader.add(item5);

        ExpandedMenuModel item6 = new ExpandedMenuModel();
        item6.setIconName("Disclaimer");
        listDataHeader.add(item6);

        ExpandedMenuModel item7 = new ExpandedMenuModel();
        item7.setIconName("Privacy");
        listDataHeader.add(item7);

        ExpandedMenuModel item8 = new ExpandedMenuModel();
        item8.setIconName("Login");
        listDataHeader.add(item8);

        ExpandedMenuModel item9 = new ExpandedMenuModel();
        item9.setIconName("Feedback");
        listDataHeader.add(item9);

        List<String> heading2 = new ArrayList<String>();
        heading2.add("What is LPI about?");
        heading2.add("What is EQ?");
        heading2.add("EQ in the workplace");
        heading2.add("EQ success factor");

        listDataChild.put(listDataHeader.get(1), heading2);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        //revision: this don't works, use setOnChildClickListener() and setOnGroupClickListener() above instead
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        return true;
                    }
                });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.rel_BookInfo:
                setVisibility(rel_BookMsgDesc, img_BookMsgDrpdwn);
                break;

            case R.id.rel_abtLpiInfo:
                setVisibility(rel_abtLpiDesc, img_abtLpiDrpdwn);
                break;

            case R.id.rel_sccsLpiInfo:
                setVisibility(rel_sccsLpiDesc, img_sccsLpiDrpdwn);
                break;

            case R.id.rel_PrpseLpiInfo:
                setVisibility(rel_PrpseLpiDesc, img_PrpseLpiDrpdwn);
                break;

        }
    }

    public void setVisibility(RelativeLayout rel, ImageView img){
        if (rel.getVisibility()==View.VISIBLE){
            rel.setVisibility(View.GONE);
            img.setImageResource(R.mipmap.ic_dropdown_png1);
        }
        else {
            rel.setVisibility(View.VISIBLE);
            img.setImageResource(R.mipmap.ic_dropup_png1);
        }
    }

    public void intialMenuVisibility( int menuIndex){

        switch (menuIndex){
            case 0:
                getSupportActionBar().setTitle("Home");
                menu_home.setVisibility(View.VISIBLE);
                menu_Author.setVisibility(View.GONE);
                menu_Disclaimer.setVisibility(View.GONE);
                menu_EQSuccssFctr.setVisibility(View.GONE);
                menu_EQWrkplce.setVisibility(View.GONE);
                menu_LPIabout.setVisibility(View.GONE);
                menu_LPIEQ.setVisibility(View.GONE);
                menu_LPINews.setVisibility(View.GONE);
                menu_Privacy.setVisibility(View.GONE);
                break;

            case 100:
                getSupportActionBar().setTitle("Background");
                menu_home.setVisibility(View.GONE);
                menu_Author.setVisibility(View.GONE);
                menu_Disclaimer.setVisibility(View.GONE);
                menu_EQSuccssFctr.setVisibility(View.GONE);
                menu_EQWrkplce.setVisibility(View.GONE);
                menu_LPIabout.setVisibility(View.VISIBLE);
                menu_LPIEQ.setVisibility(View.GONE);
                menu_LPINews.setVisibility(View.GONE);
                menu_Privacy.setVisibility(View.GONE);
                break;

            case 101:
                getSupportActionBar().setTitle("Background");
                menu_home.setVisibility(View.GONE);
                menu_Author.setVisibility(View.GONE);
                menu_Disclaimer.setVisibility(View.GONE);
                menu_EQSuccssFctr.setVisibility(View.GONE);
                menu_EQWrkplce.setVisibility(View.GONE);
                menu_LPIabout.setVisibility(View.GONE);
                menu_LPIEQ.setVisibility(View.VISIBLE);
                menu_LPINews.setVisibility(View.GONE);
                menu_Privacy.setVisibility(View.GONE);
                break;

            case 102:
                getSupportActionBar().setTitle("Background");
                menu_home.setVisibility(View.GONE);
                menu_Author.setVisibility(View.GONE);
                menu_Disclaimer.setVisibility(View.GONE);
                menu_EQSuccssFctr.setVisibility(View.GONE);
                menu_EQWrkplce.setVisibility(View.VISIBLE);
                menu_LPIabout.setVisibility(View.GONE);
                menu_LPIEQ.setVisibility(View.GONE);
                menu_LPINews.setVisibility(View.GONE);
                menu_Privacy.setVisibility(View.GONE);
                break;

            case 103:
                getSupportActionBar().setTitle("Background");
                menu_home.setVisibility(View.GONE);
                menu_Author.setVisibility(View.GONE);
                menu_Disclaimer.setVisibility(View.GONE);
                menu_EQSuccssFctr.setVisibility(View.VISIBLE);
                menu_EQWrkplce.setVisibility(View.GONE);
                menu_LPIabout.setVisibility(View.GONE);
                menu_LPIEQ.setVisibility(View.GONE);
                menu_LPINews.setVisibility(View.GONE);
                menu_Privacy.setVisibility(View.GONE);
                break;

            case 2:
                getSupportActionBar().setTitle("News Articles");
                menu_home.setVisibility(View.GONE);
                menu_Author.setVisibility(View.GONE);
                menu_Disclaimer.setVisibility(View.GONE);
                menu_EQSuccssFctr.setVisibility(View.GONE);
                menu_EQWrkplce.setVisibility(View.GONE);
                menu_LPIabout.setVisibility(View.GONE);
                menu_LPIEQ.setVisibility(View.GONE);
                menu_LPINews.setVisibility(View.VISIBLE);
                menu_Privacy.setVisibility(View.GONE);
                break;



            case 3:
                getSupportActionBar().setTitle("About Author");
                menu_home.setVisibility(View.GONE);
                menu_Author.setVisibility(View.VISIBLE);
                menu_Disclaimer.setVisibility(View.GONE);
                menu_EQSuccssFctr.setVisibility(View.GONE);
                menu_EQWrkplce.setVisibility(View.GONE);
                menu_LPIabout.setVisibility(View.GONE);
                menu_LPIEQ.setVisibility(View.GONE);
                menu_LPINews.setVisibility(View.GONE);
                menu_Privacy.setVisibility(View.GONE);
                break;

            case 4:
                Toast.makeText(PublicHomeScrnActivity.this,"Work in progress",Toast.LENGTH_SHORT).show();
                break;

            case 5:
                getSupportActionBar().setTitle("Disclaimer");
                menu_home.setVisibility(View.GONE);
                menu_Author.setVisibility(View.GONE);
                menu_Disclaimer.setVisibility(View.VISIBLE);
                menu_EQSuccssFctr.setVisibility(View.GONE);
                menu_EQWrkplce.setVisibility(View.GONE);
                menu_LPIabout.setVisibility(View.GONE);
                menu_LPIEQ.setVisibility(View.GONE);
                menu_LPINews.setVisibility(View.GONE);
                menu_Privacy.setVisibility(View.GONE);
                break;

            case 6:
                getSupportActionBar().setTitle("Privacy");
                menu_home.setVisibility(View.GONE);
                menu_Author.setVisibility(View.GONE);
                menu_Disclaimer.setVisibility(View.GONE);
                menu_EQSuccssFctr.setVisibility(View.GONE);
                menu_EQWrkplce.setVisibility(View.GONE);
                menu_LPIabout.setVisibility(View.GONE);
                menu_LPIEQ.setVisibility(View.GONE);
                menu_LPINews.setVisibility(View.GONE);
                menu_Privacy.setVisibility(View.VISIBLE);
                break;

            case 7:
                Intent Login_intnt= new Intent(getApplicationContext(), Login.class);
                startActivity(Login_intnt);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                PublicHomeScrnActivity.this.finish();
                break;

            case 8:
                Toast.makeText(PublicHomeScrnActivity.this,"Work in progress",Toast.LENGTH_SHORT).show();
                break;
        }

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        return false;
    }
}
