package com.lpi.kmi.lpi.activities.PublicUser;

/**
 * Created by Bharat on 22/2/2018.
 */

public class SearchModel {


    private String name;

    private String username;

    private String mobile_no;

    private String user_id;

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    private String profileName;


    public String getTrainer_id() {
        return trainer_id;
    }

    public void setTrainer_id(String trainer_id) {
        this.trainer_id = trainer_id;
    }

    private String trainer_id;

    private String email_id;

    private String mobile_rec_id;

    public String getRespCount() {
        return respCount;
    }

    public void setRespCount(String respCount) {
        this.respCount = respCount;
    }

    private String respCount;


    public String getMobile_rec_id() {
        return mobile_rec_id;
    }

    public void setMobile_rec_id(String mobile_rec_id) {
        this.mobile_rec_id = mobile_rec_id;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    private byte[] image;

    private String user_picture;

    private String status;
    private String quesId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    public String getUser_picture() {
        return user_picture;
    }

    public void setUser_picture(String user_picture) {
        this.user_picture = user_picture;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getQuesId() {
        return quesId;
    }

    public void setQuesId(String quesId) {
        this.quesId = quesId;
    }
}
