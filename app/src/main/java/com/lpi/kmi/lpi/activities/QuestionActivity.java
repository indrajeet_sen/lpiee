package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.adapter.Adapters.QuestionBaseAdapter;
import com.lpi.kmi.lpi.adapter.GetterSetter.GetterSetter;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;

import java.util.ArrayList;

@SuppressLint("Range")
public class QuestionActivity extends AppCompatActivity {


    public static ArrayList<GetterSetter> arrlst_Doc;
    public static ArrayList<String> arrlst_SrNo = new ArrayList<String>();
    public static ArrayList<String> arrlst_que = new ArrayList<String>();
    public static QuestionBaseAdapter custAdapter;

    ListView list_question;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_question);

        list_question = (ListView) findViewById(R.id.list_question);


        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_Que);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setTitle("Questions");

        arrlst_que.clear();
        arrlst_SrNo.clear();

        arrlst_que.add("Likes to win");
        arrlst_que.add("Tries to live in harmony with others");
        arrlst_que.add("Think carefully without making a decision");
        arrlst_que.add("Is a fun person to be with");
        arrlst_que.add("Is known for comming up with new ideas");
        arrlst_que.add("Is cheerful");
        arrlst_que.add("Try hard not to hurt people's feeling");
        arrlst_que.add("Is careful");
        arrlst_que.add("Is innovative");
        arrlst_que.add("Is competitive");
        arrlst_que.add("Tends to be cautious");
        arrlst_que.add("Likes to live in peace with others");
        arrlst_que.add("Is creative");
        arrlst_que.add("Makes firends easily");
        arrlst_que.add("Desires to be in control");
        arrlst_que.add("Tries to think well of others");
        arrlst_que.add("Is confident about myself");
        arrlst_que.add("Prefers changes to be made only after careful planning");
        arrlst_que.add("Is social");
        arrlst_que.add("Likes to lead");


        arrlst_SrNo.add("1.");
        arrlst_SrNo.add("2.");
        arrlst_SrNo.add("3.");
        arrlst_SrNo.add("4.");
        arrlst_SrNo.add("5.");
        arrlst_SrNo.add("6.");
        arrlst_SrNo.add("7.");
        arrlst_SrNo.add("8.");
        arrlst_SrNo.add("9.");
        arrlst_SrNo.add("10.");
        arrlst_SrNo.add("11.");
        arrlst_SrNo.add("12.");
        arrlst_SrNo.add("13.");
        arrlst_SrNo.add("14.");
        arrlst_SrNo.add("15.");
        arrlst_SrNo.add("16.");
        arrlst_SrNo.add("17.");
        arrlst_SrNo.add("18.");
        arrlst_SrNo.add("19.");
        arrlst_SrNo.add("20.");

        custList();

    }

    public void custList() {
        try {
            arrlst_Doc = getLstData();
            custAdapter = new QuestionBaseAdapter(this, arrlst_Doc);
            list_question.setAdapter(custAdapter);
            int index = list_question.getFirstVisiblePosition();
            View v = list_question.getChildAt(0);
            int top = (v == null) ? 0 : v.getTop();
            list_question.setSelectionFromTop(index, top);
        } catch (Exception e) {
            Log.e("Tag", "cust error=" + e.toString());
            DatabaseHelper.SaveErroLog(QuestionActivity.this, DatabaseHelper.db,
                    "QuestionActivity", "custList()", e.toString());
        }

    }

    private ArrayList<GetterSetter> getLstData() {
        // TODO Auto-generated method stub
        ArrayList<GetterSetter> arrlst = new ArrayList<GetterSetter>();
        for (int i = 0; i < arrlst_SrNo.size(); i++) {
            try {
                GetterSetter candi = new GetterSetter(arrlst_SrNo.get(i), arrlst_que.get(i));
                arrlst.add(candi);
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
                DatabaseHelper.SaveErroLog(QuestionActivity.this, DatabaseHelper.db,
                        "QuestionActivity", "getLstData()", e.toString());
            }
        }
        return arrlst;
    }
}
