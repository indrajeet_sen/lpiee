package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Services.SyncActivityDetails;
import com.lpi.kmi.lpi.Services.SyncErrorLog;
import com.lpi.kmi.lpi.Services.SyncExamMaster;
import com.lpi.kmi.lpi.Services.SyncFAQModuleData;
import com.lpi.kmi.lpi.Services.SyncOfflineData;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.StaticVariables;

@SuppressLint("Range")
public class SettingActivity extends AppCompatActivity {

    Switch switch_wifiAutoSync, switch_mobileAutoSync, switch_notification;
    TextView text_logout, text_sync, curr_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_setting);

        switch_wifiAutoSync = (Switch) findViewById(R.id.switch_wifiAutoSync);
        switch_mobileAutoSync = (Switch) findViewById(R.id.switch_mobileAutoSync);
        switch_notification = (Switch) findViewById(R.id.switch_notification);

        text_logout = (TextView) findViewById(R.id.text_logout);
        text_sync = (TextView) findViewById(R.id.text_sync);
        curr_user = (TextView) findViewById(R.id.curr_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBarSetup();
        CommonClass.checkSettings(SettingActivity.this, "C");

        curr_user.setText("Current User : " + StaticVariables.UserName);
        if (StaticVariables.SyncWIFI.equals("Yes")) {
            switch_wifiAutoSync.setChecked(true);
        } else if (StaticVariables.SyncMobile.equals("Yes")) {
            switch_mobileAutoSync.setChecked(true);
        } else if (StaticVariables.Notification.equals("Yes")) {
            switch_notification.setChecked(true);
        } else {
            switch_notification.setChecked(false);
            switch_wifiAutoSync.setChecked(false);
            switch_mobileAutoSync.setChecked(false);
        }

        text_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertCustomDialog("Logout!", "Are you sure, you want to logout?");
            }
        });

        text_sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertCustomDialog("Alert!", "Are you sure, you want to sync apps data to server?");
            }
        });

        switch_wifiAutoSync.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    StaticVariables.SyncWIFI = "Yes";
                } else {
                    StaticVariables.SyncWIFI = "No";
                }

            }
        });
        switch_mobileAutoSync.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    StaticVariables.SyncMobile = "Yes";
                } else {
                    StaticVariables.SyncMobile = "No";
                }
            }
        });
        switch_notification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    StaticVariables.Notification = "Yes";
                } else {
                    StaticVariables.Notification = "No";
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        CommonClass.checkSettings(SettingActivity.this, "U");

        if (StaticVariables.UserType.trim().equalsIgnoreCase("C")) {
            Intent intent = new Intent(SettingActivity.this, ParticipantHomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            SettingActivity.this.finish();
        } else if (StaticVariables.UserType.trim().equalsIgnoreCase("T")) {
            Intent intent = new Intent(SettingActivity.this, LPI_TrainerMenuActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            SettingActivity.this.finish();
        } else {
            super.onBackPressed();
        }
    }

    private void alertCustomDialog(String title, String message) {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(SettingActivity.this).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_exit_dialog, null);
            TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            Button btn_ok = (Button) dialogView.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialogView.findViewById(R.id.btn_no);

            promt_title.setText(title);
            txt_message.setText(message);
            if (title.equalsIgnoreCase("Logout!")) {
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        boolean tablesDroped = CommonClass.onDropCustomTables(DatabaseHelper.db, SettingActivity.this);
                        boolean tablesCreated = false;
                        if (tablesDroped) {
                            tablesCreated = CommonClass.onCreateCustomTables(DatabaseHelper.db, SettingActivity.this);
                        }
                        if (tablesCreated && tablesCreated) {
                            DatabaseHelper.db.delete(getResources().getString(R.string.TBL_iCandidate), null, null);
                        }
                        System.gc();
                        Intent i = new Intent(getApplicationContext(), Login.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        SettingActivity.this.finish();

                    }
                });
            } else if (title.equalsIgnoreCase("Alert!")) {
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        syncServices(SettingActivity.this);
                    }
                });
            }
            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });

            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void syncServices(Context context) {
        try {

            if (CommonClass.isConnected(context)) {
                if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncExamMaster")) {
                    Intent newIntent = new Intent(context.getApplicationContext(), SyncExamMaster.class);
//                    context.getApplicationContext().startService(newIntent);
                    CommonClass.StartService(getApplicationContext(), newIntent);
                }

                if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncFAQModuleData")) {
                    Intent newIntent = new Intent(context.getApplicationContext(), SyncFAQModuleData.class);
//                    context.getApplicationContext().startService(newIntent);
                    CommonClass.StartService(getApplicationContext(), newIntent);
                }

                if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncActivityDetails")) {
                    Intent trackIntent = new Intent(context.getApplicationContext(), SyncActivityDetails.class);
//                    context.getApplicationContext().startService(trackIntent);
                    CommonClass.StartService(getApplicationContext(), trackIntent);
                }

                if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncErrorLog")) {
                    Intent newIntent = new Intent(context.getApplicationContext(), SyncErrorLog.class);
//                    context.getApplicationContext().startService(newIntent);
                    CommonClass.StartService(getApplicationContext(), newIntent);
                }

                if (!CommonClass.isMyServiceRunning(context.getApplicationContext(), "SyncOfflineData")) {
                    Intent newIntent = new Intent(context.getApplicationContext(), SyncOfflineData.class);
//                    context.getApplicationContext().startService(newIntent);
                    CommonClass.StartService(getApplicationContext(), newIntent);
                }
            } else {
            }

        } catch (Exception e) {
            Log.e("", "wewewe" + e.toString());
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().setTitle("Settings");
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
