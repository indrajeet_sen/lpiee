package com.lpi.kmi.lpi.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Services.AlarmReceiver;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.stringcare.library.SC;

import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@SuppressLint("Range")
public class SplashScreen extends AppCompatActivity {
    //implements GoogleApiClient.ConnectionCallbacks
    public static final String PERMISSION = Manifest.permission.READ_PHONE_STATE;

    public static String androidId = "";
    private static boolean Expired = false;
    //final Handler handler = new Handler();
    //    public static SQLiteDatabase db;
    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.ENGLISH);

    TextView txt_intialize;
    //    ImageView imgExpiry;
    ProgressBar progressBar;
    LinearLayout lin_appName, footer, lin_trialexpired, linInit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        CommonClass.setLaguage(SplashScreen.this, "en");


        try {
            StaticVariables.androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            StaticVariables.locale = getResources().getConfiguration().locale.getCountry();
            txt_intialize = (TextView) findViewById(R.id.txt_intialize);
            lin_trialexpired = (LinearLayout) findViewById(R.id.lin_trialexpired);
            linInit = (LinearLayout) findViewById(R.id.linInit);
            footer = (LinearLayout) findViewById(R.id.footer);
            lin_appName = (LinearLayout) findViewById(R.id.lin_appName);
            progressBar = (ProgressBar) findViewById(R.id.native_progress_bar);
            lin_trialexpired.setVisibility(View.GONE);
            linInit.setVisibility(View.VISIBLE);
            footer.setVisibility(View.GONE);
            lin_appName.setVisibility(View.GONE);

            SQLiteDatabase.loadLibs(SplashScreen.this);
            DatabaseHelper.db = DatabaseHelper.getInstance(SplashScreen.this).
                    getWritableDatabase(SC.reveal(CommonClass.DBPassword));

            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());

        } catch (Exception e) {
            e.printStackTrace();
        }

//        txt_intialize.setVisibility(View.GONE);
//        progressBar.setVisibility(View.GONE);
//        if (!Expired) {
//            imgExpiry.setVisibility(View.GONE);
//            imgKmiLogo.setVisibility(View.GONE);
//            lin_appName.setVisibility(View.GONE);
        /*try {
            StaticVariables.androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            StaticVariables.locale = getResources().getConfiguration().locale.getCountry();

            SharedPreferences preferences = getPreferences(MODE_PRIVATE);
            String installDate = preferences.getString("InstallDate", null);
            String expiryDate = preferences.getString("expiryDate", null);
            if (installDate == null) {
// First run, so save the current date
                SharedPreferences.Editor editor = preferences.edit(native_progress_bar);
                Calendar c = Calendar.getInstance();
                c.setTime(new Date());
                String dateString = formatter.format(c.getTime());
                editor.putString("InstallDate", dateString);
                c.add(Calendar.DAY_OF_MONTH, 15);
                String dateexpiry = formatter.format(c.getTime());
                editor.putString("expiryDate", dateexpiry);
// Commit the edits!
                editor.commit();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }*/

        initData();
    }

    private void initData() {
//        if (CommonClass.isConnected(getApplicationContext())) {
//            Intent intent = new Intent("com.lpi.kmi.candidate");
//            sendBroadcast(intent);
//        }
        try {
            Intent alarmIntent = new Intent(SplashScreen.this, AlarmReceiver.class);
            PendingIntent pendingIntent;// = PendingIntent.getBroadcast(SplashScreen.this, 0, alarmIntent, 0);
//            pendingIntent= PendingIntent.getBroadcast(SplashScreen.this, 0, alarmIntent, 0);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                pendingIntent = PendingIntent.getBroadcast(SplashScreen.this, 0, alarmIntent, PendingIntent.FLAG_IMMUTABLE);
            } else {
                pendingIntent = PendingIntent.getBroadcast(SplashScreen.this, 0, alarmIntent, PendingIntent.FLAG_MUTABLE);
            }
            AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            int interval = 5000;
            manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);

            //                        boolean isAvailableData = false;
            //                        String strLoginId = "";
            Cursor mCursor = DatabaseHelper.db.query(getString(R.string.TBL_iCandidate), null,
                    null, null, null, null, null, "1");
            if (mCursor.getCount() > 0) {

                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    StaticVariables.crnt_InscType = "";
                    StaticVariables.isLogin = true;
                    StaticVariables.UserEmail = mCursor.getString(mCursor.getColumnIndex("CandidateEmailId"));
                    StaticVariables.UserName = mCursor.getString(mCursor.getColumnIndex("FirstName"));
                    StaticVariables.UserLoginId = mCursor.getString(mCursor.getColumnIndex("CandidateLoginId"));
                    StaticVariables.ParentId = mCursor.getString(mCursor.getColumnIndex("ParentId"));
                    StaticVariables.ParentName = mCursor.getString(mCursor.getColumnIndex("ParentName"));
                    StaticVariables.UserMobileNo = mCursor.getString(mCursor.getColumnIndex("CandidateMobileNo1"));
                    StaticVariables.offlineMode = "N";
                    StaticVariables.AppLink = mCursor.getString(mCursor.getColumnIndex("AppLink"));
                    StaticVariables.UserType = mCursor.getString(mCursor.getColumnIndex("UserType"));
                    StaticVariables.profileBitmap = mCursor.getBlob(mCursor.getColumnIndex("Image"));
                }

                //                        if (isAvailableData) {
                ////                            CommonClass.startFAQJob(getApplicationContext());
                //                            Intent i = new Intent(SplashScreen.this, PIN_AuthActivity.class);
                //                            i.putExtra("user_id", strLoginId);
                //                            startActivity(i);
                //                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                //                            Intent alarmIntent = new Intent(SplashScreen.this, AlarmReceiver.class);
                //                            PendingIntent pendingIntent = PendingIntent.getBroadcast(SplashScreen.this, 0, alarmIntent, 0);
                //                            AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                //                            int interval = 5000;
                //                            manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
                //
                //                        } else {
                //                            Intent i = new Intent(SplashScreen.this, Login.class);
                //                            startActivity(i);
                //                        }


                linInit.setVisibility(View.GONE);
                if (StaticVariables.UserEmail.equalsIgnoreCase("")) {
                    Intent i = new Intent(SplashScreen.this, Login.class);
                    startActivity(i);
                } else if (StaticVariables.UserType.trim().equalsIgnoreCase("C")) {
                    Intent intent = new Intent(SplashScreen.this, ParticipantHomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else if (StaticVariables.UserType.trim().equalsIgnoreCase("T")) {
                    Intent intent = new Intent(SplashScreen.this, LPI_TrainerMenuActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                SplashScreen.this.finish();
                /*if (CommonClass.isConnected(SplashScreen.this)) {
                    new RegisterDevice(SplashScreen.this, StaticVariables.UserEmail, StaticVariables.androidId).execute();
                } else {
                    checkOfflineExpiry();
                }*/
            } else {
                linInit.setVisibility(View.GONE);
                Intent intent = new Intent(SplashScreen.this, Login.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                SplashScreen.this.finish();
            }

            mCursor.close();


        } catch (Exception e) {
            linInit.setVisibility(View.GONE);
            DatabaseHelper.SaveErroLog(SplashScreen.this, DatabaseHelper.db,
                    "SplashScreen", "InsertDummyData > SplashScreen()", e.toString());
            Intent intent = new Intent(SplashScreen.this, Login.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            SplashScreen.this.finish();

        }
    }


    @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
    private void checkOfflineExpiry() {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        String installDate = preferences.getString("InstallDate", null);
        String expiryDate = preferences.getString("expiryDate", null);
        try {
            Date before = null, now = null;
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            String dateString = formatter.format(c.getTime());
            before = formatter.parse(expiryDate);
            now = formatter.parse(dateString);

            long diff = before.getTime() - now.getTime();

//                long diffSeconds = diff / 1000 % 60;
//                long diffMinutes = diff / (60 * 1000) % 60;
//                long diffHours = diff / (60 * 60 * 1000) % 24;
//                long diffDays = diff / (24 * 60 * 60 * 1000);
            //long Day = (diff / (24 * 60 * 60 * 1000));
            if (diff <= 0) {
                // expired
                Expired = true;
            } else {
                Expired = false;
            }

            if (!Expired) {
                lin_trialexpired.setVisibility(View.GONE);
                footer.setVisibility(View.GONE);
                lin_appName.setVisibility(View.GONE);

                if (StaticVariables.UserType.trim().equalsIgnoreCase("C")) {
                    Intent intent = new Intent(SplashScreen.this, ParticipantHomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else if (StaticVariables.UserType.trim().equalsIgnoreCase("T")) {
                    Intent intent = new Intent(SplashScreen.this, LPI_TrainerMenuActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                SplashScreen.this.finish();

            } else {
                linInit.setVisibility(View.GONE);
//                txt_intialize.setVisibility(View.GONE);
//                progressBar.setVisibility(View.GONE);
                footer.setVisibility(View.VISIBLE);
                lin_appName.setVisibility(View.VISIBLE);
                lin_trialexpired.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Intent intent = new Intent(SplashScreen.this, Login.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
//        handler.removeCallbacksAndMessages(null);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        handler.removeCallbacksAndMessages(null);
        finish();
    }

    /*public class RegisterDevice extends AsyncTask<String, String, String> {

        Context context;
        String emailId, androidId;

        public RegisterDevice(Context context, String emailId, String androidId) {
            this.context = context;
            this.emailId = emailId;
            this.androidId = androidId;
        }

        @Override
        protected String doInBackground(String... params) {
            String Status = "";
            try {

                Cursor cursor = DatabaseHelper.db.query(SplashScreen.this.getResources().getString(R.string.TBL_Country),
                        null, null, null, null, null, null);
                if (cursor.getCount() < 1) {
                    CommonClass.initDataArray(SplashScreen.this);
                }
                cursor.close();

                org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                        getResources().getString(R.string.Exam_NAMESPACE), getResources().getString(R.string.RegisterDevice));
                //property which holds input parameters
                PropertyInfo inputPI1 = new PropertyInfo();
                inputPI1.setName("objXMLDoc");
                String objXMLDoc = CommonClass.GenerateXML(getApplicationContext(), 2, "RegisterDevice",
                        new String[]{"EmailId", "AndroidId"}, new String[]{emailId, androidId});
                inputPI1.setValue(objXMLDoc);
                inputPI1.setType(String.class);
                request.addProperty(inputPI1);
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);
                envelope.dotNet = true;
                //Set output SOAP object
                envelope.setOutputSoapObject(request);
                //Create HTTP call object
                HttpTransportSE androidHttpTransport = new HttpTransportSE(getResources().getString(R.string.Exam_URL));
//        InputStream isResponse = null;

                //Involve Web service
                LPIEEX509TrustManager.allowAllSSL();
//                FakeX509TrustManager.trustSSLCertificate(SplashScreen.this);
                androidHttpTransport.call(getResources().getString(R.string.Exam_SOAP_ACTION) +
                        getResources().getString(R.string.RegisterDevice), envelope);
                //Get the response
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

                JSONObject objJsonObject = new JSONObject(response.toString());
                JSONArray objJsonArray = (JSONArray) objJsonObject.get("Table");
                for (int index = 0; index < objJsonArray.length(); index++) {
                    JSONObject jsonobject = objJsonArray.getJSONObject(index);
                    Status = jsonobject.getString("Status");

                }

            } catch (Exception e) {
                Log.e("Login", "Login Error=" + e.toString());
                DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
                        "Login", "onHandleIntent", e.toString());
//            onDestroy();
                Status = "Failed";
            }
            return Status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String Status) {
            if (Status.equalsIgnoreCase("Expired")) {
                linInit.setVisibility(View.GONE);
//                txt_intialize.setVisibility(View.GONE);
//                progressBar.setVisibility(View.GONE);
                footer.setVisibility(View.VISIBLE);
                lin_appName.setVisibility(View.VISIBLE);
                lin_trialexpired.setVisibility(View.VISIBLE);
            } else {
                if (StaticVariables.UserEmail.equalsIgnoreCase("")) {
                    Intent i = new Intent(SplashScreen.this, Login.class);
                    startActivity(i);
                } else if (StaticVariables.UserType.trim().equalsIgnoreCase("C")) {
                    Intent intent = new Intent(SplashScreen.this, ParticipantHomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else if (StaticVariables.UserType.trim().equalsIgnoreCase("T")) {
                    Intent intent = new Intent(SplashScreen.this, LPI_TrainerMenuActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

                //                                String isPIN = mCursor.getString(mCursor.getColumnIndex("is_mPIN_Data"));
                //                                if (isPIN != null && isPIN.trim().equalsIgnoreCase("Y")) {
                //                                    isAvailableData = true;
                //                                } else {
                //                                    isAvailableData = false;
                //                                }
            }

            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//            onDestroy();
        }
    }*/
}

