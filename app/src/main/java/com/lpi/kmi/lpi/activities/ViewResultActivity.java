package com.lpi.kmi.lpi.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.AsyncSubmitExams;
import com.lpi.kmi.lpi.activities.AsyncTasks.syncExams;
import com.lpi.kmi.lpi.adapter.Adapters.PersonalityTraits_BaseAdapter;
import com.lpi.kmi.lpi.adapter.GetterSetter.GetterSetter;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.ExceptionHandlerClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;
import com.stringcare.library.SC;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@SuppressLint("Range")
public class ViewResultActivity extends AppCompatActivity implements View.OnClickListener {

    public static List<GetterSetter> arrlst_allTraits;
    public static PersonalityTraits_BaseAdapter custAdapter;
    public ProgressBar ExmResult_progress_bar;
    BarChart chart;
    ArrayList<BarEntry> BARENTRY;
    ArrayList<String> BarEntryLabels;
    BarDataSet Bardataset;
    BarData BARDATA;
    int ResponseCode;
    Button btn_PsychoResultFfback, Btn_Strength, Btn_Weakness, Btn_Video;
    //    LinearLayout LnrLayout_Strength,LnrLayout_Weakness;
    LinearLayout Lnr_ExmResultInit,// Lnr_ExamResultDesc, Lnr_ExmResult, Lnr_ExamResultGraph,
            Lnr_ExamResultTraits, Lnr_AttemptExmResult;
    TextView txt_ExmResultInit, txt_ExamResult, txt_CndName,//, txt_ExamResultDesc
            txt_TraitPrsnalityType,
            txt_TotalQue, txt_AttemptedQue, txt_CorrectAns, txt_TotalScore, txt_ExamDate;
    ImageView imageView;
    InputStream VwRsltStream;
    String str_ExamID, str_Result, str_ResultType, str_RsltDesc,
            str_ExamId, str_ResultDesc, str_AttemptId, str_CandidateId, str_initFrom;
    List<String> arrlist_StrengthTrait, arrlist_WeaknessTrait, arrlist_ONARDCodeDescription;
    ListView lstVw_PersonalityTraits;
    CoordinatorLayout coordinatorLayout = null;
    private ProgressDialog progressDialog;
//    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

    //    Button btnViewVideo;
    RelativeLayout profileVideo;
    WebView webViewVideo;
    ProgressBar progsVid;
    TextView txt_noInternet;
    String URL = "";


    AlertDialog videoDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerClass(this));
        setContentView(R.layout.activity_view_result);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_ViewPscychoResult);
        toolbar.setTitle("View Result");
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        profileVideo = (RelativeLayout) findViewById(R.id.profileVideo);
        webViewVideo = (WebView) findViewById(R.id.webViewVideo);
        progsVid = (ProgressBar) findViewById(R.id.progsVid);
        txt_noInternet = (TextView) findViewById(R.id.txt_noInternet);

        btn_PsychoResultFfback = (Button) findViewById(R.id.btn_PsychoResultFfback);
        btn_PsychoResultFfback.setVisibility(View.GONE);
        Btn_Strength = (Button) findViewById(R.id.Btn_Strength);
        Btn_Weakness = (Button) findViewById(R.id.Btn_Weakness);
        Btn_Video = (Button) findViewById(R.id.Btn_Video);
        txt_CndName = (TextView) findViewById(R.id.cndName);
//        LnrLayout_Strength = (LinearLayout) findViewById(R.id.LnrLayout_Strength);
//        LnrLayout_Weakness = (LinearLayout) findViewById(R.id.LnrLayout_Weakness);
        Lnr_ExmResultInit = (LinearLayout) findViewById(R.id.Lnr_ExmResultInit);
//        Lnr_ExmResult = (LinearLayout) findViewById(R.id.Lnr_ExmResult);
//        Lnr_ExamResultDesc = (LinearLayout) findViewById(R.id.Lnr_ExamResultDesc);
//        Lnr_ExamResultGraph = (LinearLayout) findViewById(R.id.Lnr_ExamResultGraph);
        Lnr_ExamResultTraits = (LinearLayout) findViewById(R.id.Lnr_ExamResultTraits);
        Lnr_AttemptExmResult = (LinearLayout) findViewById(R.id.Lnr_AttemptExmResult);

        lstVw_PersonalityTraits = (ListView) findViewById(R.id.lstVw_PersonalityTraits);

        txt_ExmResultInit = (TextView) findViewById(R.id.txt_ExmResultInit);
        txt_ExamResult = (TextView) findViewById(R.id.txt_ExamResult);
//        txt_ExamResultDesc = (TextView) findViewById(R.id.txt_ExamResultDesc);
        txt_TraitPrsnalityType = (TextView) findViewById(R.id.txt_TraitPrsnalityType);

        imageView = (ImageView) findViewById(R.id.imgResult);
        txt_ExamDate = (TextView) findViewById(R.id.txt_ExamDate);
        txt_TotalQue = (TextView) findViewById(R.id.txt_TotalQue);
        txt_AttemptedQue = (TextView) findViewById(R.id.txt_AttemptedQue);
        txt_CorrectAns = (TextView) findViewById(R.id.txt_CorrectAns);
        txt_TotalScore = (TextView) findViewById(R.id.txt_TotalScore);

        ResponseCode = 0;
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.cordinatorLayout);

        chart = (BarChart) findViewById(R.id.ResultBarchart);
        ExmResult_progress_bar = (ProgressBar) findViewById(R.id.ExmResult_progress_bar);

//        btnViewVideo  = (Button) findViewById(R.id.btnViewVideo);


        btn_PsychoResultFfback.setOnClickListener(this);
        Btn_Strength.setOnClickListener(this);
        Btn_Weakness.setOnClickListener(this);
        Btn_Video.setOnClickListener(this);

        Lnr_AttemptExmResult.setVisibility(View.GONE);
        txt_CndName.setVisibility(View.GONE);

//        LnrLayout_Strength.setVisibility(View.VISIBLE);
//        LnrLayout_Weakness.setVisibility(View.GONE);

        arrlist_StrengthTrait = new ArrayList<String>();
        arrlist_WeaknessTrait = new ArrayList<String>();
        arrlist_ONARDCodeDescription = new ArrayList<String>();

        BARENTRY = new ArrayList<>();
        BarEntryLabels = new ArrayList<String>();

        progressDialog = new ProgressDialog(ViewResultActivity.this,
                R.style.AppTheme_Dark_Dialog);

        try {
            str_ExamId = getIntent().getExtras().getString("ExamId");
            str_AttemptId = getIntent().getExtras().getString("AttemptedId");
            str_CandidateId = getIntent().getExtras().getString("CandidateId");
            str_initFrom = getIntent().getExtras().getString("initFrom");
//            if (!str_CandidateId.equals(""))
//                txt_CndName.setText(DatabaseHelper.UserName(ViewResultActivity.this, DatabaseHelper.db, str_CandidateId));
//            else
//                txt_CndName.setText("Dear LPI User");
//            viewprofile.setSmoothScrollingEnabled(true);
//            viewprofile.setNestedScrollingEnabled(true);

            if (DatabaseHelper.db == null) {
                SQLiteDatabase.loadLibs(getApplicationContext());
                DatabaseHelper.db = DatabaseHelper.getInstance(getApplicationContext()).
                        getWritableDatabase(SC.reveal(CommonClass.DBPassword));
            }

        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
        try {
            Cursor cursor = DatabaseHelper.db.query(getResources().getString(R.string.TBL_LPICandidateExamResult),
                    null, "ExamId = ? and CandId = ?", new String[]{str_ExamId, str_CandidateId},
                    null, null, null);

            if (cursor.getCount() > 0) {
                showResult();
            } else if (CommonClass.isConnected(ViewResultActivity.this)) {
                new GetResult(0, ViewResultActivity.this).execute();
            } else {
                btn_PsychoResultFfback.setVisibility(View.GONE);
                Lnr_AttemptExmResult.setVisibility(View.GONE);
                txt_ExmResultInit.setText("No Internet Connection Found, \nPlease connect to the internet and try again.");
                Lnr_ExmResultInit.setVisibility(View.VISIBLE);
                ExmResult_progress_bar.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            if (CommonClass.isConnected(ViewResultActivity.this)) {
                new GetResult(0, ViewResultActivity.this).execute();
            } else {
                btn_PsychoResultFfback.setVisibility(View.GONE);
                Lnr_AttemptExmResult.setVisibility(View.GONE);
                txt_ExmResultInit.setText("No Internet Connection Found, \nPlease connect to the internet and try again.");
                Lnr_ExmResultInit.setVisibility(View.VISIBLE);
                ExmResult_progress_bar.setVisibility(View.GONE);
            }
        }

//        btnViewVideo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showProfileVideo();
//            }
//        });

        try {
            String code = DatabaseHelper.getActivityMaster(ViewResultActivity.this, DatabaseHelper.db, "View Personality Profile");
            DatabaseHelper.InsertActivityTracker(ViewResultActivity.this, DatabaseHelper.db, StaticVariables.UserLoginId,
                    code, "", "View Personality Profile Page", "", "",
                    "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "", StaticVariables.UserName, "",
                    "", "", "Pending");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showResult() {
        try {
            Cursor cursor = DatabaseHelper.db.query(getResources().getString(R.string.TBL_LPICandidateExamResult),
                    null, "ExamId=? and CandId=?", new String[]{str_ExamId, str_CandidateId},
                    null, null, null);
            if (cursor.getCount() > 0) {
                Lnr_ExmResultInit.setVisibility(View.GONE);
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    str_ExamID = cursor.getString(cursor.getColumnIndex("ExamId"));
                    str_Result = cursor.getString(cursor.getColumnIndex("Result"));
                    str_ResultDesc = cursor.getString(cursor.getColumnIndex("ResultDesc"));
                    txt_CndName.setText(cursor.getString(cursor.getColumnIndex("CndName")));
                    txt_CndName.setVisibility(View.VISIBLE);
                    txt_ExamResult.setText("Result: " + str_Result);
                    txt_ExamDate.setText("Exam Date : " + cursor.getString(cursor.getColumnIndex("ExamDate")));
                    URL = cursor.getString(cursor.getColumnIndex("ProfileVideo"));
                    String s = cursor.getString(cursor.getColumnIndex("IsAllowResultFeedback"));
                    if (s.equals("Y")) {
                        if (str_initFrom.equals("LPI_SearchActivity") || str_initFrom.equals("LPI_StatusActivity")
                                || str_initFrom.equals("LPI_ViewProfileActivity") || str_initFrom.equals("TrainerHomeActivity")) {
                            btn_PsychoResultFfback.setVisibility(View.GONE);
                        } else {
                            //  btn_PsychoResultFfback.setVisibility(View.VISIBLE); bharat 19 08 20
                            btn_PsychoResultFfback.setVisibility(View.GONE);
                        }
                    } else
                        btn_PsychoResultFfback.setVisibility(View.GONE);

                    if (str_ExamID.equals("4001")) {
                        str_RsltDesc = cursor.getString(cursor.getColumnIndex("ResultDesc"));
                        Log.e("", "str_RsltDesc=" + str_RsltDesc);
                        if (str_RsltDesc.length() == 0) {
                            //                                        Lnr_ExamResultDesc.setVisibility(View.GONE);
                            txt_TraitPrsnalityType.setVisibility(View.GONE);
                        } else {
                            //                                        Lnr_ExamResultDesc.setVisibility(View.VISIBLE);
                            //                                        txt_ExamResultDesc.setText(str_ResultType);
                            txt_TraitPrsnalityType.setText(str_RsltDesc);
                            txt_TraitPrsnalityType.setVisibility(View.VISIBLE);
                        }

                        // JSONParsing for TraitTypes
                        try {
                            str_ResultType = cursor.getString(cursor.getColumnIndex("ResultTrait"));
                            if (str_ResultType.length() == 0) {
                                Lnr_ExamResultTraits.setVisibility(View.GONE);
                            } else {
                                str_ResultType = str_ResultType.replace("[{\"TraitTypesResponseCode\":\"0\"}]",
                                        "{\"TraitTypesResponseCode\":\"0\"");
                                str_ResultType = str_ResultType + "}";
                                arrlist_StrengthTrait.clear();
                                arrlist_WeaknessTrait.clear();
                                JSONObject objJsonObject = new JSONObject(str_ResultType);
                                JSONObject objJsonObject_TraitDtls = objJsonObject.getJSONObject("TraitDetails");
                                JSONArray Json_Array;
                                String RespondeCode = objJsonObject_TraitDtls.getString("TraitTypesResponseCode");
                                if (RespondeCode.equals("0")) {
                                    Lnr_ExamResultTraits.setVisibility(View.VISIBLE);
                                    Json_Array = (JSONArray) objJsonObject_TraitDtls.get("TraitTypes");
                                    //                                                for (int j = 0; j < Json_Array.length(); j++) {

                                    for (int j = 0; j < Json_Array.length(); j++) {
                                        JSONObject jsonobject = Json_Array.getJSONObject(j);
                                        if (jsonobject.getString("TraitType").equals("Strength"))
                                            arrlist_StrengthTrait.add("- " + jsonobject.getString("TraitDesc"));
                                        else if (jsonobject.getString("TraitType").equals("Weakness"))
                                            arrlist_WeaknessTrait.add("- " + jsonobject.getString("TraitDesc"));
                                    }
                                }

                                if (arrlist_StrengthTrait.size() > 6)
                                    arrlist_StrengthTrait = arrlist_StrengthTrait.subList(0, 6);

                                if (arrlist_WeaknessTrait.size() > 6)
                                    arrlist_WeaknessTrait = arrlist_WeaknessTrait.subList(0, 6);

                                custList(arrlist_StrengthTrait);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        // JSONParsing for ONARD graph
                        try {
                            str_ResultType = cursor.getString(cursor.getColumnIndex("GraphValue"));
                            if (str_ResultType.length() == 0) {
                                chart.setVisibility(View.GONE);
                            } else {
                                JSONObject objJsonObject = new JSONObject(str_ResultType);
                                JSONObject objJsonObject_TraitDtls = objJsonObject.getJSONObject("ONARDTypes");
                                String RespondeCode = objJsonObject_TraitDtls.getString("ONARDTypesResponseCode");
                                JSONArray Json_Array;
                                if (RespondeCode.equals("0")) {
                                    chart.setVisibility(View.VISIBLE);
                                    Json_Array = (JSONArray) objJsonObject_TraitDtls.get("ONARDType");
                                    for (int j = 0; j < Json_Array.length(); j++) {
                                        JSONObject jsonobject = Json_Array.getJSONObject(j);
                                        String str = jsonobject.getString("ONARDValue");
                                        BARENTRY.add(new BarEntry(Float.parseFloat(str), j));
                                        BarEntryLabels.add(jsonobject.getString("ONARDCode"));
                                        arrlist_ONARDCodeDescription.add(jsonobject.getString("ONARDCodeDescription"));

                                    }
                                    PlotGraph(arrlist_ONARDCodeDescription);
                                } else {
                                    profileVideo.setVisibility(View.GONE);
                                    progsVid.setVisibility(ProgressBar.GONE);
                                    webViewVideo.setVisibility(View.GONE);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (CommonClass.isConnected(this)) {
                            new syncExams(new Callback<String>() {
                                @Override
                                public void execute(String result, String status) {

                                }
                            }, ViewResultActivity.this, "ViewResultActivity", StaticVariables.UserLoginId, "").execute();
                        }
                    } else {
                        try {
                            btn_PsychoResultFfback.setVisibility(View.GONE);
                            str_ResultDesc = cursor.getString(cursor.getColumnIndex("ResultDesc"));
                            if (str_ResultDesc.length() == 0) {
                                Lnr_AttemptExmResult.setVisibility(View.GONE);
                            } else {
                                Lnr_AttemptExmResult.setVisibility(View.VISIBLE);
                                JSONObject objJsonObject = new JSONObject(str_ResultDesc);
                                JSONArray objJsonArray = (JSONArray) objJsonObject.get("ResultDesc");
                                for (int index = 0; index < objJsonArray.length(); index++) {
                                    JSONObject jsonobject = objJsonArray.getJSONObject(index);

                                    if (jsonobject.getString("Result").equals("Pass")) {
                                        imageView.setImageDrawable(getResources().getDrawable(R.drawable.pass));
                                    } else {
                                        imageView.setImageDrawable(getResources().getDrawable(R.drawable.failed));
                                    }
                                    //                                    txt_ExamDate.setText("Exam Date : " + jsonobject.getString("ExamDate"));
                                    txt_TotalQue.setText("Total Questions : " + jsonobject.getString("TotalQue"));
                                    txt_AttemptedQue.setText("Attempted Questions : " + jsonobject.getString("AttemptedQue"));
                                    txt_CorrectAns.setText("Correct Answers : " + jsonobject.getString("CorrectAns"));
                                    txt_TotalScore.setText("Score : " + jsonobject.getString("Score"));
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }

            } else if (ResponseCode == 2) {
                Lnr_ExmResultInit.setVisibility(View.VISIBLE);
                //                Lnr_ExmResult.setVisibility(View.GONE);
                btn_PsychoResultFfback.setVisibility(View.GONE);
                Lnr_AttemptExmResult.setVisibility(View.GONE);
                txt_ExmResultInit.setText("Result details not found or Server not responding...");
                ExmResult_progress_bar.setVisibility(View.GONE);

                //                    txt_ExmResultInit.setText("Server not responding...");
                Toast.makeText(ViewResultActivity.this, "Server not responding...", Toast.LENGTH_LONG).show();
            } else {
                Lnr_ExmResultInit.setVisibility(View.VISIBLE);
                //                Lnr_ExmResult.setVisibility(View.GONE);
                btn_PsychoResultFfback.setVisibility(View.GONE);
                Lnr_AttemptExmResult.setVisibility(View.GONE);
                txt_ExmResultInit.setText("Result details not found or Server not responding...");
                ExmResult_progress_bar.setVisibility(View.GONE);
                Toast.makeText(ViewResultActivity.this, "Something went worng...", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Lnr_ExmResultInit.setVisibility(View.VISIBLE);
            //                Lnr_ExmResult.setVisibility(View.GONE);
            btn_PsychoResultFfback.setVisibility(View.GONE);
            Lnr_AttemptExmResult.setVisibility(View.GONE);
            txt_ExmResultInit.setText("Result details not found or Server not responding...");
            ExmResult_progress_bar.setVisibility(View.GONE);
            Toast.makeText(ViewResultActivity.this, "Something went worng...", Toast.LENGTH_LONG).show();
        }

        /*if (str_initFrom.equals("MCQ_QuestionActivity") || str_initFrom.equals("Custom_GridAdapter")
                || str_initFrom.equals("TrainerGridAdapter")) {
            checkFeedback();
        }*/
    }

    void checkFeedback() {
        try {
            if (StaticVariables.UserLoginId.startsWith("E")) {
                String str = StaticVariables.UserLoginId.replace("E", "0");
                StaticVariables.crnt_ExmId = BigInteger.valueOf(Long.valueOf(str + "00"));
            } else if (StaticVariables.UserLoginId.contains("LPI")) {
                String str = StaticVariables.UserLoginId.replace("LPI", "0");
                StaticVariables.crnt_ExmId = BigInteger.valueOf(Long.valueOf(str + "00"));
            } else {
                StaticVariables.crnt_ExmId = BigInteger.valueOf(Long.valueOf(StaticVariables.UserLoginId + "00"));
            }
            /*int AllowedAttempt = DatabaseHelper.AllowedAttempt(ViewResultActivity.this,
                    DatabaseHelper.db, "" + StaticVariables.crnt_ExmId, StaticVariables.UserLoginId);
            int attemptId = DatabaseHelper.CheckAttempts(ViewResultActivity.this,
                    DatabaseHelper.db, "" + StaticVariables.crnt_ExmId, StaticVariables.UserLoginId);
            if (AllowedAttempt - attemptId > 0) {
                alertCompleteProfile();
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void alertCompleteProfile() {
        final AlertDialog alertDialog = new AlertDialog.Builder(ViewResultActivity.this).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_exit_dialog, null);
            TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            Button btn_ok = (Button) dialogView.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialogView.findViewById(R.id.btn_no);

            promt_title.setText("Feedback");
            txt_message.setText("Do you want to complete Psychometric Profiling Feedback?");
            btn_ok.setText("YES");
            btn_no.setText("Not Now");
            btn_no.setPadding(5, 0, 5, 0);
            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    try {
                        if (StaticVariables.UserLoginId.contains("LPI")) {
                            String str = StaticVariables.UserLoginId.replace("LPI", "0");
                            StaticVariables.crnt_ExmId = BigInteger.valueOf(Long.valueOf(str + "00"));
                        } else {
                            StaticVariables.crnt_ExmId = BigInteger.valueOf(Long.valueOf(StaticVariables.UserLoginId + "00"));
                        }
                        StaticVariables.crnt_QueId = 1;
                        StaticVariables.TblNameForCount = DatabaseHelper.TotalQueCount(ViewResultActivity.this,
                                DatabaseHelper.db, StaticVariables.crnt_ExmId);
                        StaticVariables.crnt_ExmTypeName = "Test feedback";
                        StaticVariables.crnt_ExmName = "Test feedback";
                        StaticVariables.crnt_ExamType = 5;
                        Intent i = new Intent(ViewResultActivity.this, MCQ_QuestionActivity.class);
                        i.putExtra("initFrom", "ViewResultActivity");
                        startActivity(i);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        ViewResultActivity.this.finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });

            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void alertCustom(String title, String message) {
        final AlertDialog alertDialog = new AlertDialog.Builder(ViewResultActivity.this).create();
        alertDialog.setCancelable(false);
        try {
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_exit_dialog, null);
            TextView promt_title = (TextView) dialogView.findViewById(R.id.promt_title);
            TextView txt_message = (TextView) dialogView.findViewById(R.id.txt_message);
            Button btn_ok = (Button) dialogView.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialogView.findViewById(R.id.btn_no);

            promt_title.setText(title);
            txt_message.setText(message);
            btn_ok.setText("OK");
            btn_no.setVisibility(View.GONE);
            btn_no.setPadding(5, 0, 5, 0);
            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    onBackPressed();
                }
            });
            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });

            alertDialog.setView(dialogView);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void PlotGraph(List<String> arrLst) {
        int[] VORDIPLOM_COLORS = {
                Color.rgb(153, 255, 51), Color.rgb(255, 102, 102), Color.rgb(255, 166, 77),
                Color.rgb(51, 153, 255), Color.rgb(219, 112, 184)
        };

        String[] str = new String[arrLst.size()];
        for (int i = 0; i < arrLst.size(); i++) {
            str[i] = arrLst.get(i);
        }


//        AddValuesToBARENTRY();
//        AddValuesToBarEntryLabels();
        Bardataset = new BarDataSet(BARENTRY, "");
        BARDATA = new BarData(BarEntryLabels, Bardataset);
//        Bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
        Bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
        chart.setData(BARDATA);
        Legend legend = chart.getLegend();
        legend.setCustom(ColorTemplate.COLORFUL_COLORS, str);
        legend.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        chart.getLegend().setWordWrapEnabled(true);
        chart.setDescription("");

        chart.animateY(3000);
    }

    public void AddValuesToBARENTRY() {

        BARENTRY.add(new BarEntry(55f, 0));
        BARENTRY.add(new BarEntry(100f, 1));
        BARENTRY.add(new BarEntry(70f, 2));
        BARENTRY.add(new BarEntry(65f, 3));
        BARENTRY.add(new BarEntry(70f, 4));

    }

    public void AddValuesToBarEntryLabels() {

        BarEntryLabels.add("O");
        BarEntryLabels.add("N");
        BarEntryLabels.add("A");
        BarEntryLabels.add("R");
        BarEntryLabels.add("D");

    }

    public void custList(List<String> arrlist) {
        try {
            arrlst_allTraits = getLstData(arrlist);
            custAdapter = new PersonalityTraits_BaseAdapter(this, arrlst_allTraits);
            lstVw_PersonalityTraits.setAdapter(custAdapter);
            int index = lstVw_PersonalityTraits.getFirstVisiblePosition();
            View v = lstVw_PersonalityTraits.getChildAt(0);
            int top = (v == null) ? 0 : v.getTop();
            lstVw_PersonalityTraits.setSelectionFromTop(index, top);
        } catch (Exception e) {
            Log.e("Tag", "cust error=" + e.toString());
        }

    }

    private List<GetterSetter> getLstData(List<String> arrlist) {
        // TODO Auto-generated method stub
        List<GetterSetter> arrlst = new ArrayList<GetterSetter>();
        for (int i = 0; i < arrlist.size(); i++) {
            try {
                GetterSetter traitGttrSttr = new GetterSetter(arrlist.get(i));
                arrlst.add(traitGttrSttr);
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }
        return arrlst;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_PsychoResultFfback:

                try {
//                if (CommonClass.isConnected(ViewResultActivity.this)) {
//                    ResponseCode = 0;
//                    new GetResultFeedBck_QueAns().execute();
//                }else {

                    if (StaticVariables.UserLoginId.startsWith("E")) {
                        String str = StaticVariables.UserLoginId.replace("E", "0");
                        StaticVariables.crnt_ExmId = BigInteger.valueOf(Long.valueOf(str + "00"));
                    } else if (StaticVariables.UserLoginId.contains("LPI")) {
                        String str = StaticVariables.UserLoginId.replace("LPI", "0");
                        StaticVariables.crnt_ExmId = BigInteger.valueOf(Long.valueOf(str + "00"));
                    } else {
                        StaticVariables.crnt_ExmId = BigInteger.valueOf(Long.valueOf(StaticVariables.UserLoginId + "00"));
                    }

                    StaticVariables.crnt_QueId = 1;
//                        StaticVariables.TblNameForCount = (arrlist_StrengthTrait.size()+arrlist_WeaknessTrait.size()) ;
                    StaticVariables.TblNameForCount = DatabaseHelper.TotalQueCount(ViewResultActivity.this,
                            DatabaseHelper.db, StaticVariables.crnt_ExmId);
//
                    StaticVariables.crnt_ExmTypeName = "Test feedback";
                    StaticVariables.crnt_ExmName = "Test feedback";
                    StaticVariables.crnt_ExamType = 5;

                    if ((DatabaseHelper.TotalQueCount(ViewResultActivity.this, DatabaseHelper.db, StaticVariables.crnt_ExmId) > 1)) {
                        try {
                            Intent i = new Intent(ViewResultActivity.this, MCQ_QuestionActivity.class);
                            i.putExtra("initFrom", "ViewResultActivity");
                            startActivity(i);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            ViewResultActivity.this.finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (CommonClass.isConnected(ViewResultActivity.this) && (DatabaseHelper.CheckAttempts(ViewResultActivity.this,
                                DatabaseHelper.db, "" + StaticVariables.crnt_ExmId, StaticVariables.UserLoginId) == 0)) {
                            new syncExams(new Callback<String>() {
                                @Override
                                public void execute(String result, String status) {
                                    try {
                                        Intent i = new Intent(ViewResultActivity.this, MCQ_QuestionActivity.class);
                                        i.putExtra("initFrom", "ViewResultActivity");
                                        startActivity(i);
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                        ViewResultActivity.this.finish();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, ViewResultActivity.this,
                                    "ViewResultActivity", StaticVariables.UserLoginId,
                                    "" + StaticVariables.crnt_ExmId, "" + StaticVariables.crnt_ExamType).execute();
                        } else if ((DatabaseHelper.CheckAttempts(ViewResultActivity.this,
                                DatabaseHelper.db, "" + StaticVariables.crnt_ExmId, StaticVariables.UserLoginId) > 0)) {
                            Intent i = new Intent(ViewResultActivity.this, MCQ_QuestionActivity.class);
                            i.putExtra("initFrom", "ViewResultActivity");
                            startActivity(i);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            ViewResultActivity.this.finish();
                        } else {
                            Toast.makeText(ViewResultActivity.this, "No internet connectivity found for get feedback question", Toast.LENGTH_SHORT);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.Btn_Strength:
                profileVideo.setVisibility(View.GONE);
                lstVw_PersonalityTraits.setVisibility(View.VISIBLE);
                Btn_Strength.setTextColor(getResources().getColor(R.color.colorPrimary));
                Btn_Weakness.setTextColor(getResources().getColor(R.color.Gray));
                Btn_Video.setTextColor(getResources().getColor(R.color.Gray));
//                LnrLayout_Strength.setVisibility(View.VISIBLE);
//                LnrLayout_Weakness.setVisibility(View.GONE);
                custList(arrlist_StrengthTrait);
                break;
            case R.id.Btn_Weakness:
                profileVideo.setVisibility(View.GONE);
                lstVw_PersonalityTraits.setVisibility(View.VISIBLE);
                Btn_Weakness.setTextColor(getResources().getColor(R.color.colorPrimary));
                Btn_Strength.setTextColor(getResources().getColor(R.color.Gray));
                Btn_Video.setTextColor(getResources().getColor(R.color.Gray));
//                LnrLayout_Strength.setVisibility(View.GONE);
//                LnrLayout_Weakness.setVisibility(View.VISIBLE);
                custList(arrlist_WeaknessTrait);
                break;
            case R.id.Btn_Video:
                lstVw_PersonalityTraits.setVisibility(View.GONE);
                profileVideo.setVisibility(View.VISIBLE);
                Btn_Video.setTextColor(getResources().getColor(R.color.colorPrimary));
                Btn_Weakness.setTextColor(getResources().getColor(R.color.Gray));
                Btn_Strength.setTextColor(getResources().getColor(R.color.Gray));
//                LnrLayout_Strength.setVisibility(View.GONE);
//                LnrLayout_Weakness.setVisibility(View.VISIBLE);
                showProfileVideo();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = null;
        if (str_initFrom.equals("SyncOfflineData")) {
            if (CommonClass.isAppRunning(ViewResultActivity.this, getPackageName())) {

            } else {
                ViewResultActivity.this.finish();
                System.exit(10);
            }
        }

        if (str_initFrom.equals("MCQ_QuestionActivity")) {
            if (StaticVariables.UserType.equalsIgnoreCase("C")) {
                intent = new Intent(ViewResultActivity.this, ParticipantMenuActivity.class);
            } else {
                intent = new Intent(ViewResultActivity.this, LPI_TrainerMenuActivity.class);
            }
        } else if (str_initFrom.equals("AttempetedAxmList")) {
            intent = new Intent(ViewResultActivity.this, AttemtedExamActivity.class);
        } else if (str_initFrom.equals("Custom_GridAdapter")) {
            intent = new Intent(ViewResultActivity.this, ParticipantMenuActivity.class);
        } else if (str_initFrom.equals("TrainerGridAdapter")) {
            intent = new Intent(ViewResultActivity.this, LPI_TrainerMenuActivity.class);
        } else if (str_initFrom.equals("LPI_SearchActivity")) {
            intent = new Intent(ViewResultActivity.this, LPI_SearchActivity.class);
        } else if (str_initFrom.equals("LPI_StatusActivity")) {
            intent = new Intent(ViewResultActivity.this, LPI_StatusActivity.class);
        } else if (str_initFrom.equals("LPI_ViewProfileActivity")) {
            intent = new Intent(ViewResultActivity.this, LPI_ViewProfileActivity.class);
        } else if (str_initFrom.equals("TrainerHomeActivity")) {
            super.onBackPressed();
        } else {
            /*intent = new Intent(ViewResultActivity.this, PIN_AuthActivity.class);*/
            super.onBackPressed();
        }
        if (intent != null) {
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
        ViewResultActivity.this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (coordinatorLayout == null) {
                coordinatorLayout = (CoordinatorLayout) findViewById(R.id.cordinatorLayout);
            }
            if (CommonClass.isConnected(ViewResultActivity.this)) {
                Cursor cursor = DatabaseHelper.GetXMLData(getApplicationContext(), DatabaseHelper.db);
                if ((cursor.getCount() > 0) && (CommonClass.isConnected(ViewResultActivity.this))) {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Offline Exam data available,\nPlease Sync to submit.", Snackbar.LENGTH_INDEFINITE)
                            .setAction("Sync Data", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (CommonClass.isConnected(ViewResultActivity.this)) {
                                        new AsyncSubmitExams(ViewResultActivity.this, "ParticipantMenuActivity").execute();
                                    } else {
                                        Toast.makeText(ViewResultActivity.this, "No internet connectivity", Toast.LENGTH_SHORT);
                                    }
                                }
                            });

                    snackbar.show();
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (webViewVideo != null) {
                webViewVideo.resumeTimers();
                webViewVideo.onResume();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        try {
            webViewVideo.onPause();
            webViewVideo.pauseTimers();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        try {
            webViewVideo.loadUrl("about:blank");
            webViewVideo.reload();
            webViewVideo.destroy();
            webViewVideo = null;
            clearWebViewAllCache();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show, String Msg) {
        if (show) {
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(Msg);
            progressDialog.show();
        } else
            progressDialog.dismiss();

    }

    public class GetResult extends AsyncTask<String, String, InputStream> {
        public int position;
        public Context context;

        public GetResult(int position, Context con) {
            this.position = position;
            this.context = con;
        }

        @Override
        protected void onPreExecute() {
            Lnr_ExmResultInit.setVisibility(View.VISIBLE);
//            Lnr_ExmResult.setVisibility(View.GONE);
        }

        @SuppressLint("NewApi")
        @Override
        protected InputStream doInBackground(String... params) {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    ViewResultActivity.this.getString(R.string.Exam_NAMESPACE),
                    ViewResultActivity.this.getString(R.string.ViewResult));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            inputPI1.setName("objXMLDoc");
            try {
                String objXMLDoc = CommonClass.GenerateXML(context, 3, "ViewResult", new String[]{"LoginId", "ExamId",
                        "ExamStartDtime", "AttemptId"}, new String[]{"" + str_CandidateId, str_ExamId,
                        StaticVariables.sdf.format(new Date()), str_AttemptId});
                objXMLDoc = objXMLDoc.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
                inputPI1.setValue(objXMLDoc);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            // Set output SOAP object
            envelope.setOutputSoapObject(request);
            // Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(ViewResultActivity.this.getString(R.string.Exam_URL));
            InputStream isResponse = null;
            try {
                // Involve Web service
                LPIEEX509TrustManager.allowAllSSL();
//                FakeX509TrustManager.trustSSLCertificate(ViewResultActivity.this);
                androidHttpTransport.call(ViewResultActivity.this.getString(R.string.Exam_SOAP_ACTION) +
                        ViewResultActivity.this.getString(R.string.ViewResult), envelope);
                // Get the response
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

                String strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString();
                Log.e("", "response=" + strResponse);
                isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
                VwRsltStream = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));

                Log.e("", "Response=" + isResponse);
            } catch (Exception e) {
                Log.e("", "Login Error=" + e.toString());
                ResponseCode = 2;
            }
            return isResponse;
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {

            /*try {
                if (CommonClass.IsCorrectXMLResponse(inputStream)) {
                    NodeList nList = CommonClass.GetXMLElementNodeList(inputStream, "ExamResult");
                    if (nList!=null && nList.getLength() > 0) {
                        for (int i = 0; i < nList.getLength(); i++) {
                            Node node = nList.item(i);
                            if (node.getNodeType() == Node.ELEMENT_NODE) {
                                Element element = (Element) node;
                                ContentValues values = new ContentValues();
                                values.clear();
                                values.put("ExamId", CommonClass.getXMLElementValue("ExamID", element));
                                values.put("CandId", CommonClass.getXMLElementValue("CandId", element));
                                values.put("Result", CommonClass.getXMLElementValue("Result", element));
                                values.put("ExamDate", CommonClass.getXMLElementValue("ResultDesc", element));
                                values.put("ExamAttempt", CommonClass.getXMLElementValue("ExamDate", element));
                                values.put("ResultTrait", CommonClass.getXMLElementValue("ResultTrait", element));
                                values.put("GraphValue", CommonClass.getXMLElementValue("GraphValue", element));
                                values.put("IsAllowResultFeedback", CommonClass.getXMLElementValue("IsAllowResultFeedback", element));
                                values.put("CndName", CommonClass.getXMLElementValue("CndName", element));
                                values.put("ProfileVideo", CommonClass.getXMLElementValue("ProfileVideo", element));

                                Cursor cursor = DatabaseHelper.db.query(context.getResources().getString(R.string.TBL_LPICandidateExamResult),
                                        null, "ExamId=? and CandId=?", new String[]{CommonClass.getXMLElementValue("ExamID", element),
                                                CommonClass.getXMLElementValue("CandId", element)},
                                        null, null, null);

                                if (cursor.getCount() > 0) {
                                    DatabaseHelper.db.delete(context.getResources().getString(R.string.TBL_LPICandidateExamResult),
                                            "ExamId=? and CandId=?", new String[]{CommonClass.getXMLElementValue("ExamID", element),
                                                    CommonClass.getXMLElementValue("CandId", element)});
                                    DatabaseHelper.db.insert(context.getResources().getString(R.string.TBL_LPICandidateExamResult),
                                            null, values);
                                }

                            }
                        }
                    }else {
                        Lnr_ExmResultInit.setVisibility(View.VISIBLE);
                        //                Lnr_ExmResult.setVisibility(View.GONE);
                        btn_PsychoResultFfback.setVisibility(View.GONE);
                        Lnr_AttemptExmResult.setVisibility(View.GONE);
                        txt_ExmResultInit.setText("Result details not found or Server not responding...");
                        ExmResult_progress_bar.setVisibility(View.GONE);
                        Toast.makeText(ViewResultActivity.this, "Something went worng...", Toast.LENGTH_LONG).show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            showResult();*/

            try {

                if (CommonClass.IsCorrectXMLResponse(inputStream)) {

                    Lnr_ExmResultInit.setVisibility(View.GONE);
//                Lnr_ExmResult.setVisibility(View.VISIBLE);

                    NodeList nList = CommonClass.GetXMLElementNodeList(VwRsltStream, "ExamResult");
                    if (nList.getLength() > 0) {
                        for (int i = 0; i < nList.getLength(); i++) {
                            Node node = nList.item(i);
                            if (node.getNodeType() == Node.ELEMENT_NODE) {
                                try {
                                    Element element = (Element) node;
                                    str_ExamID = CommonClass.getXMLElementValue("ExamID", element);
                                    str_Result = CommonClass.getXMLElementValue("Result", element);
                                    str_ResultDesc = CommonClass.getXMLElementValue("ResultDesc", element);
                                    txt_CndName.setText(CommonClass.getXMLElementValue("CndName", element));
                                    txt_CndName.setVisibility(View.VISIBLE);
                                    txt_ExamResult.setText("Result: " + str_Result);
                                    txt_ExamDate.setText("Exam Date : " + CommonClass.getXMLElementValue("ExamDate", element));
                                    URL = CommonClass.getXMLElementValue("ProfileVideo", element);

                                    //Save To DB
                                    ContentValues values = new ContentValues();
                                    values.clear();
                                    values.put("ExamId", CommonClass.getXMLElementValue("ExamID", element));
                                    values.put("CandId", CommonClass.getXMLElementValue("CandId", element));
                                    values.put("Result", CommonClass.getXMLElementValue("Result", element));
                                    values.put("ResultDesc", CommonClass.getXMLElementValue("ResultDesc", element));
                                    values.put("ExamDate", CommonClass.getXMLElementValue("ExamDate", element));
//                                    values.put("ExamAttempt", CommonClass.getXMLElementValue("ExamDate", element));
                                    values.put("ResultTrait", CommonClass.getXMLElementValue("ResultTrait", element));
                                    values.put("GraphValue", CommonClass.getXMLElementValue("GraphValue", element));
                                    values.put("IsAllowResultFeedback", CommonClass.getXMLElementValue("IsAllowResultFeedback", element));
                                    values.put("CndName", CommonClass.getXMLElementValue("CndName", element));
                                    values.put("ProfileVideo", CommonClass.getXMLElementValue("ProfileVideo", element));

                                    Cursor cursor = DatabaseHelper.db.query(context.getResources().getString(R.string.TBL_LPICandidateExamResult),
                                            null, "ExamId=? and CandId=?", new String[]{CommonClass.getXMLElementValue("ExamID", element),
                                                    CommonClass.getXMLElementValue("CandId", element)},
                                            null, null, null);

                                    if (cursor.getCount() > 0) {
                                        DatabaseHelper.db.delete(context.getResources().getString(R.string.TBL_LPICandidateExamResult),
                                                "ExamId=? and CandId=?", new String[]{CommonClass.getXMLElementValue("ExamID", element),
                                                        CommonClass.getXMLElementValue("CandId", element)});
                                        DatabaseHelper.db.insert(context.getResources().getString(R.string.TBL_LPICandidateExamResult),
                                                null, values);
                                    } else {
                                        DatabaseHelper.db.insert(context.getResources().getString(R.string.TBL_LPICandidateExamResult),
                                                null, values);
                                    }

                                    String s = CommonClass.getXMLElementValue("IsAllowResultFeedback", element);

                                    if (s.equals("Y")) {
                                        if (str_initFrom.equals("LPI_SearchActivity") || str_initFrom.equals("LPI_StatusActivity")
                                                || str_initFrom.equals("LPI_ViewProfileActivity") || str_initFrom.equals("TrainerHomeActivity")) {
                                            btn_PsychoResultFfback.setVisibility(View.GONE);
                                        } else {
                                            // btn_PsychoResultFfback.setVisibility(View.VISIBLE); bharat 19 08 20
                                            btn_PsychoResultFfback.setVisibility(View.GONE);
                                        }
                                    } else
                                        btn_PsychoResultFfback.setVisibility(View.GONE);

                                    if (str_ExamID.equals("4001")) {
                                        str_RsltDesc = CommonClass.getXMLElementValue("ResultDesc", element);
                                        Log.e("", "str_RsltDesc=" + str_RsltDesc);
                                        if (str_RsltDesc.length() == 0) {
//                                        Lnr_ExamResultDesc.setVisibility(View.GONE);
                                            txt_TraitPrsnalityType.setVisibility(View.GONE);
                                        } else {
//                                        Lnr_ExamResultDesc.setVisibility(View.VISIBLE);
//                                        txt_ExamResultDesc.setText(str_ResultType);
                                            txt_TraitPrsnalityType.setText(str_RsltDesc);
                                            txt_TraitPrsnalityType.setVisibility(View.VISIBLE);
                                        }

                                        // JSONParsing for TraitTypes
                                        str_ResultType = CommonClass.getXMLElementValue("ResultTrait", element);
                                        if (str_ResultType.length() == 0) {
                                            Lnr_ExamResultTraits.setVisibility(View.GONE);
                                        } else {
                                            str_ResultType = str_ResultType.replace("[{\"TraitTypesResponseCode\":\"0\"}]",
                                                    "{\"TraitTypesResponseCode\":\"0\"");
                                            str_ResultType = str_ResultType + "}";

                                            arrlist_StrengthTrait.clear();
                                            arrlist_WeaknessTrait.clear();

                                            JSONObject objJsonObject = new JSONObject(str_ResultType);
                                            JSONObject objJsonObject_TraitDtls = objJsonObject.getJSONObject("TraitDetails");
                                            JSONArray Json_Array;
                                            String RespondeCode = objJsonObject_TraitDtls.getString("TraitTypesResponseCode");
                                            if (RespondeCode.equals("0")) {
                                                Lnr_ExamResultTraits.setVisibility(View.VISIBLE);
                                                Json_Array = (JSONArray) objJsonObject_TraitDtls.get("TraitTypes");
//                                                for (int j = 0; j < Json_Array.length(); j++) {

                                                for (int j = 0; j < Json_Array.length(); j++) {
                                                    JSONObject jsonobject = Json_Array.getJSONObject(j);
                                                    if (jsonobject.getString("TraitType").equals("Strength"))
                                                        arrlist_StrengthTrait.add("- " + jsonobject.getString("TraitDesc"));
                                                    else if (jsonobject.getString("TraitType").equals("Weakness"))
                                                        arrlist_WeaknessTrait.add("- " + jsonobject.getString("TraitDesc"));
                                                }
                                            }

                                            if (arrlist_StrengthTrait.size() > 6)
                                                arrlist_StrengthTrait = arrlist_StrengthTrait.subList(0, 6);

                                            if (arrlist_WeaknessTrait.size() > 6)
                                                arrlist_WeaknessTrait = arrlist_WeaknessTrait.subList(0, 6);

                                            custList(arrlist_StrengthTrait);
                                        }

                                        // JSONParsing for ONARD graph
                                        str_ResultType = CommonClass.getXMLElementValue("GraphValue", element);
                                        if (str_ResultType.length() == 0) {
                                            chart.setVisibility(View.GONE);
                                        } else {
                                            JSONObject objJsonObject = new JSONObject(str_ResultType);
                                            JSONObject objJsonObject_TraitDtls = objJsonObject.getJSONObject("ONARDTypes");
                                            String RespondeCode = objJsonObject_TraitDtls.getString("ONARDTypesResponseCode");
                                            JSONArray Json_Array;
                                            if (RespondeCode.equals("0")) {
                                                chart.setVisibility(View.VISIBLE);
                                                Json_Array = (JSONArray) objJsonObject_TraitDtls.get("ONARDType");
                                                for (int j = 0; j < Json_Array.length(); j++) {
                                                    JSONObject jsonobject = Json_Array.getJSONObject(j);
                                                    String str = jsonobject.getString("ONARDValue");
                                                    BARENTRY.add(new BarEntry(Float.parseFloat(str), j));
                                                    BarEntryLabels.add(jsonobject.getString("ONARDCode"));
                                                    arrlist_ONARDCodeDescription.add(jsonobject.getString("ONARDCodeDescription"));
                                                }
                                                PlotGraph(arrlist_ONARDCodeDescription);
                                            } else {
                                                profileVideo.setVisibility(View.GONE);
                                                progsVid.setVisibility(ProgressBar.GONE);
                                                webViewVideo.setVisibility(View.INVISIBLE);
                                            }
                                        }

                                        /*if (CommonClass.isConnected(context)) {
                                            new syncExams(new Callback<String>() {
                                                @Override
                                                public void execute(String result, String status) {

                                                }
                                            }, ViewResultActivity.this, "ViewResultActivity", StaticVariables.UserLoginId, "").execute();
                                        }*/

                                    } else {
                                        btn_PsychoResultFfback.setVisibility(View.GONE);
                                        str_ResultDesc = CommonClass.getXMLElementValue("ResultDesc", element);
                                        if (str_ResultDesc.length() == 0) {
                                            Lnr_AttemptExmResult.setVisibility(View.GONE);
                                        } else {
                                            Lnr_AttemptExmResult.setVisibility(View.VISIBLE);
                                            JSONObject objJsonObject = new JSONObject(str_ResultDesc);
                                            JSONArray objJsonArray = (JSONArray) objJsonObject.get("ResultDesc");
                                            for (int index = 0; index < objJsonArray.length(); index++) {
                                                JSONObject jsonobject = objJsonArray.getJSONObject(index);

                                                if (jsonobject.getString("Result").equals("Pass")) {
                                                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.pass));
                                                } else {
                                                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.failed));
                                                }
//                                    txt_ExamDate.setText("Exam Date : " + jsonobject.getString("ExamDate"));
                                                txt_TotalQue.setText("Total Questions : " + jsonobject.getString("TotalQue"));
                                                txt_AttemptedQue.setText("Attempted Questions : " + jsonobject.getString("AttemptedQue"));
                                                txt_CorrectAns.setText("Correct Answers : " + jsonobject.getString("CorrectAns"));
                                                txt_TotalScore.setText("Score : " + jsonobject.getString("Score"));
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    Log.e("", "Error=" + e.toString());
//                                txt_ExamResult.setText("No data found.");
//                                Lnr_ExamResultDesc.setVisibility(View.GONE);
                                    btn_PsychoResultFfback.setVisibility(View.GONE);
                                    Lnr_ExmResultInit.setVisibility(View.VISIBLE);
                                    ExmResult_progress_bar.setVisibility(View.GONE);
                                    txt_ExmResultInit.setText("Server not responding...");
                                    Lnr_ExamResultTraits.setVisibility(View.GONE);
                                    chart.setVisibility(View.GONE);
                                    Lnr_AttemptExmResult.setVisibility(View.GONE);
                                }
                            }
                        }
                    } else {
//                    txt_ExamResult.setText("No data found.");
//                    Lnr_ExamResultDesc.setVisibility(View.GONE);
                        btn_PsychoResultFfback.setVisibility(View.GONE);
                        Lnr_ExmResultInit.setVisibility(View.VISIBLE);
                        ExmResult_progress_bar.setVisibility(View.GONE);
                        txt_ExmResultInit.setText("Result details not found");
                        Lnr_ExamResultTraits.setVisibility(View.GONE);
                        chart.setVisibility(View.GONE);
                        Lnr_AttemptExmResult.setVisibility(View.GONE);
                        try {
                            int attemptId = DatabaseHelper.CheckAttempts(ViewResultActivity.this,
                                    DatabaseHelper.db, "4001", StaticVariables.UserLoginId);
                            int count = DatabaseHelper.PPExamQueCount(ViewResultActivity.this, DatabaseHelper.db, "4001");
                            if (attemptId <= 0 && count >= 20) {
                                alertCustom("Complete Profiling", "Your Psychometric Profiling is pending, Please proceed for Psychometric Profiling.");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (ResponseCode == 2) {
                    Lnr_ExmResultInit.setVisibility(View.VISIBLE);
//                Lnr_ExmResult.setVisibility(View.GONE);
                    btn_PsychoResultFfback.setVisibility(View.GONE);
                    Lnr_AttemptExmResult.setVisibility(View.GONE);
                    txt_ExmResultInit.setText("Result details not found or Server not responding...");
                    ExmResult_progress_bar.setVisibility(View.GONE);

//                    txt_ExmResultInit.setText("Server not responding...");
                    Toast.makeText(ViewResultActivity.this, "Server not responding...", Toast.LENGTH_LONG).show();
                } else {
                    Lnr_ExmResultInit.setVisibility(View.VISIBLE);
//                Lnr_ExmResult.setVisibility(View.GONE);
                    btn_PsychoResultFfback.setVisibility(View.GONE);
                    Lnr_AttemptExmResult.setVisibility(View.GONE);
                    txt_ExmResultInit.setText("Result details not found or Server not responding...");
                    ExmResult_progress_bar.setVisibility(View.GONE);
                    Toast.makeText(ViewResultActivity.this, "Something went worng...", Toast.LENGTH_LONG).show();
                }

                /*if (str_initFrom.equals("MCQ_QuestionActivity") || str_initFrom.equals("Custom_GridAdapter")
                        || str_initFrom.equals("TrainerGridAdapter")) {
                    checkFeedback();
                }*/

            } catch (Exception e) {
                DatabaseHelper.SaveErroLog(ViewResultActivity.this, DatabaseHelper.db,
                        "ViewResultActivity", "GetResult.onPostExecute()", e.toString());
            }
        }
    }

    public class GetResultFeedBck_QueAns extends AsyncTask<String, String, InputStream> {

//        Callback callback;

        public GetResultFeedBck_QueAns() {

        }

        @Override
        protected void onPreExecute() {
            showProgress(true, "Fetching data...");
        }

        @SuppressLint("NewApi")
        @Override
        protected InputStream doInBackground(String... params) {
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    ViewResultActivity.this.getString(R.string.Exam_NAMESPACE),
                    ViewResultActivity.this.getString(R.string.GetTraitFeedBackQuesAns));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            inputPI1.setName("objXMLDoc");
            try {
                String objXMLDoc = CommonClass.GenerateXML(ViewResultActivity.this, 0, "TraitQuesAnsRequest", new String[]{""}, new String[]{""});
                objXMLDoc = objXMLDoc.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
                inputPI1.setValue(objXMLDoc);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            // Set output SOAP object
            envelope.setOutputSoapObject(request);
            // Create HTTP call object
            HttpTransportSE androidHttpTransport = new HttpTransportSE(ViewResultActivity.this.getString(R.string.Exam_URL));
            InputStream isResponse = null;


            try {
                // Involve Web service
                LPIEEX509TrustManager.allowAllSSL();
//                FakeX509TrustManager.trustSSLCertificate(ViewResultActivity.this);
                androidHttpTransport.call(ViewResultActivity.this.getString(R.string.Exam_SOAP_ACTION) +
                        ViewResultActivity.this.getString(R.string.GetTraitFeedBackQuesAns), envelope);
                // Get the response
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

                String strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + response.toString();
                Log.e("", "response=" + strResponse);

                isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
                VwRsltStream = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));


                Log.e("", "Response=" + isResponse);
            } catch (Exception e) {
                ResponseCode = 2;
                Log.e("", "Error=" + e.toString());
            }

            return isResponse;
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {

            showProgress(false, "");
            if (CommonClass.IsCorrectXMLResponse(inputStream)) {
                try {
                    String ExamId = "";
                    NodeList nList = CommonClass.GetXMLElementNodeList(inputStream, "Questions");
                    for (int i = 0; i < nList.getLength(); i++) {

                        Node node = nList.item(i);
                        if (node.getNodeType() == Node.ELEMENT_NODE) {
                            Element element = (Element) node;
                            DatabaseHelper.SaveQuestionMaster(ViewResultActivity.this, DatabaseHelper.db,
                                    CommonClass.getValue(ViewResultActivity.this, "ExamId", element),
                                    CommonClass.getValue(ViewResultActivity.this, "QuestionId", element),
                                    CommonClass.getValue(ViewResultActivity.this, "QuestionCode", element),
                                    CommonClass.getValue(ViewResultActivity.this, "Question", element),
                                    CommonClass.getValue(ViewResultActivity.this, "NoOfAttemptAllow", element),
                                    CommonClass.getValue(ViewResultActivity.this, "CorrectAnsRemark", element),
                                    CommonClass.getValue(ViewResultActivity.this, "WrongAnsRemark", element),
                                    CommonClass.getValue(ViewResultActivity.this, "OptionCount", element));
                            ExamId = CommonClass.getValue(ViewResultActivity.this, "ExamId", element);
                        }
                    }

                    DatabaseHelper.SaveExamMaster(getApplicationContext(), DatabaseHelper.db, ExamId,
                            "E" + ExamId, "5", "Test feedback",
                            "Test feedback", "N", "5", "N",
                            "" + nList.getLength(), "30", "N", "N",
                            "1", "", "Y", "1");

                    StaticVariables.crnt_QueId = 1;
                    StaticVariables.TblNameForCount = nList.getLength();
                    StaticVariables.crnt_ExmId = BigInteger.valueOf(Long.valueOf(ExamId));
                    StaticVariables.crnt_ExmTypeName = "Test feedback";
                    StaticVariables.crnt_ExmName = "Test feedback";
                    StaticVariables.crnt_ExamType = 5;
                    Intent i = new Intent(ViewResultActivity.this, MCQ_QuestionActivity.class);
                    i.putExtra("initFrom", "ViewResultActivity");
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    ViewResultActivity.this.finish();
                } catch (Exception e) {
                    DatabaseHelper.SaveErroLog(ViewResultActivity.this, DatabaseHelper.db,
                            "ViewResultActivity", "GetResultFeedback.onPostExecute()", e.toString());
                }

            } else if (ResponseCode == 2) {
                Toast.makeText(ViewResultActivity.this, "Server not responding...", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(ViewResultActivity.this, "Something went worng...", Toast.LENGTH_LONG).show();
            }
        }
    }

    private class myChrome extends WebChromeClient {
        private View mCustomView;
        private CustomViewCallback mCustomViewCallback;
        protected FrameLayout mFullscreenContainer;
        View decorView;
        //private int mOriginalOrientation;
        private int mOriginalSystemUiVisibility;

        myChrome() {
        }

        public void onHideCustomView() {
            try {
//            ((FrameLayout) getWindow().getDecorView()).removeView(this.mCustomView);
                mFullscreenContainer.removeView(this.mCustomView);
                mFullscreenContainer.setBackground(getResources().getDrawable(R.color.white));
//            getWindow().getDecorView().setSystemUiVisibility(this.mOriginalSystemUiVisibility);
                showSystemUI();
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                this.mCustomViewCallback.onCustomViewHidden();
                this.mCustomViewCallback = null;
                this.mCustomView = null;
//                this.mFullscreenContainer = null;
//                videoDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onShowCustomView(View paramView, CustomViewCallback paramCustomViewCallback) {
            try {
                mFullscreenContainer = ((FrameLayout) getWindow().getDecorView());
                mFullscreenContainer.setBackground(getResources().getDrawable(R.color.black));
                decorView = getWindow().getDecorView();

                if (this.mCustomView != null) {
                    onHideCustomView();
                    return;
                }
                this.mCustomView = paramView;
                this.mCustomView.setBackground(getResources().getDrawable(R.color.black));
//            this.mOriginalSystemUiVisibility = getWindow().getDecorView().getSystemUiVisibility();
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                //this.mOriginalOrientation = getRequestedOrientation();
                this.mCustomViewCallback = paramCustomViewCallback;
//            ((FrameLayout) getWindow().getDecorView()).addView(this.mCustomView, new FrameLayout.LayoutParams(-1, -1));
                mFullscreenContainer.addView(this.mCustomView);
                mFullscreenContainer.bringToFront();
//            getWindow().getDecorView().setSystemUiVisibility(3846);

                mFullscreenContainer.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            hideSystemUI();
                        }
                    }
                });

                hideSystemUI();
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }
        }

        private void hideSystemUI() {
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_IMMERSIVE
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN);
        }

        private void showSystemUI() {
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
    }

    private class ChromeClient extends WebChromeClient {
        private View mCustomView;
        private WebChromeClient.CustomViewCallback mCustomViewCallback;
        protected FrameLayout mFullscreenContainer;
        private int mOriginalOrientation;
        private int mOriginalSystemUiVisibility;

        ChromeClient() {
        }

        public Bitmap getDefaultVideoPoster() {
            if (mCustomView == null) {
                return null;
            }
            return BitmapFactory.decodeResource(getApplicationContext().getResources(), 2130837573);
        }

        public void onHideCustomView() {
            ((FrameLayout) getWindow().getDecorView()).removeView(this.mCustomView);
            this.mCustomView = null;
            getWindow().getDecorView().setSystemUiVisibility(this.mOriginalSystemUiVisibility);
            setRequestedOrientation(this.mOriginalOrientation);
            this.mCustomViewCallback.onCustomViewHidden();
            this.mCustomViewCallback = null;
        }

        public void onShowCustomView(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback) {
            if (this.mCustomView != null) {
                onHideCustomView();
                return;
            }
            this.mCustomView = paramView;
            this.mOriginalSystemUiVisibility = getWindow().getDecorView().getSystemUiVisibility();
            this.mOriginalOrientation = getRequestedOrientation();
            this.mCustomViewCallback = paramCustomViewCallback;
            ((FrameLayout) getWindow().getDecorView()).addView(this.mCustomView, new FrameLayout.LayoutParams(-1, -1));
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        }
    }

    public void showProfileVideo() {
        try {
            profileVideo.setVisibility(View.VISIBLE);
            progsVid.setVisibility(ProgressBar.VISIBLE);
            progsVid.bringToFront();
            webViewVideo.setVisibility(View.VISIBLE);

            WebSettings settings = webViewVideo.getSettings();
            settings.setJavaScriptEnabled(true);
            settings.setLoadWithOverviewMode(true);
            settings.setUseWideViewPort(true);
            settings.setPluginState(WebSettings.PluginState.ON);
            settings.setSupportZoom(true);
            settings.setBuiltInZoomControls(false);
            settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
            settings.setDomStorageEnabled(true);
            settings.setJavaScriptCanOpenWindowsAutomatically(true);
            settings.setSaveFormData(true);
            webViewVideo.getSettings().setMediaPlaybackRequiresUserGesture(false);
            webViewVideo.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
            webViewVideo.setScrollbarFadingEnabled(true);

//            if (Build.VERSION.SDK_INT >= 19) {
//                webViewVideo.setLayerType(View.LAYER_TYPE_HARDWARE, null);
//            } else {
//                webViewVideo.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                settings.setMixedContentMode(0);
                webViewVideo.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                webViewVideo.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            } else {
                webViewVideo.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            }

            webViewVideo.setWebChromeClient(new ChromeClient());
//            webViewVideo.setWebChromeClient(new WebChromeClient() {
//            });
            webViewVideo.clearCache(true);
            webViewVideo.clearHistory();
            webViewVideo.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        WebView webView = (WebView) v;
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_BACK:
                                if (webView.canGoBack()) {
                                    webView.goBack();
                                    return true;
                                }
                                break;
                        }
                    }
                    return false;
                }
            });

            webViewVideo.setWebViewClient(new WebViewClient());
            webViewVideo.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    //                    progsVid.setVisibility(ProgressBar.VISIBLE);
                    //                    webView.setVisibility(View.INVISIBLE);
                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if (url != null && !url.equalsIgnoreCase("https://www.youtube.com/signin?context=popup&next=https%3A%2F%2Fwww.youtube.com%2Fpost_login&feature=wl_button")) {
                        progsVid.setVisibility(ProgressBar.VISIBLE);
                        webViewVideo.setVisibility(View.GONE);
                    }
                    view.loadUrl(url);
                    return true;
                }


                @Override
                public void onPageCommitVisible(WebView view, String url) {
                    super.onPageCommitVisible(view, url);
                    //                    progsVid.setVisibility(ProgressBar.GONE);
                    //                    webView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    progsVid.setVisibility(ProgressBar.GONE);
                    webViewVideo.setVisibility(View.VISIBLE);
                    super.onPageFinished(view, url);
                }
            });
            if (CommonClass.isConnected(ViewResultActivity.this)) {
                progsVid.setVisibility(View.VISIBLE);
                webViewVideo.setVisibility(View.VISIBLE);
                txt_noInternet.setVisibility(View.GONE);
                //            String url = "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/Ebs6lk9oXG8?rel=0\" frameborder=\"0\" allowfullscreen></iframe>";
                String url = "<iframe width=\"100%\" height=\"100%\" src=" + URL + " frameborder=\"0\" allowfullscreen></iframe>";
                String strBaseURL = "https://img.youtube.com/vi/MK50cGWG0VU/0.jpg";
                webViewVideo.loadDataWithBaseURL(strBaseURL, url, "text/html; charset=utf-8", "utf-8", null);
            } else {
                txt_noInternet.setVisibility(View.VISIBLE);
                progsVid.setVisibility(View.GONE);
                webViewVideo.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void clearWebViewAllCache() {
        try {
            WebStorage.getInstance().deleteAllData();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
        } catch (Exception ignore) {
            //ignore.printStackTrace();
        }

    }
}
