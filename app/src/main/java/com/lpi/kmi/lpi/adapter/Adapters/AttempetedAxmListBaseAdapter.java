package com.lpi.kmi.lpi.adapter.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.ViewResultActivity;
import com.lpi.kmi.lpi.adapter.GetterSetter.GetterSetter;
import com.lpi.kmi.lpi.adapter.GetterSetter.ViewHolder;

import java.util.ArrayList;

/**
 * Created by mustafakachwalla on 14/06/17.
 */

public class AttempetedAxmListBaseAdapter extends BaseAdapter {

    Context context;
    private ArrayList<GetterSetter> custReceiverlst = new ArrayList<GetterSetter>();

    public AttempetedAxmListBaseAdapter(Context context, ArrayList<GetterSetter> custReceiverlst) {
        this.context = context;
        this.custReceiverlst = custReceiverlst;
    }

    @Override
    public int getCount() {
        return custReceiverlst.size();
    }

    @Override
    public Object getItem(int position) {
        return custReceiverlst.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GetterSetter AttmptedExamType = (GetterSetter) this.getItem(position);


        final TextView txt_ExamSrNo;
        final TextView txt_AttemptedExamID;
        final TextView txt_AttemptedCandidateID;
        final TextView txt_AttemptedExamDate;
        final TextView txt_AttemptedExamResult;
        final TextView txt_AttemptedExamName;
        final TextView txt_AttemptedExamDetail;
        final TextView txt_AttemptedExamType;
        final TextView txt_AttemptedId;
        final TextView txt_IsAllQuesMendatory;
        final TextView txt_BtnStartExam, txt_AttemptedExamIsAllowFdback;
        try {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
//        if (convertView == null) {
            convertView = inflater.inflate(R.layout.custom_examtypelist, null);

            txt_ExamSrNo = (TextView) convertView.findViewById(R.id.txt_ExamSrNo);
            txt_AttemptedExamID = (TextView) convertView.findViewById(R.id.txt_ExamId);
            txt_AttemptedCandidateID = (TextView) convertView.findViewById(R.id.txt_ExamCode);
            txt_AttemptedExamDate = (TextView) convertView.findViewById(R.id.txt_AllowNavigationFlag);
            txt_AttemptedExamResult = (TextView) convertView.findViewById(R.id.txt_ShowRandomQuestions);
            txt_AttemptedExamName = (TextView) convertView.findViewById(R.id.txt_ExamTypeName);
            txt_AttemptedExamDetail = (TextView) convertView.findViewById(R.id.txt_ExamTypeDescrptn);
            txt_AttemptedExamType = (TextView) convertView.findViewById(R.id.txt_ExamType);
            txt_AttemptedId = (TextView) convertView.findViewById(R.id.txt_AttemptedExamId);
            txt_BtnStartExam = (TextView) convertView.findViewById(R.id.txt_BtnStartExam);
            txt_AttemptedExamIsAllowFdback = (TextView) convertView.findViewById(R.id.txt_OptionCount);
            txt_IsAllQuesMendatory = (TextView) convertView.findViewById(R.id.txt_IsAllQuesMendatory);

            convertView.setTag(new ViewHolder(txt_ExamSrNo, txt_AttemptedExamID, txt_AttemptedCandidateID, txt_AttemptedExamDate,
                    txt_AttemptedExamResult, txt_AttemptedExamName, txt_AttemptedExamDetail, txt_AttemptedExamType,
                    txt_AttemptedExamIsAllowFdback, txt_AttemptedId, txt_IsAllQuesMendatory));
        /*} else {
            ViewHolder viewHolder = (ViewHolder) convertView.getTag();

            txt_ExamSrNo = viewHolder.getTxt_msgID();
            txt_AttemptedExamID = viewHolder.getTxt_AttemptedExamID();
            txt_AttemptedCandidateID = viewHolder.getTxt_AttemptedCandidateID();

            txt_AttemptedExamDate = viewHolder.getTxt_AttemptedExamDate();
            txt_AttemptedExamResult = viewHolder.getTxt_AttemptedExamResult();
            txt_AttemptedExamName = viewHolder.getTxt_AttemptedExamName();
            txt_AttemptedExamDetail = viewHolder.getTxt_AttemptedExamDetail();
            txt_AttemptedExamType = viewHolder.getTxt_AttemptedExamType();
            txt_AttemptedId = viewHolder.getTxt_AttemptedId();
            txt_AttemptedExamIsAllowFdback = viewHolder.getTxt_AttemptedExamIsAllowFdback();
            txt_IsAllQuesMendatory = viewHolder.getTxt_IsAllQuesMendatory();

            txt_BtnStartExam = (TextView) convertView.findViewById(R.id.txt_BtnStartExam);
        }*/

            txt_BtnStartExam.setText("View Result");
            txt_ExamSrNo.setText("" + AttmptedExamType.getTxt_ExamSrNo());
            txt_AttemptedExamID.setText("" + AttmptedExamType.getTxt_AttemptedExamID());
            txt_AttemptedCandidateID.setText("" + AttmptedExamType.getTxt_AttemptedCandidateID());
            txt_AttemptedExamDate.setText("" + AttmptedExamType.getTxt_AttemptedExamDate());
            txt_AttemptedExamResult.setText("" + AttmptedExamType.getTxt_AttemptedExamResult());
            txt_AttemptedExamName.setText("" + AttmptedExamType.getTxt_AttemptedExamName());
            txt_AttemptedExamDetail.setText("" + AttmptedExamType.getTxt_AttemptedExamDetail());
            txt_AttemptedExamType.setText("" + AttmptedExamType.getTxt_AttemptedExamType());
            txt_AttemptedId.setText("" + AttmptedExamType.getTxt_AttemptedId());
            txt_AttemptedExamIsAllowFdback.setText("" + AttmptedExamType.getTxt_AttemptedExamIsAllowFdback());

//        if(txt_AttemptedExamIsAllowFdback.getText().toString().equals("Y"))
//            txt_BtnStartExam.setVisibility(View.VISIBLE);
//        else
//            txt_BtnStartExam.setVisibility(View.GONE);

            txt_BtnStartExam.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, ViewResultActivity.class);
                    i.putExtra("ExamId", txt_AttemptedExamID.getText().toString());
                    i.putExtra("AttemptedId", txt_AttemptedId.getText().toString());
                    i.putExtra("CandidateId", txt_AttemptedCandidateID.getText().toString());
                    i.putExtra("initFrom", "AttempetedAxmList");
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(i);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "AttemptedAxmListBaseAdapter", "getView()", e.toString());
        }
        return convertView;
    }
}
