package com.lpi.kmi.lpi.adapter.Adapters;

/**
 * Created by mustafakachwalla on 13/04/17.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.EmotionalExcellenceListActivity;
import com.lpi.kmi.lpi.activities.LPI_FAQActivity;
import com.lpi.kmi.lpi.activities.LPI_Lesson_Intro;
import com.lpi.kmi.lpi.activities.MCQ_QuestionActivity;
import com.lpi.kmi.lpi.activities.ViewResultActivity;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.math.BigInteger;

public class Custom_GridAdapter extends BaseAdapter {

    String[] result;
    Context context;
    int[] imageId;
    private static LayoutInflater inflater = null;

    public Custom_GridAdapter(Context context, String[] osNameList, int[] osImages) {
        // TODO Auto-generated constructor stub
        result = osNameList;
        this.context = context;
        imageId = osImages;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView os_text;
        ImageView os_img;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final Holder holder = new Holder();
        final View rowView;

        rowView = inflater.inflate(R.layout.custom_gridview, null);
        holder.os_text = (TextView) rowView.findViewById(R.id.os_texts);
        holder.os_img = (ImageView) rowView.findViewById(R.id.os_images);

        holder.os_text.setText(result[position]);
        holder.os_img.setImageResource(imageId[position]);
        if (position == 5)
            rowView.setAlpha(0.5f);

        rowView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (position == 0) {
//                    if (DatabaseHelper.GetAttemptedExam(context, DatabaseHelper.db, "4001",StaticVariables.UserLoginId) > 0) {
//                        Toast.makeText(context, "If you wish to repeat Psychometric Profiling,\n" +
//                                "please contact your administrator", Toast.LENGTH_LONG).show();
//                    } else {
                    StaticVariables.TestPage_flag = "PsychometricTest";
                    StaticVariables.crnt_QueId = 401;
//                    StaticVariables.QuestionId.clear();
//                    StaticVariables.QuestionId.add(401);
                    StaticVariables.TblNameForCount = 20;
                    StaticVariables.crnt_ExmId = BigInteger.valueOf(4001);
                    StaticVariables.crnt_ExmTypeName = "Psychometric Profiling";
                    StaticVariables.crnt_ExmName = "Psychometric Profiling";
                    StaticVariables.crnt_ExamType = 4;
                    StaticVariables.crnt_ExamDuration = "30";
                    StaticVariables.IsPracticeExam = "N";
                    StaticVariables.IsAllQuesMendatory = "Y";
                    StaticVariables.allowedAttempts = "1";


//                    Intent i=new Intent(context,ParticipantTest_Personal_info.class);
                    Intent i = new Intent(context, MCQ_QuestionActivity.class);
                    i.putExtra("initFrom", "Custom_GridAdapter");
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(i);

//                    }
                } else if (position == 1) {
//                    if (DatabaseHelper.GetAttemptedExam(context,DatabaseHelper.db,"4001")>0)
//                    {
                    StaticVariables.crnt_ExmId = BigInteger.valueOf(4001);
                    Intent i = new Intent(context, ViewResultActivity.class);
                    i.putExtra("ExamId", "" + StaticVariables.crnt_ExmId);
                    i.putExtra("AttemptedId", "");
                    i.putExtra("CandidateId", "" + StaticVariables.UserLoginId);
                    i.putExtra("initFrom", "Custom_GridAdapter");
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(i);
//                    }
//                    else{
//                        Toast.makeText(context,"Please submit your psychometric profiling first!",Toast.LENGTH_LONG).show();
//                    }
                } else if (position == 2) {


//                    StaticVariables.TestPage_flag = "IRDATest";
//
//                    boolean IsExamMasterAvailable = DatabaseHelper.IsExamMasterAvailable(context, DatabaseHelper.db);
//
//                    if (!IsExamMasterAvailable) {
//                        Intent newIntent = new Intent(context, SyncExamMaster.class);
//                        context.startService(newIntent);
//                    }
//                    Intent i=new Intent(context,ParticipantTest_Personal_info.class);
//                    Intent i = new Intent(context, LPILessonQuizActivity.class);
                    Intent i = new Intent(context, LPI_Lesson_Intro.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(i);
                } else if (position == 3) {
                    Intent i = new Intent(context, LPI_FAQActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra("CandidateId", "" + StaticVariables.UserLoginId);
                    i.putExtra("TrainerId", "" + StaticVariables.ParentId);
                    context.startActivity(i);
                } else if (position == 4) {
                    Intent i = new Intent(context, EmotionalExcellenceListActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(i);
                } else if (position == 5) {
//                    Intent i = new Intent(context, EmotionalExcellenceListActivity.class);
//                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    context.startActivity(i);
                }

//                else if (position == 5) {
//                    Intent i = new Intent(context, LicenseDocument.class);
//                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    context.startActivity(i);
//                } else if (position == 6) {
////                    Intent i=new Intent(context,ActivityCategory.class);
////                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
////                    context.startActivity(i);
//                    Intent i = new Intent(context, ActivityCategory.class);
//                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    context.startActivity(i);
//                } else if (position == 7) {
////                    Intent i=new Intent(context,LicenseDocument.class);
////                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
////                    context.startActivity(i);
//                }
                ((Activity) context).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });

        return rowView;
    }
}