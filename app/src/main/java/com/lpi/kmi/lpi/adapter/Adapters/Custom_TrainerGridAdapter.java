package com.lpi.kmi.lpi.adapter.Adapters;

/**
 * Created by bharat on 20/02/18.
 */

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.EmotionalExcellenceListActivity;
import com.lpi.kmi.lpi.activities.LPI_FAQGroupedActivity;
import com.lpi.kmi.lpi.activities.LPI_Lesson_Intro;
import com.lpi.kmi.lpi.activities.LPI_RegistrationActivity;
import com.lpi.kmi.lpi.activities.LPI_SearchActivity;
import com.lpi.kmi.lpi.activities.LPI_StatusActivity;
import com.lpi.kmi.lpi.activities.MCQ_QuestionActivity;
import com.lpi.kmi.lpi.activities.ViewResultActivity;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.math.BigInteger;

public class Custom_TrainerGridAdapter extends BaseAdapter {

    String[] result;
    Context context;
    int[] imageId;
    private static LayoutInflater inflater = null;


    public Custom_TrainerGridAdapter(Context context, String[] osNameList, int[] osImages) {
        // TODO Auto-generated constructor stub
        result = osNameList;
        this.context = context;
        imageId = osImages;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView os_text;
        ImageView os_img;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final Holder holder = new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.custom_gridview, null);
//        rowView.setAlpha(0.5f);

        CardView cardView = (CardView) rowView.findViewById(R.id.cardView);
        cardView.setCardElevation(8);
        holder.os_text = (TextView) rowView.findViewById(R.id.os_texts);
        holder.os_img = (ImageView) rowView.findViewById(R.id.os_images);

        holder.os_text.setText(result[position]);
        holder.os_img.setImageResource(imageId[position]);
//        if (position==1 || position==0){
//            rowView.setAlpha(1);
//        }
        rowView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (position == 0) {
                    StaticVariables.TestPage_flag = "PsychometricTest";
                    StaticVariables.crnt_QueId = 401;
//                    StaticVariables.QuestionId.clear();
//                    StaticVariables.QuestionId.add(401);
                    StaticVariables.TblNameForCount = 20;
                    StaticVariables.crnt_ExmId = BigInteger.valueOf(4001);
                    StaticVariables.crnt_ExmTypeName = "Psychometric Profiling";
                    StaticVariables.crnt_ExmName = "Psychometric Profiling";
                    StaticVariables.crnt_ExamType = 4;
                    StaticVariables.crnt_ExamDuration = "30";
                    StaticVariables.IsPracticeExam = "N";
                    StaticVariables.IsAllQuesMendatory = "Y";
                    StaticVariables.allowedAttempts = "1";
                    context.startActivity(new Intent(context, MCQ_QuestionActivity.class).putExtra("initFrom", "TrainerGridAdapter"));

                } else if (position == 1) {
                    StaticVariables.crnt_ExmId = BigInteger.valueOf(4001);
                    Intent i = new Intent(context, ViewResultActivity.class);
                    i.putExtra("ExamId", "" + StaticVariables.crnt_ExmId);
                    i.putExtra("AttemptedId", "");
                    i.putExtra("CandidateId", "" + StaticVariables.UserLoginId);
                    i.putExtra("initFrom", "TrainerGridAdapter");
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(i);
//                    context.startActivity(new Intent(context, ViewResultActivity.class));
                } else if (position == 2) {
                    context.startActivity(new Intent(context, LPI_RegistrationActivity.class));
                } else if (position == 3) {
                    context.startActivity(new Intent(context, LPI_SearchActivity.class));
                } else if (position == 4) {
                    context.startActivity(new Intent(context, LPI_StatusActivity.class));
                } else if (position == 5) {
//                    context.startActivity(new Intent(context, LPI_FAQActivity.class));
                    context.startActivity(new Intent(context, LPI_Lesson_Intro.class));
                }else if (position == 6) {
//                    context.startActivity(new Intent(context, LPI_FAQActivity.class));
                    context.startActivity(new Intent(context, EmotionalExcellenceListActivity.class));
                } else if (position == 7) {
//                    context.startActivity(new Intent(context, LPI_FAQActivity.class));
                    context.startActivity(new Intent(context, LPI_FAQGroupedActivity.class));
                } else {
                    AlertDialog.Builder builder = CommonClass.DialogOpen(context, "Alert!",
                            "You don't have access to this module, please contact your administrator");

                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                ((Activity) context).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        return rowView;
    }
}