package com.lpi.kmi.lpi.adapter.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lpi.kmi.lpi.R;

/**
 * Created by bharatg on 12-May-18.
 */

public class DailogGridAdapter extends BaseAdapter {


    Context context;
    String[] titles;
    int[] icons;
    private static LayoutInflater inflater = null;


    public DailogGridAdapter(Context context, String[] titles, int[] icons) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.titles = titles;
        this.icons = icons;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return titles.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView title;
        ImageView icon;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.bottom_sheet_grid_item, null);
        holder.title = (TextView) rowView.findViewById(R.id.title);
        holder.icon = (ImageView) rowView.findViewById(R.id.icon);
        holder.title.setText(titles[position]);
        holder.icon.setImageResource(icons[position]);

        return rowView;
    }
}
