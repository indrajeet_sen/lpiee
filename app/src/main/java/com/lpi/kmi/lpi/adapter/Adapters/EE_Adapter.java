package com.lpi.kmi.lpi.adapter.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Training.Adapters.TViewGroup;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.math.BigInteger;
import java.util.ArrayList;
@SuppressLint("Range")
public class EE_Adapter extends RecyclerView.Adapter {
    private onClickListener onClickListener;
    ArrayList<TViewGroup> searchArrayList;
    Context context;
    public ArrayList<String> arrayListQues = new ArrayList<>();
    public ArrayList<String> arrayListSelAns = new ArrayList<>();
    public ArrayList<String> arrayListAnsStatus = new ArrayList<>();
    public ArrayList<String> arrayListCorrAns = new ArrayList<>();

    public EE_Adapter(Context context, ArrayList<TViewGroup> listItems) {
        this.context = context;
        this.searchArrayList = listItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ee_list_row, parent, false);
        return new VHItem(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        try {
            if (searchArrayList.size() > 0) {
                if (searchArrayList.get(position) != null) {
                    VHItem ee_data = (VHItem) holder;
                    ee_data.tv_ee_name.setText(searchArrayList.get(position).getEe_Name());
                    ee_data.tv_ee_desc.setText(searchArrayList.get(position).getEe_Details());
                    ee_data.tv_result.setText("Result\n" + searchArrayList.get(position).getEe_Result());

                    if (searchArrayList.get(position).getEe_Status() != null && searchArrayList.get(position).getEe_Status().trim().equalsIgnoreCase("Y")) {
                        ee_data.lin_menu.setVisibility(View.VISIBLE);
                    } else {
                        ee_data.lin_menu.setVisibility(View.GONE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return searchArrayList.size();
    }

    class VHItem extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_ee_name;
        TextView tv_ee_desc;
        TextView tv_result;
        TextView tv_summery;
        LinearLayout cardView, linearlayout, lin_summary, lin_result, lin_menu, linChild_summery;
        RecyclerView rv_summery;

        public VHItem(View itemView) {
            super(itemView);
            tv_ee_name = (TextView) itemView.findViewById(R.id.tv_ee_name);
            tv_ee_desc = (TextView) itemView.findViewById(R.id.tv_desc);
            tv_result = (TextView) itemView.findViewById(R.id.tv_result);
            tv_summery = (TextView) itemView.findViewById(R.id.tv_summery);
            lin_menu = (LinearLayout) itemView.findViewById(R.id.lin_menu);
            cardView = (LinearLayout) itemView.findViewById(R.id.cardView);
            linearlayout = (LinearLayout) itemView.findViewById(R.id.linearlayout);
            lin_summary = (LinearLayout) itemView.findViewById(R.id.lin_summary);
            lin_result = (LinearLayout) itemView.findViewById(R.id.lin_result);
            linChild_summery = (LinearLayout) itemView.findViewById(R.id.linChild_summery);
            rv_summery = (RecyclerView) itemView.findViewById(R.id.rv_summery);
            rv_summery.setNestedScrollingEnabled(true);
            rv_summery.hasNestedScrollingParent();
            linChild_summery.setVisibility(View.GONE);
            /*cardView.setOnClickListener(this);*/
            linearlayout.setOnClickListener(this);
            //tv_summery.setOnClickListener(this);
            lin_summary.setOnClickListener(this);
            lin_result.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.linearlayout: {
                    onClickListener.onLinearCardViewClick(getAdapterPosition(), v);
                }
                break;
                case R.id.lin_result: {
                    Toast.makeText(context, searchArrayList.get(getAdapterPosition()).getEe_Name()
                            + " Exam result is " + searchArrayList.get(getAdapterPosition()).getEe_Result(), Toast.LENGTH_LONG).show();
                }
                break;
                case R.id.lin_summary: {
                    if (linChild_summery.getVisibility() != View.VISIBLE) {
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.MATCH_PARENT);
                        linChild_summery.setLayoutParams(lp);
                        tv_summery.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_up, 0);
                        onClickListener.onSummeryClick(getAdapterPosition(), v);
                        initializeArray(rv_summery, searchArrayList.get(getAdapterPosition()).getEe_examID());
                        linChild_summery.setVisibility(View.VISIBLE);
                    } else {
                        tv_summery.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_down, 0);
                        onClickListener.onSummeryClick(getAdapterPosition(), v);
                        linChild_summery.setVisibility(View.GONE);
                    }
                }
                break;
            }
        }
    }

    public void setOnItemClickListener(onClickListener onItemClickListener) {
        this.onClickListener = onItemClickListener;
    }

    public interface onClickListener {
        void onLinearCardViewClick(int position, View view);

        void onSummeryClick(int position, View view);
    }

    private void initializeArray(RecyclerView rv_summery, String ExamId) {
        try {
            Cursor mCursor = DatabaseHelper.GetAllAttemptedAnsDetails(context, DatabaseHelper.db,
                    Integer.parseInt(ExamId), StaticVariables.UserLoginId);
            arrayListQues.clear();
            arrayListSelAns.clear();
            arrayListAnsStatus.clear();
            arrayListCorrAns.clear();

            if (mCursor.getCount() > 0) {
                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    String queID = mCursor.getString(mCursor.getColumnIndex("QuestionId"));
                    String attemptID = mCursor.getString(mCursor.getColumnIndex("AttemptId"));

                    String strAttemptedAns = DatabaseHelper.GetAttemptedAns(context, DatabaseHelper.db,
                            ""+ExamId, ""+queID, StaticVariables.UserLoginId, Integer.parseInt(attemptID));
                    String strRightAns = DatabaseHelper.GetRightAns_Exm1(context, DatabaseHelper.db,
                            ""+ExamId, ""+queID);

                    arrayListSelAns.add(strAttemptedAns);
                    arrayListCorrAns.add(strRightAns);
                    if (strRightAns.trim().equalsIgnoreCase(strAttemptedAns)) {
                        arrayListAnsStatus.add("Correct");
                    } else {
                        arrayListAnsStatus.add("Wrong");
                    }
                    Cursor cquery_que = DatabaseHelper.GetQue(context, DatabaseHelper.db, BigInteger.valueOf(Integer.parseInt(ExamId)),
                            Integer.parseInt(queID));
                    if (cquery_que.getCount() > 0) {
                        for (cquery_que.moveToFirst(); !cquery_que.isAfterLast(); cquery_que.moveToNext()) {
                            arrayListQues.add(cquery_que.getString(3));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        MyRecyclerViewAdapter myRecyclerViewAdapter = new MyRecyclerViewAdapter(context, arrayListQues, arrayListSelAns, arrayListAnsStatus, arrayListCorrAns);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rv_summery.setLayoutManager(linearLayoutManager);
        rv_summery.setAdapter(myRecyclerViewAdapter);
    }
}