package com.lpi.kmi.lpi.adapter.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.MCQ_QuestionActivity;
import com.lpi.kmi.lpi.adapter.GetterSetter.GetterSetter;
import com.lpi.kmi.lpi.adapter.GetterSetter.ViewHolder;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by rupeshchakole on 25/05/17.
 */

public class ExamTypeList_BaseAdapter extends BaseAdapter {

    Context context;
    private ArrayList<GetterSetter> custReceiverlst = new ArrayList<GetterSetter>();
    LinearLayout lin_sync_status;

    public ExamTypeList_BaseAdapter() {
    }

    public ExamTypeList_BaseAdapter(Context context, ArrayList<GetterSetter> custReceiverlst, LinearLayout lin_sync_status) {
        this.context = context;
        this.custReceiverlst = custReceiverlst;
        this.lin_sync_status = lin_sync_status;
    }

    @Override
    public int getCount() {
        return custReceiverlst.size();
    }

    @Override
    public Object getItem(int position) {
        return custReceiverlst.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GetterSetter ExamType = (GetterSetter) this.getItem(position);
        final ViewHolder holder;
        final TextView txt_ExamSrNo, txt_ExamId, txt_ExamCode, txt_ExamDetails, txt_ExamTypeName,
                txt_ExamType, txt_BtnStartExam, txt_AllowNavigationFlag, txt_OptionCount,
                txt_TotalQuestions, txt_TotalTimeAllowedInMinutes, txt_IsAllowResultFeedback,
                txt_IsPracticeExam, txt_AllowedAttempt, txt_EffectiveDate, txt_ShowRandomQuestions,
                txt_IsAllQuesMendatory;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.custom_examtypelist, null);
            holder = new ViewHolder();
            txt_ExamSrNo = (TextView) convertView.findViewById(R.id.txt_ExamSrNo);
            txt_ExamId = (TextView) convertView.findViewById(R.id.txt_ExamId);
            txt_ExamCode = (TextView) convertView.findViewById(R.id.txt_ExamCode);
            txt_ExamType = (TextView) convertView.findViewById(R.id.txt_ExamType);
            txt_ExamTypeName = (TextView) convertView.findViewById(R.id.txt_ExamTypeName);
            txt_ExamDetails = (TextView) convertView.findViewById(R.id.txt_ExamTypeDescrptn);
            txt_AllowNavigationFlag = (TextView) convertView.findViewById(R.id.txt_AllowNavigationFlag);
            txt_OptionCount = (TextView) convertView.findViewById(R.id.txt_OptionCount);
            txt_TotalQuestions = (TextView) convertView.findViewById(R.id.txt_TotalQuestions);
            txt_TotalTimeAllowedInMinutes = (TextView) convertView.findViewById(R.id.txt_TotalTimeAllowedInMinutes);
            txt_IsAllowResultFeedback = (TextView) convertView.findViewById(R.id.txt_IsAllowResultFeedback);
            txt_IsPracticeExam = (TextView) convertView.findViewById(R.id.txt_IsPracticeExam);
            txt_AllowedAttempt = (TextView) convertView.findViewById(R.id.txt_AllowedAttempt);
            txt_EffectiveDate = (TextView) convertView.findViewById(R.id.txt_EffectiveDate);
            txt_ShowRandomQuestions = (TextView) convertView.findViewById(R.id.txt_ShowRandomQuestions);
            txt_IsAllQuesMendatory = (TextView) convertView.findViewById(R.id.txt_IsAllQuesMendatory);

            txt_BtnStartExam = (TextView) convertView.findViewById(R.id.txt_BtnStartExam);

            convertView.setTag(new ViewHolder(txt_ExamSrNo, txt_ExamId, txt_ExamCode, txt_ExamDetails,
                    txt_ExamType, txt_ExamTypeName, txt_AllowNavigationFlag, txt_OptionCount, txt_ShowRandomQuestions,
                    txt_TotalQuestions, txt_TotalTimeAllowedInMinutes, txt_IsAllowResultFeedback,
                    txt_IsPracticeExam, txt_AllowedAttempt, txt_EffectiveDate,txt_IsAllQuesMendatory));
        } else {
            holder = (ViewHolder) convertView.getTag();
            txt_ExamSrNo = holder.getTxt_ExamSrNo();
            txt_ExamId = holder.getTxt_ExamId();
            txt_ExamCode = holder.getTxt_ExamCode();
            txt_ExamType = holder.getTxt_ExamType();
            txt_ExamTypeName = holder.getTxt_ExamTypeName();
            txt_ExamDetails = holder.getTxt_ExamDetails();
            txt_AllowNavigationFlag = holder.getTxt_AllowNavigationFlag();
            txt_OptionCount = holder.getTxt_OptionCount();
            txt_TotalQuestions = holder.getTxt_TotalQuestions();
            txt_TotalTimeAllowedInMinutes = holder.getTxt_TotalTimeAllowedInMinutes();
            txt_IsAllowResultFeedback = holder.getTxt_IsAllowResultFeedback();
            txt_IsPracticeExam = holder.getTxt_IsPracticeExam();
            txt_AllowedAttempt = holder.getTxt_AllowedAttempt();
            txt_EffectiveDate = holder.getTxt_EffectiveDate();
            txt_ShowRandomQuestions = holder.getTxt_ShowRandomQuestions();
            txt_IsAllQuesMendatory = holder.getTxt_IsAllQuesMendatory();


            txt_BtnStartExam = (TextView) convertView.findViewById(R.id.txt_BtnStartExam);
        }

        txt_ExamSrNo.setText("" + ExamType.getTxt_ExamSrNo());
        txt_ExamId.setText("" + ExamType.getTxt_ExamId());
        txt_ExamCode.setText("" + ExamType.getTxt_ExamCode());
        txt_ExamType.setText("" + ExamType.getTxt_ExamType());
        txt_ExamTypeName.setText("" + ExamType.getTxt_ExamTypeName());
        txt_ExamDetails.setText("" + ExamType.getTxt_ExamDetails());
        txt_AllowNavigationFlag.setText("" + ExamType.getTxt_AllowNavigationFlag());
        txt_OptionCount.setText("" + ExamType.getTxt_OptionCount());
        txt_TotalQuestions.setText("" + ExamType.getTxt_TotalQuestions());
        txt_TotalTimeAllowedInMinutes.setText("" + ExamType.getTxt_TotalTimeAllowedInMinutes());
        txt_IsAllowResultFeedback.setText("" + ExamType.getTxt_IsAllowResultFeedback());
        txt_IsPracticeExam.setText("" + ExamType.getTxt_IsPracticeExam());
        txt_AllowedAttempt.setText("" + ExamType.getTxt_AllowedAttempt());
        txt_EffectiveDate.setText("" + ExamType.getTxt_EffectiveDate());
        txt_ShowRandomQuestions.setText("" + ExamType.getTxt_ShowRandomQuestions());
        txt_IsAllQuesMendatory.setText("" + ExamType.getTxt_IsAllQuesMendatory());


        txt_BtnStartExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    if (DatabaseHelper.DidExamMastersSynced(context, DatabaseHelper.db, txt_ExamId.getText().toString()) > 0) {

                        StaticVariables.crnt_QueId = DatabaseHelper.GetTopQuestionId(context,
                                DatabaseHelper.db, txt_ExamId.getText().toString());
                        StaticVariables.TblNameForCount = Integer.parseInt(txt_TotalQuestions.getText().toString());
                        StaticVariables.crnt_ExmId = BigInteger.valueOf(Long.valueOf(txt_ExamId.getText().toString()));
                        StaticVariables.crnt_ExmTypeName = txt_ExamTypeName.getText().toString();
                        StaticVariables.crnt_ExmName = txt_ExamDetails.getText().toString();
                        StaticVariables.crnt_ExamType = Integer.parseInt(txt_ExamType.getText().toString());
                        StaticVariables.crnt_ExamDuration = txt_TotalTimeAllowedInMinutes.getText().toString();
                        StaticVariables.IsPracticeExam = txt_IsPracticeExam.getText().toString();
                        StaticVariables.IsAllQuesMendatory = txt_IsAllQuesMendatory.getText().toString();
                        StaticVariables.allowedAttempts = txt_AllowedAttempt.getText().toString();
                        if (StaticVariables.crnt_ExamType == 1)
                            DatabaseHelper.InsertActivityTracker(context, DatabaseHelper.db, StaticVariables.UserLoginId,
                                    "A1060101", "A10601", "Attempted Exam", "", "",
                                    "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "", StaticVariables.UserName, "",
                                    "", "", "Pending");
                        if (StaticVariables.crnt_ExamType == 2)
                            DatabaseHelper.InsertActivityTracker(context, DatabaseHelper.db, StaticVariables.UserLoginId,
                                    "A1060201", "A10602", "Attempted Exam", "", "",
                                    "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "", StaticVariables.UserName, "",
                                    "", "", "Pending");

                        if (StaticVariables.crnt_ExamType == 3)
                            DatabaseHelper.InsertActivityTracker(context, DatabaseHelper.db, StaticVariables.UserLoginId,
                                    "A1060301", "A10603", "Attempted Exam", "", "",
                                    "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "", StaticVariables.UserName, "",
                                    "", "", "Pending");

                        if (StaticVariables.TblNameForCount > 0) {
                            Intent intent = new Intent(context, MCQ_QuestionActivity.class);
                            intent.putExtra("initFrom","ExamTypeList");
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context.startActivity(intent);
                        } else {
                            lin_sync_status.setVisibility(View.VISIBLE);
                        }


                    } else {
                        AlertDialog.Builder builder = CommonClass.DialogOpen(context, "Exam data initializing...",
                                "Still some Question and answers of relative exam are remains to be downloaded,\n" +
                                        "Please try after some time");

                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                dialog.cancel();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                } catch (Exception e) {
                    Log.e("ExamTypeList", "error @ exam list =" + e.toString());
                }

            }
        });

        return convertView;
    }
}
