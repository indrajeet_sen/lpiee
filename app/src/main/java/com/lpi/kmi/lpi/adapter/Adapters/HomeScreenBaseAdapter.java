package com.lpi.kmi.lpi.adapter.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.adapter.GetterSetter.GetterSetter;
import com.lpi.kmi.lpi.adapter.GetterSetter.ViewHolder;
import com.lpi.kmi.lpi.classes.CommonClass;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by mustafakachwalla on 18/04/17.
 */

public class HomeScreenBaseAdapter extends BaseAdapter {

    Context context;
    private ArrayList<GetterSetter> custReceiverlst = new ArrayList<GetterSetter>();

    public HomeScreenBaseAdapter() {
    }

    public HomeScreenBaseAdapter(Context context, ArrayList<GetterSetter> custReceiverlst) {
        this.context = context;
        this.custReceiverlst = custReceiverlst;
    }

    @Override
    public int getCount() {
        return custReceiverlst.size();
    }

    @Override
    public Object getItem(int position) {
        return custReceiverlst.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GetterSetter Home = (GetterSetter) this.getItem(position);
        final ViewHolder holder;

        final TextView txt_msgID;
        final TextView txt_msgHeader;
        final TextView txt_msgDesc;
        final TextView txt_noInternet;
        final ProgressBar progsVid;

        final RelativeLayout _rel_MsgInfo, _rel_msgDesc;
        final ImageView img_msgdrpdwn;
        final WebView webView;
        final VideoView videoView;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.custom_homelist, null);
            txt_msgID = (TextView) convertView.findViewById(R.id.txt_msgId);
            txt_msgHeader = (TextView) convertView.findViewById(R.id.txt_msgHeader);
            txt_msgDesc = (TextView) convertView.findViewById(R.id.txt_msgDesc);
            txt_noInternet = (TextView) convertView.findViewById(R.id.txt_noInternet);
            progsVid = (ProgressBar) convertView.findViewById(R.id.progsVid);
            _rel_MsgInfo = (RelativeLayout) convertView.findViewById(R.id.rel_msgHeader);
            _rel_msgDesc = (RelativeLayout) convertView.findViewById(R.id.rel_msgDesc);
            img_msgdrpdwn = (ImageView) convertView.findViewById(R.id.img_msgDrpdwn);
            webView = (WebView) convertView.findViewById(R.id.webViewVideo);
            videoView = (VideoView) convertView.findViewById(R.id.VideoView);
            convertView.setTag(new ViewHolder(txt_msgID, txt_msgHeader, txt_msgDesc));
        } else {
            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            txt_msgID = viewHolder.getTxt_msgID();
            txt_msgHeader = viewHolder.getTxt_msgHeader();
            txt_msgDesc = viewHolder.getTxt_msgDesc();
            txt_noInternet = (TextView) convertView.findViewById(R.id.txt_noInternet);
            webView = (WebView) convertView.findViewById(R.id.webViewVideo);
            videoView = (VideoView) convertView.findViewById(R.id.VideoView);
            progsVid = (ProgressBar) convertView.findViewById(R.id.progsVid);
            _rel_MsgInfo = (RelativeLayout) convertView.findViewById(R.id.rel_msgHeader);
            img_msgdrpdwn = (ImageView) convertView.findViewById(R.id.img_msgDrpdwn);
            _rel_msgDesc = (RelativeLayout) convertView.findViewById(R.id.rel_msgDesc);
        }


        try {
           /* webView.setWebViewClient(new WebViewClient() {

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {

                    return false;
                }
            });*/

            WebSettings settings = webView.getSettings();
            settings.setJavaScriptEnabled(true);
            settings.setLoadWithOverviewMode(true);
            settings.setUseWideViewPort(true);
            settings.setSupportZoom(true);
            settings.setBuiltInZoomControls(false);
            settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
            settings.setDomStorageEnabled(true);
            webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
            webView.setScrollbarFadingEnabled(true);

//            settings.setJavaScriptCanOpenWindowsAutomatically(true);
            /*settings.setPluginState(WebSettings.PluginState.ON);
            settings.setLoadWithOverviewMode(true);
            settings.setUseWideViewPort(true);
            settings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);*/
            /*webView.clearCache(true);
            webView.clearHistory();
            settings.setMediaPlaybackRequiresUserGesture(false);*/
            if (Build.VERSION.SDK_INT >= 19) {
                webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            } else {
                webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            }
//            webView.setWebViewClient(new WebViewClient());
            webView.setWebChromeClient(new WebChromeClient() {
                @Override
                public void onProgressChanged(WebView view, int newProgress) {
                    super.onProgressChanged(view, newProgress);
                }

                @Override
                public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback) {
                }

                @Override
                public void onHideCustomView() {
                    //do stuff
                }
            });
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
//                    progsVid.setVisibility(ProgressBar.VISIBLE);
//                    webView.setVisibility(View.INVISIBLE);
                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if (url != null && !url.equalsIgnoreCase("https://www.youtube.com/signin?context=popup&next=https%3A%2F%2Fwww.youtube.com%2Fpost_login&feature=wl_button")) {
                        progsVid.setVisibility(ProgressBar.VISIBLE);
                        webView.setVisibility(View.INVISIBLE);
                    }
                    return true;
                }

                @Override
                public void onPageCommitVisible(WebView view, String url) {
                    super.onPageCommitVisible(view, url);
//                    progsVid.setVisibility(ProgressBar.GONE);
//                    webView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    progsVid.setVisibility(ProgressBar.GONE);
                    webView.setVisibility(View.VISIBLE);
                }
            });


            txt_msgID.setText("" + Home.getTxt_msgID());
            txt_msgHeader.setText("" + Home.getTxt_msgHeader());
            txt_msgDesc.setText("" + Home.getTxt_msgDesc());

//            if(txt_msgDesc.getText().toString().contains("http")){
//                txt_msgDesc.setVisibility(View.GONE);
//                webView.setVisibility(View.VISIBLE);
//                webView.loadUrl(txt_msgDesc.getText().toString());
//                img_msgdrpdwn.setImageResource(R.mipmap.ic_dropup_png1);
//            }else {
//                webView.setVisibility(View.GONE);
//                img_msgdrpdwn.setImageResource(R.mipmap.ic_dropdown_png1);
//            }

            if (txt_msgHeader.getText().toString().equals(context.getResources().getString(R.string.str_msgIntro))) {
                _rel_msgDesc.setVisibility(View.VISIBLE);
                img_msgdrpdwn.setImageResource(R.mipmap.ic_dropup_png1);
            } else {
                _rel_msgDesc.setVisibility(View.GONE);
                img_msgdrpdwn.setImageResource(R.mipmap.ic_dropdown_png1);
            }

            _rel_MsgInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (txt_msgDesc.getText().toString().contains("http")) {
                        if (CommonClass.isConnected(context)) {
                            progsVid.setVisibility(View.VISIBLE);
                            txt_msgDesc.setVisibility(View.GONE);
                            webView.setVisibility(View.VISIBLE);
                            String url = "<iframe width=\"100%\" height=\"100%\" src=\"" + txt_msgDesc.getText().toString() + "\" frameborder=\"0\" allowfullscreen></iframe>";
                            String strBaseURL = "https://img.youtube.com/vi/MK50cGWG0VU/0.jpg";
                            webView.loadDataWithBaseURL(strBaseURL, url, "text/html", "utf-8", null);
                        } else {
                            txt_noInternet.setVisibility(View.VISIBLE);
                            progsVid.setVisibility(View.GONE);
                            txt_msgDesc.setVisibility(View.GONE);
                            webView.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        txt_noInternet.setVisibility(View.GONE);
                        progsVid.setVisibility(View.GONE);
                        webView.setVisibility(View.GONE);
                    }

                    if (_rel_msgDesc.getVisibility() == View.VISIBLE) {
                        _rel_msgDesc.setVisibility(View.GONE);
                        img_msgdrpdwn.setImageResource(R.mipmap.ic_dropdown_png1);
                    } else {
                        _rel_msgDesc.setVisibility(View.VISIBLE);
                        img_msgdrpdwn.setImageResource(R.mipmap.ic_dropup_png1);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "HomeScreenBaseAdapter", "getView()", e.toString());
        }
        return convertView;
    }

    public void videoStream(final VideoView videoview) {
        try {
            // Start the MediaController
            FrameLayout.LayoutParams layoutParams =
                    new FrameLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.gravity = Gravity.BOTTOM;

            MediaController mediacontroller = new MediaController(context);
            mediacontroller.setAnchorView(videoview);
            mediacontroller.setLayoutParams(layoutParams);
            // Get the URL from String VideoURL

            Uri video = Uri.parse(getVideoFile());
            videoview.setMediaController(mediacontroller);
            videoview.getHolder().setFixedSize(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            videoview.setVideoURI(video);

        } catch (Exception e) {
            Log.e("videoStream", "Error=" + e.getMessage());
        }

        videoview.requestFocus();
        videoview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                videoview.start();
            }
        });

//        videoview.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                videoview.stopPlayback();
//            }
//        });

    }

    public String getVideoFile() {
        try {
//            String root = Environment.getExternalStoragePublicDirectory(
//                    Environment.DIRECTORY_DOCUMENTS).toString() + "/.www/";
            String root = context.getExternalFilesDir(null).toString() + "/.www/";
            File mFile, mDir;
            mDir = new File(root);
            if (mDir.exists()) {
                mFile = new File(mDir, "Reliance HealthGain Policy - Health.mp4");
                if (mFile.exists()) {
                    return mFile.getAbsolutePath();
                } else {
//                    Toast.makeText(context, "Video not found", Toast.LENGTH_SHORT).show();
                    return "https://www.youtube.com/watch?v=MK50cGWG0VU";
                }
            } else {
//                Toast.makeText(context, "Video not found", Toast.LENGTH_SHORT).show();
//                return "https://www.youtube.com/watch?v=-T4G2E5GoTc&feature=youtu.be";
                return "https://www.youtube.com/watch?v=MK50cGWG0VU";
            }
        } catch (Exception e) {
            Log.e("getVideoFile", "Error=" + e.toString());
            return "https://www.youtube.com/watch?v=MK50cGWG0VU";
        }
    }

}
