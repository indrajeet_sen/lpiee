package com.lpi.kmi.lpi.adapter.Adapters;

import android.content.Context;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.PublicUser.SearchModel;
import com.lpi.kmi.lpi.classes.CircleImageView;
import com.lpi.kmi.lpi.classes.ImageNicer;

import java.util.ArrayList;


/**
 * Created by bharat  on 22/2/2018.
 */
public class LPI_FAQAdapter extends RecyclerView.Adapter {

    private ArrayList<SearchModel> searchArrayList;
    private static String initFrom = "";
    Context context;
    private onClickListener onClickListener;

    public LPI_FAQAdapter(Context context, ArrayList<SearchModel> listItems, String initFrom) {
        this.context = context;
        this.searchArrayList = listItems;
        this.initFrom = initFrom;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.faq_row, parent, false);
        return new VHItem(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        try {
            if (searchArrayList.size() > 0) {
                if (searchArrayList.get(position) != null) {
                    VHItem search_data = (VHItem) holder;
                    search_data.tv_name.setText(searchArrayList.get(position).getName());
                    search_data.tv_mobile.setText(searchArrayList.get(position).getMobile_no());
                    search_data.tv_que.setText(searchArrayList.get(position).getStatus());
                    if ((searchArrayList.get(position).getImage() != null)
                            && !searchArrayList.get(position).getImage().equals("null") && !searchArrayList.get(position).getImage().equals("")) {
                        if (ImageNicer.DecodeBitmapFromByte(context, searchArrayList.get(position).getImage()) == null) {
                            search_data.profile_pic.setImageBitmap(ImageNicer.DecodeBitmapFromResource(context.getResources(), (R.drawable.profile_blank_m), 100, 100));
                        } else {
                            Glide.with(context)
                                    .load(searchArrayList.get(position).getImage())
                                    .into(search_data.profile_pic);
//                            search_data.profile_pic.setImageBitmap(ImageNicer.DecodeBitmapFromByte(context, searchArrayList.get(position).getImage()));
                        }
                    } else {
                        search_data.profile_pic.setImageBitmap(ImageNicer.DecodeBitmapFromResource(context.getResources(), (R.drawable.profile_blank_m), 100, 100));
                    }
                    if (searchArrayList.get(position).getRespCount() == null ||
                            searchArrayList.get(position).getRespCount().equals("") ||
                            searchArrayList.get(position).getRespCount().equals("0")) {
                        search_data.respCount.setText("");
                        search_data.respCount.setVisibility(View.GONE);
                    } else {
                        search_data.respCount.setText(searchArrayList.get(position).getRespCount());
                        search_data.respCount.setVisibility(View.VISIBLE);
                    }

                    if (searchArrayList.get(position).getMobile_no() == null ||
                            searchArrayList.get(position).getMobile_no().equals("")) {
                        search_data.tv_mobile.setText("");
                        search_data.tv_mobile.setVisibility(View.GONE);
                    } else {
                        search_data.tv_mobile.setText(searchArrayList.get(position).getMobile_no());
                        search_data.tv_mobile.setVisibility(View.VISIBLE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return searchArrayList.size();
    }

    public void setOnItemClickListener(onClickListener onItemClickListener) {
        this.onClickListener = onItemClickListener;
    }

    public interface onClickListener {
        void onCardViewClick(int position, View view);

        void onProfileClick(int position, View view);

    }

    class VHItem extends RecyclerView.ViewHolder implements View.OnClickListener {

        CircleImageView profile_pic;
        TextView tv_name;
        TextView tv_mobile;
        TextView tv_que, respCount;
        TextView tv_QuesId, tv_MobRedId, tv_TrainerId, tv_CandidateId;
        ImageView iv_status;
        LinearLayout lin_profile_pic, lin_individual, lin_group;

        CardView cardView;

        public VHItem(View itemView) {
            super(itemView);

            profile_pic = (CircleImageView) itemView.findViewById(R.id.profile_pic);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_mobile = (TextView) itemView.findViewById(R.id.tv_mobile);
            tv_que = (TextView) itemView.findViewById(R.id.tv_que);
            tv_QuesId = (TextView) itemView.findViewById(R.id.tv_QuesId);
            tv_MobRedId = (TextView) itemView.findViewById(R.id.tv_MobRedId);
            tv_TrainerId = (TextView) itemView.findViewById(R.id.tv_TrainerId);
            tv_CandidateId = (TextView) itemView.findViewById(R.id.tv_CandidateId);
            respCount = (TextView) itemView.findViewById(R.id.respCount);
            cardView = (CardView) itemView.findViewById(R.id.cardViewLayout);

            lin_profile_pic = (LinearLayout) itemView.findViewById(R.id.lin_profile_pic);
            lin_individual = (LinearLayout) itemView.findViewById(R.id.lin_individual);
            lin_group = (LinearLayout) itemView.findViewById(R.id.lin_group);

            respCount.setVisibility(View.GONE);

            if (initFrom.equalsIgnoreCase("FQAGroup")) {
                lin_individual.setVisibility(View.GONE);
                lin_group.setVisibility(View.VISIBLE);
            } else {
                lin_group.setVisibility(View.GONE);
                lin_individual.setVisibility(View.VISIBLE);
            }
            profile_pic.setOnClickListener(this);
            cardView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.cardViewLayout: {
                    onClickListener.onCardViewClick(getAdapterPosition(), v);
                }
                break;
                case R.id.profile_pic: {
                    onClickListener.onProfileClick(getAdapterPosition(), v);
                }
                break;
            }
        }
    }

}