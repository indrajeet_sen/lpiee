package com.lpi.kmi.lpi.adapter.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.adapter.GetterSetter.QuizPojo;

import java.util.List;

public class LPI_QuizListAdapter extends RecyclerView.Adapter  {

    onClickListener onClickListener;
    Context context;
    List<QuizPojo> listItems;

    public LPI_QuizListAdapter(Context context, List<QuizPojo> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.quize_list_row, viewGroup, false);
        return new VHItem(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        try {
            if (listItems.size() > 0) {
                if (listItems.get(position) != null) {
                    VHItem holder = (VHItem) viewHolder;
                    holder.tv_quiz_id.setText(listItems.get(position).getTv_quiz_id());
                    holder.tv_quiz_name.setText(listItems.get(position).getTv_quiz_name());
                    holder.tv_quiz_desc.setText(listItems.get(position).getTv_quiz_desc());
                    holder.tv_quiz_completed.setText(listItems.get(position).getTv_quiz_completed());
                    holder.tv_quiz_nextid.setText(listItems.get(position).getTv_quiz_nextid());
                    holder.tv_quiz_result.setText(listItems.get(position).getTv_quiz_result());
                    holder.tv_quiz_tquiz.setText(listItems.get(position).getTv_quiz_tquiz());
                    holder.tv_quiz_attempted.setText(listItems.get(position).getTv_quiz_attempted());
                    holder.tv_quiz_correct.setText(listItems.get(position).getTv_quiz_correct());
                    holder.tv_quiz_accuracy.setText(listItems.get(position).getTv_quiz_accuracy()+"%");

                    if (listItems.get(position).getTv_quiz_completed() != null
                            && listItems.get(position).getTv_quiz_completed().trim().equalsIgnoreCase("Y")) {
                        holder.tv_quiz_start.setVisibility(View.GONE);
                        holder.lin_quiz_menu.setVisibility(View.VISIBLE);
                    } else {
                        holder.lin_quiz_menu.setVisibility(View.GONE);
                        holder.tv_quiz_start.setVisibility(View.VISIBLE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    class VHItem extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_quiz_id,tv_quiz_name,tv_quiz_desc,tv_quiz_completed,tv_quiz_nextid,tv_quiz_result,
                tv_quiz_tquiz,tv_quiz_attempted,tv_quiz_correct,tv_quiz_accuracy,tv_quiz_summery;
        TextView tv_quiz_start,tv_quiz_details;
        LinearLayout lin_quiz_menu,lin_quiz_result,lin_summary,lin_quiz_summary;

        public VHItem(View itemView) {
            super(itemView);
            try {
                tv_quiz_id = (TextView) itemView.findViewById(R.id.tv_quiz_id);
                tv_quiz_name = (TextView) itemView.findViewById(R.id.tv_quiz_name);
                tv_quiz_desc = (TextView) itemView.findViewById(R.id.tv_quiz_desc);
                tv_quiz_completed = (TextView) itemView.findViewById(R.id.tv_quiz_completed);
                tv_quiz_nextid = (TextView) itemView.findViewById(R.id.tv_quiz_next_id);
                tv_quiz_result = (TextView) itemView.findViewById(R.id.tv_quiz_understanding);
                tv_quiz_tquiz = (TextView) itemView.findViewById(R.id.tv_quiz_tquiz);
                tv_quiz_attempted = (TextView) itemView.findViewById(R.id.tv_quiz_attempted);
                tv_quiz_correct = (TextView) itemView.findViewById(R.id.tv_quiz_correct);
                tv_quiz_accuracy = (TextView) itemView.findViewById(R.id.tv_quiz_accuracy);
                tv_quiz_start = (TextView) itemView.findViewById(R.id.tv_quiz_start);
                tv_quiz_summery = (TextView) itemView.findViewById(R.id.tv_quiz_summery);
                tv_quiz_details = (TextView) itemView.findViewById(R.id.tv_quiz_details);

                lin_quiz_menu = (LinearLayout) itemView.findViewById(R.id.lin_quiz_menu);
                lin_quiz_result = (LinearLayout) itemView.findViewById(R.id.lin_quiz_result);
                lin_summary = (LinearLayout) itemView.findViewById(R.id.lin_summary);
                lin_quiz_summary = (LinearLayout) itemView.findViewById(R.id.lin_quiz_summary);

                lin_quiz_menu.setVisibility(View.GONE);
                lin_quiz_summary.setVisibility(View.GONE);

                tv_quiz_start.setOnClickListener(this);
                lin_quiz_result.setOnClickListener(this);
                lin_summary.setOnClickListener(this);
                tv_quiz_details.setOnClickListener(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.tv_quiz_start: {
                    onClickListener.onLinearCardViewClick(getAdapterPosition(), v);
                }
                break;
                case R.id.lin_quiz_result: {
                    Toast.makeText(context, listItems.get(getAdapterPosition()).getTv_quiz_desc()
                            + "- Your understanding is " + listItems.get(getAdapterPosition()).getTv_quiz_result(), Toast.LENGTH_LONG).show();
                }
                break;
                case R.id.lin_summary: {
                    if (lin_quiz_summary.getVisibility() != View.VISIBLE) {
                        lin_quiz_summary.setVisibility(View.VISIBLE);
                        tv_quiz_summery.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_up, 0);
                        onClickListener.onSummeryClick(getAdapterPosition(), v);
                    } else {
                        tv_quiz_summery.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_down, 0);
                        lin_quiz_summary.setVisibility(View.GONE);
                        onClickListener.onSummeryClick(getAdapterPosition(), v);
                    }
                }
                break;
                case R.id.tv_quiz_details:{
                    onClickListener.onLinearCardViewClick(getAdapterPosition(), v);
                }
                break;
            }
        }
    }

    public void setOnItemClickListener(onClickListener onItemClickListener) {
        this.onClickListener = onItemClickListener;
    }

    public interface onClickListener {
        void onLinearCardViewClick(int position, View view);

        void onSummeryClick(int position, View view);
    }
}
