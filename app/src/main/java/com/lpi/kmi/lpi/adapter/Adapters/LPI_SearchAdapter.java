package com.lpi.kmi.lpi.adapter.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.PublicUser.SearchModel;
import com.lpi.kmi.lpi.classes.CircleImageView;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Created by bharat  on 22/2/2018.
 */
public class LPI_SearchAdapter extends RecyclerView.Adapter {

    private static onClickListener onClickListener;
    private Bitmap btpDefaultPic = null;
    private static ArrayList<SearchModel> searchArrayList;
    private Context context;

    public LPI_SearchAdapter(Context context, ArrayList<SearchModel> listItems) {
        this.context = context;
        this.searchArrayList = listItems;
        btpDefaultPic = getDefaultBitmap();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_row, parent, false);
        return new VHItem(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        try {
            if (searchArrayList.size() > 0) {
                if (searchArrayList.get(position) != null) {
                    VHItem search_data = (VHItem) holder;
                    search_data.tv_name.setText(searchArrayList.get(position).getName());
                    search_data.tv_mobile.setText(searchArrayList.get(position).getMobile_no());

                    if (searchArrayList.get(position).getStatus().trim().equalsIgnoreCase("Y")) {
                        search_data.iv_status.setVisibility(View.VISIBLE);
                        search_data.tv_like.setVisibility(View.VISIBLE);
                        search_data.tv_status.setText(searchArrayList.get(position).getProfileName());
                    } else {
                        search_data.iv_status.setVisibility(View.INVISIBLE);
                        search_data.tv_like.setVisibility(View.INVISIBLE);
                        search_data.tv_status.setText(searchArrayList.get(position).getProfileName());
                    }
//
                    /*
                    if ((searchArrayList.get(position).getImage() != null)
                            && !searchArrayList.get(position).getImage().equals("null") && !searchArrayList.get(position).getImage().equals("")) {
                        Bitmap bitmap = ImageNicer.DecodeBitmapFromByte(context, searchArrayList.get(position).getImage());
                        if (bitmap == null) {
                            if (btpDefaultPic == null) {
                                search_data.profile_pic.setImageBitmap(ImageNicer.DecodeBitmapFromResource(context.getResources(), (R.drawable.profile_blank_m), 100, 100));
                            } else {
                                search_data.profile_pic.setImageBitmap(btpDefaultPic);
                            }
                        } else {
                            search_data.profile_pic.setImageBitmap(bitmap);
                        }
                    } else {
                        if (btpDefaultPic == null) {
                            search_data.profile_pic.setImageBitmap(ImageNicer.DecodeBitmapFromResource(context.getResources(), (R.drawable.profile_blank_m), 100, 100));
                        } else {
                            search_data.profile_pic.setImageBitmap(btpDefaultPic);
                        }
//                        search_data.profile_pic.setImageBitmap(ImageNicer.DecodeBitmapFromResource(context.getResources(), (R.drawable.profile_blank_m), 100, 100));
                    }*/
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    @Override
    public int getItemCount() {
        return searchArrayList.size();
    }


    class VHItem extends RecyclerView.ViewHolder implements View.OnClickListener {

        CircleImageView profile_pic;
        TextView tv_name;
        TextView tv_mobile;
        TextView tv_status;
        TextView tv_modify;
        TextView tv_view;
        TextView tv_like;
        ImageView iv_status;

        LinearLayout cardView, linearlayout;

        public VHItem(View itemView) {
            super(itemView);

            profile_pic = (CircleImageView) itemView.findViewById(R.id.profile_pic);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_mobile = (TextView) itemView.findViewById(R.id.tv_mobile);
            tv_status = (TextView) itemView.findViewById(R.id.tv_status);
            tv_view = (TextView) itemView.findViewById(R.id.tv_view);
            tv_modify = (TextView) itemView.findViewById(R.id.tv_modify);
            tv_like = (TextView) itemView.findViewById(R.id.tv_like);
            iv_status = (ImageView) itemView.findViewById(R.id.iv_status);

            profile_pic.setOnClickListener(this);
            iv_status.setOnClickListener(this);
            tv_view.setOnClickListener(this);
            tv_like.setOnClickListener(this);
            tv_modify.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.iv_status: {
                    onClickListener.onStatusBarChartClick(getAdapterPosition(), v);
                }
                break;
                case R.id.profile_pic: {
                    onClickListener.onProfileClick(getAdapterPosition(), v);
                }
                break;
                case R.id.tv_view: {
                    onClickListener.onViewClick(getAdapterPosition(), v);
                }
                break;
                case R.id.tv_modify: {
                    onClickListener.onModifyClick(getAdapterPosition(), v);
                }
                break;
                case R.id.tv_like: {
                    onClickListener.onFeedbackClick(getAdapterPosition(), v);
                }
                break;
            }
        }

    }

    public void setOnItemClickListener(onClickListener onItemClickListener) {
        this.onClickListener = onItemClickListener;
    }

    public interface onClickListener {
        void onCardViewClick(View view);

        void onProfileClick(int position, View view);

        void onStatusBarChartClick(int position, View view);

        void onViewClick(int position, View view);

        void onFeedbackClick(int position, View view);

        void onModifyClick(int position, View view);
    }

    Bitmap getDefaultBitmap() {
        try {
            Resources res = context.getResources();
            Drawable drawable = res.getDrawable(R.drawable.profile_blank_m);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
            byte[] bitMapData = stream.toByteArray();
            return BitmapFactory.decodeByteArray(bitMapData, 0, bitMapData.length);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}