package com.lpi.kmi.lpi.adapter.Adapters;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.lpi.kmi.lpi.BuildConfig;
import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.AsyncUpdateRespStatus;
import com.lpi.kmi.lpi.adapter.GetterSetter.MessageContent;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.LPIEEX509TrustManager;
import com.lpi.kmi.lpi.classes.StaticVariables;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import nu.xom.Serializer;

public class MessageListAdapter extends RecyclerView.Adapter {
    final private List<MessageContent> messageContent;
    Bundle bndlanimation;
    private Context mContext;
    private Animator mCurrentAnimator;
    private int mShortAnimationDuration = 100;
    private ArrayList<String> arrReadMessages = new ArrayList<>();

    public MessageListAdapter(Context context, List<MessageContent> messageContent) {
        this.mContext = context;
        this.messageContent = messageContent;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder = null;
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_container, parent, false);

        view.findViewById(R.id.message_item_send).setVisibility(View.GONE);
        view.findViewById(R.id.message_item_receive).setVisibility(View.GONE);

        if (viewType == 0) {
            viewHolder = new SentMessageHolder(view);
        } else {
            viewHolder = new ReceivedMessageHolder(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        try {
            if (messageContent.get(position).getRespFrom().equals(StaticVariables.UserLoginId)) {
                ((SentMessageHolder) holder).bind(messageContent.get(position));
            } else {
                ((ReceivedMessageHolder) holder).bind(messageContent.get(position));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return messageContent.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (messageContent.get(position).getRespFrom().equals(StaticVariables.UserLoginId))
            return 0;
        else
            return 1;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public String getLocation(String mFileType) {
        String childPath = "";
        if (mFileType.equalsIgnoreCase("image/ief") ||
                mFileType.equalsIgnoreCase("image/jpeg") ||
                mFileType.equalsIgnoreCase("image/png")) {
            childPath = "Images/";
        } else if (mFileType.equalsIgnoreCase("application/vnd.ms-excel") ||
                mFileType.equalsIgnoreCase("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
            childPath = "Documents/";
        } else if (mFileType.trim().equalsIgnoreCase("application/pdf")) {
            childPath = "Documents/";
        } else if (mFileType.trim().equalsIgnoreCase("text/plain")) {
            childPath = "Documents/";
        } else if (mFileType.trim().equalsIgnoreCase("application/msword")) {
            childPath = "Documents/";
        } else if (mFileType.trim().equalsIgnoreCase("application/vnd.ms-powerpoint") ||
                mFileType.trim().equalsIgnoreCase("application/vnd.openxmlformats-officedocument.presentationml.presentation")) {
            childPath = "Documents/";
        } else if (mFileType.trim().equalsIgnoreCase("audio/x-aac") ||
                mFileType.trim().equalsIgnoreCase("audio/mpeg") ||
                mFileType.trim().equalsIgnoreCase("audio/amr")) {
            childPath = "Audios/";
        } else if (mFileType.trim().equalsIgnoreCase("video/mp4") ||
                mFileType.trim().equalsIgnoreCase("video/x-msvideo") ||
                mFileType.trim().equalsIgnoreCase("video/3gpp") ||
                mFileType.trim().equalsIgnoreCase("video/x-msvideo")) {
            childPath = "Videos/";
        }
        return childPath;
    }

    private class SentMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText, mRefId, mStatus, isAttachment, mFileName, mFilePath;
        ImageView imageView, imgIcon, imgDownload, ImgSent;
        LinearLayout messageSend, sendAttachment, sendMessage, sendDateTime;
        ProgressBar progressBar;

        SentMessageHolder(View itemView) {
            super(itemView);
            messageSend = (LinearLayout) itemView.findViewById(R.id.message_item_send);
            sendMessage = (LinearLayout) itemView.findViewById(R.id.sendMessage);
            sendDateTime = (LinearLayout) itemView.findViewById(R.id.linSentDatetime);
            sendAttachment = (LinearLayout) itemView.findViewById(R.id.sendAttachment);
            ImgSent = (ImageView) itemView.findViewById(R.id.imageSent);
            sendAttachment.setVisibility(View.GONE);
            ImgSent.setVisibility(View.GONE);

            messageText = (TextView) itemView.findViewById(R.id.txt_message_send);
            timeText = (TextView) itemView.findViewById(R.id.txt_datetime_send);
            mRefId = (TextView) itemView.findViewById(R.id.mSendRefId);
            mStatus = (TextView) itemView.findViewById(R.id.mStatus);
            imageView = (ImageView) itemView.findViewById(R.id.imgStatus);
            imgIcon = (ImageView) itemView.findViewById(R.id.send_attachment_icon);
            imgDownload = (ImageView) itemView.findViewById(R.id.send_attachment_dnld);
            isAttachment = (TextView) itemView.findViewById(R.id.isAttachment_send);
            mFileName = (TextView) itemView.findViewById(R.id.send_attachment_name);
            mFilePath = (TextView) itemView.findViewById(R.id.mFilePath_send);
            progressBar = (ProgressBar) itemView.findViewById(R.id.send_attachment_progress);
        }

        void bind(final MessageContent message) {
            try {
                try {
                    if (message.getRespStatus().equals("D") && !message.getRespFrom().equalsIgnoreCase(StaticVariables.UserLoginId) &&
                            !arrReadMessages.contains(message.getMobRefId())) {
                        arrReadMessages.add(message.getMobRefId());

                        ContentValues cv = new ContentValues();
                        cv.clear();
                        cv.put("RespStatus", "R");
                        DatabaseHelper.db.update(mContext.getResources().getString(R.string.TBL_FAQ_Conversation), cv,
                                "MobRefId=?", new String[]{message.getMobRefId()});
                        AsyncUpdateRespStatus asyncUpdateRespStatus = new AsyncUpdateRespStatus();
                        if (CommonClass.isConnected(mContext) && asyncUpdateRespStatus.getStatus().RUNNING != AsyncTask.Status.RUNNING) {
                            new AsyncUpdateRespStatus(mContext.getApplicationContext()).execute();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                messageText.setText(message.getRespDesc());
                timeText.setText(message.getRespDtime());
                mRefId.setText(message.getMobRefId());
                mStatus.setText(message.getRespStatus());
                isAttachment.setText(message.getIsAttachment());
                mFileName.setText(message.getmFileName());
                mFilePath.setText(message.getmFilePath());
                ImgSent.setVisibility(View.GONE);
                if (messageText.getText().toString().trim().equalsIgnoreCase("")) {
                    sendMessage.setVisibility(View.GONE);
                } else {
                    sendMessage.setVisibility(View.VISIBLE);
                }
                progressBar.setVisibility(View.GONE);

                if (message.getIsAttachment().equalsIgnoreCase("Y")) {
                    if (message.getmFileType().equalsIgnoreCase("image/ief") ||
                            message.getmFileType().equalsIgnoreCase("image/jpeg") ||
                            message.getmFileType().equalsIgnoreCase("image/png")) {
                        imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_picture));
                        try {
                            String tempfileName = mContext.getExternalFilesDir(Environment.MEDIA_SHARED).toString() + StaticVariables.ParentStoragePath
                                    + getLocation(message.getmFileType().toString()) + message.getmFileName().toString();
//                            File file = new File(message.getmFilePath().toString());
                            File file = new File(tempfileName);
                            if (file.exists()) {
                                Glide.with(mContext)
                                        .load(Uri.fromFile(file))
                                        .apply(new RequestOptions().placeholder(R.drawable.dummy).override(500, 500))
                                        .into(ImgSent);
                                imgDownload.setVisibility(View.GONE);
                                ImgSent.setVisibility(View.VISIBLE);
                            } else {
                                ImgSent.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            ImgSent.setVisibility(View.GONE);
                        }
                    } else if (message.getmFileType().equalsIgnoreCase("application/vnd.ms-excel") ||
                            message.getmFileType().equalsIgnoreCase("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                        imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_microsoft_excel));
                    } else if (message.getmFileType().trim().equalsIgnoreCase("application/pdf")) {
                        imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_pdf));
                    } else if (message.getmFileType().trim().equalsIgnoreCase("text/plain")) {
                        imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_document));
                    } else if (message.getmFileType().trim().equalsIgnoreCase("application/msword")) {
                        imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_microsoft_word));
                    } else if (message.getmFileType().trim().equalsIgnoreCase("application/vnd.ms-powerpoint") ||
                            message.getmFileType().trim().equalsIgnoreCase("application/vnd.openxmlformats-officedocument.presentationml.presentation")) {
                        imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_microsoft_powerpoint));
                    } else if (message.getmFileType().trim().equalsIgnoreCase("audio/x-aac") ||
                            message.getmFileType().trim().equalsIgnoreCase("audio/mpeg")) {
                        imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_audio));
                    } else if (message.getmFileType().trim().equalsIgnoreCase("video/mp4") ||
                            message.getmFileType().trim().equalsIgnoreCase("video/x-msvideo") ||
                            message.getmFileType().trim().equalsIgnoreCase("video/3gpp") ||
                            message.getmFileType().trim().equalsIgnoreCase("video/x-msvideo")) {
                        imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_video));
                    } else {
                        imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_document));
                    }
                    progressBar.setVisibility(View.VISIBLE);
                    sendAttachment.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.GONE);
                    sendAttachment.setVisibility(View.GONE);
                }

                if (message.getIsAttachment().equals("Y")) {

                    String tempfileName = mContext.getExternalFilesDir(Environment.MEDIA_SHARED).toString() + StaticVariables.ParentStoragePath
                            + getLocation(message.getmFileType().toString()) + message.getmFileName().toString();
                    File mfile = new File(tempfileName);

//                    File mfile = new File(message.getmFilePath());
                    if (!mfile.exists()) {
                        imgDownload.setVisibility(View.VISIBLE);
                    } else {
                        imgDownload.setVisibility(View.GONE);
                    }
                }

                if (!message.getIsAttachment().equalsIgnoreCase("Y")
                        && messageText.getText().toString().trim().equalsIgnoreCase("")) {
                    messageSend.setVisibility(View.GONE);
                } else {
                    messageSend.setVisibility(View.VISIBLE);
                }

                if (message.isShowProgress()) {
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.GONE);
                }

                if (message.getRespStatus().equals("D") || message.getRespStatus().equals("R")) {
                    imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_action_double_tick));
                } else {
                    imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_action_tick));
                }

                imgDownload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            imgDownload.setVisibility(View.GONE);
                            String tempfileName = mContext.getExternalFilesDir(Environment.MEDIA_SHARED).toString() + StaticVariables.ParentStoragePath
                                    + getLocation(message.getmFileType().toString()) + message.getmFileName().toString();
                            File mfile = new File(tempfileName);
//                            File mfile = new File(message.getmFilePath());
                            if (!mfile.exists()) {
                                if (CommonClass.isConnected(mContext)) {
                                    progressBar.setVisibility(View.VISIBLE);
                                    CommonClass.CreateDirectories(mContext.getApplicationContext());
                                    new GetDocument(mContext, message.getMobRefId(), progressBar).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                }
                            } else {
                                progressBar.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });


                mFileName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            String tempfileName = mContext.getExternalFilesDir(Environment.MEDIA_SHARED).toString() + StaticVariables.ParentStoragePath
                                    + getLocation(message.getmFileType().toString()) + message.getmFileName().toString();
                            File file = new File(tempfileName);
                            if (file.exists()) {
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                i.setDataAndType(Uri.fromFile(file), CommonClass.getMimeType(file.getAbsolutePath()));
                                i.setDataAndType(FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID+".provider",new File(tempfileName)), CommonClass.getMimeType(file.getAbsolutePath()));
                                i.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                mContext.startActivity(i);
                            } else {
                                Toast.makeText(mContext, "File not found", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(mContext, "File not found", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                ImgSent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            String tempfileName = mContext.getExternalFilesDir(Environment.MEDIA_SHARED).toString() + StaticVariables.ParentStoragePath
                                    + getLocation(message.getmFileType().toString()) + message.getmFileName().toString();
                            File file = new File(tempfileName);
//                            File file = new File(message.getmFilePath().toString());
                            if (file.exists()) {
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                i.setDataAndType(Uri.fromFile(file), CommonClass.getMimeType(file.getAbsolutePath()));
//                                i.setDataAndType(Uri.parse(tempfileName), CommonClass.getMimeType(file.getAbsolutePath()));
                                i.setDataAndType(FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID+".provider",new File(tempfileName)), CommonClass.getMimeType(file.getAbsolutePath()));
                                i.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                mContext.startActivity(i);
                            } else {
                                Toast.makeText(mContext, "File not found", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(mContext, "File not found", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText, mRefId, isAttachment, mFileName, mFilePath;
        ImageView imgIcon, imgDownload, ImgReceived;
        LinearLayout messageReceive, receiveAttachment, receiveMessage, receiveDateTime;
        ProgressBar progressBar;

        ReceivedMessageHolder(View itemView) {
            super(itemView);
            messageReceive = (LinearLayout) itemView.findViewById(R.id.message_item_receive);
            receiveMessage = (LinearLayout) itemView.findViewById(R.id.receiveMessage);
            receiveDateTime = (LinearLayout) itemView.findViewById(R.id.receiveDatetime);
            receiveAttachment = (LinearLayout) itemView.findViewById(R.id.receiveAttachment);
            ImgReceived = (ImageView) itemView.findViewById(R.id.imageReceived);
            receiveMessage.setVisibility(View.GONE);
            receiveAttachment.setVisibility(View.GONE);
            messageReceive.setVisibility(View.GONE);
            ImgReceived.setVisibility(View.GONE);
            messageText = (TextView) itemView.findViewById(R.id.txt_message);
            timeText = (TextView) itemView.findViewById(R.id.txt_datetime);
            mRefId = (TextView) itemView.findViewById(R.id.mRefId);

            imgIcon = (ImageView) itemView.findViewById(R.id.receive_attachment_icon);
            imgDownload = (ImageView) itemView.findViewById(R.id.receive_attachment_dnld);

            isAttachment = (TextView) itemView.findViewById(R.id.isAttachment_receive);
            mFileName = (TextView) itemView.findViewById(R.id.receive_attachment_name);
            mFilePath = (TextView) itemView.findViewById(R.id.mFilePath_receive);
            progressBar = (ProgressBar) itemView.findViewById(R.id.receive_attachment_progress);

            imgDownload.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }

        void bind(final MessageContent message) {
            try {
                try {
                    if (message.getRespStatus().equals("D") && !message.getRespFrom().equalsIgnoreCase(StaticVariables.UserLoginId) &&
                            !arrReadMessages.contains(message.getMobRefId())) {
                        arrReadMessages.add(message.getMobRefId());

                        ContentValues cv = new ContentValues();
                        cv.clear();
                        cv.put("RespStatus", "R");
                        DatabaseHelper.db.update(mContext.getResources().getString(R.string.TBL_FAQ_Conversation), cv,
                                "MobRefId=?", new String[]{message.getMobRefId()});
                        AsyncUpdateRespStatus asyncUpdateRespStatus = new AsyncUpdateRespStatus();
                        if (CommonClass.isConnected(mContext) && asyncUpdateRespStatus.getStatus().RUNNING != AsyncTask.Status.RUNNING) {
                            new AsyncUpdateRespStatus(mContext.getApplicationContext()).execute();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                messageText.setText(message.getRespDesc());
                timeText.setText(message.getRespDtime());
                mRefId.setText(message.getMobRefId());
                isAttachment.setText(message.getIsAttachment());
                mFileName.setText(message.getmFileName());
                mFilePath.setText(message.getmFilePath());
                ImgReceived.setVisibility(View.GONE);
                if (message.getRespDesc().toString().trim().equalsIgnoreCase("")) {
                    receiveMessage.setVisibility(View.GONE);
                } else {
                    receiveMessage.setVisibility(View.VISIBLE);
                    messageReceive.setVisibility(View.VISIBLE);
                }

                if (!message.getIsAttachment().equalsIgnoreCase("Y")
                        && message.getRespDesc().trim().equalsIgnoreCase("")) {
                    receiveMessage.setVisibility(View.GONE);
                    receiveAttachment.setVisibility(View.GONE);
                    messageReceive.setVisibility(View.GONE);
                } else {
                    messageReceive.setVisibility(View.VISIBLE);
                }

                if (message.getIsAttachment().equalsIgnoreCase("Y")) {
                    if (message.getmFileType().equalsIgnoreCase("image/ief") ||
                            message.getmFileType().equalsIgnoreCase("image/jpeg") ||
                            message.getmFileType().equalsIgnoreCase("image/png")) {
                        imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_picture));
                        try {
                            String tempfileName = mContext.getExternalFilesDir(Environment.MEDIA_SHARED).toString() + StaticVariables.ParentStoragePath
                                    + getLocation(message.getmFileType().toString()) + message.getmFileName().toString();
                            File file = new File(tempfileName);
//                            File file = new File(message.getmFilePath().toString());
                            if (file.exists()) {
                                Glide.with(mContext)
                                        .load(Uri.fromFile(file))
                                        .apply(new RequestOptions().placeholder(R.drawable.dummy).override(500, 500))
                                        .into(ImgReceived);
                                imgDownload.setVisibility(View.GONE);
                                ImgReceived.setVisibility(View.VISIBLE);
                            } else {
                                ImgReceived.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            ImgReceived.setVisibility(View.GONE);
                        }
                    } else if (message.getmFileType().equalsIgnoreCase("application/vnd.ms-excel") ||
                            message.getmFileType().equalsIgnoreCase("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                        imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_microsoft_excel));
                    } else if (message.getmFileType().trim().equalsIgnoreCase("application/pdf")) {
                        imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_pdf));
                    } else if (message.getmFileType().trim().equalsIgnoreCase("text/plain")) {
                        imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_document));
                    } else if (message.getmFileType().trim().equalsIgnoreCase("application/msword")) {
                        imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_microsoft_word));
                    } else if (message.getmFileType().trim().equalsIgnoreCase("application/vnd.ms-powerpoint") ||
                            message.getmFileType().trim().equalsIgnoreCase("application/vnd.openxmlformats-officedocument.presentationml.presentation")) {
                        imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_microsoft_powerpoint));
                    } else if (message.getmFileType().trim().equalsIgnoreCase("audio/x-aac") ||
                            message.getmFileType().trim().equalsIgnoreCase("audio/mpeg")) {
                        imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_audio));
                    } else if (message.getmFileType().trim().equalsIgnoreCase("video/mp4") ||
                            message.getmFileType().trim().equalsIgnoreCase("video/x-msvideo") ||
                            message.getmFileType().trim().equalsIgnoreCase("video/3gpp") ||
                            message.getmFileType().trim().equalsIgnoreCase("video/x-msvideo")) {
                        imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_video));
                    } else {
                        imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_document));
                    }

                    imgDownload.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_download));
                    String tempfileName = mContext.getExternalFilesDir(Environment.MEDIA_SHARED).toString() + StaticVariables.ParentStoragePath
                            + getLocation(message.getmFileType().toString()) + message.getmFileName().toString();
                    File mfile = new File(tempfileName);
//                    File mfile = new File(message.getmFilePath());
                    if (!mfile.exists()) {
                        imgDownload.setVisibility(View.VISIBLE);
                    } else {
                        imgDownload.setVisibility(View.GONE);
                    }
                    receiveAttachment.setVisibility(View.VISIBLE);
                } else {
                    receiveAttachment.setVisibility(View.GONE);
                }

                imgDownload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            imgDownload.setVisibility(View.GONE);
                            if (CommonClass.isConnected(mContext)) {
                                progressBar.setVisibility(View.VISIBLE);
                                CommonClass.CreateDirectories(mContext.getApplicationContext());
                                new GetDocument(mContext, message.getMobRefId(), progressBar).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            } else {
                                progressBar.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                mFileName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {

                            Intent i = new Intent(Intent.ACTION_VIEW);
                            String tempfileName = mContext.getExternalFilesDir(Environment.MEDIA_SHARED).toString() + StaticVariables.ParentStoragePath
                                    + getLocation(message.getmFileType().toString()) + message.getmFileName().toString();
                            File file = new File(tempfileName);
//                            File file = new File(message.getmFilePath());
                            if (file.exists()) {
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                i.setDataAndType(Uri.fromFile(file), message.getmFileType());
                                i.setDataAndType(FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID+".provider",new File(tempfileName)), message.getmFileType());
                                i.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                mContext.startActivity(i);
                            } else {
                                Toast.makeText(mContext, "File not found", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(mContext, "File not found", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                ImgReceived.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {

                            Intent i = new Intent(Intent.ACTION_VIEW);
                            String tempfileName = mContext.getExternalFilesDir(Environment.MEDIA_SHARED).toString() + StaticVariables.ParentStoragePath
                                    + getLocation(message.getmFileType().toString()) + message.getmFileName().toString();
                            File file = new File(tempfileName);
//                            File file = new File(message.getmFilePath());
                            if (file.exists()) {
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                i.setDataAndType(Uri.fromFile(file), message.getmFileType());
                                i.setDataAndType(FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID+".provider",new File(tempfileName)), message.getmFileType());
                                i.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                mContext.startActivity(i);
                            } else {
                                Toast.makeText(mContext, "No compatible application", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(mContext, "No compatible application", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private class GetDocument extends AsyncTask<String, String, String> {
        Context context;
        String MobRefId;
        ProgressBar progressBar;

        public GetDocument(Context mContext, String mobRefId, final ProgressBar progressBar) {
            this.context = mContext.getApplicationContext();
            this.MobRefId = mobRefId;
            this.progressBar = progressBar;
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... strings) {
            String strResponse = "";
            try {
                strResponse = getFileFromServer(context, MobRefId);
                strResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + strResponse.toString();
                Log.e("", "response=" + strResponse);

                InputStream isResponse = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
                InputStream is = new ByteArrayInputStream(strResponse.getBytes(StandardCharsets.UTF_8));
                byte[] bytes = null;
                String mFilePath = "", mFileName = "", mFileType = "";
                if (CommonClass.IsCorrectXMLResponse(isResponse)) {
                    NodeList nList = getBytesfrmXML(is, "GetDocument");
                    for (int i = 0; i < nList.getLength(); i++) {
                        Node node = nList.item(i);
                        if (node.getNodeType() == Node.ELEMENT_NODE) {
                            Element element = (Element) node;
                            bytes = Base64.decode(getValue("bytes", element), Base64.DEFAULT);
//                            mFilePath = getValue("mFilePath", element);
                            mFileName = getValue("mFileName", element);
                            mFileType = getValue("mFileType", element);
                        }
                    }
                    mFilePath = context.getExternalFilesDir(Environment.MEDIA_SHARED).toString() + StaticVariables.ParentStoragePath
                            + getLocation(mFileType) + mFileName;
//                    File file = new File(mFilePath);
//                    if (!file.exists())
//                        file.createNewFile();
                    InputStream inputStream = new ByteArrayInputStream(bytes);
                    OutputStream outputStream = new FileOutputStream(mFilePath); // filename.png, .mp3, .mp4 ...
                    try {
                        if (outputStream != null) {
                            //Log.e( TAG, "Output Stream Opened successfully");
                            byte[] buffer = new byte[1024];
                            int bytesRead = 0;
                            while ((bytesRead = inputStream.read(buffer, 0, buffer.length)) >= 0) {
                                outputStream.write(buffer, 0, buffer.length);
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        outputStream.flush();
                        outputStream.close();
                        inputStream.reset();
                        inputStream.close();
                    }

                }
                return "Success";
            } catch (Exception e) {
                e.printStackTrace();
                return "Failed";
            }
        }

        public NodeList getBytesfrmXML(InputStream strXML, String NodeListItem) {
            NodeList nList = null;
            try {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(strXML);
                Element element = doc.getDocumentElement();
                element.normalize();
                nList = doc.getElementsByTagName(NodeListItem);
            } catch (Exception e) {
                Log.e("", "error=" + e.toString());
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "SyncExamMaster", "getExamDatafrmXML()", e.toString());
            }

            return nList;
        }

        public String getValue(String tag, Element element) {
            try {
                NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
                Node node = nodeList.item(0);
                return node.getNodeValue();
            } catch (Exception e) {
                Log.e("", "error=" + e.toString());
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "SyncExamMaster", "getValue()", e.toString());
                return "";
            }
        }

        @Override
        protected void onPostExecute(String response) {
            if (response.equals("Success")) {
                try {
                    for (int j = 0; j < messageContent.size(); j++) {
                        if (messageContent.get(j).getMobRefId().equals(MobRefId)) {
                            messageContent.get(j).setShowProgress(false);
                            notifyItemChanged(j);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    MessageListAdapter.this.notify();
                }
                progressBar.setVisibility(View.GONE);
                CommonClass.createNotification(context.getApplicationContext(), "Download Complete", "New file is ready to use", "");
            }
        }

        private String getFileFromServer(Context context, String MobRefId) {
            String strResp = "";
            org.ksoap2.serialization.SoapObject request = new org.ksoap2.serialization.SoapObject(
                    context.getString(R.string.Exam_NAMESPACE),
                    context.getString(R.string.GetDocument));

            // property which holds input parameters
            PropertyInfo inputPI1 = new PropertyInfo();
            String strReqXML = reqBuild(context, MobRefId);
            inputPI1.setName("objXMLDoc");
            try {
                inputPI1.setValue(strReqXML);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            inputPI1.setType(String.class);
            request.addProperty(inputPI1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(context.getString(R.string.Exam_URL), 900000);

            try {
                LPIEEX509TrustManager.allowAllSSL();
                androidHttpTransport.call(context.getString(R.string.Exam_SOAP_ACTION) +
                        context.getString(R.string.GetDocument), envelope);
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strResp = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return strResp;
        }

        private String reqBuild(Context context, String MobRef) {

            String str_XML = "";
            nu.xom.Element root = new nu.xom.Element("GetDocument");

            nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
            AppUserId.appendChild("LPIUser");

            nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
            AppPassword.appendChild("pass@123");

            nu.xom.Element AppID = new nu.xom.Element("AppID");
            AppID.appendChild("1");

            nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
            CallingMode.appendChild("Mobile");

            nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
            ServiceVersionID.appendChild("1");

            nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
            UniqRefID.appendChild("121");

            nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
            IMEINo.appendChild(CommonClass.getIMEINumber(context));

            nu.xom.Element InscType = new nu.xom.Element("InscType");
            InscType.appendChild("" + StaticVariables.crnt_InscType);

            nu.xom.Element MobRefId = new nu.xom.Element("MobRefId");
            MobRefId.appendChild("" + MobRef);

            root.appendChild(AppUserId);
            root.appendChild(AppPassword);
            root.appendChild(AppID);
            root.appendChild(CallingMode);
            root.appendChild(ServiceVersionID);
            root.appendChild(UniqRefID);
            root.appendChild(IMEINo);
            root.appendChild(InscType);
            root.appendChild(MobRefId);

            nu.xom.Document doc = new nu.xom.Document(root);
            try {
                OutputStream out = new ByteArrayOutputStream();
                Serializer serializer = new Serializer(System.out, "UTF-8");
                serializer.setOutputStream(out);
                serializer.setIndent(4);
                serializer.setMaxLength(64);
                serializer.write(doc);
                str_XML = out.toString();
                str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
                str_XML = str_XML.replaceAll("\n", "");
                str_XML = str_XML.replaceAll("\r", "");
                str_XML = str_XML.replaceAll("     ", "");
                str_XML = str_XML.replaceAll("         ", "");
                str_XML = str_XML.trim();
                Log.e("", "XMLStr==" + str_XML);
            } catch (IOException e) {
                Log.e("", "GetDocument Error=" + e.toString());
                DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                        "GetDocument", "reqBuild", e.toString());
            }
            return str_XML;
        }
    }


}