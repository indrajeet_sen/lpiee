package com.lpi.kmi.lpi.adapter.Adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lpi.kmi.lpi.R;

import java.util.ArrayList;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    Context context;
    ArrayList<String> arrayListQues = new ArrayList<>();
    ArrayList<String> arrayListSelAns = new ArrayList<>();
    ArrayList<String> arrayListAnsStatus = new ArrayList<>();
    ArrayList<String> arrayListCorrAns = new ArrayList<>();

    public MyRecyclerViewAdapter(Context context, ArrayList<String> arrayListQues,
                                 ArrayList<String> arrayListSelAns, ArrayList<String> arrayListAnsStatus,
                                 ArrayList<String> arrayListCorrAns) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.arrayListQues = arrayListQues;
        this.arrayListSelAns = arrayListSelAns;
        this.arrayListAnsStatus = arrayListAnsStatus;
        this.arrayListCorrAns = arrayListCorrAns;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.summery_list_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txt_Ques_value.setText(arrayListQues.get(position));
        holder.txt_Selected_ans_value.setText(arrayListSelAns.get(position));
        holder.txt_Ans_Status_value.setText(arrayListAnsStatus.get(position) + " Answer");
        if (!arrayListAnsStatus.get(position).trim().equalsIgnoreCase("Correct")) {
            holder.txt_Corr_ans_value.setText(arrayListCorrAns.get(position));
            holder.lin_Corr_ans.setVisibility(View.VISIBLE);
        } else {
            holder.lin_Corr_ans.setVisibility(View.GONE);
            holder.txt_Corr_ans_value.setText("-");
        }
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return arrayListQues.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txt_Ques_value;
        TextView txt_Selected_ans_value;
        TextView txt_Ans_Status_value;
        TextView txt_Corr_ans_value;
        LinearLayout lin_Corr_ans;

        ViewHolder(View itemView) {
            super(itemView);
            txt_Ques_value = (TextView) itemView.findViewById(R.id.txt_Ques_value);
            txt_Selected_ans_value = (TextView) itemView.findViewById(R.id.txt_Selected_ans_value);
            txt_Ans_Status_value = (TextView) itemView.findViewById(R.id.txt_Ans_Status_value);
            txt_Corr_ans_value = (TextView) itemView.findViewById(R.id.txt_Corr_ans_value);
            lin_Corr_ans = (LinearLayout) itemView.findViewById(R.id.lin_Corr_ans);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
