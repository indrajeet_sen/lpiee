package com.lpi.kmi.lpi.adapter.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.adapter.GetterSetter.GetterSetter;
import com.lpi.kmi.lpi.adapter.GetterSetter.ViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mustafakachwalla on 20/06/17.
 */

public class PersonalityTraits_BaseAdapter extends BaseAdapter {

    Context context;
    private List<GetterSetter> custReceiverlst=new ArrayList<GetterSetter>();

    public PersonalityTraits_BaseAdapter(Context context, List<GetterSetter> custReceiverlst) {
        this.context = context;
        this.custReceiverlst = custReceiverlst;
    }

    @Override
    public int getCount() {
        return custReceiverlst.size();
    }

    @Override
    public Object getItem(int position) {
        return custReceiverlst.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        GetterSetter TraitsGetterSetter=(GetterSetter) this.getItem(position);
        final TextView txt_TraitResult;
        LayoutInflater inflater = ( LayoutInflater )context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if(convertView==null) {
            convertView = inflater.inflate(R.layout.custom_traitresult, null);

            txt_TraitResult= (TextView) convertView.findViewById(R.id.txt_TraitResult);

            convertView.setTag(new ViewHolder(txt_TraitResult));
        }
        else {
            ViewHolder viewHolder = (ViewHolder) convertView.getTag();

            txt_TraitResult = viewHolder.getTxt_TraitResult();
        }

        txt_TraitResult.setText(""+TraitsGetterSetter.getTxt_TraitResult());
        return convertView;
    }
}
