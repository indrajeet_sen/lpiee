package com.lpi.kmi.lpi.adapter.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.adapter.GetterSetter.GetterSetter;
import com.lpi.kmi.lpi.adapter.GetterSetter.ViewHolder;

import java.util.ArrayList;

/**
 * Created by mustafakachwalla on 09/02/17.
 */

public class QuestionBaseAdapter extends BaseAdapter {

    Context context;
    private ArrayList<GetterSetter> custReceiverlst=new ArrayList<GetterSetter>();

    public QuestionBaseAdapter() {
    }

    public QuestionBaseAdapter(Context context, ArrayList<GetterSetter> custReceiverlst) {
        this.context = context;
        this.custReceiverlst = custReceiverlst;
    }

    @Override
    public int getCount() {
        return custReceiverlst.size();
    }

    @Override
    public Object getItem(int i) {
        return custReceiverlst.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        GetterSetter Que=(GetterSetter) this.getItem(position);
        final ViewHolder holder;

        final TextView txt_SrNo;
        final TextView txt_que;

        LayoutInflater inflater = ( LayoutInflater )context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if(convertView==null) {
            convertView = inflater.inflate(R.layout.cutome_test_query_listrow, null);
            holder=new ViewHolder();
            txt_SrNo= (TextView) convertView.findViewById(R.id.txt_SrNo);
            txt_que= (TextView) convertView.findViewById(R.id.txt_que);

            convertView.setTag(new ViewHolder(txt_SrNo,txt_que));
        }
        else {
            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            txt_SrNo = viewHolder.getTxt_SrNo();
            txt_que = viewHolder.getTxt_Que();
        }


        txt_SrNo.setText(""+Que.getTxt_SrNo());
        txt_que.setText(""+Que.getTxt_Que());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i=new Intent(context,MCQ_QuestionActivity.class);
//                context.startActivity(i);
                Toast.makeText(context,"Work in progress",Toast.LENGTH_SHORT).show();
            }
        });
        return convertView;
    }
}
