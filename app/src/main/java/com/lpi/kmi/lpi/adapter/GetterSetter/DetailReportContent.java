package com.lpi.kmi.lpi.adapter.GetterSetter;

public class DetailReportContent {
    String strNewsId;
    String strNews;
    String strNewsDesc;

    String ExamId, QuesId, Question,status;

    public DetailReportContent(){

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExamId() {
        return ExamId;
    }

    public void setExamId(String examId) {
        ExamId = examId;
    }

    public String getQuesId() {
        return QuesId;
    }

    public void setQuesId(String quesId) {
        QuesId = quesId;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getStrNewsId() {
        return strNewsId;
    }

    public void setStrNewsId(String strNewsId) {
        this.strNewsId = strNewsId;
    }

    public String getStrNews() {
        return strNews;
    }

    public void setStrNews(String strNews) {
        this.strNews = strNews;
    }

    public String getStrNewsDesc() {
        return strNewsDesc;
    }

    public void setStrNewsDesc(String strNewsDesc) {
        this.strNewsDesc = strNewsDesc;
    }
}
