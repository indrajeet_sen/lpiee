package com.lpi.kmi.lpi.adapter.GetterSetter;

/**
 * Created by mustafakachwalla on 09/02/17.
 */

public class GetterSetter {

    //GetterSetter For Question
    private String txt_SrNo;
    private String txt_Que ;

    //GetterSetter For Home screen Messages
    private String txt_msgID;
    private String txt_msgHeader;
    private String txt_msgDesc;

    //GetterSetter For Exam Type list
    private String txt_ExamSrNo;
    private String txt_ExamId;
    private String txt_ExamCode;
    private String txt_ExamDetails;
    private String txt_ExamType;
    private String txt_ExamTypeName;
    private String txt_AllowNavigationFlag;
    private String txt_OptionCount;
    private String txt_ShowRandomQuestions;
    private String txt_TotalQuestions;
    private String txt_TotalTimeAllowedInMinutes;
    private String txt_IsAllowResultFeedback;
    private String txt_IsPracticeExam;
    private String txt_AllowedAttempt;
    private String txt_EffectiveDate;
    private String txt_IsAllQuesMendatory;


    //GetterSetter For Attempted Exam list
    private String txt_AttemptedExamID;
    private String txt_AttemptedCandidateID;
    private String txt_AttemptedExamDate;
    private String txt_AttemptedExamResult;
    private String txt_AttemptedExamName;
    private String txt_AttemptedExamDetail;
    private String txt_AttemptedExamType;
    private String txt_AttemptedId;
    private String txt_AttemptedExamIsAllowFdback;

    //GetterSetter for different traits in View Result
    private String txt_TraitResult;

    // for News feeds
    String strNewsId;
    private String strNews;
    private String strNewsDesc;

    public GetterSetter() {

    }

    public String getStrNewsId() {
        return strNewsId;
    }

    public void setStrNewsId(String strNewsId) {
        this.strNewsId = strNewsId;
    }

    public String getStrNews() {
        return strNews;
    }

    public void setStrNews(String strNews) {
        this.strNews = strNews;
    }

    public String getStrNewsDesc() {
        return strNewsDesc;
    }

    public void setStrNewsDesc(String strNewsDesc) {
        this.strNewsDesc = strNewsDesc;
    }

    public GetterSetter(String txt_TraitResult) {
        this.txt_TraitResult = txt_TraitResult;
    }

    public GetterSetter(String txt_SrNo, String txt_Que) {
        this.txt_SrNo = txt_SrNo;
        this.txt_Que = txt_Que;
    }

    public GetterSetter(String txt_msgID, String txt_msgHeader, String txt_msgDesc) {
        this.txt_msgID = txt_msgID;
        this.txt_msgHeader = txt_msgHeader;
        this.txt_msgDesc = txt_msgDesc;
    }

    public GetterSetter(String txt_ExamSrNo, String txt_ExamId, String txt_ExamCode,
                        String txt_ExamDetails, String txt_ExamType, String txt_ExamTypeName,
                        String txt_AllowNavigationFlag, String txt_OptionCount,String txt_ShowRandomQuestions,
                        String txt_TotalQuestions, String txt_TotalTimeAllowedInMinutes,
                        String txt_IsAllowResultFeedback, String txt_IsPracticeExam,
                        String txt_AllowedAttempt, String txt_EffectiveDate,String txt_IsAllQuesMendatory) {
        this.txt_ExamSrNo = txt_ExamSrNo;
        this.txt_ExamId = txt_ExamId;
        this.txt_ExamCode = txt_ExamCode;
        this.txt_ExamDetails = txt_ExamDetails;
        this.txt_ExamType = txt_ExamType;
        this.txt_ExamTypeName = txt_ExamTypeName;
        this.txt_AllowNavigationFlag = txt_AllowNavigationFlag;
        this.txt_OptionCount = txt_OptionCount;
        this.txt_ShowRandomQuestions = txt_ShowRandomQuestions;
        this.txt_TotalQuestions = txt_TotalQuestions;
        this.txt_TotalTimeAllowedInMinutes = txt_TotalTimeAllowedInMinutes;
        this.txt_IsAllowResultFeedback = txt_IsAllowResultFeedback;
        this.txt_IsPracticeExam = txt_IsPracticeExam;
        this.txt_AllowedAttempt = txt_AllowedAttempt;
        this.txt_EffectiveDate = txt_EffectiveDate;
        this.txt_IsAllQuesMendatory = txt_IsAllQuesMendatory;
    }

    public GetterSetter(String txt_ExamSrNo,String txt_AttemptedExamID, String txt_AttemptedCandidateID,
                        String txt_AttemptedExamDate, String txt_AttemptedExamResult, String txt_AttemptedExamName,
                        String txt_AttemptedExamDetail, String txt_AttemptedExamType, String txt_AttemptedExamIsAllowFdback,
                        String txt_AttemptedID,String txt_IsAllQuesMendatory) {
        this.txt_ExamSrNo = txt_ExamSrNo;
        this.txt_AttemptedExamID = txt_AttemptedExamID;
        this.txt_AttemptedCandidateID = txt_AttemptedCandidateID;
        this.txt_AttemptedExamDate = txt_AttemptedExamDate;
        this.txt_AttemptedExamResult = txt_AttemptedExamResult;
        this.txt_AttemptedExamName = txt_AttemptedExamName;
        this.txt_AttemptedExamDetail = txt_AttemptedExamDetail;
        this.txt_AttemptedExamType = txt_AttemptedExamType;
        this.txt_AttemptedId=txt_AttemptedID;
        this.txt_AttemptedExamIsAllowFdback = txt_AttemptedExamIsAllowFdback;
        this.txt_IsAllQuesMendatory = txt_IsAllQuesMendatory;
    }

    public String getTxt_TraitResult() {
        return txt_TraitResult;
    }

    public void setTxt_TraitResult(String txt_TraitResult) {
        this.txt_TraitResult = txt_TraitResult;
    }

    public String getTxt_AttemptedExamIsAllowFdback() {
        return txt_AttemptedExamIsAllowFdback;
    }

    public void setTxt_AttemptedExamIsAllowFdback(String txt_AttemptedExamIsAllowFdback) {
        this.txt_AttemptedExamIsAllowFdback = txt_AttemptedExamIsAllowFdback;
    }

    public String getTxt_AttemptedExamID() {
        return txt_AttemptedExamID;
    }

    public void setTxt_AttemptedExamID(String txt_AttemptedExamID) {
        this.txt_AttemptedExamID = txt_AttemptedExamID;
    }

    public String getTxt_AttemptedId() {
        return txt_AttemptedId;
    }

    public void setTxt_AttemptedId(String txt_AttemptedId) {
        this.txt_AttemptedId = txt_AttemptedId;
    }

    public String getTxt_AttemptedCandidateID() {
        return txt_AttemptedCandidateID;
    }

    public void setTxt_AttemptedCandidateID(String txt_AttemptedCandidateID) {
        this.txt_AttemptedCandidateID = txt_AttemptedCandidateID;
    }

    public String getTxt_AttemptedExamDate() {
        return txt_AttemptedExamDate;
    }

    public void setTxt_AttemptedExamDate(String txt_AttemptedExamDate) {
        this.txt_AttemptedExamDate = txt_AttemptedExamDate;
    }

    public String getTxt_AttemptedExamResult() {
        return txt_AttemptedExamResult;
    }

    public void setTxt_AttemptedExamResult(String txt_AttemptedExamResult) {
        this.txt_AttemptedExamResult = txt_AttemptedExamResult;
    }

    public String getTxt_AttemptedExamName() {
        return txt_AttemptedExamName;
    }

    public void setTxt_AttemptedExamName(String txt_AttemptedExamName) {
        this.txt_AttemptedExamName = txt_AttemptedExamName;
    }

    public String getTxt_AttemptedExamDetail() {
        return txt_AttemptedExamDetail;
    }

    public void setTxt_AttemptedExamDetail(String txt_AttemptedExamDetail) {
        this.txt_AttemptedExamDetail = txt_AttemptedExamDetail;
    }

    public String getTxt_AttemptedExamType() {
        return txt_AttemptedExamType;
    }

    public void setTxt_AttemptedExamType(String txt_AttemptedExamType) {
        this.txt_AttemptedExamType = txt_AttemptedExamType;
    }

    public String getTxt_ShowRandomQuestions() {
        return txt_ShowRandomQuestions;
    }

    public void setTxt_ShowRandomQuestions(String txt_ShowRandomQuestions) {
        this.txt_ShowRandomQuestions = txt_ShowRandomQuestions;
    }

    public String getTxt_AllowNavigationFlag() {
        return txt_AllowNavigationFlag;
    }

    public void setTxt_AllowNavigationFlag(String txt_AllowNavigationFlag) {
        this.txt_AllowNavigationFlag = txt_AllowNavigationFlag;
    }

    public String getTxt_OptionCount() {
        return txt_OptionCount;
    }

    public void setTxt_OptionCount(String txt_OptionCount) {
        this.txt_OptionCount = txt_OptionCount;
    }

    public String getTxt_TotalQuestions() {
        return txt_TotalQuestions;
    }

    public void setTxt_TotalQuestions(String txt_TotalQuestions) {
        this.txt_TotalQuestions = txt_TotalQuestions;
    }

    public String getTxt_TotalTimeAllowedInMinutes() {
        return txt_TotalTimeAllowedInMinutes;
    }

    public void setTxt_TotalTimeAllowedInMinutes(String txt_TotalTimeAllowedInMinutes) {
        this.txt_TotalTimeAllowedInMinutes = txt_TotalTimeAllowedInMinutes;
    }

    public String getTxt_IsAllowResultFeedback() {
        return txt_IsAllowResultFeedback;
    }

    public void setTxt_IsAllowResultFeedback(String txt_IsAllowResultFeedback) {
        this.txt_IsAllowResultFeedback = txt_IsAllowResultFeedback;
    }

    public String getTxt_IsPracticeExam() {
        return txt_IsPracticeExam;
    }

    public void setTxt_IsPracticeExam(String txt_IsPracticeExam) {
        this.txt_IsPracticeExam = txt_IsPracticeExam;
    }

    public String getTxt_AllowedAttempt() {
        return txt_AllowedAttempt;
    }

    public void setTxt_AllowedAttempt(String txt_AllowedAttempt) {
        this.txt_AllowedAttempt = txt_AllowedAttempt;
    }

    public String getTxt_EffectiveDate() {
        return txt_EffectiveDate;
    }

    public void setTxt_EffectiveDate(String txt_EffectiveDate) {
        this.txt_EffectiveDate = txt_EffectiveDate;
    }

    public String getTxt_ExamSrNo() {
        return txt_ExamSrNo;
    }

    public void setTxt_ExamSrNo(String txt_ExamSrNo) {
        this.txt_ExamSrNo = txt_ExamSrNo;
    }

    public String getTxt_ExamTypeName() {
        return txt_ExamTypeName;
    }

    public void setTxt_ExamTypeName(String txt_ExamTypeName) {
        this.txt_ExamTypeName = txt_ExamTypeName;
    }

    public String getTxt_ExamId() {
        return txt_ExamId;
    }

    public void setTxt_ExamId(String txt_ExamId) {
        this.txt_ExamId = txt_ExamId;
    }

    public String getTxt_ExamCode() {
        return txt_ExamCode;
    }

    public void setTxt_ExamCode(String txt_ExamCode) {
        this.txt_ExamCode = txt_ExamCode;
    }

    public String getTxt_ExamDetails() {
        return txt_ExamDetails;
    }

    public void setTxt_ExamDetails(String txt_ExamDetails) {
        this.txt_ExamDetails = txt_ExamDetails;
    }

    public String getTxt_ExamType() {
        return txt_ExamType;
    }

    public void setTxt_ExamType(String txt_ExamType) {
        this.txt_ExamType = txt_ExamType;
    }

    public String getTxt_msgID() {
        return txt_msgID;
    }

    public void setTxt_msgID(String txt_msgID) {
        this.txt_msgID = txt_msgID;
    }

    public String getTxt_msgHeader() {
        return txt_msgHeader;
    }

    public void setTxt_msgHeader(String txt_msgHeader) {
        this.txt_msgHeader = txt_msgHeader;
    }

    public String getTxt_msgDesc() {
        return txt_msgDesc;
    }

    public void setTxt_msgDesc(String txt_msgDesc) {
        this.txt_msgDesc = txt_msgDesc;
    }

    public String getTxt_SrNo() {
        return txt_SrNo;
    }

    public void setTxt_SrNo(String txt_SrNo) {
        this.txt_SrNo = txt_SrNo;
    }

    public String getTxt_Que() {
        return txt_Que;
    }

    public void setTxt_Que(String txt_Que) {
        this.txt_Que = txt_Que;
    }

    public String getTxt_IsAllQuesMendatory() {
        return txt_IsAllQuesMendatory;
    }

    public void setTxt_IsAllQuesMendatory(String txt_IsAllQuesMendatory) {
        this.txt_IsAllQuesMendatory = txt_IsAllQuesMendatory;
    }
}
