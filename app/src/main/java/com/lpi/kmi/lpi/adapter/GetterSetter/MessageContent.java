package com.lpi.kmi.lpi.adapter.GetterSetter;

public class MessageContent {
    String QuesId,
            QuesDesc,
            TrainerId,
            CandidateId,
            RespDesc,
            RespDtime,
            RespFrom,
            isClosed,
            MobRefId,
            RespStatus,
            isShow,
            isAttachment,
            mFile,
            mFileName,
            mFilePath,
            mFileType;
    boolean showProgress;

    public String getQuesId() {
        return QuesId;
    }

    public void setQuesId(String quesId) {
        QuesId = quesId;
    }

    public String getQuesDesc() {
        return QuesDesc;
    }

    public void setQuesDesc(String quesDesc) {
        QuesDesc = quesDesc;
    }

    public String getTrainerId() {
        return TrainerId;
    }

    public void setTrainerId(String trainerId) {
        TrainerId = trainerId;
    }

    public String getCandidateId() {
        return CandidateId;
    }

    public void setCandidateId(String candidateId) {
        CandidateId = candidateId;
    }

    public String getRespDesc() {
        return RespDesc;
    }

    public void setRespDesc(String respDesc) {
        RespDesc = respDesc;
    }

    public String getRespDtime() {
        return RespDtime;
    }

    public void setRespDtime(String respDtime) {
        RespDtime = respDtime;
    }

    public String getRespFrom() {
        return RespFrom;
    }

    public void setRespFrom(String respFrom) {
        RespFrom = respFrom;
    }

    public String getIsClosed() {
        return isClosed;
    }

    public void setIsClosed(String isClosed) {
        this.isClosed = isClosed;
    }

    public String getMobRefId() {
        return MobRefId;
    }

    public void setMobRefId(String mobRefId) {
        MobRefId = mobRefId;
    }

    public String getRespStatus() {
        return RespStatus;
    }

    public void setRespStatus(String respStatus) {
        RespStatus = respStatus;
    }

    public String getIsShow() {
        return isShow;
    }

    public void setIsShow(String isShow) {
        this.isShow = isShow;
    }

    public String getIsAttachment() {
        return isAttachment;
    }

    public void setIsAttachment(String isAttachment) {
        this.isAttachment = isAttachment;
    }

    public String getmFile() {
        return mFile;
    }

    public void setmFile(String mFile) {
        this.mFile = mFile;
    }

    public String getmFileName() {
        return mFileName;
    }

    public void setmFileName(String mFileName) {
        this.mFileName = mFileName;
    }

    public String getmFilePath() {
        return mFilePath;
    }

    public void setmFilePath(String mFilePath) {
        this.mFilePath = mFilePath;
    }

    public String getmFileType() {
        return mFileType;
    }

    public void setmFileType(String mFileType) {
        this.mFileType = mFileType;
    }

    public boolean isShowProgress() {
        return showProgress;
    }

    public void setShowProgress(boolean showProgress) {
        this.showProgress = showProgress;
    }

}
