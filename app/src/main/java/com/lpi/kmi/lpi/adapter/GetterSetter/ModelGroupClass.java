package com.lpi.kmi.lpi.adapter.GetterSetter;

public class ModelGroupClass {

    String strExam_id;
    String strExam_name;
    String strExam_desc;
    String strQuestion_id;
    String strQuestion;
    String strFeedback;

    public String getStrExam_id() {
        return strExam_id;
    }

    public void setStrExam_id(String strExam_id) {
        this.strExam_id = strExam_id;
    }

    public String getStrExam_name() {
        return strExam_name;
    }

    public void setStrExam_name(String strExam_name) {
        this.strExam_name = strExam_name;
    }

    public String getStrExam_desc() {
        return strExam_desc;
    }

    public void setStrExam_desc(String strExam_desc) {
        this.strExam_desc = strExam_desc;
    }

    public String getStrQuestion_id() {
        return strQuestion_id;
    }

    public void setStrQuestion_id(String strQuestion_id) {
        this.strQuestion_id = strQuestion_id;
    }

    public String getStrQuestion() {
        return strQuestion;
    }

    public void setStrQuestion(String strQuestion) {
        this.strQuestion = strQuestion;
    }


    public String getStrFeedback() {
        return strFeedback;
    }

    public void setStrFeedback(String strFeedback) {
        this.strFeedback = strFeedback;
    }
}
