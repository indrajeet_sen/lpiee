package com.lpi.kmi.lpi.adapter.GetterSetter;

public class QuizPojo {
    public String tv_quiz_id,tv_quiz_name,tv_quiz_desc,tv_quiz_completed,tv_quiz_nextid,tv_quiz_result,
            tv_quiz_tquiz,tv_quiz_attempted,tv_quiz_correct,tv_quiz_accuracy;

    public String getTv_quiz_id() {
        return tv_quiz_id;
    }

    public void setTv_quiz_id(String tv_quiz_id) {
        this.tv_quiz_id = tv_quiz_id;
    }

    public String getTv_quiz_name() {
        return tv_quiz_name;
    }

    public void setTv_quiz_name(String tv_quiz_name) {
        this.tv_quiz_name = tv_quiz_name;
    }

    public String getTv_quiz_desc() {
        return tv_quiz_desc;
    }

    public void setTv_quiz_desc(String tv_quiz_desc) {
        this.tv_quiz_desc = tv_quiz_desc;
    }

    public String getTv_quiz_completed() {
        return tv_quiz_completed;
    }

    public void setTv_quiz_completed(String tv_quiz_completed) {
        this.tv_quiz_completed = tv_quiz_completed;
    }

    public String getTv_quiz_nextid() {
        return tv_quiz_nextid;
    }

    public void setTv_quiz_nextid(String tv_quiz_nextid) {
        this.tv_quiz_nextid = tv_quiz_nextid;
    }

    public String getTv_quiz_result() {
        return tv_quiz_result;
    }

    public void setTv_quiz_result(String tv_quiz_result) {
        this.tv_quiz_result = tv_quiz_result;
    }

    public String getTv_quiz_tquiz() {
        return tv_quiz_tquiz;
    }

    public void setTv_quiz_tquiz(String tv_quiz_tquiz) {
        this.tv_quiz_tquiz = tv_quiz_tquiz;
    }

    public String getTv_quiz_attempted() {
        return tv_quiz_attempted;
    }

    public void setTv_quiz_attempted(String tv_quiz_attempted) {
        this.tv_quiz_attempted = tv_quiz_attempted;
    }

    public String getTv_quiz_correct() {
        return tv_quiz_correct;
    }

    public void setTv_quiz_correct(String tv_quiz_correct) {
        this.tv_quiz_correct = tv_quiz_correct;
    }

    public String getTv_quiz_accuracy() {
        return tv_quiz_accuracy;
    }

    public void setTv_quiz_accuracy(String tv_quiz_accuracy) {
        this.tv_quiz_accuracy = tv_quiz_accuracy;
    }
}
