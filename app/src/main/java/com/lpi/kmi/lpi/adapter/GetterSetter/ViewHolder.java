package com.lpi.kmi.lpi.adapter.GetterSetter;

import android.widget.TextView;

/**
 * Created by mustafakachwalla on 09/02/17.
 */

public class ViewHolder {

    //ViewHolder For Question
    public TextView txt_SrNo;
    public TextView txt_Que;

    //ViewHolder For Home screen Messages
    public TextView txt_msgID;
    public TextView txt_msgHeader;
    public TextView txt_msgDesc;

    //ViewHolder For Exam Type list
    private TextView txt_ExamSrNo;
    private TextView txt_ExamId;
    private TextView txt_ExamCode;
    private TextView txt_ExamDetails;
    private TextView txt_ExamType;
    private TextView txt_ExamTypeName;
    private TextView txt_AllowNavigationFlag;
    private TextView txt_OptionCount;
    private TextView txt_ShowRandomQuestions;
    private TextView txt_TotalQuestions;
    private TextView txt_TotalTimeAllowedInMinutes;
    private TextView txt_IsAllowResultFeedback;
    private TextView txt_IsPracticeExam;
    private TextView txt_AllowedAttempt;
    private TextView txt_EffectiveDate;

    //ViewHolder For Attempted Exam list
    static private TextView txt_AttemptedExamID;
    static private TextView txt_AttemptedCandidateID;
    static private TextView txt_AttemptedExamDate;
    static private TextView txt_AttemptedExamResult;
    static private TextView txt_AttemptedExamName;
    static private TextView txt_AttemptedExamDetail;
    static private TextView txt_AttemptedExamType;
    static private TextView txt_AttemptedId;
    private TextView txt_AttemptedExamIsAllowFdback;
    private TextView txt_IsAllQuesMendatory;


    //ViewHolder for different traits in View Result
    private TextView txt_TraitResult;

    public ViewHolder() {
    }

    public ViewHolder(TextView txt_TraitResult) {
        this.txt_TraitResult = txt_TraitResult;
    }

    public ViewHolder(TextView txt_msgID, TextView txt_msgHeader, TextView txt_msgDesc) {
        this.txt_msgID = txt_msgID;
        this.txt_msgHeader = txt_msgHeader;
        this.txt_msgDesc = txt_msgDesc;
    }

    public ViewHolder(TextView txt_SrNo, TextView txt_Que) {
        this.txt_SrNo = txt_SrNo;
        this.txt_Que = txt_Que;
    }

    // For Exam List
    public ViewHolder(TextView txt_ExamSrNo, TextView txt_ExamId, TextView txt_ExamCode,
                      TextView txt_ExamDetails, TextView txt_ExamType, TextView txt_ExamTypeName,
                      TextView txt_AllowNavigationFlag, TextView txt_OptionCount, TextView txt_ShowRandomQuestions,
                      TextView txt_TotalQuestions, TextView txt_TotalTimeAllowedInMinutes,
                      TextView txt_IsAllowResultFeedback, TextView txt_IsPracticeExam,
                      TextView txt_AllowedAttempt, TextView txt_EffectiveDate, TextView txt_IsAllQuesMendatory) {
        this.txt_ExamSrNo = txt_ExamSrNo;
        this.txt_ExamId = txt_ExamId;
        this.txt_ExamCode = txt_ExamCode;
        this.txt_ExamDetails = txt_ExamDetails;
        this.txt_ExamType = txt_ExamType;
        this.txt_ExamTypeName = txt_ExamTypeName;
        this.txt_AllowNavigationFlag = txt_AllowNavigationFlag;
        this.txt_OptionCount = txt_OptionCount;
        this.txt_ShowRandomQuestions = txt_ShowRandomQuestions;
        this.txt_TotalQuestions = txt_TotalQuestions;
        this.txt_TotalTimeAllowedInMinutes = txt_TotalTimeAllowedInMinutes;
        this.txt_IsAllowResultFeedback = txt_IsAllowResultFeedback;
        this.txt_IsPracticeExam = txt_IsPracticeExam;
        this.txt_AllowedAttempt = txt_AllowedAttempt;
        this.txt_EffectiveDate = txt_EffectiveDate;
        this.txt_IsAllQuesMendatory = txt_IsAllQuesMendatory;
    }

    // For Attempted ExamList
    public ViewHolder(TextView txt_SrNo, TextView txt_AttemptedExamID, TextView txt_AttemptedCandidateID,
                      TextView txt_AttemptedExamDate, TextView txt_AttemptedExamResult, TextView txt_AttemptedExamName,
                      TextView txt_AttemptedExamDetail, TextView txt_AttemptedExamType,
                      TextView txt_AttemptedExamIsAllowFdback, TextView txt_AttemptedId, TextView txt_IsAllQuesMendatory) {
        this.txt_AttemptedExamID = txt_AttemptedExamID;
        this.txt_AttemptedCandidateID = txt_AttemptedCandidateID;
        this.txt_AttemptedExamDate = txt_AttemptedExamDate;
        this.txt_AttemptedExamResult = txt_AttemptedExamResult;
        this.txt_AttemptedExamName = txt_AttemptedExamName;
        this.txt_AttemptedExamDetail = txt_AttemptedExamDetail;
        this.txt_AttemptedExamType = txt_AttemptedExamType;
        this.txt_AttemptedId = txt_AttemptedId;
        this.txt_AttemptedExamIsAllowFdback = txt_AttemptedExamIsAllowFdback;
        this.txt_IsAllQuesMendatory = txt_IsAllQuesMendatory;
        this.txt_SrNo = txt_SrNo;
    }


    public TextView getTxt_TraitResult() {
        return txt_TraitResult;
    }

    public void setTxt_TraitResult(TextView txt_TraitResult) {
        this.txt_TraitResult = txt_TraitResult;
    }

    public TextView getTxt_AttemptedExamIsAllowFdback() {
        return txt_AttemptedExamIsAllowFdback;
    }

    public void setTxt_AttemptedExamIsAllowFdback(TextView txt_AttemptedExamIsAllowFdback) {
        this.txt_AttemptedExamIsAllowFdback = txt_AttemptedExamIsAllowFdback;
    }

    public TextView getTxt_AttemptedExamID() {
        return txt_AttemptedExamID;
    }

    public void setTxt_AttemptedExamID(TextView txt_AttemptedExamID) {
        this.txt_AttemptedExamID = txt_AttemptedExamID;
    }

    public TextView getTxt_AttemptedCandidateID() {
        return txt_AttemptedCandidateID;
    }

    public void setTxt_AttemptedCandidateID(TextView txt_AttemptedCandidateID) {
        this.txt_AttemptedCandidateID = txt_AttemptedCandidateID;
    }

    public TextView getTxt_AttemptedExamDate() {
        return txt_AttemptedExamDate;
    }

    public void setTxt_AttemptedExamDate(TextView txt_AttemptedExamDate) {
        this.txt_AttemptedExamDate = txt_AttemptedExamDate;
    }

    public TextView getTxt_AttemptedExamResult() {
        return txt_AttemptedExamResult;
    }

    public void setTxt_AttemptedExamResult(TextView txt_AttemptedExamResult) {
        this.txt_AttemptedExamResult = txt_AttemptedExamResult;
    }

    public TextView getTxt_AttemptedExamName() {
        return txt_AttemptedExamName;
    }

    public void setTxt_AttemptedExamName(TextView txt_AttemptedExamName) {
        this.txt_AttemptedExamName = txt_AttemptedExamName;
    }

    public TextView getTxt_AttemptedExamDetail() {
        return txt_AttemptedExamDetail;
    }

    public void setTxt_AttemptedExamDetail(TextView txt_AttemptedExamDetail) {
        this.txt_AttemptedExamDetail = txt_AttemptedExamDetail;
    }

    public TextView getTxt_AttemptedExamType() {
        return txt_AttemptedExamType;
    }

    public void setTxt_AttemptedExamType(TextView txt_AttemptedExamType) {
        this.txt_AttemptedExamType = txt_AttemptedExamType;
    }

    public TextView getTxt_AttemptedId() {
        return txt_AttemptedId;
    }

    public void setTxt_AttemptedId(TextView txt_AttemptedId) {
        this.txt_AttemptedId = txt_AttemptedId;
    }

    public TextView getTxt_ShowRandomQuestions() {
        return txt_ShowRandomQuestions;
    }

    public void setTxt_ShowRandomQuestions(TextView txt_ShowRandomQuestions) {
        this.txt_ShowRandomQuestions = txt_ShowRandomQuestions;
    }

    public TextView getTxt_AllowNavigationFlag() {
        return txt_AllowNavigationFlag;
    }

    public void setTxt_AllowNavigationFlag(TextView txt_AllowNavigationFlag) {
        this.txt_AllowNavigationFlag = txt_AllowNavigationFlag;
    }

    public TextView getTxt_OptionCount() {
        return txt_OptionCount;
    }

    public void setTxt_OptionCount(TextView txt_OptionCount) {
        this.txt_OptionCount = txt_OptionCount;
    }

    public TextView getTxt_TotalQuestions() {
        return txt_TotalQuestions;
    }

    public void setTxt_TotalQuestions(TextView txt_TotalQuestions) {
        this.txt_TotalQuestions = txt_TotalQuestions;
    }

    public TextView getTxt_TotalTimeAllowedInMinutes() {
        return txt_TotalTimeAllowedInMinutes;
    }

    public void setTxt_TotalTimeAllowedInMinutes(TextView txt_TotalTimeAllowedInMinutes) {
        this.txt_TotalTimeAllowedInMinutes = txt_TotalTimeAllowedInMinutes;
    }

    public TextView getTxt_IsAllowResultFeedback() {
        return txt_IsAllowResultFeedback;
    }

    public void setTxt_IsAllowResultFeedback(TextView txt_IsAllowResultFeedback) {
        this.txt_IsAllowResultFeedback = txt_IsAllowResultFeedback;
    }

    public TextView getTxt_IsPracticeExam() {
        return txt_IsPracticeExam;
    }

    public void setTxt_IsPracticeExam(TextView txt_IsPracticeExam) {
        this.txt_IsPracticeExam = txt_IsPracticeExam;
    }

    public TextView getTxt_AllowedAttempt() {
        return txt_AllowedAttempt;
    }

    public void setTxt_AllowedAttempt(TextView txt_AllowedAttempt) {
        this.txt_AllowedAttempt = txt_AllowedAttempt;
    }

    public TextView getTxt_EffectiveDate() {
        return txt_EffectiveDate;
    }

    public void setTxt_EffectiveDate(TextView txt_EffectiveDate) {
        this.txt_EffectiveDate = txt_EffectiveDate;
    }

    public TextView getTxt_ExamSrNo() {
        return txt_ExamSrNo;
    }

    public void setTxt_ExamSrNo(TextView txt_ExamSrNo) {
        this.txt_ExamSrNo = txt_ExamSrNo;
    }

    public TextView getTxt_ExamTypeName() {
        return txt_ExamTypeName;
    }

    public void setTxt_ExamTypeName(TextView txt_ExamTypeName) {
        this.txt_ExamTypeName = txt_ExamTypeName;
    }

    public TextView getTxt_ExamId() {
        return txt_ExamId;
    }

    public void setTxt_ExamId(TextView txt_ExamId) {
        this.txt_ExamId = txt_ExamId;
    }

    public TextView getTxt_ExamCode() {
        return txt_ExamCode;
    }

    public void setTxt_ExamCode(TextView txt_ExamCode) {
        this.txt_ExamCode = txt_ExamCode;
    }

    public TextView getTxt_ExamDetails() {
        return txt_ExamDetails;
    }

    public void setTxt_ExamDetails(TextView txt_ExamDetails) {
        this.txt_ExamDetails = txt_ExamDetails;
    }

    public TextView getTxt_ExamType() {
        return txt_ExamType;
    }

    public void setTxt_ExamType(TextView txt_ExamType) {
        this.txt_ExamType = txt_ExamType;
    }

    public TextView getTxt_msgID() {
        return txt_msgID;
    }

    public void setTxt_msgID(TextView txt_msgID) {
        this.txt_msgID = txt_msgID;
    }

    public TextView getTxt_msgHeader() {
        return txt_msgHeader;
    }

    public void setTxt_msgHeader(TextView txt_msgHeader) {
        this.txt_msgHeader = txt_msgHeader;
    }

    public TextView getTxt_msgDesc() {
        return txt_msgDesc;
    }

    public void setTxt_msgDesc(TextView txt_msgDesc) {
        this.txt_msgDesc = txt_msgDesc;
    }

    public TextView getTxt_SrNo() {
        return txt_SrNo;
    }

    public void setTxt_SrNo(TextView txt_SrNo) {
        this.txt_SrNo = txt_SrNo;
    }

    public TextView getTxt_Que() {
        return txt_Que;
    }

    public void setTxt_Que(TextView txt_Que) {
        this.txt_Que = txt_Que;
    }

    public TextView getTxt_IsAllQuesMendatory() {
        return txt_IsAllQuesMendatory;
    }

    public void setTxt_IsAllQuesMendatory(TextView txt_IsAllQuesMendatory) {
        this.txt_IsAllQuesMendatory = txt_IsAllQuesMendatory;
    }
}
