package com.lpi.kmi.lpi.adapter.GetterSetter;

/**
 * Created by mustafakachwalla on 18/11/17.
 */

public class ZipContent {
    String BatchId,FileName,Path,Synced;

    public String getBatchId() {
        return BatchId;
    }

    public void setBatchId(String batchId) {
        BatchId = batchId;
    }

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String fileName) {
        FileName = fileName;
    }

    public String getPath() {
        return Path;
    }

    public void setPath(String path) {
        Path = path;
    }

    public String getSynced() {
        return Synced;
    }

    public void setSynced(String synced) {
        Synced = synced;
    }
}
