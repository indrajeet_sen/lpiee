package com.lpi.kmi.lpi.classes;

import android.graphics.Bitmap;

/**
 * Created by bharatg on 27-07-2017.
 */

public interface Callback<T> {
    void execute(T result,String status);

}
