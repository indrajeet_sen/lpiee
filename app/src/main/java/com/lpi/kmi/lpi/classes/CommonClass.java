package com.lpi.kmi.lpi.classes;

import static android.content.Context.TELEPHONY_SERVICE;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.RemoteViews;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Services.StopServiceReceiver;
import com.lpi.kmi.lpi.ToolTip.SimpleTooltip;
import com.lpi.kmi.lpi.activities.LPI_FAQ_ConversationActivity;
import com.lpi.kmi.lpi.activities.ParticipantHomeActivity;
import com.stringcare.library.SC;

import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import nu.xom.Serializer;

/**
 * Created by mustafakachwalla on 07/06/17.
 */

public class CommonClass {
    public static final int sNOTIFICATION_ID = 1809;

    public static String DBName= SC.Companion.obfuscate("LPIEEC.db");
    public static String DBPassword= SC.Companion.obfuscate("LPI@321");
    public static String DummyAppId= SC.Companion.obfuscate("LPIEEC.db");
//    public static String Exam_NAMESPACE= SC.Companion.obfuscate("http://krishmark.centralindia.cloudapp.azure.com/LPIEEWS/");
//    public static String Exam_SOAP_ACTION= SC.Companion.obfuscate("http://krishmark.centralindia.cloudapp.azure.com/LPIEEWS/");
//    public static String Exam_URL= SC.Companion.obfuscate("http://krishmark.centralindia.cloudapp.azure.com/LPIEEWS/CandidateExams.asmx");
//    public static String ContentURL= SC.Companion.obfuscate("http://krishmark.centralindia.cloudapp.azure.com/LPIEEWS/HtmlContent/");
    public static String cipher= SC.Companion.obfuscate("LPIEEC.db");
    public static String seed= SC.Companion.obfuscate("LPIEEC.db");


    public static void showTooltip(Context context, final View view, String msg) {

        try {
            new SimpleTooltip.Builder(context)
                    .anchorView(view)
                    .text(msg)
                    .gravity(Gravity.START)
                    .animated(true)
                    .build()
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void custom_ResultRemrk(Context context, String title, String remark) {
        try {
            final AlertDialog alertD;
            LayoutInflater logout_inflator = LayoutInflater.from(context);
            final View openDialog = logout_inflator.inflate(R.layout.alert_ansalertdialog, null);
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            alertDialogBuilder.setView(openDialog);
            alertD = alertDialogBuilder.create();
            alertD.setCancelable(false);
            alertD.show();

            final TextView alrtText_ResultRemark = (TextView) openDialog.findViewById(R.id.alrtText_ResultRemart);
            final TextView alrtText_CrrectResult = (TextView) openDialog.findViewById(R.id.alrtText_CrrectResult);
            final TextView Txt_AlertTitle = (TextView) openDialog.findViewById(R.id.Txt_AlertTitle);

            final Button alrtBtn_done = (Button) openDialog.findViewById(R.id.alrtbtn_Done);
            final Button alrtBtn_cncl = (Button) openDialog.findViewById(R.id.alrtbtn_cancle);
            alrtText_CrrectResult.setVisibility(View.GONE);

            Txt_AlertTitle.setText(title);
            alrtText_ResultRemark.setText(remark);
            alrtBtn_done.setText("OK");
//            alrtBtn_cncl.setText("Ask me later");

            alrtBtn_done.setVisibility(View.VISIBLE);
            alrtBtn_cncl.setVisibility(View.GONE);

            alrtBtn_done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertD.dismiss();
                }
            });

//            alrtBtn_cncl.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    alertD.dismiss();
//                    gotoMenu();
//                }
//            });
        } catch (Exception e) {
            Log.e("", "err=" + e.toString());
        }
    }

    public static AlertDialog.Builder DialogOpen(Context context, String Title, String Msg) {

        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, R.style.AppTheme_Dark_Dialog);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(context.getResources().getColor(R.color.primary));
        // Initialize a new spannable string builder instance
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(Title);
        // Apply the text color span
        ssBuilder.setSpan(
                foregroundColorSpan,
                0,
                Title.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        );
        // Set the alert dialog title using spannable string builder
        builder.setTitle(ssBuilder);
//        builder.setTitle(Title);
        builder.setMessage(Msg);
        builder.setCancelable(true);
        return builder;
    }

    public static boolean isValidEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // TODO : Get the size of Local DB in MB
    public static long GetInternalDBSizeInMB(Context cntxt) {
        String path = cntxt.getDatabasePath(SC.reveal(CommonClass.DBName)).toString();

        File file = new File(path);
        long length = file.length();
        long fileSizeInKB = length / 1024;
        long fileSizeInMB = fileSizeInKB / 1024;
        return fileSizeInMB;
    }


    public static void Demo_getDatafrmXML(Context cntxt) {
        try {
            InputStream is = cntxt.getAssets().open("file.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(is);

            Element element = doc.getDocumentElement();
            element.normalize();

            String strr = CommonClass.getXMLElementValue("ResponseCode", element);


            NodeList nList = doc.getElementsByTagName("TraitQues");

            for (int i = 0; i < nList.getLength(); i++) {

                NodeList nList1 = doc.getElementsByTagName("TraitQuestions");
                for (int j = 0; j < nList1.getLength(); j++) {
                    Node node = nList1.item(j);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element2 = (Element) node;
                        String str = CommonClass.getXMLElementValue("QuesDesc", element2);
                        str = CommonClass.getXMLElementValue("TraitType", element2);
                        Log.e("", "str=" + str);
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("", "error=" + e.toString());
        }
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    public static String networkType(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting() && (netInfo.getType() == ConnectivityManager.TYPE_WIFI)) {
            return "WiFi";
        } else {
            return "MobileData";
        }
    }

    // TODO: To Generate the XML String
    public static String GenerateXML(Context context, int NoOfParam, String RootElement, String[] XMLElementIDs,
                                     String[] XMLElementValues) {
        StringBuilder str_XML = new StringBuilder();
        try {

            OutputStream output = new ByteArrayOutputStream();
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            Element rootElement = document.createElement(RootElement);
            document.appendChild(rootElement);
            XML_Element(document, rootElement, "AppUserId", "LPIUser");
            XML_Element(document, rootElement, "AppPassword", "pass@123");
            XML_Element(document, rootElement, "AppID", "1");
            XML_Element(document, rootElement, "CallingMode", "Mobile");
//            XML_Element(document,rootElement,"ServiceVersionID","1");
//            XML_Element(document, rootElement, "UniqRefID", "121");
//            XML_Element(document, rootElement, "IMEINo", "49-015420-323751");

            if (!RootElement.trim().equalsIgnoreCase("setErrorLogMob")) {
                XML_Element(document, rootElement, "IMEINo", getIMEINumber(context));
                XML_Element(document, rootElement, "UniqRefID", "121");
            }
//            XML_Element(document,rootElement,"InscType",StaticVariables.crnt_InscType);"UserID","BatchId","Synced"

            if (RootElement.equals("GetSectionData")) {
                XML_Element(document, rootElement, "SectionId", "");
                XML_Element(document, rootElement, "Mode", "");
            } else {
                XML_Element(document, rootElement, "ServiceVersionID", "1");
                XML_Element(document, rootElement, "InscType", StaticVariables.crnt_InscType);
            }

            if (NoOfParam > 0) {
                for (int i = 0; i < XMLElementIDs.length; i++) {
//                    if (XMLElementValues[i].equals(null)) {
//                        XML_Element(document, rootElement, XMLElementIDs[i], "");
//                    }
//                    else {
                    XML_Element(document, rootElement, XMLElementIDs[i], XMLElementValues[i]);
//                    }
                }
            }

//            XML_Element(document,rootElement,"LoginID","100");

            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            Properties outFormat = new Properties();
            outFormat.setProperty(OutputKeys.INDENT, "yes");
            outFormat.setProperty(OutputKeys.METHOD, "xml");
            outFormat.setProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            outFormat.setProperty(OutputKeys.VERSION, "1.0");
            outFormat.setProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperties(outFormat);
            DOMSource domSource = new DOMSource(document.getDocumentElement());
            output = new ByteArrayOutputStream();
            StreamResult result = new StreamResult(output);
            transformer.transform(domSource, result);
            str_XML.append(output.toString());
//            Log.e("GenerateXML", "GenerateXML XML=" + str_XML);
            return "" + str_XML;
        } catch (Exception e) {
            Log.e("GenerateXML", "Error=" + e.toString());
//            DatabaseHelper.SaveErroLog(getApplicationContext(), DatabaseHelper.db,
//                    "SyncErrorLog", "syncData()", e.toString());
            return "";
        }
    }


    public static Element XML_Element(Document document, Element rootElement,
                                      String ElementID, String ElementValue) {
        Element Sub_Element = document.createElement("" + ElementID);
        rootElement.appendChild(Sub_Element);
        Sub_Element.appendChild(document.createTextNode("" + ElementValue));
        return Sub_Element;
    }

    // TODO: Get the List of Node from XML
    public static NodeList GetXMLElementNodeList(InputStream strXML, String NodeListItem) {
        NodeList nList = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(strXML);
            Element element = doc.getDocumentElement();
            element.normalize();
            nList = doc.getElementsByTagName(NodeListItem);
        } catch (Exception e) {
            Log.e("", "error=" + e.toString());
            e.printStackTrace();
        }

        return nList;
    }

    // TODO: Get the Value of Element
    public static String getValue(Context context, String tag, Element element) {
        try {
            if (element.hasChildNodes()) {
                NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
                Node node = nodeList.item(0);
                return node.getNodeValue();
            } else {
                return "";
            }
        } catch (Exception e) {
            Log.e("", "error=" + e.toString());
//            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
//                    "SyncExamMaster", "getValue()", e.toString());
            return "";
        }
    }

    // TODO: Get the Value of Element
    public static String getXMLElementValue(String tag, Element element) {
        try {
            NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
            if (nodeList.getLength() == 0) {
                return "";
            } else {
                Node node = nodeList.item(0);
                if (node == null) {
                    return "";
                } else {
                    return node.getNodeValue();
                }
            }
        } catch (Exception e) {
            Log.e("getXMLElementValue", "" + e.toString());
            return "";
        }
    }

    public static void read_feedBackQueXML(Context cntxt, InputStream is, String ParentNode_name, String ChildNodeName) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(is);

            Element element = doc.getDocumentElement();
            element.normalize();

            NodeList nList_Que = doc.getElementsByTagName(ParentNode_name);
            for (int i = 0; i < nList_Que.getLength(); i++) {
                NodeList nList1 = doc.getElementsByTagName(ChildNodeName);
                for (int j = 0; j < nList1.getLength(); j++) {
                    Node node = nList1.item(j);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element2 = (Element) node;
                        String QuesDesc = CommonClass.getXMLElementValue("QuesDesc", element2);
                        String QueId = "" + (500 + (j + 1));
                        if (CommonClass.getXMLElementValue("TraitType", element2).equals("S"))
                            QuesDesc = "Strength: " + QuesDesc;
                        else
                            QuesDesc = "Weakness: " + QuesDesc;
                        DatabaseHelper.SaveQueAnsForFeedback(cntxt, DatabaseHelper.db, "5001", QueId, QuesDesc);
                    }
                }
            }
        } catch (Exception e) {
            Log.e("read_feedBackQueXML", "error=" + e.toString());
            DatabaseHelper.SaveErroLog(cntxt, DatabaseHelper.db,
                    "CommonClass", "read_feedBackQueXML()", e.toString());
        }
    }

    // TODO: Check the Response Code
    public static boolean IsCorrectXMLResponse(InputStream strXML) {
        boolean isZeroResponseCode = false;
        try {
            if (strXML.equals(null) || strXML == null) {
                isZeroResponseCode = false;
            } else {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(strXML);
                Element element = doc.getDocumentElement();
                element.normalize();
                String ResponseCode = getXMLElementValue("ResponseCode", element);
                if (ResponseCode.equals("0")) {
                    isZeroResponseCode = true;
                } else {
                    isZeroResponseCode = false;
                }
            }
        } catch (Exception e) {
            Log.e("IsCorrectXMLResponse", "error=" + e.toString());
            isZeroResponseCode = false;
        }
        return isZeroResponseCode;
    }

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }


    public static String saveHtmlInStorage(Context context, String fName, String html) {
        Log.e("", "In saveForm");
        File mDir = null;
        if (isExternalStorageWritable()) {
            String root = null;
            /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)
                        .getAbsolutePath() + "/.www/HTML/";
            } else {
//                root = Environment.getExternalStorageDirectory() + "/Documents" + "/.www/HTML/";
                root = context.getExternalFilesDir(null).toString()+ "/.www/HTML/";
            }*/
            root = context.getExternalFilesDir(Environment.MEDIA_SHARED).toString()+ "/.www/HTML/";
//            String root = context.getFilesDir()+"/www";
            mDir = new File(root);
            if (!mDir.exists()) {
                mDir.mkdirs();
                mDir.setReadable(true);
                mDir.setWritable(true);
            }
            try {
                File mFile = new File(mDir, fName);
                if (mFile.exists()) {
                    mFile.delete();
                }
                FileOutputStream out = new FileOutputStream(mFile);
                byte[] data = html.getBytes();
                out.write(data);
                out.close();
                Log.e("", "File Save : " + mFile.getPath());
            } catch (Exception e) {
                Log.e("createImageFile", "Error while creating " + fName + e.toString());
                return "Failed";
            }
        } else {
            return "Failed";
        }
        return "Success";
    }

    public static File CheckHtml(Context context, String root, String fName) {

//        String root = context.getFilesDir() + "/www/";
        File mFile, mDir;
//        mDir = new File(context.getFilesDir().getAbsolutePath());
        mDir = new File(root);
        if (mDir.exists()) {
            mFile = new File(mDir, fName);
            if (mFile.exists()) {
                return mFile;
            } else {
//                Toast.makeText(context, "Page not found", Toast.LENGTH_SHORT).show();
                mFile = getErrorFile(context);
                return mFile;
            }
        } else {
//            Toast.makeText(context, "Directory not found", Toast.LENGTH_SHORT).show();
            mFile = getErrorFile(context);
            return mFile;
        }
    }

    public static File getErrorFile(Context context) {
        File cacheFile = new File(context.getCacheDir(), "404.html");
        try {
            InputStream inputStream = context.getAssets().open("404.html");
            try {
                FileOutputStream outputStream = new FileOutputStream(cacheFile);
                try {
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = inputStream.read(buf)) > 0) {
                        outputStream.write(buf, 0, len);
                    }
                } finally {
                    outputStream.close();
                }
            } finally {
                inputStream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cacheFile;
    }

    public static InputStream checkFile(Context context, String fName) {
        try {
            AssetManager assetManager = context.getAssets();
            InputStream stream = assetManager.open(fName);
            return stream;
        } catch (Exception xxx) {
            Log.e("", "Load assets/page.html", xxx);
            return new InputStream() {
                @Override
                public int read() throws IOException {
                    return 0;
                }
            };
        }

    }

    public static boolean isMyServiceRunning(Context context, String classname) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (classname.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isTaskRunning(Context ctx) {
        ActivityManager activityManager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (ActivityManager.RunningTaskInfo task : tasks) {
            if (ctx.getPackageName().equalsIgnoreCase(task.baseActivity.getPackageName()))
                return true;
        }
        return false;
    }

    // IMEI Number
    public static String getIMEINumber(Context c) {
        String _number = "Permission Not Granted";
        try {
            TelephonyManager tm = (TelephonyManager) c.getSystemService(TELEPHONY_SERVICE);
            _number = tm.getDeviceId();
//        if (ActivityCompat.checkSelfPermission(c, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//            _number = tm.getDeviceId();
//        }else {
//
//        }
        } catch (SecurityException e) {
            _number = "Permission Not Granted";
        }
        return _number;
    }

    /**
     * Returns the consumer friendly device name
     */
    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }

    public static String SubmitExamXML(Context context, String str_LoginId, String str_ExamId) {

        String str_XML = "";
        nu.xom.Element root = new nu.xom.Element("SubmitExamRequest");

        nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
        AppUserId.appendChild("LPIUser");

        nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
        AppPassword.appendChild("pass@123");

        nu.xom.Element AppID = new nu.xom.Element("AppID");
        AppID.appendChild("1");

        nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
        CallingMode.appendChild("Mobile");

        nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
        ServiceVersionID.appendChild("1");

        nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
        UniqRefID.appendChild("121");

        nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
        IMEINo.appendChild(CommonClass.getIMEINumber(context));

        nu.xom.Element InscType = new nu.xom.Element("InscType");
        InscType.appendChild("" + StaticVariables.crnt_InscType);

        root.appendChild(AppUserId);
        root.appendChild(AppPassword);
        root.appendChild(AppID);
        root.appendChild(CallingMode);
        root.appendChild(ServiceVersionID);
        root.appendChild(UniqRefID);
        root.appendChild(IMEINo);
        root.appendChild(InscType);

        Cursor cquery;
        cquery = DatabaseHelper.db.query(context.getResources().getString(R.string.TBL_LPICandidateAttemptQueAns),
                new String[]{"LoginId", "ExamId", "ExamName", "ExamStartDtime", "ExamEndDtime", "AttemptId"}, "LoginId=? and ExamId=?",
                new String[]{str_LoginId, str_ExamId}, null, null, null, "1");
        for (cquery.moveToFirst(); !cquery.isAfterLast(); cquery.moveToNext()) {

            nu.xom.Element LoginId = new nu.xom.Element("LoginId");
            LoginId.appendChild(cquery.getString(0));
            String string = cquery.getString(0);
            root.appendChild(LoginId);

            nu.xom.Element ExamId = new nu.xom.Element("ExamId");
            ExamId.appendChild(cquery.getString(1));
            string = cquery.getString(1);
            root.appendChild(ExamId);

            nu.xom.Element ExamName = new nu.xom.Element("ExamName");
            ExamName.appendChild(cquery.getString(2));
            string = cquery.getString(2);
            root.appendChild(ExamName);

            nu.xom.Element ExamStartDtime = new nu.xom.Element("ExamStartDtime");
            ExamStartDtime.appendChild(cquery.getString(3));
            string = cquery.getString(3);
            root.appendChild(ExamStartDtime);

            nu.xom.Element ExamEndDtime = new nu.xom.Element("ExamEndDtime");
            ExamEndDtime.appendChild(cquery.getString(4));
            string = cquery.getString(4);
            root.appendChild(ExamEndDtime);

            int attemptId = DatabaseHelper.CheckAttempts(context,
                    DatabaseHelper.db, "" + StaticVariables.crnt_ExmId, StaticVariables.UserLoginId);

            nu.xom.Element AttemptId = new nu.xom.Element("AttemptId");
            AttemptId.appendChild(attemptId + "");
            root.appendChild(AttemptId);
        }
        cquery.close();


        cquery = DatabaseHelper.db.query(context.getResources().getString(R.string.TBL_LPICandidateAttemptQueAns),
                new String[]{"QuestionId", "QuestionAttempted", "AnswerId", "AnswerSequens"}, "LoginId=? and ExamId=?",
                new String[]{str_LoginId, str_ExamId}, null, null, null);
        for (cquery.moveToFirst(); !cquery.isAfterLast(); cquery.moveToNext()) {

            nu.xom.Element Questions = new nu.xom.Element("Questions");


            nu.xom.Element QuestionId = new nu.xom.Element("QuestionId");
            QuestionId.appendChild(cquery.getString(0));
            String string = cquery.getString(0);
            Questions.appendChild(QuestionId);

            nu.xom.Element QuestionAttempted = new nu.xom.Element("QuestionAttempted");
            QuestionAttempted.appendChild(cquery.getString(1));
            string = cquery.getString(1);
            Questions.appendChild(QuestionAttempted);

            nu.xom.Element AnswerId = new nu.xom.Element("AnswerId");
            AnswerId.appendChild(cquery.getString(2));
            string = cquery.getString(2);
            Questions.appendChild(AnswerId);

            nu.xom.Element AnswerSequens = new nu.xom.Element("AnswerSequens");
            AnswerSequens.appendChild(cquery.getString(3));
            string = cquery.getString(3);
            Questions.appendChild(AnswerSequens);

            root.appendChild(Questions);

        }
        nu.xom.Document doc = new nu.xom.Document(root);
        cquery.close();
        try {
            OutputStream out = new ByteArrayOutputStream();
            Serializer serializer = new Serializer(System.out, "UTF-8");
            serializer.setOutputStream(out);
            serializer.setIndent(4);
            serializer.setMaxLength(64);
            serializer.write(doc);
            str_XML = out.toString();
            str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
            str_XML = str_XML.replaceAll("\n", "");
            str_XML = str_XML.replaceAll("\r", "");
            str_XML = str_XML.replaceAll("     ", "");
            str_XML = str_XML.replaceAll("         ", "");
            str_XML = str_XML.trim();
            Log.e("", "XMLStr==" + str_XML);
        } catch (IOException e) {
            Log.e("", "SubmitExamXML Error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "MCQ_QuestionActivity", "SubmitExamXML", e.toString());
        }

        return str_XML;
    }

    public static String SubmitExamFeedXML(Context context, String str_LoginId, String str_ExamId) {

        String str_XML = "";
        nu.xom.Element root = new nu.xom.Element("ExamResultFeedbackRequest");

        nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
        AppUserId.appendChild("LPIUser");

        nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
        AppPassword.appendChild("pass@123");

        nu.xom.Element AppID = new nu.xom.Element("AppID");
        AppID.appendChild("1");

        nu.xom.Element LoginId = new nu.xom.Element("LoginId");
        LoginId.appendChild("" + StaticVariables.UserLoginId);

        nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
        CallingMode.appendChild("Mobile");

        nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
        ServiceVersionID.appendChild("1");

        nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
        UniqRefID.appendChild("121");

        nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
        IMEINo.appendChild("49-015420-323751");

        nu.xom.Element InscType = new nu.xom.Element("InscType");
        InscType.appendChild("" + StaticVariables.crnt_InscType);

        nu.xom.Element ExamId = new nu.xom.Element("ExamId");
        ExamId.appendChild("" + str_ExamId);

        root.appendChild(AppUserId);
        root.appendChild(AppPassword);
        root.appendChild(AppID);
        root.appendChild(CallingMode);
        root.appendChild(ServiceVersionID);
        root.appendChild(UniqRefID);
        root.appendChild(IMEINo);
        root.appendChild(InscType);
        root.appendChild(ExamId);
        root.appendChild(LoginId);


        Cursor cquery;

        cquery = DatabaseHelper.db.query(context.getResources().getString(R.string.TBL_LPICandidateAttemptQueAns),
                new String[]{"QuestionId", "Feedback"}, "LoginId=? and ExamId=?",
                new String[]{str_LoginId, str_ExamId}, null, null, null);
        for (cquery.moveToFirst(); !cquery.isAfterLast(); cquery.moveToNext()) {

            nu.xom.Element Questions = new nu.xom.Element("Questions");


            nu.xom.Element QuestionId = new nu.xom.Element("Question");
            QuestionId.appendChild(cquery.getString(0));
            String string = cquery.getString(0);
            Questions.appendChild(QuestionId);

            nu.xom.Element Feedback = new nu.xom.Element("Feedback");
            Feedback.appendChild(cquery.getString(1));
            string = cquery.getString(1);
            Questions.appendChild(Feedback);

            root.appendChild(Questions);

        }
        cquery.close();
        nu.xom.Document doc = new nu.xom.Document(root);

        try {
            OutputStream out = new ByteArrayOutputStream();
            Serializer serializer = new Serializer(System.out, "UTF-8");
            serializer.setOutputStream(out);
            serializer.setIndent(4);
            serializer.setMaxLength(64);
            serializer.write(doc);
            str_XML = out.toString();
            str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
            str_XML = str_XML.replaceAll("\n", "");
            str_XML = str_XML.replaceAll("\r", "");
            str_XML = str_XML.replaceAll("     ", "");
            str_XML = str_XML.replaceAll("         ", "");
            str_XML = str_XML.trim();
            Log.e("", "XMLStr==" + str_XML);
        } catch (IOException ex) {
            Log.e("", "SubmitExamFeedXML Error=" + ex.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "MCQ_QuestionActivity", "SubmitExamFeedXML", ex.toString());
        }

        return str_XML;
    }

    public static String ContentQueryXML(Context context, String uniqueId) {

        String str_XML = "";
        nu.xom.Element root = new nu.xom.Element("SubmitContentQueries");

        nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
        AppUserId.appendChild("LPIUser");

        nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
        AppPassword.appendChild("pass@123");

        nu.xom.Element AppID = new nu.xom.Element("AppID");
        AppID.appendChild("1");

        nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
        CallingMode.appendChild("Mobile");

        nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
        ServiceVersionID.appendChild("1");

        nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
        UniqRefID.appendChild("121");

        nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
        IMEINo.appendChild(CommonClass.getIMEINumber(context));

        nu.xom.Element InscType = new nu.xom.Element("InscType");
        InscType.appendChild("" + StaticVariables.crnt_InscType);

        nu.xom.Element MobRefId = new nu.xom.Element("MobRefId");
        MobRefId.appendChild(uniqueId);


        root.appendChild(AppUserId);
        root.appendChild(AppPassword);
        root.appendChild(AppID);
        root.appendChild(CallingMode);
        root.appendChild(ServiceVersionID);
        root.appendChild(UniqRefID);
        root.appendChild(IMEINo);
        root.appendChild(InscType);
        root.appendChild(MobRefId);

        Cursor cquery;
        cquery = DatabaseHelper.getContentQueries(context, DatabaseHelper.db, StaticVariables.UserLoginId);
        for (cquery.moveToFirst(); !cquery.isAfterLast(); cquery.moveToNext()) {
            String string = "";
            nu.xom.Element RecId = new nu.xom.Element("RecId");
            RecId.appendChild(cquery.getString(0));
            root.appendChild(RecId);

            nu.xom.Element UserId = new nu.xom.Element("UserId");
            UserId.appendChild(cquery.getString(1));
            root.appendChild(UserId);

            nu.xom.Element SectionId = new nu.xom.Element("SectionId");
            SectionId.appendChild(cquery.getString(2));
            root.appendChild(SectionId);

            nu.xom.Element QueryType = new nu.xom.Element("QueryType");
            QueryType.appendChild(cquery.getString(3));
            root.appendChild(QueryType);

            nu.xom.Element QueryCat = new nu.xom.Element("QueryCat");
            QueryCat.appendChild(cquery.getString(4));
            root.appendChild(QueryCat);

            nu.xom.Element QueryDesc = new nu.xom.Element("QueryDesc");
            QueryDesc.appendChild(cquery.getString(5));
            root.appendChild(QueryDesc);

            nu.xom.Element isSkiped = new nu.xom.Element("isSkiped");
            isSkiped.appendChild(cquery.getString(6));
            root.appendChild(isSkiped);

            nu.xom.Element isLiked = new nu.xom.Element("isLiked");
            isLiked.appendChild(cquery.getString(7));
            root.appendChild(isLiked);

            nu.xom.Element isDisliked = new nu.xom.Element("isDisliked");
            isDisliked.appendChild(cquery.getString(8));
            root.appendChild(isDisliked);

            nu.xom.Element QueryDtime = new nu.xom.Element("QueryDtime");
            QueryDtime.appendChild(cquery.getString(9));
            root.appendChild(QueryDtime);
        }
        nu.xom.Document doc = new nu.xom.Document(root);
        cquery.close();
        try {
            OutputStream out = new ByteArrayOutputStream();
            Serializer serializer = new Serializer(System.out, "UTF-8");
            serializer.setOutputStream(out);
            serializer.setIndent(4);
            serializer.setMaxLength(64);
            serializer.write(doc);
            str_XML = out.toString();
            str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
            str_XML = str_XML.replaceAll("\n", "");
            str_XML = str_XML.replaceAll("\r", "");
            str_XML = str_XML.replaceAll("     ", "");
            str_XML = str_XML.replaceAll("         ", "");
            str_XML = str_XML.trim();
            Log.e("", "XMLStr==" + str_XML);
        } catch (IOException ex) {
            Log.e("", "error=" + ex.toString());
        }

        return str_XML;
    }

    public static String FAQXML(Context context, String query, String uniqueId) {

        String str_XML = "";
        nu.xom.Element root = new nu.xom.Element("SubmitFAQTopic");

        nu.xom.Element AppUserId = new nu.xom.Element("AppUserId");
        AppUserId.appendChild("LPIUser");

        nu.xom.Element AppPassword = new nu.xom.Element("AppPassword");
        AppPassword.appendChild("pass@123");

        nu.xom.Element AppID = new nu.xom.Element("AppID");
        AppID.appendChild("1");

        nu.xom.Element CallingMode = new nu.xom.Element("CallingMode");
        CallingMode.appendChild("Mobile");

        nu.xom.Element ServiceVersionID = new nu.xom.Element("ServiceVersionID");
        ServiceVersionID.appendChild("1");

        nu.xom.Element UniqRefID = new nu.xom.Element("UniqRefID");
        UniqRefID.appendChild("121");

        nu.xom.Element IMEINo = new nu.xom.Element("IMEINo");
        IMEINo.appendChild(CommonClass.getIMEINumber(context));

        nu.xom.Element InscType = new nu.xom.Element("InscType");
        InscType.appendChild("" + StaticVariables.crnt_InscType);

        nu.xom.Element CandidateId = new nu.xom.Element("CandidateId");
        CandidateId.appendChild("" + StaticVariables.UserLoginId);

        nu.xom.Element QueryDesc = new nu.xom.Element("QueryDesc");
        QueryDesc.appendChild(query);

        nu.xom.Element QueryDtime = new nu.xom.Element("QueryDtime");
        QueryDtime.appendChild(StaticVariables.sdf.format(new Date()));

        nu.xom.Element MobRefId = new nu.xom.Element("MobRefId");
        MobRefId.appendChild(uniqueId);

        root.appendChild(AppUserId);
        root.appendChild(AppPassword);
        root.appendChild(AppID);
        root.appendChild(CallingMode);
        root.appendChild(ServiceVersionID);
        root.appendChild(UniqRefID);
        root.appendChild(IMEINo);
        root.appendChild(InscType);
        root.appendChild(CandidateId);
        root.appendChild(QueryDesc);
        root.appendChild(QueryDtime);
        root.appendChild(MobRefId);

        nu.xom.Document doc = new nu.xom.Document(root);

        try {
            OutputStream out = new ByteArrayOutputStream();
            Serializer serializer = new Serializer(System.out, "UTF-8");
            serializer.setOutputStream(out);
            serializer.setIndent(4);
            serializer.setMaxLength(64);
            serializer.write(doc);
            str_XML = out.toString();
            str_XML = str_XML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
            str_XML = str_XML.replaceAll("\n", "");
            str_XML = str_XML.replaceAll("\r", "");
            str_XML = str_XML.replaceAll("     ", "");
            str_XML = str_XML.replaceAll("         ", "");
            str_XML = str_XML.trim();
            Log.e("", "XMLStr==" + str_XML);
        } catch (IOException ex) {
            Log.e("", "error=" + ex.toString());
        }

        return str_XML;
    }

    public static boolean isAppRunning(final Context context, final String packageName) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        if (procInfos != null) {
            for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                if (processInfo.processName.equals(packageName)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void setLaguage(Context context, String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        (context).getApplicationContext().getResources().updateConfiguration(config,
                (context).getApplicationContext().getResources().getDisplayMetrics());
    }

    public static String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public static String getMimeType(String url) {
        String type = null;

        try {
            String extension = MimeTypeMap.getFileExtensionFromUrl(url);
            if (extension == null || extension.trim().equalsIgnoreCase("")) {
                String filenameArray[] = url.split("\\.");
                extension = filenameArray[filenameArray.length - 1];
            }
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            }
        } catch (Exception e) {
            e.printStackTrace();
            type = "";
        }
        return type;
    }

    public static int GetCountArrayResultSet(ArrayList<String> arrayList, String resultType) {
        int count = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            if (resultType.trim().equalsIgnoreCase(arrayList.get(i).trim())) {
                ++count;
            }
        }
        return count;
    }
//
//    public String TranslateText(Context context,String string){
//        Translate.setHttpReferrer("www.krishmark.com");
//        String translatedText=null;
//        try {
//            translatedText = Translate.execute("How are you", Language.ENGLISH, Language.FRENCH);//here You have to pass text to translate,language of you test and language to translate respectively.
//        } catch (Exception e) {
//            e.printStackTrace();
//            translatedText = string;
//        }
//        return translatedText;
//    }

    public static void CreateDirectories(Context context) {
        try {
//            File root = Environment.getExternalStorageDirectory();
            File root = context.getExternalFilesDir(Environment.MEDIA_SHARED);
            if (root == null) {
                // Log.d(TAG, "Failed to get root");
            }

            File file = new File(context.getExternalFilesDir(Environment.MEDIA_SHARED).getAbsolutePath() +
                    StaticVariables.ParentStoragePath, "/.nomedia");
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            File mediaStorageDir = new File(context.getExternalFilesDir(Environment.MEDIA_SHARED).getAbsolutePath() +
                    StaticVariables.ParentStoragePath + "Documents");
            if (!mediaStorageDir.exists())
                mediaStorageDir.mkdirs();

            File mediaStorageDirVideo = new File(context.getExternalFilesDir(Environment.MEDIA_SHARED).getAbsolutePath() +
                    StaticVariables.ParentStoragePath + "Videos");
            if (!mediaStorageDirVideo.exists())
                mediaStorageDirVideo.mkdirs();

            File mediaStorageDirAudio = new File(context.getExternalFilesDir(Environment.MEDIA_SHARED).getAbsolutePath() +
                    StaticVariables.ParentStoragePath + "Audios");
            if (!mediaStorageDirAudio.exists())
                mediaStorageDirAudio.mkdirs();

            File mediaStorageDirImages = new File(context.getExternalFilesDir(Environment.MEDIA_SHARED).getAbsolutePath() +
                    StaticVariables.ParentStoragePath + "Images");
            if (!mediaStorageDirImages.exists())
                mediaStorageDirImages.mkdirs();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void initCDataArray(Context context) {
        try {
            ArrayList<String> Country_list = new ArrayList<>();
            ArrayList<String> Country_code_list = new ArrayList<>();
            String strCounty = "{\"country_list\":[{\"id\":\"344\",\"title\":\"Afghanistan\",\"code\":\"93\"},{\"id\":\"345\",\"title\":\"Albania\",\"code\":\"355\"},{\"id\":\"346\",\"title\":\"Algeria\",\"code\":\"213\"},{\"id\":\"347\",\"title\":\"American Samoa\",\"code\":\"1684\"},{\"id\":\"348\",\"title\":\"Andorra\",\"code\":\"376\"},{\"id\":\"349\",\"title\":\"Angola\",\"code\":\"244\"},{\"id\":\"350\",\"title\":\"Anguilla\",\"code\":\"1264\"},{\"id\":\"351\",\"title\":\"Antarctica\",\"code\":\"672\"},{\"id\":\"352\",\"title\":\"Antigua and Barbuda\",\"code\":\"1268\"},{\"id\":\"353\",\"title\":\"Argentina\",\"code\":\"54\"},{\"id\":\"586\",\"title\":\"Ariba\",\"code\":\"66\"},{\"id\":\"354\",\"title\":\"Armenia\",\"code\":\"374\"},{\"id\":\"355\",\"title\":\"Aruba\",\"code\":\"297\"},{\"id\":\"356\",\"title\":\"Australia\",\"code\":\"61\"},{\"id\":\"357\",\"title\":\"Austria\",\"code\":\"43\"},{\"id\":\"358\",\"title\":\"Azerbaijan\",\"code\":\"994\"},{\"id\":\"359\",\"title\":\"Bahamas\",\"code\":\"1242\"},{\"id\":\"360\",\"title\":\"Bahrain\",\"code\":\"973\"},{\"id\":\"361\",\"title\":\"Bangladesh\",\"code\":\"880\"},{\"id\":\"362\",\"title\":\"Barbados\",\"code\":\"1246\"},{\"id\":\"363\",\"title\":\"Belarus\",\"code\":\"375\"},{\"id\":\"364\",\"title\":\"Belgium\",\"code\":\"32\"},{\"id\":\"365\",\"title\":\"Belize\",\"code\":\"501\"},{\"id\":\"366\",\"title\":\"Benin\",\"code\":\"229\"},{\"id\":\"367\",\"title\":\"Bermuda\",\"code\":\"1441\"},{\"id\":\"368\",\"title\":\"Bhutan\",\"code\":\"975\"},{\"id\":\"369\",\"title\":\"Bolivia\",\"code\":\"591\"},{\"id\":\"371\",\"title\":\"Botswana\",\"code\":\"267\"},{\"id\":\"373\",\"title\":\"Brazil\",\"code\":\"55\"},{\"id\":\"374\",\"title\":\"British Indian Ocean Territory\",\"code\":\"246\"},{\"id\":\"376\",\"title\":\"Bulgaria\",\"code\":\"359\"},{\"id\":\"377\",\"title\":\"Burkina Faso\",\"code\":\"226\"},{\"id\":\"378\",\"title\":\"Burundi\",\"code\":\"257\"},{\"id\":\"379\",\"title\":\"Cambodia\",\"code\":\"855\"},{\"id\":\"380\",\"title\":\"Cameroon\",\"code\":\"237\"},{\"id\":\"381\",\"title\":\"Canada\",\"code\":\"1\"},{\"id\":\"382\",\"title\":\"Cape Verde\",\"code\":\"238\"},{\"id\":\"383\",\"title\":\"Cayman Islands\",\"code\":\"1345\"},{\"id\":\"384\",\"title\":\"Central African Republic\",\"code\":\"236\"},{\"id\":\"385\",\"title\":\"Chad\",\"code\":\"235\"},{\"id\":\"386\",\"title\":\"Chile\",\"code\":\"56\"},{\"id\":\"387\",\"title\":\"China\",\"code\":\"86\"},{\"id\":\"388\",\"title\":\"Christmas Island\",\"code\":\"61\"},{\"id\":\"390\",\"title\":\"Colombia\",\"code\":\"57\"},{\"id\":\"391\",\"title\":\"Comoros\",\"code\":\"269\"},{\"id\":\"394\",\"title\":\"Cook Islands\",\"code\":\"682\"},{\"id\":\"395\",\"title\":\"Costa Rica\",\"code\":\"506\"},{\"id\":\"398\",\"title\":\"Cuba\",\"code\":\"53\"},{\"id\":\"399\",\"title\":\"Cyprus\",\"code\":\"357\"},{\"id\":\"400\",\"title\":\"Czech Republic\",\"code\":\"420\"},{\"id\":\"401\",\"title\":\"Denmark\",\"code\":\"45\"},{\"id\":\"402\",\"title\":\"Djibouti\",\"code\":\"253\"},{\"id\":\"403\",\"title\":\"Dominica\",\"code\":\"1767\"},{\"id\":\"404\",\"title\":\"Dominican Republic\",\"code\":\"1809, 1829, 1849\"},{\"id\":\"405\",\"title\":\"East Timor\",\"code\":\"670\"},{\"id\":\"406\",\"title\":\"Ecuador\",\"code\":\"593\"},{\"id\":\"407\",\"title\":\"Egypt\",\"code\":\"20\"},{\"id\":\"408\",\"title\":\"El Salvador\",\"code\":\"503\"},{\"id\":\"409\",\"title\":\"Equatorial Guinea\",\"code\":\"240\"},{\"id\":\"410\",\"title\":\"Eritrea\",\"code\":\"291\"},{\"id\":\"411\",\"title\":\"Estonia\",\"code\":\"372\"},{\"id\":\"412\",\"title\":\"Ethiopia\",\"code\":\"251\"},{\"id\":\"414\",\"title\":\"Faroe Islands\",\"code\":\"298\"},{\"id\":\"415\",\"title\":\"Fiji\",\"code\":\"679\"},{\"id\":\"416\",\"title\":\"Finland\",\"code\":\"358\"},{\"id\":\"417\",\"title\":\"France\",\"code\":\"33\"},{\"id\":\"420\",\"title\":\"French Polynesia\",\"code\":\"689\"},{\"id\":\"422\",\"title\":\"Gabon\",\"code\":\"241\"},{\"id\":\"423\",\"title\":\"Gambia\",\"code\":\"220\"},{\"id\":\"424\",\"title\":\"Georgia\",\"code\":\"995\"},{\"id\":\"425\",\"title\":\"Germany\",\"code\":\"49\"},{\"id\":\"426\",\"title\":\"Ghana\",\"code\":\"233\"},{\"id\":\"427\",\"title\":\"Gibraltar\",\"code\":\"350\"},{\"id\":\"428\",\"title\":\"Greece\",\"code\":\"30\"},{\"id\":\"429\",\"title\":\"Greenland\",\"code\":\"299\"},{\"id\":\"430\",\"title\":\"Grenada\",\"code\":\"1473\"},{\"id\":\"432\",\"title\":\"Guam\",\"code\":\"1671\"},{\"id\":\"433\",\"title\":\"Guatemala\",\"code\":\"502\"},{\"id\":\"434\",\"title\":\"Guinea\",\"code\":\"224\"},{\"id\":\"435\",\"title\":\"Guinea-Bissau\",\"code\":\"245\"},{\"id\":\"436\",\"title\":\"Guyana\",\"code\":\"592\"},{\"id\":\"437\",\"title\":\"Haiti\",\"code\":\"509\"},{\"id\":\"440\",\"title\":\"Honduras\",\"code\":\"504\"},{\"id\":\"441\",\"title\":\"Hong Kong\",\"code\":\"852\"},{\"id\":\"442\",\"title\":\"Hungary\",\"code\":\"36\"},{\"id\":\"443\",\"title\":\"Iceland\",\"code\":\"354\"},{\"id\":\"444\",\"title\":\"India\",\"code\":\"91\"},{\"id\":\"445\",\"title\":\"Indonesia\",\"code\":\"62\"},{\"id\":\"447\",\"title\":\"Iraq\",\"code\":\"964\"},{\"id\":\"448\",\"title\":\"Ireland\",\"code\":\"353\"},{\"id\":\"449\",\"title\":\"Israel\",\"code\":\"972\"},{\"id\":\"450\",\"title\":\"Italy\",\"code\":\"39\"},{\"id\":\"451\",\"title\":\"Jamaica\",\"code\":\"1876\"},{\"id\":\"452\",\"title\":\"Japan\",\"code\":\"81\"},{\"id\":\"453\",\"title\":\"Jordan\",\"code\":\"962\"},{\"id\":\"454\",\"title\":\"Kazakhstan\",\"code\":\"7\"},{\"id\":\"455\",\"title\":\"Kenya\",\"code\":\"254\"},{\"id\":\"456\",\"title\":\"Kiribati\",\"code\":\"686\"},{\"id\":\"459\",\"title\":\"Kuwait\",\"code\":\"965\"},{\"id\":\"460\",\"title\":\"Kyrgyzstan\",\"code\":\"996\"},{\"id\":\"462\",\"title\":\"Latvia\",\"code\":\"371\"},{\"id\":\"463\",\"title\":\"Lebanon\",\"code\":\"961\"},{\"id\":\"464\",\"title\":\"Lesotho\",\"code\":\"266\"},{\"id\":\"465\",\"title\":\"Liberia\",\"code\":\"231\"},{\"id\":\"467\",\"title\":\"Liechtenstein\",\"code\":\"423\"},{\"id\":\"468\",\"title\":\"Lithuania\",\"code\":\"370\"},{\"id\":\"469\",\"title\":\"Luxembourg\",\"code\":\"352\"},{\"id\":\"470\",\"title\":\"Macau\",\"code\":\"853\"},{\"id\":\"472\",\"title\":\"Madagascar\",\"code\":\"261\"},{\"id\":\"473\",\"title\":\"Malawi\",\"code\":\"265\"},{\"id\":\"474\",\"title\":\"Malaysia\",\"code\":\"60\"},{\"id\":\"475\",\"title\":\"Maldives\",\"code\":\"960\"},{\"id\":\"476\",\"title\":\"Mali\",\"code\":\"223\"},{\"id\":\"477\",\"title\":\"Malta\",\"code\":\"356\"},{\"id\":\"478\",\"title\":\"Marshall Islands\",\"code\":\"692\"},{\"id\":\"480\",\"title\":\"Mauritania\",\"code\":\"222\"},{\"id\":\"481\",\"title\":\"Mauritius\",\"code\":\"230\"},{\"id\":\"482\",\"title\":\"Mayotte\",\"code\":\"262\"},{\"id\":\"483\",\"title\":\"Mexico\",\"code\":\"52\"},{\"id\":\"486\",\"title\":\"Monaco\",\"code\":\"377\"},{\"id\":\"487\",\"title\":\"Mongolia\",\"code\":\"976\"},{\"id\":\"488\",\"title\":\"Montserrat\",\"code\":\"1664\"},{\"id\":\"489\",\"title\":\"Morocco\",\"code\":\"212\"},{\"id\":\"490\",\"title\":\"Mozambique\",\"code\":\"258\"},{\"id\":\"491\",\"title\":\"Myanmar\",\"code\":\"95\"},{\"id\":\"492\",\"title\":\"Namibia\",\"code\":\"264\"},{\"id\":\"493\",\"title\":\"Nauru\",\"code\":\"674\"},{\"id\":\"494\",\"title\":\"Nepal\",\"code\":\"977\"},{\"id\":\"495\",\"title\":\"Netherlands\",\"code\":\"31\"},{\"id\":\"496\",\"title\":\"Netherlands Antilles\",\"code\":\"599\"},{\"id\":\"497\",\"title\":\"New Caledonia\",\"code\":\"687\"},{\"id\":\"498\",\"title\":\"New Zealand\",\"code\":\"64\"},{\"id\":\"499\",\"title\":\"Nicaragua\",\"code\":\"505\"},{\"id\":\"500\",\"title\":\"Niger\",\"code\":\"227\"},{\"id\":\"501\",\"title\":\"Nigeria\",\"code\":\"234\"},{\"id\":\"502\",\"title\":\"Niue\",\"code\":\"683\"},{\"id\":\"504\",\"title\":\"Northern Mariana Islands\",\"code\":\"1670\"},{\"id\":\"505\",\"title\":\"Norway\",\"code\":\"47\"},{\"id\":\"506\",\"title\":\"Oman\",\"code\":\"968\"},{\"id\":\"507\",\"title\":\"Pakistan\",\"code\":\"92\"},{\"id\":\"508\",\"title\":\"Palau\",\"code\":\"680\"},{\"id\":\"509\",\"title\":\"Panama\",\"code\":\"507\"},{\"id\":\"510\",\"title\":\"Papua New Guinea\",\"code\":\"675\"},{\"id\":\"511\",\"title\":\"Paraguay\",\"code\":\"595\"},{\"id\":\"512\",\"title\":\"Peru\",\"code\":\"51\"},{\"id\":\"513\",\"title\":\"Philippines\",\"code\":\"63\"},{\"id\":\"514\",\"title\":\"Pitcairn\",\"code\":\"64\"},{\"id\":\"515\",\"title\":\"Poland\",\"code\":\"48\"},{\"id\":\"516\",\"title\":\"Portugal\",\"code\":\"351\"},{\"id\":\"517\",\"title\":\"Puerto Rico\",\"code\":\"1787, 1939\"},{\"id\":\"518\",\"title\":\"Qatar\",\"code\":\"974\"},{\"id\":\"519\",\"title\":\"Reunion\",\"code\":\"262\"},{\"id\":\"520\",\"title\":\"Romania\",\"code\":\"40\"},{\"id\":\"522\",\"title\":\"Rwanda\",\"code\":\"250\"},{\"id\":\"523\",\"title\":\"Saint Kitts and Nevis\",\"code\":\"1869\"},{\"id\":\"524\",\"title\":\"Saint Lucia\",\"code\":\"1758\"},{\"id\":\"525\",\"title\":\"Saint Vincent and the Grenadines\",\"code\":\"1784\"},{\"id\":\"526\",\"title\":\"Samoa\",\"code\":\"685\"},{\"id\":\"527\",\"title\":\"San Marino\",\"code\":\"378\"},{\"id\":\"528\",\"title\":\"Sao Tome and Principe\",\"code\":\"239\"},{\"id\":\"529\",\"title\":\"Saudi Arabia\",\"code\":\"966\"},{\"id\":\"530\",\"title\":\"Senegal\",\"code\":\"221\"},{\"id\":\"531\",\"title\":\"Seychelles\",\"code\":\"248\"},{\"id\":\"532\",\"title\":\"Sierra Leone\",\"code\":\"232\"},{\"id\":\"533\",\"title\":\"Singapore\",\"code\":\"65\"},{\"id\":\"535\",\"title\":\"Slovenia\",\"code\":\"386\"},{\"id\":\"536\",\"title\":\"Solomon Islands\",\"code\":\"677\"},{\"id\":\"537\",\"title\":\"Somalia\",\"code\":\"252\"},{\"id\":\"538\",\"title\":\"South Africa\",\"code\":\"27\"},{\"id\":\"540\",\"title\":\"Spain\",\"code\":\"34\"},{\"id\":\"541\",\"title\":\"Sri Lanka\",\"code\":\"94\"},{\"id\":\"544\",\"title\":\"Sudan\",\"code\":\"249\"},{\"id\":\"545\",\"title\":\"Suriname\",\"code\":\"597\"},{\"id\":\"547\",\"title\":\"Swaziland\",\"code\":\"268\"},{\"id\":\"548\",\"title\":\"Sweden\",\"code\":\"46\"},{\"id\":\"549\",\"title\":\"Switzerland\",\"code\":\"41\"},{\"id\":\"552\",\"title\":\"Tajikistan\",\"code\":\"992\"},{\"id\":\"554\",\"title\":\"Thailand\",\"code\":\"66\"},{\"id\":\"555\",\"title\":\"Togo\",\"code\":\"228\"},{\"id\":\"556\",\"title\":\"Tokelau\",\"code\":\"690\"},{\"id\":\"557\",\"title\":\"Tonga\",\"code\":\"676\"},{\"id\":\"558\",\"title\":\"Trinidad and Tobago\",\"code\":\"1868\"},{\"id\":\"559\",\"title\":\"Tunisia\",\"code\":\"216\"},{\"id\":\"560\",\"title\":\"Turkey\",\"code\":\"90\"},{\"id\":\"561\",\"title\":\"Turkmenistan\",\"code\":\"993\"},{\"id\":\"562\",\"title\":\"Turks and Caicos Islands\",\"code\":\"1649\"},{\"id\":\"563\",\"title\":\"Tuvalu\",\"code\":\"688\"},{\"id\":\"564\",\"title\":\"Uganda\",\"code\":\"256\"},{\"id\":\"565\",\"title\":\"Ukraine\",\"code\":\"380\"},{\"id\":\"566\",\"title\":\"United Arab Emirates\",\"code\":\"971\"},{\"id\":\"567\",\"title\":\"United Kingdom\",\"code\":\"44\"},{\"id\":\"568\",\"title\":\"United States\",\"code\":\"1\"},{\"id\":\"570\",\"title\":\"Uruguay\",\"code\":\"598\"},{\"id\":\"571\",\"title\":\"Uzbekistan\",\"code\":\"998\"},{\"id\":\"572\",\"title\":\"Vanuatu\",\"code\":\"678\"},{\"id\":\"573\",\"title\":\"Venezuela\",\"code\":\"58\"},{\"id\":\"574\",\"title\":\"Vietnam\",\"code\":\"84\"},{\"id\":\"578\",\"title\":\"Western Sahara\",\"code\":\"212\"},{\"id\":\"579\",\"title\":\"Yemen\",\"code\":\"967\"},{\"id\":\"581\",\"title\":\"Zambia\",\"code\":\"260\"},{\"id\":\"582\",\"title\":\"Zimbabwe\",\"code\":\"263\"}]}\n";
            JSONObject DataJsonObject = null;

            DataJsonObject = new JSONObject(strCounty);
            JSONArray jsonArray = DataJsonObject.getJSONArray("country_list");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = new JSONObject(jsonArray.get(i).toString());
                Country_list.add(i, jsonObject.getString("title"));
                Country_code_list.add(i, jsonObject.getString("code"));
            }
            for (int j = 0; j < Country_list.size(); j++) {
                DatabaseHelper.SaveCountry(context, DatabaseHelper.db, Country_list.get(j), Country_code_list.get(j));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void initPDataArray(Context context) {
        try {
            String[] arrProfession = new String[]{"Retired", "Software Professional", "Supervisor", "Sales Person", "Self Employed Professional", "Shop Owner", "Student", "Not Working", "Housewife", "Advocate/Lawyer", "Chartered Accounts", "Clerical", "Service", "Others"};
            String[] arrProfessionCode = new String[]{"R", "SP", "S", "SLP", "SEP", "SO", "St", "NW", "H", "AL", "CA", "C", "Ser", "O"};
            Arrays.sort(arrProfession);
            for (int i = 0; i < arrProfession.length; i++) {
                DatabaseHelper.SaveProfession(context, DatabaseHelper.db, arrProfessionCode[i], arrProfession[i]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean onDropCustomTables(SQLiteDatabase db, Context context) {

        try {

//            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_iCandidate));
            db.execSQL(context.getResources().getString(R.string.dropIsysLookupParam));
            db.execSQL(context.getResources().getString(R.string.dropLPICandidateExamAttempt));
            db.execSQL(context.getResources().getString(R.string.dropLPICandidateExamAttemptDtl));
            db.execSQL(context.getResources().getString(R.string.dropLPICandidateExamMapping));
            db.execSQL(context.getResources().getString(R.string.dropLPICandidateExamResult));
            db.execSQL(context.getResources().getString(R.string.dropLPICandidateExamResultFeedback));
            db.execSQL(context.getResources().getString(R.string.dropSyncMstSU));
            db.execSQL(context.getResources().getString(R.string.dropLPIExamAnswerMST));
            db.execSQL(context.getResources().getString(R.string.dropLPIExamMST));
            db.execSQL(context.getResources().getString(R.string.dropLPIExamQuestionMST));
            db.execSQL(context.getResources().getString(R.string.dropLPIQuestionCorrectAnswerMapping));
            db.execSQL(context.getResources().getString(R.string.dropLPIExamQueAns_SyncMaster));
            db.execSQL(context.getResources().getString(R.string.dropLPICandidateAttemptQueAns));
            db.execSQL(context.getResources().getString(R.string.dropLPICandidateAttemptedExam));
            db.execSQL(context.getResources().getString(R.string.dropLPIErrorLog));
            db.execSQL(context.getResources().getString(R.string.dropPostLicModuleData));
            db.execSQL(context.getResources().getString(R.string.dropActivityMaster));
            db.execSQL(context.getResources().getString(R.string.dropActivityTracker));
            db.execSQL(context.getResources().getString(R.string.dropCreateTblLPI_BasicDetails));
            db.execSQL(context.getResources().getString(R.string.dropLPI_ViewSummary));
            db.execSQL(context.getResources().getString(R.string.dropZipContent));
            db.execSQL(context.getResources().getString(R.string.dropContentQueries));
            db.execSQL(context.getResources().getString(R.string.dropLPISysGeneratedXML));
            db.execSQL(context.getResources().getString(R.string.dropLPICandidateRegistration));

            db.execSQL(context.getResources().getString(R.string.dropFAQ_List));
            db.execSQL(context.getResources().getString(R.string.dropReferApp));
            db.execSQL(context.getResources().getString(R.string.dropProfession));
            db.execSQL(context.getResources().getString(R.string.dropCountry));
            db.execSQL(context.getResources().getString(R.string.dropFAQ_Group));
            db.execSQL(context.getResources().getString(R.string.dropTBL_SearchResult));
            db.execSQL(context.getResources().getString(R.string.dropTBL_CndFeedback));
            db.execSQL(context.getResources().getString(R.string.dropTbl_TokenMst));

            return true;
        } catch (Exception e) {
            Log.e("", "Error At table = " + e.toString());
            return false;
        }

    }

    public static boolean onCreateCustomTables(SQLiteDatabase db, Context context) {
        try {
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_iCandidate));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_IsysLookupParam));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPICandidateExamAttempt));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPICandidateExamAttemptDtl));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPICandidateExamMapping));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPICandidateExamResult));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPICandidateExamResultFeedback));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPIChangeMaster));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPIExamAnswerMST));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPIExamMST));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPIExamQuestionMST));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPIQuestionCorrectAnswerMapping));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_ExamQueAns_SyncMaster));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_LPICandidateAttemptQueAns));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPICandidateAttemptedExam));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_LPIErrorLog));
            db.execSQL(context.getResources().getString(R.string.create_TblPostLicModuleData));
            db.execSQL(context.getResources().getString(R.string.create_TblActivityMaster));
            db.execSQL(context.getResources().getString(R.string.create_TblActivityTracker));
            db.execSQL(context.getResources().getString(R.string.CreateTblLPI_BasicDetails));
            db.execSQL(context.getResources().getString(R.string.CreateTblLPI_ViewSummary));
            db.execSQL(context.getResources().getString(R.string.CreateTblLPI_ZipContent));
            db.execSQL(context.getResources().getString(R.string.CreateTblLPI_ContentQueries));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_LPISysGeneratedXML));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_LPICandidateRegistration));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_FAQ_Conversation));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_FAQ_List));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_ReferApp));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_Profession));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_Country));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_FAQ_Group));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_SearchResult));
            db.execSQL(context.getResources().getString(R.string.CreateTbl_TBL_CndFeedback));
            db.execSQL(context.getResources().getString(R.string.createtbl_settings));
            db.execSQL(context.getResources().getString(R.string.createTbl_TokenMst));
            return true;
        } catch (Exception e) {
            Log.e("", "Error At table = " + e.toString());
            return false;
        }

    }

    static public void createNotification(Context context, String Title, String message, Intent intent) {
        int uniqueId = (int) System.currentTimeMillis();
        NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = null;
        if (intent != null) {
//            pendingIntent = PendingIntent.getActivity(context, uniqueId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), uniqueId, intent, PendingIntent.FLAG_IMMUTABLE);
            } else {
                pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), uniqueId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            }
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_appicon));
            builder.setSmallIcon(R.drawable.ic_notification_white);///use app icon
            builder.setColor(ContextCompat.getColor(context, R.color.primary));
        } else {
            builder.setSmallIcon(R.drawable.ic_notification_white); ///use app icon
        }
        builder.setAutoCancel(true)
                .setContentTitle(Title)
                .setContentIntent(pendingIntent)
                .setContentText(message)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setPriority(Notification.PRIORITY_HIGH);

        builder.getNotification().flags |= Notification.FLAG_AUTO_CANCEL;
        builder.setOngoing(false);
        builder.setAutoCancel(true);
        builder.setVibrate(new long[]{500, 1000});
        builder.setLights(Color.RED, 3000, 3000);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(uri);
        Notification n = builder.build();
        n.flags |= Notification.FLAG_AUTO_CANCEL;
        nm.notify(uniqueId, n);
    }

    @SuppressLint("NewApi")
    static public void createNotification(Context context, String Title, String message, String response) {
        int uniqueId = (int) System.currentTimeMillis();
        NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = null;
        if (!response.equals("")) {
            String[] str = response.split("|&|");
            Intent intent = new Intent(context, LPI_FAQ_ConversationActivity.class);
            intent.putExtra("question", str[0]);
            intent.putExtra("question_id", str[1]);
            intent.putExtra("user_id", str[2]);
            intent.putExtra("trainer_id", str[3]);
            intent.putExtra("user_name", str[4]);
            intent.putExtra("profile_pic", "");
            intent.putExtra("isClosed", str[5]);
//            pendingIntent = PendingIntent.getActivity(context, uniqueId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), uniqueId, intent, PendingIntent.FLAG_IMMUTABLE);
            } else {
                pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), uniqueId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            }
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_appicon));
            builder.setSmallIcon(R.drawable.ic_notification_white);///use app icon
            builder.setColor(ContextCompat.getColor(context, R.color.primary));
        } else {
            builder.setSmallIcon(R.drawable.ic_notification_white); ///use app icon
        }
        builder.setAutoCancel(true)
                .setContentTitle(Title)
                .setContentIntent(pendingIntent)
                .setContentText(message)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setPriority(Notification.PRIORITY_HIGH);

        builder.getNotification().flags |= Notification.FLAG_AUTO_CANCEL;
        builder.setOngoing(false);
        builder.setAutoCancel(true);
        builder.setVibrate(new long[]{500, 1000});
        builder.setLights(Color.RED, 3000, 3000);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(uri);
        Notification n = builder.build();
        n.flags |= Notification.FLAG_AUTO_CANCEL;
        nm.notify(uniqueId, n);
    }

    public static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }



//    public static void startFAQJob(Context context) {
//        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
//        Job myJob = dispatcher.newJobBuilder()
//                // the JobService that will be called
//                .setService(JobSyncFAQ.class)
//                // uniquely identifies the job
//                .setTag("JobSyncFAQ_12345")
//                // one-off job
//                .setRecurring(true)
//                // don't persist past a device reboot
//                .setLifetime(Lifetime.UNTIL_NEXT_BOOT)
//                // start between 0 and 60 seconds from now
//                .setTrigger(Trigger.executionWindow(0, 60))
//                // don't overwrite an existing job with the same tag
//                .setReplaceCurrent(false)
//                // retry with exponential backoff
//                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
//                // constraints that need to be satisfied for the job to run
//                .setConstraints(
//                        // only run on an unmetered network
//                        Constraint.ON_UNMETERED_NETWORK,
//                        // only run when the device is charging
//                        Constraint.DEVICE_CHARGING,
//                        Constraint.DEVICE_IDLE
//                )
//                .build();
//
//        dispatcher.mustSchedule(myJob);
//    }

    // Device Id
    public String GetDeviceId(Context cntxt) {
        String android_id = Settings.Secure.getString(cntxt.getContentResolver(), Settings.Secure.ANDROID_ID);
        return android_id;
    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    public static Boolean isActivityRunning(Context context, Class activityClass) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (ActivityManager.RunningTaskInfo task : tasks) {
            if (activityClass.getCanonicalName().equalsIgnoreCase(task.baseActivity.getClassName()))
                return true;
        }
        return false;
    }

    public static NodeList getExamDatafrmXML(Context context, InputStream strXML, String NodeListItem) {
        NodeList nList = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(strXML);
            Element element = doc.getDocumentElement();
            element.normalize();
            nList = doc.getElementsByTagName(NodeListItem);
        } catch (Exception e) {
            Log.e("", "error=" + e.toString());
            DatabaseHelper.SaveErroLog(context, DatabaseHelper.db,
                    "SyncExamMaster", "getExamDatafrmXML()", e.toString());
        }
        return nList;
    }

    public static void checkSettings(Context context, String update) {
        try {
            ContentValues values = new ContentValues();
            values.clear();
            Cursor cursor = DatabaseHelper.db.query(context.getResources().getString(R.string.tbl_settings),
                    new String[]{"wifi", "cellular", "notification"}, "userid=?",
                    new String[]{"" + StaticVariables.UserLoginId}, null, null, null, "1");
            if (cursor.getCount() > 0) {
                if (update.equals("U")) {
                    values.clear();
                    values.put("userid", "" + StaticVariables.UserLoginId);
                    values.put("wifi", "" + StaticVariables.SyncWIFI);
                    values.put("cellular", "" + StaticVariables.SyncMobile);
                    values.put("notification", "" + StaticVariables.Notification);
                    DatabaseHelper.db.update(context.getResources().getString(R.string.tbl_settings), values,
                            "userid=?", new String[]{"" + StaticVariables.UserLoginId});
                } else {
                    for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                        StaticVariables.SyncWIFI = cursor.getString(0);
                        StaticVariables.SyncMobile = cursor.getString(1);
                        StaticVariables.Notification = cursor.getString(2);
                    }
                }
            } else {
                values.clear();
                values.put("userid", "" + StaticVariables.UserLoginId);
                values.put("wifi", "" + StaticVariables.SyncWIFI);
                values.put("cellular", "" + StaticVariables.SyncMobile);
                values.put("notification", "" + StaticVariables.Notification);
                DatabaseHelper.db.insert(context.getResources().getString(R.string.tbl_settings), null, values);
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String saveFilesToPath(Context context, String fileName, String mimeType, String path,
                                  String URL){
        String result="";
        Uri uri = null;
        try {
            URL url = new URL(URL + fileName); // link of the song which you want to download like (http://...)
            InputStream input = url.openStream();
            OutputStream output=null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.MediaColumns.DISPLAY_NAME,fileName);
                values.put(MediaStore.MediaColumns.MIME_TYPE,mimeType);
                values.put(MediaStore.MediaColumns.RELATIVE_PATH,Environment.MEDIA_SHARED + path);
                uri = context.getContentResolver().insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI,values);
                output = context.getContentResolver().openOutputStream(uri);
            }else {
                File zip = new File(Environment.getExternalStoragePublicDirectory(Environment.MEDIA_SHARED)+path, fileName);
                output = new FileOutputStream(zip,false);
            }

            try {
                byte[] buffer = new byte[1024];
                int bytesRead = 0;
                while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
                    output.write(buffer, 0, bytesRead);
                }
                output.close();
                result= "Success";
            } catch (Exception exception) {
                output.close();
                Log.e("", "Error=" + exception.toString());
                result= "Failed";
            }
        } catch (Exception e) {
            e.printStackTrace();
            result= "Failed";
        }
        return result;
    }

    public static Boolean unzip(String sourceFile, String destinationFolder) {
        ZipInputStream zis = null;

        try {
            zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(sourceFile)));
            ZipEntry ze;
            int count;
            byte[] buffer = new byte[1024];
            while ((ze = zis.getNextEntry()) != null) {
                String filename = ze.getName();
                if (filename.contains("/")) {
                    File fmd = new File(destinationFolder, filename.substring(0, filename.indexOf("/")));
                    if (fmd.isDirectory())
                        fmd.delete();
                    fmd.mkdirs();
                }
                if (ze.isDirectory()) {

                    continue;
                }
                FileOutputStream fout = new FileOutputStream(destinationFolder + filename);
                try {
                    while ((count = zis.read(buffer)) != -1)
                        fout.write(buffer, 0, count);
                } finally {
                    fout.close();
                }
            }
            return true;
        } catch (IOException ioe) {
            Log.d("unzip", "unzip error=" + ioe.getMessage());
            return false;
        }
    }

    public static void DeleteFileFolders(File fileOrDirectory) {
        try {
            if (fileOrDirectory.isDirectory())
                for (File child : fileOrDirectory.listFiles())
                    DeleteFileFolders(child);

            fileOrDirectory.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static File getOutputMediaFile(Context context, String path,String fileName) {
        File mediaFile = null;
        try {
            File mediaStorageDir = new File(path);
            if (!mediaStorageDir.exists()) {
                /*if (!mediaStorageDir.mkdirs()) {
                    return null;
                }*/
            } else {

                File file = new File(mediaStorageDir.getPath() + File.separator + ".nomedia");
                if (!file.exists()) {
                    File output = new File(mediaStorageDir, ".nomedia");
                    boolean fileCreated = output.createNewFile();
                }
            }

            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + fileName);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mediaFile;
    }

    public static boolean isValidCursor(Cursor mCursor) {
        try {
            if (mCursor != null && mCursor.getCount() > 0)
                return true;
            else
                return false;
        } catch (NoSuchMethodError e) {
            e.printStackTrace();
            return false;
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static long getMilliFromDate(String strDate) {
        strDate.replace("'T'", " ");
        strDate.replace("T", " ");
        Date date = new Date();
        try {
            //SimpleDateFormat mDateFormat = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat simpleDateFormat;
            if (strDate.contains("T")) {
                simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            } else {
                simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            }

            date = simpleDateFormat.parse(strDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    public String getTimeStamp(long timeinMillies) {
        String date = null;
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        date = formatter.format(new Date(timeinMillies));

        return date;
    }

    public static void StartService(Context context, Intent serviceIntent) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                ContextCompat.startForegroundService(context, serviceIntent);
            } else {
                context.startService(serviceIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getNotificationId() {
        return sNOTIFICATION_ID;
    }

    public static Notification createServiceNotification(Context context) {
        NotificationCompat.Builder builder;
        String strMessage = "Background Service is running.";
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        RemoteViews mRemoteViews;
        Intent intentNotif = new Intent(context, StopServiceReceiver.class);
        PendingIntent pendIntent;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            pendIntent = PendingIntent.getBroadcast(context, (int) System.currentTimeMillis(), intentNotif, PendingIntent.FLAG_IMMUTABLE);
        } else {
//            pendingIntent = PendingIntent.getBroadcast(LaunchScreen.this, 0, alarmIntent, 0);
            pendIntent = PendingIntent.getBroadcast(context, (int) System.currentTimeMillis(), intentNotif, PendingIntent.FLAG_MUTABLE);
        }


        mRemoteViews = new RemoteViews(context.getPackageName(), R.layout.notification_service_view);
        mRemoteViews.setImageViewResource(R.id.notif_icon, R.mipmap.ic_launcher_trainee);
        mRemoteViews.setTextViewText(R.id.notif_title, context.getResources().getString(R.string.app_name));
        // notification's content
//            mRemoteViews.setTextViewText(R.id.notif_content, getResources().getString(R.string.content_text));
        mRemoteViews.setTextViewText(R.id.notif_content, strMessage);
        mRemoteViews.setOnClickPendingIntent(R.id.imgStop, pendIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel mChannel = mNotificationManager.getNotificationChannel(sNOTIFICATION_ID + "");
            if (mChannel == null) {
                mChannel = new NotificationChannel(sNOTIFICATION_ID + "", context.getResources().getString(R.string.app_name), importance);
                mChannel.enableVibration(false);
                mChannel.setSound(null, null);
//                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                mNotificationManager.createNotificationChannel(mChannel);
            }
            builder = new NotificationCompat.Builder(context, sNOTIFICATION_ID + "");
            builder.setContent(mRemoteViews)                         // required
                    .setContentText(context.getString(R.string.app_name)) // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setSound(null)
                    .setAutoCancel(true)
                    .setPriority(Notification.PRIORITY_LOW);
            NotificationCompat.InboxStyle inboxStyle =
                    new NotificationCompat.InboxStyle();
            inboxStyle.addLine(strMessage);
            builder.setStyle(inboxStyle);
            builder.getNotification().flags |= Notification.FLAG_AUTO_CANCEL;
            builder.setOngoing(false);
            builder.setAutoCancel(true);
//            builder.setVibrate(new long[]{500, 1000});
            builder.setLights(Color.RED, 3000, 3000);
            builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher_trainee));
            builder.setSmallIcon(R.mipmap.ic_launcher_trainee);
            builder.setColor(ContextCompat.getColor(context, R.color.primary));
//            context.startForeground(NOTIF_ID, builder.build());
        } else {
            NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            builder = new NotificationCompat.Builder(context);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher_trainee));
                builder.setSmallIcon(R.mipmap.ic_launcher_trainee);
                builder.setColor(ContextCompat.getColor(context, R.color.primary));
            } else {
                builder.setSmallIcon(R.mipmap.ic_launcher_trainee);
            }
            builder.setAutoCancel(true)
                    .setContent(mRemoteViews)
                    .setSound(null)
                    .setPriority(Notification.PRIORITY_LOW);
            NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
            builder.setStyle(inboxStyle);
            builder.getNotification().flags |= Notification.FLAG_AUTO_CANCEL;
            builder.setOngoing(false);
            builder.setAutoCancel(true);
//            startForeground(NOTIF_ID, builder.build());
        }
        return builder.build();
    }

}
