package com.lpi.kmi.lpi.classes;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.lpi.kmi.lpi.R;

import java.util.ArrayList;

/**
 * Created by bharatg on 24-Mar-18.
 */

public class CustomDialog extends Dialog {
    ListView dialoglist;
    TextView tv_title;
    ArrayList<String> arrayList;
    String title = "";
    Context context;
    int CUSTOM_DIALOG_ID;
    TextView textView;
    Callback<String> callback;
    ImageView iv_close;

    public CustomDialog(Context context, ArrayList<String> arrayList, String title, int CUSTOM_DIALOG_ID, TextView textView, Callback<String> callback) {
        super(context);
        this.context = context;
        this.arrayList = arrayList;
        this.title = title;
        this.CUSTOM_DIALOG_ID = CUSTOM_DIALOG_ID;
        this.textView = textView;
        this.callback = callback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_list_item);
        getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        initView();
        UIListener();
    }

    private void UIListener() {
        dialoglist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                String strSelected = arrayList.get(position);
                textView.setText(strSelected);
                callback.execute(strSelected, "");
                dismiss();
            }

        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    private void initView() {
        dialoglist = (ListView) findViewById(R.id.dialoglist);
        tv_title = (TextView) findViewById(R.id.tv_title);
        iv_close = (ImageView) findViewById(R.id.iv_close);
        tv_title.setText(title);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_list_item_1, android.R.id.text1, arrayList);
        dialoglist.setAdapter(adapter);
    }
}
