package com.lpi.kmi.lpi.classes;

/**
 * Created by mustafakachwalla on 06/06/17.
 */

import android.app.Application;
import android.content.Context;
import android.util.Base64;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.activities.Login;
import com.stringcare.library.SC;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import Decoder.BASE64Encoder;

public class EncrptDcrpt extends Application {

    private static Context mContext;

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        mContext = this;
    }

    public static Context getContext() {
        return mContext;
    }

    public static String encrypt(String value) {
        try {
//            String key = "" + Login.seedValue;
            byte[] keyBytes = new byte[16];
            byte[] b = SC.reveal(CommonClass.cipher).getBytes("UTF8");
            int len = b.length;
            if (len > keyBytes.length)
                len = keyBytes.length;
            System.arraycopy(b, 0, keyBytes, 0, len);
            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");

            IvParameterSpec iv = new IvParameterSpec(keyBytes);

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());

            BASE64Encoder encoder = new BASE64Encoder();
            return encoder.encode(encrypted); // it returns the result as a String

        } catch (Exception ex) {
            ex.printStackTrace();
            DatabaseHelper.SaveErroLog(mContext, DatabaseHelper.db,
                    "EncrptDcrpt", "encrypt()", ex.toString());
        }

        return null;
    }

    public static String decrypt(String text) {
        try {
//            String key = "" + Login.seedValue;

//	        	Resources.getSystem().getString(R.string.Cancel);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            byte[] keyBytes = new byte[16];
//	            byte[] b= generateKey().toString().getBytes("UTF-8");
            byte[] b = SC.reveal(CommonClass.cipher).getBytes("UTF8");
            int len = b.length;
            if (len > keyBytes.length) len = keyBytes.length;
            System.arraycopy(b, 0, keyBytes, 0, len);
            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
            IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
            byte[] results = cipher.doFinal(Base64.decode(text, 0));
            return new String(results, "UTF-8");
        } catch (Exception e) {
            // TODO: handle exception
            //Log.e(("decryption error=", e.getMessage());
            DatabaseHelper.SaveErroLog(mContext, DatabaseHelper.db,
                    "EncrptDcrpt", "decrypt()", e.toString());
            return null;

        }
    }


    public static String decrypt_key(String text, String key) {
        try {
//	        	Resources.getSystem().getString(R.string.Cancel);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            byte[] keyBytes = new byte[16];
//	            byte[] b= generateKey().toString().getBytes("UTF-8");
            byte[] b = key.getBytes("UTF8");
            int len = b.length;
            if (len > keyBytes.length) len = keyBytes.length;
            System.arraycopy(b, 0, keyBytes, 0, len);
            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
            IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
            byte[] results = cipher.doFinal(Base64.decode(text, 0));
            return new String(results, "UTF-8");
        } catch (Exception e) {
            // TODO: handle exception
            //Log.e(("decryption error=", e.getMessage());
            DatabaseHelper.SaveErroLog(mContext, DatabaseHelper.db,
                    "EncrptDcrpt", "decrypt_key()", e.toString());
            return null;
        }
    }


    private SecretKey generateKey() throws NoSuchAlgorithmException {
        // Generate a 256-bit key
        final int outputKeyLength = 256;

        SecureRandom secureRandom = new SecureRandom();
        // Do *not* seed secureRandom! Automatically seeded from system entropy.
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(outputKeyLength, secureRandom);
        SecretKey key = keyGenerator.generateKey();
        return key;
    }


    public static String XMLEncryptiton(String text) throws Exception {
        //1DONTWANT2NOPASSWORD
        //1DONT
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] keyBytes = new byte[16];
//			byte[] b = "1DONTWANT2NOPASSWORD".getBytes("UTF8");
//			byte[] b = "1DONT".getBytes("UTF8");

        byte[] b = SC.reveal(CommonClass.seed).getBytes("UTF8");
        int len = b.length;

        if (len > keyBytes.length)
            len = keyBytes.length;

        System.arraycopy(b, 0, keyBytes, 0, len);
        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

        byte[] results = cipher.doFinal(text.getBytes("UTF-8"));
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(results); // it returns the result as a String
    }


    public static String EncryptionString(String text) throws Exception {
        //1DONTWANT2NOPASSWORD
        //1DONT
//			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
        byte[] keyBytes = new byte[16];
//			byte[] b = "1DONTWANT2NOPASSWORD".getBytes("UTF8");
//			byte[] b = "1DONT".getBytes("UTF8");
        byte[] b = "pass@122".getBytes("UTF8");
        int len = b.length;
        if (len > keyBytes.length)
            len = keyBytes.length;
        System.arraycopy(b, 0, keyBytes, 0, len);
        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

        byte[] results = cipher.doFinal(text.getBytes("UTF-8"));
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(results); // it returns the result as a String
    }


}
