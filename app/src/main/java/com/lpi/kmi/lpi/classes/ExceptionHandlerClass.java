package com.lpi.kmi.lpi.classes;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionHandlerClass implements Thread.UncaughtExceptionHandler {

    private final Activity myContext;
    private final String LINE_SEPARATOR = "\n";

    public ExceptionHandlerClass(Activity context) {
        myContext = context;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        // TODO Auto-generated method stub

        StringWriter stackTrace = new StringWriter();
        ex.printStackTrace(new PrintWriter(stackTrace));
        Log.e("", "Error in Main thread=" + ex.toString());

        DatabaseHelper.SaveErroLog(myContext, DatabaseHelper.db,
                "ExceptionHandlerClass", myContext.getLocalClassName() + "", ex.toString());

//        Intent intent = new Intent(myContext, SplashScreen.class);
        Intent intent = myContext.getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(myContext.getBaseContext().getPackageName());
        intent.putExtra("ExceptionFound_Flag", "Yes");
//        intent.putExtra("error", ex.toString());
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        myContext.startActivity(intent);

        if (DatabaseHelper.db != null &&
                (DatabaseHelper.db.isDbLockedByCurrentThread() || DatabaseHelper.db.isDbLockedByOtherThreads())) {
            DatabaseHelper.db.close();
        }

        System.gc();
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(10);
    }
}