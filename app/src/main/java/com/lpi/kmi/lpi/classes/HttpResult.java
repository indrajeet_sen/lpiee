package com.lpi.kmi.lpi.classes;

public class HttpResult
{
    public HttpErrors Error;
    public String Result;

    public HttpResult(String  serverMessage, HttpErrors error)
    {
        Result = serverMessage;
        Error = error;
    }

    public boolean IsError()
    {
        return !Error.equals(HttpErrors.NO_ERROR);
    }
}

enum HttpErrors
{
    NO_ERROR,
    ConnectTimeoutException,
    SocketTimeoutException,
    ClientProtocolException,
    IOException
}