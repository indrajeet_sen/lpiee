package com.lpi.kmi.lpi.classes;

import android.annotation.SuppressLint;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class LPIEEX509TrustManager implements X509TrustManager {

//	private static TrustManager[] trustManagers;
	private static boolean acceptSSL=true;
    private static final X509Certificate[] _AcceptedIssuers = new X509Certificate[] {};

    static SSLContext sslContext = null;
    static HostnameVerifier hostnameVerifier = null;
    static boolean allowUntrusted = true;
    static TrustManager[] trustAllCerts;


    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return _AcceptedIssuers;
    }

    // Commented by chandrashekhar as app got rejected due to TrustManager fault
    /*public static void allowAllSSL() {
       *//* HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {

            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });
        SSLContext context = null;
        if (trustManagers == null) {
            trustManagers = new TrustManager[] { new FakeX509TrustManager() };
        }
        try {
            context = SSLContext.getInstance("TLS");
            context.init(null, trustManagers, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
        }
        catch (Exception e) {
            e.printStackTrace();
        }*//*

        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }
        };

// Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            Log.d("CertError",e.toString());
        }

    }*/

    public static void allowAllSSL() {
        try {
            if (allowUntrusted) {
                GetAllCert();
                sslContext = SSLContext.getInstance("TLS");
                sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
                hostnameVerifier = new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        //                                if (hostname.equals("selfeuat.reliancegeneral.co.in")) {
                        return hostname.equals("krishmark.centralindia.cloudapp.azure.com");
                    }
                };
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void checkClientTrusted(final X509Certificate[] arg0, final String arg1) throws CertificateException {
            if(arg0.hashCode()==200){

            }
    }

    @Override
    public void checkServerTrusted(final X509Certificate[] chain, final String authType) {
        try{
            chain[0].checkValidity();
            acceptSSL = true;
        }catch (CertificateException ce){
            acceptSSL = true;
        }
    }

    @SuppressLint("TrustAllX509TrustManager")
    static void GetAllCert(){
        trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                X509Certificate[] cArrr = new X509Certificate[0];
                return cArrr;
            }

            @Override
            public void checkServerTrusted(final X509Certificate[] chain,
                                           final String authType) throws CertificateException {
            }


            @Override
            public void checkClientTrusted(final X509Certificate[] chain,
                                           final String authType) throws CertificateException {
            }
        }};
    }

}
