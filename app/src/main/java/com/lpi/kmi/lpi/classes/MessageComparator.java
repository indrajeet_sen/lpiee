package com.lpi.kmi.lpi.classes;

import com.lpi.kmi.lpi.adapter.GetterSetter.MessageContent;

import java.util.Comparator;

public class MessageComparator implements Comparator<MessageContent> {
    @Override
    public int compare(MessageContent mc1, MessageContent mc2) {
        if (mc1.getMobRefId().equals(mc2.getMobRefId())) {
            if (!mc1.getRespStatus().equals(mc2.getRespStatus())) {
                return 2;
            } else {
                return 0;
            }
        } else {
            return 1;
        }
    }
}
