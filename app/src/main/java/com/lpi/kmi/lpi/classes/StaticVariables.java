package com.lpi.kmi.lpi.classes;

import android.os.Build;

import java.math.BigInteger;
import java.text.SimpleDateFormat;

/**
 * Created by mustafakachwalla on 25/05/17.
 */

public class StaticVariables {
    public static BigInteger crnt_ExmId;
    public static int crnt_ExamType, TblNameForCount, crnt_QueId;
    public static String crnt_InscType = "0", crnt_ExmTypeName, crnt_ExmName, TestPage_flag, allowedAttempts = "1", UserMobileNo,
            UserName="", UserLoginId, ParentId, ParentName, IsAllQuesMendatory, crnt_ExamDuration,
            crnt_ExamDetails, IsPracticeExam, UserType = "", AppLink = "";
    public static byte[] profileBitmap = null;
    public static String mLob = "", mProduct = "", mSection = "";
    public static boolean isLogin = false;
    public static String mTitle = "", mIdentity = "", openContent = "", SectionId = "", InsType = "";
    public static String fileCreated = "", productFlag = "", offlineMode = "N";
    public static String deviceInfo = "MANUFACTURER : " + android.os.Build.MANUFACTURER +
            ", MODEL : " + Build.MODEL +
            ", OS Version : " + Build.VERSION.RELEASE;
    //    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss.SSS");
    public static SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy hh:mm a");
    public static String ParentStoragePath = "/.TrainingManager/FAQ/";
    public static String LessonStoragePath = "/.TrainingManager/Lesson/";
    public static String PostLicStoragePath = "/.www/";
    public static boolean isSummary = false;

    public static String SyncWIFI = "No", SyncMobile = "Yes", Notification = "Yes";
    public static String UserEmail = "";
    public static String statusDate = "";
    public static String androidId = "", locale = "";
//    public static List<Integer> QuestionId=new ArrayList<Integer>();
}
