package com.lpi.kmi.lpi.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.CandidateFeedbackActivity;
import com.lpi.kmi.lpi.activities.PublicUser.SearchModel;
import com.lpi.kmi.lpi.activities.ViewResultActivity;
import com.lpi.kmi.lpi.adapter.Adapters.LPI_StatusAdapter;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.util.ArrayList;
import java.util.Calendar;


public class AttemptedStatusFragment extends Fragment {

    RecyclerView recyclerView;
    LPI_StatusAdapter statusAdapter;
    LinearLayoutManager linearLayoutManager;
    ArrayList<SearchModel> mArraylistEventslist = new ArrayList<SearchModel>();
    TextView no_feeds_foundTextView;
    LPI_StatusAdapter.onClickListener onClickListener = new LPI_StatusAdapter.onClickListener() {


        @Override
        public void onCardViewClick(View view) {

        }

        @Override
        public void onProfileClick(int position, View view) {

        }

        @Override
        public void onStatusBarChartClick(int position, View view) {
            try {
                DatabaseHelper.InsertActivityTracker(getActivity(), DatabaseHelper.db,
                        StaticVariables.UserLoginId,
                        "A80016", "A80014", "View Psychometric test button clicked",
                        "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                        StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                        "", "", "Pending");
            } catch (Exception e) {
                e.printStackTrace();
            }
//            StaticVariables.UserLoginId = mArraylistEventslist.get(position).getUser_id();
            Intent i = new Intent(getActivity(), ViewResultActivity.class);
            i.putExtra("ExamId", "" + 4001);
            i.putExtra("AttemptedId", "");
            i.putExtra("CandidateId", "" + mArraylistEventslist.get(position).getUser_id());
            i.putExtra("initFrom", "LPI_StatusActivity");
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }

        @Override
        public void onFeedbackClick(int position, View view) {
            try {
                Intent intent = new Intent(getActivity(), CandidateFeedbackActivity.class);
                intent.putExtra("CandidateId", mArraylistEventslist.get(position).getUser_id());
                intent.putExtra("CandidateName", mArraylistEventslist.get(position).getName());
                intent.putExtra("ExamType", "5");
                intent.putExtra("initFrom", "Status");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().getApplicationContext().startActivity(intent);
                getActivity().finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onViewStatusClick(int position, View view) {

        }
    };

    public AttemptedStatusFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public AttemptedStatusFragment(ArrayList<SearchModel> mArraylistEventslist) {
        this.mArraylistEventslist = mArraylistEventslist;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void initUI() {
        if (mArraylistEventslist.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            no_feeds_foundTextView.setVisibility(View.GONE);
            statusAdapter = new LPI_StatusAdapter(getActivity(), mArraylistEventslist);
            linearLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemViewCacheSize(20);
            recyclerView.setDrawingCacheEnabled(true);
            recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            //statusAdapter.setHasStableIds(true);

            recyclerView.setAdapter(statusAdapter);
            statusAdapter.setOnItemClickListener(onClickListener);
        } else {
            recyclerView.setVisibility(View.GONE);
            no_feeds_foundTextView.setVisibility(View.VISIBLE);
        }

        try {
            DatabaseHelper.InsertActivityTracker(getActivity(), DatabaseHelper.db,
                    StaticVariables.UserLoginId,
                    "A80015", "A80014", "Attempted Status page called",
                    "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                    StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                    "", "", "Pending");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_attempted_status, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.lv_attempted);
        no_feeds_foundTextView = (TextView) view.findViewById(R.id.no_feeds_foundTextView);
        initUI();
        return view;
    }
}