package com.lpi.kmi.lpi.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.LPI_FAQ_ConversationActivity;
import com.lpi.kmi.lpi.activities.PublicUser.SearchModel;
import com.lpi.kmi.lpi.adapter.Adapters.LPI_FAQAdapter;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.util.ArrayList;
import java.util.Calendar;


public class CompleteFAQFragment extends Fragment {

    RecyclerView recyclerView;
    LPI_FAQAdapter statusAdapter;
    LinearLayoutManager linearLayoutManager;
    ArrayList<SearchModel> mArraylistEventslist = new ArrayList<SearchModel>();
    TextView no_feeds_foundTextView;

    public CompleteFAQFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public CompleteFAQFragment(ArrayList<SearchModel> mArraylistEventslistComplete) {
        this.mArraylistEventslist = mArraylistEventslistComplete;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void initUI() {
//        dummydata();
        if (mArraylistEventslist.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            statusAdapter = new LPI_FAQAdapter(getActivity(), mArraylistEventslist, "FAQ");
            linearLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(statusAdapter);
            statusAdapter.setOnItemClickListener(onClickListener);
        } else {
            recyclerView.setVisibility(View.GONE);
            no_feeds_foundTextView.setVisibility(View.VISIBLE);
        }
        try {
            DatabaseHelper.InsertActivityTracker(getActivity(), DatabaseHelper.db,
                    StaticVariables.UserLoginId,
                    "A80023", "A80018", "Complete FAQ page called",
                    "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                    StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                    "", "", "Pending");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_complete_faq, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.lv_complete);
        no_feeds_foundTextView = (TextView) view.findViewById(R.id.no_feeds_foundTextView);
        initUI();
        return view;
    }

    LPI_FAQAdapter.onClickListener onClickListener = new LPI_FAQAdapter.onClickListener() {


        @Override
        public void onCardViewClick(int position, View view) {
            try {
                Bundle bundle = new Bundle();
                bundle.putString("question", mArraylistEventslist.get(position).getStatus());
                bundle.putString("user_id", mArraylistEventslist.get(position).getUser_id());
                bundle.putString("trainer_id", mArraylistEventslist.get(position).getTrainer_id());
                bundle.putString("question_id", mArraylistEventslist.get(position).getQuesId());
                bundle.putString("user_name", mArraylistEventslist.get(position).getName());
                bundle.putByteArray("profile_pic", mArraylistEventslist.get(position).getImage());
                bundle.putString("isClosed", "Y");

                Intent i = new Intent(getContext(), LPI_FAQ_ConversationActivity.class);
                i.putExtras(bundle);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().startActivity(i);
                getActivity().finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onProfileClick(int position, View view) {

        }
    };
}