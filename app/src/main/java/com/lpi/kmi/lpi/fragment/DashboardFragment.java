package com.lpi.kmi.lpi.fragment;

import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;

import java.util.ArrayList;

/**
 * Created by mustafakachwalla on 02/02/17.
 */

public class DashboardFragment extends Fragment {

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_dashboard);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.dash_frament, container, false);
        PieChart pieChart = (PieChart) view.findViewById(R.id.chart1);
        try {
            ArrayList<Entry> entries = new ArrayList<>();
            entries.add(new Entry(6f, 0));
            entries.add(new Entry(8f, 1));
            entries.add(new Entry(6f, 2));
            entries.add(new Entry(10f, 3));
            entries.add(new Entry(18f, 4));
            entries.add(new Entry(9f, 5));

            PieDataSet dataset = new PieDataSet(entries, "");

            ArrayList<String> labels = new ArrayList<String>();
            labels.add("Creative thinker");
            labels.add("Innovator");
            labels.add("Neutral expert");
            labels.add("Versatile");
            labels.add("Error buster");
            labels.add("Helpful Encourager");

            int[] VORDIPLOM_COLORS = {
                    Color.rgb(153, 255, 51), Color.rgb(255, 102, 102), Color.rgb(255, 166, 77),
                    Color.rgb(51, 153, 255), Color.rgb(219, 112, 184), Color.rgb(0, 191, 255)
            };
            PieData data = new PieData(labels, dataset);
            dataset.setColors(VORDIPLOM_COLORS); //
            Legend legend = pieChart.getLegend();
            legend.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
            pieChart.getLegend().setWordWrapEnabled(true);
            pieChart.setDescription("");
//        pieChart.setDescriptionPosition(100,700);
            pieChart.setData(data);

            pieChart.animateY(4000);

            pieChart.saveToGallery("/sd/mychart.jpg", 85); // 85 is the quality of the image
        } catch (Exception e) {
            DatabaseHelper.SaveErroLog(getActivity(), DatabaseHelper.db,
                    "DashboardFragment", "PieChart", e.toString());
        }
        return view;

//        return inflater.inflate(R.layout.dash_frament, container, false);
    }
}
