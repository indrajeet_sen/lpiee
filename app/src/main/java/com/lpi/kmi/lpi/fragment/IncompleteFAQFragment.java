package com.lpi.kmi.lpi.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.syncFAQQueries;
import com.lpi.kmi.lpi.activities.LPI_FAQActivity;
import com.lpi.kmi.lpi.activities.LPI_FAQ_ConversationActivity;
import com.lpi.kmi.lpi.activities.PublicUser.SearchModel;
import com.lpi.kmi.lpi.adapter.Adapters.LPI_FAQAdapter;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;


public class IncompleteFAQFragment extends Fragment {

    static ImageView addFQATopic;
    RecyclerView recyclerView;
    LPI_FAQAdapter statusAdapter;
    LinearLayoutManager linearLayoutManager;
    TextView no_feeds_foundTextView;
    LinearLayout linAddSubject;
    ArrayList<SearchModel> mArraylistEventslist = new ArrayList<SearchModel>();
    ArrayList<SearchModel> mArraylistEventslistTemp = new ArrayList<SearchModel>();
    LPI_FAQAdapter.onClickListener onClickListener = new LPI_FAQAdapter.onClickListener() {

        @Override
        public void onCardViewClick(int position, View view) {
            try {
                DatabaseHelper.InsertActivityTracker(getActivity(), DatabaseHelper.db,
                        StaticVariables.UserLoginId,
                        "A80020", "A80018", "FAQ List item clicked",
                        "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                        StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                        "", "", "Pending");
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Bundle bundle = new Bundle();
                bundle.putString("question", mArraylistEventslist.get(position).getStatus());
                bundle.putString("user_id", mArraylistEventslist.get(position).getUser_id());
                bundle.putString("trainer_id", mArraylistEventslist.get(position).getTrainer_id());
                bundle.putString("question_id", mArraylistEventslist.get(position).getQuesId());
                bundle.putString("user_name", mArraylistEventslist.get(position).getName());
                bundle.putByteArray("profile_pic", mArraylistEventslist.get(position).getImage());
                bundle.putString("isClosed", "N");
                LPI_FAQActivity.strIsClosed = "N";
                Intent i = new Intent(getActivity(), LPI_FAQ_ConversationActivity.class);
                i.putExtras(bundle);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                getActivity().finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onProfileClick(int position, View view) {

        }

    };

    public IncompleteFAQFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public IncompleteFAQFragment(ArrayList<SearchModel> mArraylistEventslistIncomplete) {
        this.mArraylistEventslist = mArraylistEventslistIncomplete;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_incomplete_faq, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.lv_incomplete);
        no_feeds_foundTextView = (TextView) view.findViewById(R.id.no_feeds_foundTextView);
        addFQATopic = (ImageView) view.findViewById(R.id.addFaqTopic);
        linAddSubject = (LinearLayout) view.findViewById(R.id.linAddSubject);
        if (StaticVariables.UserType.equals("T"))
            linAddSubject.setVisibility(View.GONE);
        else {
            try {
                linAddSubject.setVisibility(View.VISIBLE);
                CommonClass.showTooltip(getActivity(), addFQATopic, "Add a new subject for conversation");
            } catch (Exception e) {
                e.printStackTrace();
                linAddSubject.setVisibility(View.VISIBLE);
            }
        }
        initUI();
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void initUI() {
//        dummydata();
        if (mArraylistEventslist.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            statusAdapter = new LPI_FAQAdapter(getActivity(), mArraylistEventslist, "FAQ");
            linearLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemViewCacheSize(20);
            recyclerView.setDrawingCacheEnabled(true);
            recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            recyclerView.setAdapter(statusAdapter);
            statusAdapter.setOnItemClickListener(onClickListener);
        } else {
            recyclerView.setVisibility(View.GONE);
            no_feeds_foundTextView.setVisibility(View.VISIBLE);
        }


        addFQATopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showConversationDialog();
            }
        });
        try {
            DatabaseHelper.InsertActivityTracker(getActivity(), DatabaseHelper.db,
                    StaticVariables.UserLoginId,
                    "A80019", "A80018", "Incomplete FAQ page called",
                    "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                    StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                    "", "", "Pending");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showConversationDialog() {
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        try {
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.add_conversation_dialog, null);
            Button btn_send = (Button) dialogView.findViewById(R.id.btn_send);
            final EditText edt_query = (EditText) dialogView.findViewById(R.id.edt_query);


            btn_send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String query = edt_query.getText().toString().trim();
                    String mobref = UUID.randomUUID().toString();
                    if (query.length() > 0 || !query.equals("")) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        DatabaseHelper.SaveFAQList(getActivity(), DatabaseHelper.db, StaticVariables.UserLoginId.trim(),
                                "0", "" + StaticVariables.ParentId, query, "",
                                StaticVariables.sdf.format(Calendar.getInstance().getTime()), StaticVariables.UserLoginId,
                                "N", StaticVariables.ParentName,
                                DatabaseHelper.getUserMobNo(getActivity(), DatabaseHelper.db, StaticVariables.UserLoginId.trim()),
                                null, "", mobref, simpleDateFormat.format(Calendar.getInstance().getTime()), "Pending");
                        alertDialog.dismiss();
                        displayList();
                        if (CommonClass.isConnected(getActivity())) {
                            new syncFAQQueries(new Callback<String>() {
                                @Override
                                public void execute(String result, String status) {
                                    displayList();
                                }
                            }, getActivity(), query, mobref).execute();
                        }
                    } else {
                        Toast.makeText(getContext(), "Please write query and then press submit button", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            alertDialog.setView(dialogView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            displayList();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void displayList() {
        try {

            Cursor mCursor = (DatabaseHelper.getFAQListCursor(getActivity(), DatabaseHelper.db, LPI_FAQActivity.strCandidateId, LPI_FAQActivity.strTrainerId, StaticVariables.UserType));
            if (mCursor.getCount() > 0) {
                mArraylistEventslistTemp.clear();
                for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                    SearchModel searchModel = new SearchModel();
                    searchModel.setName(mCursor.getString(mCursor.getColumnIndex("Name")));
                    searchModel.setImage(mCursor.getBlob(mCursor.getColumnIndex("Images")));
                    searchModel.setMobile_no(mCursor.getString(mCursor.getColumnIndex("Mobile")));
                    searchModel.setStatus(mCursor.getString(mCursor.getColumnIndex("QuesDesc")));
                    searchModel.setUser_id(mCursor.getString(mCursor.getColumnIndex("CandidateId")));
                    searchModel.setTrainer_id(mCursor.getString(mCursor.getColumnIndex("TrainerId")));
                    searchModel.setQuesId(mCursor.getString(mCursor.getColumnIndex("QuesId")));
                    if (!mCursor.getString(mCursor.getColumnIndex("isClosed")).equalsIgnoreCase("Y")) {
                        mArraylistEventslistTemp.add(searchModel);
                    }
                }
                if (mArraylistEventslistTemp.size() > 0) {
                    mArraylistEventslist.clear();
                    mArraylistEventslist.addAll(mArraylistEventslistTemp);
                    statusAdapter.notifyDataSetChanged();
//                recyclerView.invalidate();
                    recyclerView.refreshDrawableState();
                }
            }
            mCursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            initUI();
        }
    }

}