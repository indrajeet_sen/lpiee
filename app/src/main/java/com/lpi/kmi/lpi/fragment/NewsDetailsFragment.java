package com.lpi.kmi.lpi.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.AsyncTasks.GetCandidateFeedback;
import com.lpi.kmi.lpi.activities.AsyncTasks.SubmitExamResultFdback;
import com.lpi.kmi.lpi.adapter.GetterSetter.DetailReportContent;
import com.lpi.kmi.lpi.classes.Callback;
import com.lpi.kmi.lpi.classes.CommonClass;
import com.lpi.kmi.lpi.classes.StaticVariables;

import net.sqlcipher.Cursor;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class NewsDetailsFragment extends Fragment {

    public static String ExamId, QuestionId, ExamName;
    RecyclerView recyclerView;
    NewsDetailsFragment.NewsFeedAdapter newsFeedAdapter;
    LinearLayoutManager linearLayoutManager;
    TextView no_feeds_foundTextView;
    List<DetailReportContent> mNewsArrayList = new ArrayList<DetailReportContent>();
    NewsDetailsFragment.NewsFeedAdapter.onClickListener onClickListener = new NewsDetailsFragment.NewsFeedAdapter.onClickListener() {

        @Override
        public void onCardViewClick(int position, View view) {
            String ques = mNewsArrayList.get(position).getQuestion();
            QuestionId = mNewsArrayList.get(position).getQuesId();
            //showExamDialog(ExamId, QuestionId, ques); bharat 19 08 2020
        }
    };

    public NewsDetailsFragment() {

    }

    @SuppressLint("ValidFragment")
    public NewsDetailsFragment(String ExamId) {
        this.ExamId = ExamId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view;
        view = inflater.inflate(R.layout.fragment_news_details, container, false);
        initUI(view);
        return view;
    }

    private void initUI(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_feeds_details);
        no_feeds_foundTextView = (TextView) view.findViewById(R.id.no_feeds_foundTextView_details);

        if (CommonClass.isConnected(getActivity())) {
            if (DatabaseHelper.GetExamAttemptedCount(getActivity(), DatabaseHelper.db, StaticVariables.UserLoginId, ExamId) == 0) {
                new GetCandidateFeedback(getActivity(), new Callback() {
                    @Override
                    public void execute(Object result, String status) {
                        setRecyclerView();
                    }
                }, StaticVariables.UserLoginId, "5").execute();
            } else {
                setRecyclerView();
            }
        } else {
            setRecyclerView();
        }

    }

    private void setRecyclerView() {
        try {
            mNewsArrayList.clear();
            mNewsArrayList = getNewDetailArray();
            if (mNewsArrayList != null && mNewsArrayList.size() > 0) {
                recyclerView.setVisibility(View.VISIBLE);
                newsFeedAdapter = new NewsDetailsFragment.NewsFeedAdapter(getActivity(), mNewsArrayList);
                linearLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setAdapter(newsFeedAdapter);
                newsFeedAdapter.setOnItemClickListener(onClickListener);
            } else {
                recyclerView.setVisibility(View.GONE);
                no_feeds_foundTextView.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            recyclerView.setVisibility(View.GONE);
            no_feeds_foundTextView.setVisibility(View.VISIBLE);
        }
    }

    private List<DetailReportContent> getNewDetailArray() {
        Cursor mCursor = null;
        try {
            mCursor = DatabaseHelper.db.query(getActivity().getString(R.string.TBL_LPIExamQuestionMST),
                    null, "ExamId =?",
                    new String[]{"" + ExamId}, null, null, null);
            for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                DetailReportContent getterSetter = new DetailReportContent();
                getterSetter.setExamId(mCursor.getString(mCursor.getColumnIndex("ExamId")));
                getterSetter.setQuesId(mCursor.getString(mCursor.getColumnIndex("QuestionId")));
                getterSetter.setQuestion(mCursor.getString(mCursor.getColumnIndex("Question")));
                String strFeedback = DatabaseHelper.GetCndFeedbackAns(getActivity(), DatabaseHelper.db, getterSetter.getExamId(),
                        getterSetter.getQuesId(), StaticVariables.UserLoginId, 1);
                if (strFeedback != null && !strFeedback.equalsIgnoreCase("") && !strFeedback.equalsIgnoreCase("null")) {
                    getterSetter.setStatus("Feedback : " + strFeedback);
                } else
                    getterSetter.setStatus("Awaiting your feedback");
                /*if (!mCursor.isNull(mCursor.getColumnIndex("ReadStatus"))
                        && mCursor.getString(mCursor.getColumnIndex("ReadStatus")).equals("A")) {
                    getterSetter.setStatus("Feedback : " + DatabaseHelper.GetAttemptedAns(getActivity(), DatabaseHelper.db, getterSetter.getExamId(),
                        getterSetter.getQuesId(), StaticVariables.UserLoginId, 1));
                } else
                    getterSetter.setStatus("Awaiting your feedback");*/
                mNewsArrayList.add(getterSetter);
            }
            mCursor.close();
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
        return mNewsArrayList;
    }

    public void showExamDialog(final String examId, final String questionId, String ques) {
        try {
            final AlertDialog alertD;
            LayoutInflater logout_inflator = LayoutInflater.from(getActivity());
            final View openDialog = logout_inflator.inflate(R.layout.custom_content_exam_dialog, null);
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            alertDialogBuilder.setView(openDialog);
            alertD = alertDialogBuilder.create();
            alertD.setCancelable(false);


            final TextView promt_title = (TextView) openDialog.findViewById(R.id.promt_title);
            final TextView txt_ques = (TextView) openDialog.findViewById(R.id.txt_ques);
            final Button btn_yes = (Button) openDialog.findViewById(R.id.btn_yes);
            final Button btn_no = (Button) openDialog.findViewById(R.id.btn_no);
            final int[] str_SelectedAnswer = {0};
            final String[] str_SelectedAnswerFeedback = {""};
            final LinearLayout btn_1stAnsOpt = (LinearLayout) openDialog.findViewById(R.id.btn_1stAnsOpt);
            final LinearLayout btn_2ndAnsOpt = (LinearLayout) openDialog.findViewById(R.id.btn_2ndAnsOpt);
            final LinearLayout btn_3rdAnsOpt = (LinearLayout) openDialog.findViewById(R.id.btn_3rdAnsOpt);
            final LinearLayout btn_4thAnsOpt = (LinearLayout) openDialog.findViewById(R.id.btn_4thAnsOpt);
            final LinearLayout btn_psycho5thOpt = (LinearLayout) openDialog.findViewById(R.id.btn_psycho5thOpt);

            final TextView txt_1stAnsOpt = (TextView) openDialog.findViewById(R.id.txt_1stAnsOpt);
            final TextView txt_2ndAnsOpt = (TextView) openDialog.findViewById(R.id.txt_2ndAnsOpt);
            final TextView txt_3rdAnsOpt = (TextView) openDialog.findViewById(R.id.txt_3rdAnsOpt);
            final TextView txt_4thAnsOpt = (TextView) openDialog.findViewById(R.id.txt_4thAnsOpt);
            final TextView txt_5thAnsOpt = (TextView) openDialog.findViewById(R.id.txt_5thAnsOpt);
//        promt_title.setText(title);
            txt_ques.setText(ques);

            Cursor mCursor;
            try {
                mCursor = DatabaseHelper.db.query(getActivity().getString(R.string.TBL_LPIExamAnswerMST),
                        null, "ExamId =? and QuestionId =?",
                        new String[]{"" + examId, questionId}, null, null, null);
                if (mCursor.getCount() > 0) {
                    for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                        if (mCursor.getInt(mCursor.getColumnIndex("AnswerId")) == 1)
                            txt_1stAnsOpt.setText(mCursor.getString(mCursor.getColumnIndex("Answer")));
                        if (mCursor.getInt(mCursor.getColumnIndex("AnswerId")) == 2)
                            txt_2ndAnsOpt.setText(mCursor.getString(mCursor.getColumnIndex("Answer")));
                        if (mCursor.getInt(mCursor.getColumnIndex("AnswerId")) == 3)
                            txt_3rdAnsOpt.setText(mCursor.getString(mCursor.getColumnIndex("Answer")));
                        if (mCursor.getInt(mCursor.getColumnIndex("AnswerId")) == 4)
                            txt_4thAnsOpt.setText(mCursor.getString(mCursor.getColumnIndex("Answer")));
                        if (mCursor.getInt(mCursor.getColumnIndex("AnswerId")) == 5)
                            txt_5thAnsOpt.setText(mCursor.getString(mCursor.getColumnIndex("Answer")));
                    }
                } else {
                    Toast.makeText(getActivity(), "Please try after some time", Toast.LENGTH_SHORT).show();
                }
                mCursor.close();
            } catch (Exception e) {
                Log.e("", "Error=" + e.toString());
            }


            btn_1stAnsOpt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        btn_1stAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button_selected));
                        btn_2ndAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                        btn_3rdAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                        btn_4thAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                        btn_psycho5thOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                    } else {
                        btn_1stAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button_selected));
                        btn_2ndAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));
                        btn_3rdAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));
                        btn_4thAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));
                        btn_psycho5thOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));

                    }
                    str_SelectedAnswerFeedback[0] = txt_1stAnsOpt.getText().toString();
                }
            });
            btn_2ndAnsOpt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        btn_1stAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                        btn_2ndAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button_selected));
                        btn_3rdAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                        btn_4thAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                        btn_psycho5thOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                    } else {
                        btn_1stAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));
                        btn_2ndAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button_selected));
                        btn_3rdAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));
                        btn_4thAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));
                        btn_psycho5thOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));

                    }
                    str_SelectedAnswerFeedback[0] = txt_2ndAnsOpt.getText().toString();
                }
            });
            btn_3rdAnsOpt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                        btn_1stAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                        btn_2ndAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                        btn_3rdAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button_selected));
                        btn_4thAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                        btn_psycho5thOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                    } else {
                        btn_1stAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));
                        btn_2ndAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));
                        btn_3rdAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button_selected));
                        btn_4thAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));
                        btn_psycho5thOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));
                    }
                    str_SelectedAnswerFeedback[0] = txt_3rdAnsOpt.getText().toString();
                }
            });
            btn_4thAnsOpt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        btn_1stAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                        btn_2ndAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                        btn_3rdAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                        btn_4thAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button_selected));
                        btn_psycho5thOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                    } else {
                        btn_1stAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));
                        btn_2ndAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));
                        btn_3rdAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));
                        btn_4thAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button_selected));
                        btn_psycho5thOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));
                    }
                    str_SelectedAnswerFeedback[0] = txt_4thAnsOpt.getText().toString();
                }
            });
            btn_psycho5thOpt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        btn_1stAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                        btn_2ndAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                        btn_3rdAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                        btn_4thAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button));
                        btn_psycho5thOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ripple_effect_button_selected));
                    } else {
                        btn_1stAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));
                        btn_2ndAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));
                        btn_3rdAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));
                        btn_4thAnsOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button));
                        btn_psycho5thOpt.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_button_selected));
                    }
                    str_SelectedAnswerFeedback[0] = txt_5thAnsOpt.getText().toString();
                }
            });

            btn_yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (str_SelectedAnswerFeedback[0] != null && !str_SelectedAnswerFeedback[0].equals("")) {
                            alertD.dismiss();
                            str_SelectedAnswer[0] = DatabaseHelper.GetAnsId(getActivity(), DatabaseHelper.db,
                                    str_SelectedAnswerFeedback[0], BigInteger.valueOf(Long.valueOf(examId)), Integer.parseInt(questionId));

                            DatabaseHelper.SaveAttemptedQueAns(getActivity(), DatabaseHelper.db, "" + StaticVariables.UserLoginId,
                                    "" + ExamId, ExamName, "" + QuestionId,
                                    "Y", "" + str_SelectedAnswer[0], "" + str_SelectedAnswer[0],
                                    str_SelectedAnswerFeedback[0], "1");

                            DatabaseHelper.UpdateCndFeedback(getActivity(), DatabaseHelper.db, "" + StaticVariables.UserLoginId,
                                    "" + ExamId, "" + QuestionId, str_SelectedAnswerFeedback[0]);

                            new SubmitExamResultFdback(new Callback() {
                                @Override
                                public void execute(Object result, String status) {
                                    if (status.equals("0")) {
                                        ContentValues values = new ContentValues();
                                        values.put("ReadStatus", "A");
                                        DatabaseHelper.db.update(getActivity().getString(R.string.TBL_LPIExamQuestionMST),
                                                values, "ExamId=? and QuestionId=?",
                                                new String[]{examId, questionId});
                                        setRecyclerView();
                                    }
                                }
                            }, getActivity(), examId).execute();
                        } else {
                            Toast.makeText(getActivity(), "Please select any one option", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertD.dismiss();
                    ContentValues values = new ContentValues();
                    values.put("ReadStatus", "R");
                    DatabaseHelper.db.update(getActivity().getString(R.string.TBL_LPIExamQuestionMST),
                            values, "ExamId=? and QuestionId=?",
                            new String[]{examId, questionId});
                }
            });

            alertD.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static class NewsFeedAdapter extends RecyclerView.Adapter {

        List<DetailReportContent> newsArrayList;
        Context context;
        private NewsFeedAdapter.onClickListener onClickListener;

        public NewsFeedAdapter(Context context, List<DetailReportContent> listItems) {
            this.context = context;
            this.newsArrayList = listItems;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v;
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_feeds_details_row, parent, false);
            return new NewsDetailsFragment.NewsFeedAdapter.VHItem(v);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            try {
                if (newsArrayList.size() > 0) {
                    if (newsArrayList.get(position) != null) {
                        NewsFeedAdapter.VHItem news_data = (NewsFeedAdapter.VHItem) holder;
                        news_data.tv_ExamId.setText(newsArrayList.get(position).getExamId());
                        news_data.tv_QuesId.setText(newsArrayList.get(position).getQuesId());
                        news_data.tv_Question.setText(newsArrayList.get(position).getQuestion());
                        news_data.tv_Status.setText(newsArrayList.get(position).getStatus());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return newsArrayList.size();
        }

        public void setOnItemClickListener(NewsDetailsFragment.NewsFeedAdapter.onClickListener onItemClickListener) {
            onClickListener = onItemClickListener;
        }

        public interface onClickListener {
            void onCardViewClick(int position, View view);
        }

        class VHItem extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView tv_QuesId;
            TextView tv_Question;
            TextView tv_ExamId;
            TextView tv_Status;
            CardView cardView;

            public VHItem(View itemView) {
                super(itemView);

                tv_ExamId = (TextView) itemView.findViewById(R.id.text_ExamId);
                tv_QuesId = (TextView) itemView.findViewById(R.id.text_QuesId);
                tv_Question = (TextView) itemView.findViewById(R.id.text_Question);
                tv_Status = (TextView) itemView.findViewById(R.id.text_status);
                cardView = (CardView) itemView.findViewById(R.id.cardViewLayout);
                tv_Status.setVisibility(View.GONE);
                cardView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {

                switch (v.getId()) {
                    case R.id.cardViewLayout: {
                        onClickListener.onCardViewClick(getAdapterPosition(), v);
                    }
                    break;
                }
            }
        }

    }

}


