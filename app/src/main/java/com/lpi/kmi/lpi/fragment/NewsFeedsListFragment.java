package com.lpi.kmi.lpi.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.Services.PlayAudio;
import com.lpi.kmi.lpi.activities.EmotionalExcellenceListActivity;
import com.lpi.kmi.lpi.adapter.GetterSetter.DetailReportContent;
import com.lpi.kmi.lpi.classes.CommonClass;

import net.sqlcipher.Cursor;

import java.util.ArrayList;
import java.util.List;

public class NewsFeedsListFragment extends Fragment {
    View view;
    RecyclerView recyclerView;
    NewsFeedAdapter newsFeedAdapter;
    LinearLayoutManager linearLayoutManager;
    TextView no_feeds_foundTextView;
    List<DetailReportContent> mNewsArrayList = new ArrayList<DetailReportContent>();
   public static String CurrPosition=null;
   LinearLayout lin_recommendation;
   RelativeLayout rel_norec;
   Button btn_read;
   Boolean isPlayingSound = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_news_feeds_list, container, false);
        intUI(view);
        return view;
    }

    NewsFeedAdapter.onClickListener onClickListener = new NewsFeedAdapter.onClickListener() {

        @Override
        public void onCardViewClick(int position, View view) {
            FragmentManager fm = getFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            CurrPosition=mNewsArrayList.get(position).getStrNewsId();
            fragmentTransaction.replace(R.id.frameLayout, new NewsDetailsFragment(mNewsArrayList.get(position).getStrNewsId()), "Feedback");
            fragmentTransaction.commit();
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopAudio();
    }

    private void intUI(View view) {
        try {
            recyclerView = (RecyclerView) view.findViewById(R.id.rv_feeds);
            lin_recommendation = (LinearLayout) view.findViewById(R.id.lin_recommendation);
            rel_norec = (RelativeLayout) view.findViewById(R.id.rel_norec);
            no_feeds_foundTextView = (TextView) view.findViewById(R.id.no_feeds_foundTextView);
            btn_read = (Button) view.findViewById(R.id.btn_read);

            mNewsArrayList = getNewDetailArray();
            if (mNewsArrayList != null && mNewsArrayList.size() > 0) {
                EmotionalExcellenceListActivity.RefreshGrpTopic.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.VISIBLE);
                no_feeds_foundTextView.setVisibility(View.GONE);
                lin_recommendation.setVisibility(View.VISIBLE);
                rel_norec.setVisibility(View.GONE);
                newsFeedAdapter = new NewsFeedAdapter(getActivity(), mNewsArrayList);
                linearLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setAdapter(newsFeedAdapter);
                newsFeedAdapter.setOnItemClickListener(onClickListener);
            } else {
                EmotionalExcellenceListActivity.RefreshGrpTopic.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                no_feeds_foundTextView.setVisibility(View.GONE);
                lin_recommendation.setVisibility(View.GONE);
                rel_norec.setVisibility(View.VISIBLE);
            }

            btn_read.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isPlayingSound = !isPlayingSound;
                    if (isPlayingSound && !CommonClass.isMyServiceRunning(getActivity(),"PlayAudio")) {
                        btn_read.setText("Let me read it for you..!");
//                        btn_read.setCompoundDrawables(null,getActivity().getResources().getDrawable(R.drawable.ic_media_pause),null,null);
                        playAudio();
                    }else if (CommonClass.isMyServiceRunning(getActivity(),"PlayAudio")){
                        stopAudio();
                        btn_read.setText("Let me read it for you..!");
//                        btn_read.setCompoundDrawables(null,getActivity().getResources().getDrawable(R.drawable.ic_media_play),null,null);
                    }else {
                        stopAudio();
                        btn_read.setText("Let me read it for you..!");
//                        btn_read.setCompoundDrawables(null,getActivity().getResources().getDrawable(R.drawable.ic_media_play),null,null);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
//            recyclerView.setVisibility(View.GONE);
//            no_feeds_foundTextView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            no_feeds_foundTextView.setVisibility(View.GONE);
            lin_recommendation.setVisibility(View.GONE);
            rel_norec.setVisibility(View.VISIBLE);
        }
    }

    public void playAudio() {
        Intent objIntent = new Intent(getActivity(), PlayAudio.class);
//        getActivity().startService(objIntent);
        CommonClass.StartService(getActivity().getApplicationContext(), objIntent);
    }

    public void stopAudio() {
        Intent objIntent = new Intent(getActivity(), PlayAudio.class);
        getActivity().stopService(objIntent);
    }

    @SuppressLint("Range")
    private List<DetailReportContent> getNewDetailArray() {
        Cursor mCursor = null;
        String mobileNo = "", ParentId = "";
//        try {
//            mCursor = DatabaseHelper.db.query(getActivity().getString(R.string.TBL_iCandidate), null, "CandidateLoginId=?",
//                    new String[]{StaticVariables.UserLoginId + ""}, null, null, null);
//            for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
//                mobileNo = mCursor.getString(mCursor.getColumnIndex("CandidateMobileNo1"));
//                ParentId = mCursor.getString(mCursor.getColumnIndex("ParentId"));
//            }
//        } catch (Exception e) {
//            Log.e("", "Error=" + e.toString());
//        }

//        GetterSetter getterSetter = new GetterSetter();
//        getterSetter.setStrNewsId("1");
//        getterSetter.setStrNews("Welcome to LPI");
//        getterSetter.setStrNewsDesc(StaticVariables.UserName + "\nUser ID: " + StaticVariables.UserLoginId + "\nMobile No: " + mobileNo + "\nTrainer Id: " + ParentId);
//        mNewsArrayList.add(getterSetter);
        try {
            mCursor = DatabaseHelper.db.query(getActivity().getString(R.string.TBL_LPIExamMST), null, "ExamId <>? and ExamType=?",
                    new String[]{"" + 5001, "" + 5}, null, null, null);
            for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                DetailReportContent getterSetter = new DetailReportContent();
                getterSetter.setStrNewsId(mCursor.getString(mCursor.getColumnIndex("ExamId")));
                getterSetter.setStrNews(mCursor.getString(mCursor.getColumnIndex("ExamDesc")));
                getterSetter.setStrNewsDesc(mCursor.getString(mCursor.getColumnIndex("ExamDetails")));
                mNewsArrayList.add(getterSetter);
            }
            mCursor.close();
        } catch (Exception e) {
            Log.e("", "Error=" + e.toString());
        }
        return mNewsArrayList;
    }

    static class NewsFeedAdapter extends RecyclerView.Adapter {

        private onClickListener onClickListener;

        List<DetailReportContent> newsArrayList;
        Context context;

        public NewsFeedAdapter(Context context, List<DetailReportContent> listItems) {
            this.context = context;
            this.newsArrayList = listItems;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v;
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_feeds_row, parent, false);
            return new VHItem(v);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            try {
                if (newsArrayList.size() > 0) {
                    if (newsArrayList.get(position) != null) {
                        VHItem news_data = (VHItem) holder;
                        news_data.tv_news_heading.setText(newsArrayList.get(position).getStrNews());
                        news_data.tv_news_desc.setText(newsArrayList.get(position).getStrNewsDesc());
                        news_data.tv_news_Id.setText(newsArrayList.get(position).getStrNewsId());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return newsArrayList.size();
        }


        class VHItem extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView tv_news_heading;
            TextView tv_news_desc;
            TextView tv_news_Id;
            CardView cardView;

            public VHItem(View itemView) {
                super(itemView);

                tv_news_heading = (TextView) itemView.findViewById(R.id.tv_news_heading);
                tv_news_desc = (TextView) itemView.findViewById(R.id.tv_news_desc);
                tv_news_Id = (TextView) itemView.findViewById(R.id.text_ExamId);
                cardView = (CardView) itemView.findViewById(R.id.cardViewLayout);
                cardView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {

                switch (v.getId()) {
                    case R.id.cardViewLayout: {
                        onClickListener.onCardViewClick(getAdapterPosition(), v);
                    }
                    break;
                }
            }
        }

        public void setOnItemClickListener(onClickListener onItemClickListener) {
            this.onClickListener = onItemClickListener;
        }

        public interface onClickListener {
            void onCardViewClick(int position, View view);
        }
    }
}