package com.lpi.kmi.lpi.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lpi.kmi.lpi.DBHelper.DatabaseHelper;
import com.lpi.kmi.lpi.R;
import com.lpi.kmi.lpi.activities.PublicUser.SearchModel;
import com.lpi.kmi.lpi.adapter.Adapters.LPI_StatusAdapter;
import com.lpi.kmi.lpi.classes.StaticVariables;

import java.util.ArrayList;
import java.util.Calendar;


public class NonAttemptedStatusFragment extends Fragment {
    RecyclerView recyclerView;
    LPI_StatusAdapter statusAdapter;
    LinearLayoutManager linearLayoutManager;
    ArrayList<SearchModel> mArraylistEventslist = new ArrayList<SearchModel>();
    TextView no_feeds_foundTextView;


    public NonAttemptedStatusFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public NonAttemptedStatusFragment(ArrayList<SearchModel> mArraylistEventslist) {
        this.mArraylistEventslist = mArraylistEventslist;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_non_attempted_status, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.lv_non_attempted);
        no_feeds_foundTextView = (TextView) view.findViewById(R.id.no_feeds_foundTextView);

        initUI();
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("FragmentTwo", "onCreate");
    }

    private void initUI() {
        if (mArraylistEventslist.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            statusAdapter = new LPI_StatusAdapter(getActivity(), mArraylistEventslist);
            linearLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(statusAdapter);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemViewCacheSize(20);
            recyclerView.setDrawingCacheEnabled(true);
            recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
//            statusAdapter.setHasStableIds(true);
            statusAdapter.setOnItemClickListener(onClickListener);
        } else {
            recyclerView.setVisibility(View.GONE);
            no_feeds_foundTextView.setVisibility(View.VISIBLE);
        }
        try {
            DatabaseHelper.InsertActivityTracker(getActivity(), DatabaseHelper.db,
                    StaticVariables.UserLoginId,
                    "A80016", "A80014", "Not attempted page called",
                    "", "", "", StaticVariables.sdf.format(Calendar.getInstance().getTime()), "", "",
                    StaticVariables.UserLoginId, StaticVariables.sdf.format(Calendar.getInstance().getTime()),
                    "", "", "Pending");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    LPI_StatusAdapter.onClickListener onClickListener = new LPI_StatusAdapter.onClickListener() {


        @Override
        public void onCardViewClick(View view) {

        }

        @Override
        public void onProfileClick(int position, View view) {

        }

        @Override
        public void onStatusBarChartClick(int position, View view) {

        }

        @Override
        public void onFeedbackClick(int position, View view) {

        }

        @Override
        public void onViewStatusClick(int position, View view) {

        }
    };
}